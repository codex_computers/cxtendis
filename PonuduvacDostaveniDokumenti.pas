unit PonuduvacDostaveniDokumenti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, dxSkinsdxStatusBarPainter,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.ExtCtrls,
  dxStatusBar, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Vcl.ActnList,
  cxCheckBox, cxRichEdit, cxTextEdit, cxMemo, cxBlobEdit, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxContainer, cxLabel, cxDBLabel, Vcl.StdCtrls, Vcl.Menus,
  cxButtons, dxScreenTip, dxCustomHint, cxHint;

type
  TfrmPonuduvacDostaveniDokumenti = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dxStatusBar1: TdxStatusBar;
    cxGrid1DBTableView1PONUDA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1DOSTAVEN_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1tipNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1DOSTAVEN: TcxGridDBColumn;
    cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    ActionList1: TActionList;
    aIzlez: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    Panel1: TPanel;
    Label8: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxDBLabel2: TcxDBLabel;
    Label7: TLabel;
    Label1: TLabel;
    cxDBLabel3: TcxDBLabel;
    Panel2: TPanel;
    CheckBox1: TcxCheckBox;
    aPrevzemiPotrebniDokOdTender: TAction;
    cxHintStyleController1: TcxHintStyleController;
    Panel3: TPanel;
    ZapisiButton: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1VALIDNOSTPropertiesEditValueChanged(
      Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DOSTAVENPropertiesEditValueChanged(
      Sender: TObject);
    procedure aPrevzemiPotrebniDokOdTenderExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPonuduvacDostaveniDokumenti: TfrmPonuduvacDostaveniDokumenti;

implementation

{$R *.dfm}

uses dmUnit, Utils, dmResources, dmProcedureQuey, dmSystem;

procedure TfrmPonuduvacDostaveniDokumenti.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmPonuduvacDostaveniDokumenti.aPrevzemiPotrebniDokOdTenderExecute(
  Sender: TObject);
begin

 try
     dmPQ.insert6(dmpq.pPrevzemiKriteriumi, 'BROJ_TENDER', 'PONUDA_ID', Null,Null,Null,Null, dm.tblTenderBROJ.Value, dm.tblPonudiID.Value, Null,Null,Null,Null);
     dm.tblPonuduvacPotrebniDok.close;
     dm.tblPonuduvacPotrebniDok.Open;
 finally
     ShowMessage('������� ���������� �� ���������� ���������� !!!');
 end;
end;

procedure TfrmPonuduvacDostaveniDokumenti.CheckBox1Click(Sender: TObject);
var i:Integer;
begin
     if CheckBox1.Checked then
       begin
          dmPQ.qUpdateDostavenDokumentPonuda.Close;
          dmPQ.qUpdateDostavenDokumentPonuda.ParamByName('DOSTAVEN').Value:=1;
          dmpq.qUpdateDostavenDokumentPonuda.ParamByName('PONUDA_ID').Value:=dm.tblPonudiID.Value;
          dmpq.qUpdateDostavenDokumentPonuda.ExecQuery;
          dm.tblPonuduvacPotrebniDok.FullRefresh;
        end
      else
        begin
          dmPQ.qUpdateDostavenDokumentPonuda.Close;
          dmPQ.qUpdateDostavenDokumentPonuda.ParamByName('DOSTAVEN').Value:=0;
          dmpq.qUpdateDostavenDokumentPonuda.ParamByName('PONUDA_ID').Value:=dm.tblPonudiID.Value;
          dmpq.qUpdateDostavenDokumentPonuda.ExecQuery;
          dm.tblPonuduvacPotrebniDok.FullRefresh;
     end;
     cxGrid1.SetFocus;
end;

procedure TfrmPonuduvacDostaveniDokumenti.cxGrid1DBTableView1DOSTAVENPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPonuduvacPotrebniDok.Edit;
     dm.tblPonuduvacPotrebniDok.Post;
end;

procedure TfrmPonuduvacDostaveniDokumenti.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPonuduvacDostaveniDokumenti.cxGrid1DBTableView1VALIDNOSTPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPonuduvacPotrebniDok.Edit;
     dm.tblPonuduvacPotrebniDok.Post;
end;

procedure TfrmPonuduvacDostaveniDokumenti.FormCreate(Sender: TObject);
begin
     dm.tblPonuduvacPotrebniDok.close;
     dm.tblPonuduvacPotrebniDok.Open;

     cxGrid1DBTableView1.ViewData.Expand(true);
end;

end.
