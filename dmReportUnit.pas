unit dmReportUnit;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 10.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Controls, SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, pFIBStoredProc, Variants,
  FIBQuery, pFIBQuery, FIBDatabase, pFIBDatabase, Vcl.Dialogs, frxClass,
  frxDesgn, frxDBSet;

type
  TdmReport = class(TDataModule)
    dsTemplate: TDataSource;
    Template: TpFIBDataSet;
    TemplateID: TFIBIntegerField;
    TemplateVID: TFIBIntegerField;
    TemplateNAZIV: TFIBStringField;
    TemplateREPORT: TFIBBlobField;
    TemplateTS_INS: TFIBDateTimeField;
    TemplateTS_UPD: TFIBDateTimeField;
    TemplateUSR_UPD: TFIBStringField;
    TemplateUSR_INS: TFIBStringField;
    TemplateBROJ: TFIBIntegerField;
    OpenDialog1: TOpenDialog;
    qSetupD: TpFIBQuery;
    PokanaZaPodnesuvanjePonuda: TpFIBDataSet;
    PokanaZaPodnesuvanjePonudaBROJ: TFIBStringField;
    PokanaZaPodnesuvanjePonudaGODINA: TFIBSmallIntField;
    PokanaZaPodnesuvanjePonudaPREDMET_NABAVKA: TFIBStringField;
    PokanaZaPodnesuvanjePonudaOGLAS: TFIBStringField;
    PokanaZaPodnesuvanjePonudaDATUMOTVARANJEPONUDI: TFIBDateField;
    PokanaZaPodnesuvanjePonudaVREMEOTVARANJEPONUDI: TFIBTimeField;
    PokanaZaPodnesuvanjePonudaMESTOOTVARANJEPONUDI: TFIBStringField;
    PokanaZaPodnesuvanjePonudaPRIMANJEPONUDIDATUMDO: TFIBDateTimeField;
    frxPokanaZaPodnesuvanjePonuda: TfrxDBDataset;
    viewFirmi: TfrxDBDataset;
    PokanaZaPodnesuvanjePonudaLICE_KONTAKT: TFIBStringField;
    PokanaZaPodnesuvanjePonudaEMAIL_KONTAKT: TFIBStringField;
    PokanaZaPodnesuvanjePonudaINFO_AUKCIJA: TFIBBlobField;
    PokanaZaPodnesuvanjePonudaTIP_TENDER: TFIBSmallIntField;
    PokanaZaPodnesuvanjePonudaPOSTAPKANAZIV: TFIBStringField;
    PokanaZaPodnesuvanjePonudaramkovnaSpogodba: TFIBStringField;
    PokanaZaPodnesuvanjePonudaVREMETRAENJE_DOG: TFIBStringField;
    PokanaZaPodnesuvanjePonudaelektronskaAukcija: TFIBStringField;
    PokanaZaPodnesuvanjePonudaIZVOR_SREDSTVA: TFIBStringField;
    PokanaZaPodnesuvanjePonudatipOcena: TFIBStringField;
    PokanaZaPodnesuvanjePonudaBODIRANJE: TFIBStringField;
    TenderskaDokumentacija: TpFIBDataSet;
    frxTenderskaDokumentacija: TfrxDBDataset;
    TenderskaDokumentacijaBROJ: TFIBStringField;
    TenderskaDokumentacijaGODINA: TFIBSmallIntField;
    TenderskaDokumentacijaDATUM: TFIBDateField;
    TenderskaDokumentacijaMesec: TFIBStringField;
    TenderskaDokumentacijaPREDMET_NABAVKA: TFIBStringField;
    TenderskaDokumentacijaKriteriumDodeluvanjeDog: TFIBStringField;
    TenderskaDokumentacijaOGLAS: TFIBStringField;
    TenderskaDokumentacijaARHIVSKI_BROJ: TFIBStringField;
    TenderskaDokumentacijaPERIODPRIMANJEPONUDIDATUMDO: TFIBStringField;
    TenderskaDokumentacijaDATUMVREMEOTVARANJE: TFIBStringField;
    TenderskaDokumentacijaMESTO_OTVARANJE: TFIBStringField;
    TenderskaDokumentacijaGrupaNaziv: TFIBStringField;
    TenderskaDokumentacijaIZVOR_SREDSTVA: TFIBStringField;
    TenderskaDokumentacijaVIDPOSTAPKA: TFIBStringField;
    TenderskaDokumentacijagrupnaNabavka: TFIBStringField;
    TenderskaDokumentacijaelektronskiSredstva: TFIBStringField;
    TenderskaDokumentacijaPROCENETAVREDNOST: TFIBBCDField;
    TenderskaDokumentacijaBODIRANJE: TFIBStringField;
    PonudiPredmetNaEvaluacija: TpFIBDataSet;
    PonudiPredmetNaEvaluacijaPARTNERNAZIV: TFIBStringField;
    PonudiPredmetNaEvaluacijaBROJ: TFIBStringField;
    PonudiPredmetNaEvaluacijaDATUM_PONUDA: TFIBDateField;
    frxPonudiPredmetNaEvaluacija: TfrxDBDataset;
    KomisijaZaJavnaNabavka: TpFIBDataSet;
    KomisijaZaJavnaNabavkaIME_PREZIME: TFIBStringField;
    KomisijaZaJavnaNabavkaFUNKCIJA: TFIBStringField;
    frxKomisijaZaJavnaNabavka: TfrxDBDataset;
    frxReport1: TfrxReport;
    IzborNajpovolnaPonudaPoGrupi: TpFIBDataSet;
    frxIzborNajpovolnaPonudaPoGrupi: TfrxDBDataset;
    KriteriumiUtvrduvanjeSposobnost: TpFIBDataSet;
    frxKriteriumiUtvrduvanjeSposobnost: TfrxDBDataset;
    KriteriumiUtvrduvanjeSposobnostTIP_PARTNER: TFIBIntegerField;
    KriteriumiUtvrduvanjeSposobnostPARTNER: TFIBIntegerField;
    KriteriumiUtvrduvanjeSposobnostPARTNERNAZIV: TFIBStringField;
    KriteriumiUtvrduvanjeSposobnostDOSTAVEN: TFIBSmallIntField;
    KriteriumiUtvrduvanjeSposobnostVALIDNOST: TFIBStringField;
    KriteriumiUtvrduvanjeSposobnostNAZIV: TFIBMemoField;
    KriteriumiUtvrduvanjeSposobnostTIP: TFIBSmallIntField;
    KriteriumiUtvrduvanjeSposobnosttipNaziv: TFIBStringField;
    KriteriumiUtvrduvanjeSposobnostdostavenNaziv: TFIBStringField;
    IzborNajpovolnaPonudaPoGrupiPARTNERNAZIV: TFIBStringField;
    IzborNajpovolnaPonudaPoGrupiPARTNER: TFIBIntegerField;
    IzborNajpovolnaPonudaPoGrupiTIP_PARTNER: TFIBIntegerField;
    IzborNajpovolnaPonudaPoGrupiVKUPNO_BODOVI: TFIBBCDField;
    IzborNajpovolnaPonudaPoGrupiGRUPANAZIV_OUT: TFIBStringField;
    IzborNajpovolnaPonudaPoGrupiGRUPA_OUT: TFIBStringField;
    IzborNajpovolnaPonudaPoGrupiARTIKAL_SPOJUVANJE: TFIBStringField;
    ObrazlozenieNeprifateniPonudiStavki: TpFIBDataSet;
    ObrazlozenieNeprifateniPonudiStavkiTIP_PARTNER: TFIBIntegerField;
    ObrazlozenieNeprifateniPonudiStavkiPARTNER: TFIBIntegerField;
    ObrazlozenieNeprifateniPonudiStavkiARTIKAL: TFIBIntegerField;
    ObrazlozenieNeprifateniPonudiStavkiODBIENA_OBRAZLOZENIE: TFIBStringField;
    ObrazlozenieNeprifateniPonudiStavkiGRUPA: TFIBStringField;
    ObrazlozenieNeprifateniPonudiStavkiPARTNERNAZIV: TFIBStringField;
    frxObrazlozenieNeprifateniPonudiStavki: TfrxDBDataset;
    FinansiskaPonuda_1: TpFIBDataSet;
    frxFinansiskaPonuda_1: TfrxDBDataset;
    PokanetiPonuduvaci: TpFIBDataSet;
    frxPokanetiPonuduvaci: TfrxDBDataset;
    PokanetiPonuduvaciPOCETNA_CENA: TFIBFloatField;
    PokanetiPonuduvaciPONUDA_ID: TFIBIntegerField;
    PokanetiPonuduvaciTIP_PARTNER: TFIBIntegerField;
    PokanetiPonuduvaciPARTNER: TFIBIntegerField;
    PokanetiPonuduvaciGRUPA: TFIBStringField;
    PokanetiPonuduvaciGRUPANAZIV: TFIBStringField;
    PokanetiPonuduvaciPARTNERNAZIV: TFIBStringField;
    FinansiskaPonuda_1PARTNER: TFIBIntegerField;
    FinansiskaPonuda_1TIP_PARTNER: TFIBIntegerField;
    FinansiskaPonuda_1PARTNER_NAZIV: TFIBStringField;
    FinansiskaPonuda_1PODGRUPI: TFIBStringField;
    FinansiskaPonuda_1PONUDENA_CENA: TFIBBCDField;
    ObrazlozenieNeprifateniPonudi: TpFIBDataSet;
    frxObrazlozenieNeprifateniPonudi: TfrxDBDataset;
    ObrazlozenieNeprifateniPonudiTIP_PARTNER: TFIBIntegerField;
    ObrazlozenieNeprifateniPonudiPARTNER: TFIBIntegerField;
    ObrazlozenieNeprifateniPonudiPARTNERNAZIV: TFIBStringField;
    ObrazlozenieNeprifateniPonudiZABELESKA: TFIBMemoField;
    TenderskaDokumentacijaGARANCIJA_PONUDA: TFIBSmallIntField;
    TenderskaDokumentacijaavansno: TFIBStringField;
    OdlukaJavnaNabavka: TpFIBDataSet;
    frxDBOdlukaJavnaNabavka: TfrxDBDataset;
    OdlukaJavnaNabavkaPROCENETAVREDNOST: TFIBBCDField;
    OdlukaJavnaNabavkaPROCENETAVREDNOSTSODDV: TFIBBCDField;
    OdlukaJavnaNabavkaBODIRANJE: TFIBStringField;
    OdlukaJavnaNabavkaKOMISIJA: TFIBStringField;
    OdlukaJavnaNabavkaIZVOR_SREDSTVA: TFIBStringField;
    OdlukaJavnaNabavkaPOSTAPKA: TFIBStringField;
    OdlukaJavnaNabavkaOBRAZLOZENIE_POSTAPKA: TFIBMemoField;
    OdlukaJavnaNabavkaLICE_PODGOTOVKA: TFIBStringField;
    OdlukaJavnaNabavkaLICE_REALIZACIJA: TFIBStringField;
    qSetupUpravnik: TpFIBQuery;
    PodgrupiVoJN: TpFIBDataSet;
    PodgrupiVoJNNAZIV: TFIBStringField;
    frxDBPodgrupiVoJN: TfrxDBDataset;
    PodgrupiVoJNPREDMET_NABAVKA: TFIBStringField;
    PodgrupiVoJNGRUPA: TFIBStringField;
    KriteriumiBodiranjeZaJN: TpFIBDataSet;
    KriteriumiBodiranjeZaJNKRITERIUMNAZIV: TFIBStringField;
    KriteriumiBodiranjeZaJNMAX_BODOVI: TFIBBCDField;
    frxDBKriteriumiBodiranjeZaJN: TfrxDBDataset;
    KriteriumSposobnostiZaJN: TpFIBDataSet;
    frxDBKriteriumSposobnostiZaJN: TfrxDBDataset;
    KriteriumSposobnostiZaJNPOTREBNI_DOKUMENTI_ID: TFIBIntegerField;
    KriteriumSposobnostiZaJNNAZIV: TFIBMemoField;
    KriteriumSposobnostiZaJNTIP: TFIBSmallIntField;
    KriteriumSposobnostiZaJNtipNaziv: TFIBStringField;
    qSetupKategorijaDogOrgan: TpFIBQuery;
    ZapisnikOtvoranjePonudi: TpFIBDataSet;
    frxDBZapisnikOtvoranjePonudi: TfrxDBDataset;
    ZapisnikOtvoranjePonudiPONUDUVAC: TFIBStringField;
    ZapisnikOtvoranjePonudiKOMPLETNOST: TFIBStringField;
    ZapisnikOtvoranjePonudiGARANCII_POPUST: TFIBStringField;
    ZapisnikOtvoranjePonudiPRIMENA_VO_ROK: TFIBStringField;
    ZapisnikOtvoranjePonudiZATVOREN_KOVERT: TFIBStringField;
    ZapisnikOtvoranjePonudiOZNAKA_NE_OTVORAJ: TFIBStringField;
    ZapisnikOtvoranjePonudiPOVLEKUVANJE_PONUDA: TFIBStringField;
    ZapisnikOtvoranjePonudiIZMENA_PONUDA: TFIBStringField;
    ZapisnikOtvoranjePonudiROK_VAZNOST: TFIBStringField;
    ZapisnikOtvoranjePonudiVALUTA: TFIBStringField;
    ZapisnikOtvoranjePonudiPONUDUVAC_RED_BR: TFIBStringField;
    ZapisnikOtvoranjePonudiPRAZNO: TFIBStringField;
    ZapisnikOtvoranjePonudiGARANCIJA_IZNOS: TFIBBCDField;
    ZapisnikOtvoranjePonudiVKUPNACENABEZDDV: TFIBBCDField;
    ZapisnikOtvoranjePonudiPRETSTAVNIK: TFIBStringField;
    ZapisnikOtvoranjePonudiNAZIV_PONUDUVAC: TFIBStringField;
    TemplateFRF: TpFIBDataSet;
    TemplateFRFDATA: TFIBBlobField;
    dsTemplateFRF: TDataSource;
    qUserImePrezime: TpFIBQuery;
    OdlukaJavnaNabavkaODL_NABAVKA_BR: TFIBStringField;
    OdlukaJavnaNabavkaODL_NABAVKA_DAT: TFIBDateField;
    frxDBDogovorZaJN: TfrxDBDataset;
    DogovorZaJN: TpFIBDataSet;
    PocetnaRangListaNedeliviGrupi: TpFIBDataSet;
    frxDBDPocetnaRangListaNedeliviGrupi: TfrxDBDataset;
    KriteriumSposobnostiZaJNGRUPA: TFIBIntegerField;
    KriteriumSposobnostiZaJNGRUPANAZIV: TFIBStringField;
    OdlukaJavnaNabavkaKOMISIJA_POTPIS: TFIBStringField;
    OdlukaJavnaNabavkaLINIJA_POTPIS: TFIBStringField;
    tblDobitnici: TpFIBDataSet;
    tblDobitniciTIP_PARTNER: TFIBIntegerField;
    tblDobitniciPARTNER: TFIBIntegerField;
    tblDobitniciVID_ARTIKAL: TFIBIntegerField;
    tblDobitniciARTIKAL: TFIBIntegerField;
    tblDobitniciGRUPA: TFIBStringField;
    tblDobitniciARTIKAL_SPOJUVANJE: TFIBStringField;
    frxDBDobitnici: TfrxDBDataset;
    tblDobitniciPARTNER_NAZIV: TFIBStringField;
    KomisijaZaJavnaNabavkaREDBR: TFIBSmallIntField;
    tblPocetnaRangLista: TpFIBDataSet;
    frxDBDtblPocetnaRangLista: TfrxDBDataset;
    tblPocetnaRangListaO_TIP_PARTNER: TFIBIntegerField;
    tblPocetnaRangListaO_PARTNER: TFIBIntegerField;
    tblPocetnaRangListaO_PARTNER_NAZIV: TFIBStringField;
    tblPocetnaRangListaO_VID_ARTIKAL: TFIBIntegerField;
    tblPocetnaRangListaO_ARTIKAL: TFIBIntegerField;
    tblPocetnaRangListaO_NAZIV_ARTIKAL: TFIBStringField;
    tblPocetnaRangListaO_IZNOS_BEZDDV: TFIBFloatField;
    tblPocetnaRangListaO_BODOVI_KVALITET: TFIBBCDField;
    tblPocetnaRangListaO_VKUPNO_BODOVI: TFIBBCDField;
    tblPocetnaRangListaO_BROJ_PONUDA: TFIBStringField;
    tblPocetnaRangListaO_PONUDA_DATUM: TFIBDateField;
    tblPocetnaRangListaO_GRUPA: TFIBStringField;
    ZapisnikOtvoranjePonudiGARANCIJA_DATUM: TFIBDateField;
    OdlukaJavnaNabavkaKOMISIJA_SITE: TFIBStringField;
    frxDBDPocetnaRangListaNedeliviGrupiKriterium: TfrxDBDataset;
    PocetnaRangListaNedeliviGrupiKriterium: TpFIBDataSet;
    PocetnaRangListaNedeliviGrupiEDP: TpFIBDataSet;
    frxDBDPocetnaRangListaNedeliviGrupiEDP: TfrxDBDataset;
    PocetnaRangListaNedeliviGrupiEDPO_TIP_PARTNER: TFIBIntegerField;
    PocetnaRangListaNedeliviGrupiEDPO_PARTNER: TFIBIntegerField;
    PocetnaRangListaNedeliviGrupiEDPO_PARTNER_NAZIV: TFIBStringField;
    PocetnaRangListaNedeliviGrupiEDPO_IZNOS_BEZDDV: TFIBFloatField;
    PocetnaRangListaNedeliviGrupiEDPO_BODOVI_KVALITET: TFIBBCDField;
    PocetnaRangListaNedeliviGrupiEDPO_VKUPNO_BODOVI: TFIBBCDField;
    PocetnaRangListaNedeliviGrupiEDPO_BROJ_PONUDA: TFIBStringField;
    PocetnaRangListaNedeliviGrupiEDPO_PONUDA_DATUM: TFIBDateField;
    PocetnaRangListaNedeliviGrupiEDPO_GRUPA: TFIBStringField;
    PocetnaRangListaNedeliviGrupiEDPO_GRUPA_NAZIV: TFIBStringField;
    frxDogovorStavki: TfrxDBDataset;
    tblDogovorStavki: TpFIBDataSet;
    tblSiteDobitnici: TpFIBDataSet;
    frxSiteDobitnici: TfrxDBDataset;
    ObrazlozenieNeprifateniPonudiStavkiPARTNER_TIP_SIFRA: TFIBStringField;
    ObrazlozenieNeprifateniPonudiGrupno: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    frxObrazlozenieNeprifateniPonudiStavkiGrupno: TfrxDBDataset;
    frxSiteDobitniciVoIzvestajNeobitni: TfrxDBDataset;
    tblSiteDobitniciVoIzvestajNeobitni: TpFIBDataSet;
    tblSiteDobitniciVoIzvestajNeobitniPARTNER: TFIBIntegerField;
    tblSiteDobitniciVoIzvestajNeobitniTIP_PARTNER: TFIBIntegerField;
    tblSiteDobitniciVoIzvestajNeobitniDOBAVUVAC: TFIBStringField;
    tblSiteDobitniciVoIzvestajNeobitniDOBAVUVAC_MESTONAZIV: TFIBStringField;
    tblEmptyTable: TpFIBDataSet;
    frxEmptyTable: TfrxDBDataset;
    tblEmptyTableTIP_PARTNER: TFIBIntegerField;
    PonudiPredmetNaEvaluacijaSUMA_PONUDA_SO_DDV: TFIBFloatField;
    KriteriumiUtvrduvanjeSposobnostvalidnostNaziv: TFIBStringField;
    TenderskaDokumentacijaARH_BR_ISP: TFIBStringField;
    TenderskaDokumentacijaARH_DATUM_ISP: TFIBDateField;
    tblDogovorStavkiARTIKAL: TFIBIntegerField;
    tblDogovorStavkiJN_CPV: TFIBStringField;
    tblDogovorStavkiCENABEZDDV: TFIBFloatField;
    tblDogovorStavkiKOLICINA: TFIBBCDField;
    tblDogovorStavkiDANOK: TFIBFloatField;
    tblDogovorStavkiCENA: TFIBFloatField;
    tblDogovorStavkiIZNOS: TFIBFloatField;
    tblDogovorStavkiIZNOOSBEZDDV: TFIBFloatField;
    tblDogovorStavkiDDV: TFIBFloatField;
    tblDogovorStavkiARTIKALNAZIV: TFIBStringField;
    tblDogovorStavkiMERKA: TFIBStringField;
    tblDogovorStavkiINTEREN_BROJ: TFIBStringField;
    tblDogovorStavkiGRUPA: TFIBStringField;
    tblDogovorStavkiJN_TIP_ARTIKAL: TFIBIntegerField;
    tblDogovorStavkiGRUPA_NAZIV: TFIBStringField;
    tblDogovorStavkiVID_NAZIV: TFIBStringField;
    tblDogovorStavkiDELIVA: TFIBStringField;
    tblDogovorStavkiOdluka: TpFIBDataSet;
    frxDogovorStavkiOdluka: TfrxDBDataset;
    tblDogovorStavkiOdlukaBROJ_TENDER: TFIBStringField;
    tblDogovorStavkiOdlukaARTIKAL: TFIBIntegerField;
    tblDogovorStavkiOdlukaKOLICINA: TFIBBCDField;
    tblDogovorStavkiOdlukaARTNAZIV: TFIBStringField;
    tblDogovorStavkiOdlukaMERKA: TFIBStringField;
    tblDogovorStavkiOdlukaJN_CPV: TFIBStringField;
    tblDogovorStavkiOdlukaGODINA: TFIBSmallIntField;
    tblDogovorStavkiOdlukaCENA_BEZ_DDV: TFIBFloatField;
    tblDogovorStavkiOdlukaCENA_SO_DDV: TFIBFloatField;
    tblDogovorStavkiOdlukaDDV: TFIBFloatField;
    tblDogovorStavkiOdlukaGRUPA_NAZIV: TFIBStringField;
    tblDogovorStavkiOdlukaVID_NAZIV: TFIBStringField;
    tblDogovorStavkiOdlukaDELIVA: TFIBStringField;
    tblDogovorStavkiOdlukaGRUPA: TFIBStringField;
    tblDogovorStavkiOdlukaJN_TIP_ARTIKAL: TFIBIntegerField;
    tblDogovorStavkiOdlukaIZNOS_SO_DDV: TFIBBCDField;
    tblDogovorStavkiOdlukaIZNOS_BEZ_DDV: TFIBBCDField;
    tblDogovorStavkiOdlukaINTEREN_BROJ: TFIBStringField;
    tblPocetnaRangListaNedeliviGrupiO_TIP_PARTNER: TFIBIntegerField;
    tblPocetnaRangListaNedeliviGrupiO_PARTNER: TFIBIntegerField;
    tblPocetnaRangListaNedeliviGrupiO_PARTNER_NAZIV: TFIBStringField;
    PocetnaRangListaNedeliviGrupiO_IZNOS_BEZDDV: TFIBFloatField;
    tblPocetnaRangListaNedeliviGrupiO_BODOVI_KVALITET: TFIBBCDField;
    tblPocetnaRangListaNedeliviGrupiO_VKUPNO_BODOVI: TFIBBCDField;
    tblPocetnaRangListaNedeliviGrupiO_BROJ_PONUDA: TFIBStringField;
    tblPocetnaRangListaNedeliviGrupiO_PONUDA_DATUM: TFIBDateField;
    tblPocetnaRangListaNedeliviGrupiO_GRUPA: TFIBStringField;
    tblPocetnaRangListaNedeliviGrupiO_GRUPA_NAZIV: TFIBStringField;
    PocetnaRangListaNedeliviGrupiPROSECNA_CENA: TFIBFloatField;
    tblPocetnaRangListaNedeliviGrupiPOMALA_OD_PROSECNA: TFIBStringField;
    PocetnaRangListaNedeliviGrupiPRETHODNA_CENA: TFIBFloatField;
    PocetnaRangListaNedeliviGrupiODNOS_PROCENT: TFIBFloatField;
    PocetnaRangListaNedeliviGrupiPROSECNA_POLA_CENA: TFIBFloatField;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    function frxDesignSaveReport(Report: TfrxReport;
  SaveAs: Boolean; tag: integer): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  dmReport: TdmReport;

implementation

uses dmKonekcija, dmMaticni, DaNe, dmUnit;

{$R *.dfm}

function TdmReport.frxDesignSaveReport(Report: TfrxReport;
  SaveAs: Boolean; tag: integer): Boolean;
var template : TStream;
begin
  if tag = 1 then
    begin
        template := TMemoryStream.Create;
        template.Position := 0;
        frxReport1.SaveToStream(template);
        dm.tblTenderDokumenti.edit;
        (dm.tblTenderDokumentiTEMPLATE as TBlobField).LoadFromStream(template);
        dm.tblTenderDokumenti.Post;

        FreeAndNil(template);
     end;
end;

procedure TdmReport.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

end.
