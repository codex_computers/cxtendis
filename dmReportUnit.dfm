﻿object dmReport: TdmReport
  OldCreateOrder = False
  Height = 2006
  Width = 1215
  object dsTemplate: TDataSource
    DataSet = Template
    Left = 224
    Top = 24
  end
  object Template: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select hrt.id, hrt.vid, hrt.broj, hrt.naziv, hrt.report, hrt.ts_' +
        'ins, hrt.ts_upd, hrt.usr_upd, hrt.usr_ins'
      'from jn_report_tamplate hrt'
      'where hrt.broj = :broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 160
    Top = 24
    object TemplateID: TFIBIntegerField
      FieldName = 'ID'
    end
    object TemplateVID: TFIBIntegerField
      FieldName = 'VID'
    end
    object TemplateNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateREPORT: TFIBBlobField
      FieldName = 'REPORT'
      Size = 8
    end
    object TemplateTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object TemplateTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object TemplateUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object TemplateBROJ: TFIBIntegerField
      FieldName = 'BROJ'
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Rich Text  Format|*.rtf'
    InitialDir = '...\JN_DOKUMENTI\Template'
    Title = #1048#1079#1073#1077#1088#1077#1090#1077' '#1090#1077#1088#1082' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
    Left = 296
    Top = 24
  end
  object qSetupD: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(ss.v1,'#39#39') as v1'
      'from sys_setup ss'
      'where ss.p1='#39'JN'#39' and p2='#39'dokumenti'#39)
    Left = 368
    Top = 24
    qoStartTransaction = True
  end
  object PokanaZaPodnesuvanjePonuda: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.broj, '
      '       t.godina,'
      '       t.predmet_nabavka,'
      '       t.oglas,'
      '       ti.datum_otvaranje as DatumOtvaranjePonudi,'
      '       ti.vreme_otvaranje as VremeOtvaranjePonudi,'
      '       ti.mesto_otvaranje as mestoOtvaranjePonudi,'
      '       ti.datum_do as primanjePonudiDatumDo,'
      '       t.lice_kontakt,'
      '       t.email_kontakt,'
      '       ti.info_aukcija,'
      '       t.tip_tender,'
      '       tn.naziv as postapkaNaziv,'
      
        '       case when ti.ramkovna_spog = 1 then '#39#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074 +
        #1088#1096#1080' '#1089#1086' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072'.'#39
      
        '            when ti.ramkovna_spog = 0 then '#39#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074 +
        #1088#1096#1080' '#1089#1086' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072', '#1086#1076#1085#1086#1089#1085#1086' '#1085#1077#1084#1072' '#1076#1072' '#1089 +
        #1077' '#1082#1086#1088#1080#1089#1090#1080' '#1087#1086#1089#1077#1073#1085#1080#1086#1090' '#1085#1072#1095#1080#1085' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1089#1086' '#1088#1072#1084#1082#1086#1074#1085#1072 +
        ' '#1089#1087#1086#1075#1086#1076#1073#1072'.'#39
      
        '            else  '#39#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074#1088#1096#1080' '#1089#1086' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1072#1084#1082 +
        #1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072'/'#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074#1088#1096#1080' '#1089#1086' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079 +
        #1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072', '#1086#1076#1085#1086#1089#1085#1086' '#1085#1077#1084#1072' '#1076#1072' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1080' '#1087#1086#1089#1077#1073#1085#1080#1086#1090' '#1085#1072#1095#1080#1085' '#1079#1072' '#1076 +
        #1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1089#1086' '#1088#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072'.'#39
      '       end "ramkovnaSpogodba",'
      '       ti.vremetraenje_dog,'
      
        '       case when ti.el_aukcija = 1 then '#39#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074#1088#1096#1080 +
        ' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'.'#39
      
        '            when ti.el_aukcija = 0 then '#39'O'#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1085#1077#1084#1072' '#1076#1072' '#1079 +
        #1072#1074#1088#1096#1080' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'.'#39
      
        '            else '#39'O'#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1085#1077#1084#1072' '#1076#1072' '#1079#1072#1074#1088#1096#1080' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091 +
        #1082#1094#1080#1112#1072'. / '#1054#1074#1072#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1116#1077' '#1079#1072#1074#1088#1096#1080' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'. '#39
      '       end "elektronskaAukcija",'
      '       ti.izvor_sredstva,'
      
        '       case when t.tip_ocena = 0 then '#39#1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085 +
        #1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1077' '#1085#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072'. '#1047#1072' '#1085#1086#1089#1080#1090#1077#1083' '#1085#1072' '#1085#1072#1073#1072 +
        #1074#1082#1072#1090#1072' '#1116#1077' '#1073#1080#1076#1077' '#1080#1079#1073#1088#1072#1085' '#1086#1085#1086#1112' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088' '#1082#1086#1112' '#1116#1077' '#1087#1086#1085#1091#1076#1080' '#1085#1072#1112#1085#1080 +
        #1089#1082#1072' '#1094#1077#1085#1072', '#1072' '#1095#1080#1112#1072' '#1087#1086#1085#1091#1076#1072' '#1087#1088#1077#1090#1093#1086#1076#1085#1086' '#1077' '#1086#1094#1077#1085#1077#1090#1072' '#1082#1072#1082#1086' '#1087#1088#1080#1092#1072#1090#1083#1080#1074#1072'.'#39
      
        '            when t.tip_ocena = 1 then '#39#1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085 +
        #1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1077' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072#1090#1072' '#1087#1086#1085#1091#1076#1072'. '#1047#1072 +
        ' '#1085#1086#1089#1080#1090#1077#1083' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072#1090#1072' '#1116#1077' '#1073#1080#1076#1077' '#1080#1079#1073#1088#1072#1085' '#1086#1085#1086#1112' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088' '#1095#1080#1112 +
        #1072' '#1087#1086#1085#1091#1076#1072' '#1116#1077' '#1073#1080#1076#1077' '#1086#1094#1077#1085#1077#1090#1072' '#1082#1072#1082#1086' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072', '#1086#1076#1085#1086#1089#1085#1086' '#1082#1086#1112#1072 +
        ' '#1116#1077' '#1086#1089#1074#1086#1080' '#1085#1072#1112#1075#1086#1083#1077#1084' '#1073#1088#1086#1112' '#1085#1072' '#1073#1086#1076#1086#1074#1080' '#1082#1072#1082#1086' '#1079#1073#1080#1088' '#1085#1072' '#1073#1086#1076#1086#1074#1080#1090#1077' '#1079#1072' '#1089#1077#1082#1086#1112 +
        ' '#1077#1083#1077#1084#1077#1085#1090' '#1085#1072' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1086#1090' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072', '#1072' '#1082#1086#1112#1072' '#1087#1088#1077#1090 +
        #1093#1086#1076#1085#1086' '#1077' '#1086#1094#1077#1085#1077#1090#1072' '#1082#1072#1082#1086' '#1087#1088#1080#1092#1072#1090#1083#1080#1074#1072#39
      '       end "tipOcena",'
      
        '       (select proc_jn_template_1.str_kriterium from proc_jn_tem' +
        'plate_1(:broj)) as bodiranje'
      'from jn_tender t'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      'where t.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 80
    Top = 160
    object PokanaZaPodnesuvanjePonudaBROJ: TFIBStringField
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object PokanaZaPodnesuvanjePonudaPREDMET_NABAVKA: TFIBStringField
      FieldName = 'PREDMET_NABAVKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaOGLAS: TFIBStringField
      FieldName = 'OGLAS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaDATUMOTVARANJEPONUDI: TFIBDateField
      FieldName = 'DATUMOTVARANJEPONUDI'
    end
    object PokanaZaPodnesuvanjePonudaVREMEOTVARANJEPONUDI: TFIBTimeField
      FieldName = 'VREMEOTVARANJEPONUDI'
    end
    object PokanaZaPodnesuvanjePonudaMESTOOTVARANJEPONUDI: TFIBStringField
      FieldName = 'MESTOOTVARANJEPONUDI'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaPRIMANJEPONUDIDATUMDO: TFIBDateTimeField
      FieldName = 'PRIMANJEPONUDIDATUMDO'
    end
    object PokanaZaPodnesuvanjePonudaLICE_KONTAKT: TFIBStringField
      FieldName = 'LICE_KONTAKT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaEMAIL_KONTAKT: TFIBStringField
      FieldName = 'EMAIL_KONTAKT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaINFO_AUKCIJA: TFIBBlobField
      FieldName = 'INFO_AUKCIJA'
      Size = 8
    end
    object PokanaZaPodnesuvanjePonudaTIP_TENDER: TFIBSmallIntField
      FieldName = 'TIP_TENDER'
    end
    object PokanaZaPodnesuvanjePonudaPOSTAPKANAZIV: TFIBStringField
      FieldName = 'POSTAPKANAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaramkovnaSpogodba: TFIBStringField
      FieldName = 'ramkovnaSpogodba'
      Size = 217
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaVREMETRAENJE_DOG: TFIBStringField
      FieldName = 'VREMETRAENJE_DOG'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaelektronskaAukcija: TFIBStringField
      FieldName = 'elektronskaAukcija'
      Size = 103
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaIZVOR_SREDSTVA: TFIBStringField
      FieldName = 'IZVOR_SREDSTVA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudatipOcena: TFIBStringField
      FieldName = 'tipOcena'
      Size = 376
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanaZaPodnesuvanjePonudaBODIRANJE: TFIBStringField
      FieldName = 'BODIRANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxPokanaZaPodnesuvanjePonuda: TfrxDBDataset
    UserName = 'D1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'PREDMET_NABAVKA=PREDMET_NABAVKA'
      'OGLAS=OGLAS'
      'DATUMOTVARANJEPONUDI=DATUMOTVARANJEPONUDI'
      'VREMEOTVARANJEPONUDI=VREMEOTVARANJEPONUDI'
      'MESTOOTVARANJEPONUDI=MESTOOTVARANJEPONUDI'
      'PRIMANJEPONUDIDATUMDO=PRIMANJEPONUDIDATUMDO'
      'LICE_KONTAKT=LICE_KONTAKT'
      'EMAIL_KONTAKT=EMAIL_KONTAKT'
      'INFO_AUKCIJA=INFO_AUKCIJA'
      'TIP_TENDER=TIP_TENDER'
      'POSTAPKANAZIV=POSTAPKANAZIV'
      'ramkovnaSpogodba=ramkovnaSpogodba'
      'VREMETRAENJE_DOG=VREMETRAENJE_DOG'
      'elektronskaAukcija=elektronskaAukcija'
      'IZVOR_SREDSTVA=IZVOR_SREDSTVA'
      'tipOcena=tipOcena'
      'BODIRANJE=BODIRANJE')
    DataSet = PokanaZaPodnesuvanjePonuda
    BCDToCurrency = False
    Left = 272
    Top = 160
  end
  object viewFirmi: TfrxDBDataset
    UserName = 'viewFirmi'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'NAZIV=NAZIV'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'KOREN=KOREN'
      'SPISOK=SPISOK'
      'POTEKLO=POTEKLO'
      'RE=RE'
      'LOGO=LOGO'
      'PARTNERNAZIV=PARTNERNAZIV'
      'MESTONAZIV=MESTONAZIV'
      'ADRESA=ADRESA'
      'TEL=TEL'
      'FAX=FAX'
      'DANOCEN=DANOCEN'
      'LOGOTEXT=LOGOTEXT'
      'EMAIL_FIRMA=EMAIL_FIRMA'
      'EMAIL_USER=EMAIL_USER'
      'POSTA=POSTA'
      'PRAVA=PRAVA')
    DataSet = dmKon.viewFirmi
    BCDToCurrency = False
    Left = 304
    Top = 80
  end
  object TenderskaDokumentacija: TpFIBDataSet
    SelectSQL.Strings = (
      
        '                                                                ' +
        '             --substring(ti.datum_do from 9 for 2) ||'#39'.'#39'||substr' +
        'ing(ti.datum_do from 6 for 2)||'#39'.'#39'||substring(ti.datum_do from 1' +
        ' for 4)ee,'
      ''
      'select t.broj,'
      '       t.godina,'
      '       t.datum,'
      '       case when extract(month from t.datum) = 1 then '#39#1032#1072#1085#1091#1072#1088#1080#39
      '            when extract(month from t.datum) = 2 then '#39#1060#1077#1074#1088#1091#1072#1088#1080#39
      '            when extract(month from t.datum) = 3 then '#39#1052#1072#1088#1090#39
      '            when extract(month from t.datum) = 4 then '#39#1040#1087#1088#1080#1083#39
      '            when extract(month from t.datum) = 5 then '#39#1052#1072#1112#39
      '            when extract(month from t.datum) = 6 then '#39#1032#1091#1085#1080#39
      '            when extract(month from t.datum) = 7 then '#39#1032#1091#1083#1080#39
      '            when extract(month from t.datum) = 8 then '#39#1040#1074#1075#1091#1089#1090#39
      
        '            when extract(month from t.datum) = 9 then '#39#1057#1077#1087#1090#1077#1084#1074#1088#1080 +
        #39
      
        '            when extract(month from t.datum) = 10 then '#39#1054#1082#1090#1086#1084#1074#1088#1080 +
        #39
      '            when extract(month from t.datum) = 11 then '#39#1053#1086#1077#1084#1074#1088#1080#39
      
        '            when extract(month from t.datum) = 12 then '#39#1044#1077#1082#1077#1084#1074#1088#1080 +
        #39
      '       end "Mesec",'
      '       t.predmet_nabavka,'
      '       case when t.tip_ocena = 0 then '#39#1053#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072#39
      
        '            when t.tip_ocena = 1 then '#39#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091 +
        #1076#1072#39
      '       end "KriteriumDodeluvanjeDog",'
      '       t.oglas,'
      '       t.arhivski_broj,'
      
        '       substring(ti.datum_do from 9 for 2) ||'#39'.'#39'||substring(ti.d' +
        'atum_do from 6 for 2)||'#39'.'#39'||substring(ti.datum_do from 1 for 4)|' +
        '|'#39' '#1075#1086#1076#1080#1085#1072' '#1074#1086' '#39'||substring(ti.datum_do from 12 for 5) ||'#39' '#1095#1072#1089#1086#1090#39' ' +
        'as PeriodPrimanjePonudiDatumDo,'
      
        '       substring(ti.datum_otvaranje from 9 for 2) ||'#39'.'#39'||substri' +
        'ng(ti.datum_otvaranje from 6 for 2)||'#39'.'#39'||substring(ti.datum_otv' +
        'aranje from 1 for 4)||'#39' '#1075#1086#1076#1080#1085#1072' '#1074#1086' '#39'||substring(ti.vreme_otvaranj' +
        'e from 1 for 5) ||'#39' '#1095#1072#1089#1086#1090#39' as datumVremeOtvaranje,'
      '       ti.mesto_otvaranje,'
      '       case when t.grupa = 1 then '#39#1057#1090#1086#1082#1080#39
      '            when t.grupa = 2 then '#39#1059#1089#1083#1091#1075#1080#39
      '            when t.grupa = 3 then '#39#1056#1072#1073#1086#1090#1080#39
      '       end "GrupaNaziv",'
      '       ti.izvor_sredstva,'
      '       tn.naziv as vidPostapka,'
      '       case when ti.grupna_nabavka = 1 then '#39#1044#1072#39
      '            when ti.grupna_nabavka = 0 then '#39#1053#1077#39
      '            else '#39' '#39
      '       end "grupnaNabavka",'
      '       case when ti.el_sredstva = 0 then '#39#1053#1077#39
      '            when ti.el_sredstva = 1 then '#39#1044#1072#39
      '            else '#39' '#39
      '       end "elektronskiSredstva",'
      
        '       case when ti.avansno = 0 then '#39#1047#1072' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090 +
        ' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1085#1077' '#1077' '#1076#1086#1079#1074#1086#1083#1077#1085#1086' '#1072#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077'.'#39
      
        '            when ti.avansno = 1 then '#39#1047#1072' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090 +
        ' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1076#1086#1079#1074#1086#1083#1077#1085#1086' e '#1072#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077'.'#39
      '            else '#39' '#39
      '       end "avansno",'
      '       ti.garancija_ponuda,'
      '       t.iznos as procenetaVrednost,'
      
        '       (select proc_jn_template_1.str_kriterium from proc_jn_tem' +
        'plate_1(:mas_broj)) as bodiranje,'
      '       ti.ARH_BR_ISP, ti.ARH_DATUM_ISP'
      'from jn_tender t'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      'where t.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 232
    object TenderskaDokumentacijaBROJ: TFIBStringField
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object TenderskaDokumentacijaDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object TenderskaDokumentacijaMesec: TFIBStringField
      FieldName = 'Mesec'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaPREDMET_NABAVKA: TFIBStringField
      FieldName = 'PREDMET_NABAVKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaKriteriumDodeluvanjeDog: TFIBStringField
      FieldName = 'KriteriumDodeluvanjeDog'
      Size = 27
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaOGLAS: TFIBStringField
      FieldName = 'OGLAS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaARHIVSKI_BROJ: TFIBStringField
      FieldName = 'ARHIVSKI_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaPERIODPRIMANJEPONUDIDATUMDO: TFIBStringField
      FieldName = 'PERIODPRIMANJEPONUDIDATUMDO'
      Size = 119
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaDATUMVREMEOTVARANJE: TFIBStringField
      FieldName = 'DATUMVREMEOTVARANJE'
      Size = 62
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaMESTO_OTVARANJE: TFIBStringField
      FieldName = 'MESTO_OTVARANJE'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaGrupaNaziv: TFIBStringField
      FieldName = 'GrupaNaziv'
      Size = 6
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaIZVOR_SREDSTVA: TFIBStringField
      FieldName = 'IZVOR_SREDSTVA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaVIDPOSTAPKA: TFIBStringField
      FieldName = 'VIDPOSTAPKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijagrupnaNabavka: TFIBStringField
      FieldName = 'grupnaNabavka'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaelektronskiSredstva: TFIBStringField
      FieldName = 'elektronskiSredstva'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaPROCENETAVREDNOST: TFIBBCDField
      FieldName = 'PROCENETAVREDNOST'
      DisplayFormat = '0.0000 , .'
      Size = 2
    end
    object TenderskaDokumentacijaBODIRANJE: TFIBStringField
      FieldName = 'BODIRANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaGARANCIJA_PONUDA: TFIBSmallIntField
      FieldName = 'GARANCIJA_PONUDA'
    end
    object TenderskaDokumentacijaavansno: TFIBStringField
      FieldName = 'avansno'
      Size = 75
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaARH_BR_ISP: TFIBStringField
      FieldName = 'ARH_BR_ISP'
      Transliterate = False
      EmptyStrToNull = True
    end
    object TenderskaDokumentacijaARH_DATUM_ISP: TFIBDateField
      FieldName = 'ARH_DATUM_ISP'
    end
  end
  object frxTenderskaDokumentacija: TfrxDBDataset
    UserName = 'TenDok'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ=BROJ'
      'GODINA=GODINA'
      'DATUM=DATUM'
      'Mesec=Mesec'
      'PREDMET_NABAVKA=PREDMET_NABAVKA'
      'KriteriumDodeluvanjeDog=KriteriumDodeluvanjeDog'
      'OGLAS=OGLAS'
      'ARHIVSKI_BROJ=ARHIVSKI_BROJ'
      'PERIODPRIMANJEPONUDIDATUMDO=PONUDIDATUMDO'
      'DATUMVREMEOTVARANJE=DATUMVREMEOTVARANJE'
      'MESTO_OTVARANJE=MESTO_OTVARANJE'
      'GrupaNaziv=GrupaNaziv'
      'IZVOR_SREDSTVA=IZVOR_SREDSTVA'
      'VIDPOSTAPKA=VIDPOSTAPKA'
      'grupnaNabavka=grupnaNabavka'
      'elektronskiSredstva=elektronskiSredstva'
      'PROCENETAVREDNOST=PROCVREDNOST'
      'BODIRANJE=BODIRANJE'
      'GARANCIJA_PONUDA=GARANCIJA_PONUDA'
      'avansno=avansno'
      'ARH_BR_ISP=ARH_BR_ISP'
      'ARH_DATUM_ISP=ARH_DATUM_ISP')
    DataSet = TenderskaDokumentacija
    BCDToCurrency = False
    Left = 272
    Top = 232
  end
  object PonudiPredmetNaEvaluacija: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT p.NAZIV partnerNaziv,'
      '       jp.broj,'
      '       jp.datum_ponuda,'
      '       sum(ps.cena) as suma_ponuda_so_DDV'
      'FROM   jn_ponudi jp'
      
        'inner join mat_PARTNER p on (jp.PARTNER = P.ID and jp.TIP_PARTNE' +
        'R = P.TIP_PARTNER)'
      
        'inner join jn_ponudi_stavki ps on ps.ponuda_id = jp.id and ps.od' +
        'biena = 0'
      'where  jp.BROJ_TENDER =:mas_broj and jp.odbiena = 0'
      'group by 1, 2, 3'
      'ORDER BY  4')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 312
    object PonudiPredmetNaEvaluacijaPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPredmetNaEvaluacijaBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PonudiPredmetNaEvaluacijaDATUM_PONUDA: TFIBDateField
      FieldName = 'DATUM_PONUDA'
    end
    object PonudiPredmetNaEvaluacijaSUMA_PONUDA_SO_DDV: TFIBFloatField
      FieldName = 'SUMA_PONUDA_SO_DDV'
    end
  end
  object frxPonudiPredmetNaEvaluacija: TfrxDBDataset
    UserName = #1055#1086#1085#1091#1076#1080' '#1082#1086#1080' '#1089#1077' '#1087#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1077#1074#1072#1083#1091#1072#1094#1080#1112#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'PARTNERNAZIV=PARTNERNAZIV'
      'BROJ=BROJ'
      'DATUM_PONUDA=DATUM_PONUDA'
      'SUMA_PONUDA_SO_DDV=SUMA_PONUDA_SO_DDV')
    DataSet = PonudiPredmetNaEvaluacija
    BCDToCurrency = False
    Left = 272
    Top = 312
  end
  object KomisijaZaJavnaNabavka: TpFIBDataSet
    SelectSQL.Strings = (
      'select k.ime_prezime,'
      '       tk.funkcija,'
      '       tk.redbr'
      'from jn_tender_komisija tk'
      'inner join jn_komisija k on k.id = tk.clen'
      'where tk.broj_tender = :mas_broj  and tk.prisuten = 1'
      'order by tk.redbr')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 80
    Top = 384
    object KomisijaZaJavnaNabavkaIME_PREZIME: TFIBStringField
      FieldName = 'IME_PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object KomisijaZaJavnaNabavkaFUNKCIJA: TFIBStringField
      FieldName = 'FUNKCIJA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object KomisijaZaJavnaNabavkaREDBR: TFIBSmallIntField
      FieldName = 'REDBR'
    end
  end
  object frxKomisijaZaJavnaNabavka: TfrxDBDataset
    UserName = #1050#1086#1084#1080#1089#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'IME_PREZIME=IME_PREZIME'
      'FUNKCIJA=FUNKCIJA'
      'REDBR=REDBR')
    DataSet = KomisijaZaJavnaNabavka
    BCDToCurrency = False
    Left = 272
    Top = 384
  end
  object frxReport1: TfrxReport
    Version = '5.4.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40519.596180625000000000
    ReportOptions.LastChange = 41192.540644108800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '                                                                ' +
        '                   '
      'end;'
      ''
      'procedure richFileOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '               '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 40
    Top = 32
    Datasets = <
      item
        DataSet = frxPokanaZaPodnesuvanjePonuda
        DataSetName = 'D1'
      end
      item
        DataSet = frxFinansiskaPonuda_1
        DataSetName = 'FinansiskaPonuda_1'
      end
      item
        DataSet = dmRes.frxDBDataset1
        DataSetName = 'frxDBDataset1'
      end
      item
        DataSet = dmRes.frxDBViewFirmi
        DataSetName = 'frxDBviewFirmi'
      end
      item
        DataSet = frxDBDPocetnaRangListaNedeliviGrupi
        DataSetName = 'PocetnaRangListaNedeliviGrupi'
      end
      item
        DataSet = frxTenderskaDokumentacija
        DataSetName = 'TenDok'
      end
      item
        DataSet = viewFirmi
        DataSetName = 'viewFirmi'
      end
      item
        DataSet = frxDBDogovorZaJN
        DataSetName = #1044#1086#1075#1086#1074#1086#1088
      end
      item
        DataSet = frxDBZapisnikOtvoranjePonudi
        DataSetName = #1047#1072#1087#1080#1089#1085#1080#1082' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080
      end
      item
        DataSet = frxKomisijaZaJavnaNabavka
        DataSetName = #1050#1086#1084#1080#1089#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      end
      item
        DataSet = frxDBKriteriumSposobnostiZaJN
        DataSetName = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1032#1053
      end
      item
        DataSet = frxDBKriteriumiBodiranjeZaJN
        DataSetName = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
      end
      item
        DataSet = frxKriteriumiUtvrduvanjeSposobnost
        DataSetName = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072' '
      end
      item
        DataSet = frxObrazlozenieNeprifateniPonudi
        DataSetName = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1085#1077#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
      end
      item
        DataSet = frxDBOdlukaJavnaNabavka
        DataSetName = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      end
      item
        DataSet = frxDBPodgrupiVoJN
        DataSetName = #1055#1086#1076#1075#1088#1091#1087#1080' '#1074#1086' '#1032#1053
      end
      item
        DataSet = frxPonudiPredmetNaEvaluacija
        DataSetName = #1055#1086#1085#1091#1076#1080' '#1082#1086#1080' '#1089#1077' '#1087#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1077#1074#1072#1083#1091#1072#1094#1080#1112#1072
      end
      item
        DataSet = frxPokanetiPonuduvaci
        DataSetName = #1055#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1087#1086#1082#1072#1085#1077#1090#1080' '#1085#1072' '#1077#1040#1091#1082#1094#1080#1112#1072
      end
      item
        DataSet = frxIzborNajpovolnaPonudaPoGrupi
        DataSetName = #1055#1088#1077#1076#1083#1086#1075' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      end
      item
        DataSet = frxObrazlozenieNeprifateniPonudiStavki
        DataSetName = #1056#1072#1079#1075#1083#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1090#1077#1093#1085#1080#1095#1082#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 25.400000000000000000
      RightMargin = 25.400000000000000000
      TopMargin = 25.400000000000000000
      BottomMargin = 25.400000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 910.866730000000000000
        Top = 18.897650000000000000
        Width = 601.701176000000000000
        RowCount = 0
        StartNewPage = True
        Stretched = True
        object richFile: TfrxRichView
          Align = baClient
          Width = 601.701176000000000000
          Height = 910.866730000000000000
          StretchMode = smMaxHeight
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235315C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313037317B5C666F6E7474626C7B5C66
            305C666E696C5C6663686172736574323034205461686F6D613B7D7D0D0A7B5C
            2A5C67656E657261746F7220526963686564323020362E322E393230307D5C76
            6965776B696E64345C756331200D0A5C706172645C66305C667331365C706172
            0D0A7D0D0A00}
        end
      end
    end
  end
  object IzborNajpovolnaPonudaPoGrupi: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_jn_najponuda_grupa_artikli.partnernaziv,'
      '       proc_jn_najponuda_grupa_artikli.partner,'
      '       proc_jn_najponuda_grupa_artikli.tip_partner,'
      '       proc_jn_najponuda_grupa_artikli.vkupno_bodovi,'
      '       proc_jn_najponuda_grupa_artikli.grupanaziv_out,'
      '       proc_jn_najponuda_grupa_artikli.grupa_out,'
      '       proc_jn_najponuda_grupa_artikli.artikal_spojuvanje'
      'from proc_jn_najponuda_grupa_artikli(:BROJ_TENDER)'
      
        'order by proc_jn_najponuda_grupa_artikli.tip_partner,proc_jn_naj' +
        'ponuda_grupa_artikli.partner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 456
    object IzborNajpovolnaPonudaPoGrupiPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborNajpovolnaPonudaPoGrupiPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object IzborNajpovolnaPonudaPoGrupiTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object IzborNajpovolnaPonudaPoGrupiVKUPNO_BODOVI: TFIBBCDField
      FieldName = 'VKUPNO_BODOVI'
      Size = 2
    end
    object IzborNajpovolnaPonudaPoGrupiGRUPANAZIV_OUT: TFIBStringField
      FieldName = 'GRUPANAZIV_OUT'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborNajpovolnaPonudaPoGrupiGRUPA_OUT: TFIBStringField
      FieldName = 'GRUPA_OUT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object IzborNajpovolnaPonudaPoGrupiARTIKAL_SPOJUVANJE: TFIBStringField
      FieldName = 'ARTIKAL_SPOJUVANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxIzborNajpovolnaPonudaPoGrupi: TfrxDBDataset
    UserName = #1055#1088#1077#1076#1083#1086#1075' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072
    CloseDataSource = False
    DataSet = IzborNajpovolnaPonudaPoGrupi
    BCDToCurrency = False
    Left = 272
    Top = 448
  end
  object KriteriumiUtvrduvanjeSposobnost: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.tip_partner,'
      '       p.partner,'
      '       mp.naziv as partnerNaziv,'
      '       pd.dostaven,'
      '       pd.validnost,'
      '       jpd.naziv,'
      '       jpd.tip,'
      '       case when jpd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when jpd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112 +
        #1073#1072#39
      
        '            when jpd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087 +
        #1086#1089#1086#1073#1085#1086#1089#1090#39
      
        '            when jpd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080 +
        #1090#1077#1090#39
      
        '            when jpd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080 +
        #1074#1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when jpd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077 +
        #1089#1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39'        '
      '       end "tipNaziv",'
      '       case when pd.dostaven = 0 then '#39#1053#1077#39
      '            when pd.dostaven = 1 then '#39#1044#1072#39
      '       end "dostavenNaziv",'
      '       case when pd.validnost = 0 then '#39#1053#1077#39
      '            when pd.validnost = 1 then '#39#1044#1072#39
      '       end "validnostNaziv"'
      'from jn_ponuduvac_dok pd'
      
        'inner join jn_potrebni_dokumenti jpd on jpd.id = pd.dostaven_dok' +
        'ument'
      'inner join jn_ponudi p on p.id = pd.ponuda_id'
      
        'inner join mat_partner mp on mp.tip_partner = p.tip_partner and ' +
        'mp.id = p.partner'
      'where p.broj_tender = :mas_broj'
      'order by p.tip_partner, p.partner, jpd.tip')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 80
    Top = 520
    object KriteriumiUtvrduvanjeSposobnostTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object KriteriumiUtvrduvanjeSposobnostPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object KriteriumiUtvrduvanjeSposobnostPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumiUtvrduvanjeSposobnostDOSTAVEN: TFIBSmallIntField
      FieldName = 'DOSTAVEN'
    end
    object KriteriumiUtvrduvanjeSposobnostVALIDNOST: TFIBStringField
      FieldName = 'VALIDNOST'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumiUtvrduvanjeSposobnostNAZIV: TFIBMemoField
      FieldName = 'NAZIV'
      BlobType = ftMemo
      Size = 8
    end
    object KriteriumiUtvrduvanjeSposobnostTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
    object KriteriumiUtvrduvanjeSposobnosttipNaziv: TFIBStringField
      FieldName = 'tipNaziv'
      Size = 45
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumiUtvrduvanjeSposobnostdostavenNaziv: TFIBStringField
      FieldName = 'dostavenNaziv'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumiUtvrduvanjeSposobnostvalidnostNaziv: TFIBStringField
      FieldName = 'validnostNaziv'
      Size = 12
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxKriteriumiUtvrduvanjeSposobnost: TfrxDBDataset
    UserName = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072' '
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'PARTNERNAZIV=PARTNERNAZIV'
      'DOSTAVEN=DOSTAVEN'
      'VALIDNOST=VALIDNOST'
      'NAZIV=NAZIV'
      'TIP=TIP'
      'tipNaziv=tipNaziv'
      'dostavenNaziv=dostavenNaziv'
      'validnostNaziv=validnostNaziv')
    DataSet = KriteriumiUtvrduvanjeSposobnost
    BCDToCurrency = False
    Left = 272
    Top = 520
  end
  object ObrazlozenieNeprifateniPonudiStavki: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.tip_partner,'
      '       ps.partner,'
      '       ps.artikal,'
      '       ps.odbiena_obrazlozenie,'
      '       ma.grupa,'
      '       mp.naziv partnernaziv,'
      '       ps.TIP_PARTNER||'#39'/'#39'||ps.partner as partner_tip_sifra'
      'from jn_ponudi_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ps.vid_artik' +
        'al = ma.artvid'
      
        'inner join mat_partner mp on mp.tip_partner = ps.tip_partner and' +
        ' mp.id = ps.partner'
      
        'where ps.broj_tender = :broj_tender and ps.odbiena = 1 and ps.ti' +
        'p_partner like :tip_partner and ps.partner like :partner'
      
        '     and ps.odbiena_obrazlozenie is not null  and ps.odbiena_obr' +
        'azlozenie <> '#39#39
      'order by ps.tip_partner, ps.partner, ma.grupa, ps.artikal')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 584
    object ObrazlozenieNeprifateniPonudiStavkiTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object ObrazlozenieNeprifateniPonudiStavkiPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object ObrazlozenieNeprifateniPonudiStavkiARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object ObrazlozenieNeprifateniPonudiStavkiODBIENA_OBRAZLOZENIE: TFIBStringField
      FieldName = 'ODBIENA_OBRAZLOZENIE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object ObrazlozenieNeprifateniPonudiStavkiGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object ObrazlozenieNeprifateniPonudiStavkiPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object ObrazlozenieNeprifateniPonudiStavkiPARTNER_TIP_SIFRA: TFIBStringField
      FieldName = 'PARTNER_TIP_SIFRA'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxObrazlozenieNeprifateniPonudiStavki: TfrxDBDataset
    UserName = #1056#1072#1079#1075#1083#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1090#1077#1093#1085#1080#1095#1082#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'ARTIKAL=ARTIKAL'
      'ODBIENA_OBRAZLOZENIE=ODBIENA_OBRAZLOZENIE'
      'GRUPA=GRUPA'
      'PARTNERNAZIV=PARTNERNAZIV'
      'PARTNER_TIP_SIFRA=PARTNER_TIP_SIFRA')
    DataSet = ObrazlozenieNeprifateniPonudiStavki
    BCDToCurrency = False
    Left = 272
    Top = 584
  end
  object FinansiskaPonuda_1: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_jn_ponudacena_podgrupi.partner,'
      '       proc_jn_ponudacena_podgrupi.tip_partner,'
      '       proc_jn_ponudacena_podgrupi.partner_naziv,'
      '       proc_jn_ponudacena_podgrupi.podgrupi,'
      '       proc_jn_ponudacena_podgrupi.ponudena_cena'
      'from proc_jn_ponudacena_podgrupi (:broj_tender)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 648
    object FinansiskaPonuda_1PARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object FinansiskaPonuda_1TIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object FinansiskaPonuda_1PARTNER_NAZIV: TFIBStringField
      FieldName = 'PARTNER_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object FinansiskaPonuda_1PODGRUPI: TFIBStringField
      FieldName = 'PODGRUPI'
      Size = 3000
      Transliterate = False
      EmptyStrToNull = True
    end
    object FinansiskaPonuda_1PONUDENA_CENA: TFIBBCDField
      FieldName = 'PONUDENA_CENA'
      Size = 8
    end
  end
  object frxFinansiskaPonuda_1: TfrxDBDataset
    UserName = 'FinansiskaPonuda_1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PARTNER=PARTNER'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER_NAZIV=PARTNER_NAZIV'
      'PODGRUPI=PODGRUPI'
      'PONUDENA_CENA=PONUDENA_CENA')
    DataSet = FinansiskaPonuda_1
    BCDToCurrency = False
    Left = 264
    Top = 648
  end
  object PokanetiPonuduvaci: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(coalesce(ps.cena_prva_bezddv,0)) as pocetna_cena,'
      '       ps.ponuda_id,'
      '       ps.tip_partner,'
      '       ps.partner,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       mp.naziv as partnerNaziv'
      'from jn_ponudi_stavki ps'
      'inner join jn_ponudi p on p.id = ps.ponuda_id and p.odbiena = 0'
      
        'left outer join mtr_artikal ma on ma.artvid = ps.vid_artikal and' +
        ' ma.id = ps.artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      
        'inner join mat_partner mp on mp.tip_partner = ps.tip_partner and' +
        ' mp.id = ps.partner'
      'where ps.broj_tender = :broj_tender and ps.odbiena = 0'
      
        'group by  ps.ponuda_id, ps.tip_partner, ps.partner, ma.grupa, mg' +
        '.naziv , mp.naziv'
      'order by  grupaNaziv, pocetna_cena, partnerNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 712
    object PokanetiPonuduvaciPOCETNA_CENA: TFIBFloatField
      FieldName = 'POCETNA_CENA'
    end
    object PokanetiPonuduvaciPONUDA_ID: TFIBIntegerField
      FieldName = 'PONUDA_ID'
    end
    object PokanetiPonuduvaciTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object PokanetiPonuduvaciPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object PokanetiPonuduvaciGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanetiPonuduvaciGRUPANAZIV: TFIBStringField
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object PokanetiPonuduvaciPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxPokanetiPonuduvaci: TfrxDBDataset
    UserName = #1055#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1087#1086#1082#1072#1085#1077#1090#1080' '#1085#1072' '#1077#1040#1091#1082#1094#1080#1112#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'POCETNA_CENA=POCETNA_CENA'
      'PONUDA_ID=PONUDA_ID'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'GRUPA=GRUPA'
      'GRUPANAZIV=GRUPANAZIV'
      'PARTNERNAZIV=PARTNERNAZIV')
    DataSet = PokanetiPonuduvaci
    BCDToCurrency = False
    Left = 264
    Top = 712
  end
  object ObrazlozenieNeprifateniPonudi: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.tip_partner, p.partner, mp.naziv as  partnerNaziv, p.za' +
        'beleska'
      'from jn_ponudi p'
      
        'inner join mat_partner mp on mp.id = p.partner and mp.tip_partne' +
        'r = p.tip_partner'
      'where p.odbiena = 1 and p.broj_tender = :broj_tender')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 776
    object ObrazlozenieNeprifateniPonudiTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object ObrazlozenieNeprifateniPonudiPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object ObrazlozenieNeprifateniPonudiPARTNERNAZIV: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object ObrazlozenieNeprifateniPonudiZABELESKA: TFIBMemoField
      FieldName = 'ZABELESKA'
      BlobType = ftMemo
      Size = 8
    end
  end
  object frxObrazlozenieNeprifateniPonudi: TfrxDBDataset
    UserName = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1085#1077#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'PARTNERNAZIV=PARTNERNAZIV'
      'ZABELESKA=ZABELESKA')
    DataSet = ObrazlozenieNeprifateniPonudi
    BCDToCurrency = False
    Left = 264
    Top = 776
  end
  object OdlukaJavnaNabavka: TpFIBDataSet
    SelectSQL.Strings = (
      'select  cast(t.iznos as numeric (15,2))as procenetaVrednost,'
      '        ((t.iznos* 18/100) + t.iznos)as procenetaVrednostSoDDV,'
      
        '        (select proc_jn_template_1.str_kriterium from proc_jn_te' +
        'mplate_1(:mas_broj)) as bodiranje,'
      
        '        (select proc_jn_template_1.str_komisija from proc_jn_tem' +
        'plate_1(:mas_broj)) as komisija,'
      
        '        (select proc_jn_template_1.str_komisija_site from proc_j' +
        'n_template_1(:mas_broj)) as komisija_site,'
      
        '        (select proc_jn_template_1.str_komisija_potpis from proc' +
        '_jn_template_1(:mas_broj)) as komisija_potpis,'
      
        '        (select proc_jn_template_1.linija_potpis from proc_jn_te' +
        'mplate_1(:mas_broj)) as linija_potpis,'
      '        ti.izvor_sredstva,'
      '        tn.naziv as postapka,'
      '        t.obrazlozenie_postapka,'
      '        t.lice_podgotovka,'
      '        t.lice_realizacija,'
      '        ti.odl_nabavka_br, '
      '        ti.odl_nabavka_dat'
      'from jn_tender t'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      'where t.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 840
    object OdlukaJavnaNabavkaPROCENETAVREDNOST: TFIBBCDField
      FieldName = 'PROCENETAVREDNOST'
      DisplayFormat = '0.00, .'
      EditFormat = '0.00, .'
      Size = 2
    end
    object OdlukaJavnaNabavkaPROCENETAVREDNOSTSODDV: TFIBBCDField
      DefaultExpression = '0.00, .'
      FieldName = 'PROCENETAVREDNOSTSODDV'
      DisplayFormat = '0.00, .'
      EditFormat = '0.00, .'
      Size = 2
    end
    object OdlukaJavnaNabavkaBODIRANJE: TFIBStringField
      FieldName = 'BODIRANJE'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaKOMISIJA: TFIBStringField
      FieldName = 'KOMISIJA'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaIZVOR_SREDSTVA: TFIBStringField
      FieldName = 'IZVOR_SREDSTVA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaPOSTAPKA: TFIBStringField
      FieldName = 'POSTAPKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaOBRAZLOZENIE_POSTAPKA: TFIBMemoField
      FieldName = 'OBRAZLOZENIE_POSTAPKA'
      BlobType = ftMemo
      Size = 8
    end
    object OdlukaJavnaNabavkaLICE_PODGOTOVKA: TFIBStringField
      FieldName = 'LICE_PODGOTOVKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaLICE_REALIZACIJA: TFIBStringField
      FieldName = 'LICE_REALIZACIJA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaODL_NABAVKA_BR: TFIBStringField
      FieldName = 'ODL_NABAVKA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaODL_NABAVKA_DAT: TFIBDateField
      FieldName = 'ODL_NABAVKA_DAT'
    end
    object OdlukaJavnaNabavkaKOMISIJA_POTPIS: TFIBStringField
      FieldName = 'KOMISIJA_POTPIS'
      Size = 3000
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaLINIJA_POTPIS: TFIBStringField
      FieldName = 'LINIJA_POTPIS'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object OdlukaJavnaNabavkaKOMISIJA_SITE: TFIBStringField
      FieldName = 'KOMISIJA_SITE'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBOdlukaJavnaNabavka: TfrxDBDataset
    UserName = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
    CloseDataSource = False
    FieldAliases.Strings = (
      'PROCENETAVREDNOST=PROCENETAVREDNOST'
      'PROCENETAVREDNOSTSODDV=PROCENETAVREDNOSTSODDV'
      'BODIRANJE=BODIRANJE'
      'KOMISIJA=KOMISIJA'
      'IZVOR_SREDSTVA=IZVOR_SREDSTVA'
      'POSTAPKA=POSTAPKA'
      'OBRAZLOZENIE_POSTAPKA=OBRAZLOZENIE_POSTAPKA'
      'LICE_PODGOTOVKA=LICE_PODGOTOVKA'
      'LICE_REALIZACIJA=LICE_REALIZACIJA'
      'ODL_NABAVKA_BR=ODL_NABAVKA_BR'
      'ODL_NABAVKA_DAT=ODL_NABAVKA_DAT'
      'KOMISIJA_POTPIS=KOMISIJA_POTPIS'
      'LINIJA_POTPIS=LINIJA_POTPIS'
      'KOMISIJA_SITE=KOMISIJA_SITE')
    DataSet = OdlukaJavnaNabavka
    BCDToCurrency = False
    Left = 264
    Top = 840
  end
  object qSetupUpravnik: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1,'
      '       s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'JN'#39' and s.p2 = '#39'upravnik_zvanje_naziv'#39)
    Left = 448
    Top = 24
  end
  object PodgrupiVoJN: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct mg.naziv,'
      '                t.predmet_nabavka,'
      '                ma.grupa'
      'from jn_tender_stavki ts'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'inner join mtr_artikal ma on ma.artvid = ts.vid_artikal and ma.i' +
        'd = ts.artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'where mg.naziv <>'#39#39' and ts.broj_tender = :mas_broj'
      'order by ma.grupa')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 72
    Top = 904
    object PodgrupiVoJNNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object PodgrupiVoJNPREDMET_NABAVKA: TFIBStringField
      FieldName = 'PREDMET_NABAVKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object PodgrupiVoJNGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBPodgrupiVoJN: TfrxDBDataset
    UserName = #1055#1086#1076#1075#1088#1091#1087#1080' '#1074#1086' '#1032#1053
    CloseDataSource = False
    FieldAliases.Strings = (
      'NAZIV=NAZIV'
      'PREDMET_NABAVKA=PREDMET_NABAVKA'
      'GRUPA=GRUPA')
    DataSet = PodgrupiVoJN
    BCDToCurrency = False
    Left = 264
    Top = 904
  end
  object KriteriumiBodiranjeZaJN: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '       k.naziv as kriteriumNaziv,'
      '       tgb.max_bodovi'
      ''
      'from  jn_tender_gen_bodiranje tgb'
      'inner join jn_tender t on t.broj = tgb.broj_tender'
      'inner join jn_kriteriumi k on k.sifra = tgb.bodiranje_po'
      'where tgb.broj_tender = :mas_broj'
      'order by kriteriumNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 72
    Top = 968
    object KriteriumiBodiranjeZaJNKRITERIUMNAZIV: TFIBStringField
      FieldName = 'KRITERIUMNAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumiBodiranjeZaJNMAX_BODOVI: TFIBBCDField
      FieldName = 'MAX_BODOVI'
      Size = 2
    end
  end
  object frxDBKriteriumiBodiranjeZaJN: TfrxDBDataset
    UserName = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
    CloseDataSource = False
    FieldAliases.Strings = (
      'KRITERIUMNAZIV=KRITERIUMNAZIV'
      'MAX_BODOVI=MAX_BODOVI')
    DataSet = KriteriumiBodiranjeZaJN
    BCDToCurrency = False
    Left = 256
    Top = 968
  end
  object KriteriumSposobnostiZaJN: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '       tpd.potrebni_dokumenti_id,'
      '       pd.naziv,'
      '       pd.tip,'
      '       case when pd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when pd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073 +
        #1072#39
      
        '            when pd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086 +
        #1089#1086#1073#1085#1086#1089#1090#39
      
        '            when pd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090 +
        #1077#1090#39
      
        '            when pd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074 +
        #1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when pd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089 +
        #1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39
      '       end "tipNaziv",'
      '       pd.grupa,'
      '       g.naziv as grupaNaziv'
      'from jn_tender_potrebni_dok tpd'
      
        'inner join jn_potrebni_dokumenti pd on tpd.potrebni_dokumenti_id' +
        '= pd.id'
      'left outer join jn_potrebni_dok_grupa g on g.id = pd.grupa'
      'where tpd.broj_tender = :mas_broj'
      'order by pd.tip, pd.grupa, pd.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 72
    Top = 1032
    object KriteriumSposobnostiZaJNPOTREBNI_DOKUMENTI_ID: TFIBIntegerField
      FieldName = 'POTREBNI_DOKUMENTI_ID'
    end
    object KriteriumSposobnostiZaJNNAZIV: TFIBMemoField
      FieldName = 'NAZIV'
      BlobType = ftMemo
      Size = 8
    end
    object KriteriumSposobnostiZaJNTIP: TFIBSmallIntField
      FieldName = 'TIP'
    end
    object KriteriumSposobnostiZaJNtipNaziv: TFIBStringField
      FieldName = 'tipNaziv'
      Size = 45
      Transliterate = False
      EmptyStrToNull = True
    end
    object KriteriumSposobnostiZaJNGRUPA: TFIBIntegerField
      FieldName = 'GRUPA'
    end
    object KriteriumSposobnostiZaJNGRUPANAZIV: TFIBStringField
      FieldName = 'GRUPANAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBKriteriumSposobnostiZaJN: TfrxDBDataset
    UserName = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1032#1053
    CloseDataSource = False
    FieldAliases.Strings = (
      'POTREBNI_DOKUMENTI_ID=POTREBNI_DOKUMENTI_ID'
      'NAZIV=NAZIV'
      'TIP=TIP'
      'tipNaziv=tipNaziv'
      'GRUPA=GRUPA'
      'GRUPANAZIV=GRUPANAZIV')
    DataSet = KriteriumSposobnostiZaJN
    BCDToCurrency = False
    Left = 256
    Top = 1032
  end
  object qSetupKategorijaDogOrgan: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1'
      'from sys_setup s'
      'where s.p1 = '#39'JN'#39' and s.p2 = '#39'KATEGORIJA_DOGOVOREN_ORGAN'#39)
    Left = 568
    Top = 24
  end
  object ZapisnikOtvoranjePonudi: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_jn_zapisnik_ponudi.ponuduvac,'
      '       proc_jn_zapisnik_ponudi.kompletnost,'
      '       proc_jn_zapisnik_ponudi.garancii_popust,'
      '       proc_jn_zapisnik_ponudi.primena_vo_rok,'
      '       proc_jn_zapisnik_ponudi.zatvoren_kovert,'
      '       proc_jn_zapisnik_ponudi.oznaka_ne_otvoraj,'
      '       proc_jn_zapisnik_ponudi.povlekuvanje_ponuda,'
      '       proc_jn_zapisnik_ponudi.izmena_ponuda,'
      '       proc_jn_zapisnik_ponudi.rok_vaznost,'
      '       proc_jn_zapisnik_ponudi.valuta,'
      '       proc_jn_zapisnik_ponudi.garancija_iznos,'
      '       proc_jn_zapisnik_ponudi.vkupnacenabezddv,'
      '       proc_jn_zapisnik_ponudi.ponuduvac_red_br,'
      '       proc_jn_zapisnik_ponudi.pretstavnik,'
      '       proc_jn_zapisnik_ponudi.naziv_ponuduvac,'
      '       proc_jn_zapisnik_ponudi.prazno,'
      '       proc_jn_zapisnik_ponudi.garancija_datum'
      'from proc_jn_zapisnik_ponudi(:mas_broj)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 64
    Top = 1096
    object ZapisnikOtvoranjePonudiPONUDUVAC: TFIBStringField
      FieldName = 'PONUDUVAC'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiKOMPLETNOST: TFIBStringField
      FieldName = 'KOMPLETNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiGARANCII_POPUST: TFIBStringField
      FieldName = 'GARANCII_POPUST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiPRIMENA_VO_ROK: TFIBStringField
      FieldName = 'PRIMENA_VO_ROK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiZATVOREN_KOVERT: TFIBStringField
      FieldName = 'ZATVOREN_KOVERT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiOZNAKA_NE_OTVORAJ: TFIBStringField
      FieldName = 'OZNAKA_NE_OTVORAJ'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiPOVLEKUVANJE_PONUDA: TFIBStringField
      FieldName = 'POVLEKUVANJE_PONUDA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiIZMENA_PONUDA: TFIBStringField
      FieldName = 'IZMENA_PONUDA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiROK_VAZNOST: TFIBStringField
      FieldName = 'ROK_VAZNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiPONUDUVAC_RED_BR: TFIBStringField
      FieldName = 'PONUDUVAC_RED_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiPRAZNO: TFIBStringField
      FieldName = 'PRAZNO'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiGARANCIJA_IZNOS: TFIBBCDField
      FieldName = 'GARANCIJA_IZNOS'
      Size = 2
    end
    object ZapisnikOtvoranjePonudiVKUPNACENABEZDDV: TFIBBCDField
      FieldName = 'VKUPNACENABEZDDV'
      Size = 2
    end
    object ZapisnikOtvoranjePonudiPRETSTAVNIK: TFIBStringField
      FieldName = 'PRETSTAVNIK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiNAZIV_PONUDUVAC: TFIBStringField
      FieldName = 'NAZIV_PONUDUVAC'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object ZapisnikOtvoranjePonudiGARANCIJA_DATUM: TFIBDateField
      FieldName = 'GARANCIJA_DATUM'
    end
  end
  object frxDBZapisnikOtvoranjePonudi: TfrxDBDataset
    UserName = #1047#1072#1087#1080#1089#1085#1080#1082' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080
    CloseDataSource = False
    FieldAliases.Strings = (
      'PONUDUVAC='#1053#1072#1079#1080#1074' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
      
        'KOMPLETNOST='#1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1076#1086#1089#1090#1072#1074#1077#1085#1072' '#1074#1086' '#1086#1088#1080#1075#1080#1085#1072#1083' '#1080' '#1086#1085#1086#1083#1082#1091' '#1082#1086#1087#1080#1080' '#1082#1086#1083#1082 +
        #1091' '#1096#1090#1086' '#1089#1077' '#1073#1072#1088#1072#1083#1077' '#1074#1086' '#1090#1077#1085#1076#1077#1088#1089#1082#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      
        'GARANCII_POPUST='#1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072'. '#1043#1072#1088#1072#1085#1094#1080#1112#1072#1090#1072' '#1116#1077' '#1074#1072#1078#1080' 14 '#1076#1077 +
        #1085#1072' '#1087#1086' '#1087#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1083#1080#1076#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
      'PRIMENA_VO_ROK='#1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1087#1088#1080#1084#1077#1085#1072' '#1074#1086' '#1088#1086#1082
      'ZATVOREN_KOVERT='#1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1077' '#1079#1072#1090#1074#1086#1088#1077#1085
      'OZNAKA_NE_OTVORAJ='#1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1080#1084#1072' '#1086#1079#1085#1072#1082#1072' '#8222#1085#1077' '#1086#1090#1074#1086#1088#1072#1112#8220
      'POVLEKUVANJE_PONUDA='#1055#1086#1074#1083#1077#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      'IZMENA_PONUDA='#1048#1079#1084#1077#1085#1072' '#1080#1083#1080' '#1079#1072#1084#1077#1085#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      'ROK_VAZNOST='#1056#1086#1082' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
      'VALUTA='#1042#1072#1083#1091#1090#1072' '#1085#1072' '#1087#1085#1091#1076#1072#1090#1072
      'PONUDUVAC_RED_BR=PONUDUVAC_RED_BR'
      'PRAZNO='#1054#1087#1080#1089
      'GARANCIJA_IZNOS='#1048#1079#1085#1086#1089' '#1085#1072' '#1075#1072#1088#1072#1085#1094#1080#1112#1072
      'VKUPNACENABEZDDV='#1042#1082#1091#1087#1085#1072' '#1094#1077#1085#1072' ('#1073#1077#1079' '#1044#1044#1042')'
      'PRETSTAVNIK=PRETSTAVNIK'
      'NAZIV_PONUDUVAC=NAZIV_PONUDUVAC'
      'GARANCIJA_DATUM=GARANCIJA_DATUM')
    DataSet = ZapisnikOtvoranjePonudi
    BCDToCurrency = False
    Left = 256
    Top = 1096
  end
  object TemplateFRF: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.data'
      'from sys_report s'
      'where s.br = :br and s.tabela_grupa = :app')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 160
    Top = 72
    object TemplateFRFDATA: TFIBBlobField
      FieldName = 'DATA'
      Size = 8
    end
  end
  object dsTemplateFRF: TDataSource
    DataSet = TemplateFRF
    Left = 224
    Top = 72
  end
  object qUserImePrezime: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select u.ime || " "|| u.prezime as imeprezime'
      'from sys_user u where u.username = :username')
    Left = 392
    Top = 80
  end
  object frxDBDogovorZaJN: TfrxDBDataset
    UserName = #1044#1086#1075#1086#1074#1086#1088
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ_DOGOVOR='#1041#1088#1086#1112'_'#1044#1086#1075
      'DATUM='#1044#1072#1090#1091#1084'_'#1044#1086#1075
      'BROJ_TENDER='#1041#1088#1086#1112'_'#1032#1053
      'OPIS='#1054#1087#1080#1089'_'#1044#1086#1075
      'PARTNER='#1055#1086#1085#1091#1076#1091#1074#1072#1095
      'PARTNERNAZIV='#1053#1086#1089#1080#1090#1077#1083'_'#1044#1086#1075
      'DATUM_VAZENJE='#1044#1072#1090#1091#1084#1042#1072#1078#1077#1114#1077'_'#1044#1086#1075
      'PERIOD='#1055#1077#1088#1080#1086#1076#1042#1072#1078#1077#1114#1077'_'#1044#1086#1075
      'ROK_ISPORAKA='#1056#1086#1082#1048#1089#1087#1086#1088#1072#1082#1072
      'ROK_FAKTURA='#1056#1086#1082#1060#1072#1082#1090#1091#1088#1072
      'GARANTEN_ROK='#1043#1072#1088#1072#1085#1090#1077#1085#1056#1086#1082
      'DIREKTOR='#1044#1080#1088#1077#1082#1090#1086#1088
      'JN_PREDMET_NABAVKA='#1055#1088#1077#1076#1084#1077#1090#1053#1072#1073#1072#1074#1082#1072
      'JN_DETALENOPIS_PREDMET='#1044#1077#1090#1072#1083#1077#1085#1054#1087#1080#1089#1055#1088#1077#1076#1084#1077#1090
      'JN_OBRAZLOZENIE_POSTAPKA='#1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077#1055#1086#1089#1090#1072#1087#1082#1072
      'JN_VREMETRAENJE_DOG='#1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077#1044#1086#1075
      'JN_LOKACIJA_ISPORAKA='#1051#1086#1082#1072#1094#1080#1112#1072#1048#1089#1087#1086#1088#1072#1082#1072
      'JN_LICE_REALIZACIJA='#1051#1080#1094#1077#1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      'JN_GARANCIJA_PONUDA='#1043#1072#1088#1072#1085#1094#1080#1112#1072#1055#1086#1085#1091#1076#1072
      'JN_GARANCIJA_KVALITET='#1043#1072#1088#1072#1085#1094#1080#1112#1072#1050#1074#1072#1083#1080#1090#1077#1090
      'P_GARANCIJA_IZNOS='#1043#1072#1088#1072#1085#1094#1080#1112#1072#1048#1079#1085#1086#1089
      'IZNOS='#1048#1079#1085#1086#1089
      'IZNOSBEZDDV='#1048#1079#1085#1086#1089#1041#1077#1079#1044#1044#1042
      'DDV='#1048#1079#1085#1086#1089'DDV')
    DataSet = DogovorZaJN
    BCDToCurrency = False
    Left = 256
    Top = 1160
  end
  object DogovorZaJN: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.broj_dogovor,'
      '       d.datum,'
      '       d.broj_tender,'
      '       d.opis,'
      '       d.partner,'
      '       mp.naziv as partnerNaziv,'
      '       d.datum_vazenje,'
      '       d.period,'
      '       d.rok_isporaka,'
      '       d.rok_faktura,'
      '       d.garanten_rok,'
      '       d.direktor,'
      '       t.predmet_nabavka jn_predmet_nabavka,'
      '       t.opis as jn_detalenopis_predmet,'
      '       t.obrazlozenie_postapka jn_obrazlozenie_postapka,'
      '       ti.vremetraenje_dog jn_vremetraenje_dog,'
      '       ti.lokacija_isporaka jn_lokacija_isporaka,'
      '       t.lice_realizacija jn_lice_realizacija,'
      '       ti.garancija_ponuda jn_garancija_ponuda,'
      '       ti.garancija_kvalitet jn_garancija_kvalitet,'
      '       p.garancija_iznos p_garancija_iznos,'
      '       /*$$IBEC$$ (select sum(ps.cena)'
      '        from jn_tender_dobitnici ds'
      
        '        inner join jn_ponudi_stavki ps on ds.id_ponudi_stavki = ' +
        'ps.id and ds.broj_tender = ps.broj_tender'
      
        '                                          and ds.partner = ps.pa' +
        'rtner and ds.tip_partner = ps.tip_partner'
      
        '        where ds.broj_tender = d.broj_tender and ds.partner = d.' +
        'partner and ds.tip_partner = d.tip_partner), $$IBEC$$*/'
      '       sum(psb.cena*ds.KOLICINA) as iznos,'
      '       sum(psb.cenabezddv*ds.KOLICINA) as iznosbezddv,'
      
        '       (sum(psb.cena*ds.KOLICINA) - sum(psb.cenabezddv*ds.KOLICI' +
        'NA))ddv'
      ''
      'from jn_dogovor d'
      
        'inner join jn_dogovor_stavki ds on d.broj_dogovor = ds.broj_dogo' +
        'vor'
      
        'left outer join mat_partner mp on mp.tip_partner = d.tip_partner' +
        ' and mp.id = d.partner'
      'left outer join jn_tender t on t.broj = d.broj_tender'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      
        'left outer join jn_ponudi p on p.tip_partner = d.tip_partner and' +
        ' p.partner = d.partner and p.broj_tender = d.broj_tender'
      
        'left outer join jn_ponudi_stavki psb on (psb.artikal=ds.artikal ' +
        'and psb.vid_artikal=ds.vid_artikal and'
      
        '                                         psb.broj_tender=ds.broj' +
        '_tender and'
      
        '                                         psb.tip_partner=d.tip_p' +
        'artner and psb.partner=d.partner )'
      'where d.broj_dogovor = :broj_dogovor'
      
        'group by 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13, 14,15, 16, 17, 1' +
        '8, 19, 20, 21'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 72
    Top = 1160
  end
  object PocetnaRangListaNedeliviGrupi: TpFIBDataSet
    SelectSQL.Strings = (
      'select PROC_JN_N_RANGLISTA_AUKCIJA.o_tip_partner,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_partner,'
      '       p.naziv as  o_partner_naziv,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_iznos_bezddv,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_bodovi_kvalitet,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_vkupno_bodovi,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_broj_ponuda,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_ponuda_datum,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.o_grupa,'
      '       g.naziv as o_grupa_naziv,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.prosecna_cena,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.pomala_od_prosecna,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.prosecna_pola_cena,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.prethodna_cena,'
      '       PROC_JN_N_RANGLISTA_AUKCIJA.odnos_procent'
      'from PROC_JN_N_RANGLISTA_AUKCIJA(:tender,2)'
      
        'inner join mat_partner p on p.tip_partner = PROC_JN_N_RANGLISTA_' +
        'AUKCIJA.o_tip_partner and p.id = PROC_JN_N_RANGLISTA_AUKCIJA.o_p' +
        'artner'
      
        'inner join mtr_artgrupa g on g.id =PROC_JN_N_RANGLISTA_AUKCIJA.o' +
        '_grupa'
      'order by 9, 6 desc'
      ''
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 64
    Top = 1232
    object tblPocetnaRangListaNedeliviGrupiO_TIP_PARTNER: TFIBIntegerField
      FieldName = 'O_TIP_PARTNER'
    end
    object tblPocetnaRangListaNedeliviGrupiO_PARTNER: TFIBIntegerField
      FieldName = 'O_PARTNER'
    end
    object tblPocetnaRangListaNedeliviGrupiO_PARTNER_NAZIV: TFIBStringField
      FieldName = 'O_PARTNER_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiO_IZNOS_BEZDDV: TFIBFloatField
      FieldName = 'O_IZNOS_BEZDDV'
    end
    object tblPocetnaRangListaNedeliviGrupiO_BODOVI_KVALITET: TFIBBCDField
      FieldName = 'O_BODOVI_KVALITET'
    end
    object tblPocetnaRangListaNedeliviGrupiO_VKUPNO_BODOVI: TFIBBCDField
      FieldName = 'O_VKUPNO_BODOVI'
    end
    object tblPocetnaRangListaNedeliviGrupiO_BROJ_PONUDA: TFIBStringField
      FieldName = 'O_BROJ_PONUDA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaRangListaNedeliviGrupiO_PONUDA_DATUM: TFIBDateField
      FieldName = 'O_PONUDA_DATUM'
    end
    object tblPocetnaRangListaNedeliviGrupiO_GRUPA: TFIBStringField
      FieldName = 'O_GRUPA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaRangListaNedeliviGrupiO_GRUPA_NAZIV: TFIBStringField
      FieldName = 'O_GRUPA_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiPROSECNA_CENA: TFIBFloatField
      FieldName = 'PROSECNA_CENA'
    end
    object tblPocetnaRangListaNedeliviGrupiPOMALA_OD_PROSECNA: TFIBStringField
      FieldName = 'POMALA_OD_PROSECNA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiPRETHODNA_CENA: TFIBFloatField
      FieldName = 'PRETHODNA_CENA'
    end
    object PocetnaRangListaNedeliviGrupiODNOS_PROCENT: TFIBFloatField
      FieldName = 'ODNOS_PROCENT'
    end
    object PocetnaRangListaNedeliviGrupiPROSECNA_POLA_CENA: TFIBFloatField
      FieldName = 'PROSECNA_POLA_CENA'
    end
  end
  object frxDBDPocetnaRangListaNedeliviGrupi: TfrxDBDataset
    UserName = 'PocetnaRangListaNedeliviGrupi'
    CloseDataSource = False
    FieldAliases.Strings = (
      'O_TIP_PARTNER=O_TIP_PARTNER'
      'O_PARTNER=O_PARTNER'
      'O_PARTNER_NAZIV=O_PARTNER_NAZIV'
      'O_IZNOS_BEZDDV=O_IZNOS_BEZDDV'
      'O_BODOVI_KVALITET=O_BODOVI_KVALITET'
      'O_VKUPNO_BODOVI=O_VKUPNO_BODOVI'
      'O_BROJ_PONUDA=O_BROJ_PONUDA'
      'O_PONUDA_DATUM=O_PONUDA_DATUM'
      'O_GRUPA=O_GRUPA'
      'O_GRUPA_NAZIV=O_GRUPA_NAZIV'
      'PROSECNA_CENA=PROSECNA_CENA'
      'POMALA_OD_PROSECNA=POMALA_OD_PROSECNA'
      'PRETHODNA_CENA=PRETHODNA_CENA'
      'ODNOS_PROCENT=ODNOS_PROCENT'
      'PROSECNA_POLA_CENA=PROSECNA_POLA_CENA')
    DataSet = PocetnaRangListaNedeliviGrupi
    BCDToCurrency = False
    Left = 256
    Top = 1232
  end
  object tblDobitnici: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_jn_dobitnici.tip_partner,'
      '       proc_jn_dobitnici.partner,'
      '       proc_jn_dobitnici.partner_naziv,'
      ''
      '       proc_jn_dobitnici.vid_artikal,'
      '       proc_jn_dobitnici.artikal,'
      '       proc_jn_dobitnici.grupa,'
      '       proc_jn_dobitnici.artikal_spojuvanje'
      'from proc_jn_dobitnici(:broj_tender)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 56
    Top = 1312
    object tblDobitniciTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblDobitniciPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblDobitniciVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblDobitniciARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblDobitniciGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciARTIKAL_SPOJUVANJE: TFIBStringField
      FieldName = 'ARTIKAL_SPOJUVANJE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciPARTNER_NAZIV: TFIBStringField
      FieldName = 'PARTNER_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBDobitnici: TfrxDBDataset
    UserName = 'Dobitnici'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'VID_ARTIKAL=VID_ARTIKAL'
      'ARTIKAL=ARTIKAL'
      'GRUPA=GRUPA'
      'ARTIKAL_SPOJUVANJE=ARTIKAL_SPOJUVANJE'
      'PARTNER_NAZIV=PARTNER_NAZIV')
    DataSet = tblDobitnici
    BCDToCurrency = False
    Left = 240
    Top = 1307
  end
  object tblPocetnaRangLista: TpFIBDataSet
    SelectSQL.Strings = (
      'select PROC_JN_RANGLISTA_AUKCIJA.o_tip_partner,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_partner,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_partner_naziv,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_vid_artikal,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_artikal,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_naziv_artikal,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_iznos_bezddv,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_bodovi_kvalitet,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_vkupno_bodovi,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_broj_ponuda,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_ponuda_datum,'
      '       PROC_JN_RANGLISTA_AUKCIJA.o_grupa'
      'from PROC_JN_RANGLISTA_AUKCIJA(:tender,1)'
      'order by 12,4, 5,9 desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 56
    Top = 1376
    object tblPocetnaRangListaO_TIP_PARTNER: TFIBIntegerField
      FieldName = 'O_TIP_PARTNER'
    end
    object tblPocetnaRangListaO_PARTNER: TFIBIntegerField
      FieldName = 'O_PARTNER'
    end
    object tblPocetnaRangListaO_PARTNER_NAZIV: TFIBStringField
      FieldName = 'O_PARTNER_NAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaRangListaO_VID_ARTIKAL: TFIBIntegerField
      FieldName = 'O_VID_ARTIKAL'
    end
    object tblPocetnaRangListaO_ARTIKAL: TFIBIntegerField
      FieldName = 'O_ARTIKAL'
    end
    object tblPocetnaRangListaO_NAZIV_ARTIKAL: TFIBStringField
      FieldName = 'O_NAZIV_ARTIKAL'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaRangListaO_IZNOS_BEZDDV: TFIBFloatField
      FieldName = 'O_IZNOS_BEZDDV'
    end
    object tblPocetnaRangListaO_BODOVI_KVALITET: TFIBBCDField
      FieldName = 'O_BODOVI_KVALITET'
      Size = 8
    end
    object tblPocetnaRangListaO_VKUPNO_BODOVI: TFIBBCDField
      FieldName = 'O_VKUPNO_BODOVI'
      Size = 8
    end
    object tblPocetnaRangListaO_BROJ_PONUDA: TFIBStringField
      FieldName = 'O_BROJ_PONUDA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaRangListaO_PONUDA_DATUM: TFIBDateField
      FieldName = 'O_PONUDA_DATUM'
    end
    object tblPocetnaRangListaO_GRUPA: TFIBStringField
      FieldName = 'O_GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBDtblPocetnaRangLista: TfrxDBDataset
    UserName = 'PocetnaRangLista'
    CloseDataSource = False
    FieldAliases.Strings = (
      'O_TIP_PARTNER=O_TIP_PARTNER'
      'O_PARTNER=O_PARTNER'
      'O_PARTNER_NAZIV=O_PARTNER_NAZIV'
      'O_VID_ARTIKAL=O_VID_ARTIKAL'
      'O_ARTIKAL=O_ARTIKAL'
      'O_NAZIV_ARTIKAL=O_NAZIV_ARTIKAL'
      'O_IZNOS_BEZDDV=O_IZNOS_BEZDDV'
      'O_BODOVI_KVALITET=O_BODOVI_KVALITET'
      'O_VKUPNO_BODOVI=O_VKUPNO_BODOVI'
      'O_BROJ_PONUDA=O_BROJ_PONUDA'
      'O_PONUDA_DATUM=O_PONUDA_DATUM'
      'O_GRUPA=O_GRUPA')
    DataSet = tblPocetnaRangLista
    BCDToCurrency = False
    Left = 240
    Top = 1376
  end
  object frxDBDPocetnaRangListaNedeliviGrupiKriterium: TfrxDBDataset
    UserName = 'PocetnaRangListaNedeliviGrupiKriterium'
    CloseDataSource = False
    FieldAliases.Strings = (
      'O_TIP_PARTNER=O_TIP_PARTNER'
      'O_PARTNER=O_PARTNER'
      'O_PARTNER_NAZIV=O_PARTNER_NAZIV'
      'O_IZNOS_BEZDDV=O_IZNOS_BEZDDV'
      'O_BODOVI_KVALITET=O_BODOVI_KVALITET'
      'O_VKUPNO_BODOVI=O_VKUPNO_BODOVI'
      'O_BROJ_PONUDA=O_BROJ_PONUDA'
      'O_PONUDA_DATUM=O_PONUDA_DATUM'
      'O_GRUPA=O_GRUPA'
      'O_GRUPA_NAZIV=O_GRUPA_NAZIV'
      'O_BODIRANJE_PO=O_BODIRANJE_PO'
      'O_KRITERIUM_NAZIV=O_KRITERIUM_NAZIV'
      'O_BOD_PO_KRITERIUM=O_BOD_PO_KRITERIUM'
      'O_GRUPA_STRING=O_GRUPA_STRING')
    DataSet = PocetnaRangListaNedeliviGrupiKriterium
    BCDToCurrency = False
    Left = 696
    Top = 1232
  end
  object PocetnaRangListaNedeliviGrupiKriterium: TpFIBDataSet
    SelectSQL.Strings = (
      'select PROC_JN_K_RANGLISTA_AUKCIJA.o_tip_partner,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_partner,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_partner_naziv,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_iznos_bezddv,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_bodovi_kvalitet,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_vkupno_bodovi,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_broj_ponuda,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_ponuda_datum,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_grupa,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_grupa_naziv,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_bodiranje_po,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_kriterium_naziv,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_bod_po_kriterium,'
      '       PROC_JN_K_RANGLISTA_AUKCIJA.o_grupa_string'
      'from PROC_JN_K_RANGLISTA_AUKCIJA(:tender,2)'
      'order by 9, 6 desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 456
    Top = 1232
  end
  object PocetnaRangListaNedeliviGrupiEDP: TpFIBDataSet
    SelectSQL.Strings = (
      'select PROC_JN_RANGLISTA_AUKCIJA_EDP.o_tip_partner,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_partner,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_partner_naziv,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_iznos_bezddv,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_bodovi_kvalitet,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_vkupno_bodovi,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_broj_ponuda,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_ponuda_datum,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_grupa,'
      '       PROC_JN_RANGLISTA_AUKCIJA_EDP.o_grupa_naziv'
      'from PROC_JN_RANGLISTA_AUKCIJA_EDP(:tender,2)'
      'order by 9, 6 desc')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 900
    Top = 1232
    object PocetnaRangListaNedeliviGrupiEDPO_TIP_PARTNER: TFIBIntegerField
      FieldName = 'O_TIP_PARTNER'
    end
    object PocetnaRangListaNedeliviGrupiEDPO_PARTNER: TFIBIntegerField
      FieldName = 'O_PARTNER'
    end
    object PocetnaRangListaNedeliviGrupiEDPO_PARTNER_NAZIV: TFIBStringField
      FieldName = 'O_PARTNER_NAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiEDPO_IZNOS_BEZDDV: TFIBFloatField
      FieldName = 'O_IZNOS_BEZDDV'
    end
    object PocetnaRangListaNedeliviGrupiEDPO_BODOVI_KVALITET: TFIBBCDField
      FieldName = 'O_BODOVI_KVALITET'
      Size = 8
    end
    object PocetnaRangListaNedeliviGrupiEDPO_VKUPNO_BODOVI: TFIBBCDField
      FieldName = 'O_VKUPNO_BODOVI'
      Size = 8
    end
    object PocetnaRangListaNedeliviGrupiEDPO_BROJ_PONUDA: TFIBStringField
      FieldName = 'O_BROJ_PONUDA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiEDPO_PONUDA_DATUM: TFIBDateField
      FieldName = 'O_PONUDA_DATUM'
    end
    object PocetnaRangListaNedeliviGrupiEDPO_GRUPA: TFIBStringField
      FieldName = 'O_GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object PocetnaRangListaNedeliviGrupiEDPO_GRUPA_NAZIV: TFIBStringField
      FieldName = 'O_GRUPA_NAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDBDPocetnaRangListaNedeliviGrupiEDP: TfrxDBDataset
    UserName = 'PocetnaRangListaNedeliviGrupiEDP'
    CloseDataSource = False
    FieldAliases.Strings = (
      'O_TIP_PARTNER=O_TIP_PARTNER'
      'O_PARTNER=O_PARTNER'
      'O_PARTNER_NAZIV=O_PARTNER_NAZIV'
      'O_IZNOS_BEZDDV=O_IZNOS_BEZDDV'
      'O_BODOVI_KVALITET=O_BODOVI_KVALITET'
      'O_VKUPNO_BODOVI=O_VKUPNO_BODOVI'
      'O_BROJ_PONUDA=O_BROJ_PONUDA'
      'O_PONUDA_DATUM=O_PONUDA_DATUM'
      'O_GRUPA=O_GRUPA'
      'O_GRUPA_NAZIV=O_GRUPA_NAZIV')
    DataSet = PocetnaRangListaNedeliviGrupiEDP
    BCDToCurrency = False
    Left = 1088
    Top = 1232
  end
  object frxDogovorStavki: TfrxDBDataset
    UserName = 'DogovorStavki'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ARTIKAL=ARTIKAL'
      'JN_CPV=JN_CPV'
      'CENABEZDDV=CENABEZDDV'
      'KOLICINA=KOLICINA'
      'DANOK=DANOK'
      'CENA=CENA'
      'IZNOS=IZNOS'
      'IZNOOSBEZDDV=IZNOOSBEZDDV'
      'DDV=DDV'
      'ARTIKALNAZIV=ARTIKALNAZIV'
      'MERKA=MERKA'
      'INTEREN_BROJ=INTEREN_BROJ'
      'GRUPA=GRUPA'
      'JN_TIP_ARTIKAL=JN_TIP_ARTIKAL'
      'GRUPA_NAZIV=GRUPA_NAZIV'
      'VID_NAZIV=VID_NAZIV'
      'DELIVA=DELIVA')
    DataSet = tblDogovorStavki
    BCDToCurrency = False
    Left = 240
    Top = 1440
  end
  object tblDogovorStavki: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '       d.artikal,'
      '       ma.jn_cpv,'
      '       ps.cenabezddv,'
      '       ps.kolicina,'
      '       ps.danok,'
      '       ps.cena,'
      '       ps.kolicina * ps.cena as iznos,'
      '       ps.kolicina * ps.cenabezddv as iznoosbezddv,'
      ''
      
        '       coalesce(((ps.kolicina * ps.cena) - (ps.kolicina * ps.cen' +
        'abezddv)),0) ddv,'
      '       jts.naziv as artikalNaziv,'
      '       jts.merka,'
      '       t.interen_broj,'
      '       ma.grupa,'
      '       ma.jn_tip_artikal,'
      '       g.naziv as grupa_naziv,'
      '       v.naziv as vid_naziv,'
      '       (case when t.deliv=1 and  ng.grupa is null  then '#39#39' else'
      
        '        case when t.deliv=1 and  ng.grupa is not  null then '#39'('#1051#1086 +
        #1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39
      '        else '#39'('#1051#1086#1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39' end end) deliva'
      'from jn_tender_dobitnici d'
      'inner join jn_tender t on t.broj = d.broj_tender'
      
        'inner join mtr_artikal ma on ma.id = d.artikal and ma.artvid = d' +
        '.vid_artikal'
      
        'inner join jn_ponudi_stavki ps on ps.id = d.id_ponudi_stavki and' +
        ' ps.broj_tender = d.broj_tender'
      
        '                                  and ps.partner = d.partner and' +
        ' ps.tip_partner = d.tip_partner'
      
        'join jn_tender_stavki jts on jts.vid_artikal=d.vid_artikal and j' +
        'ts.artikal=d.artikal    and  jts.broj_tender=d.broj_tender'
      ''
      'inner join jn_tip_artikal v on v.id=ma.jn_tip_artikal'
      'left outer join mtr_artgrupa g on g.id=ma.grupa'
      
        'left outer join jn_nedelivi_grupi ng on ng.grupa=g.id   and ng.b' +
        'roj_tender=t.broj'
      ''
      
        'where d.broj_tender = :broj_tender and d.partner = :partner and ' +
        'd.tip_partner = :tip_partner'
      'order by d.tip_partner, d.partner,ma.artvid, ma.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 56
    Top = 1440
    object tblDogovorStavkiARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblDogovorStavkiJN_CPV: TFIBStringField
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiCENABEZDDV: TFIBFloatField
      FieldName = 'CENABEZDDV'
    end
    object tblDogovorStavkiKOLICINA: TFIBBCDField
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblDogovorStavkiDANOK: TFIBFloatField
      FieldName = 'DANOK'
    end
    object tblDogovorStavkiCENA: TFIBFloatField
      FieldName = 'CENA'
    end
    object tblDogovorStavkiIZNOS: TFIBFloatField
      FieldName = 'IZNOS'
    end
    object tblDogovorStavkiIZNOOSBEZDDV: TFIBFloatField
      FieldName = 'IZNOOSBEZDDV'
    end
    object tblDogovorStavkiDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object tblDogovorStavkiARTIKALNAZIV: TFIBStringField
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiINTEREN_BROJ: TFIBStringField
      FieldName = 'INTEREN_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblDogovorStavkiGRUPA_NAZIV: TFIBStringField
      FieldName = 'GRUPA_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiVID_NAZIV: TFIBStringField
      FieldName = 'VID_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiDELIVA: TFIBStringField
      FieldName = 'DELIVA'
      Size = 18
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblSiteDobitnici: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct td.partner,'
      '       td.tip_partner,'
      '       td.tip_partner ||'#39'/'#39'||td.partner as partner_tip_sifra,'
      '       p.naziv dobavuvac,'
      '       p.adresa,'
      '       m.naziv mesto,'
      '       td.broj_tender,'
      '       t.tip_tender,'
      '       ttt.naziv,'
      '       ttt.denovi_zalba,'
      '       t.predmet_nabavka'
      'from jn_tender_dobitnici td'
      
        'inner join mat_partner p on (p.id=td.partner and p.tip_partner=t' +
        'd.tip_partner)'
      'inner join jn_tender t on (t.broj=td.broj_tender)'
      'left outer join mat_mesto m on (m.id =p.mesto)'
      'left join jn_tip_nabavka ttt on t.tip_tender=ttt.sifra'
      'where  td.broj_tender=:tender'
      'order by td.tip_partner,td.partner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 1512
  end
  object frxSiteDobitnici: TfrxDBDataset
    UserName = 'SiteDobitnici'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PARTNER=PARTNER'
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER_TIP_SIFRA=PARTNER_TIP_SIFRA'
      'DOBAVUVAC=DOBAVUVAC'
      'ADRESA=ADRESA'
      'MESTO=MESTO'
      'BROJ_TENDER=BROJ_TENDER'
      'TIP_TENDER=TIP_TENDER'
      'NAZIV=NAZIV'
      'DENOVI_ZALBA=DENOVI_ZALBA'
      'PREDMET_NABAVKA=PREDMET_NABAVKA')
    DataSet = tblSiteDobitnici
    BCDToCurrency = False
    Left = 272
    Top = 1512
  end
  object ObrazlozenieNeprifateniPonudiGrupno: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.tip_partner,'
      '       ps.partner,'
      '       ps.artikal,'
      '       ps.odbiena_obrazlozenie,'
      '       ma.grupa,'
      '       mp.naziv partnernaziv,'
      '       ps.TIP_PARTNER||'#39'/'#39'||ps.partner as partner_tip_sifra'
      'from jn_ponudi_stavki ps'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ps.vid_artik' +
        'al = ma.artvid'
      
        'inner join mat_partner mp on mp.tip_partner = ps.tip_partner and' +
        ' mp.id = ps.partner'
      
        'where ps.broj_tender = :broj_tender and ps.odbiena = 1 and ps.od' +
        'biena_obrazlozenie is not null  and ps.odbiena_obrazlozenie <> '#39 +
        #39
      'order by ps.tip_partner, ps.partner, ma.grupa, ps.artikal')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 1584
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object FIBIntegerField2: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object FIBIntegerField3: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'ODBIENA_OBRAZLOZENIE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField3: TFIBStringField
      FieldName = 'PARTNERNAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      FieldName = 'PARTNER_TIP_SIFRA'
      Size = 23
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxObrazlozenieNeprifateniPonudiStavkiGrupno: TfrxDBDataset
    UserName = #1056#1072#1079#1075#1083#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1090#1077#1093#1085#1080#1095#1082#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077' Grupno'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER'
      'PARTNER=PARTNER'
      'ARTIKAL=ARTIKAL'
      'ODBIENA_OBRAZLOZENIE=ODBIENA_OBRAZLOZENIE'
      'GRUPA=GRUPA'
      'PARTNERNAZIV=PARTNERNAZIV'
      'PARTNER_TIP_SIFRA=PARTNER_TIP_SIFRA')
    DataSet = ObrazlozenieNeprifateniPonudiGrupno
    BCDToCurrency = False
    Left = 272
    Top = 1584
  end
  object frxSiteDobitniciVoIzvestajNeobitni: TfrxDBDataset
    UserName = 'SiteDobitniciVoIzvestajNeobitni'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PARTNER=PARTNER'
      'TIP_PARTNER=TIP_PARTNER'
      'DOBAVUVAC=DOBAVUVAC'
      'DOBAVUVAC_MESTONAZIV=DOBAVUVAC_MESTONAZIV')
    DataSet = tblSiteDobitniciVoIzvestajNeobitni
    BCDToCurrency = False
    Left = 272
    Top = 1648
  end
  object tblSiteDobitniciVoIzvestajNeobitni: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct td.partner,td.tip_partner, p.naziv dobavuvac,'
      
        '       '#39'- '#39'||p.naziv ||'#39', '#39'||coalesce(m.naziv, '#39#39') dobavuvac_mes' +
        'tonaziv'
      'from jn_tender_dobitnici td'
      
        'inner join mat_partner p on (p.id=td.partner and p.tip_partner=t' +
        'd.tip_partner)'
      'left outer join mat_mesto m on m.id=p.mesto'
      'where td.broj_tender = :broj_tender')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 48
    Top = 1648
    object tblSiteDobitniciVoIzvestajNeobitniPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblSiteDobitniciVoIzvestajNeobitniTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblSiteDobitniciVoIzvestajNeobitniDOBAVUVAC: TFIBStringField
      FieldName = 'DOBAVUVAC'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSiteDobitniciVoIzvestajNeobitniDOBAVUVAC_MESTONAZIV: TFIBStringField
      FieldName = 'DOBAVUVAC_MESTONAZIV'
      Size = 359
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblEmptyTable: TpFIBDataSet
    SelectSQL.Strings = (
      'select first 1 0 as tip_partner'
      'from mat_partner p')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 56
    Top = 1720
    object tblEmptyTableTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
  end
  object frxEmptyTable: TfrxDBDataset
    UserName = 'EmptyTable'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIP_PARTNER=TIP_PARTNER')
    DataSet = tblEmptyTable
    BCDToCurrency = False
    Left = 272
    Top = 1720
  end
  object tblDogovorStavkiOdluka: TpFIBDataSet
    SelectSQL.Strings = (
      'select ts.broj_tender,'
      '       ts.artikal,'
      '       ts.kolicina,'
      '       ts.naziv artnaziv,'
      '       ts.merka,'
      '       a.jn_cpv,'
      '       t.godina,'
      ''
      '       ((select sum(ps.cenabezddv)'
      '         from jn_godisen_plan_sektori ps'
      
        '         inner join  jn_tender_stavki_re tsr on ps.artikal = tsr' +
        '.artikal and ps.vid_artikal = tsr.vid_artikal and tsr.re = ps.re' +
        ' and tsr.broj_tender =:tender'
      
        '         where ps.godina = t.godina and ps.artikal = ts.artikal ' +
        'and ps.vid_artikal = ts.vid_artikal)'
      ''
      '       ) as cena_bez_ddv,-- vo odlukata e as cena ,'
      ''
      '       ((select sum(ps.cena)'
      '         from jn_godisen_plan_sektori ps'
      
        '         inner join  jn_tender_stavki_re tsr on ps.artikal = tsr' +
        '.artikal and ps.vid_artikal = tsr.vid_artikal and tsr.re = ps.re' +
        ' and tsr.broj_tender =:tender'
      
        '         where ps.godina = t.godina and ps.artikal = ts.artikal ' +
        'and ps.vid_artikal = ts.vid_artikal)'
      ''
      '       ) as cena_so_ddv,'
      ''
      '       ((select first 1 ps.danok'
      '         from jn_godisen_plan_sektori ps'
      
        '         inner join  jn_tender_stavki_re tsr on ps.artikal = tsr' +
        '.artikal and ps.vid_artikal = tsr.vid_artikal and tsr.re = ps.re' +
        ' and tsr.broj_tender =:tender'
      
        '         where ps.godina = t.godina and ps.artikal = ts.artikal ' +
        'and ps.vid_artikal = ts.vid_artikal)'
      ''
      '       ) as ddv,'
      ''
      ''
      '       g.naziv as grupa_naziv,'
      '       v.naziv as vid_naziv,'
      '       (case when t.deliv=1 and  ng.grupa is null  then '#39#39' else'
      
        '        case when t.deliv=1 and  ng.grupa is not  null then '#39'('#1051#1086 +
        #1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39
      '        else '#39'('#1051#1086#1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39' end end) deliva,'
      '       a.grupa,'
      '       a.jn_tip_artikal,'
      '       t.iznos_so_ddv,'
      '       t.iznos as iznos_bez_ddv,'
      ''
      
        '      -- ((select sum(p.cenabezddv) from jn_godisen_plan_sektori' +
        ' p where p.godina = t.godina and p.artikal = ts.artikal and p.vi' +
        'd_artikal = ts.vid_artikal)'
      '         --*'
      '      --  (ts.kolicina)'
      '     --  ) as cena,'
      '     t.interen_broj'
      ''
      'from jn_tender_dobitnici ds'
      
        'inner join jn_tender_stavki ts on ts.vid_artikal = ds.vid_artika' +
        'l and ts.artikal = ds.artikal and ts.broj_tender = ds.broj_tende' +
        'r'
      'inner join jn_tender t on t.broj=ts.broj_tender'
      
        'inner join mtr_artikal a on a.artvid=ts.vid_artikal and a.id=ts.' +
        'artikal'
      'inner join jn_tip_artikal v on v.id=a.jn_tip_artikal'
      'left outer join mtr_artgrupa g on g.id=a.grupa'
      
        'left outer join jn_nedelivi_grupi ng on ng.grupa=g.id   and ng.b' +
        'roj_tender=t.broj'
      
        'where ds.broj_tender=:tender and ds.tip_partner = :tip_partner a' +
        'nd ds.partner = :partner'
      'order by a.jn_tip_artikal,a.grupa,a.artvid, a.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 55
    Top = 1796
    object tblDogovorStavkiOdlukaBROJ_TENDER: TFIBStringField
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblDogovorStavkiOdlukaKOLICINA: TFIBBCDField
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblDogovorStavkiOdlukaARTNAZIV: TFIBStringField
      FieldName = 'ARTNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaJN_CPV: TFIBStringField
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblDogovorStavkiOdlukaCENA_BEZ_DDV: TFIBFloatField
      FieldName = 'CENA_BEZ_DDV'
    end
    object tblDogovorStavkiOdlukaCENA_SO_DDV: TFIBFloatField
      FieldName = 'CENA_SO_DDV'
    end
    object tblDogovorStavkiOdlukaDDV: TFIBFloatField
      FieldName = 'DDV'
    end
    object tblDogovorStavkiOdlukaGRUPA_NAZIV: TFIBStringField
      FieldName = 'GRUPA_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaVID_NAZIV: TFIBStringField
      FieldName = 'VID_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaDELIVA: TFIBStringField
      FieldName = 'DELIVA'
      Size = 18
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaGRUPA: TFIBStringField
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiOdlukaJN_TIP_ARTIKAL: TFIBIntegerField
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblDogovorStavkiOdlukaIZNOS_SO_DDV: TFIBBCDField
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
    end
    object tblDogovorStavkiOdlukaIZNOS_BEZ_DDV: TFIBBCDField
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
    end
    object tblDogovorStavkiOdlukaINTEREN_BROJ: TFIBStringField
      FieldName = 'INTEREN_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object frxDogovorStavkiOdluka: TfrxDBDataset
    UserName = 'DogovorStavkiOdluka'
    CloseDataSource = False
    FieldAliases.Strings = (
      'BROJ_TENDER=BROJ_TENDER'
      'ARTIKAL=ARTIKAL'
      'KOLICINA=KOLICINA'
      'ARTNAZIV=ARTNAZIV'
      'MERKA=MERKA'
      'JN_CPV=JN_CPV'
      'GODINA=GODINA'
      'CENA_BEZ_DDV=CENA_BEZ_DDV'
      'CENA_SO_DDV=CENA_SO_DDV'
      'DDV=DDV'
      'GRUPA_NAZIV=GRUPA_NAZIV'
      'VID_NAZIV=VID_NAZIV'
      'DELIVA=DELIVA'
      'GRUPA=GRUPA'
      'JN_TIP_ARTIKAL=JN_TIP_ARTIKAL'
      'IZNOS_SO_DDV=IZNOS_SO_DDV'
      'IZNOS_BEZ_DDV=IZNOS_BEZ_DDV'
      'INTEREN_BROJ=INTEREN_BROJ')
    DataSet = tblDogovorStavkiOdluka
    BCDToCurrency = False
    Left = 272
    Top = 1792
  end
end
