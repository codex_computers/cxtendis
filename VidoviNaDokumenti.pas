unit VidoviNaDokumenti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmVidDokumet = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    NAZIV: TcxDBTextEdit;
    cxGrid1DBTableView1PATEKA: TcxGridDBColumn;
    Label3: TLabel;
    PATEKA: TcxDBTextEdit;
    OpenDialog1: TOpenDialog;
    cxButton1: TcxButton;
    aOpenPateka: TAction;
    cxGrid1DBTableView1TEMPLATE_FRF: TcxGridDBColumn;
    aDizajnReport: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aOpenPatekaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVidDokumet: TfrmVidDokumet;

implementation

{$R *.dfm}

uses dmUnit, dmResources;

procedure TfrmVidDokumet.aAzurirajExecute(Sender: TObject);
begin
   if dm.tblVidDokumentTEMPLATE_FRF.Value = 1 then
     begin
       ShowMessage('��� ��� �� �������� ������ �� �� ������� !!!');
//       Sifra.Enabled:=false;
//       NAZIV.Enabled:=false;
//       PATEKA.Enabled:=false;
//       cxButton1.Enabled:=False;
     end
   else inherited;
end;

procedure TfrmVidDokumet.aDizajnReportExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',dm.tblVidDokumentBROJ_FRF.Value);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmVidDokumet.aOpenPatekaExecute(Sender: TObject);
begin
  inherited;
  OpenDialog1.InitialDir:=pat_dokumenti;
  OpenDialog1.Execute();
  dm.tblVidDokumentPATEKA.Value:=OpenDialog1.FileName;
end;

procedure TfrmVidDokumet.aOtkaziExecute(Sender: TObject);
begin
  inherited;
  Sifra.Enabled:=true;
  NAZIV.Enabled:=true;
  PATEKA.Enabled:=true;
  cxButton1.Enabled:=true;
end;

procedure TfrmVidDokumet.aZapisiExecute(Sender: TObject);
begin
  inherited;
  Sifra.Enabled:=true;
  NAZIV.Enabled:=true;
  PATEKA.Enabled:=true;
  cxButton1.Enabled:=true;
end;

procedure TfrmVidDokumet.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
  if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1TEMPLATE_FRF.Index] = 0) then
       AStyle := dmRes.MoneyGreen;
end;

procedure TfrmVidDokumet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.tblVidDokument.Close;
  dm.tblVidDokument.Open;
end;

end.
