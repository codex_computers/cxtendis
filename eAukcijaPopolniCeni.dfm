object frmAukcijaPopolniCeni: TfrmAukcijaPopolniCeni
  Left = 0
  Top = 0
  Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1094#1077#1085#1080#1090#1077' '#1087#1086#1077#1076#1080#1085#1077#1095#1085#1086' '#1087#1086' '#1089#1090#1072#1074#1082#1072
  ClientHeight = 644
  ClientWidth = 1020
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 625
    Width = 1020
    Height = 19
    Panels = <
      item
        Text = 
          'Shift+Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel, Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086 +
          #1090', Shift+Ctrl+D - '#1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076' '
        Width = 600
      end
      item
        Text = 'Esc - '#1048#1079#1083#1077#1079
        Width = 80
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1020
    Height = 75
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 1
    object cxRadioGroup1: TcxRadioGroup
      Left = 16
      Top = 10
      Caption = #1055#1086#1087#1086#1083#1085#1080' '
      Properties.Columns = 2
      Properties.DefaultValue = 1
      Properties.Items = <
        item
          Caption = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
          Value = 1
        end
        item
          Caption = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
          Value = 2
        end
        item
          Caption = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
          Value = 3
        end
        item
          Caption = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
          Value = 4
        end>
      ItemIndex = 0
      Style.BorderStyle = ebsUltraFlat
      Style.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      TabOrder = 0
      Transparent = True
      OnClick = cxRadioGroup1Click
      Height = 59
      Width = 257
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 75
    Width = 1020
    Height = 550
    Align = alClient
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsStavkiOdPonuda
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      object cxGrid1DBTableView1ODBIENA: TcxGridDBColumn
        DataBinding.FieldName = 'ODBIENA'
        Visible = False
      end
      object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Options.Editing = False
        Width = 34
      end
      object cxGrid1DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDARTIKALNAZIV'
        Visible = False
        Options.Editing = False
        Width = 70
      end
      object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKAL'
        Options.Editing = False
        Width = 56
      end
      object cxGrid1DBTableView1ARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKALNAZIV'
        Options.Editing = False
        Width = 165
      end
      object cxGrid1DBTableView1STAVKA_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'STAVKA_NAZIV'
        Width = 180
      end
      object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
        Options.Editing = False
        Width = 61
      end
      object cxGrid1DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Options.Editing = False
        Width = 40
      end
      object cxGrid1DBTableView1STAVKA_MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'STAVKA_MERKA'
        Width = 81
      end
      object cxGrid1DBTableView1CENA_PRVA_BEZDDV: TcxGridDBColumn
        DataBinding.FieldName = 'CENA_PRVA_BEZDDV'
        Visible = False
        Options.Editing = False
        Width = 107
      end
      object cxGrid1DBTableView1DANOK: TcxGridDBColumn
        DataBinding.FieldName = 'DANOK'
        Options.Editing = False
        Styles.Content = dmRes.cxStyle176
        Width = 37
      end
      object cxGrid1DBTableView1CENABEZDDV: TcxGridDBColumn
        DataBinding.FieldName = 'CENABEZDDV'
        Options.IncSearch = False
        Styles.Content = dmRes.cxStyle120
        Width = 96
      end
      object cxGrid1DBTableView1CENA: TcxGridDBColumn
        DataBinding.FieldName = 'CENA'
        Options.Editing = False
        Options.IncSearch = False
        Styles.Content = dmRes.cxStyle176
        Width = 80
      end
      object cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Visible = False
        Options.Editing = False
        Width = 79
      end
      object cxGrid1DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn
        DataBinding.FieldName = 'IZNOSCENBEZDDV'
        Options.Editing = False
        Options.IncSearch = False
        Styles.Content = dmRes.cxStyle176
        Width = 91
      end
      object cxGrid1DBTableView1IZNOSCENA: TcxGridDBColumn
        DataBinding.FieldName = 'IZNOSCENA'
        Options.Editing = False
        Options.IncSearch = False
        Styles.Content = dmRes.cxStyle176
        Width = 80
      end
      object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Visible = False
        Options.Editing = False
        Width = 49
      end
      object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Visible = False
        Options.Editing = False
        Width = 189
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ActionList1: TActionList
    Left = 456
    Top = 184
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aBrisiIzgled: TAction
      Caption = 'aBrisiIzgled'
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aSnimiIzgled: TAction
      Caption = 'aSnimiIzgled'
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = 'aZacuvajExcel'
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
  end
  object tblStavkiOdPonuda: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDI_STAVKI'
      'SET '
      '    CENA = :CENA,'
      '    CENABEZDDV = :CENABEZDDV,'
      '    DANOK = :DANOK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select ps.id,'
      '       ps.vid_artikal,'
      '       ta.naziv as vidArtikalNaziv,'
      '       ps.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka, '
      '       mm.naziv as merkaNaziv,'
      '       ma.jn_tip_artikal,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ps.kolicina,'
      '       ps.cena,'
      '       ps.cenabezddv,'
      '       ps.danok,'
      '       ps.cena_prva_bezddv,'
      '       ps.cena * ps.kolicina as iznosCena,'
      '       ps.cenabezddv * ps.kolicina as iznosCenBezDDV,'
      '       ps.odbiena,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      ''
      'from jn_ponudi_stavki ps'
      
        'inner join jn_ponudi p on p.id = ps.ponuda_id and p.broj_tender ' +
        '= ps.broj_tender and p.tip_partner = ps.tip_partner and p.partne' +
        'r = ps.partner'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = ps.broj_tende' +
        'r and ts.vid_artikal = ps.vid_artikal and ts.artikal = ps.artika' +
        'l'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ma.artvid = ' +
        'ps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      
        'where(  ps.broj_tender = :mas_broj_tender and ps.tip_partner = :' +
        'mas_tip_partner and ps.partner = :mas_partner and p.odbiena = 0'
      '     ) and (     PS.ID = :OLD_ID'
      '     )'
      'order by ma.jn_tip_artikal, ma.naziv    ')
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.vid_artikal,'
      '       ta.naziv as vidArtikalNaziv,'
      '       ps.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka, '
      '       mm.naziv as merkaNaziv,'
      '       ma.jn_tip_artikal,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ps.kolicina,'
      '       ps.cena,'
      '       ps.cenabezddv,'
      '       ps.danok,'
      '       ps.cena_prva_bezddv,'
      '       ps.cena * ps.kolicina as iznosCena,'
      '       ps.cenabezddv * ps.kolicina as iznosCenBezDDV,'
      '       ps.odbiena,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      ''
      'from jn_ponudi_stavki ps'
      
        'inner join jn_ponudi p on p.id = ps.ponuda_id and p.broj_tender ' +
        '= ps.broj_tender and p.tip_partner = ps.tip_partner and p.partne' +
        'r = ps.partner'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = ps.broj_tende' +
        'r and ts.vid_artikal = ps.vid_artikal and ts.artikal = ps.artika' +
        'l'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ma.artvid = ' +
        'ps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      
        'where ps.broj_tender = :mas_broj_tender and ps.tip_partner = :ma' +
        's_tip_partner and ps.partner = :mas_partner and p.odbiena = 0'
      'order by ma.jn_tip_artikal, ma.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dm.dsPonudi
    Left = 200
    Top = 192
    object tblStavkiOdPonudaVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblStavkiOdPonudaARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblStavkiOdPonudaGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblStavkiOdPonudaCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblStavkiOdPonudaCENASetText
      DisplayFormat = '0.0000 , .'
    end
    object tblStavkiOdPonudaCENABEZDDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENABEZDDV'
      OnSetText = tblStavkiOdPonudaCENABEZDDVSetText
      DisplayFormat = '0.0000 , .'
    end
    object tblStavkiOdPonudaDANOK: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DANOK'
      OnSetText = tblStavkiOdPonudaDANOKSetText
      DisplayFormat = '0.00 , .'
    end
    object tblStavkiOdPonudaCENA_PRVA_BEZDDV: TFIBFloatField
      DisplayLabel = #1055#1088#1074#1072' '#1094#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_PRVA_BEZDDV'
      DisplayFormat = '0.0000, .'
    end
    object tblStavkiOdPonudaIZNOSCENA: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOSCENA'
      OnSetText = tblStavkiOdPonudaIZNOSCENASetText
      DisplayFormat = '0.0000, .'
    end
    object tblStavkiOdPonudaIZNOSCENBEZDDV: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOSCENBEZDDV'
      OnSetText = tblStavkiOdPonudaIZNOSCENBEZDDVSetText
      DisplayFormat = '0.0000, .'
    end
    object tblStavkiOdPonudaODBIENA: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ODBIENA'
    end
    object tblStavkiOdPonudaSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiOdPonudaSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStavkiOdPonuda: TDataSource
    DataSet = tblStavkiOdPonuda
    Left = 304
    Top = 192
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 240
    Top = 368
  end
end
