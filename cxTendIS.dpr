program cxTendIS;

uses
  Forms,
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  Login in '..\Share2010\Login.pas' {frmLogin},
  Master in '..\Share2010\Master.pas' {frmMaster},
  Utils in '..\Share2010\Utils.pas',
  AboutBox in '..\Share2010\AboutBox.pas' {frmAboutBox},
  Trebovanje in 'Trebovanje.pas' {frmTrebovanje},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  MK in '..\Share2010\MK.pas' {frmMK},
  IzborStavkiGodisenPlan in 'IzborStavkiGodisenPlan.pas' {frmIzborStavkiGodisenPlan},
  Artikal in '..\Share2010\Artikal.pas' {frmArtikal},
  dmProcedureQuey in 'dmProcedureQuey.pas' {dmPQ: TDataModule},
  JavnaNabavka in 'JavnaNabavka.pas' {frmJavnaNabavka},
  Komisija in 'Komisija.pas' {frmKomisija},
  Kriteriumi in 'Kriteriumi.pas' {frmKriteriumi},
  Main in 'Main.pas' {frmMain},
  StavkiVoJavnaNabavka in 'StavkiVoJavnaNabavka.pas' {frmStavkiVoJavnaNabavka},
  TIpNabavka in 'TIpNabavka.pas' {frmTipNabavka},
  VidoviDogovor in 'VidoviDogovor.pas' {frmVidoviDogovor},
  Ponudi in 'Ponudi.pas' {frmPonudi},
  IzborStavkiPonuda in 'IzborStavkiPonuda.pas' {frmIzborStavkiPonuda},
  Dobitnici in 'Dobitnici.pas' {frmDobitnici},
  ArtGenerika in '..\Share2010\ArtGenerika.pas' {frmArtGenerika},
  Dogovor in 'Dogovor.pas' {frmDogovor},
  IzborStavkiDogovor in 'IzborStavkiDogovor.pas' {frmIzborStavkiDogovor},
  Baranja in 'Baranja.pas' {frmBaranja},
  IzborStavkiBaranje in 'IzborStavkiBaranje.pas' {frmIzborStavkiBaranja},
  DinamikaRealizacija in 'DinamikaRealizacija.pas' {frmDinamikaRealizacija},
  VidoviNaDokumenti in 'VidoviNaDokumenti.pas' {frmVidDokumet},
  DokumentiZaJavnaNabavka in 'DokumentiZaJavnaNabavka.pas' {frmDokumentiZaJavnaNabavka},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  IzborPartneriDobitnici in 'IzborPartneriDobitnici.pas' {frmIzborPartnerDobitnici},
  SifPotrebniDokumenti in 'SifPotrebniDokumenti.pas' {frmPotrebniDokumenti},
  PonuduvacDostaveniDokumenti in 'PonuduvacDostaveniDokumenti.pas' {frmPonuduvacDostaveniDokumenti},
  eAukcija in 'eAukcija.pas' {frmAukcija},
  eAukcijaPopolniCeni in 'eAukcijaPopolniCeni.pas' {frmAukcijaPopolniCeni},
  RealizacijaPlanSektor in 'RealizacijaPlanSektor.pas' {frmGrafikRealPlan},
  FormConfig in '..\Share2010\FormConfig.pas' {frmFormConfig},
  EAukcijaOsnova in 'EAukcijaOsnova.pas' {frmEAukcijaOsnova},
  PotrebniDokGrupi in 'PotrebniDokGrupi.pas' {frmPotrebniDokGrupi},
  RabotniEdinici in '..\Share2010\RabotniEdinici.pas' {frmRE},
  Partner in '..\Share2010\Partner.pas' {frmPartner},
  TipPartner in '..\Share2010\TipPartner.pas' {frmTipPartner},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  RealizacijaJN in 'RealizacijaJN.pas' {frmRealizacijaJN},
  GodisenPlan in 'GodisenPlan.pas' {frmGodisenPlan},
  Nurko in '..\Share2010\Nurko.pas' {frmNurko},
  NurkoRepository in '..\Share2010\NurkoRepository.pas' {frmNurkoRepository},
  PriciniPonishtuvanje in 'PriciniPonishtuvanje.pas' {frmPricinaPonistuvanje},
  Setup in 'Setup.pas' {frmSetup},
  dmReportUnit in 'dmReportUnit.pas' {dmReport: TDataModule},
  StringUtils in '..\Share2010\StringUtils.pas',
  Notepad in '..\Share2010\Notepad.pas' {frmNotepad},
  JN_CPV in '..\Share2010\JN_CPV.pas' {frmJN_CPV},
  ArtKategorija in '..\Share2010\ArtKategorija.pas' {frmArtKategorija},
  ArtGrupnoKategorija in '..\Share2010\ArtGrupnoKategorija.pas' {frmArtGrupnoKategorija},
  ArtKategorijaArtikal in '..\Share2010\ArtKategorijaArtikal.pas' {frmArtKategorijaArtikal},
  Firma in '..\Share2010\Firma.pas' {frmFirma},
  FirmaInit in '..\Share2010\FirmaInit.pas' {frmFirmaInit},
  AzurirajNazivMerkaStavka in 'AzurirajNazivMerkaStavka.pas' {frmAzurirajStavkiNabavka},
  Poraka in 'Poraka.pas' {frmPoraka},
  PostapkiPredIstekNaDogovor in 'PostapkiPredIstekNaDogovor.pas' {frmPostapkiPredIstekNaDogovor},
  GodisenPlanJN in 'GodisenPlanJN.pas' {frmGodisenPlanJN};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '����� �������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  //Application.CreateForm(TdmReport, dmReport);
  dmKon.aplikacija:='JN';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
  // Application.MainFormOnTaskbar := True;
    Application.CreateForm(TdmReport, dmReport);
    Application.CreateForm(TdmPQ, dmPQ);
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.

