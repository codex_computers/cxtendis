unit JavnaNabavka;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, cxPCdxBarPopupMenu, cxRichEdit, cxDBRichEdit, dxCustomHint,
  cxHint, cxBlobEdit, cxSpinEdit, cxTimeEdit, cxCheckGroup, cxDBCheckGroup,
  FIBQuery, pFIBQuery, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxScrollBox, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxBarBuiltInMenu, cxImageComboBox, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, FIBDataSet,
  pFIBDataSet;

type
//  niza = Array[1..5] of Variant;

  TfrmJavnaNabavka = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxPageControlTabelarenDetalen: TcxPageControl;
    cxTabSheetTabelaren: TcxTabSheet;
    cxTabSheetDetalen: TcxTabSheet;
    PanelTabelarenGrid: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1TIPTENDERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIPTENDERDENOVIZALBA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1PONISTEN: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1INTEREN_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVOREN: TcxGridDBColumn;
    cxGrid1DBTableView1VRSKA_PREDMET: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1DELIV: TcxGridDBColumn;
    cxGrid1DBTableView1OGLAS: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA_SIFRA: TcxGridDBColumn;
    cxGrid1DBTableView1PREDMET_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1LICE_PODGOTOVKA: TcxGridDBColumn;
    cxGrid1DBTableView1LICE_REALIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1LICE_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_BARANJA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_OCENA: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL_KONTAKT: TcxGridDBColumn;
    cxGrid1DBTableView1OBRAZLOZENIE_POSTAPKA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ponistenNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1zatvorenNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1delivNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOGOVOROPIS: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    cxPageControlDetalenPodelen: TcxPageControl;
    cxTabSheetOsnovniPodatoci: TcxTabSheet;
    cxTabSheetOdlukiSoglasnosti: TcxTabSheet;
    cxTabSheetAdministrativni: TcxTabSheet;
    cxGrid1DBTableView1tip_baranjaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1tip_ocenaNaziv: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    PanelOsnovniPodatoci: TPanel;
    cbStatus: TdxBarCombo;
    cxGrid1DBTableView1GrupaNaziv: TcxGridDBColumn;
    PanelOdluki: TPanel;
    PanelAdministrativni: TPanel;
    cxGroupBoxZalbiZaPredmet: TcxGroupBox;
    cxDBCheckBoxZALBA: TcxDBCheckBox;
    Label60: TLabel;
    BROJ_ZALBI: TcxDBTextEdit;
    Label61: TLabel;
    ZALBA_OPIS: TcxDBMemo;
    cxTabSheetKriteriumBodirawe: TcxTabSheet;
    PanelKriterium: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid2DBTableView1TENDERGODINA: TcxGridDBColumn;
    cxGrid2DBTableView1BODIRANJE_PO: TcxGridDBColumn;
    cxGrid2DBTableView1KRITERIUMNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KRITERIUMVREDNUVANJE: TcxGridDBColumn;
    cxGrid2DBTableView1KRITERIUMBODIRANJE: TcxGridDBColumn;
    cxGrid2DBTableView1MAX_BODOVI: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1vrednuvanjeNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1bodiranjeNaziv: TcxGridDBColumn;
    kriteriumButtonDodadi: TcxButton;
    aDodadiKriterium: TAction;
    kriteriumButtonIzvadi: TcxButton;
    aIzvadiStar: TAction;
    aZapisiKriteriumPanel: TAction;
    cxGrid1DBTableView1ZALBA: TcxGridDBColumn;
    cxGrid1DBTableView1zalbaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ZALBA_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_ZALBI: TcxGridDBColumn;
    cxGrid1DBTableView1ODLUKA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1ODLUKA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OTVARANJE: TcxGridDBColumn;
    cxGrid1DBTableView1OSTETENI_KOVERTI: TcxGridDBColumn;
    cxGrid1DBTableView1ODL_NABAVKA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1ODL_NABAVKA_DAT: TcxGridDBColumn;
    cxGrid1DBTableView1SOGLASNOST_BR: TcxGridDBColumn;
    cxGrid1DBTableView1SOGLASNOST_DAT: TcxGridDBColumn;
    cxGrid1DBTableView1ODL_POKANA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1ODL_POKANA_DAT: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_POKANI: TcxGridDBColumn;
    cxGrid1DBTableView1POTVRDA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1POTVRDA_DAT: TcxGridDBColumn;
    cxGrid1DBTableView1POTVRDA_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1ANEKS_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ANEKS_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1ANEKS_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1BODIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1VREME_BOD: TcxGridDBColumn;
    cxGrid1DBTableView1VREME_OTVARANJE: TcxGridDBColumn;
    cxGrid1DBTableView1VKUPNO_PONUDI: TcxGridDBColumn;
    cxGrid1DBTableView1ZA_POVLEKUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1ZA_IZMENA: TcxGridDBColumn;
    cxGrid1DBTableView1BR_ZADOCNETI_PONUDI: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAPISNIK: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_OTVARANJE: TcxGridDBColumn;
    cxGrid1DBTableView1CENTRALNO_TELO: TcxGridDBColumn;
    cxGrid1DBTableView1centralnoTeloNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPNA_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1grupnaNabavkaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1RAMKOVNA_SPOG: TcxGridDBColumn;
    cxGrid1DBTableView1ramkovnaSpogodbaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1RS_POVEKE_ORGANI: TcxGridDBColumn;
    cxGrid1DBTableView1rsPovekeOrganiNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1RS_POVEKE_OPERATORI: TcxGridDBColumn;
    cxGrid1DBTableView1rsPovekeOperatoriNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ALTERNATIVNI_PONUDI: TcxGridDBColumn;
    cxGrid1DBTableView1alternativniPonudiNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1GARANCIJA_PONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1GARANCIJA_KVALITET: TcxGridDBColumn;
    cxGrid1DBTableView1AVANSNO: TcxGridDBColumn;
    cxGrid1DBTableView1avansnoNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1ZDRUZUVANJE_OPERATORI: TcxGridDBColumn;
    cxGrid1DBTableView1zdruzuvanjeOperatoriNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1SKRATENI_ROKOVI: TcxGridDBColumn;
    cxGrid1DBTableView1skratenNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1EL_SREDSTVA: TcxGridDBColumn;
    cxGrid1DBTableView1elSredstvaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1EL_AUKCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1elAukcijaNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1INFO_AUKCIJA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_DOKUMENTACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1INFO_PLAKJANJE: TcxGridDBColumn;
    cxGrid1DBTableView1DENARSKA: TcxGridDBColumn;
    cxGrid1DBTableView1DEVIZNA: TcxGridDBColumn;
    cxGrid1DBTableView1VALIDNOST_PONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1DOPOLNITELNI_INFO: TcxGridDBColumn;
    cxGrid1DBTableView1VREMETRAENJE_DOG: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIJA_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView1ODLUKA_PROSIRUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ODLUKA_PROSIRUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_PROSIRUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1IZVOR_SREDSTVA: TcxGridDBColumn;
    cxGrid1DBTableView1MIN_PONUDI: TcxGridDBColumn;
    cxGrid1DBTableView1MAKS_PONUDI: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1TI_TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TI_TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1TI_USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TI_USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton10: TdxBarLargeButton;
    aStavkiVoJavnaNabavka: TAction;
    cxGrid1DBTableView1tip_god_plan_Naziv: TcxGridDBColumn;
    cxGroupBoxAdministrativniInformacii: TcxGroupBox;
    CENA_DOKUMENTACIJA: TcxDBTextEdit;
    Label55: TLabel;
    Label57: TLabel;
    DENARSKA: TcxDBTextEdit;
    Label58: TLabel;
    DEVIZNA: TcxDBTextEdit;
    Label56: TLabel;
    INFO_PLAKJANJE: TcxDBRichEdit;
    DATUM: TcxDBDateEdit;
    Label4: TLabel;
    BROJ: TcxDBTextEdit;
    Label2: TLabel;
    GODINA: TcxDBComboBox;
    Label3: TLabel;
    cxGroupBoxOtvarawePonudi: TcxGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    VREME_OTVARANJE: TcxDBTimeEdit;
    DATUM_OTVARANJE: TcxDBDateEdit;
    MESTO_OTVARANJE: TcxDBTextEdit;
    Label59: TLabel;
    DOPOLNITELNI_INFO: TcxDBRichEdit;
    cxTabSheetPostapka: TcxTabSheet;
    PanelPostapka: TPanel;
    cxGroupBoxPredmetNaDogovor: TcxGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label53: TLabel;
    Label5: TLabel;
    cxDBRadioGroupVID: TcxDBRadioGroup;
    GRUPA: TcxDBTextEdit;
    GRUPA_SIFRA: TcxDBTextEdit;
    GRUPA_OPIS: TcxDBLookupComboBox;
    PREDMET_NABAVKA: TcxDBMemo;
    cxDBRadioGroupRAMKOVNA_SPOG: TcxDBRadioGroup;
    cxDBRadioGroupRS_POVEKE_ORGANI: TcxDBRadioGroup;
    cxDBRadioGroupRS_POVEKE_OPERATORI: TcxDBRadioGroup;
    cxDBRadioGroupCentralnoTelo: TcxDBRadioGroup;
    cxDBRadioGroupDELIV: TcxDBRadioGroup;
    cxDBRadioGroupGrupnaNabavka: TcxDBRadioGroup;
    cxDBRadioGroupALTERNATIVNI_PONUDI: TcxDBRadioGroup;
    VREMETRAENJE_DOG: TcxDBTextEdit;
    OPIS: TcxDBRichEdit;
    cxGroupBoxAneksDogovor: TcxGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    ANEKS_DATUM: TcxDBDateEdit;
    ANEKS_BROJ: TcxDBTextEdit;
    ANEKS_IZNOS: TcxDBTextEdit;
    cxGroupBoxIzvorSredstva: TcxGroupBox;
    Label47: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    POTVRDA_DAT: TcxDBDateEdit;
    POTVRDA_BR: TcxDBTextEdit;
    POTVRDA_IZNOS: TcxDBTextEdit;
    cxGroupBoxSoglasnostOdBiroto: TcxGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    SOGLASNOST_DAT: TcxDBDateEdit;
    SOGLASNOST_BR: TcxDBTextEdit;
    cxGroupBoxOdlukaNajpovolenPonuduvac: TcxGroupBox;
    Label43: TLabel;
    Label44: TLabel;
    ODLUKA_DATUM: TcxDBDateEdit;
    ODLUKA_BROJ: TcxDBTextEdit;
    cxGroupBoxOdlukaZgolemuvanjeIznos: TcxGroupBox;
    Label42: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    DATUM_ODLUKA_PROSIRUVANJE: TcxDBDateEdit;
    ODLUKA_PROSIRUVANJE: TcxDBTextEdit;
    IZNOS_PROSIRUVANJE: TcxDBTextEdit;
    cxGroupBoxOdlukaPokani: TcxGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    ODL_POKANA_DAT: TcxDBDateEdit;
    ODL_POKANA_BR: TcxDBTextEdit;
    BROJ_POKANI: TcxDBTextEdit;
    cxGroupBoxOdlukiZaNabavka: TcxGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    ODL_NABAVKA_DAT: TcxDBDateEdit;
    ODL_NABAVKA_BR: TcxDBTextEdit;
    cxGroupBoxBrKandidatiPokaneti: TcxGroupBox;
    Label65: TLabel;
    Label66: TLabel;
    MIN_PONUDI: TcxDBTextEdit;
    MAKS_PONUDI: TcxDBTextEdit;
    cxGroupBoxPostapka: TcxGroupBox;
    Label1: TLabel;
    Label17: TLabel;
    TIP_TENDER_NAZIV: TcxDBLookupComboBox;
    OBRAZLOZENIE_POSTAPKA: TcxDBRichEdit;
    TIP_TENDER: TcxDBTextEdit;
    cxDBRadioGroupSKRATENI_ROKOVI: TcxDBRadioGroup;
    cxDBRadioGroupTIP_OCENA: TcxDBRadioGroup;
    Label14: TLabel;
    LICE_REALIZACIJA: TcxDBTextEdit;
    Label63: TLabel;
    LOKACIJA_ISPORAKA: TcxDBTextEdit;
    cxGroupBoxElektronskiSistem: TcxGroupBox;
    Label54: TLabel;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxDBRadioGroup2: TcxDBRadioGroup;
    INFO_AUKCIJA: TcxDBRichEdit;
    Label13: TLabel;
    LICE_PODGOTOVKA: TcxDBTextEdit;
    Label15: TLabel;
    LICE_KONTAKT: TcxDBTextEdit;
    EMAIL_KONTAKT: TcxDBTextEdit;
    Label16: TLabel;
    Label6: TLabel;
    INTEREN_BROJ: TcxDBTextEdit;
    Label7: TLabel;
    ARHIVSKI_BROJ: TcxDBTextEdit;
    Label10: TLabel;
    OGLAS: TcxDBTextEdit;
    cxTabSheetFinansiskiTehnicki: TcxTabSheet;
    PanelFinansiski: TPanel;
    cxGroupBoxGaranciiAvansnoPlakjanje: TcxGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label52: TLabel;
    GARANCIJA_PONUDA: TcxDBTextEdit;
    GARANCIJA_KVALITET: TcxDBTextEdit;
    cxDBRadioGroupAVANSNO: TcxDBRadioGroup;
    cxDBRadioGroupZDRUZUVANJE_OPERATORI: TcxDBRadioGroup;
    cxDBRadioGroupPONISTEN: TcxDBRadioGroup;
    cxDBRadioGroupZATVOREN: TcxDBRadioGroup;
    VRSKA_PREDMET: TcxDBTextEdit;
    Label8: TLabel;
    cxGroupBoxZapisnikOtvaranjePonudi: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DATUM_ZAPISNIK: TcxDBDateEdit;
    VKUPNO_PONUDI: TcxDBTextEdit;
    BR_ZADOCNETI_PONUDI: TcxDBTextEdit;
    ZA_POVLEKUVANJE: TcxDBRichEdit;
    ZA_IZMENA: TcxDBRichEdit;
    OSTETENI_KOVERTI: TcxDBTextEdit;
    Label62: TLabel;
    cxDBRadioGroupTIP_BARANJA: TcxDBRadioGroup;
    Label9: TLabel;
    IZNOS: TcxDBTextEdit;
    cxGrid1DBTableView1SEKTORSKI_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1sektorski_dogovorTeloNaziv: TcxGridDBColumn;
    cxDBRadioGroup3: TcxDBRadioGroup;
    ButtonZapisi3: TcxButton;
    ButtonOtkazi3: TcxButton;
    cxGroupBoxPeriodNaPrimawePonudi: TcxGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;
    VALIDNOST_PONUDA: TcxDBTextEdit;
    Label29: TLabel;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    aOsnovniProdolzi: TAction;
    ButtonOtkazi2: TcxButton;
    PanelVnesOsnovniPodatoci: TPanel;
    cxGroupBox2: TcxGroupBox;
    Label51: TLabel;
    GODINA_1: TcxDBComboBox;
    Label68: TLabel;
    BROJ_1: TcxDBTextEdit;
    Label69: TLabel;
    GRUPA_1: TcxDBTextEdit;
    GRUPA_SIFRA_1: TcxDBTextEdit;
    GRUPA_OPIS_1: TcxDBLookupComboBox;
    cxDBRadioGroupGRUPA_1: TcxDBRadioGroup;
    Label70: TLabel;
    PREDMET_NABAVKA_1: TcxDBMemo;
    Label71: TLabel;
    TIP_TENDER_1: TcxDBTextEdit;
    TIP_TENDER_NAZIV_1: TcxDBLookupComboBox;
    ButtonProdolzi_1: TcxButton;
    ButtonOtkazi1: TcxButton;
    ButtonProdolzi_2: TcxButton;
    ButtonZapisi2: TcxButton;
    ButtonProdolzi_3: TcxButton;
    ButtonZapisi4: TcxButton;
    ButtonOtkazi4: TcxButton;
    ButtonProdolzi_4: TcxButton;
    ButtonZapisi5: TcxButton;
    ButtonOtkazi5: TcxButton;
    ButtonProdolzi_5: TcxButton;
    ButtonZapisi6: TcxButton;
    ButtonOtkazi6: TcxButton;
    ButtonProdolzi_6: TcxButton;
    cxTabSheetKomisija: TcxTabSheet;
    PanelKomisija: TPanel;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ButtonDodadiK������: TcxButton;
    ButtonIzvadiK������: TcxButton;
    cxGridDBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGridDBTableView1CLEN: TcxGridDBColumn;
    cxGridDBTableView1IME_PREZIME: TcxGridDBColumn;
    cxGridDBTableView1FUNKCIJA: TcxGridDBColumn;
    cxGridDBTableView1REDBR: TcxGridDBColumn;
    cxGridDBTableView1TS_INS: TcxGridDBColumn;
    cxGridDBTableView1TS_UPD: TcxGridDBColumn;
    cxGridDBTableView1USR_INS: TcxGridDBColumn;
    cxGridDBTableView1USR_UPD: TcxGridDBColumn;
    aDodadiKomisija: TAction;
    aIzvadiKomisija: TAction;
    qRedBrClenKomisija: TpFIBQuery;
    aZapisiKomisija: TAction;
    PanelVnesiKomisija: TPanel;
    Label67: TLabel;
    cxGroupBox3: TcxGroupBox;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    CLEN: TcxDBLookupComboBox;
    REDBR: TcxDBTextEdit;
    FUNKCIJA: TcxDBComboBox;
    PanelMaxPoeni: TPanel;
    Label64: TLabel;
    cxGroupBox1: TcxGroupBox;
    MAX_BODOVI: TcxDBTextEdit;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aPonudi: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    aDobitnici: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    aDogovori: TAction;
    dxBarManager1Bar8: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    aPSpecifikacijaJavnaNabavka: TAction;
    aDizajnSpecifikacijaJavnaNabavka: TAction;
    N2: TMenuItem;
    aPopUpMenu: TAction;
    aPSpecifikacijaPoSektori: TAction;
    aDizajnSpecifikacijaPoSektori: TAction;
    aPSpecifikacijaPoArtikli: TAction;
    aDizajnSpecifikacijaPoArtikli: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    dxBarLargeButton23: TdxBarLargeButton;
    aBaranjaIsporaka: TAction;
    dxBarManager1Bar9: TdxBar;
    aDokumenti: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    cxScrollBoxOsnovniPodatoci: TcxScrollBox;
    cxScrollBoxPostapka: TcxScrollBox;
    cxScrollBoxAdministrativni: TcxScrollBox;
    cxScrollBoxOdlukiSoglasnosti: TcxScrollBox;
    cxTabSheetPotrebniDokumenti: TcxTabSheet;
    PanelPotrebniDokumenti: TPanel;
    cxGrid4: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxButtonDodadiDokument: TcxButton;
    cxButtonIzvadiDokument: TcxButton;
    cxGridDBTableView2BROJ_TENDER: TcxGridDBColumn;
    cxGridDBTableView2POTREBNI_DOKUMENTI_ID: TcxGridDBColumn;
    cxGridDBTableView2NAZIV: TcxGridDBColumn;
    cxGridDBTableView2TIP: TcxGridDBColumn;
    cxGridDBTableView2tipNaziv: TcxGridDBColumn;
    cxGridDBTableView2TS_INS: TcxGridDBColumn;
    cxGridDBTableView2TS_UPD: TcxGridDBColumn;
    cxGridDBTableView2USR_INS: TcxGridDBColumn;
    cxGridDBTableView2USR_UPD: TcxGridDBColumn;
    aDodadiPotrebenDok: TAction;
    aIzvadiPotrebenDokument: TAction;
    DateTimePicker1: TDateTimePicker;
    dxBarButton5: TdxBarButton;
    aOdlukaZaNabavka: TAction;
    aDOdlukaZaNabavka: TAction;
    N5: TMenuItem;
    aOglasJN: TAction;
    aDOglasJN: TAction;
    N6: TMenuItem;
    dxBarButton6: TdxBarButton;
    Label75: TLabel;
    KONTAKT_TEL_FAKS: TcxDBTextEdit;
    cxGrid1DBTableView1KONTAKT_TEL_FAKS: TcxGridDBColumn;
    ZABELESKA_ZAPISNIK: TcxDBRichEdit;
    Label76: TLabel;
    cxGrid1DBTableView1ZABELESKA_ZAPISNIK: TcxGridDBColumn;
    dxBarButton7: TdxBarButton;
    aPZapisnikOtvoranjePonudi: TAction;
    aDZapisnikOtvoranjePonudi: TAction;
    N7: TMenuItem;
    PopupMenu2: TPopupMenu;
    aPOdlukaIzborNajpovolnaPonuda: TAction;
    aDOdlukaIzborNajpovolnaPonuda: TAction;
    dxBarButton8: TdxBarButton;
    aDOdlukaIzborNajpovolnaPonuda1: TMenuItem;
    dxBarButton9: TdxBarButton;
    aPOdlukaZaPonistuvanjeNaStavki: TAction;
    aDOdlukaZaPonistuvanjeNaStavki: TAction;
    N8: TMenuItem;
    aPRealizacijaJN: TAction;
    aDRealizacijaJN: TAction;
    dxBarButton10: TdxBarButton;
    j1: TMenuItem;
    dxBarButton11: TdxBarButton;
    aPRealizacijaPlanNabavkaPoRE: TAction;
    aDRealizacijaPlanNabavkaPoRE: TAction;
    N9: TMenuItem;
    dxBarLargeButton25: TdxBarLargeButton;
    aRealizacijaPlanNabavkaRe: TAction;
    aDOdlukaZaGodisenPlan: TAction;
    aPOdlukaZaGodisenPlan: TAction;
    N10: TMenuItem;
    dxBarButton12: TdxBarButton;
    aPIzvestajSprovedenaPostapka: TAction;
    aDIzvestajSprovedenaPostapka: TAction;
    dxBarButton13: TdxBarButton;
    N11: TMenuItem;
    cxScrollBoxFinansiski: TcxScrollBox;
    IZVOR_SREDSTVA: TcxDBComboBox;
    aPPocetnaRangListaZaUcestvoNaEAukcija: TAction;
    aDPocetnaRangListaZaUcestvoNaEAukcija: TAction;
    N12: TMenuItem;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCena: TAction;
    N13: TMenuItem;
    cxDBCheckBox1: TcxDBCheckBox;
    cxGrid1DBTableView1garancijaIzjavaNaziv: TcxGridDBColumn;
    aPOdlukaIzborNajpovolnaPonudaCena: TAction;
    aDOdlukaIzborNajpovolnaPonudaCena: TAction;
    dxBarButton16: TdxBarButton;
    N14: TMenuItem;
    cxGridDBTableView2GRUPANAZIV: TcxGridDBColumn;
    dxBarButton17: TdxBarButton;
    aDIzvestajSprovedenaPostapkaNajniskaCena: TAction;
    N15: TMenuItem;
    lbl1: TLabel;
    NAZIVPONISTEN_PRICINA: TcxDBTextEdit;
    PONISTEN_PRICINA_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1PONISTEN_PRICINA: TcxGridDBColumn;
    cxGrid1DBTableView1PRICINAPONISTENNAZIV: TcxGridDBColumn;
    dxBarButton18: TdxBarButton;
    actPOdlukaZaPonistuvanjePostapka: TAction;
    actDOdlukaZaPonistuvanjePostapka: TAction;
    N16: TMenuItem;
    cxGridDBTableView1PRISUTEN: TcxGridDBColumn;
    dxBarLargeButton26: TdxBarLargeButton;
    actPrevzemiPonisteniStavki: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    aPIznosNaDogovorPoPartnerGddini: TAction;
    aDIznosNaDogovorPoPartnerGddini: TAction;
    N17: TMenuItem;
    dxBarButton19: TdxBarButton;
    aPPocetnaRangListaPoKriteriumi: TAction;
    aDPocetnaRangListaPoKriteriumi: TAction;
    N18: TMenuItem;
    aDSpecifikacijaJavnaNabavkaUslugi: TAction;
    N19: TMenuItem;
    aPNedobitniStavki: TAction;
    aDNedobitniStavki: TAction;
    aDNedobitniStavki1: TMenuItem;
    dxBarButton20: TdxBarButton;
    dxBarButton21: TdxBarButton;
    aPPokanaKonecnaCena: TAction;
    aDPokanaKonecnaCena: TAction;
    N20: TMenuItem;
    dxBarButton22: TdxBarButton;
    aPDobitniStavki: TAction;
    aDDobitniStavki: TAction;
    Action21: TMenuItem;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxDBCheckBox2: TcxDBCheckBox;
    dxBarButton23: TdxBarButton;
    aPTehnickaSpecifikacija: TAction;
    aDTehnickaSpecifikacija: TAction;
    N21: TMenuItem;
    dxBarButton24: TdxBarButton;
    aPOdlukaJN_SO_CENA: TAction;
    aDOdlukaJN_SO_CENA: TAction;
    N22: TMenuItem;
    Label77: TLabel;
    IZNOS_SO_DDV: TcxDBTextEdit;
    cxGrid1DBTableView1IZNOS_SO_DDV: TcxGridDBColumn;
    qPromenetiCeni: TpFIBDataSet;
    qPromenetiCeniBROJ_TENDER: TFIBStringField;
    pnlPromenetiCeni: TPanel;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5: TcxGrid;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    dsPromenetiCeni: TDataSource;
    cxGrid5DBTableView1BROJ_TENDER: TcxGridDBColumn;
    dxBarButton25: TdxBarButton;
    dxBarButton26: TdxBarButton;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    actPSporedbaOdlukaJN_OdlukaIzborNajpovolen: TAction;
    actDSporedbaOdlukaJN_OdlukaIzborNajpovolen: TAction;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton27: TdxBarButton;
    N26: TMenuItem;
    actPSpecifikacijaPoSektoriSoPlan: TAction;
    actDSpecifikacijaPoSektoriSoPlan: TAction;
    actDSpecifikacijaPoSektoriSoBaranje1: TMenuItem;
    dxBarButton28: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton29: TdxBarButton;
    dxBarButton30: TdxBarButton;
    dxBarButton31: TdxBarButton;
    dxBarButton32: TdxBarButton;
    act_S_VoPodgotovka: TAction;
    act_S_Otvoren: TAction;
    act_S_Zatvoren: TAction;
    act_S_DelumnoZavrsen: TAction;
    cxGroupBoxIzvestaOdSprovedenaPostapka: TcxGroupBox;
    lbl2: TLabel;
    lbl3: TLabel;
    ARH_DATUM_ISP: TcxDBDateEdit;
    ARH_BR_ISP: TcxDBTextEdit;
    actGodisenPlanJN: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    pnlExportExcel: TPanel;
    cxGrid6DBTableView1: TcxGridDBTableView;
    cxGrid6Level1: TcxGridLevel;
    cxGrid6: TcxGrid;
    tblIzborNajpovolnaPonudaCena: TpFIBDataSet;
    dsIzborNajpovolnaPonudaCena: TDataSource;
    tblIzborNajpovolnaPonudaCenaSKRATENNAZIVPARTNER: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaPARTNERNAZIV: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaARTIKAL: TFIBIntegerField;
    tblIzborNajpovolnaPonudaCenaJN_CPV: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaCENABEZDDV: TFIBFloatField;
    tblIzborNajpovolnaPonudaCenaKOLICINA: TFIBBCDField;
    tblIzborNajpovolnaPonudaCenaDANOK: TFIBFloatField;
    tblIzborNajpovolnaPonudaCenaCENA: TFIBFloatField;
    tblIzborNajpovolnaPonudaCenaIZNOS: TFIBFloatField;
    tblIzborNajpovolnaPonudaCenaIZNOOSBEZDDV: TFIBFloatField;
    tblIzborNajpovolnaPonudaCenaARTIKALNAZIV: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaMERKA: TFIBStringField;
    cxGrid6DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid6DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid6DBTableView1CENABEZDDV: TcxGridDBColumn;
    cxGrid6DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid6DBTableView1DANOK: TcxGridDBColumn;
    cxGrid6DBTableView1CENA: TcxGridDBColumn;
    cxGrid6DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid6DBTableView1IZNOOSBEZDDV: TcxGridDBColumn;
    cxGrid6DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1MERKA: TcxGridDBColumn;
    tblIzborNajpovolnaPonudaCenaGRUPA_NAZIV: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaVID_NAZIV: TFIBStringField;
    tblIzborNajpovolnaPonudaCenaDELIVA: TFIBStringField;
    cxGrid6DBTableView1GRUPA_NAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1VID_NAZIV: TcxGridDBColumn;
    cxGrid6DBTableView1DELIVA: TcxGridDBColumn;
    dxBarSubItem5: TdxBarSubItem;
    dxBarSeparator1: TdxBarSeparator;
    dxBarButton33: TdxBarButton;
    actExportToExcelOdlukaNajpovolna: TAction;
    cxGrid1DBTableView1ZATVORI_PONUDI_STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVORI_PONUDI_TIME: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVORI_PONUDI_USER: TcxGridDBColumn;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton34: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarButton35: TdxBarButton;
    cxGrid1DBTableView1BROJ_PART1: TcxGridDBColumn;
    dxBarButton36: TdxBarButton;
    actPNevoobicaenaNiskaCena: TAction;
    actDNevoobicaenaNiskaCena: TAction;
    actDNevoobicaenaNiskaCena1: TMenuItem;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure OBRAZLOZENIE_POSTAPKADblClick(Sender: TObject);
    procedure OPISDblClick(Sender: TObject);
    procedure PREDMET_NABAVKADblClick(Sender: TObject);
    procedure cxDBRadioGroupVIDExit(Sender: TObject);
    procedure cxDBRadioGroupVIDPropertiesEditValueChanged(Sender: TObject);
    procedure cbStatusChange(Sender: TObject);
    procedure INFO_AUKCIJADblClick(Sender: TObject);
    procedure INFO_PLAKJANJEDblClick(Sender: TObject);
    procedure DOPOLNITELNI_INFODblClick(Sender: TObject);
    procedure ZA_POVLEKUVANJEDblClick(Sender: TObject);
    procedure ZA_IZMENADblClick(Sender: TObject);
    procedure ZALBA_OPISDblClick(Sender: TObject);
    procedure aDodadiKriteriumExecute(Sender: TObject);
    procedure aZapisiKriteriumPanelExecute(Sender: TObject);
    procedure aIzvadiStarExecute(Sender: TObject);
    procedure cxPageControlTabelarenDetalenPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
    procedure cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure aStavkiVoJavnaNabavkaExecute(Sender: TObject);
    procedure aOsnovniProdolziExecute(Sender: TObject);
    procedure cxDBRadioGroupGRUPA_1Exit(Sender: TObject);
    procedure PREDMET_NABAVKA_1DblClick(Sender: TObject);
    procedure aDodadiKomisijaExecute(Sender: TObject);
    procedure aIzvadiKomisijaExecute(Sender: TObject);
    procedure aZapisiKomisijaExecute(Sender: TObject);
    procedure aPonudiExecute(Sender: TObject);
    procedure dxBarLargeButton18Click(Sender: TObject);
    procedure aDobitniciExecute(Sender: TObject);
    procedure aDogovoriExecute(Sender: TObject);
    procedure cxDBRadioGroupPONISTENExit(Sender: TObject);
    procedure aPopUpMenuExecute(Sender: TObject);
    procedure aPSpecifikacijaJavnaNabavkaExecute(Sender: TObject);
    procedure aDizajnSpecifikacijaJavnaNabavkaExecute(Sender: TObject);
    procedure aPSpecifikacijaPoSektoriExecute(Sender: TObject);
    procedure aDizajnSpecifikacijaPoSektoriExecute(Sender: TObject);
    procedure aPSpecifikacijaPoArtikliExecute(Sender: TObject);
    procedure aDizajnSpecifikacijaPoArtikliExecute(Sender: TObject);
    procedure aBaranjaIsporakaExecute(Sender: TObject);
    procedure aDokumentiExecute(Sender: TObject);
    procedure aDodadiPotrebenDokExecute(Sender: TObject);
    procedure aIzvadiPotrebenDokumentExecute(Sender: TObject);
    procedure aOdlukaZaNabavkaExecute(Sender: TObject);
    procedure aDOdlukaZaNabavkaExecute(Sender: TObject);
    procedure aOglasJNExecute(Sender: TObject);
    procedure aDOglasJNExecute(Sender: TObject);
    procedure ZABELESKA_ZAPISNIKDblClick(Sender: TObject);
    procedure aPZapisnikOtvoranjePonudiExecute(Sender: TObject);
    procedure aDZapisnikOtvoranjePonudiExecute(Sender: TObject);
    procedure aPOdlukaIzborNajpovolnaPonudaExecute(Sender: TObject);
    procedure aDOdlukaIzborNajpovolnaPonudaExecute(Sender: TObject);
    procedure aDOdlukaZaPonistuvanjeNaStavkiExecute(Sender: TObject);
    procedure aPOdlukaZaPonistuvanjeNaStavkiExecute(Sender: TObject);
    procedure aPRealizacijaJNExecute(Sender: TObject);
    procedure aDRealizacijaJNExecute(Sender: TObject);
    procedure aPRealizacijaPlanNabavkaPoREExecute(Sender: TObject);
    procedure aDRealizacijaPlanNabavkaPoREExecute(Sender: TObject);
    procedure aRealizacijaPlanNabavkaReExecute(Sender: TObject);
    procedure aPOdlukaZaGodisenPlanExecute(Sender: TObject);
    procedure aDOdlukaZaGodisenPlanExecute(Sender: TObject);
    procedure aPIzvestajSprovedenaPostapkaExecute(Sender: TObject);
    procedure aDIzvestajSprovedenaPostapkaExecute(Sender: TObject);
    procedure aDPocetnaRangListaZaUcestvoNaEAukcijaExecute(Sender: TObject);
    procedure aPPocetnaRangListaZaUcestvoNaEAukcijaExecute(Sender: TObject);
    procedure aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCenaExecute(
      Sender: TObject);
    procedure cxPageControlDetalenPodelenPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
    procedure cxDBRadioGroupRAMKOVNA_SPOGClick(Sender: TObject);
    procedure aDOdlukaIzborNajpovolnaPonudaCenaExecute(Sender: TObject);
    procedure aPOdlukaIzborNajpovolnaPonudaCenaExecute(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure actPOdlukaZaPonistuvanjePostapkaExecute(Sender: TObject);
    procedure actDOdlukaZaPonistuvanjePostapkaExecute(Sender: TObject);
    procedure cxGridDBTableView1PRISUTENPropertiesChange(Sender: TObject);
    procedure actPrevzemiPonisteniStavkiExecute(Sender: TObject);
    function  ponisten_tender(broj:String; res:Boolean):Boolean;
    procedure aDIznosNaDogovorPoPartnerGddiniExecute(Sender: TObject);
    procedure aPIznosNaDogovorPoPartnerGddiniExecute(Sender: TObject);
    procedure aPPocetnaRangListaPoKriteriumiExecute(Sender: TObject);
    procedure aDPocetnaRangListaPoKriteriumiExecute(Sender: TObject);
    procedure aDSpecifikacijaJavnaNabavkaUslugiExecute(Sender: TObject);
    procedure aPNedobitniStavkiExecute(Sender: TObject);
    procedure aDNedobitniStavkiExecute(Sender: TObject);
    procedure aPPokanaKonecnaCenaExecute(Sender: TObject);
    procedure aDPokanaKonecnaCenaExecute(Sender: TObject);
    procedure aPDobitniStavkiExecute(Sender: TObject);
    procedure aDDobitniStavkiExecute(Sender: TObject);
    procedure aPTehnickaSpecifikacijaExecute(Sender: TObject);
    procedure aDTehnickaSpecifikacijaExecute(Sender: TObject);
    procedure aPOdlukaJN_SO_CENAExecute(Sender: TObject);
    procedure aDOdlukaJN_SO_CENAExecute(Sender: TObject);
    procedure dxBarButton25Click(Sender: TObject);
    procedure dxBarButton26Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure actPSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute(
      Sender: TObject);
    procedure actDSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute(
      Sender: TObject);
    procedure actPSpecifikacijaPoSektoriSoPlanExecute(Sender: TObject);
    procedure actDSpecifikacijaPoSektoriSoPlanExecute(Sender: TObject);
    procedure act_S_VoPodgotovkaExecute(Sender: TObject);
    procedure act_S_OtvorenExecute(Sender: TObject);
    procedure act_S_ZatvorenExecute(Sender: TObject);
    procedure act_S_DelumnoZavrsenExecute(Sender: TObject);
    procedure actGodisenPlanJNExecute(Sender: TObject);
    procedure actExportToExcelOdlukaNajpovolnaExecute(Sender: TObject);
    procedure dxBarButton34Click(Sender: TObject);
    procedure dxBarButton35Click(Sender: TObject);
    procedure GODINA_1PropertiesEditValueChanged(Sender: TObject);
    procedure GODINAPropertiesEditValueChanged(Sender: TObject);
    procedure actPNevoobicaenaNiskaCenaExecute(Sender: TObject);
    procedure actDNevoobicaenaNiskaCenaExecute(Sender: TObject);
   // function zemiRezultat5(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):Variant;
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmJavnaNabavka: TfrmJavnaNabavka;
  ponisten_potoa, ponisten_pred, azuriraj:Integer;
  vrska_pred, vrska_potoa:string;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, Notepad,
  Kriteriumi, StavkiVoJavnaNabavka, Komisija, Ponudi, Dobitnici, Dogovor,
  dmProcedureQuey, Baranja, DokumentiZaJavnaNabavka, SifPotrebniDokumenti,
  dmReportUnit, RealizacijaPlanSektor, RealizacijaJN, GodisenPlanJN;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmJavnaNabavka.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmJavnaNabavka.aPNedobitniStavkiExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40049);
    dmKon.tblSqlReport.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end
end;

procedure TfrmJavnaNabavka.aNovExecute(Sender: TObject);
var pom:String;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
   begin
      cxPageControlTabelarenDetalen.Enabled:=false;
      PanelVnesOsnovniPodatoci.Visible:=true;

       dm.qZemiPovtoruvackiPodatoci.Close;
                  dm.qZemiPovtoruvackiPodatoci.ExecQuery;

      cxGrid1DBTableView1.DataController.DataSet.Insert;

      dm.tblTenderGODINA.Value:=dmKon.godina;
      dm.tblTenderDATUM.Value:=Now;
      if cbStatus.Text = '' then
         dm.tblTenderZATVOREN.Value:= -1
      else if cbStatus.Text = '�� ����������' then
         dm.tblTenderZATVOREN.Value:= -1
      else if cbStatus.Text = '�������' then
         dm.tblTenderZATVOREN.Value:= 0
      else if cbStatus.Text = '��������' then
         dm.tblTenderZATVOREN.Value:= 1
      else if cbStatus.Text = '������� �������' then
         dm.tblTenderZATVOREN.Value:= 2;

      dm.tblTenderBROJ.Value:=dmPQ.zemiRezultat5Str(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_OUT');
      dm.tblTenderBROJ_PART1.Value:=dmPQ.zemiRezultat5(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_PART1');

      GRUPA_1.SetFocus;
   end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmJavnaNabavka.aAzurirajExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse])  and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false) then
begin
// if dm.tblTenderZATVOREN.Value = -1  then
  // begin
    cxPageControlTabelarenDetalen.ActivePage:=cxTabSheetDetalen;
    cxTabSheetTabelaren.Enabled:=False;

    if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetOsnovniPodatoci) then
       begin
         cxGrid1DBTableView1.DataController.DataSet.Edit;
         dm.tblTenderInfo.Edit;
         PanelOsnovniPodatoci.Enabled:=true;
         BROJ.SetFocus;
         ponisten_pred:=dm.tblTenderPONISTEN.Value;
         vrska_pred:=dm.tblTenderVRSKA_PREDMET.Value;
         azuriraj:=1;
       end;
    if cxPageControlDetalenPodelen.ActivePage = cxTabSheetPostapka then
       begin
         cxGrid1DBTableView1.DataController.DataSet.Edit;
         dm.tblTenderInfo.Edit;
         PanelPostapka.Enabled:=true;
         TIP_TENDER.SetFocus;
       end;
     if cxPageControlDetalenPodelen.ActivePage = cxTabSheetFinansiskiTehnicki then
       begin
         dm.tblTenderInfo.Edit;
         PanelFinansiski.Enabled:=true;
         GARANCIJA_PONUDA.SetFocus;
       end;
    if cxPageControlDetalenPodelen.ActivePage = cxTabSheetAdministrativni then
       begin
         dm.tblTenderInfo.Edit;
         PanelAdministrativni.Enabled:=true;
         CENA_DOKUMENTACIJA.SetFocus;
       end;
    if cxPageControlDetalenPodelen.ActivePage = cxTabSheetOdlukiSoglasnosti then
       begin
         dm.tblTenderInfo.Edit;
         PanelOdluki.Enabled:=True;
         ODL_NABAVKA_BR.SetFocus;
       end;

     if cxPageControlDetalenPodelen.ActivePage = cxTabSheetKriteriumBodirawe then
       begin
         cxPageControlDetalenPodelen.ActivePage:=cxTabSheetOsnovniPodatoci;
         cxGrid1DBTableView1.DataController.DataSet.Edit;
         dm.tblTenderInfo.Edit;
         PanelOsnovniPodatoci.Enabled:=true;
         BROJ.SetFocus;
       end;
  // end
 // else  ShowMessage('�� � ��������� ��������� �� �������� ������� �� � �� ������ �� ���������� !');
end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmJavnaNabavka.aBaranjaIsporakaExecute(Sender: TObject);
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
         (cxGrid1DBTableView1.Controller.SelectedRecordCount <> 0))  then
        begin
          frmBaranja:=TfrmBaranja.Create(self,false);
          frmBaranja.Tag:=1;
          frmBaranja.ShowModal;
          frmBaranja.Free;
        end;
end;

procedure TfrmJavnaNabavka.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmJavnaNabavka.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmJavnaNabavka.aRealizacijaPlanNabavkaReExecute(Sender: TObject);
begin
     frmRealizacijaJN:=TfrmRealizacijaJN.Create(Application);
     frmRealizacijaJN.ShowModal;
     frmRealizacijaJN.Free;
end;

procedure TfrmJavnaNabavka.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmJavnaNabavka.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmJavnaNabavka.aIzvadiKomisijaExecute(Sender: TObject);
begin
    if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse])and (PanelMaxPoeni.Visible = false)then
       begin
         if dm.tblTenderZATVOREN.Value = -1 then
            begin
              if ((cxGridDBTableView1.DataController.DataSource.State = dsBrowse) and (cxGridDBTableView1.DataController.RecordCount <> 0)) then
                 cxGridDBTableView1.DataController.DataSet.Delete();
            end
         else ShowMessage('������ �� �������� ������� ������� ���������� �� � �� ������ �� ���������� !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
    cxGrid3.SetFocus;
end;

procedure TfrmJavnaNabavka.aIzvadiPotrebenDokumentExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse])and (PanelMaxPoeni.Visible = false)then
       begin
         if dm.tblTenderZATVOREN.Value = -1 then
            begin
              if ((cxGridDBTableView2.DataController.DataSource.State = dsBrowse) and (cxGridDBTableView2.DataController.RecordCount <> 0)) then
               cxGridDBTableView2.DataController.DataSet.Delete();
            end
         else ShowMessage('������ �� �������� ��������� �� ���������� �� ����������� ������� ���������� �� � �� ������ �� ���������� !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
    cxGrid4.SetFocus;
end;

procedure TfrmJavnaNabavka.aIzvadiStarExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse])and (PanelMaxPoeni.Visible = false)then
       begin
//         dmPQ.qCountPonudiStavkiZaTender.Close;
//         dmPQ.qCountPonudiStavkiZaTender.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
//         dmPQ.qCountPonudiStavkiZaTender.ExecQuery;
//         if dmPQ.qCountPonudiStavkiZaTender.FldByName['br'].Value=0 then
           if dm.tblTenderZATVOREN.Value = -1 then
            begin
              if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
                   cxGrid2DBTableView1.DataController.DataSet.Delete();
            end
         else ShowMessage('������ �� �������� ���������� �� �������� ������� ���������� �� � �� ������ �� ���������� !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
    cxGrid2.SetFocus;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmJavnaNabavka.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmJavnaNabavka.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '����� �������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmJavnaNabavka.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          if Sender = MAX_BODOVI then
             begin
               cxPageControlTabelarenDetalen.Enabled:=true;
               aZapisiKriteriumPanel.Execute();
             end;
          if Sender = FUNKCIJA then
            begin
               cxPageControlTabelarenDetalen.Enabled:=true;
               aZapisiKomisija.Execute();
            end
          else
             PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if Sender = CLEN then
             begin
                frmKomisija:=TfrmKomisija.Create(self,false);
                frmKomisija.ShowModal;
                if (frmKomisija.ModalResult = mrOK) then
                  begin
                    dm.tblTenderKomisijaCLEN.Value := strToInt(frmKomisija.GetSifra(0));
                  end;
                frmKomisija.Free;
                CLEN.SetFocus;
             end;
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmJavnaNabavka.cxDBRadioGroupGRUPA_1Exit(Sender: TObject);
begin
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       GRUPA_OPIS_1.Clear;
       GRUPA_SIFRA_1.SetFocus;
       GRUPA_SIFRA_1.Clear;
     end;
end;

procedure TfrmJavnaNabavka.cxDBRadioGroupVIDExit(Sender: TObject);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       GRUPA_OPIS.Clear;
       GRUPA_SIFRA.SetFocus;
       GRUPA_SIFRA.Clear;
     end;
end;

procedure TfrmJavnaNabavka.cxDBRadioGroupVIDPropertiesEditValueChanged(
  Sender: TObject);
begin
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       if dm.tblTenderGRUPA.IsNull then
         begin
           dm.tblVidoviDogovori.ParamByName('vid').Value:='%';
           dm.tblVidoviDogovori.FullRefresh;
         end
       else
         begin
           dm.tblVidoviDogovori.ParamByName('vid').Value:=dm.tblTenderGRUPA.Value;
           dm.tblVidoviDogovori.FullRefresh;
         end;
     end;
end;

procedure TfrmJavnaNabavka.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if(Sender = EMAIL_KONTAKT)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmJavnaNabavka.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
      begin
        if(Sender = EMAIL_KONTAKT)then
          ActivateKeyboardLayout($042F042F, KLF_REORDER);
        if (Sender = GRUPA_OPIS) then
           Begin
             dm.tblVidoviDogovori.Locate('SIFRA;OPIS', VarArrayOf([dm.tblTenderGRUPA_SIFRA.Value, GRUPA_OPIS.Text]), []);
             dm.tblTenderGRUPA.Value:= dm.tblVidoviDogovoriVID.Value;
           End;
        if (Sender = GRUPA_OPIS_1) then
           Begin
             dm.tblVidoviDogovori.Locate('SIFRA;OPIS', VarArrayOf([dm.tblTenderGRUPA_SIFRA.Value, GRUPA_OPIS_1.Text]), []);
             dm.tblTenderGRUPA.Value:= dm.tblVidoviDogovoriVID.Value;
           End;
      end;
end;

procedure TfrmJavnaNabavka.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmJavnaNabavka.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmJavnaNabavka.ZABELESKA_ZAPISNIKDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoZABELESKA_ZAPISNIK, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoZABELESKA_ZAPISNIK as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmJavnaNabavka.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmJavnaNabavka.GODINAPropertiesEditValueChanged(Sender: TObject);
begin
     if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
      begin
        dm.tblTenderBROJ.Value:=dmPQ.zemiRezultat5Str(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_OUT');
        dm.tblTenderBROJ_PART1.Value:=dmPQ.zemiRezultat5(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_PART1');
      end;
end;

procedure TfrmJavnaNabavka.GODINA_1PropertiesEditValueChanged(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
      begin
        dm.tblTenderBROJ.Value:=dmPQ.zemiRezultat5Str(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_OUT');
        dm.tblTenderBROJ_PART1.Value:=dmPQ.zemiRezultat5(dmPQ.pBrojPostapka,'TAG', 'GODINA', 'BROJ_IN', 'BROJ_PART1_IN',Null,1,dm.tblTenderGODINA.Value,null,null, null,'BROJ_PART1');
      end;

end;

procedure TfrmJavnaNabavka.INFO_AUKCIJADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoINFO_AUKCIJA, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoINFO_AUKCIJA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.INFO_PLAKJANJEDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoINFO_PLAKJANJE, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoINFO_PLAKJANJE as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.N15Click(Sender: TObject);
begin
  dmRes.Spremi('JN',40041);
  dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.N23Click(Sender: TObject);
begin
        dmRes.Spremi('JN',40062);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.N24Click(Sender: TObject);
begin
        dmRes.Spremi('JN',40061);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.N25Click(Sender: TObject);
begin
     dmRes.Spremi('JN',40060);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.OBRAZLOZENIE_POSTAPKADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTender.CreateBlobStream(dm.tblTenderOBRAZLOZENIE_POSTAPKA , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderOBRAZLOZENIE_POSTAPKA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.OPISDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTender.CreateBlobStream(dm.tblTenderOPIS , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderOPIS as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.PREDMET_NABAVKADblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblTenderPREDMET_NABAVKA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblTenderPREDMET_NABAVKA.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.PREDMET_NABAVKA_1DblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblTenderPREDMET_NABAVKA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblTenderPREDMET_NABAVKA.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmJavnaNabavka.prefrli;
begin
end;

procedure TfrmJavnaNabavka.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(PanelOsnovniPodatoci) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dm.tblVidoviDogovori.ParamByName('vid').Value:='%';
  dm.tblVidoviDogovori.FullRefresh;
end;
procedure TfrmJavnaNabavka.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

end;

//------------------------------------------------------------------------------

procedure TfrmJavnaNabavka.FormShow(Sender: TObject);
var pom:string;
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

    PanelVnesOsnovniPodatoci.Top:=301;
    PanelVnesOsnovniPodatoci.left:=301;

    PanelVnesiKomisija.Top:=301;
    PanelVnesiKomisija.Left:=291;

    cxPageControlTabelarenDetalen.ActivePage:=cxTabSheetTabelaren;
    cxPageControlDetalenPodelen.ActivePage:=cxTabSheetOsnovniPodatoci;

    cbStatus.Text:='�������';

    dm.tblTipNabavkaAktivni.Close;
    dm.tblTipNabavkaAktivni.Open;

    dmReport.KomisijaZaJavnaNabavka.open;

    dxComponentPrinter1Link1.ReportTitle.Text := '����� �������';
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    qPromenetiCeni.Close;
    qPromenetiCeni.Open;
    if not qPromenetiCeni.IsEmpty then

      pnlPromenetiCeni.Visible:=true
    else pnlPromenetiCeni.Visible:=false;
    cxGrid1.SetFocus;
end;
//------------------------------------------------------------------------------

procedure TfrmJavnaNabavka.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmJavnaNabavka.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmJavnaNabavka.cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
     if cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen then
     if cxPageControlDetalenPodelen.ActivePage = cxTabSheetKriteriumBodirawe then
        begin
           if AValue > 100 then
              begin
                ShowMessage('������ �� �������� ��������� 100 !!!');
              end;
        end;
end;

procedure TfrmJavnaNabavka.cxGridDBTableView1PRISUTENPropertiesChange(
  Sender: TObject);
begin
     dm.tblTenderKomisija.Edit;
     dm.tblTenderKomisija.Post;
end;

procedure TfrmJavnaNabavka.cxPageControlDetalenPodelenPageChanging(
  Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
      if NewPage = cxTabSheetPotrebniDokumenti then
        begin
         if dm.tblTenderPotrebniDok.State = dsInactive then
            begin
              dm.tblTenderPotrebniDok.Close;
              dm.tblTenderPotrebniDok.Open;
            end;
        end
      else if NewPage = cxTabSheetKomisija then
        begin
         if dm.tblTenderKomisija.State = dsInactive then
            begin
              dm.tblTenderKomisija.Close;
              dm.tblTenderKomisija.Open;
            end;
        end
      else if NewPage = cxTabSheetKriteriumBodirawe then
        begin
         if dm.tblTenderGenBodiranje.State = dsInactive then
            begin
              dm.tblTenderGenBodiranje.Close;
              dm.tblTenderGenBodiranje.Open;
            end;
        end

end;

procedure TfrmJavnaNabavka.cxPageControlTabelarenDetalenPageChanging(
  Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
     if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetTabelaren) then
        begin
          cxTabSheetDetalen.Caption:='������� ������ �� ����� �������: ������ - '+intToStr(dm.tblTenderGODINA.Value)+', ��Σ - ' + dm.tblTenderBROJ.Value;
        end
     else
        begin
           cxTabSheetDetalen.Caption:='������� ������';
           //cxGrid1.SetFocus;
        end;
end;

procedure TfrmJavnaNabavka.DOPOLNITELNI_INFODblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoDOPOLNITELNI_INFO, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoDOPOLNITELNI_INFO as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.dxBarButton25Click(Sender: TObject);
begin
      try
        dmRes.Spremi('JN',40060);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.dxBarButton26Click(Sender: TObject);
begin
    try
    if (dm.tblTenderGRUPA.Value = 1)or (dm.tblTenderGRUPA.Value = 3) then
      begin
        dmRes.Spremi('JN',40061);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
      end
    else if (dm.tblTenderGRUPA.Value = 2)then
      begin
        dmRes.Spremi('JN',40062);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
      end;
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.dxBarButton34Click(Sender: TObject);
begin
  dm.qZatvoriPonudaStatus.Close;
  dm.qZatvoriPonudaStatus.ParamByName('tender_broj').Value:=dm.tblTenderBROJ.Value;
  dm.qZatvoriPonudaStatus.ExecQuery;


  if (dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].Value = 1) then
     begin
        //if (dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_user'].Value = dmKon.user) then
           //begin
              frmDaNe := TfrmDaNe.Create(self, '�������� �� ������', '���� ��������� ������ �� ��������� ������� �� �������� �� ���� �������� ?', 1);
              if (frmDaNe.ShowModal <> mrYes) then
                 Abort
              else
                begin
                   dm.tblTender.Edit;
                   dm.tblTenderZATVORI_PONUDI_STATUS.Value:=0;
                   dm.tblTenderZATVORI_PONUDI_TIME.Value:=Now;
                   dm.tblTenderZATVORI_PONUDI_USER.Value:=dmKon.user;
                   dm.tblTender.Post;
                end;
           //end
        //else ShowMessage('��� ������ ���� �� �� ������� ���� ���������� '+dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_user'].Value);
     end;
end;

procedure TfrmJavnaNabavka.dxBarButton35Click(Sender: TObject);
begin
  dm.qZatvoriPonudaStatus.Close;
  dm.qZatvoriPonudaStatus.ParamByName('tender_broj').Value:=dm.tblTenderBROJ.Value;
  dm.qZatvoriPonudaStatus.ExecQuery;

  if ((dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].Value = 0)or (dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].IsNull)) then
     begin
        frmDaNe := TfrmDaNe.Create(self, '��������� �� ������', '���� ��������� ������ �� ��������� ������� �� �������� �� ���� �������� ?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
            Abort
        else
         begin
           dm.tblTender.Edit;
           dm.tblTenderZATVORI_PONUDI_STATUS.Value:=1;
           dm.tblTenderZATVORI_PONUDI_TIME.Value:=Now;
           dm.tblTenderZATVORI_PONUDI_USER.Value:=dmKon.user;
           dm.tblTender.Post;
         end;
     end;
end;

procedure TfrmJavnaNabavka.dxBarLargeButton18Click(Sender: TObject);
begin
     
end;

//  ����� �� �����
procedure TfrmJavnaNabavka.aZapisiExecute(Sender: TObject);
var
  st, stInfo: TDataSetState;
  broj:string;
begin
     if (dm.tblTenderInfo.State in [dsInsert, dsEdit]) or (dm.tblTender.State in [dsInsert, dsEdit]) then

     if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and(cxPageControlDetalenPodelen.ActivePage = cxTabSheetOdlukiSoglasnosti) then
        begin
          if (Validacija(PanelOdluki) = false) then
             begin
                broj:=dm.tblTenderBROJ.Value;
                dm.tblTenderInfo.Post;
                dm.tblTender.FullRefresh;
                PanelOdluki.Enabled:=False;
                cxTabSheetTabelaren.Enabled:=true;
                dm.tblTender.Locate('BROJ', broj, []);
             end;
        end
     else  if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetAdministrativni) then
        begin
           if (Validacija(PanelAdministrativni) = false) then
              begin
                 broj:=dm.tblTenderBROJ.Value;
                 dm.tblTenderInfo.Post;
                 dm.tblTender.FullRefresh;
                 PanelAdministrativni.Enabled:=False;
                 cxTabSheetTabelaren.Enabled:=true;
                 dm.tblTender.Locate('BROJ', broj, []);
              end
        end
     else  if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetFinansiskiTehnicki) then
        begin
           if (Validacija(PanelFinansiski) = false) then
              begin
                 broj:=dm.tblTenderBROJ.Value;
                 dm.tblTenderInfo.Post;
                 dm.tblTender.FullRefresh;
                 PanelFinansiski.Enabled:=False;
                 cxTabSheetTabelaren.Enabled:=true;
                 dm.tblTender.Locate('BROJ', broj, []);
              end
        end
     else  if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetPostapka) then
        begin
           if (Validacija(PanelPostapka) = false) then
              begin
                  broj:=dm.tblTenderBROJ.Value;
                  dm.tblTenderInfo.Post;
                  cxGrid1DBTableView1.DataController.DataSet.Post;
                  dm.tblTender.FullRefresh;
                  PanelPostapka.Enabled:=False;
                  cxTabSheetTabelaren.Enabled:=true;
                  dm.tblTender.Locate('BROJ', broj, []);
              end
        end
      else  if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetOsnovniPodatoci) then
        begin
           if (Validacija(PanelOsnovniPodatoci) = false) then
              begin
                  broj:=dm.tblTenderBROJ.Value;
                  if azuriraj = 1 then
                    begin
                      ponisten_potoa:=dm.tblTenderPONISTEN.Value;
                      vrska_potoa:=dm.tblTenderVRSKA_PREDMET.Value;
                      if ({((ponisten_potoa = 1) and (ponisten_pred = 0)and (vrska_pred <> null))
                          or} ((vrska_pred <> vrska_potoa) {and(ponisten_potoa = 0)and(ponisten_pred = 0)}and (vrska_pred <> null))) then
                            begin
                              dm.qCountStavkiTender.Close;
                              dm.qCountStavkiTender.ParamByName('broj').Value:=vrska_pred;
                              dm.qCountStavkiTender.ExecQuery;
                              if dm.qCountStavkiTender.FldByName['br'].Value >0 then
                                begin
                                  ShowMessage('������ ��������� �� �������� �� �������� �� ��� '+vrska_pred);
                                  dm.tblTenderVRSKA_PREDMET.Value:=vrska_pred;
                                end;
                            end;
                      azuriraj:=0;
                    end;
                 dm.tblTenderInfo.Post;
                 cxGrid1DBTableView1.DataController.DataSet.Post;

                 if cbStatus.Text<> '' then
                    begin
                       if dm.tblTenderZATVOREN.Value = -1 then
                          cbStatus.Text:='�� ����������'
                       else if dm.tblTenderZATVOREN.Value = 0 then
                          cbStatus.Text:='�������'
                       else if dm.tblTenderZATVOREN.Value = 1 then
                          cbStatus.Text:='��������'
                       else if dm.tblTenderZATVOREN.Value = 2 then
                          cbStatus.Text:='������� �������';
                     end;
                 dm.tblTender.FullRefresh;

                 PanelOsnovniPodatoci.Enabled:=False;
                 cxTabSheetTabelaren.Enabled:=true;
                 dm.tblTender.Locate('BROJ', broj, []);
              end
        end;
   cxTabSheetDetalen.Caption:='������� ������ �� ����� �������: ������ - '+intToStr(dm.tblTenderGODINA.Value)+', ��Σ - ' + dm.tblTenderBROJ.Value
end;

procedure TfrmJavnaNabavka.aZapisiKomisijaExecute(Sender: TObject);
begin
     if Validacija(PanelVnesiKomisija) = false then
        begin
           dm.tblTenderKomisija.post;
           PanelVnesiKomisija.Visible:=false;
           cxGrid3.SetFocus;
        end;
end;

procedure TfrmJavnaNabavka.aZapisiKriteriumPanelExecute(Sender: TObject);
begin
     if Validacija(PanelMaxPoeni) = false then
        begin
          if not dm.tbltenderGenBodiranjeBODIRANJE_PO.isNull then
             begin
              dm.tbltenderGenBodiranjeBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
              dm.tbltenderGenBodiranje.post;
              PanelMaxPoeni.Visible:=false;
              cxGrid2.SetFocus;
             end;
        end;
end;

procedure TfrmJavnaNabavka.aPZapisnikOtvoranjePonudiExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40022);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.ZapisnikOtvoranjePonudi.Close;
    dmReport.ZapisnikOtvoranjePonudi.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.ZapisnikOtvoranjePonudi.Open;

    dmReport.KomisijaZaJavnaNabavka.Close;
    dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.KomisijaZaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.cbStatusChange(Sender: TObject);
begin
    dm.tblTender.Close;
    if cbStatus.Text = '�� ����������' then
       dm.tblTender.ParamByName('status').Value:=-1
    else if cbStatus.Text = '�������' then
       dm.tblTender.ParamByName('status').Value:=0
    else if cbStatus.Text = '��������' then
       dm.tblTender.ParamByName('status').Value:=1
    else if cbStatus.Text = '������� �������' then
       dm.tblTender.ParamByName('status').Value:=2
    else if cbStatus.Text = '' then
       dm.tblTender.ParamByName('status').Value:='%';
    dm.tbltender.Open;

    dm.tblTenderInfo.Close;
    dm.tblTenderInfo.Open;
    dm.tblTenderGenBodiranje.Close;
    dm.tblTenderGenBodiranje.Open;
    dm.tblTenderKomisija.Close;
    dm.tblTenderKomisija.Open;
    dm.tblTenderPotrebniDok.Close;
    dm.tblTenderPotrebniDok.Open;
end;

//	����� �� ���������� �� �������
procedure TfrmJavnaNabavka.aOdlukaZaNabavkaExecute(Sender: TObject);
begin
if dm.tblTenderGRUPA.Value = 1 then
  begin
   try
    dmRes.Spremi('JN',40020);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end
  end

  else  if (dm.tblTenderGRUPA.Value = 3) then
   begin
    try
      dmRes.Spremi('JN',40038);
      dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
      dmKon.tblSqlReport.Open;

      dmReport.OdlukaJavnaNabavka.Close;
      dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
      dmReport.OdlukaJavnaNabavka.Open;

      dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
   end
   else  if (dm.tblTenderGRUPA.Value = 2) then
   begin
    try
      dmRes.Spremi('JN',40046);
      dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
      dmKon.tblSqlReport.Open;

      dmReport.OdlukaJavnaNabavka.Close;
      dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
      dmReport.OdlukaJavnaNabavka.Open;

      dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
      dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
   end;
end;

procedure TfrmJavnaNabavka.aOglasJNExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40021);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'kategorija_do_organ', QuotedStr(kategorija_do_organ));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aOsnovniProdolziExecute(Sender: TObject);
 var broj:string;
begin
     if PanelVnesOsnovniPodatoci.Visible = true then
        begin
           if (Validacija(PanelVnesOsnovniPodatoci) = false) then
               begin
                  cxGrid1DBTableView1.DataController.DataSet.Post;

                  if cbStatus.Text<> '' then
                     begin
                        if dm.tblTenderZATVOREN.Value = -1 then
                           cbStatus.Text:='�� ����������'
                        else if dm.tblTenderZATVOREN.Value = 0 then
                           cbStatus.Text:='�������'
                        else if dm.tblTenderZATVOREN.Value = 1 then
                           cbStatus.Text:='��������'
                        else if dm.tblTenderZATVOREN.Value = 2 then
                           cbStatus.Text:='������� �������';
                      end;

                  PanelVnesOsnovniPodatoci.Visible:=False;
                  cxPageControlTabelarenDetalen.Enabled:=True;
                  cxPageControlTabelarenDetalen.ActivePage:=cxTabSheetDetalen;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetOsnovniPodatoci;
                  cxTabSheetTabelaren.Enabled:=false;
                  PanelOsnovniPodatoci.Enabled:=true;

                  cxGrid1DBTableView1.DataController.DataSet.Edit;
                  dm.tblTenderInfo.Edit;

                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_KONTAKT'].IsNull) then
                     dm.tblTenderLICE_KONTAKT.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_KONTAKT'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['EMAIL_KONTAKT'].IsNull) then
                     dm.tblTenderEMAIL_KONTAKT.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['EMAIL_KONTAKT'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['KONTAKT_TEL_FAKS'].IsNull) then
                     dm.tblTenderKONTAKT_TEL_FAKS.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['KONTAKT_TEL_FAKS'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_REALIZACIJA'].IsNull) then
                     dm.tblTenderLICE_REALIZACIJA.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_REALIZACIJA'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_PODGOTOVKA'].IsNull) then
                     dm.tblTenderLICE_PODGOTOVKA.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['LICE_PODGOTOVKA'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['LOKACIJA_ISPORAKA'].IsNull) then
                     dm.tblTenderInfoLOKACIJA_ISPORAKA.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['LOKACIJA_ISPORAKA'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['mesto_otvaranje'].IsNull) then
                     dm.tblTenderInfoMESTO_OTVARANJE.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['mesto_otvaranje'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['IZVOR_SREDSTVA'].IsNull) then
                     dm.tblTenderInfoIZVOR_SREDSTVA.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['IZVOR_SREDSTVA'].Value;
                  if (not dm.qZemiPovtoruvackiPodatoci.FldByName['VREMETRAENJE_DOG'].IsNull) then
                     dm.tblTenderInfoVREMETRAENJE_DOG.Value:=dm.qZemiPovtoruvackiPodatoci.FldByName['VREMETRAENJE_DOG'].Value;
                  INTEREN_BROJ.SetFocus;
               end;
        end
       else if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetOsnovniPodatoci)and(PanelOsnovniPodatoci.Enabled = true) then
               begin
                 if azuriraj = 1 then
                    begin
                      ponisten_potoa:=dm.tblTenderPONISTEN.Value;
                      vrska_potoa:=dm.tblTenderVRSKA_PREDMET.Value;
                      if ({((ponisten_potoa = 1) and (ponisten_pred = 0) and (vrska_pred <> null))
                          or }((vrska_pred <> vrska_potoa) {and(ponisten_potoa = 0)and(ponisten_pred = 0)}and (vrska_pred <> null))) then
                            begin
                              dm.qCountStavkiTender.Close;
                              dm.qCountStavkiTender.ParamByName('broj').Value:=vrska_pred;
                              dm.qCountStavkiTender.ExecQuery;
                              if dm.qCountStavkiTender.FldByName['br'].Value >0 then
                                begin
                                  ShowMessage('������ ��������� �� �������� �� �������� �� ��� '+vrska_pred);
                                  dm.tblTenderVRSKA_PREDMET.Value:=vrska_pred;
                                end;
                            end;
                      azuriraj:=0;
                    end;

                  dm.tblTenderInfo.Post;
                  cxGrid1DBTableView1.DataController.DataSet.Post;


                  PanelOsnovniPodatoci.Enabled:=false;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetPostapka;
                  PanelPostapka.Enabled:=true;

                  cxGrid1DBTableView1.DataController.DataSet.Edit;
                  dm.tblTenderInfo.Edit;

                  OBRAZLOZENIE_POSTAPKA.SetFocus;
               end
       else if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetPostapka)and(PanelPostapka.Enabled = true) then
               begin
                  dm.tblTenderInfo.Post;
                  cxGrid1DBTableView1.DataController.DataSet.Post;

                  PanelPostapka.Enabled:=false;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetFinansiskiTehnicki;
                  PanelFinansiski.Enabled:=true;

                  dm.tblTenderInfo.Edit;

                  GARANCIJA_PONUDA.SetFocus;
               end
       else if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetFinansiskiTehnicki)and(PanelFinansiski.Enabled = true) then
               begin
                  dm.tblTenderInfo.Post;

                  PanelFinansiski.Enabled:=false;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetAdministrativni;
                  PanelAdministrativni.Enabled:=true;

                  dm.tblTenderInfo.Edit;

                  CENA_DOKUMENTACIJA.SetFocus;
               end
       else if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetAdministrativni)and(PanelAdministrativni.Enabled = true) then
               begin
                  dm.tblTenderInfo.Post;

                  PanelAdministrativni.Enabled:=false;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetOdlukiSoglasnosti;
                  PanelOdluki.Enabled:=true;

                  dm.tblTenderInfo.Edit;

                  ODL_NABAVKA_BR.SetFocus;
               end
        else if (cxPageControlDetalenPodelen.ActivePage = cxTabSheetOdlukiSoglasnosti)and(PanelOdluki.Enabled = true) then
               begin
                  broj:=dm.tblTenderBROJ.Value;
                  dm.tblTenderInfo.Post;
                  dm.tblTender.FullRefresh;
                  dm.tblTender.Locate('BROJ', broj, []);


                  PanelOdluki.Enabled:=false;
                  cxPageControlDetalenPodelen.ActivePage:=cxTabSheetKriteriumBodirawe;
                  cxTabSheetTabelaren.Enabled:=true;

                  cxGrid2.SetFocus;
               end;
end;

procedure TfrmJavnaNabavka.aOtkaziExecute(Sender: TObject);
begin
       if pnlPromenetiCeni.Visible = true then
             begin

               pnlPromenetiCeni.Visible:=false;
             end
          else
          if PanelVnesOsnovniPodatoci.Visible = true then
             begin
               cxGrid1DBTableView1.DataController.DataSet.Cancel;
               cxPageControlTabelarenDetalen.Enabled:=True;
               PanelVnesOsnovniPodatoci.Visible:=false;
             end
          else
            if PanelMaxPoeni.Visible = true then
             begin
                dm.tblTenderGenBodiranje.Cancel;
                PanelMaxPoeni.Visible:=false;
                cxPageControlTabelarenDetalen.Enabled:=true;
                cxGrid2.SetFocus;
             end
             else
               if PanelVnesiKomisija.Visible = true then
               begin
                  dm.tblTenderKomisija.Cancel;
                  PanelVnesiKomisija.Visible:=false;
                  cxPageControlTabelarenDetalen.Enabled:=true;
                  cxGrid3.SetFocus;
               end

              else
                if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State = dsBrowse)then
                 begin
                    ModalResult := mrCancel;
                    Close();
                 end
                 else
                  if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetOsnovniPodatoci) then
                  begin
                     cxGrid1DBTableView1.DataController.DataSet.Cancel;
                     dm.tblTenderInfo.Cancel;
                     RestoreControls(PanelOsnovniPodatoci);
                     PanelOsnovniPodatoci.Enabled:=False;
                     cxTabSheetTabelaren.Enabled:=True;
                  end
                  else
                    if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetPostapka) then
                    begin
                       cxGrid1DBTableView1.DataController.DataSet.Cancel;
                       dm.tblTenderInfo.Cancel;
                       RestoreControls(PanelPostapka);
                       PanelPostapka.Enabled:=False;
                       cxTabSheetTabelaren.Enabled:=True;
                    end
                    else
                      if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetFinansiskiTehnicki) then
                      begin
                         dm.tblTenderInfo.Cancel;
                         RestoreControls(PanelFinansiski);
                         PanelFinansiski.Enabled:=False;
                         cxTabSheetTabelaren.Enabled:=true;
                      end
                      else
                        if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and (cxPageControlDetalenPodelen.ActivePage = cxTabSheetAdministrativni) then
                        begin
                           dm.tblTenderInfo.Cancel;
                           RestoreControls(PanelAdministrativni);
                           PanelAdministrativni.Enabled:=False;
                           cxTabSheetTabelaren.Enabled:=true;
                        end
                        else
                          if (cxPageControlTabelarenDetalen.ActivePage = cxTabSheetDetalen)and(cxPageControlDetalenPodelen.ActivePage = cxTabSheetOdlukiSoglasnosti) then
                              begin
                                 dm.tblTenderInfo.Cancel;
                                 RestoreControls(PanelOdluki);
                                 PanelOdluki.Enabled:=False;
                                 cxTabSheetTabelaren.Enabled:=true;
                              end;
      dm.tblTender.FullRefresh;
      cxTabSheetDetalen.Caption:='������� ������ �� ����� �������: ������ - '+intToStr(dm.tblTenderGODINA.Value)+', ��Σ - ' + dm.tblTenderBROJ.Value
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmJavnaNabavka.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmJavnaNabavka.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + cbStatus.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmJavnaNabavka.aPIznosNaDogovorPoPartnerGddiniExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40045);
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPIzvestajSprovedenaPostapkaExecute(Sender: TObject);
begin
  inherited;
   try

    dm.tblTenderDokumenti.Close;
    dm.tblTenderDokumenti.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dm.tblTenderDokumenti.Open;

    dmReport.PonudiPredmetNaEvaluacija.Close;
    dmReport.PonudiPredmetNaEvaluacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.PonudiPredmetNaEvaluacija.open;

    dmReport.IzborNajpovolnaPonudaPoGrupi.Close;
    dmReport.IzborNajpovolnaPonudaPoGrupi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.IzborNajpovolnaPonudaPoGrupi.Open;

    dmReport.KomisijaZaJavnaNabavka.Close;
    dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.KomisijaZaJavnaNabavka.open;

    dmReport.TenderskaDokumentacija.Close;
    dmReport.TenderskaDokumentacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.TenderskaDokumentacija.open;

    dmReport.tblEmptyTable.Close;
    dmReport.ObrazlozenieNeprifateniPonudi.Close;
    dmReport.ObrazlozenieNeprifateniPonudi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.ObrazlozenieNeprifateniPonudi.Open;

    if dmReport.ObrazlozenieNeprifateniPonudi.IsEmpty then
       dmReport.tblEmptyTable.Open
    else
       dmReport.tblEmptyTable.Close;

    dmReport.FinansiskaPonuda_1.Close;
    dmReport.FinansiskaPonuda_1.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.FinansiskaPonuda_1.Open;

    dmReport.PokanetiPonuduvaci.Close;
    dmReport.PokanetiPonuduvaci.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.PokanetiPonuduvaci.Open;

    dmReport.tblDobitnici.Close;
    dmReport.tblDobitnici.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.tblDobitnici.Open;

    dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:='%';
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:='%';
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

    dmReport.KriteriumiUtvrduvanjeSposobnost.Close;
    dmReport.KriteriumiUtvrduvanjeSposobnost.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmReport.PocetnaRangListaNedeliviGrupi.Close;
    dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.PocetnaRangListaNedeliviGrupi.Open;

    dmReport.tblPocetnaRangLista.Close;
    dmReport.tblPocetnaRangLista.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.tblPocetnaRangLista.Open;

    if dm.tblTenderTIP_OCENA.Value = 1 then
       dmRes.Spremi('JN',40019)
    else //if dm.tblTenderTIP_OCENA.Value = 0 then
       dmRes.Spremi('JN',40041);

    //dmRes.Spremi('JN',40019);
    //dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    //dmKon.tblSqlReport.Open;
    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmJavnaNabavka.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmJavnaNabavka.aPPokanaKonecnaCenaExecute(Sender: TObject);
begin
          dmRes.Spremi('JN',40051);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupiEDP.Close;
          dmReport.PocetnaRangListaNedeliviGrupiEDP.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupiEDP.Open;

          dmReport.OdlukaJavnaNabavka.Close;
          dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
          dmReport.OdlukaJavnaNabavka.Open;

          dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
end;

procedure TfrmJavnaNabavka.aPonudiExecute(Sender: TObject);
begin
     frmPonudi:=TfrmPonudi.Create(Self, false);
     frmPonudi.ShowModal;
     frmPonudi.Free;
end;

procedure TfrmJavnaNabavka.aPopUpMenuExecute(Sender: TObject);
begin
      PopupMenu2.Popup(500,500 );
end;

procedure TfrmJavnaNabavka.aPPocetnaRangListaPoKriteriumiExecute(
  Sender: TObject);
begin
  try
          dmRes.Spremi('JN',40050);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupiKriterium.Close;
          dmReport.PocetnaRangListaNedeliviGrupiKriterium.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupiKriterium.Open;

          dmReport.OdlukaJavnaNabavka.Close;
          dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
          dmReport.OdlukaJavnaNabavka.Open;

          dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();


  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPPocetnaRangListaZaUcestvoNaEAukcijaExecute(
  Sender: TObject);
begin
try
    if dm.tblTenderTIP_OCENA.Value = 1 then
       begin
          dmRes.Spremi('JN',40032);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupi.Close;
          dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupi.Open;

          dmReport.OdlukaJavnaNabavka.Close;
          dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
          dmReport.OdlukaJavnaNabavka.Open;

          dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
       end
    else if ((dm.tblTenderTIP_OCENA.Value = 0)or (dm.tblTenderTIP_OCENA.Value = 2)or (dm.tblTenderTIP_OCENA.Value = 3)) then
       begin
          dmRes.Spremi('JN',40033);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupi.Close;
          dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupi.Open;

          dmReport.OdlukaJavnaNabavka.Close;
          dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
          dmReport.OdlukaJavnaNabavka.Open;

          dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
       end
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPRealizacijaJNExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40025);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPRealizacijaPlanNabavkaPoREExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40026);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPSpecifikacijaJavnaNabavkaExecute(Sender: TObject);
begin
 try
    if (dm.tblTenderGRUPA.Value = 1)or (dm.tblTenderGRUPA.Value = 3) then
      begin
        dmRes.Spremi('JN',40004);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
      end
    else if (dm.tblTenderGRUPA.Value = 2)then
      begin
        dmRes.Spremi('JN',40047);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
      end;
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPSpecifikacijaPoArtikliExecute(Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40006);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPSpecifikacijaPoSektoriExecute(Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40005);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmJavnaNabavka.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmJavnaNabavka.actDNevoobicaenaNiskaCenaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40333);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.actDOdlukaZaPonistuvanjePostapkaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40044);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.actDSpecifikacijaPoSektoriSoPlanExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40171);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.actDSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40170);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.actExportToExcelOdlukaNajpovolnaExecute(
  Sender: TObject);
begin
     tblIzborNajpovolnaPonudaCena.Close;
     tblIzborNajpovolnaPonudaCena.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
     tblIzborNajpovolnaPonudaCena.Open;

     zacuvajVoExcel(cxGrid6, '����� �������');
end;

procedure TfrmJavnaNabavka.actGodisenPlanJNExecute(Sender: TObject);
begin
      frmGodisenPlanJN:=TfrmGodisenPlanJN.Create(Application);
      frmGodisenPlanJN.ShowModal();
      frmGodisenPlanJN.Free;
end;

procedure TfrmJavnaNabavka.actPNevoobicaenaNiskaCenaExecute(Sender: TObject);
begin
try
   if ((dm.tblTenderTIP_OCENA.Value = 0)or (dm.tblTenderTIP_OCENA.Value = 2)or (dm.tblTenderTIP_OCENA.Value = 3)) then
       begin
          dmRes.Spremi('JN',40333);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupi.Close;
          dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupi.Open;

          dmReport.OdlukaJavnaNabavka.Close;
          dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
          dmReport.OdlukaJavnaNabavka.Open;

          dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
       end
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPDobitniStavkiExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40052);
    dmKon.tblSqlReport.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end
end;

procedure TfrmJavnaNabavka.aDDobitniStavkiExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40052);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.actPOdlukaZaPonistuvanjePostapkaExecute(
  Sender: TObject);
begin
try
    dmPQ.qCountPonisteniStavkiTender.Close;
    dmPQ.qCountPonisteniStavkiTender.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmPQ.qCountPonisteniStavkiTender.ExecQuery;
    if dm.tblTenderPONISTEN.Value = 0 then
       begin
          if (dmPQ.qCountPonisteniStavkiTender.FldByName['br'].Value > 0)then
            begin
                dmRes.Spremi('JN',40044);
                dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
                dmKon.tblSqlReport.Open;

                dmReport.KomisijaZaJavnaNabavka.Close;
                dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
                dmReport.KomisijaZaJavnaNabavka.open;

                dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
                dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
                dmRes.frxReport1.ShowReport();
            end
         else ShowMessage('���� ��������� ������ �� ���������� !!!');
       end
    else ShowMessage('���������� �� � ��������� !!!');
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.actPrevzemiPonisteniStavkiExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
   begin
     //if dm.tblTenderPONISTEN.Value = 0 then
        //begin
          if (not dm.tblTenderVRSKA_PREDMET.IsNull) then
             begin
                 dm.qCountBrTender.Close;
                 dm.qCountBrTender.ParamByName('broj').Value:=dm.tblTenderVRSKA_PREDMET.Value;
                 dm.qCountBrTender.ExecQuery;
                 if (dm.qCountBrTender.FldByName['br'].Value = 1) then
                    begin
                       dm.qCountPonisteniStavki.Close;
                       dm.qCountPonisteniStavki.ParamByName('broj').Value:=dm.tblTenderBROJ.Value;
                       dm.qCountPonisteniStavki.ExecQuery;
                        if (dm.qCountPonisteniStavki.FldByName['br'].Value > 0) then
                           begin
                             dm.qCountStavkiTender.Close;
                             dm.qCountStavkiTender.ParamByName('broj').Value:=dm.tblTenderVRSKA_PREDMET.Value;
                             dm.qCountStavkiTender.ExecQuery;
                             if (dm.qCountStavkiTender.FldByName['br'].Value = 0) then
                                begin
                                  dmpq.insert6(dmPQ.PROC_JN_PREVZEMI_PONISTENI,'BROJ_TENDER', 'BROJ_VRSKA',Null,null,Null,null,dm.tblTenderBROJ.Value, dm.tblTenderVRSKA_PREDMET.Value,Null,null,Null,null);
                                  ShowMessage('������� �������� �� ����������� ������ �� �������� �� ��� '+dm.tblTenderBROJ.Value+' �� �������� �� ��� '+dm.tblTenderVRSKA_PREDMET.Value+' !!!');
                                end
                             else ShowMessage('������ ��������� �� �������� �� ����� �� ������� �� ��� '+dm.tblTenderVRSKA_PREDMET.Value+' !!!');
                           end
                        else ShowMessage('�� ��������� �������� �� ��� '+dm.tblTenderBROJ.Value+' ���� ���� ���� ��������� ������ !!!');
                    end
                 else ShowMessage('��������� ����� �� ������� �� � ������� !!!');
             end
          else ShowMessage('������ ����� �� ������� �� � ��������� !!!');
        //end
     //else ShowMessage('������������� �������� �� � ��������� !!!');
   end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmJavnaNabavka.actPSpecifikacijaPoSektoriSoPlanExecute(
  Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40171);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.actPSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40170);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.act_S_DelumnoZavrsenExecute(Sender: TObject);
begin
     if dm.tblTenderZATVOREN.Value = 2 then
        ShowMessage('���������� � ��� �� ������ ������� ������� !')
     else
        begin
          dm.tblTender.Edit;
          dm.tblTenderZATVOREN.Value:=2;
          dm.tblTender.Post;
          ShowMessage('�������� �� ���������� � �������� �� ������� ������� !');
        end;
end;

procedure TfrmJavnaNabavka.act_S_OtvorenExecute(Sender: TObject);
begin
     if dm.tblTenderZATVOREN.Value = 0 then
        ShowMessage('���������� � ��� �� ������ ������� !')
     else
        begin
          dm.tblTender.Edit;
          dm.tblTenderZATVOREN.Value:=0;
          dm.tblTender.Post;
          ShowMessage('�������� �� ���������� � �������� �� ������� !');
        end;
end;

procedure TfrmJavnaNabavka.act_S_VoPodgotovkaExecute(Sender: TObject);
begin
     if dm.tblTenderZATVOREN.Value = - 1 then
        ShowMessage('���������� � ��� �� ������ �� ���������� !')
     else
        begin
          dm.qCountPonudiPoTender.Close;
          dm.qCountPonudiPoTender.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dm.qCountPonudiPoTender.ExecQuery;
          if dm.qCountPonudiPoTender.FldByName['br'].Value >0 then
             ShowMessage('�� � ��������� ������� �� ������ ���弝� ����� ������������ ������ �� ��������� �������� !')
          else
             begin
               dm.tblTender.Edit;
               dm.tblTenderZATVOREN.Value:=-1;
               dm.tblTender.Post;
               ShowMessage('�������� �� ���������� � �������� �� ���������� !');
             end;
        end;
end;

procedure TfrmJavnaNabavka.act_S_ZatvorenExecute(Sender: TObject);
begin
     if dm.tblTenderZATVOREN.Value = 1 then
        ShowMessage('���������� � ��� �� ������ �������� !')
     else
        begin
          dm.tblTender.Edit;
          dm.tblTenderZATVOREN.Value:=1;
          dm.tblTender.Post;
          ShowMessage('�������� �� ���������� � �������� �� �������� !');
        end;
end;

procedure TfrmJavnaNabavka.aPOdlukaIzborNajpovolnaPonudaCenaExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40036);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;

  dm.qZatvoriPonudaStatus.Close;
  dm.qZatvoriPonudaStatus.ParamByName('tender_broj').Value:=dm.tblTenderBROJ.Value;
  dm.qZatvoriPonudaStatus.ExecQuery;

  if ((dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].Value = 0)or (dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].IsNull)) then
     begin
        frmDaNe := TfrmDaNe.Create(self, '��������� �� ������', '���� ��������� ������ �� �� ��������� ��������� �� �������� �� ���� �������� ?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
            Abort
        else
         begin
           dm.tblTender.Edit;
           dm.tblTenderZATVORI_PONUDI_STATUS.Value:=1;
           dm.tblTenderZATVORI_PONUDI_TIME.Value:=Now;
           dm.tblTenderZATVORI_PONUDI_USER.Value:=dmKon.user;
           dm.tblTender.Post;
         end;
     end;
end;

procedure TfrmJavnaNabavka.aPOdlukaIzborNajpovolnaPonudaExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40023);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPOdlukaJN_SO_CENAExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40054);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end
end;

procedure TfrmJavnaNabavka.aPOdlukaZaGodisenPlanExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40027);
    dmKon.tblSqlReport.ParamByName('godina').Value:=dmKon.godina;
    dmKon.tblSqlReport.ParamByName('param_godina').Value:=0;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aPOdlukaZaPonistuvanjeNaStavkiExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40024);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmJavnaNabavka.aDizajnSpecifikacijaPoArtikliExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40006);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDizajnSpecifikacijaJavnaNabavkaExecute(
  Sender: TObject);
begin
   dmRes.Spremi('JN',40004);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDizajnSpecifikacijaPoSektoriExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40005);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDIznosNaDogovorPoPartnerGddiniExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40045);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDIzvestajSprovedenaPostapkaExecute(Sender: TObject);
begin
  dmRes.Spremi('JN',40019);
  dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDNedobitniStavkiExecute(Sender: TObject);
begin
         dmRes.Spremi('JN',40049);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDobitniciExecute(Sender: TObject);
begin
     frmDobitnici:=TfrmDobitnici.Create(Self, false);
     frmDobitnici.ShowModal;
     frmDobitnici.Free;
end;

procedure TfrmJavnaNabavka.aDodadiKomisijaExecute(Sender: TObject);
begin
    if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
       begin
          if dm.tblTenderZATVOREN.Value = -1 then
            begin
              cxPageControlTabelarenDetalen.Enabled:=false;
              PanelVnesiKomisija.Visible:=true;
              qRedBrClenKomisija.close;
              qRedBrClenKomisija.ParamByName('broj_tender').Value:= dm.tblTenderBROJ.Value;
              qRedBrClenKomisija.ExecQuery;
              dm.tblTenderKomisija.Insert;
              dm.tblTenderKomisijaREDBR.Value:=qRedBrClenKomisija.FldByName['redbr'].Value;
              dm.tblTenderKomisijaBROJ_TENDER.Value:= dm.tblTenderBROJ.Value;
              REDBR.SetFocus;
            end
          else ShowMessage('������ �� �������� ������� ������� ���������� �� � �� ������ �� ���������� !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmJavnaNabavka.aDodadiKriteriumExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
    begin
     //dmPQ.qCountPonudiStavkiZaTender.Close;
     //dmPQ.qCountPonudiStavkiZaTender.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     //dmPQ.qCountPonudiStavkiZaTender.ExecQuery;

     //if dmPQ.qCountPonudiStavkiZaTender.FldByName['br'].Value=0 then
    // if dm.tblTenderZATVOREN.Value = -1 then
     //   begin
          cxPageControlTabelarenDetalen.Enabled:=false;
          dm.tblTenderGenBodiranje.Insert;
          frmKriteriumi:=TfrmKriteriumi.Create(self,false);
          frmKriteriumi.ShowModal;
          if (frmKriteriumi.ModalResult = mrOK) then
             begin
              dm.tbltenderGenBodiranjeBODIRANJE_PO.Value := frmKriteriumi.GetSifra(0);
             end;
          frmKriteriumi.Free;
          if not dm.tbltenderGenBodiranjeBODIRANJE_PO.IsNull then
            begin
              if dm.tbltenderGenBodiranjeBODIRANJE_PO.Value ='01' then
                 begin
                   dm.tblTenderGenBodiranjeMAX_BODOVI.Value:=100;
                   dm.tbltenderGenBodiranjeBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
                   dm.tbltenderGenBodiranje.post;
                   cxPageControlTabelarenDetalen.Enabled:=true;
                   cxGrid2.SetFocus;
                 end
              else
                 begin
                   PanelMaxPoeni.Visible:=true;
                   MAX_BODOVI.SetFocus;
                   dm.tblTenderGenBodiranjeMAX_BODOVI.Value:=100;
                 end;
            end
          else
            begin
              dm.tblTenderGenBodiranje.Cancel;
              PanelMaxPoeni.Visible:=false;
              cxPageControlTabelarenDetalen.Enabled:=true;
              cxGrid2.SetFocus;
            end;
        end
    // else ShowMessage('������ �� �������� ���������� �� �������� ������� ���������� �� � �� ������ �� ���������� !!!');
    //end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmJavnaNabavka.aDodadiPotrebenDokExecute(Sender: TObject);
begin
   if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse]) and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false)then
    begin
      if dm.tblTenderZATVOREN.Value = -1 then
         begin
           dm.tblPotrebniDokumneti.Close;
           dm.tblPotrebniDokumneti.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
           dm.tblPotrebniDokumneti.ParamByName('param').Value:=1;
           dm.tblPotrebniDokumneti.Open;

           frmPotrebniDokumenti:=TfrmPotrebniDokumenti.Create(self,false);
           frmPotrebniDokumenti.Tag:=1;
           frmPotrebniDokumenti.ShowModal;
           if (frmPotrebniDokumenti.ModalResult = mrOK) then
            begin
              dm.tblTenderPotrebniDok.Insert;
              dm.tblTenderPotrebniDokPOTREBNI_DOKUMENTI_ID.Value := StrToInt(frmPotrebniDokumenti.GetSifra(0));
              dm.tblTenderPotrebniDokBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
              dm.tblTenderPotrebniDok.Post;
            end;
           frmPotrebniDokumenti.Free;
           cxGrid4.SetFocus;
         end
      else ShowMessage('������ �� �������� ��������� �� ���������� �� ����������� ������� ���������� �� � �� ������ �� ���������� !!!');
    end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmJavnaNabavka.aDOdlukaIzborNajpovolnaPonudaCenaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40036);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDOdlukaIzborNajpovolnaPonudaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40023);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDOdlukaJN_SO_CENAExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40054);
     dmRes.frxReport1.DesignReport();

end;

procedure TfrmJavnaNabavka.aDOdlukaZaGodisenPlanExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40027);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDOdlukaZaNabavkaExecute(Sender: TObject);
begin
    if (dm.tblTenderGRUPA.Value = 1)  then
     begin
      dmRes.Spremi('JN',40020);
      dmRes.frxReport1.DesignReport();
     end
    else if (dm.tblTenderGRUPA.Value = 3) then
     begin
       dmRes.Spremi('JN',40038);
       dmRes.frxReport1.DesignReport();
     end
    else  if (dm.tblTenderGRUPA.Value = 2) then
     begin
       dmRes.Spremi('JN',40046);
       dmRes.frxReport1.DesignReport();
     end;
end;

procedure TfrmJavnaNabavka.aDOdlukaZaPonistuvanjeNaStavkiExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40024);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDOglasJNExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40021);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDogovoriExecute(Sender: TObject);
begin
     dmpq.qCountPonudiZaTender.Close;
     dmpq.qCountPonudiZaTender.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     dmpq.qCountPonudiZaTender.ExecQuery;
     if dmpq.qCountPonudiZaTender.FldByName['c'].Value > 0 then
        begin
           frmDogovor:=TfrmDogovor.Create(Self, false);
           frmDogovor.ShowModal;
           frmDogovor.Free;
        end
     else
        ShowMessage('�������� �� ������������ ������� �� ��������� ����� �������. ������ ������������ ������ !!!');
end;

procedure TfrmJavnaNabavka.aDokumentiExecute(Sender: TObject);
begin
   frmDokumentiZaJavnaNabavka:=TfrmDokumentiZaJavnaNabavka.Create(Self, false);
   frmDokumentiZaJavnaNabavka.ShowModal;
   frmDokumentiZaJavnaNabavka.Free;
end;

procedure TfrmJavnaNabavka.aDPocetnaRangListaPoKriteriumiExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40050);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDPocetnaRangListaZaUcestvoNaEAukcijaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40032);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCenaExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40033);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.aDPokanaKonecnaCenaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40051);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDRealizacijaJNExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40025);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDRealizacijaPlanNabavkaPoREExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40026);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDSpecifikacijaJavnaNabavkaUslugiExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40047);
     dmRes.frxReport1.DesignReport()
end;

procedure TfrmJavnaNabavka.aDTehnickaSpecifikacijaExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',40053);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aDZapisnikOtvoranjePonudiExecute(Sender: TObject);
begin
     dmReport.ZapisnikOtvoranjePonudi.Close;
     dmReport.ZapisnikOtvoranjePonudi.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
     dmReport.ZapisnikOtvoranjePonudi.Open;
     dmRes.Spremi('JN',40022);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmJavnaNabavka.aStavkiVoJavnaNabavkaExecute(Sender: TObject);
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (dm.tblTenderInfo.State in [dsBrowse])  and (PanelMaxPoeni.Visible = false) and (PanelVnesiKomisija.Visible = false) then
       begin
          frmStavkiVoJavnaNabavka:=TfrmStavkiVoJavnaNabavka.Create(Self, false);
          frmStavkiVoJavnaNabavka.ShowModal;
          frmStavkiVoJavnaNabavka.Free;
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmJavnaNabavka.aPTehnickaSpecifikacijaExecute(Sender: TObject);
begin
 try
        dmRes.Spremi('JN',40053);
        dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
        dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmJavnaNabavka.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmJavnaNabavka.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

procedure TfrmJavnaNabavka.ZALBA_OPISDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblTenderInfoZALBA_OPIS.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblTenderInfoZALBA_OPIS.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.ZA_IZMENADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoZA_IZMENA, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoZA_IZMENA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmJavnaNabavka.ZA_POVLEKUVANJEDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblTenderInfo.CreateBlobStream(dm.tblTenderInfoZA_POVLEKUVANJE, bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblTenderInfoZA_POVLEKUVANJE as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmJavnaNabavka.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmJavnaNabavka.cxDBRadioGroupPONISTENExit(Sender: TObject);
begin
 if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       if dm.tblTenderPONISTEN.Value = 0  then
          VRSKA_PREDMET.SetFocus
      // else  if (dm.tblTenderPONISTEN.Value = 1) and  (not dm.tblTenderVRSKA_PREDMET.IsNull)then
       //   VRSKA_PREDMET.Clear;
     end;
end;

procedure TfrmJavnaNabavka.cxDBRadioGroupRAMKOVNA_SPOGClick(Sender: TObject);
begin
      if dm.tblTenderInfoRAMKOVNA_SPOG.Value = 1 then
        begin
            cxDBRadioGroupRS_POVEKE_OPERATORI.Clear;
            cxDBRadioGroupRS_POVEKE_ORGANI.Clear;
        end;
end;

function  TfrmJavnaNabavka.ponisten_tender(broj:String; res:Boolean):Boolean;
//var res:Boolean;
begin
    res:=true;
    if dm.tblTenderPONISTEN.Value = 0 then
       begin
         if (not dm.tblTenderVRSKA_PREDMET.IsNull) then
             begin
               dm.qCountStavkiTender.Close;
               dm.qCountStavkiTender.ParamByName('broj').Value:=dm.tblTenderVRSKA_PREDMET.Value;
               dm.qCountStavkiTender.ExecQuery;
               if (dm.qCountStavkiTender.FldByName['br'].Value > 0) then
                  res:=False;
             end;
       end;

end;
end.
