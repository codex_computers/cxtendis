﻿object frmDobitnici: TfrmDobitnici
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 765
  ClientWidth = 1252
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1252
    765)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1252
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          Caption = #1055#1088#1077#1075#1083#1077#1076#1080
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 742
    Width = 1252
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F8 - '#1041#1088#1080#1096#1080' '#1076#1086#1073#1080#1090#1085#1080#1082' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072', F7 - '#1054#1089#1074#1077#1078#1080
        Width = 270
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1048#1079#1073#1086#1088' '#1085#1072' '#1089#1090#1072#1074#1082#1072' '#1086#1076' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        Width = 220
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1252
    Height = 67
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 2
    object Label8: TLabel
      Left = 14
      Top = 16
      Width = 194
      Height = 14
      Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112' :'
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Panel8: TPanel
      Left = 1
      Top = 39
      Width = 1250
      Height = 27
      Align = alBottom
      Color = 11302478
      ParentBackground = False
      TabOrder = 0
      object CheckBox1: TcxCheckBox
        Left = 27
        Top = 5
        TabStop = False
        Caption = #1064#1090#1080#1082#1083#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077' '#1089#1090#1072#1074#1082#1080' '#1086#1076' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072
        Style.TextColor = clWhite
        TabOrder = 0
        Transparent = True
        OnClick = CheckBox1Click
        Width = 260
      end
      object CheckBox2: TcxCheckBox
        Left = 315
        Top = 5
        TabStop = False
        Caption = #1064#1090#1080#1082#1083#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077' '#1089#1090#1072#1074#1082#1080' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1072#1090#1072' '#1085#1077#1076#1077#1083#1080#1074#1072' '#1075#1088#1091#1087#1072
        Style.TextColor = clWhite
        TabOrder = 1
        Transparent = True
        OnClick = CheckBox2Click
        Width = 337
      end
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 548
    Width = 1252
    Height = 194
    Align = alClient
    TabOrder = 3
    object cxGrid2DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid2DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsDobitniciTender
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1076#1086#1073#1080#1090#1085#1080#1094#1080
      object cxGrid2DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Width = 90
      end
      object cxGrid2DBTableView1ID_PONUDI_STAVKI: TcxGridDBColumn
        DataBinding.FieldName = 'ID_PONUDI_STAVKI'
        Visible = False
        Width = 149
      end
      object cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ_TENDER'
        Visible = False
        Width = 122
      end
      object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Options.Editing = False
        Width = 39
      end
      object cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDNAZIV'
        Width = 70
      end
      object cxGrid2DBTableView1GENERIKA: TcxGridDBColumn
        DataBinding.FieldName = 'GENERIKA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 1
          end>
        Width = 69
      end
      object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKAL'
        Width = 63
      end
      object cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKALNAZIV'
        Width = 323
      end
      object cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Visible = False
        Width = 127
      end
      object cxGrid2DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'TIPPARTNERNAZIV'
        Width = 94
      end
      object cxGrid2DBTableView1PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNER'
        Visible = False
        Width = 108
      end
      object cxGrid2DBTableView1PARTNERNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PARTNERNAZIV'
        Visible = False
        Width = 260
      end
      object cxGrid2DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV_SKRATEN'
        Width = 260
      end
      object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
      end
      object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Width = 66
      end
      object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Width = 224
      end
      object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
        DataBinding.FieldName = 'TS_INS'
        Visible = False
        Width = 160
      end
      object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'TS_UPD'
        Visible = False
        Width = 211
      end
      object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
        DataBinding.FieldName = 'USR_INS'
        Visible = False
        Width = 201
      end
      object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'USR_UPD'
        Visible = False
        Width = 237
      end
      object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'VID_ARTIKAL'
        Visible = False
        Width = 40
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 507
    Width = 1252
    Height = 41
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 4
    object Label1: TLabel
      Left = 14
      Top = 13
      Width = 220
      Height = 14
      Caption = #1044#1086#1073#1080#1090#1085#1080#1094#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112' :'
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object cxDBLabel2: TcxDBLabel
      Left = 244
      Top = 41
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsТender
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 186
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 193
    Width = 1252
    Height = 262
    Align = alTop
    TabOrder = 5
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsTenderStavki
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1080#1079#1073#1086#1088
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      object cxGrid1DBTableView1ColumnOdberi: TcxGridDBColumn
        Caption = #1054#1076#1073#1077#1088#1080
        DataBinding.ValueType = 'Boolean'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.MultiLine = True
        Options.Sorting = False
        Width = 46
      end
      object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ_TENDER'
        Visible = False
        Options.Editing = False
        Width = 121
      end
      object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Options.Editing = False
        Width = 40
      end
      object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDNAZIV'
        Options.Editing = False
        Width = 70
      end
      object cxGrid1DBTableView1GENERIKA: TcxGridDBColumn
        DataBinding.FieldName = 'GENERIKA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 68
      end
      object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKAL'
        Options.Editing = False
        Width = 62
      end
      object cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIVARTIKAL'
        Options.Editing = False
        Width = 251
      end
      object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
        Options.Editing = False
        Width = 66
      end
      object cxGrid1DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Options.Editing = False
        Width = 41
      end
      object cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Options.Editing = False
        Width = 79
      end
      object cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn
        DataBinding.FieldName = 'PROZVODITEL'
        Options.Editing = False
        Width = 122
      end
      object cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PROIZVODITELNAZIV'
        Options.Editing = False
        Width = 142
      end
      object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Options.Editing = False
        Width = 47
      end
      object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1JN_CPV: TcxGridDBColumn
        DataBinding.FieldName = 'JN_CPV'
        Options.Editing = False
        Width = 112
      end
      object cxGrid1DBTableView1MK: TcxGridDBColumn
        DataBinding.FieldName = 'MK'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1EN: TcxGridDBColumn
        DataBinding.FieldName = 'EN'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'VID_ARTIKAL'
        Visible = False
        Options.Editing = False
        Width = 79
      end
      object cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn
        DataBinding.FieldName = 'VALIDNOST'
        Visible = False
      end
      object cxGrid1DBTableView1PRICINA_PONISTENA_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PRICINA_PONISTENA_NAZIV'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 455
    Width = 1252
    Height = 5
    AlignSplitter = salTop
    Control = cxGrid1
  end
  object Panel2: TPanel
    Left = 0
    Top = 460
    Width = 1252
    Height = 47
    Align = alTop
    TabOrder = 7
    DesignSize = (
      1252
      47)
    object cxButton6: TcxButton
      Left = 278
      Top = 10
      Width = 283
      Height = 25
      Action = aOznaciPonusteniStavkiPanel
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton8: TcxButton
      Left = 567
      Top = 10
      Width = 283
      Height = 25
      Action = aOznaciValidniStavki
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton7: TcxButton
      Left = 13
      Top = 10
      Width = 259
      Height = 25
      Action = aIzborStavka
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object PanelIzborGenerikaPonuduvac: TPanel
    Left = 55
    Top = 133
    Width = 673
    Height = 401
    Color = 11302478
    ParentBackground = False
    TabOrder = 8
    Visible = False
    DesignSize = (
      673
      401)
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 671
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1082#1086#1085#1082#1088#1077#1090#1077#1085' '#1072#1088#1090#1080#1082#1072#1083
      Color = 11302478
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindow
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object cxGrid4: TcxGrid
      Left = 24
      Top = 40
      Width = 625
      Height = 137
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
      object cxGrid4DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsDobitniciIzborArtikal
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1080#1079#1073#1086#1088
        OptionsView.GroupByBox = False
        object cxGrid4DBTableView1GENERIKA: TcxGridDBColumn
          DataBinding.FieldName = 'GENERIKA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 68
        end
        object cxGrid4DBTableView1VID: TcxGridDBColumn
          DataBinding.FieldName = 'VID'
          Width = 44
        end
        object cxGrid4DBTableView1VIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDNAZIV'
          Width = 62
        end
        object cxGrid4DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
        end
        object cxGrid4DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 382
        end
        object cxGrid4DBTableView1ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
          Visible = False
        end
      end
      object cxGrid4Level1: TcxGridLevel
        GridView = cxGrid4DBTableView1
      end
    end
    object Panel7: TPanel
      Left = -24
      Top = 273
      Width = 672
      Height = 39
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvNone
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
      Color = 11302478
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindow
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object cxGrid5: TcxGrid
      Left = 24
      Top = 214
      Width = 625
      Height = 137
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 3
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsDobitniciIzborPonuduvac
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1089#1090#1072#1074#1082#1072
        OptionsView.GroupByBox = False
        object cxGridDBColumn1: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_TENDER'
          Visible = False
          Options.Editing = False
          Width = 117
        end
        object cxGridDBColumn2: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBColumn3: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
          Options.Editing = False
        end
        object cxGridDBColumn4: TcxGridDBColumn
          DataBinding.FieldName = 'PONUDI_STAVKI_ID'
          Visible = False
          Options.Editing = False
          Width = 144
        end
        object cxGridDBColumn5: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
          Options.Editing = False
          Width = 128
        end
        object cxGridDBColumn6: TcxGridDBColumn
          DataBinding.FieldName = 'TIPPARTNERNAZIV'
          Visible = False
          Options.Editing = False
          Width = 93
        end
        object cxGridDBColumn7: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Visible = False
          Options.Editing = False
          Width = 107
        end
        object cxGridDBColumn8: TcxGridDBColumn
          DataBinding.FieldName = 'PARTBERNAZIV'
          Options.Editing = False
          Width = 531
        end
        object cxGridDBColumn9: TcxGridDBColumn
          DataBinding.FieldName = 'SUMABODOVI'
          Options.Editing = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
    object cxButton2: TcxButton
      Left = 24
      Top = 365
      Width = 91
      Height = 25
      Action = aOK
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 4
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton3: TcxButton
      Left = 121
      Top = 365
      Width = 91
      Height = 25
      Action = aOtkazi
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 5
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object PanelPonuduvacMulti: TPanel
    Left = 660
    Top = 88
    Width = 513
    Height = 302
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    Color = 11302478
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 9
    Visible = False
    DesignSize = (
      513
      302)
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 513
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel11: TPanel
      Left = 0
      Top = 39
      Width = 513
      Height = 211
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        513
        211)
      object cxGrid6: TcxGrid
        Left = 24
        Top = 2
        Width = 465
        Height = 209
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsPonuduvaciZaSite
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1089#1090#1072#1074#1082#1072
          OptionsView.GroupByBox = False
          object cxGridDBTableView2TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Width = 125
          end
          object cxGridDBTableView2TIPPARTNERNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'TIPPARTNERNAZIV'
            Visible = False
            Width = 105
          end
          object cxGridDBTableView2PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Visible = False
            Width = 108
          end
          object cxGridDBTableView2PARTNERNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNERNAZIV'
            Width = 373
          end
          object cxGridDBTableView2SVBODOVI: TcxGridDBColumn
            DataBinding.FieldName = 'SVBODOVI'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object cxButton4: TcxButton
      Left = 27
      Top = 263
      Width = 91
      Height = 25
      Action = aOK
      Anchors = [akTop, akRight]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton5: TcxButton
      Left = 124
      Top = 263
      Width = 91
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object PanelIzborPonuduvac: TPanel
    Left = 488
    Top = 396
    Width = 513
    Height = 302
    Anchors = [akTop, akRight]
    BevelOuter = bvNone
    Color = 11302478
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 10
    Visible = False
    DesignSize = (
      513
      302)
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 513
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel5: TPanel
      Left = 0
      Top = 39
      Width = 513
      Height = 211
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        513
        211)
      object cxGrid3: TcxGrid
        Left = 27
        Top = 6
        Width = 465
        Height = 326
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsDobitniciIzborPonuduvac
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1085#1091#1076#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1089#1090#1072#1074#1082#1072
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Width = 117
          end
          object cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'ARTIKAL'
            Visible = False
          end
          object cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'VID_ARTIKAL'
            Visible = False
          end
          object cxGrid3DBTableView1PONUDI_STAVKI_ID: TcxGridDBColumn
            DataBinding.FieldName = 'PONUDI_STAVKI_ID'
            Visible = False
            Width = 144
          end
          object cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Width = 128
          end
          object cxGrid3DBTableView1PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Visible = False
            Width = 107
          end
          object cxGrid3DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'TIPPARTNERNAZIV'
            Visible = False
            Width = 93
          end
          object cxGrid3DBTableView1PARTBERNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PARTBERNAZIV'
            Width = 373
          end
          object cxGrid3DBTableView1VREDNOST: TcxGridDBColumn
            DataBinding.FieldName = 'SUMABODOVI'
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
    object btnOK: TcxButton
      Left = 27
      Top = 263
      Width = 91
      Height = 25
      Action = aOK
      Anchors = [akTop, akRight]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton1: TcxButton
      Left = 124
      Top = 263
      Width = 91
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel9: TPanel
    Left = 448
    Top = 219
    Width = 513
    Height = 126
    Caption = 'Panel9'
    TabOrder = 11
    Visible = False
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 12
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077
      TabOrder = 0
      DesignSize = (
        481
        93)
      Height = 93
      Width = 481
      object cxExtLookupComboBox1: TcxExtLookupComboBox
        Left = 16
        Top = 25
        Properties.ImmediatePost = True
        Properties.View = cxGridViewRepository1DBTableView1
        Properties.KeyFieldNames = 'ID'
        Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
        TabOrder = 0
        Width = 449
      end
      object cxButton9: TcxButton
        Left = 24
        Top = 52
        Width = 290
        Height = 25
        Action = aOznaciPonusteniStavki
        Anchors = [akLeft, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 1
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxButton10: TcxButton
        Left = 320
        Top = 52
        Width = 145
        Height = 25
        Action = aOtkaziPonistuvanje
        Anchors = [akLeft, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 2
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 360
    Top = 528
  end
  object PopupMenu1: TPopupMenu
    Left = 464
    Top = 520
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 544
    Top = 504
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 241
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 733
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1086#1073#1080#1090#1085#1080#1094#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1026
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 582
      DockedTop = 0
      FloatLeft = 1026
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 163
      DockedTop = 0
      FloatLeft = 1026
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aSnimiIzgledStavkiJN
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiIzgledStavkiJN
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPDobitniciJavnaNabavka
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Action = aPIzvestuvanjeZaDobivanjeTender
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aZacuvajExcelTabelaStavki
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = actPIzvestuvanjeZaDobivanjeTenderGrupno
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = actIzvestuvanjeZaDobitniciExportPdf
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 240
    Top = 520
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aOK: TAction
      Caption = 'OK'
      ImageIndex = 7
      OnExecute = aOKExecute
    end
    object aSnimiIzgledStavkiJN: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledStavkiJNExecute
    end
    object aBrisiIzgledStavkiJN: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledStavkiJNExecute
    end
    object aIzborStavka: TAction
      Caption = #1048#1079#1073#1077#1088#1080' '#1076#1086#1073#1080#1090#1085#1080#1082' '#1079#1072' '#1096#1090#1080#1082#1083#1080#1088#1072#1085#1080#1090#1077' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 58
      ShortCut = 45
      OnExecute = aIzborStavkaExecute
    end
    object aDizajnPopupMenu: TAction
      Caption = 'aDizajnPopupMenu'
      ShortCut = 16505
      OnExecute = aDizajnPopupMenuExecute
    end
    object aPDobitniciJavnaNabavka: TAction
      Caption = #1044#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aPDobitniciJavnaNabavkaExecute
    end
    object aDizajnDobitniciJavnaNabavka: TAction
      Caption = #1044#1086#1073#1080#1090#1085#1080#1094#1080' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aDizajnDobitniciJavnaNabavkaExecute
    end
    object aPIzvestuvanjeZaDobivanjeTender: TAction
      Caption = 
        #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1088#1072#1076#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076 +
        #1072
      ImageIndex = 19
      OnExecute = aPIzvestuvanjeZaDobivanjeTenderExecute
    end
    object aDizajnIzvestuvanjeZaDobivanjeTender: TAction
      Caption = 
        #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1088#1072#1076#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076 +
        #1072
      ImageIndex = 19
      OnExecute = aDizajnIzvestuvanjeZaDobivanjeTenderExecute
    end
    object aZacuvajExcelTabelaStavki: TAction
      Caption = 
        #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1079#1072' '#1082#1086#1080' '#1085#1077#1084#1072' '#1076 +
        #1086#1076#1077#1083#1077#1085#1086' '#1076#1086#1073#1080#1090#1085#1080#1082
      ImageIndex = 9
      OnExecute = aZacuvajExcelTabelaStavkiExecute
    end
    object aOznaciPonusteniStavki: TAction
      Caption = #1054#1079#1085#1072#1095#1080' '#1075#1080' '#1082#1072#1082#1086' '#1087#1086#1085#1080#1096#1090#1077#1085#1080' '#1096#1090#1080#1082#1083#1080#1088#1072#1085#1080#1090#1077' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 61
      OnExecute = aOznaciPonusteniStavkiExecute
    end
    object aOznaciValidniStavki: TAction
      Caption = #1054#1079#1085#1072#1095#1080' '#1075#1080' '#1082#1072#1082#1086' '#1074#1072#1083#1080#1076#1085#1080' '#1096#1090#1080#1082#1083#1080#1088#1072#1085#1080#1090#1077' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 62
      OnExecute = aOznaciValidniStavkiExecute
    end
    object aOznaciPonusteniStavkiPanel: TAction
      Caption = #1054#1079#1085#1072#1095#1080' '#1075#1080' '#1082#1072#1082#1086' '#1087#1086#1085#1080#1096#1090#1077#1085#1080' '#1096#1090#1080#1082#1083#1080#1088#1072#1085#1080#1090#1077' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 61
      OnExecute = aOznaciPonusteniStavkiPanelExecute
    end
    object aOtkaziPonistuvanje: TAction
      Caption = #1054#1090#1082#1072#1078#1080' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077
      ImageIndex = 84
      OnExecute = aOtkaziPonistuvanjeExecute
    end
    object actPIzvestuvanjeZaDobivanjeTenderGrupno: TAction
      Caption = 
        #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1088#1072#1076#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076 +
        #1072' - '#1043#1088#1091#1087#1085#1086' '#1089#1080#1090#1077' '#1076#1086#1073#1080#1090#1085#1080#1094#1080
      ImageIndex = 19
      OnExecute = actPIzvestuvanjeZaDobivanjeTenderGrupnoExecute
    end
    object actDIzvestuvanjeZaDobivanjeTenderGrupno: TAction
      Caption = 
        #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1088#1072#1076#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076 +
        #1072' - '#1043#1088#1091#1087#1085#1086' '#1089#1080#1090#1077' '#1076#1086#1073#1080#1090#1085#1080#1094#1080
      ImageIndex = 19
      OnExecute = actDIzvestuvanjeZaDobivanjeTenderGrupnoExecute
    end
    object actIzvestuvanjeZaDobitniciExportPdf: TAction
      Caption = 
        #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1088#1072#1076#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076 +
        #1072' - Export to .Pdf'
      ImageIndex = 103
      OnExecute = actIzvestuvanjeZaDobitniciExportPdfExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 528
    Top = 384
    object dxComponentPrinter1Link2: TdxGridReportLink
      Component = cxGrid2
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 96
    Top = 392
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 360
    Top = 120
  end
  object tblTenderStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select  distinct'
      '        ts.broj_tender,'
      '        ts.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ts.artikal,'
      '        ma.naziv as nazivArtikal,'
      '        ts.kolicina,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.generika,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ts.validnost,'
      '        ts.pricina_ponistena,'
      '        pp.naziv as pricina_ponistena_naziv '
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left outer join jn_ponudi_stavki ps on ps.vid_artikal = ts.vid_a' +
        'rtikal and ps.artikal = ts.artikal and ps.broj_tender = :mas_bro' +
        'j'
      
        'left outer join jn_tender_dobitnici d on d.id_ponudi_stavki = ps' +
        '.id'
      
        'left outer join jn_pricina_ponistuvanje pp on pp.id =ts.pricina_' +
        'ponistena'
      'where ts.broj_tender = :mas_broj and d.vid_artikal is null'
      
        '      and coalesce(ps.vid_artikal ||'#39'V'#39' ||ps.artikal,'#39'V'#39') not in' +
        ' (select ps.vid_artikal || '#39'V'#39' ||ps.artikal'
      
        '                                                    from jn_ponu' +
        'di_stavki ps'
      
        '                                                    left outer j' +
        'oin jn_tender_dobitnici d on d.id_ponudi_stavki = ps.id'
      
        '                                                    where ps.bro' +
        'j_tender = :mas_broj and d.artikal is not null'
      '                                                    )'
      'order by ma.jn_tip_artikal, ma.naziv')
    AutoUpdateOptions.AutoReWriteSqls = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 872
    Top = 16
    object tblTenderStavkiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblTenderStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblTenderStavkiNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderStavkiVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblTenderStavkiPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblTenderStavkiJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblTenderStavkiVALIDNOST: TFIBIntegerField
      DisplayLabel = #1042#1072#1083#1080#1076#1085#1086#1089#1090
      FieldName = 'VALIDNOST'
    end
    object tblTenderStavkiPRICINA_PONISTENA: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1096#1080#1092#1088#1072
      FieldName = 'PRICINA_PONISTENA'
    end
    object tblTenderStavkiPRICINA_PONISTENA_NAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077
      FieldName = 'PRICINA_PONISTENA_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTenderStavki: TDataSource
    DataSet = tblTenderStavki
    Left = 768
    Top = 24
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 104
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    PopupMenus = <>
    Left = 400
    Top = 384
  end
  object dsPonuduvaciZaSite: TDataSource
    DataSet = tblPonuduvaciZaSite
    Left = 616
    Top = 104
  end
  object tblPonuduvaciZaSite: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.tip_partner,'
      '       p.partner,'
      '       mp.naziv_skraten as PartnerNaziv,'
      '       tp.naziv as tipPartnerNaziv,'
      '       sum(coalesce(sb.bodovi,0))/(select count(t.broj_tender)'
      '                                   from jn_ponudi_stavki t'
      
        '                                   where t.broj_tender = :mas_br' +
        'oj'
      
        '                                   and t.tip_partner = p.tip_par' +
        'tner'
      '                                   and t.partner = p.partner'
      
        '                                   and t.odbiena = 0) as SVbodov' +
        'i'
      'from jn_ponudi_stavki p'
      
        'inner join mat_partner mp on mp.id = p.partner and mp.tip_partne' +
        'r = p.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      
        'inner join jn_ponudi_stavki_bodirani sb on sb.ponudi_stavki_id =' +
        ' p.id'
      'where p.broj_tender = :mas_broj and p.odbiena = 0'
      'group by 1, 2, 3, 4'
      'order by SVbodovi desc ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsТender
    Left = 480
    Top = 240
    object tblPonuduvaciZaSiteTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblPonuduvaciZaSitePARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblPonuduvaciZaSitePARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonuduvaciZaSiteTIPPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonuduvaciZaSiteSVBODOVI: TFIBFloatField
      DisplayLabel = #1041#1086#1076#1086#1074#1080
      FieldName = 'SVBODOVI'
      DisplayFormat = '0.0000 , .'
    end
  end
  object qCountPonudiZaStavka: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select ps.id, '
      '       count(ps.id) as c'
      'from jn_ponudi_stavki ps'
      
        'where ps.partner = :partner and ps.tip_partner = :tip_partner an' +
        'd ps.broj_tender = :broj_tender'
      
        '      and ps.vid_artikal = :vid_artikal and ps.artikal = :artika' +
        'l and ps.odbiena = 0'
      'group by 1')
    Left = 600
    Top = 240
  end
  object PopupMenu2: TPopupMenu
    Left = 488
    Top = 120
    object N2: TMenuItem
      Action = aDizajnDobitniciJavnaNabavka
    end
    object N3: TMenuItem
      Action = aDizajnIzvestuvanjeZaDobivanjeTender
    end
    object N4: TMenuItem
      Action = actDIzvestuvanjeZaDobivanjeTenderGrupno
    end
  end
  object qPricina: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      '  UPDATE JN_TENDER_STAVKI'
      'SET '
      '    '
      '    PRICINA_PONISTENA = :PRICINA_PONISTENA'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL')
    Left = 216
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dsPricinaPonistuvanje: TDataSource
    AutoEdit = False
    DataSet = tblPricinaPonistuvanje
    Left = 144
    Top = 536
  end
  object tblPricinaPonistuvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.naziv,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3'
      'from jn_pricina_ponistuvanje p')
    AutoUpdateOptions.KeyFields = 'ID'
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 128
    Top = 456
    object tblPricinaPonistuvanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPricinaPonistuvanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPricinaPonistuvanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPricinaPonistuvanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 288
    Top = 336
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsPricinaPonistuvanje
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
      end
      object cxGridViewRepository1DBTableView1TS_INS: TcxGridDBColumn
        DataBinding.FieldName = 'TS_INS'
      end
      object cxGridViewRepository1DBTableView1TS_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'TS_UPD'
      end
      object cxGridViewRepository1DBTableView1USR_INS: TcxGridDBColumn
        DataBinding.FieldName = 'USR_INS'
      end
      object cxGridViewRepository1DBTableView1USR_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'USR_UPD'
      end
      object cxGridViewRepository1DBTableView1CUSTOM1: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM1'
      end
      object cxGridViewRepository1DBTableView1CUSTOM2: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM2'
      end
      object cxGridViewRepository1DBTableView1CUSTOM3: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM3'
      end
    end
  end
  object qGrupa: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      '  select count(ng.grupa) as br'
      '  from jn_nedelivi_grupi ng'
      '  where ng.broj_tender = :broj_tender and ng.grupa = :grupa')
    Left = 120
    Top = 304
  end
  object dlgOpenExportPdf: TOpenDialog
    Left = 368
    Top = 288
  end
end
