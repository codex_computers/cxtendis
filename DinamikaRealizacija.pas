unit DinamikaRealizacija;


{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 04.12.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxNavigator,
  System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmDinamikaRealizacija = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel1: TPanel;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    PanelVnes: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label71: TLabel;
    Label2: TLabel;
    Mesec: TcxComboBox;
    TIP_NABAVKA: TcxTextEdit;
    TIP_NABAVKA_NAZIV: TcxLookupComboBox;
    aZapisPaneliVnes: TAction;
    cxButtonOtkazi: TcxButton;
    btnOK: TcxButton;
    aOK: TAction;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NABAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1TIPNABAVKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESEC: TcxGridDBColumn;
    Label3: TLabel;
    labelGodisenPlanGodina: TLabel;
    dxBarLargeButton10: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    lbl1: TLabel;
    Predmet: TcxMemo;
    cxGrid1DBTableView1PREDMET_NABAVKA: TcxGridDBColumn;
    btnaOK: TcxButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aZapisPaneliVnesExecute(Sender: TObject);
    procedure aOKExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure btnaOKClick(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDinamikaRealizacija: TfrmDinamikaRealizacija;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmProcedureQuey;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmDinamikaRealizacija.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDinamikaRealizacija.aBrisiExecute(Sender: TObject);
var i,j,flag, vid,id:Integer;
    grupa_naziv, vid_naziv:string;
begin
    with cxGrid1DBTableView1.DataController do
     for I := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
         begin
            if (cxGrid1DBTableView1.Controller.SelectedRecords[i].IsData=false) and (cxGrid1DBTableView1.Controller.SelectedRows[i].AsGroupRow.GroupedColumn.Caption = '��� - �����') then
                begin
                   dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Null, Null,cxGrid1DBTableView1.Controller.SelectedRows[i].DisplayTexts[i],
                                 ' ',0,dm.tblGodPlanOdlukaGODINA.Value, 0,0,Null,Null,Null,Null);
                   vid_naziv:=dm.tblDinamikaRealizacijaVIDARTIKALNAZIV.Value;
                   dm.tblDinamikaRealizacija.Close;
                   dm.tblDinamikaRealizacija.Open;
                   dm.tblDinamikaRealizacija.Locate('VIDARTIKALNAZIV',vid_naziv,[])
                end
            else if (cxGrid1DBTableView1.Controller.SelectedRecords[i].IsData=false) and (cxGrid1DBTableView1.Controller.SelectedRows[i].AsGroupRow.GroupedColumn.Caption = '����� - �����') then
                begin
                     dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Null, Null,' ',
                                 cxGrid1DBTableView1.Controller.SelectedRows[i].DisplayTexts[i],1,dm.tblGodPlanOdlukaGODINA.Value, 0,0,Null,Null,Null,Null);
                     grupa_naziv:=dm.tblDinamikaRealizacijaGRUPANAZIV.Value;
                     dm.tblDinamikaRealizacija.Close;
                     dm.tblDinamikaRealizacija.Open;
                     dm.tblDinamikaRealizacija.Locate('GRUPANAZIV',grupa_naziv,[])
                end
            else
                begin
                  with (cxGrid1DBTableView1.DataController) do
                    for J := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
                      begin
                       flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;
                       dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Null, Null,' ',
                                 '',2,dm.tblGodPlanOdlukaGODINA.Value, GetValue(flag,cxGrid1DBTableView1ARTIKAL.Index),GetValue(flag,cxGrid1DBTableView1VID_ARTIKAL.Index),Null,Null,Null,Null);
                       vid:=dm.tblDinamikaRealizacijaVID_ARTIKAL.Value;
                       id:=dm.tblDinamikaRealizacijaARTIKAL.Value;
                       dm.tblDinamikaRealizacija.Close;
                       dm.tblDinamikaRealizacija.Open;
                       dm.tblDinamikaRealizacija.Locate('VID_ARTIKAL; ARTIKAL', VarArrayOf([vid, id]), []);
                     end;
                end;
         end;
     PanelVnes.Visible:=false;

end;

procedure TfrmDinamikaRealizacija.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDinamikaRealizacija.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDinamikaRealizacija.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDinamikaRealizacija.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDinamikaRealizacija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDinamikaRealizacija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDinamikaRealizacija.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (sender = TIP_NABAVKA) and (TIP_NABAVKA.Text <> '') then
       begin
         dm.tblTipNabavka.Locate('SIFRA', StrToInt(TIP_NABAVKA.Text),[]);
         TIP_NABAVKA_NAZIV.Text:=dm.tblTipNabavkaNAZIV.Value;
       end
    else if (Sender = TIP_NABAVKA_NAZIV) and (TIP_NABAVKA_NAZIV.Text <> '') then
       begin
         TIP_NABAVKA.Text:=IntToStr(dm.tblTipNabavkaSIFRA.Value);
       end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDinamikaRealizacija.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDinamikaRealizacija.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmDinamikaRealizacija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := caFree
end;
procedure TfrmDinamikaRealizacija.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmDinamikaRealizacija.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    labelGodisenPlanGodina.Caption:=intTostr(dm.tblGodPlanOdlukaGODINA.Value);


    dm.tblDinamikaRealizacija.Close;
    dm.tblDinamikaRealizacija.Open;
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    //sobrano := True;
    cxGrid1DBTableView1.ViewData.Expand(true);
   // sobrano := true;
  end;
//------------------------------------------------------------------------------

procedure TfrmDinamikaRealizacija.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDinamikaRealizacija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmDinamikaRealizacija.aZapisPaneliVnesExecute(Sender: TObject);
begin
   if (panelVnes.Visible = false) and (cxGrid1DBTableView1.Controller.SelectedRecordCount > 0) then
      begin
         PanelVnes.Visible:=true;
         TIP_NABAVKA.Clear;
         TIP_NABAVKA_NAZIV.Clear;
         Mesec.Clear;
        // Predmet.Clear;
         TIP_NABAVKA.SetFocus;
      end
end;

procedure TfrmDinamikaRealizacija.btnaOKClick(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmDinamikaRealizacija.aOKExecute(Sender: TObject);
var i,j,flag, vid,id:Integer;
    grupa_naziv, vid_naziv:string;
begin
if Validacija(PanelVnes) = False then
   begin
    with cxGrid1DBTableView1.DataController do
     for I := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
         begin
            if (cxGrid1DBTableView1.Controller.SelectedRecords[i].IsData=false) and (cxGrid1DBTableView1.Controller.SelectedRows[i].AsGroupRow.GroupedColumn.Caption = '��� - �����') then
                begin
                   dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Mesec.Text, strToInt(TIP_NABAVKA.Text),cxGrid1DBTableView1.Controller.SelectedRows[i].DisplayTexts[i],
                                 ' ',0,dm.tblGodPlanOdlukaGODINA.Value, 0,0,Predmet.Text,Null,Null, Null);
                   vid_naziv:=dm.tblDinamikaRealizacijaVIDARTIKALNAZIV.Value;
                   dm.tblDinamikaRealizacija.Close;
                   dm.tblDinamikaRealizacija.Open;
                   dm.tblDinamikaRealizacija.Locate('VIDARTIKALNAZIV',vid_naziv,[])
                end
            else if (cxGrid1DBTableView1.Controller.SelectedRecords[i].IsData=false) and (cxGrid1DBTableView1.Controller.SelectedRows[i].AsGroupRow.GroupedColumn.Caption = '����� - �����') then
                begin
                     dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Mesec.Text, strToInt(TIP_NABAVKA.Text),' ',
                                 cxGrid1DBTableView1.Controller.SelectedRows[i].DisplayTexts[i],1,dm.tblGodPlanOdlukaGODINA.Value, 0,0,Predmet.Text,Null,Null,Null);
                     grupa_naziv:=dm.tblDinamikaRealizacijaGRUPANAZIV.Value;
                     dm.tblDinamikaRealizacija.Close;
                     dm.tblDinamikaRealizacija.Open;
                     dm.tblDinamikaRealizacija.Locate('GRUPANAZIV',grupa_naziv,[])
                end
            else
                begin
                  with (cxGrid1DBTableView1.DataController) do
                    for J := 0 to cxGrid1DBTableView1.Controller.SelectedRecordCount - 1 do
                      begin
                       flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;
                       dmPQ.insert12(dmPQ.pDinamikaRealizacija,'MESEC', 'TIP_NABAVKA', 'ARTVID', 'ART_GRUPA', 'PARAM', 'GODINA', 'ARTIKAL','VID_ARTIKAL','PREDMET_NABAVKA',Null,Null,Null,
                                 Mesec.Text, strToInt(TIP_NABAVKA.Text),' ',
                                 '',2,dm.tblGodPlanOdlukaGODINA.Value, GetValue(flag,cxGrid1DBTableView1ARTIKAL.Index),GetValue(flag,cxGrid1DBTableView1VID_ARTIKAL.Index),Predmet.Text,Null,Null,Null);
                       vid:=dm.tblDinamikaRealizacijaVID_ARTIKAL.Value;
                       id:=dm.tblDinamikaRealizacijaARTIKAL.Value;
                       dm.tblDinamikaRealizacija.Close;
                       dm.tblDinamikaRealizacija.Open;
                       dm.tblDinamikaRealizacija.Locate('VID_ARTIKAL; ARTIKAL', VarArrayOf([vid, id]), []);
                     end;
                end;
         end;

     PanelVnes.Visible:=false;
   end;
end;

procedure TfrmDinamikaRealizacija.aOtkaziExecute(Sender: TObject);
begin
  if PanelVnes.Visible = true then
     begin
        RestoreControls(PanelVnes);
        PanelVnes.Visible:=false;
     end
  else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDinamikaRealizacija.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmDinamikaRealizacija.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + labelGodisenPlanGodina.Caption);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDinamikaRealizacija.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmDinamikaRealizacija.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmDinamikaRealizacija.aSpustiSoberiExecute(Sender: TObject);
begin
     if (sobrano = true) then
       begin
        cxGrid1DBTableView1.ViewData.Expand(false);
        sobrano := false;
       end
     else
       begin
          cxGrid1DBTableView1.ViewData.Collapse(false);
          sobrano := true;
       end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDinamikaRealizacija.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDinamikaRealizacija.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDinamikaRealizacija.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDinamikaRealizacija.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
