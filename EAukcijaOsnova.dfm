object frmEAukcijaOsnova: TfrmEAukcijaOsnova
  Left = 0
  Top = 0
  Caption = #1047#1072#1095#1091#1074#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1087#1088#1077#1076' '#1077'-'#1040#1091#1082#1094#1080#1112#1072
  ClientHeight = 356
  ClientWidth = 581
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelNovaAukcija: TPanel
    Left = 0
    Top = 33
    Width = 581
    Height = 304
    Align = alClient
    TabOrder = 0
    object PanelDetalen: TPanel
      Left = 1
      Top = 139
      Width = 579
      Height = 164
      Align = alBottom
      Enabled = False
      TabOrder = 0
      DesignSize = (
        579
        164)
      object Label6: TLabel
        Left = 13
        Top = 19
        Width = 124
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084'  '#1085#1072' '#1077'-'#1040#1091#1082#1094#1080#1112#1072':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label9: TLabel
        Left = 87
        Top = 46
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 65
        Top = 73
        Width = 72
        Height = 16
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM: TcxDBDateEdit
        Left = 143
        Top = 16
        Hint = #1044#1072#1090#1091#1084
        BeepOnEnter = False
        DataBinding.DataField = 'DATUM'
        DataBinding.DataSource = dm.dsEAukcija
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
      object BROJ: TcxDBTextEdit
        Left = 143
        Top = 43
        Hint = #1041#1088#1086#1112' '
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsEAukcija
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 123
      end
      object ZABELESKA: TcxDBRichEdit
        Left = 143
        Top = 70
        Hint = 
          #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1079#1072#1073#1077#1083#1077#1096#1082#1080'  *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072'  '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082 +
          #1089#1090#1086#1090
        DataBinding.DataField = 'ZABELESKA'
        DataBinding.DataSource = dm.dsEAukcija
        ParentFont = False
        Properties.WantReturns = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clBlack
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        OnDblClick = ZABELESKADblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 46
        Width = 338
      end
      object ZapisiButton: TcxButton
        Left = 145
        Top = 125
        Width = 75
        Height = 25
        Action = aZapisi
        Anchors = [akTop, akRight]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 3
      end
      object OtkaziButton: TcxButton
        Left = 224
        Top = 125
        Width = 75
        Height = 25
        Action = aOtkazi
        Anchors = [akTop, akRight]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 4
      end
    end
    object PanelTabela: TPanel
      Left = 1
      Top = 1
      Width = 579
      Height = 138
      Align = alClient
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 577
        Height = 136
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsEAukcija
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Width = 124
          end
          object cxGrid1DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
          end
          object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
            DataBinding.FieldName = 'ZABELESKA'
            PropertiesClassName = 'TcxRichEditProperties'
            Properties.MemoMode = True
            Properties.ReadOnly = True
            Width = 325
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 161
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 227
          end
          object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 203
          end
          object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 239
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 337
    Width = 581
    Height = 19
    Panels = <
      item
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 581
    Height = 33
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 2
    object Label8: TLabel
      Left = 14
      Top = 10
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' :'
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object cxDBLabel1: TcxDBLabel
      Left = 175
      Top = 9
      DataBinding.DataField = 'BROJ_TENDER'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 186
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 56
    Top = 104
    object aGenerirajCeni_1: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1094#1077#1085#1080
      ImageIndex = 6
    end
    object aGenerirajCeni_2: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1094#1077#1085#1080
      ImageIndex = 6
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      SecondaryShortCuts.Strings = (
        'Esc')
      OnExecute = aOtkaziExecute
    end
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovExecute
    end
    object aEAukcija: TAction
      Caption = #1077' - '#1040#1091#1082#1094#1080#1112#1072
      ImageIndex = 41
      OnExecute = aEAukcijaExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajExecute
    end
    object aPListaSitePonudiSoBod: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aPListaSitePonudiSoBodExecute
    end
    object aPListaPonudiSoBodPoGrupaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1075#1088#1091#1087#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      ImageIndex = 19
      OnExecute = aPListaPonudiSoBodPoGrupaArtikalExecute
    end
    object aDListaSitePonudiSoBod: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aDListaSitePonudiSoBodExecute
    end
    object aDListaPonudiSoBodPoGrupaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1075#1088#1091#1087#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      ImageIndex = 19
      OnExecute = aDListaPonudiSoBodPoGrupaArtikalExecute
    end
    object aPopUp: TAction
      Caption = 'aPopUp'
      ShortCut = 16505
      OnExecute = aPopUpExecute
    end
  end
  object MainMenu1: TMainMenu
    Images = dmRes.cxSmallImages
    Left = 480
    Top = 88
    object N1: TMenuItem
      Action = aNov
    end
    object N2: TMenuItem
      Action = aAzuriraj
    end
    object N3: TMenuItem
      Action = aBrisi
    end
    object N5: TMenuItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080' '#1089#1086' '#1073#1086#1076#1086#1074#1080
      object N6: TMenuItem
        Action = aPListaPonudiSoBodPoGrupaArtikal
      end
      object N7: TMenuItem
        Action = aPListaSitePonudiSoBod
      end
    end
  end
  object PopupMenu1: TPopupMenu
    OwnerDraw = True
    Left = 264
    Top = 136
    object N9: TMenuItem
      Action = aDListaSitePonudiSoBod
    end
    object N8: TMenuItem
      Action = aDListaPonudiSoBodPoGrupaArtikal
    end
  end
end
