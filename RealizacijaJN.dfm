object frmRealizacijaJN: TfrmRealizacijaJN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
  ClientHeight = 741
  ClientWidth = 1362
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1362
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 718
    Width = 1362
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1362
    Height = 377
    Align = alTop
    Color = clWhite
    ParentBackground = False
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1360
      Height = 375
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsRealizacijaPlanNabavkaRe
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skSum
            Position = spFooter
            Column = cxGrid1DBTableView1IZNOS_PLAN
          end
          item
            Format = '0.00 , .'
            Column = cxGrid1DBTableView1CENAPLAN
          end
          item
            Format = '0.00 , .'
            Column = cxGrid1DBTableView1IZNOS_PLAN
          end
          item
            Kind = skSum
            Position = spFooter
            Column = cxGrid1DBTableView1IZNOS_PONUDA_EDINICEN
          end
          item
            Format = '0.00 , .'
            Column = cxGrid1DBTableView1IZNOS_PONUDA_EDINICEN
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Column = cxGrid1DBTableView1PONUDENACENA
          end
          item
            Format = '0.00 , .'
            Kind = skSum
            Column = cxGrid1DBTableView1IZNOS_PLAN
          end
          item
            Format = '0.00 , .'
            Kind = skSum
            Column = cxGrid1DBTableView1IZNOS_PONUDA_EDINICEN
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfAlwaysVisible
        object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_TENDER'
          Width = 119
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Width = 71
        end
        object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Width = 306
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 87
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Width = 106
        end
        object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNERNAZIV'
          Width = 222
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Width = 34
        end
        object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDNAZIV'
          Width = 69
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Width = 50
        end
        object cxGrid1DBTableView1ARTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTNAZIV'
          Width = 209
        end
        object cxGrid1DBTableView1STAVKA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'STAVKA_NAZIV'
          Width = 209
        end
        object cxGrid1DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Width = 57
        end
        object cxGrid1DBTableView1STAVKA_MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'STAVKA_MERKA'
          Width = 80
        end
        object cxGrid1DBTableView1KATALOG: TcxGridDBColumn
          DataBinding.FieldName = 'KATALOG'
          Width = 76
        end
        object cxGrid1DBTableView1PLANIRANA_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'PLANIRANA_KOLICINA'
          Width = 92
        end
        object cxGrid1DBTableView1RASPISHANA_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'RASPISHANA_KOLICINA'
          Width = 140
        end
        object cxGrid1DBTableView1DOBIENA_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'DOBIENA_KOLICINA'
          Width = 117
        end
        object cxGrid1DBTableView1POBARANA_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'POBARANA_KOLICINA'
          Width = 104
        end
        object cxGrid1DBTableView1ISPORACANA_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'ISPORACANA_KOLICINA'
          Width = 115
        end
        object cxGrid1DBTableView1CENAPLAN: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Width = 109
        end
        object cxGrid1DBTableView1IZNOS_PLAN: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_PLAN'
          Width = 114
        end
        object cxGrid1DBTableView1PONUDENACENA: TcxGridDBColumn
          DataBinding.FieldName = 'PONUDENACENA'
          Width = 122
        end
        object cxGrid1DBTableView1IZNOS_PONUDA_EDINICEN: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_PONUDA_EDINICEN'
          Width = 123
        end
        object cxGrid1DBTableView1CENA_BARANJE: TcxGridDBColumn
          DataBinding.FieldName = 'CENA_BARANJE'
          Width = 121
        end
        object cxGrid1DBTableView1IZNOS_POBARAN_EDINICEN: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_POBARAN_EDINICEN'
          Width = 123
        end
        object cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_DOGOVOR'
          Width = 91
        end
        object cxGrid1DBTableView1DATUM_DOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DOGOVOR'
          Width = 98
        end
        object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_VAZENJE'
          Width = 158
        end
        object cxGrid1DBTableView1VKUPNO_PLAN_IZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'VKUPNO_PLAN_IZNOS'
          HeaderHint = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1055#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
          Width = 111
        end
        object cxGrid1DBTableView1IZNOS_ODLUKA: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_ODLUKA'
          HeaderHint = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          Width = 212
        end
        object cxGrid1DBTableView1PONUDENIZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'PONUDENIZNOS'
          HeaderHint = 
            #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1055#1086#1085#1091#1076#1072' '#1085#1072' '#1076#1086#1073#1080#1090#1085#1080#1094#1080' '#1079#1072' '#1094#1077#1083#1072#1090#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1085#1072' '#1112#1072#1074#1085#1072' ' +
            #1085#1072#1073#1072#1074#1082#1072
          Width = 174
        end
        object cxGrid1DBTableView1IZNOS_DOGOVOR_STAVKI: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_DOGOVOR_STAVKI'
          Width = 132
        end
        object cxGrid1DBTableView1IZNOS_POBARAN: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_POBARAN'
          HeaderHint = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1041#1072#1088#1072#1114#1072' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
          Width = 126
        end
        object cxGrid1DBTableView1IZNOS_FAKTURIRAN: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS_FAKTURIRAN'
          Width = 151
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 503
    Width = 1362
    Height = 215
    Align = alClient
    TabOrder = 3
    object cxPageControl1: TcxPageControl
      Left = 1
      Top = 1
      Width = 1360
      Height = 213
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 213
      ClientRectRight = 1360
      ClientRectTop = 24
      object cxTabSheet1: TcxTabSheet
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072
        ImageIndex = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1360
          Height = 189
          Align = alClient
          TabOrder = 0
          object cxGrid2DBChartView1: TcxGridDBChartView
            Categories.DataBinding.FieldName = 'ARTNAZIV_OUT'
            DataController.DataSource = dm.dsRealizacijaPlanNabavkaRe
            DiagramColumn.Active = True
            ToolBox.DiagramSelector = True
            object cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'RENAZIV'
            end
            object cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'VIDNAZIV'
            end
            object cxGrid2DBChartView1DataGroup3: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'ARTNAZIV'
            end
            object cxGrid2DBChartView1Series1: TcxGridDBChartSeries
              DataBinding.FieldName = 'PLANIRANA_KOLICINA'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGrid2DBChartView1Series2: TcxGridDBChartSeries
              DataBinding.FieldName = 'RASPISHANA_KOLICINA'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGrid2DBChartView1Series3: TcxGridDBChartSeries
              DataBinding.FieldName = 'DOBIENA_KOLICINA'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGrid2DBChartView1Series5: TcxGridDBChartSeries
              DataBinding.FieldName = 'POBARANA_KOLICINA'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGrid2DBChartView1Series4: TcxGridDBChartSeries
              DataBinding.FieldName = 'ISPORACANA_KOLICINA'
              ValueCaptionFormat = '0.00 , .'
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBChartView1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1062#1077#1085#1072
        ImageIndex = 1
        object cxGrid3: TcxGrid
          Left = 0
          Top = 0
          Width = 1360
          Height = 189
          Align = alClient
          TabOrder = 0
          object cxGridDBChartView1: TcxGridDBChartView
            Categories.DataBinding.FieldName = 'ARTNAZIV_OUT'
            DataController.DataSource = dm.dsRealizacijaPlanNabavkaRe
            DiagramColumn.Active = True
            ToolBox.DiagramSelector = True
            object cxGridDBChartDataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'RENAZIV'
            end
            object cxGridDBChartDataGroup2: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'VIDNAZIV'
            end
            object cxGridDBChartDataGroup3: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'ARTNAZIV'
            end
            object cxGridDBChartSeries6: TcxGridDBChartSeries
              DataBinding.FieldName = 'CENA'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGridDBChartSeries7: TcxGridDBChartSeries
              DataBinding.FieldName = 'PONUDENACENA'
              ValueCaptionFormat = '0.00 , .'
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBChartView1
          end
        end
      end
      object cxTabSheet3: TcxTabSheet
        Caption = #1048#1079#1085#1086#1089
        ImageIndex = 2
        object cxGrid4: TcxGrid
          Left = 0
          Top = 0
          Width = 1360
          Height = 189
          Align = alClient
          TabOrder = 0
          object cxGridDBChartView2: TcxGridDBChartView
            Categories.DataBinding.FieldName = 'ARTNAZIV_OUT'
            DataController.DataSource = dm.dsRealizacijaPlanNabavkaRe
            DiagramColumn.Active = True
            ToolBox.DiagramSelector = True
            object cxGridDBChartView2DataGroup1: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'BROJ_TENDER'
            end
            object cxGridDBChartView2DataGroup2: TcxGridDBChartDataGroup
              DataBinding.FieldName = 'RENAZIV'
            end
            object cxGridDBChartView2Series1: TcxGridDBChartSeries
              DataBinding.FieldName = 'IZNOS_PLAN'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGridDBChartSeries19: TcxGridDBChartSeries
              DataBinding.FieldName = 'IZNOS_ODLUKA'
              GroupSummaryKind = skMax
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGridDBChartSeries21: TcxGridDBChartSeries
              DataBinding.FieldName = 'PONUDENIZNOS'
              GroupSummaryKind = skMax
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGridDBChartSeries20: TcxGridDBChartSeries
              DataBinding.FieldName = 'IZNOS_POBARAN_EDINICEN'
              ValueCaptionFormat = '0.00 , .'
            end
            object cxGridDBChartSeries22: TcxGridDBChartSeries
              DataBinding.FieldName = 'IZNOS_FAKTURIRAN'
              ValueCaptionFormat = '0.00 , .'
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBChartView2
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 512
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 245
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 224
    Top = 360
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 448
    Top = 336
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
end
