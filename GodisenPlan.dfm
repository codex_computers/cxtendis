object frmGodisenPlan: TfrmGodisenPlan
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
  ClientHeight = 813
  ClientWidth = 1272
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1272
    813)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1272
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1BarOdlukaNabavka'
        end
        item
          Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
          ToolbarName = 'dxBarManager1Bar'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 790
    Width = 1272
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080
        Width = 320
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080', Ctrl+F8 - '#1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F7 - '#1054#1089#1074#1077#1078#1080 +
          ' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085', Ctrl+D - '#1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
        Width = 645
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelPlanOdluka: TPanel
    Left = 0
    Top = 126
    Width = 1272
    Height = 187
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxPageControlPlanOdluka: TcxPageControl
      Left = 2
      Top = 43
      Width = 1268
      Height = 142
      Align = alClient
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheetTabelaren
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 142
      ClientRectRight = 1268
      ClientRectTop = 24
      object cxTabSheetTabelaren: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 0
        object PanelTabelarenPrikaz: TPanel
          Left = 0
          Top = 0
          Width = 1268
          Height = 118
          Align = alClient
          TabOrder = 0
          object cxGrid1: TcxGrid
            Left = 1
            Top = 1
            Width = 1266
            Height = 116
            Align = alClient
            TabOrder = 0
            object cxGrid1DBTableView1: TcxGridDBTableView
              OnKeyPress = cxGrid1DBTableView1KeyPress
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dm.dsGodPlanOdlulka
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
              FilterRow.Visible = True
              OptionsBehavior.IncSearch = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1086#1076#1083#1091#1082#1072
              OptionsView.GroupByBox = False
              object cxGrid1DBTableView1GODINA: TcxGridDBColumn
                DataBinding.FieldName = 'GODINA'
                Visible = False
                Options.Editing = False
              end
              object cxGrid1DBTableView1BROJ: TcxGridDBColumn
                DataBinding.FieldName = 'BROJ'
                Options.Editing = False
                Width = 85
              end
              object cxGrid1DBTableView1DATUM: TcxGridDBColumn
                DataBinding.FieldName = 'DATUM'
                Options.Editing = False
                Width = 87
              end
              object cxGrid1DBTableView1PREDMET: TcxGridDBColumn
                DataBinding.FieldName = 'PREDMET'
                PropertiesClassName = 'TcxBlobEditProperties'
                Properties.BlobEditKind = bekMemo
                Properties.BlobPaintStyle = bpsText
                Properties.PopupHeight = 140
                Properties.PopupWidth = 800
                Properties.ReadOnly = True
                Width = 800
              end
              object cxGrid1DBTableView1OPIS: TcxGridDBColumn
                DataBinding.FieldName = 'OPIS'
                PropertiesClassName = 'TcxRichEditProperties'
                Properties.MemoMode = True
                Properties.OEMConvert = True
                Properties.PlainText = True
                Properties.ReadOnly = True
                Width = 159
              end
              object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
                DataBinding.FieldName = 'TS_INS'
                Visible = False
                Options.Editing = False
                Width = 200
              end
              object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
                DataBinding.FieldName = 'TS_UPD'
                Visible = False
                Options.Editing = False
                Width = 200
              end
              object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
                DataBinding.FieldName = 'USR_INS'
                Visible = False
                Options.Editing = False
                Width = 200
              end
              object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
                DataBinding.FieldName = 'USR_UPD'
                Visible = False
                Options.Editing = False
                Width = 200
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBTableView1
            end
          end
        end
      end
      object cxTabSheetDetalen: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object PanelDetalenPrikaz: TPanel
          Left = 0
          Top = 0
          Width = 1268
          Height = 118
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            1268
            118)
          object Label2: TLabel
            Left = 47
            Top = 49
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 47
            Top = 22
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1043#1086#1076#1080#1085#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 30
            Top = 76
            Width = 67
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label5: TLabel
            Left = 232
            Top = 22
            Width = 107
            Height = 35
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1086#1076#1083#1091#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label6: TLabel
            Left = 218
            Top = 60
            Width = 121
            Height = 28
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1086#1076#1083#1091#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object BROJ: TcxDBTextEdit
            Tag = 1
            Left = 103
            Top = 46
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsGodPlanOdlulka
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object GODINA: TcxDBComboBox
            Tag = 1
            Left = 103
            Top = 19
            Hint = #1043#1086#1076#1080#1085#1072' '#1085#1072' '#1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1076#1083#1091#1082#1072
            DataBinding.DataField = 'GODINA'
            DataBinding.DataSource = dm.dsGodPlanOdlulka
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              '2010'
              '2011'
              '2012'
              '2013'
              '2014'
              '2015'
              '2016'
              '2017'
              '2018'
              '2019'
              '2020'
              '2021'
              '2022'
              '2023'
              '2024'
              '2025'
              '2026'
              '2027'
              '2028'
              '2029'
              '2030'
              '2031'
              '2032'
              '2033'
              '2034'
              '2035'
              '2036'
              '2037'
              '2038'
              '2039'
              '2040'
              '2041'
              '2042'
              '2043'
              '2044'
              '2045'
              '2046'
              '2047'
              '2048'
              '2049'
              '2050')
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object DATUM: TcxDBDateEdit
            Left = 103
            Top = 73
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1076#1086#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1076#1083#1091#1082#1072
            DataBinding.DataField = 'DATUM'
            DataBinding.DataSource = dm.dsGodPlanOdlulka
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object PREDMET: TcxDBMemo
            Left = 345
            Top = 19
            Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'PREDMET'
            DataBinding.DataSource = dm.dsGodPlanOdlulka
            Properties.WantReturns = False
            TabOrder = 3
            OnDblClick = PREDMETDblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 35
            Width = 712
          end
          object ZapisiButton: TcxButton
            Left = 1079
            Top = 69
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 5
          end
          object OtkaziButton: TcxButton
            Left = 1160
            Top = 69
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 6
          end
          object OPIS: TcxDBRichEdit
            Left = 345
            Top = 60
            Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsGodPlanOdlulka
            ParentFont = False
            Properties.WantReturns = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clBlack
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 4
            OnDblClick = OPISDblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 35
            Width = 712
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 2
      Width = 1268
      Height = 41
      Align = alTop
      Alignment = taLeftJustify
      Color = clCream
      ParentBackground = False
      TabOrder = 1
      object Label7: TLabel
        Left = 10
        Top = 16
        Width = 307
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076#1083#1091#1082#1080' '#1079#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1080' '#1080#1079#1084#1077#1085#1072' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object PanelPlanSektori: TPanel
    Left = 0
    Top = 321
    Width = 1272
    Height = 469
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 2
      Top = 57
      Width = 1268
      Height = 410
      Align = alClient
      TabOrder = 0
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid2DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsGodPlanSektori
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = '0.0000, .'
            Position = spFooter
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.00 , .'
            Kind = skSum
          end
          item
            Format = '0.00 , .'
            Kind = skSum
          end
          item
            Format = '0.00 , .'
            Kind = skSum
          end
          item
            Format = '0.00 , .'
            Kind = skSum
          end
          item
            Format = '0.00 , .'
            Kind = skSum
          end
          item
            Format = '0.00, .'
            Kind = skSum
            Column = cxGrid2DBTableView1IZNOSCENA
          end
          item
            Format = '0.00, .'
            Kind = skSum
            Column = cxGrid2DBTableView1IZNOSCENBEZDDV
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1086#1076#1083#1091#1082#1072' '
        OptionsView.Footer = True
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_TENDER'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'JN_TIP_ARTIKAL'
          Options.Editing = False
          Width = 29
        end
        object cxGrid2DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDARTIKALNAZIV'
          Options.Editing = False
          Width = 65
        end
        object cxGrid2DBTableView1GENERIKA: TcxGridDBColumn
          DataBinding.FieldName = 'GENERIKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 1
            end>
          Options.Editing = False
          Width = 66
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Options.Editing = False
          Width = 55
        end
        object cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKALNAZIV'
          Options.Editing = False
          Width = 325
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Options.Editing = False
          Width = 42
        end
        object cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Options.Editing = False
          Width = 77
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 60
        end
        object cxGrid2DBTableView1CENABEZDDV: TcxGridDBColumn
          DataBinding.FieldName = 'CENABEZDDV'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 79
        end
        object cxGrid2DBTableView1DANOK: TcxGridDBColumn
          DataBinding.FieldName = 'DANOK'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 35
        end
        object cxGrid2DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 69
        end
        object cxGrid2DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOSCENBEZDDV'
          Options.Editing = False
          Width = 101
        end
        object cxGrid2DBTableView1IZNOSCENA: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOSCENA'
          Options.Editing = False
          Width = 93
        end
        object cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'PROZVODITEL'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PROIZVODITELNAZIV'
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Options.Editing = False
          Width = 37
        end
        object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Options.Editing = False
          Width = 115
        end
        object cxGrid2DBTableView1JN_CPV: TcxGridDBColumn
          DataBinding.FieldName = 'JN_CPV'
          Options.Editing = False
          Width = 113
        end
        object cxGrid2DBTableView1MK: TcxGridDBColumn
          DataBinding.FieldName = 'MK'
          Options.Editing = False
          Width = 102
        end
        object cxGrid2DBTableView1EN: TcxGridDBColumn
          DataBinding.FieldName = 'EN'
          Options.Editing = False
          Width = 95
        end
        object cxGrid2DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid2DBTableView1PREDMET_NABAVKA: TcxGridDBColumn
          DataBinding.FieldName = 'PREDMET_NABAVKA'
          Width = 200
        end
        object cxGrid2DBTableView1MESEC: TcxGridDBColumn
          DataBinding.FieldName = 'MESEC'
          Options.Editing = False
          Width = 136
        end
        object cxGrid2DBTableView1TIP_NABAVKA: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_NABAVKA'
          Visible = False
          Options.Editing = False
          Width = 92
        end
        object cxGrid2DBTableView1TIPNABAVKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIPNABAVKANAZIV'
          Options.Editing = False
          Width = 190
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object PanelIzborSektor: TPanel
      Left = 2
      Top = 2
      Width = 1268
      Height = 55
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      Color = clCream
      ParentBackground = False
      TabOrder = 1
      DesignSize = (
        1268
        55)
      object Label1: TLabel
        Left = 15
        Top = 29
        Width = 231
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1079#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 14
        Top = 10
        Width = 105
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxDBLabel1: TcxDBLabel
        Left = 125
        Top = 8
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsGodPlanOdlulka
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
        Height = 19
        Width = 76
      end
      object cbPodsektori: TcxTextEdit
        Left = 248
        Top = 26
        BeepOnEnter = False
        Properties.OnEditValueChanged = cbPodsektoriPropertiesEditValueChanged
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 65
      end
      object cbPodsektoriTree: TdxLookupTreeView
        Left = 313
        Top = 26
        Width = 752
        Height = 21
        CanSelectParents = True
        ParentColor = False
        TabOrder = 2
        TabStop = True
        Text = ''
        TreeViewColor = clWindow
        TreeViewCursor = crDefault
        TreeViewFont.Charset = DEFAULT_CHARSET
        TreeViewFont.Color = clWindowText
        TreeViewFont.Height = -11
        TreeViewFont.Name = 'Tahoma'
        TreeViewFont.Style = []
        TreeViewIndent = 19
        TreeViewReadOnly = True
        TreeViewShowButtons = True
        TreeViewShowHint = False
        TreeViewShowLines = True
        TreeViewShowRoot = True
        TreeViewSortType = stNone
        OnCloseUp = cbPodsektoriTreeCloseUp
        OnEnter = cxDBTextEditAllEnter
        OnExit = cbPodsektoriTreeExit
        OnKeyDown = EnterKakoTab
        Anchors = [akLeft, akTop, akRight, akBottom]
        DisplayField = 'ID;NAZIV'
        DividedChar = '.'
        ListSource = dm.dsPodsektoriGodisenPlan
        KeyField = 'ID'
        ListField = 'NAZIV'
        Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
        ParentField = 'KOREN'
        RootValue = Null
        TextStyle = tvtsShort
        Alignment = taLeftJustify
      end
      object cxButton2: TcxButton
        Left = 1071
        Top = 24
        Width = 102
        Height = 25
        Action = aPrebarajRE
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 3
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        OnKeyDown = EnterKakoTab
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 313
    Width = 1272
    Height = 8
    AlignSplitter = salTop
    MinSize = 180
    Control = PanelPlanOdluka
  end
  object PanelIzborOdluka: TPanel
    Left = 419
    Top = 296
    Width = 487
    Height = 292
    Anchors = []
    BevelOuter = bvNone
    Color = 11302478
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 5
    Visible = False
    DesignSize = (
      487
      292)
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 487
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1082#1086#1112#1072' '#1089#1072#1082#1072#1090#1077' '#1076#1072' '#1082#1088#1077#1080#1088#1072#1090#1077' '#1087#1088#1077#1076#1083#1086#1075' '#1087#1083#1072#1085
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      ExplicitTop = -8
    end
    object Panel2: TPanel
      Left = 0
      Top = 39
      Width = 487
      Height = 211
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        487
        211)
      object cxGrid3: TcxGrid
        Left = 23
        Top = 2
        Width = 441
        Height = 209
        Anchors = [akRight, akBottom]
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsPredlogOdluki
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.GroupByBox = False
          object cxGrid3DBTableView1GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'GODINA'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Width = 83
          end
          object cxGrid3DBTableView1PREDMET: TcxGridDBColumn
            DataBinding.FieldName = 'PREDMET'
            Width = 368
          end
          object cxGrid3DBTableView1OPIS: TcxGridDBColumn
            DataBinding.FieldName = 'OPIS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 150
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
    object btnOK: TcxButton
      Left = 14
      Top = 256
      Width = 91
      Height = 25
      Action = aKreirajPredlogPlanIzbor
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton1: TcxButton
      Left = 111
      Top = 256
      Width = 91
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 968
    Top = 280
  end
  object PopupMenu1: TPopupMenu
    Left = 520
    Top = 264
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 792
    Top = 272
    PixelsPerInch = 96
    object dxBarManager1BarOdlukaNabavka: TdxBar
      Caption = #1054#1076#1083#1091#1082'a '#1079#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1080' '#1080#1079#1084#1077#1085#1072' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 50
          Visible = True
          ItemName = 'cbGodina'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 849
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1180
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1086#1076#1083#1091#1082#1080' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1086#1076#1083#1091#1082#1080' '
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar: TdxBar
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      CaptionButtons = <>
      DockedLeft = 469
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      CaptionButtons = <>
      DockedLeft = 929
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      CaptionButtons = <>
      DockedLeft = 771
      DockedTop = 0
      FloatLeft = 1075
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = '  '
      CaptionButtons = <>
      DockedLeft = 674
      DockedTop = 0
      FloatLeft = 1281
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1086#1076#1083#1091#1082#1080
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabelaPlanOdluka
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1086#1076#1083#1091#1082#1080
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object cbGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      Items.Strings = (
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      ItemIndex = -1
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavkiVoPlan
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavkaOdGodPlan
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072
      Category = 0
      Visible = ivAlways
      ShortCut = 16501
      LargeImageIndex = 12
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = #1054#1089#1074#1077#1078#1080
      Category = 0
      Visible = ivAlways
      ShortCut = 16502
      OnClick = aRefreshGodisenPlanSektorExecute
      LargeImageIndex = 52
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aZacuvajExcelStavki
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabelaStavki
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aPageSetupStavki
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aSnimiPecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSnimiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisiIzgledStavki
      Category = 0
      LargeImageIndex = 56
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aKreirajPredlogPlan
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxTrackBarProperties'
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aRefreshGodisenPlan
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aDinamikaRealizacija
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = False
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'btnPredlogPlan'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = aPGodisenPlan
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aPGodisenPlanPoSektori
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aPGodisenPlanIzbranSektor
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aPGodisenPlanNaPodsektoriNaIzbranSektor
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aPGodisenPlanOdJN
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Action = aPOdlukaKreiranjeIzmenaGO
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1076#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072')'
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object btnPredlogPlan: TdxBarButton
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1087#1083#1072#1085
      Category = 0
      Hint = #1055#1088#1077#1076#1083#1086#1075' '#1087#1083#1072#1085
      Visible = ivAlways
      ImageIndex = 19
      OnClick = btnPredlogPlanClick
    end
    object dxBarButton9: TdxBarButton
      Action = aPGodisenPlanPoSektoriKolicina
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = actPDinamikaNaRealizacija
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 352
    Top = 128
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1086#1076#1083#1091#1082#1080' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1080
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabelaPlanOdluka: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '
      ImageIndex = 30
      OnExecute = aPecatiTabelaPlanOdlukaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aDodadiStavkiVoPlan: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aDodadiStavkiVoPlanExecute
    end
    object aBrisiStavkaOdGodPlan: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 46
      ShortCut = 16503
      OnExecute = aBrisiStavkaOdGodPlanExecute
    end
    object aZacuvajExcelStavki: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcelStavkiExecute
    end
    object aPecatiTabelaStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaStavkiExecute
    end
    object aPodesuvanjePecatenjeStavki: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeStavkiExecute
    end
    object aPageSetupStavki: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupStavkiExecute
    end
    object aSnimiPecatenjeStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeStavkiExecute
    end
    object aBrisiPodesuvanjePecatenjeStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeStavkiExecute
    end
    object aSnimiIzgledStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledStavkiExecute
    end
    object aBrisiIzgledStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      OnExecute = aBrisiIzgledStavkiExecute
    end
    object aKreirajPredlogPlan: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1087#1088#1077#1076#1083#1086#1075' '#1087#1083#1072#1085' '#1079#1072' '#1089#1083#1077#1076#1085#1072#1090#1072' '#1075#1086#1076#1080#1085#1072
      ImageIndex = 8
      OnExecute = aKreirajPredlogPlanExecute
    end
    object aKreirajPredlogPlanIzbor: TAction
      Caption = 'OK'
      ImageIndex = 6
      OnExecute = aKreirajPredlogPlanIzborExecute
    end
    object aKreirajPredlogPlanOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aKreirajPredlogPlanOtkaziExecute
    end
    object aRefreshGodisenPlan: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 52
      ShortCut = 16502
      OnExecute = aRefreshGodisenPlanExecute
    end
    object aDinamikaRealizacija: TAction
      Caption = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      ImageIndex = 41
      ShortCut = 16452
      OnExecute = aDinamikaRealizacijaExecute
    end
    object aPGodisenPlan: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aPGodisenPlanExecute
    end
    object aDizajnGodisenPlan: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aDizajnGodisenPlanExecute
    end
    object aPopUpMeni: TAction
      Caption = 'aPopUpMeni'
      ShortCut = 16505
      OnExecute = aPopUpMeniExecute
    end
    object aPGodisenPlanPoSektori: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      ImageIndex = 19
      OnExecute = aPGodisenPlanPoSektoriExecute
    end
    object aDizajnGodisenPlanPoSektori: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      ImageIndex = 19
      OnExecute = aDizajnGodisenPlanPoSektoriExecute
    end
    object aPGodisenPlanIzbranSektor: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      ImageIndex = 19
      OnExecute = aPGodisenPlanIzbranSektorExecute
    end
    object aDizajnGodisenPlanZaIzbranSektor: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      ImageIndex = 19
      OnExecute = aDizajnGodisenPlanZaIzbranSektorExecute
    end
    object aPGodisenPlanNaPodsektoriNaIzbranSektor: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1087#1086#1076#1089#1077#1082#1090#1086#1088#1080'/'#1086#1076#1077#1083#1077#1085#1080#1112#1072' '#1085#1072' '#1089#1077#1082#1090#1086#1088
      ImageIndex = 19
      OnExecute = aPGodisenPlanNaPodsektoriNaIzbranSektorExecute
    end
    object aDizajnGodisenPlanNaPodsektoriNaIzbranSektor: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1087#1086#1076#1089#1077#1082#1090#1086#1088#1080'/'#1086#1076#1077#1083#1077#1085#1080#1112#1072' '#1085#1072' '#1089#1077#1082#1090#1086#1088
      ImageIndex = 19
      OnExecute = aDizajnGodisenPlanNaPodsektoriNaIzbranSektorExecute
    end
    object aPGodisenPlanOdJN: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080')'
      ImageIndex = 19
      OnExecute = aPGodisenPlanOdJNExecute
    end
    object aDGodisenPlanOdJN: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080')'
      ImageIndex = 19
      OnExecute = aDGodisenPlanOdJNExecute
    end
    object aPOdlukaKreiranjeIzmenaGO: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1080' '#1080#1079#1084#1077#1085#1072' '#1085#1072' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aPOdlukaKreiranjeIzmenaGOExecute
    end
    object aDOdlukaKreiranjeIzmenaGO: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1080' '#1080#1079#1084#1077#1085#1072' '#1085#1072' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aDOdlukaKreiranjeIzmenaGOExecute
    end
    object aPrebarajRE: TAction
      Caption = #1055#1088#1077#1073#1072#1088#1072#1112
      ImageIndex = 22
      OnExecute = aPrebarajREExecute
    end
    object aDizajnPredlogPlan: TAction
      ImageIndex = 19
      OnExecute = aDizajnPredlogPlanExecute
    end
    object aPGodisenPlanPoSektoriKolicina: TAction
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      ImageIndex = 19
      OnExecute = aPGodisenPlanPoSektoriKolicinaExecute
    end
    object aDGodisenPlanPoSektoriKolicina: TAction
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      ImageIndex = 19
    end
    object aGodisenPlanPrava: TAction
      Caption = 'aGodisenPlanPrava'
      OnExecute = aGodisenPlanPravaExecute
    end
    object actPDinamikaNaRealizacija: TAction
      Caption = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = actPDinamikaNaRealizacijaExecute
    end
    object actDDinamikaNaRealizacija: TAction
      Caption = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      OnExecute = actDDinamikaNaRealizacijaExecute
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 656
    Top = 280
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 872
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44287.568158333340000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 20000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44287.568158356480000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 568
    Top = 136
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 208
    Top = 520
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object tblPredlokOdluki: TpFIBDataSet
    SelectSQL.Strings = (
      'select o.broj,'
      '       o.godina,'
      '       o.predmet,'
      '       o.opis,'
      '       o.datum,'
      '       o.ts_ins,'
      '       o.ts_upd,'
      '       o.usr_ins,'
      '       o.usr_upd'
      'from jn_god_plan_odluka o'
      'where o.godina = :godina'
      'order by o.broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 56
    Top = 480
    object tblPredlokOdlukiBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlokOdlukiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPredlokOdlukiPREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090'/'#1086#1087#1080#1089' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlokOdlukiOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblPredlokOdlukiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblPredlokOdlukiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPredlokOdlukiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPredlokOdlukiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlokOdlukiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPredlogOdluki: TDataSource
    DataSet = tblPredlokOdluki
    Left = 456
    Top = 256
  end
  object PopupMenu2: TPopupMenu
    Left = 160
    Top = 256
    object TMenuItem
    end
    object N2: TMenuItem
      Action = aDizajnGodisenPlan
    end
    object N6: TMenuItem
      Action = aDGodisenPlanOdJN
    end
    object N3: TMenuItem
      Action = aDizajnGodisenPlanPoSektori
    end
    object N4: TMenuItem
      Action = aDizajnGodisenPlanZaIzbranSektor
    end
    object N5: TMenuItem
      Action = aDizajnGodisenPlanNaPodsektoriNaIzbranSektor
    end
    object N7: TMenuItem
      Action = aDOdlukaKreiranjeIzmenaGO
    end
    object N8: TMenuItem
      Action = aDizajnPredlogPlan
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
    end
    object N9: TMenuItem
      Action = actDDinamikaNaRealizacija
    end
  end
  object qCountRe: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      ''
      ''
      'select count(mr.id) as cRe'
      'from mat_re mr'
      
        'where mr.id = :re and mr.r = 1 and mr.spisok like :roditel || '#39'%' +
        #39
      
        '      and ((mr.id in (select s.re from sys_user_re_app s where s' +
        '.username = :username and s.app = :APP  and :param = 1)'
      '          ) or (:param = 0))'
      '')
    Left = 1072
    Top = 160
  end
  object qRokGodisenPlan: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select case when s.v1 < current_date then 0'
      '            when s.v1 >= current_date then 1'
      '       end rok'
      'from jn_setup s'
      'where s.p1 = '#39'rok_godisen_plan'#39
      '--where s.p1 = '#39'JN'#39' and s.p2 = '#39'rok_godisen_plan'#39)
    Left = 368
    Top = 408
  end
  object qRabotnaGodina: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1'
      'from jn_setup s'
      'where (s.p1 = '#39'rabotna_godina'#39')and s.p2 = '#39'JN'#39)
    Left = 280
    Top = 366
  end
  object qUserPrava: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(s.v1) br'
      'from jn_setup s'
      
        'where (s.p1 like '#39'plan_user_prava'#39'||'#39'%'#39')and s.p2 = '#39'JN'#39' and s.v1' +
        ' = :user')
    Left = 168
    Top = 286
  end
end
