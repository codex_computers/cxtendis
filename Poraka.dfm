object frmPoraka: TfrmPoraka
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 137
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 257
    Height = 73
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object Comments: TLabel
      Left = 16
      Top = 16
      Width = 224
      Height = 39
      Caption = 
        #1053#1077' '#1077' '#1076#1086#1079#1074#1086#1083#1077#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077'. '#1054#1074#1072#1072' '#1089#1090#1072#1074#1082#1072' '#1077' '#1076#1086#1076#1072#1076#1077#1085#1072' '#1074#1086' '#1054#1090#1074#1086#1088#1077#1085#1072' '#1087#1086#1089 +
        #1090#1072#1087#1082#1072' '#1079#1072' '#1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      IsControl = True
    end
  end
  object OKButton: TButton
    Left = 95
    Top = 95
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
end
