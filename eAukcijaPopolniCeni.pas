unit eAukcijaPopolniCeni;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.ActnList,
  cxGroupBox, cxRadioGroup, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, Vcl.ComCtrls, FIBDataSet, pFIBDataSet, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  System.Actions;

type
  TfrmAukcijaPopolniCeni = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxRadioGroup1: TcxRadioGroup;
    ActionList1: TActionList;
    aIzlez: TAction;
    tblStavkiOdPonuda: TpFIBDataSet;
    dsStavkiOdPonuda: TDataSource;
    tblStavkiOdPonudaVIDARTIKALNAZIV: TFIBStringField;
    tblStavkiOdPonudaARTIKAL: TFIBIntegerField;
    tblStavkiOdPonudaARTIKALNAZIV: TFIBStringField;
    tblStavkiOdPonudaMERKA: TFIBStringField;
    tblStavkiOdPonudaMERKANAZIV: TFIBStringField;
    tblStavkiOdPonudaJN_TIP_ARTIKAL: TFIBIntegerField;
    tblStavkiOdPonudaGRUPA: TFIBStringField;
    tblStavkiOdPonudaGRUPANAZIV: TFIBStringField;
    tblStavkiOdPonudaKOLICINA: TFIBBCDField;
    tblStavkiOdPonudaCENA: TFIBFloatField;
    tblStavkiOdPonudaCENABEZDDV: TFIBFloatField;
    tblStavkiOdPonudaDANOK: TFIBFloatField;
    tblStavkiOdPonudaCENA_PRVA_BEZDDV: TFIBFloatField;
    tblStavkiOdPonudaIZNOSCENA: TFIBFloatField;
    tblStavkiOdPonudaIZNOSCENBEZDDV: TFIBFloatField;
    cxGrid1DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1CENABEZDDV: TcxGridDBColumn;
    cxGrid1DBTableView1DANOK: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_PRVA_BEZDDV: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOSCENA: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn;
    aBrisiIzgled: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    tblStavkiOdPonudaODBIENA: TFIBIntegerField;
    cxGrid1DBTableView1ODBIENA: TcxGridDBColumn;
    tblStavkiOdPonudaSTAVKA_NAZIV: TFIBStringField;
    tblStavkiOdPonudaSTAVKA_MERKA: TFIBStringField;
    cxGrid1DBTableView1STAVKA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STAVKA_MERKA: TcxGridDBColumn;
    procedure aIzlezExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure cxRadioGroup1Click(Sender: TObject);
    procedure tblStavkiOdPonudaCENABEZDDVSetText(Sender: TField;
      const Text: string);
    procedure tblStavkiOdPonudaCENASetText(Sender: TField; const Text: string);
    procedure tblStavkiOdPonudaDANOKSetText(Sender: TField; const Text: string);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure tblStavkiOdPonudaIZNOSCENASetText(Sender: TField;
      const Text: string);
    procedure tblStavkiOdPonudaIZNOSCENBEZDDVSetText(Sender: TField;
      const Text: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAukcijaPopolniCeni: TfrmAukcijaPopolniCeni;

implementation

{$R *.dfm}

uses dmUnit, Ponudi, dmKonekcija, Utils, dmResources, dmProcedureQuey;

procedure TfrmAukcijaPopolniCeni.aBrisiIzgledExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid1DBTableView1);
     BrisiFormaIzgled(self);
end;

procedure TfrmAukcijaPopolniCeni.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmAukcijaPopolniCeni.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmAukcijaPopolniCeni.aZacuvajExcelExecute(Sender: TObject);
begin
      zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmAukcijaPopolniCeni.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1CENABEZDDV)
       and (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1CENA)
       and (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1DANOK)then
     begin
       if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1 );
     end;
end;

procedure TfrmAukcijaPopolniCeni.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
 if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1ODBIENA.Index] = 1) then
       AStyle := dmRes.RedLight;
end;

procedure TfrmAukcijaPopolniCeni.cxRadioGroup1Click(Sender: TObject);
begin
     if cxRadioGroup1.EditValue = 1 then
        begin
           cxGrid1DBTableView1CENABEZDDV.Styles.Content:=dmRes.cxStyle120;
           cxGrid1DBTableView1CENABEZDDV.Styles.Content.Font.Style:=[fsBold];
           cxGrid1DBTableView1CENABEZDDV.Options.Editing:=true;

           cxGrid1DBTableView1CENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENA.Options.Editing:=false;

           cxGrid1DBTableView1DANOK.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1DANOK.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1DANOK.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENA.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENBEZDDV.Options.Editing:=false;
        end
     else if cxRadioGroup1.EditValue = 2 then
        begin
           cxGrid1DBTableView1CENA.Styles.Content:=dmRes.cxStyle120;
           cxGrid1DBTableView1CENA.Styles.Content.Font.Style:=[fsBold];
           cxGrid1DBTableView1CENA.Options.Editing:=true;

           cxGrid1DBTableView1DANOK.Styles.Content:=dmRes.cxStyle120;
           cxGrid1DBTableView1DANOK.Styles.Content.Font.Style:=[fsBold];
           cxGrid1DBTableView1DANOK.Options.Editing:=true;

           cxGrid1DBTableView1CENABEZDDV.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENABEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENABEZDDV.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENA.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENBEZDDV.Options.Editing:=false;
        end
     else if cxRadioGroup1.EditValue = 3 then
        begin
           cxGrid1DBTableView1CENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENA.Options.Editing:=false;

           cxGrid1DBTableView1DANOK.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1DANOK.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1DANOK.Options.Editing:=false;

           cxGrid1DBTableView1CENABEZDDV.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENABEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENABEZDDV.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENA.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content:=dmRes.cxStyle120;
           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content.Font.Style:=[fsBold];
           cxGrid1DBTableView1IZNOSCENBEZDDV.Options.Editing:=true;
        end
     else if cxRadioGroup1.EditValue = 4 then
        begin
           cxGrid1DBTableView1CENA.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENA.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENA.Options.Editing:=false;

           cxGrid1DBTableView1DANOK.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1DANOK.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1DANOK.Options.Editing:=false;

           cxGrid1DBTableView1CENABEZDDV.Styles.Content:=dmRes.cxStyle176;
           cxGrid1DBTableView1CENABEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1CENABEZDDV.Options.Editing:=false;

           cxGrid1DBTableView1IZNOSCENA.Styles.Content:=dmRes.cxStyle120;
           cxGrid1DBTableView1IZNOSCENA.Styles.Content.Font.Style:=[fsBold];
           cxGrid1DBTableView1IZNOSCENA.Options.Editing:=true;

           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content:= dmRes.cxStyle176;
           cxGrid1DBTableView1IZNOSCENBEZDDV.Styles.Content.Font.Style:=[];
           cxGrid1DBTableView1IZNOSCENBEZDDV.Options.Editing:=false;
        end

end;

procedure TfrmAukcijaPopolniCeni.FormCreate(Sender: TObject);
begin
     

     tblStavkiOdPonuda.Close;
     tblStavkiOdPonuda.Open;
end;

procedure TfrmAukcijaPopolniCeni.FormShow(Sender: TObject);
begin
     cxGrid1.SetFocus;
     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
end;

procedure TfrmAukcijaPopolniCeni.tblStavkiOdPonudaCENABEZDDVSetText(
  Sender: TField; const Text: string);
begin
    tblStavkiOdPonuda.Edit;
    tblStavkiOdPonudaCENABEZDDV.Value:=StrToFloat(Text);
    tblStavkiOdPonudaCENA.Value:=(tblStavkiOdPonudacenabezddv.Value* tblStavkiOdPonudadanok.Value/100) + tblStavkiOdPonudacenabezddv.Value;
    tblStavkiOdPonuda.Post;
    ponuda_stavka_edit:=1;
end;

procedure TfrmAukcijaPopolniCeni.tblStavkiOdPonudaCENASetText(Sender: TField;
  const Text: string);
begin
   tblStavkiOdPonuda.Edit;
   tblStavkiOdPonudaCENA.Value:=StrToFloat(Text);
   tblStavkiOdPonudaCENABEZDDV.Value:=tblStavkiOdPonudaCENA.Value*100/(100 + tblStavkiOdPonudaDANOK.Value);
   tblStavkiOdPonuda.Post;
   ponuda_stavka_edit:=1;
end;

procedure TfrmAukcijaPopolniCeni.tblStavkiOdPonudaDANOKSetText(Sender: TField;
  const Text: string);
begin
   tblStavkiOdPonuda.Edit;
   tblStavkiOdPonudaDANOK.Value:=StrToFloat(Text);
   tblStavkiOdPonudaCENA.Value:=(tblStavkiOdPonudacenabezddv.Value* tblStavkiOdPonudadanok.Value/100) + tblStavkiOdPonudacenabezddv.Value;
   tblStavkiOdPonuda.Post;
   ponuda_stavka_edit:=1;
end;

procedure TfrmAukcijaPopolniCeni.tblStavkiOdPonudaIZNOSCENASetText(
  Sender: TField; const Text: string);
begin
   tblStavkiOdPonuda.Edit;
   tblStavkiOdPonudaCENA.Value:=StrToFloat(Text)/tblStavkiOdPonudaKOLICINA.Value;
   tblStavkiOdPonudaCENABEZDDV.Value:=tblStavkiOdPonudaCENA.Value*100/(100 + tblStavkiOdPonudaDANOK.Value);
   tblStavkiOdPonuda.Post;
   ponuda_stavka_edit:=1;
end;

procedure TfrmAukcijaPopolniCeni.tblStavkiOdPonudaIZNOSCENBEZDDVSetText(
  Sender: TField; const Text: string);
begin
   tblStavkiOdPonuda.Edit;
   tblStavkiOdPonudaCENABEZDDV.Value:=StrToFloat(Text)/tblStavkiOdPonudaKOLICINA.Value;
   tblStavkiOdPonudaCENA.Value:=(tblStavkiOdPonudacenabezddv.Value* tblStavkiOdPonudadanok.Value/100) + tblStavkiOdPonudacenabezddv.Value;
   tblStavkiOdPonuda.Post;
   ponuda_stavka_edit:=1;
end;

end.
