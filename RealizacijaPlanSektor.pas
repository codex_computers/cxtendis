unit RealizacijaPlanSektor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkSide, dxSkinGlassOceans, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,cxGridExportLink, cxExport,
  dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls,
  cxGridChartView, cxGridDBChartView, cxGridCustomPopupMenu, cxGridPopupMenu,
  ActnList, dxStatusBar, dxRibbonStatusBar, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinDarkRoom, dxSkinFoggy, dxSkinSeven, dxSkinSharp,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinsdxRibbonPainter, dxPSCore,
  dxPScxCommon,  dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxPCdxBarPopupMenu,
  cxPC, dxSkinOffice2013White, cxNavigator, dxBarBuiltInMenu, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmGrafikRealPlan = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    ActionList1: TActionList;
    aOtkaziIzlez: TAction;
    aZacuvajIzgled: TAction;
    aZacuvajVoExcel: TAction;
    aPecati: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxComponentPrinter1Link3: TdxCompositionReportLink;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KATALOG: TcxGridDBColumn;
    cxGrid1DBTableView1ARTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PLANIRANA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1CENAPLAN: TcxGridDBColumn;
    cxGrid1DBTableView1PONUDENACENA: TcxGridDBColumn;
    cxGrid1DBTableView1RASPISHANA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1DOBIENA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1ISPORACANA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1POBARANA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1PONUDENIZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_FAKTURIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_POBARAN: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_ODLUKA: TcxGridDBColumn;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBChartView1: TcxGridDBChartView;
    cxGrid2DBChartView1DataGroup1: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1DataGroup2: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1DataGroup3: TcxGridDBChartDataGroup;
    cxGrid2DBChartView1Series1: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series2: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series3: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series5: TcxGridDBChartSeries;
    cxGrid2DBChartView1Series4: TcxGridDBChartSeries;
    cxGrid2Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGridDBChartView1: TcxGridDBChartView;
    cxGridDBChartDataGroup1: TcxGridDBChartDataGroup;
    cxGridDBChartDataGroup2: TcxGridDBChartDataGroup;
    cxGridDBChartDataGroup3: TcxGridDBChartDataGroup;
    cxGridDBChartSeries6: TcxGridDBChartSeries;
    cxGridDBChartSeries7: TcxGridDBChartSeries;
    cxGridLevel1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGridDBChartView2: TcxGridDBChartView;
    cxGridDBChartSeries19: TcxGridDBChartSeries;
    cxGridDBChartSeries20: TcxGridDBChartSeries;
    cxGridDBChartSeries21: TcxGridDBChartSeries;
    cxGridDBChartSeries22: TcxGridDBChartSeries;
    cxGridLevel2: TcxGridLevel;
    cxGrid1DBTableView1IZNOS_PLAN: TcxGridDBColumn;
    cxGridDBChartView2Series1: TcxGridDBChartSeries;
    aBrisiIzgled: TAction;
    cxGridDBChartView2DataGroup1: TcxGridDBChartDataGroup;
    procedure FormCreate(Sender: TObject);
    procedure aOtkaziIzlezExecute(Sender: TObject);
    procedure aZacuvajIzgledExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrafikRealPlan: TfrmGrafikRealPlan;

implementation

uses dmKonekcija, dmMaticni, dmUnit, Utils, DinamikaRealizacija;

{$R *.dfm}

procedure TfrmGrafikRealPlan.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmGrafikRealPlan.aOtkaziIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmGrafikRealPlan.aPecatiExecute(Sender: TObject);
begin
      dxComponentPrinter1.CurrentLink.Preview();
end;

procedure TfrmGrafikRealPlan.aZacuvajIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true);
end;

procedure TfrmGrafikRealPlan.aZacuvajVoExcelExecute(Sender: TObject);
begin
 zacuvajVoExcel(cxGrid1, '���������� �� ����� �������');
end;

procedure TfrmGrafikRealPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGrafikRealPlan.FormCreate(Sender: TObject);
begin
     dm.tblRealizacijaPlanNabavkaRe.Close;
     dm.tblRealizacijaPlanNabavkaRe.Open;
end;

procedure TfrmGrafikRealPlan.FormShow(Sender: TObject);
begin
     procitajGridOdIni(cxGrid1DBTableView1,dmKon.aplikacija+Name,true,true);
     Caption:='���������� �� ����� ������� ��� '+ dm.tblTenderBROJ.Value;
end;

end.
