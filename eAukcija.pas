unit eAukcija;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.ComCtrls, cxGroupBox, cxRadioGroup, cxLabel,
  cxDBLabel, Vcl.StdCtrls, Vcl.ExtCtrls, cxTextEdit, Vcl.Menus, Vcl.ActnList,
  cxButtons, Data.DB, FIBDataSet, pFIBDataSet, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, FIBQuery, pFIBQuery,
  cxDBEdit, cxCalendar, cxMemo, cxRichEdit, cxDBRichEdit, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, System.Actions;

type
  TfrmAukcija = class(TForm)
    Panel1: TPanel;
    Label8: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxDBLabel2: TcxDBLabel;
    Label7: TLabel;
    StatusBar1: TStatusBar;
    Panel_1: TPanel;
    cxRadioGroup1: TcxRadioGroup;
    ActionList1: TActionList;
    aGenerirajCeni_1: TAction;
    Panel_2: TPanel;
    tblPodgrupa: TpFIBDataSet;
    dsPodgrupa: TDataSource;
    tblPodgrupaGRUPA: TFIBStringField;
    tblPodgrupaNAZIV: TFIBStringField;
    aIzlez: TAction;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    cxTextEditPosle: TcxTextEdit;
    Label2: TLabel;
    cxButton1: TcxButton;
    cxGroupBox2: TcxGroupBox;
    Label5: TLabel;
    grupa: TcxLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    cxTextEditPosleGrupa: TcxTextEdit;
    cxButton2: TcxButton;
    aGenerirajCeni_2: TAction;
    IznosPodgrupa: TcxTextEdit;
    qIznosPredPoGrupa: TpFIBQuery;
    qIznosPredCel: TpFIBQuery;
    IznosCel: TcxTextEdit;
    aPecatiCeniPredIPotoa: TAction;
    aDizajnCeniPredIPotoa: TAction;
    MainMenu1: TMainMenu;
    Panel2: TPanel;
    Label6: TLabel;
    cxButton3: TcxButton;
    aZacuvajSostojbaPredAukcija: TAction;
    LabelPoraka: TLabel;
    procedure aGenerirajCeni_1Execute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure cxRadioGroup1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure aGenerirajCeni_2Execute(Sender: TObject);
    procedure aPecatiCeniPredIPotoaExecute(Sender: TObject);
    procedure aDizajnCeniPredIPotoaExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
    Shift: TShiftState);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure aZacuvajSostojbaPredAukcijaExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAukcija: TfrmAukcija;
  pom_client_height:Integer;
implementation

{$R *.dfm}

uses dmKonekcija, Ponudi, dmUnit, eAukcijaPopolniCeni, dmProcedureQuey,
  dmResources, DaNe, Utils, EAukcijaOsnova;

procedure TfrmAukcija.aDizajnCeniPredIPotoaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40016);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmAukcija.aGenerirajCeni_1Execute(Sender: TObject);
var procent:double;
begin
  if Validacija(Panel_1) = false then
    begin
     procent:= 100*(1 - cxTextEditPosle.EditValue/IznosCel.EditValue);
     frmDaNe := TfrmDaNe.Create(self, '���������� �� ������� �� ��������', '������ ��������� ��������� ������� �� ������ ������ �� '+dm.tblPonudiPARTNERNAZIV.Value+' �� �� ������ �� '+FormatFloat('0.0000 , .', procent)+' ��������. ���� ��� ������� ���� ������ �� ���������� ?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
       begin
          dmPQ.insert6(dmpq.peAukcija,'PONUDA_ID', 'GRUPA', 'PROCENT',Null,Null,Null, dm.tblPonudiID.Value, Null,procent, Null,Null,Null);
          ShowMessage('��������� � ���������� ���������� �� ������ �� ��������. ������������ ��������� �������� �������� !!!');
          ponuda_stavka_edit:=1;
          dm.tblPonudiStavki.Close;
          dm.tblPonudiStavki.Open;
          dm.tblBodiranjeStavkiPonudi.Close;
          dm.tblBodiranjeStavkiPonudi.Open;
       end;
    end;
end;

procedure TfrmAukcija.aGenerirajCeni_2Execute(Sender: TObject);
var procent:double;
begin
   if Validacija(Panel_2) = false then
    begin
     procent:= 100*(1 - cxTextEditPosleGrupa.EditValue/IznosPodgrupa.EditValue);
     frmDaNe := TfrmDaNe.Create(self, '���������� �� ������� �� ��������', '������ ��������� ��������� ������� �� ������ ������ �� '+dm.tblPonudiPARTNERNAZIV.Value+' �� �� ������ �� '+FormatFloat('0.0000 , .', procent)+' ��������. ���� ��� ������� ���� ������ �� ���������� ?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
       begin
          dmPQ.insert6(dmpq.peAukcija,'PONUDA_ID', 'GRUPA', 'PROCENT',Null,Null,Null, dm.tblPonudiID.Value, grupa.EditValue,procent, Null,Null,Null);
          ShowMessage('��������� � ���������� ���������� �� ������ �� ��������. ������������ ��������� �������� �������� !!!');
          ponuda_stavka_edit:=1;
          dm.tblPonudiStavki.Close;
          dm.tblPonudiStavki.Open;
          dm.tblBodiranjeStavkiPonudi.Close;
          dm.tblBodiranjeStavkiPonudi.Open;
       end;
    end;
end;

procedure TfrmAukcija.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmAukcija.aPecatiCeniPredIPotoaExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40016);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmAukcija.aZacuvajSostojbaPredAukcijaExecute(Sender: TObject);
begin
     frmEAukcijaOsnova:=TfrmEAukcijaOsnova.Create(Application);
     frmEAukcijaOsnova.ShowModal;
     frmEAukcijaOsnova.Free;
end;

procedure TfrmAukcija.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
var sumaPoGrupaPred:double;
begin
     qIznosPredPoGrupa.Close;
     qIznosPredPoGrupa.ParamByName('mas_id').Value:=dm.tblPonudiID.Value;
     qIznosPredPoGrupa.ParamByName('grupa').Value:=grupa.EditValue;
     qIznosPredPoGrupa.ExecQuery;

     sumaPoGrupaPred:=qIznosPredPoGrupa.FldByName['SumaIznosPredAukcija'].Value;

     IznosPodgrupa.Text:=FormatFloat('0.0000 , .', sumaPoGrupaPred)
end;

procedure TfrmAukcija.cxRadioGroup1Click(Sender: TObject);
begin
     if cxRadioGroup1.EditValue = 3 then
      begin
        frmAukcijaPopolniCeni:=TfrmAukcijaPopolniCeni.Create(Application);
        frmAukcijaPopolniCeni.ShowModal;
        frmAukcijaPopolniCeni.Free;

        dm.tblPonudiStavki.Close;
        dm.tblPonudiStavki.Open;
        dm.tblBodiranjeStavkiPonudi.Close;
        dm.tblBodiranjeStavkiPonudi.Open;

        ClientHeight:= StatusBar1.Height+Panel1.Height+Panel2.Height;
        Panel_1.Visible:=False;
        Panel_2.Visible:=false;

        RestoreControls(Panel_2 );
        RestoreControls(Panel_1);
      end
     else if cxRadioGroup1.EditValue = 2 then
      begin
         ClientHeight:= pom_client_height - Panel_1.Height;
         Panel_2.Visible:=true;
         Panel_1.Visible:=false;

         RestoreControls(Panel_1);
      end
     else if cxRadioGroup1.EditValue = 1 then
      begin
         ClientHeight:= pom_client_height - Panel_2.Height;
         Panel_1.Visible:=true;
         Panel_2.Visible:=false;

         RestoreControls(Panel_2);
      end;


end;

procedure TfrmAukcija.FormCreate(Sender: TObject);
var sumaCelPred:double;
begin

     dmPQ.insert6(dmPQ.pPopolniPrvaCena,'BROJ_TENDER', 'TIP_PARTNER', 'PARTNER', Null, Null,Null, dm.tblPonudiBROJ_TENDER.Value, dm.tblPonudiTIP_PARTNER.Value, dm.tblPonudiPARTNER.Value, Null, Null,Null);

     dm.tblPonudiStavki.Close;
     dm.tblPonudiStavki.Open;
     dm.tblBodiranjeStavkiPonudi.Close;
     dm.tblBodiranjeStavkiPonudi.Open;

     tblPodgrupa.Close;
     tblPodgrupa.Open;

     pom_client_height:= ClientHeight;
     ClientHeight:= ClientHeight - Panel_2.Height;

     qIznosPredCel.close;
     qIznosPredCel.ParamByName('mas_id').Value:=dm.tblPonudiID.Value;
     qIznosPredCel.ExecQuery;

     sumaCelPred:=qIznosPredCel.FldByName['SumaIznosPredAukcija'].Value;

     IznosCel.Text:=FormatFloat('0.0000 , .', sumaCelPred);

     LabelPoraka.Caption:='���������� ������� � �������� ���� �-������ �� ����� '+dateToStr(frmPonudi.qAukcijaDatum.FldByName['datum'].Value);

end;

procedure TfrmAukcija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

procedure TfrmAukcija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmAukcija.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

end.
