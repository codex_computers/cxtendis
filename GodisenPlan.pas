unit GodisenPlan;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 04.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, cxPCdxBarPopupMenu, dxCustomHint, cxHint, cxRichEdit,
  cxDBRichEdit, cxBlobEdit, FIBDataSet, pFIBDataSet, cxLabel, cxDBLabel,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxTrackBar, dxdbtrel,
  cxSplitter, FIBQuery, pFIBQuery, cxImageComboBox, dxSkinOffice2013White,
  cxNavigator, System.Actions, dxBarBuiltInMenu, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmGodisenPlan = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1BarOdlukaNabavka: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabelaPlanOdluka: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelPlanOdluka: TPanel;
    PanelIzborSektor: TPanel;
    PanelPlanSektori: TPanel;
    cxPageControlPlanOdluka: TcxPageControl;
    cxTabSheetTabelaren: TcxTabSheet;
    cxTabSheetDetalen: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cbGodina: TdxBarCombo;
    Label1: TLabel;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1PREDMET: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    PanelDetalenPrikaz: TPanel;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    GODINA: TcxDBComboBox;
    DATUM: TcxDBDateEdit;
    PREDMET: TcxDBMemo;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxHintStyleController1: TcxHintStyleController;
    PanelTabelarenPrikaz: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarManager1Bar: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavkiVoPlan: TAction;
    aBrisiStavkaOdGodPlan: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    aZacuvajExcelStavki: TAction;
    aPecatiTabelaStavki: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarButton1: TdxBarButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    aPodesuvanjePecatenjeStavki: TAction;
    aPageSetupStavki: TAction;
    aSnimiPecatenjeStavki: TAction;
    aBrisiPodesuvanjePecatenjeStavki: TAction;
    aSnimiIzgledStavki: TAction;
    aBrisiIzgledStavki: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aKreirajPredlogPlan: TAction;
    tblPredlokOdluki: TpFIBDataSet;
    dsPredlogOdluki: TDataSource;
    tblPredlokOdlukiBROJ: TFIBStringField;
    tblPredlokOdlukiGODINA: TFIBIntegerField;
    tblPredlokOdlukiPREDMET: TFIBStringField;
    tblPredlokOdlukiOPIS: TFIBBlobField;
    tblPredlokOdlukiDATUM: TFIBDateField;
    tblPredlokOdlukiTS_INS: TFIBDateTimeField;
    tblPredlokOdlukiTS_UPD: TFIBDateTimeField;
    tblPredlokOdlukiUSR_INS: TFIBStringField;
    tblPredlokOdlukiUSR_UPD: TFIBStringField;
    aKreirajPredlogPlanIzbor: TAction;
    aKreirajPredlogPlanOtkazi: TAction;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid2DBTableView1MK: TcxGridDBColumn;
    cxGrid2DBTableView1EN: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1CENABEZDDV: TcxGridDBColumn;
    cxGrid2DBTableView1DANOK: TcxGridDBColumn;
    cxGrid2DBTableView1TIP: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    OPIS: TcxDBRichEdit;
    cxGrid2DBTableView1IZNOSCENA: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn;
    Panel3: TPanel;
    Label7: TLabel;
    cxDBLabel1: TcxDBLabel;
    Label8: TLabel;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    dxBarSubItem2: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    aRefreshGodisenPlan: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    aDinamikaRealizacija: TAction;
    cxGrid2DBTableView1MESEC: TcxGridDBColumn;
    cxGrid2DBTableView1TIP_NABAVKA: TcxGridDBColumn;
    cxGrid2DBTableView1TIPNABAVKANAZIV: TcxGridDBColumn;
    dxBarManager1Bar1: TdxBar;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    aPGodisenPlan: TAction;
    PopupMenu2: TPopupMenu;
    aDizajnGodisenPlan: TAction;
    N2: TMenuItem;
    aPopUpMeni: TAction;
    N3: TMenuItem;
    N4: TMenuItem;
    aPGodisenPlanPoSektori: TAction;
    dxBarButton3: TdxBarButton;
    aDizajnGodisenPlanPoSektori: TAction;
    aPGodisenPlanIzbranSektor: TAction;
    aDizajnGodisenPlanZaIzbranSektor: TAction;
    dxBarButton4: TdxBarButton;
    cbPodsektori: TcxTextEdit;
    cbPodsektoriTree: TdxLookupTreeView;
    cxSplitter1: TcxSplitter;
    qCountRe: TpFIBQuery;
    PanelIzborOdluka: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1GODINA: TcxGridDBColumn;
    cxGrid3DBTableView1BROJ: TcxGridDBColumn;
    cxGrid3DBTableView1PREDMET: TcxGridDBColumn;
    cxGrid3DBTableView1OPIS: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    btnOK: TcxButton;
    cxButton1: TcxButton;
    aPGodisenPlanNaPodsektoriNaIzbranSektor: TAction;
    aDizajnGodisenPlanNaPodsektoriNaIzbranSektor: TAction;
    dxBarButton5: TdxBarButton;
    N5: TMenuItem;
    dxBarManager1Bar7: TdxBar;
    aPGodisenPlanOdJN: TAction;
    dxBarButton6: TdxBarButton;
    aDGodisenPlanOdJN: TAction;
    N6: TMenuItem;
    aPOdlukaKreiranjeIzmenaGO: TAction;
    aDOdlukaKreiranjeIzmenaGO: TAction;
    N7: TMenuItem;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    qRokGodisenPlan: TpFIBQuery;
    cxButton2: TcxButton;
    aPrebarajRE: TAction;
    btnPredlogPlan: TdxBarButton;
    N8: TMenuItem;
    aDizajnPredlogPlan: TAction;
    dxBarButton9: TdxBarButton;
    aPGodisenPlanPoSektoriKolicina: TAction;
    aDGodisenPlanPoSektoriKolicina: TAction;
    qRabotnaGodina: TpFIBQuery;
    aGodisenPlanPrava: TAction;
    qUserPrava: TpFIBQuery;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1PREDMET_NABAVKA: TcxGridDBColumn;
    dxBarButton10: TdxBarButton;
    actPDinamikaNaRealizacija: TAction;
    actDDinamikaNaRealizacija: TAction;
    N9: TMenuItem;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaPlanOdlukaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure PREDMETDblClick(Sender: TObject);
    procedure OPISDblClick(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aDodadiStavkiVoPlanExecute(Sender: TObject);
    procedure aBrisiStavkaOdGodPlanExecute(Sender: TObject);
    procedure aRefreshGodisenPlanSektorExecute(Sender: TObject);
    procedure aZacuvajExcelStavkiExecute(Sender: TObject);
    procedure aPecatiTabelaStavkiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aPageSetupStavkiExecute(Sender: TObject);
    procedure aSnimiPecatenjeStavkiExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiExecute(Sender: TObject);
    procedure aBrisiIzgledStavkiExecute(Sender: TObject);
    procedure aKreirajPredlogPlanExecute(Sender: TObject);
    procedure aKreirajPredlogPlanIzborExecute(Sender: TObject);
    procedure aKreirajPredlogPlanOtkaziExecute(Sender: TObject);
    procedure aRefreshGodisenPlanExecute(Sender: TObject);
    procedure aDinamikaRealizacijaExecute(Sender: TObject);
    procedure aPGodisenPlanExecute(Sender: TObject);
    procedure aDizajnGodisenPlanExecute(Sender: TObject);
    procedure aPopUpMeniExecute(Sender: TObject);
    procedure aPGodisenPlanPoSektoriExecute(Sender: TObject);
    procedure aDizajnGodisenPlanPoSektoriExecute(Sender: TObject);
    procedure aPGodisenPlanIzbranSektorExecute(Sender: TObject);
    procedure aDizajnGodisenPlanZaIzbranSektorExecute(Sender: TObject);
    procedure cbPodsektoriTreeExit(Sender: TObject);
    procedure cbPodsektoriTreeSetDisplayItemText(Sender: TObject;
      var DisplayText: string);
    procedure aPGodisenPlanNaPodsektoriNaIzbranSektorExecute(Sender: TObject);
    procedure aDizajnGodisenPlanNaPodsektoriNaIzbranSektorExecute(Sender: TObject);
    procedure aPGodisenPlanOdJNExecute(Sender: TObject);
    procedure aDGodisenPlanOdJNExecute(Sender: TObject);
    procedure aPOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
    procedure aDOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
    procedure rok_godisen_plan_proc();
    procedure aPrebarajREExecute(Sender: TObject);
    procedure cbPodsektoriTreeCloseUp(Sender: TObject; Accept: Boolean);
    procedure cbPodsektoriPropertiesEditValueChanged(Sender: TObject);
    procedure btnPredlogPlanClick(Sender: TObject);
    procedure aDizajnPredlogPlanExecute(Sender: TObject);
    procedure aPGodisenPlanPoSektoriKolicinaExecute(Sender: TObject);
    procedure aGodisenPlanPravaExecute(Sender: TObject);
    procedure actPDinamikaNaRealizacijaExecute(Sender: TObject);
    procedure actDDinamikaNaRealizacijaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmGodisenPlan: TfrmGodisenPlan;
  pom_godina :Integer;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, Notepad,
  IzborStavkiGodisenPlan, dmProcedureQuey, DinamikaRealizacija;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmGodisenPlan.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmGodisenPlan.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelIzborOdluka.Visible = false)then
  begin
    cxPageControlPlanOdluka.ActivePage:=cxTabSheetDetalen;
    PanelDetalenPrikaz.Enabled:=True;
    PanelTabelarenPrikaz.Enabled:=False;
    Godina.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if cbGodina.Text <> '' then
       dm.tblGodPlanOdlukaGODINA.Value:=StrToInt(cbGodina.Text)
    else
       dm.tblGodPlanOdlukaGODINA.Value:=dmKon.godina;
    dm.tblGodPlanOdlukaDATUM.Value:=Now;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmGodisenPlan.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelIzborOdluka.Visible = false) then
  begin
    cxPageControlPlanOdluka.ActivePage:=cxTabSheetDetalen;
    PanelDetalenPrikaz.Enabled:=True;
    PanelTabelarenPrikaz.Enabled:=False;
    PREDMET.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmGodisenPlan.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) and (PanelIzborOdluka.Visible = false)then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmGodisenPlan.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmGodisenPlan.aBrisiIzgledStavkiExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmGodisenPlan.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmGodisenPlan.aRefreshGodisenPlanExecute(Sender: TObject);
begin
    cxGrid2DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
    cxGrid2DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmGodisenPlan.aRefreshGodisenPlanSektorExecute(Sender: TObject);
begin
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmGodisenPlan.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmGodisenPlan.aKreirajPredlogPlanExecute(Sender: TObject);
var br_odluki, god:Integer;
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelIzborOdluka.Visible = false)then
  begin
    if cbPodsektori.Text <> '' then
      if cbGodina.Text <> '' then
        begin
           qRokGodisenPlan.Close;
           qRokGodisenPlan.ExecQuery;

           if qRokGodisenPlan.FldByName['rok'].IsNull then
              rok_godisen_plan:=1
           else
              rok_godisen_plan:=qRokGodisenPlan.FldByName['rok'].Value;

           qRabotnaGodina.Close;
           qRabotnaGodina.ExecQuery;

           if (((rok_godisen_plan = 1) and (prava_re = 1)) or (prava_re = 0))then
               begin
                 br_odluki:=dmPQ.zemiRezultat5(dmPQ.pCountOdlukiPoGodina,'GODINA', Null, Null,Null,Null,StrToInt(cbGodina.Text) + 1, Null,Null,Null,Null,'BROJ');
                 if br_odluki = 0 then
                    begin
                      dmPQ.insert6(dmPQ.pPredlogPlan, 'FLAG','GODINA','RE', 'BROJ',Null,Null,
                                             0, strToInt(cbGodina.Text), cbPodsektori.EditValue,Null,Null,Null);
                      ShowMessage('�������� � ���� ������ �� ����� ������� �� ������� ���� �� �������� ������ �� ��������� ������ !!!');
                      god:=StrToInt(cbGodina.Text) + 1;
                      cbGodina.Text:= IntToStr(god);
                    end
                 else
                    begin
                      PanelIzborOdluka.Visible:=True;
                      tblPredlokOdluki.Close;
                      tblPredlokOdluki.ParamByName('godina').Value:=StrToInt(cbGodina.Text) + 1;
                      tblPredlokOdluki.Open;
                    end;
               end
           else ShowMessage('����� �� �������� �� ������� ���� � ������� !!!')
        end
     else ShowMessage('�������� ������ !!!')
    else ShowMessage('�������� ������� ������� !!!');
  end
 else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmGodisenPlan.aKreirajPredlogPlanIzborExecute(Sender: TObject);
var god:Integer;
begin
     dmPQ.insert6(dmPQ.pPredlogPlan, 'FLAG','GODINA','RE', 'BROJ',Null,Null,
                                                  1, strToInt(cbGodina.Text), cbPodsektori.EditValue,tblPredlokOdlukiBROJ.Value,Null,Null);
     god:=StrToInt(cbGodina.Text) + 1;
     cbGodina.Text:= IntToStr(god);
     dm.tblGodPlanOdluka.Close;
     dm.tblGodPlanOdluka.ParamByName('godina').Value:=StrToInt(cbGodina.Text);
     dm.tblGodPlanOdluka.Open;
     dm.tblGodPlanOdluka.Locate('BROJ',tblPredlokOdlukiBROJ.Value,[]);
     dm.tblGodPlanSektori.Close;
     dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
     dm.tblGodPlanSektori.Open;
     PanelIzborOdluka.Visible:=False;
end;

procedure TfrmGodisenPlan.aKreirajPredlogPlanOtkaziExecute(Sender: TObject);
begin
     PanelIzborOdluka.visible:=False;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmGodisenPlan.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmGodisenPlan.aSnimiIzgledStavkiExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmGodisenPlan.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� �������� � ������ �� ������� ����');
end;

procedure TfrmGodisenPlan.aZacuvajExcelStavkiExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������� ����');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmGodisenPlan.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmGodisenPlan.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmGodisenPlan.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmGodisenPlan.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmGodisenPlan.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmGodisenPlan.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmGodisenPlan.OPISDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblGodPlanOdluka.CreateBlobStream(dm.tblGodPlanOdlukaOPIS , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblGodPlanOdlukaOPIS as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmGodisenPlan.PREDMETDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblGodPlanOdlukaPREDMET.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblGodPlanOdlukaPREDMET.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmGodisenPlan.prefrli;
begin
end;

procedure TfrmGodisenPlan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(PanelDetalenPrikaz) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmGodisenPlan.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dm.tblPodsektoriGodisenPlan.Close;
  dm.tblPodsektoriGodisenPlan.ParamByName('roditel').Value:=dmKon.firma_id;
  dm.tblPodsektoriGodisenPlan.ParamByName('username').Value:=dmKon.user;
  dm.tblPodsektoriGodisenPlan.ParamByName('APP').Value:=dmKon.aplikacija;
  if prava_re = 1 then
     dm.tblPodsektoriGodisenPlan.ParamByName('param').Value:=1
  else
     dm.tblPodsektoriGodisenPlan.ParamByName('param').Value:=0;
  dm.tblPodsektoriGodisenPlan.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmGodisenPlan.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    pom_godina:=0;
    cbGodina.Text:=IntToStr(dmKon.godina);

     //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);

    cxPageControlPlanOdluka.ActivePage:=cxTabSheetTabelaren;
    PanelIzborOdluka.Top:=277;
    PanelIzborOdluka.Left :=277;

    if not dm.tblPodsektoriGodisenPlan.IsEmpty then
         cbPodsektori.EditValue:=dmKon.firma_id;


    if cbPodsektori.Text <> '' then
       begin
         dm.tblGodPlanSektori.Close;
         dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
         dm.tblGodPlanSektori.Open;
       end;


   //-----------------------------------------------------

     aGodisenPlanPrava.Execute();


end;

//------------------------------------------------------------------------------

procedure TfrmGodisenPlan.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmGodisenPlan.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmGodisenPlan.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
    if (cxGrid2DBTableView1.Controller.FocusedColumn <> cxGrid2DBTableView1KOLICINA)
       and (cxGrid2DBTableView1.Controller.FocusedColumn <> cxGrid2DBTableView1CENABEZDDV)
       and (cxGrid2DBTableView1.Controller.FocusedColumn <> cxGrid2DBTableView1DANOK)
       and (cxGrid2DBTableView1.Controller.FocusedColumn <> cxGrid2DBTableView1CENA)then
     begin
       if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
     end;
end;

//  ����� �� �����
procedure TfrmGodisenPlan.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  b,g: string;
begin
  ZapisiButton.SetFocus;
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDetalenPrikaz) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        b:=BROJ.Text;
        g:=GODINA.Text;
        cbGodina.Text:=GODINA.Text;
        dm.tblGodPlanOdluka.Locate('GODINA;BROJ', VarArrayOf([StrToInt(g), b]), []);
        PanelDetalenPrikaz.Enabled:=false;
        PanelTabelarenPrikaz.Enabled:=true;
        cxPageControlPlanOdluka.ActivePage:=cxTabSheetTabelaren;
        cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmGodisenPlan.btnPredlogPlanClick(Sender: TObject);
var status: TStatusWindowHandle;
begin
   aPrebarajRE.Execute();
if cbPodsektori.Text = '' then  ShowMessage('�������� ������� ������� !!!')
 else
  begin
   try
    dmRes.Spremi('JN',40056);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.ParamByName('re').Value:=cbPodsektori.EditValue;
    dmKon.tblSqlReport.Open;

   // dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(IntToStr(StrToInt(cbGodina.Text)+1)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));

    status := cxCreateStatusWindow();

    try
       dmRes.frxReport1.ShowReport();
    finally
     	cxRemoveStatusWindow(status);
    end;


  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
  end;
end;

procedure TfrmGodisenPlan.rok_godisen_plan_proc();
begin
        //// rok za godisen plan
     qRokGodisenPlan.Close;
     qRokGodisenPlan.ExecQuery;

     if qRokGodisenPlan.FldByName['rok'].IsNull then
      rok_godisen_plan:=1
     else
      rok_godisen_plan:=qRokGodisenPlan.FldByName['rok'].Value;

     if ((rok_godisen_plan = 0) and (prava_re = 1))then
        begin
          dxBarManager1Bar.Visible:=False;
          aDodadiStavkiVoPlan.Enabled:=False;
          aBrisiStavkaOdGodPlan.Enabled:=False;
          aKreirajPredlogPlan.Enabled:=false;
          cxGrid2DBTableView1.OptionsData.Editing:=False;
        end
     else
        begin
          dxBarManager1Bar.Visible:=True;
          aDodadiStavkiVoPlan.Enabled:=True;
          aBrisiStavkaOdGodPlan.Enabled:=True;
          aKreirajPredlogPlan.Enabled:=True;
          cxGrid2DBTableView1.OptionsData.Editing:=True;
        end;
     //////
end;

procedure TfrmGodisenPlan.cbGodinaChange(Sender: TObject);
begin
     if cbGodina.Text <> '' then
        begin
          dm.tblGodPlanOdluka.Close;
          dm.tblGodPlanOdluka.ParamByName('godina').Value:=StrToInt(cbGodina.Text);
          dm.tblGodPlanOdluka.Open;
        end
     else
        begin
          dm.tblGodPlanOdluka.Close;
          dm.tblGodPlanOdluka.ParamByName('godina').Value:='%';
          dm.tblGodPlanOdluka.Open;
        end;
     if pom_godina = 1 then
        begin
          dm.tblGodPlanSektori.Close;
          dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
          dm.tblGodPlanSektori.Open;
        end
     else pom_godina:=1;

    aGodisenPlanPrava.Execute();

end;

procedure TfrmGodisenPlan.cbPodsektoriPropertiesEditValueChanged(
  Sender: TObject);
begin
    if cbPodsektori.Text <> '' then
     begin
     qCountRe.Close;
     qCountRe.ParamByName('roditel').Value:=dmKon.firma_id;
     qCountRe.ParamByName('username').Value:=dmKon.user;
     qCountRe.ParamByName('APP').Value:=dmKon.aplikacija;
     if dmKon.prava <> 0 then
        qCountRe.ParamByName('param').Value:=1
     else
        qCountRe.ParamByName('param').Value:=0;
     qCountRe.ParamByName('re').Value:=cbPodsektori.EditValue;
     qCountRe.ExecQuery;

     if qCountRe.FldByName['cRE'].Value = 1 then
        begin
           dm.tblPodsektoriGodisenPlan.Locate('RE',cbPodsektori.EditValue,[]);
           cbPodsektoriTree.Text:=dm.tblPodsektoriGodisenPlanNAZIV.Value;
        end
     else if qCountRe.FldByName['cRE'].Value = 0 then
        cbPodsektoriTree.Text:='';

     if cbPodsektoriTree.Text <> '' then
        begin
          dm.tblGodPlanSektori.Close;
          dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
          dm.tblGodPlanSektori.Open;
        end;
     end
    else
      begin
        cbPodsektoriTree.Text:='';
        dm.tblGodPlanSektori.Close;
        dm.tblGodPlanSektori.ParamByName('re').Value:='DTF';
        dm.tblGodPlanSektori.Open;
      end;
end;

procedure TfrmGodisenPlan.cbPodsektoriTreeCloseUp(Sender: TObject;
  Accept: Boolean);
begin
   if cbPodsektoriTree.Text <> '' then
        begin
           cbPodsektori.EditValue:=dm.tblPodsektoriGodisenPlanRE.Value
        end
     else cbPodsektori.Clear;
end;

procedure TfrmGodisenPlan.cbPodsektoriTreeExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite;
end;

procedure TfrmGodisenPlan.cbPodsektoriTreeSetDisplayItemText(Sender: TObject;
  var DisplayText: string);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmGodisenPlan.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      if PanelIzborOdluka.Visible = true then
         PanelIzborOdluka.Visible:=false
      else
         begin
            ModalResult := mrCancel;
            Close();
         end;
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalenPrikaz);
      PanelDetalenPrikaz.Enabled:=false;
      PanelTabelarenPrikaz.Enabled:=true;
      cxPageControlPlanOdluka.ActivePage:=cxTabSheetTabelaren;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmGodisenPlan.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmGodisenPlan.aPageSetupStavkiExecute(Sender: TObject);
begin
   dxComponentPrinter1Link2.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmGodisenPlan.aPecatiTabelaPlanOdlukaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '������ �� �������� � ������ �� ������� ����';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + cbGodina.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmGodisenPlan.aPecatiTabelaStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������� ����';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: ' + intToStr(dm.tblGodPlanOdlukaGODINA.Value));
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� �� ������: ' + dm.tblGodPlanOdlukaBROJ.Value);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('���. �������: ' + cbPodsektoriTree.Text);
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmGodisenPlan.aPGodisenPlanExecute(Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40001);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmGodisenPlan.aPGodisenPlanIzbranSektorExecute(Sender: TObject);
var status: TStatusWindowHandle;
begin
aPrebarajRE.Execute();
if cbPodsektori.Text = '' then  ShowMessage('�������� ������� ������� !!!')
 else
  begin
   try
    dmRes.Spremi('JN',40003);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.ParamByName('re').Value:=cbPodsektori.EditValue;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));

    status := cxCreateStatusWindow();

    try
       dmRes.frxReport1.ShowReport();
    finally
     	cxRemoveStatusWindow(status);
    end;


  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
  end;
end;

procedure TfrmGodisenPlan.aPGodisenPlanNaPodsektoriNaIzbranSektorExecute(
  Sender: TObject);
var status: TStatusWindowHandle;
begin
if cbPodsektori.Text = '' then  ShowMessage('�������� ������� ������� !!!')
 else
  begin
   try
      status := cxCreateStatusWindow();
      try
         dmRes.Spremi('JN',40007);
         dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
         dmKon.tblSqlReport.ParamByName('sektor').Value:=cbPodsektori.EditValue;
         dmKon.tblSqlReport.Open;

         dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
         dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
         dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
         dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
         dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));

         dmRes.frxReport1.ShowReport();
      finally
     	   cxRemoveStatusWindow(status);
      end;
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
  end;
end;

procedure TfrmGodisenPlan.aPGodisenPlanOdJNExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40027);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.ParamByName('param_godina').Value:=1;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmGodisenPlan.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmGodisenPlan.aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmGodisenPlan.aPOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40029);
    dmKon.tblSqlReport.ParamByName('godina').Value:=dm.tblGodPlanOdlukaGODINA.Value;;
    dmKon.tblSqlReport.ParamByName('broj').Value:=dm.tblGodPlanOdlukaBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmGodisenPlan.aPopUpMeniExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500 );
end;

procedure TfrmGodisenPlan.aPrebarajREExecute(Sender: TObject);
begin
   if (cbPodsektori.Text <> '') and (cbPodsektoriTree.Text <>'') then
     begin
     qCountRe.Close;
     qCountRe.ParamByName('roditel').Value:=dmKon.firma_id;
     qCountRe.ParamByName('username').Value:=dmKon.user;
     qCountRe.ParamByName('APP').Value:=dmKon.aplikacija;
     if dmKon.prava <> 0 then
        qCountRe.ParamByName('param').Value:=1
     else
        qCountRe.ParamByName('param').Value:=0;
     qCountRe.ParamByName('re').Value:=cbPodsektori.EditValue;
     qCountRe.ExecQuery;

     if qCountRe.FldByName['cRE'].Value = 1 then
        begin
           dm.tblPodsektoriGodisenPlan.Locate('RE',cbPodsektori.EditValue,[]);
           cbPodsektoriTree.Text:=dm.tblPodsektoriGodisenPlanNAZIV.Value;
        end
     else if qCountRe.FldByName['cRE'].Value = 0 then
        cbPodsektoriTree.Text:='';

     if cbPodsektoriTree.Text <> '' then
        begin
          dm.tblGodPlanSektori.Close;
          dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
          dm.tblGodPlanSektori.Open;
        end;
     end
    else
      begin
        cbPodsektoriTree.Text:='';
        dm.tblGodPlanSektori.Close;
        dm.tblGodPlanSektori.ParamByName('re').Value:='DTF';
        dm.tblGodPlanSektori.Open;
      end;
end;

procedure TfrmGodisenPlan.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmGodisenPlan.aSnimiPecatenjeStavkiExecute(Sender: TObject);
begin
   zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmGodisenPlan.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmGodisenPlan.aBrisiPodesuvanjePecatenjeStavkiExecute(
  Sender: TObject);
begin
   brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmGodisenPlan.aBrisiStavkaOdGodPlanExecute(Sender: TObject);
begin
     if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
        begin
          dm.qCountRaspisanaStavka.Close;
          dm.qCountRaspisanaStavka.ParamByName('vid_artikal').Value:=dm.tblGodPlanSektoriVID_ARTIKAL.Value;
          dm.qCountRaspisanaStavka.ParamByName('artikal').Value:=dm.tblGodPlanSektoriARTIKAL.Value;
          dm.qCountRaspisanaStavka.ParamByName('re').Value:=dm.tblGodPlanSektoriRE.Value;
          dm.qCountRaspisanaStavka.ParamByName('godina').Value:=dm.tblGodPlanSektoriGODINA.Value;
          dm.qCountRaspisanaStavka.ExecQuery;
          if dm.qCountRaspisanaStavka.FldByName['br'].Value = 0 then
             cxGrid2DBTableView1.DataController.DataSet.Delete()
          else
             ShowMessage('�� � ��������� ������ �� �������� ���弝� � ������� �� �������� �� ����� ������� !!!');
        end;
end;

procedure TfrmGodisenPlan.actDDinamikaNaRealizacijaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40059);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.actPDinamikaNaRealizacijaExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40059);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmGodisenPlan.aDizajnGodisenPlanNaPodsektoriNaIzbranSektorExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',40007);
   dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
   dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
   dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
   dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDGodisenPlanOdJNExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40027);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDinamikaRealizacijaExecute(Sender: TObject);
begin
     frmDinamikaRealizacija:=TfrmDinamikaRealizacija.Create(Application);
     frmDinamikaRealizacija.ShowModal;
     frmDinamikaRealizacija.Free;
     dm.tblGodPlanSektori.FullRefresh;
end;

procedure TfrmGodisenPlan.aDizajnGodisenPlanExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',40001);
   dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
   dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDizajnGodisenPlanPoSektoriExecute(Sender: TObject);
begin
    dmRes.Spremi('JN',40002);
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDizajnGodisenPlanZaIzbranSektorExecute(
  Sender: TObject);
begin
    dmRes.Spremi('JN',40003);
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDizajnPredlogPlanExecute(Sender: TObject);
begin

    dmRes.Spremi('JN',40056);
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(IntToStr(StrToInt(cbGodina.Text)+1)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'NazivRE', QuotedStr(cbPodsektoriTree.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'RE', QuotedStr(intToStr(cbPodsektori.EditValue)));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.DesignReport();
end;

procedure TfrmGodisenPlan.aDodadiStavkiVoPlanExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
   begin

     if cbPodsektori.Text = '' then
         ShowMessage('�������� ������� ������� !!!')
     else
         begin
           frmIzborStavkiGodisenPlan:=TfrmIzborStavkiGodisenPlan.Create(Application);
            // prenesuvanje na sifrata na rabotnata edinica
           frmIzborStavkiGodisenPlan.Tag:=cbPodsektori.EditValue;
           frmIzborStavkiGodisenPlan.ShowModal;
           frmIzborStavkiGodisenPlan.Free;
           dm.tblGodPlanSektori.Close;
           dm.tblGodPlanSektori.ParamByName('re').Value:=cbPodsektori.EditValue;
           dm.tblGodPlanSektori.Open;
         end;
   end
 else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmGodisenPlan.aDOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40029);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmGodisenPlan.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmGodisenPlan.aGodisenPlanPravaExecute(Sender: TObject);
var i:integer;   kraj, user:Boolean;
begin
     qRabotnaGodina.Close;
     qRabotnaGodina.ExecQuery;

     if (not qRabotnaGodina.FldByName['v1'].IsNull)then
       begin
          qUserPrava.Close;
          qUserPrava.ParamByName('user').Value:=dmKon.user;
          qUserPrava.ExecQuery;

          qRokGodisenPlan.Close;
          qRokGodisenPlan.ExecQuery;

          if qRokGodisenPlan.FldByName['rok'].IsNull then
             rok_godisen_plan:=1
          else
             rok_godisen_plan:=qRokGodisenPlan.FldByName['rok'].Value;

          if qUserPrava.FldByName['br'].Value = 0  then
              begin
                if qRabotnaGodina.FldByName['v1'].Value <>(StrToInt(cbGodina.Text)+1)  then
                   aKreirajPredlogPlan.Enabled:=false
                else
                   begin
                      if (rok_godisen_plan = 0) then
                         aKreirajPredlogPlan.Enabled:=False
                      else
                         aKreirajPredlogPlan.Enabled:=true;
                   end;

                if qRabotnaGodina.FldByName['v1'].Value <> cbGodina.Text  then
                    begin
                      dxBarManager1Bar.Visible:=False;
                      aDodadiStavkiVoPlan.Enabled:=False;
                      aBrisiStavkaOdGodPlan.Enabled:=False;
                      cxGrid2DBTableView1.OptionsData.Editing:=False;
                    end
                  else
                    begin
                      if (rok_godisen_plan = 0)then
                        begin
                          dxBarManager1Bar.Visible:=False;
                          aDodadiStavkiVoPlan.Enabled:=False;
                          aBrisiStavkaOdGodPlan.Enabled:=False;
                          cxGrid2DBTableView1.OptionsData.Editing:=False;
                         end
                       else
                        begin
                          dxBarManager1Bar.Visible:=True;
                          aDodadiStavkiVoPlan.Enabled:=True;
                          aBrisiStavkaOdGodPlan.Enabled:=True;
                          cxGrid2DBTableView1.OptionsData.Editing:=True;
                        end
                    end;
              end
       end
     else rok_godisen_plan_proc();

end;

procedure TfrmGodisenPlan.aPGodisenPlanPoSektoriExecute(Sender: TObject);
begin
  try
    dmRes.Spremi('JN',40002);
    dmKon.tblSqlReport.ParamByName('godina').Value:=strToInt(cbGodina.Text);
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(cbGodina.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmGodisenPlan.aPGodisenPlanPoSektoriKolicinaExecute(
  Sender: TObject);
begin

end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmGodisenPlan.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmGodisenPlan.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
