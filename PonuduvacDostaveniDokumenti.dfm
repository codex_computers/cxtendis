object frmPonuduvacDostaveniDokumenti: TfrmPonuduvacDostaveniDokumenti
  Left = 0
  Top = 0
  Caption = #1044#1086#1089#1090#1072#1074#1077#1085#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1080' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
  ClientHeight = 596
  ClientWidth = 803
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 141
    Width = 803
    Height = 435
    Align = alClient
    TabOrder = 0
    ExplicitTop = 121
    ExplicitHeight = 455
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.Images = dmRes.cxSmallImages
      Navigator.Buttons.Edit.ImageIndex = 12
      Navigator.Buttons.Edit.Visible = True
      Navigator.Buttons.Post.Hint = #1047#1072#1087#1080#1096#1080
      Navigator.Buttons.Post.ImageIndex = 7
      DataController.DataSource = dm.dsPonuduvacPotrebniDok
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.EditAutoHeight = eahRow
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
      OptionsView.ShowEditButtons = gsebAlways
      object cxGrid1DBTableView1PONUDA_ID: TcxGridDBColumn
        DataBinding.FieldName = 'PONUDA_ID'
        Visible = False
        Options.Editing = False
        Width = 98
      end
      object cxGrid1DBTableView1TIP: TcxGridDBColumn
        DataBinding.FieldName = 'TIP'
        Visible = False
        Options.Editing = False
        Width = 137
      end
      object cxGrid1DBTableView1tipNaziv: TcxGridDBColumn
        DataBinding.FieldName = 'tipNaziv'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Width = 268
      end
      object cxGrid1DBTableView1DOSTAVEN_DOKUMENT: TcxGridDBColumn
        DataBinding.FieldName = 'DOSTAVEN_DOKUMENT'
        Visible = False
        Options.Editing = False
        Width = 103
      end
      object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Options.Editing = False
        Width = 484
      end
      object cxGrid1DBTableView1DOSTAVEN: TcxGridDBColumn
        DataBinding.FieldName = 'DOSTAVEN'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.MultiLine = True
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        Properties.OnEditValueChanged = cxGrid1DBTableView1DOSTAVENPropertiesEditValueChanged
        Options.IncSearch = False
        Styles.Content = dmRes.cxStyle120
        Width = 57
      end
      object cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn
        DataBinding.FieldName = 'VALIDNOST'
        PropertiesClassName = 'TcxBlobEditProperties'
        Properties.BlobEditKind = bekMemo
        Properties.BlobPaintStyle = bpsText
        Properties.OnEditValueChanged = cxGrid1DBTableView1VALIDNOSTPropertiesEditValueChanged
        Options.IncSearch = False
        Options.ShowEditButtons = isebAlways
        Options.Sorting = False
        Styles.Content = dmRes.cxStyle120
        Width = 207
      end
      object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
        DataBinding.FieldName = 'TS_INS'
        Visible = False
        Options.Editing = False
        Width = 168
      end
      object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'TS_UPD'
        Visible = False
        Options.Editing = False
        Width = 214
      end
      object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
        DataBinding.FieldName = 'USR_INS'
        Visible = False
        Options.Editing = False
        Width = 209
      end
      object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'USR_UPD'
        Visible = False
        Options.Editing = False
        Width = 240
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 576
    Width = 803
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 803
    Height = 100
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 2
    object Label8: TLabel
      Left = 14
      Top = 10
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Left = 14
      Top = 53
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 14
      Top = 31
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1085#1091#1076#1072' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object cxDBLabel1: TcxDBLabel
      Left = 175
      Top = 9
      DataBinding.DataField = 'BROJ_TENDER'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 186
    end
    object cxDBLabel2: TcxDBLabel
      Left = 175
      Top = 50
      DataBinding.DataField = 'PARTNERNAZIV'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 370
    end
    object cxDBLabel3: TcxDBLabel
      Left = 175
      Top = 31
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 370
    end
    object Panel2: TPanel
      Left = 1
      Top = 75
      Width = 801
      Height = 24
      Align = alBottom
      Color = 11302478
      ParentBackground = False
      TabOrder = 3
      ExplicitTop = 96
      object CheckBox1: TcxCheckBox
        Left = 524
        Top = 2
        TabStop = False
        Caption = #1064#1090#1080#1082#1083#1080#1088#1072#1112' '#1075#1080' '#1089#1080#1090#1077
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = 10901821
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.TextColor = clWhite
        Style.IsFontAssigned = True
        TabOrder = 0
        Transparent = True
        OnClick = CheckBox1Click
        Width = 132
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 803
    Height = 41
    Align = alTop
    TabOrder = 3
    ExplicitLeft = -8
    ExplicitTop = -13
    object ZapisiButton: TcxButton
      Left = 14
      Top = 10
      Width = 395
      Height = 25
      ParentCustomHint = False
      Action = aPrevzemiPotrebniDokOdTender
      Colors.Pressed = clGradientActiveCaption
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 264
    Top = 232
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aPrevzemiPotrebniDokOdTender: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1077#1090#1077' '#1075#1080' '#1086#1089#1090#1072#1085#1072#1090#1080#1090#1077'  '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090
      Hint = 
        #1055#1088#1077#1074#1079#1077#1084#1072#1114#1077' '#1085#1072' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080#1090#1077' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1082#1086#1080' '#1089#1077' '#1074#1084 +
        #1077#1090#1085#1072#1090#1080' '#1074#1086' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1085#1072' '#1086#1074#1072#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 62
      OnExecute = aPrevzemiPotrebniDokOdTenderExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 280
    Top = 384
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 144
  end
end
