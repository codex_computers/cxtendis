object frmAukcija: TfrmAukcija
  Left = 0
  Top = 0
  Caption = #1077' - '#1040#1091#1082#1094#1080#1112#1072
  ClientHeight = 543
  ClientWidth = 623
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 92
    Width = 623
    Height = 153
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 0
    object Label8: TLabel
      Left = 14
      Top = 10
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Left = 14
      Top = 29
      Width = 155
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object cxDBLabel1: TcxDBLabel
      Left = 175
      Top = 9
      DataBinding.DataField = 'BROJ_TENDER'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 186
    end
    object cxDBLabel2: TcxDBLabel
      Left = 175
      Top = 29
      DataBinding.DataField = 'PARTNERNAZIV'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 370
    end
    object cxRadioGroup1: TcxRadioGroup
      Left = 14
      Top = 54
      Caption = #1053#1072#1095#1080#1085' '#1085#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1094#1077#1085#1080#1090#1077
      Properties.DefaultValue = 1
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089' '#1085#1072' '#1094#1077#1083#1072#1090#1072' '#1087#1086#1085#1091#1076#1072
          Value = 1
        end
        item
          Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089' '#1087#1086' '#1083#1086#1090#1086#1074#1080'/'#1075#1088#1091#1087#1080' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
          Value = 2
        end
        item
          Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1094#1077#1085#1080#1090#1077' '#1087#1086#1077#1076#1080#1085#1077#1095#1085#1086' '#1087#1086' '#1089#1090#1072#1074#1082#1072
          Value = 3
        end>
      ItemIndex = 0
      TabOrder = 2
      OnClick = cxRadioGroup1Click
      Height = 89
      Width = 387
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 524
    Width = 623
    Height = 19
    Panels = <
      item
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
        Width = 50
      end>
  end
  object Panel_1: TPanel
    Left = 0
    Top = 413
    Width = 623
    Height = 128
    Align = alTop
    Alignment = taLeftJustify
    Color = clCream
    ParentBackground = False
    TabOrder = 2
    VerticalAlignment = taAlignTop
    object cxGroupBox1: TcxGroupBox
      Left = 1
      Top = 1
      Align = alClient
      Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089' '#1085#1072' '#1094#1077#1083#1072#1090#1072' '#1087#1086#1085#1091#1076#1072
      TabOrder = 0
      Height = 126
      Width = 621
      object Label1: TLabel
        Left = 21
        Top = 31
        Width = 227
        Height = 13
        Caption = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' '#1055#1056#1045#1044' '#1077' - '#1040#1091#1082#1094#1080#1112#1072':'
      end
      object Label2: TLabel
        Left = 21
        Top = 58
        Width = 235
        Height = 13
        Caption = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' '#1055#1054#1057#1051#1045' '#1077' - '#1040#1091#1082#1094#1080#1112#1072':'
      end
      object cxTextEditPosle: TcxTextEdit
        Tag = 1
        Left = 261
        Top = 55
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object cxButton1: TcxButton
        Left = 261
        Top = 82
        Width = 121
        Height = 25
        Action = aGenerirajCeni_1
        TabOrder = 1
      end
      object IznosCel: TcxTextEdit
        Left = 261
        Top = 28
        Enabled = False
        StyleDisabled.TextColor = clCaptionText
        TabOrder = 2
        Width = 122
      end
    end
  end
  object Panel_2: TPanel
    Left = 0
    Top = 245
    Width = 623
    Height = 168
    Align = alTop
    Alignment = taLeftJustify
    Color = clCream
    ParentBackground = False
    TabOrder = 3
    VerticalAlignment = taAlignTop
    Visible = False
    object cxGroupBox2: TcxGroupBox
      Left = 1
      Top = 1
      Align = alClient
      Caption = #1053#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089' '#1087#1086' '#1083#1086#1090#1086#1074#1080'/'#1087#1086#1076#1075#1088#1091#1087#1080' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
      TabOrder = 0
      DesignSize = (
        621
        166)
      Height = 166
      Width = 621
      object Label5: TLabel
        Left = 20
        Top = 27
        Width = 342
        Height = 13
        Caption = #1048#1079#1073#1086#1088' '#1085#1072' '#1075#1088#1091#1087#1072' '#1079#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1088#1072#1074#1080' '#1075#1088#1091#1087#1085#1086' '#1085#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1094#1077#1085#1080#1090#1077': '
      end
      object Label3: TLabel
        Left = 20
        Top = 76
        Width = 227
        Height = 13
        Caption = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' '#1055#1056#1045#1044' '#1077' - '#1040#1091#1082#1094#1080#1112#1072':'
      end
      object Label4: TLabel
        Left = 20
        Top = 103
        Width = 235
        Height = 13
        Caption = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' '#1055#1054#1057#1051#1045' '#1077' - '#1040#1091#1082#1094#1080#1112#1072':'
      end
      object grupa: TcxLookupComboBox
        Tag = 1
        Left = 18
        Top = 46
        Anchors = [akLeft, akTop, akRight, akBottom]
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'GRUPA'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'GRUPA'
          end
          item
            Width = 600
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsPodgrupa
        Properties.OnEditValueChanged = cxLookupComboBox1PropertiesEditValueChanged
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 579
      end
      object cxTextEditPosleGrupa: TcxTextEdit
        Tag = 1
        Left = 261
        Top = 100
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object cxButton2: TcxButton
        Left = 261
        Top = 127
        Width = 121
        Height = 25
        Action = aGenerirajCeni_2
        TabOrder = 2
      end
      object IznosPodgrupa: TcxTextEdit
        Left = 261
        Top = 73
        Enabled = False
        StyleDisabled.BorderColor = clBtnShadow
        StyleDisabled.TextColor = clCaptionText
        TabOrder = 3
        Width = 121
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 623
    Height = 92
    Align = alTop
    TabOrder = 4
    object Label6: TLabel
      Left = 19
      Top = 14
      Width = 564
      Height = 13
      Caption = 
        '* '#1055#1088#1077#1076' '#1077'-'#1040#1091#1082#1094#1080#1112#1072' '#1079#1072#1076#1086#1083#1078#1080#1090#1077#1083#1085#1086' '#1079#1072#1095#1091#1074#1072#1112#1090#1077' '#1112#1072' '#1084#1086#1084#1077#1085#1090#1072#1083#1085#1072#1090#1072' '#1089#1086#1089#1090#1086#1112#1073#1072 +
        ' '#1085#1072' '#1089#1090#1072#1074#1082#1080#1090#1077' '#1074#1086' '#1087#1086#1085#1091#1076#1080#1090#1077
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object LabelPoraka: TLabel
      Left = 19
      Top = 66
      Width = 346
      Height = 13
      Caption = #1055#1086#1089#1083#1077#1076#1085#1072#1090#1072' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1077' '#1079#1072#1095#1091#1074#1072#1085#1072' '#1087#1088#1077#1076' '#1077'-'#1040#1091#1082#1094#1080#1112#1072' '#1089#1086' '#1076#1072#1090#1091#1084
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object cxButton3: TcxButton
      Left = 22
      Top = 33
      Width = 267
      Height = 25
      Action = aZacuvajSostojbaPredAukcija
      TabOrder = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 432
    Top = 240
    object aGenerirajCeni_1: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1094#1077#1085#1080
      ImageIndex = 6
      OnExecute = aGenerirajCeni_1Execute
    end
    object aGenerirajCeni_2: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1094#1077#1085#1080
      ImageIndex = 6
      OnExecute = aGenerirajCeni_2Execute
    end
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aPecatiCeniPredIPotoa: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1094#1077#1085#1080#1090#1077' '#1087#1086' '#1072#1091#1082#1094#1080#1112#1072
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aPecatiCeniPredIPotoaExecute
    end
    object aDizajnCeniPredIPotoa: TAction
      Caption = 'aDizajnCeniPredIPotoa'
      ShortCut = 16505
      OnExecute = aDizajnCeniPredIPotoaExecute
    end
    object aZacuvajSostojbaPredAukcija: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1087#1088#1077#1076' '#1077'-'#1040#1091#1082#1094#1080#1112#1072
      ImageIndex = 59
      OnExecute = aZacuvajSostojbaPredAukcijaExecute
    end
  end
  object tblPodgrupa: TpFIBDataSet
    SelectSQL.Strings = (
      'select  distinct ma.grupa,'
      '       mg.naziv'
      'from jn_ponudi_stavki ps'
      
        'inner join mtr_artikal ma on ma.artvid=ps.vid_artikal and ma.id ' +
        '= ps.artikal'
      'inner join mtr_artgrupa mg on mg.id = ma.grupa'
      'where ps.ponuda_id = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsPonudi
    Left = 488
    Top = 248
    object tblPodgrupaGRUPA: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodgrupaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPodgrupa: TDataSource
    DataSet = tblPodgrupa
    Left = 544
    Top = 240
  end
  object qIznosPredPoGrupa: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        '/*$$IBEC$$ select coalesce((sum(ps.cena_prva_bezddv*ps.kolicina)' +
        '),0) SumaIznosPredAukcija'
      'from jn_ponudi_stavki ps'
      
        'inner join mtr_artikal ma on ma.artvid = ps.vid_artikal and ma.i' +
        'd = ps.artikal and ma.grupa = :grupa'
      'where ps.ponuda_id = :mas_id $$IBEC$$*/'
      ''
      'select first 1 a.datum,'
      
        '       coalesce((sum(aps.cena_bezddv_pred_aukcija*ps.kolicina)),' +
        '0) SumaIznosPredAukcija'
      'from jn_e_aukcija_stavki aps'
      'inner join jn_ponudi_stavki ps on ps.id = aps.ponuda_stavki_id'
      'inner join jn_e_aukcija a on a.id = aps.e_aukcija_id'
      
        'inner join mtr_artikal ma on ma.artvid = aps.vid_artikal and ma.' +
        'id = aps.artikal and ma.grupa = :grupa'
      'where ps.ponuda_id = :mas_id and aps.bodiranje_po = '#39'01'#39
      'group by 1'
      'order by a.datum desc')
    Left = 488
    Top = 152
  end
  object qIznosPredCel: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        '/*$$IBEC$$ select coalesce((sum(ps.cena_prva_bezddv*ps.kolicina)' +
        '),0) SumaIznosPredAukcija'
      'from jn_ponudi_stavki ps'
      'where ps.ponuda_id = :mas_id $$IBEC$$*/'
      ''
      'select first 1 a.datum,'
      
        '       coalesce((sum(aps.cena_bezddv_pred_aukcija*ps.kolicina)),' +
        '0) SumaIznosPredAukcija'
      'from jn_e_aukcija_stavki aps'
      'inner join jn_ponudi_stavki ps on ps.id = aps.ponuda_stavki_id'
      'inner join jn_e_aukcija a on a.id = aps.e_aukcija_id'
      'where ps.ponuda_id = :mas_id and aps.bodiranje_po = '#39'01'#39
      'group by 1'
      'order by a.datum desc')
    Left = 504
    Top = 384
  end
  object MainMenu1: TMainMenu
    Images = dmRes.cxSmallImages
    Left = 480
    Top = 88
  end
end
