unit Ponudi;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 05.03.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxPCdxBarPopupMenu, cxRichEdit, cxDBRichEdit, dxCustomHint, cxHint,
  FIBDataSet, pFIBDataSet, cxBlobEdit, cxLabel, cxDBLabel, cxSplitter,
  cxImageComboBox, FIBQuery, pFIBQuery, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxBarBuiltInMenu, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmPonudi = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavka: TAction;
    aBrisiStavka: TAction;
    cxPageControlPonudi: TcxPageControl;
    cxTabSheetPonudiTabelaren: TcxTabSheet;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheetPonudiDetalen: TcxTabSheet;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERSKRATENNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1KOMPLETNOST: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1GARANCII_POPUST: TcxGridDBColumn;
    cxGrid1DBTableView1DOKUMENTACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1PRETSTAVNIK: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_PONUDUVAC: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dsPartner: TDataSource;
    tblPartner: TpFIBDataSet;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    tblPartnerTIP_NAZIV: TFIBStringField;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerNAZIV_SKRATEN: TFIBStringField;
    tblPartnerRE: TFIBIntegerField;
    tblPartnerSTATUS: TFIBIntegerField;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarCombo1: TdxBarCombo;
    cxBarEditItem2: TcxBarEditItem;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    aZacuvajStavkiExcel: TAction;
    aPecatiTabelaStavki: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    dxComponentPrinter1Link2: TdxGridReportLink;
    aPodesuvanjePecatenjeStavki: TAction;
    aPageSetupStavki: TAction;
    aSnimiPecatenjeStavki: TAction;
    aBrisiPodesuvanjePecatenjeStavki: TAction;
    aSnimiIzgledStavki: TAction;
    aBrisiIzgledStavki: TAction;
    aGrupnoBodirawe: TAction;
    aZapisiGrupnoBodiranje: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    aRefreshStavki: TAction;
    cxGrid1DBTableView1TIPPARTENRNAZIV: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    Panel10: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label9: TLabel;
    Panel6: TPanel;
    cxButton1: TcxButton;
    cxSplitter3: TcxSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid3DBTableView1PONUDA_ID: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid3DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1OPIS: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1CENA_PRVA_BEZDDV: TcxGridDBColumn;
    cxGrid3DBTableView1CENABEZDDV: TcxGridDBColumn;
    cxGrid3DBTableView1DANOK: TcxGridDBColumn;
    cxGrid3DBTableView1CENA: TcxGridDBColumn;
    cxGrid3DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn;
    cxGrid3DBTableView1IZNOSCENA: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid3DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid3DBTableView1MK: TcxGridDBColumn;
    cxGrid3DBTableView1EN: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    Panel9: TPanel;
    Label8: TLabel;
    Label7: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxDBLabel2: TcxDBLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1BODIRANJE_PO: TcxGridDBColumn;
    cxGrid2DBTableView1KRITERIUMNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1vrednuvanjeNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1bodiranjeNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1MAX_BODOVI: TcxGridDBColumn;
    cxGrid2DBTableView1BODOVI: TcxGridDBColumn;
    cxGrid2DBTableView1VREDNOST: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    PanelGrupnoBodiranje: TPanel;
    Panel8: TPanel;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid4DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid4DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid4DBTableView1BODIRANJE_PO: TcxGridDBColumn;
    cxGrid4DBTableView1BODIRANJE: TcxGridDBColumn;
    cxGrid4DBTableView1KRITERIUMNAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1vrednuvanjeNaziv: TcxGridDBColumn;
    cxGrid4DBTableView1bodiranjeNaziv: TcxGridDBColumn;
    cxGrid4DBTableView1MAX_BODOVI: TcxGridDBColumn;
    cxGrid4DBTableView1BODOVI: TcxGridDBColumn;
    cxGrid4Level1: TcxGridLevel;
    btnOK: TcxButton;
    cxButton2: TcxButton;
    Panel7: TPanel;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridPopupMenu4: TcxGridPopupMenu;
    aAvtomatskoBodiranje: TAction;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton28: TdxBarLargeButton;
    cxGrid3DBTableView1ODBIENA: TcxGridDBColumn;
    cxGrid1DBTableView1ODBIENA: TcxGridDBColumn;
    dxBarManager1Bar9: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    aPPonudaOdPartnerSoOpisNaArtikal: TAction;
    aDizajnPonudaOdPartnerSoOpisNaArtikal: TAction;
    dxBarButton2: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    PopupMenu2: TPopupMenu;
    aPopUpMeniZadizajnReport: TAction;
    dxBarButton4: TdxBarButton;
    aPPonudaOdPartner: TAction;
    aDizajnPonudaOdPartner: TAction;
    N3: TMenuItem;
    aPListaSitePonud: TAction;
    aDizajnListaSitePonud: TAction;
    aPListaSitePonudiSoOpisNaArtikal: TAction;
    aDizajnListaSitePonudiSoOpisNaArtikal: TAction;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    aPListaSitePonudiSoBod: TAction;
    aDizajnListaSitePonudiSoBod: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N2: TMenuItem;
    N4: TMenuItem;
    aDizajnListaPonudiSoBodPoGrupaArtikal: TAction;
    aPListaPonudiSoBodPoGrupaArtikal: TAction;
    N5: TMenuItem;
    dxBarButton9: TdxBarButton;
    dxBarManager1Bar10: TdxBar;
    dxBarLargeButton29: TdxBarLargeButton;
    aDostaveniDokumenti: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    aAukcija: TAction;
    cxGrid3DBTableView1ODBIENA_OBRAZLOZENIE: TcxGridDBColumn;
    PanelPonudiDetalen: TPanel;
    cxGroupBoxPonuduvac: TcxGroupBox;
    Label69: TLabel;
    Label72: TLabel;
    Label74: TLabel;
    TIP_PARTNER: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    BROJ_PONUDUVAC: TcxDBTextEdit;
    PRETSTAVNIK: TcxDBTextEdit;
    PARTNER_NAZIV: TcxLookupComboBox;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGroupBoxPonuda: TcxGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    DATUM_PONUDA: TcxDBDateEdit;
    ZABELESKA: TcxDBRichEdit;
    cxGroupBoxOtvorenaPonuda: TcxGroupBox;
    PRIMENA_VO_ROK: TcxDBCheckBox;
    ZATVOREN_KOVERT: TcxDBCheckBox;
    OZNAKA_NE_OTVORAJ: TcxDBCheckBox;
    POVLEKUVANJE_PONUDA: TcxDBCheckBox;
    IZMENA_PONUDA: TcxDBCheckBox;
    KOMPLETNOST: TcxDBCheckBox;
    DOKUMENTACIJA: TcxDBRichEdit;
    Label6: TLabel;
    Label13: TLabel;
    ROK_VAZNOST: TcxDBTextEdit;
    Label1: TLabel;
    VALUTA: TcxDBLookupComboBox;
    cxDBCheckBoxValidna: TcxDBCheckBox;
    cxGrid1DBTableView1PRIMENA_VO_ROK: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVOREN_KOVERT: TcxGridDBColumn;
    cxGrid1DBTableView1OZNAKA_NE_OTVORAJ: TcxGridDBColumn;
    cxGrid1DBTableView1POVLEKUVANJE_PONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1IZMENA_PONUDA: TcxGridDBColumn;
    cxGrid1DBTableView1ROK_VAZNOST: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid1DBTableView1GARANCIJA_IZNOS: TcxGridDBColumn;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    aPIzvestuvanjeZaOtfrlenaPonuda: TAction;
    aDIzvestuvanjeZaOtfrlenaPonuda: TAction;
    N6: TMenuItem;
    aPPocetnaRangListaZaUcestvoNaEAukcija: TAction;
    aDPocetnaRangListaZaUcestvoNaEAukcija: TAction;
    dxBarButton12: TdxBarButton;
    N11: TMenuItem;
    dxBarButton13: TdxBarButton;
    aPecatiCeniPredIPotoa: TAction;
    aDecatiCeniPredIPotoa: TAction;
    N12: TMenuItem;
    dxBarLargeButton31: TdxBarLargeButton;
    aZacuvajSostojbaPredAukcija: TAction;
    qAukcijaDatum1: TpFIBQuery;
    dxBarButton14: TdxBarButton;
    aPIzvesstajPoGrupiIPartneri: TAction;
    aDIzvesstajPoGrupiIPartneri: TAction;
    N13: TMenuItem;
    qAukcijaDatum: TpFIBQuery;
    grp1: TGroupBox;
    lbl1: TLabel;
    lbl2: TLabel;
    NAZIVGARANCIJA_IZNOS: TcxDBTextEdit;
    GARANCIJA_DATUM: TcxDBDateEdit;
    cxGrid1DBTableView1GARANCIJA_DATUM: TcxGridDBColumn;
    cxGrid4DBTableView1VREDNOST: TcxGridDBColumn;
    cxGrid3DBTableView1STAVKA_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1STAVKA_MERKA: TcxGridDBColumn;
    dxBarButton15: TdxBarButton;
    actPIzvestuvanjeZaNedobitnaFirma: TAction;
    actDIzvestuvanjeZaNedobitnaFirma: TAction;
    N14: TMenuItem;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    actPIzvestuvanjeZaOtfrlenaPonudaGrupno: TAction;
    actDIzvestuvanjeZaOtfrlenaPonudaGrupno: TAction;
    actPIzvestuvanjeZaNedobitnaFirmaGrupno: TAction;
    actDIzvestuvanjeZaNedobitnaFirmaGrupno: TAction;
    N15: TMenuItem;
    N16: TMenuItem;
    lbl3: TLabel;
    ROK_ISPORAKA: TcxDBTextEdit;
    lbl4: TLabel;
    ROK_FAKTURA: TcxDBTextEdit;
    cxGrid1DBTableView1ROK_FAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView1ROK_ISPORAKA: TcxGridDBColumn;
    dxBarManager1Bar11: TdxBar;
    dxBarLargeButton32: TdxBarLargeButton;
    actDogovor: TAction;
    GARANTEN_ROK: TcxDBTextEdit;
    lbl5: TLabel;
    cxGrid1DBTableView1GARANTEN_ROK: TcxGridDBColumn;
    actDDogovori_Stoki: TAction;
    actDDogovori_Uslugi: TAction;
    actDDogovori_Raboti: TAction;
    actDDogovoriRaboti1: TMenuItem;
    actDDogovoriStoki1: TMenuItem;
    actDDogovoriUslugi1: TMenuItem;
    dxBarLargeButton33: TdxBarLargeButton;
    actDogovorKreiraj: TAction;
    GARANCII_POPUST: TcxDBMemo;
    lbl6: TLabel;
    dxBarButton18: TdxBarButton;
    dxBarButton19: TdxBarButton;
    actPIzvestuvanjeZaOtfrlenaPonudaExportPdf: TAction;
    actPIzvestuvanjeZaNedobitnaFirmaExportPdf: TAction;
    dlgOpenExportPdf: TOpenDialog;
    dxBarButton20: TdxBarButton;
    actDDogovorOtstapuvanjeOpremaNaKoristenje: TAction;
    actPDogovorOtstapuvanjeOpremaNaKoristenje: TAction;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton21: TdxBarButton;
    actDDogovorOtstapuvanjeOpremaNaKoristenje1: TMenuItem;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aDodadiStavkaExecute(Sender: TObject);
    procedure GARANCII_POPUSTDblClick(Sender: TObject);
    procedure ZABELESKADblClick(Sender: TObject);
    procedure DOKUMENTACIJADblClick(Sender: TObject);

    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure PARTNER_NAZIVPropertiesEditValueChanged(Sender: TObject);
    procedure dxBarLargeButton18Click(Sender: TObject);
    procedure aBrisiStavkaExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZacuvajStavkiExcelExecute(Sender: TObject);
    procedure aPecatiTabelaStavkiExecute(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aPageSetupStavkiExecute(Sender: TObject);
    procedure aSnimiPecatenjeStavkiExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiExecute(Sender: TObject);
    procedure aBrisiIzgledStavkiExecute(Sender: TObject);
    procedure aGrupnoBodiraweExecute(Sender: TObject);
    procedure aZapisiGrupnoBodiranjeExecute(Sender: TObject);
    procedure cxGrid4DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aRefreshStavkiExecute(Sender: TObject);
    procedure cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGrid4DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure aAvtomatskoBodiranjeExecute(Sender: TObject);
    procedure cxGrid3DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid3DBTableView1ODBIENAPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid1DBTableView1ODBIENAPropertiesEditValueChanged(
      Sender: TObject);
    procedure aPPonudaOdPartnerSoOpisNaArtikalExecute(Sender: TObject);
    procedure aDizajnPonudaOdPartnerSoOpisNaArtikalExecute(Sender: TObject);
    procedure aPopUpMeniZadizajnReportExecute(Sender: TObject);
    procedure aPPonudaOdPartnerExecute(Sender: TObject);
    procedure aDizajnPonudaOdPartnerExecute(Sender: TObject);
    procedure aPListaSitePonudExecute(Sender: TObject);
    procedure aPListaSitePonudiSoOpisNaArtikalExecute(Sender: TObject);
    procedure aDizajnListaSitePonudExecute(Sender: TObject);
    procedure aDizajnListaSitePonudiSoOpisNaArtikalExecute(Sender: TObject);
    procedure aPListaSitePonudiSoBodExecute(Sender: TObject);
    procedure aDizajnListaSitePonudiSoBodExecute(Sender: TObject);
    procedure aDizajnListaPonudiSoBodPoGrupaArtikalExecute(Sender: TObject);
    procedure aPListaPonudiSoBodPoGrupaArtikalExecute(Sender: TObject);
    procedure aDostaveniDokumentiExecute(Sender: TObject);
    procedure aAukcijaExecute(Sender: TObject);
    procedure cxGrid3DBTableView1ODBIENA_OBRAZLOZENIEPropertiesEditValueChanged(
      Sender: TObject);
    procedure cxGrid3DBTableView1EditValueChanged(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure aDIzvestuvanjeZaOtfrlenaPonudaExecute(Sender: TObject);
    procedure aPIzvestuvanjeZaOtfrlenaPonudaExecute(Sender: TObject);
    procedure aPPocetnaRangListaZaUcestvoNaEAukcijaExecute(Sender: TObject);
    procedure aDPocetnaRangListaZaUcestvoNaEAukcijaExecute(Sender: TObject);
    procedure aPecatiCeniPredIPotoaExecute(Sender: TObject);
    procedure aDecatiCeniPredIPotoaExecute(Sender: TObject);
    procedure aZacuvajSostojbaPredAukcijaExecute(Sender: TObject);
    procedure aPIzvesstajPoGrupiIPartneriExecute(Sender: TObject);
    procedure aDIzvesstajPoGrupiIPartneriExecute(Sender: TObject);
    procedure actPIzvestuvanjeZaNedobitnaFirmaExecute(Sender: TObject);
    procedure actDIzvestuvanjeZaNedobitnaFirmaExecute(Sender: TObject);
    procedure actPIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute(Sender: TObject);
    procedure actDIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute(Sender: TObject);
    procedure actPIzvestuvanjeZaNedobitnaFirmaGrupnoExecute(Sender: TObject);
    procedure actDIzvestuvanjeZaNedobitnaFirmaGrupnoExecute(Sender: TObject);
    procedure actDogovorExecute(Sender: TObject);
    procedure actDDogovori_StokiExecute(Sender: TObject);
    procedure actDDogovori_UslugiExecute(Sender: TObject);
    procedure actDDogovori_RabotiExecute(Sender: TObject);
    procedure actDogovorKreirajExecute(Sender: TObject);
    procedure actPIzvestuvanjeZaOtfrlenaPonudaExportPdfExecute(Sender: TObject);
    procedure actPIzvestuvanjeZaNedobitnaFirmaExportPdfExecute(Sender: TObject);
    procedure actDDogovorOtstapuvanjeOpremaNaKoristenjeExecute(Sender: TObject);
    procedure actPDogovorOtstapuvanjeOpremaNaKoristenjeExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmPonudi: TfrmPonudi;
implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit,
  IzborStavkiPonuda, dmMaticni, Notepad, dmProcedureQuey,
  PonuduvacDostaveniDokumenti, eAukcija, dmReportUnit, EAukcijaOsnova, Dogovor;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmPonudi.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

constructor TfrmPonudi.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmPonudi.aNovExecute(Sender: TObject);
begin
  if dm.tblTenderPONISTEN.Value = 1 then
    begin
      if dm.tblTenderZATVOREN.Value = 0  then
         begin
           if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelGrupnoBodiranje.Visible = false) then
             begin
               cxPageControlPonudi.ActivePage:=cxTabSheetPonudiDetalen;
               PanelPonudiDetalen.Enabled:=True;
               cxTabSheetPonudiTabelaren.Enabled:=false;

               BROJ.Enabled:=True;
               TIP_PARTNER.Enabled:=True;
               PARTNER.Enabled:=True;
               PARTNER_NAZIV.Enabled:=True;
               cxDBCheckBoxValidna.Enabled:=True;

               Broj.SetFocus;
               cxGrid1DBTableView1.DataController.DataSet.Insert;
               dm.tblPonudiDATUM_PONUDA.Value:=Now;
               dm.tblPonudiBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
             end
           else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
         end
      else ShowMessage('������ ��������� �� �������� �� ���������� �� ������� !');
    end
  else ShowMessage('���������� � ���������. �� � ��������� �������� �� ���� ������ !');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmPonudi.aAukcijaExecute(Sender: TObject);
begin
if dm.tblTenderPONISTEN.Value = 1 then
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelGrupnoBodiranje.Visible = false) then
       begin
          if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
             begin
                if (dm.tblPonudiodbiena.Value = 0)  then
                    begin
                       qAukcijaDatum.Close;
                       qAukcijaDatum.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
                       qAukcijaDatum.ExecQuery;
                       if qAukcijaDatum.FldByName['datum'].IsNull then
                          begin
                            ShowMessage('������ �������� �� ������������ ������� �� �������� �� �������� !!!');
                          end
                       else
                          begin
                           // if dobitnici_count = 0 then
                              // begin
                                 frmAukcija:=TfrmAukcija.Create(Application);
                                 frmAukcija.ShowModal;
                                 frmAukcija.Free;
                               //end
                            //else ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� ��������� �� ���� �������� �� �������� !');
                          end;
                    end
                else ShowMessage('��������� ������ � ������� !!!');
             end
          else ShowMessage('������ ������������ ������ !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end
else ShowMessage('���������� � ���������. �� � ��������� ������ !');
end;

procedure TfrmPonudi.aAvtomatskoBodiranjeExecute(Sender: TObject);
begin
if dm.tblTenderPONISTEN.Value = 1 then
begin
           dmPQ.insert6(dmPQ.pBodiranjeArt,'BROJ_TENDER', Null,Null,Null,Null,Null,
                          dm.tblTenderBROJ.Value,
                          Null, Null,Null,Null,Null);
           dm.tblBodiranjeStavkiPonudi.FullRefresh;
           ponuda_stavka_edit:=0;
           ShowMessage('������������ �������� � �������� !!!');
end
else ShowMessage('���������� � ���������. �� � ��������� �������� !');
end;

procedure TfrmPonudi.aAzurirajExecute(Sender: TObject);
begin
    if dm.tblTenderPONISTEN.Value = 1 then
      begin
        if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelGrupnoBodiranje.Visible = false)then
          begin
            dm.qCountDobitnici.Close;
            dm.qCountDobitnici.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
            dm.qCountDobitnici.ExecQuery;

            cxPageControlPonudi.ActivePage:=cxTabSheetPonudiDetalen;
            PanelPonudiDetalen.Enabled:=True;
            cxTabSheetPonudiTabelaren.Enabled:=false;
            Broj.SetFocus;

//            if dm.qCountDobitnici.FldByName['br'].Value = 0 then
//               begin
//                  BROJ.Enabled:=True;
//                  TIP_PARTNER.Enabled:=True;
//                  PARTNER.Enabled:=True;
//                  PARTNER_NAZIV.Enabled:=True;
//                  cxDBCheckBoxValidna.Enabled:=True;
//               end
//            else
//               begin
//                  BROJ.Enabled:=False;
//                  TIP_PARTNER.Enabled:=False;
//                  PARTNER.Enabled:=False;
//                  PARTNER_NAZIV.Enabled:=False;
//                  cxDBCheckBoxValidna.Enabled:=False;
//                  ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� ��������� �� ���� �������� �� �������� !');
//               end;
            cxGrid1DBTableView1.DataController.DataSet.Edit;
          end
        else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
      end
    else ShowMessage('���������� � ���������. �� � ��������� ��������� �� ������ !');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmPonudi.aBrisiExecute(Sender: TObject);
begin
if dm.tblTenderPONISTEN.Value = 1 then
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
     cxGrid1DBTableView1.DataController.DataSet.Delete();
end
else ShowMessage('���������� � ���������. �� � ��������� ������ ������ !');
end;

procedure TfrmPonudi.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmPonudi.aBrisiIzgledStavkiExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid3DBTableView1);
     BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmPonudi.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmPonudi.aRefreshStavkiExecute(Sender: TObject);
begin
     cxGrid3DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
     cxGrid3DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmPonudi.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmPonudi.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmPonudi.aSnimiIzgledStavkiExecute(Sender: TObject);
begin
    zacuvajGridVoBaza(Name,cxGrid3DBTableView1);
    ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmPonudi.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� ����� �������');
end;

procedure TfrmPonudi.aZacuvajSostojbaPredAukcijaExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelGrupnoBodiranje.Visible = false) then
       begin
          if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
             begin
                  frmEAukcijaOsnova:=TfrmEAukcijaOsnova.Create(Application);
                  frmEAukcijaOsnova.ShowModal;
                  frmEAukcijaOsnova.Free;
              end
          else ShowMessage('������ ������������ ������ !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmPonudi.aZacuvajStavkiExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid3, '������ �� ������ �� ����� �������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmPonudi.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmPonudi.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmPonudi.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
 if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
   begin
    if Sender = PARTNER_NAZIV then
       begin
         dm.tblPonudiTIP_PARTNER.Value:=tblPartnerTIP_PARTNER.Value;
         dm.tblPonudiPARTNER.Value:=tblPartnerID.Value;

          dm.qZemiPretsavnikPonuda.Close;
          dm.qZemiPretsavnikPonuda.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
          dm.qZemiPretsavnikPonuda.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
          dm.qZemiPretsavnikPonuda.ExecQuery;

          if not dm.qZemiPretsavnikPonuda.FldByName['pretstavnik'].IsNull then
             dm.tblPonudiPRETSTAVNIK.Value:=dm.qZemiPretsavnikPonuda.FldByName['pretstavnik'].Value;
       end
    else if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
         begin
             ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNER_NAZIV);
//             if TIP_PARTNER.Text <> '' then
//                begin
//                   tblPartner.Close;
//                   tblPartner.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
//                   tblPartner.Open;
//                end;
         end;
   end;
end;

procedure TfrmPonudi.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     ZemiImeDvoenKluc(TIP_PARTNER,PARTNER,PARTNER_NAZIV);
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmPonudi.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmPonudi.ZABELESKADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblPonudi.CreateBlobStream(dm.tblPonudiZABELESKA , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblPonudiZABELESKA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
procedure TfrmPonudi.GARANCII_POPUSTDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblPonudiGARANCII_POPUST.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblPonudiGARANCII_POPUST.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

function TfrmPonudi.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmPonudi.PARTNER_NAZIVPropertiesEditValueChanged(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        if (PARTNER_NAZIV.Text <>'') then
        begin
          dm.tblPonudiTIP_PARTNER.Value := PARTNER_NAZIV.EditValue[0];
          dm.tblPonudiPARTNER.Value := PARTNER_NAZIV.EditValue[1];

          dm.qZemiPretsavnikPonuda.Close;
          dm.qZemiPretsavnikPonuda.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
          dm.qZemiPretsavnikPonuda.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
          dm.qZemiPretsavnikPonuda.ExecQuery;

          if not dm.qZemiPretsavnikPonuda.FldByName['pretstavnik'].IsNull then
             dm.tblPonudiPRETSTAVNIK.Value:=dm.qZemiPretsavnikPonuda.FldByName['pretstavnik'].Value;
        end
        else
        begin
          dm.tblPonudiTIP_PARTNER.Clear;
          dm.tblPonudiPARTNER.Clear;
        end;
    end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPonudi.prefrli;
begin
end;

procedure TfrmPonudi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

    dm.qCountBodiranjeStavki.Close;
    dm.qCountBodiranjeStavki.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dm.qCountBodiranjeStavki.ExecQuery;
    if dm.qCountBodiranjeStavki.FldByName['br'].Value >0 then
       begin
        if ponuda_stavka_edit=1 then
           begin
            ShowMessage('��������� �� ������� �� ��������. ������� ���������� �������� �� �������� !!!');
            dmPQ.insert6(dmPQ.pBodiranjeArt,'BROJ_TENDER', Null,Null,Null,Null,Null,
                        dm.tblTenderBROJ.Value,Null, Null,Null,Null,Null);
            dm.tblBodiranjeStavkiPonudi.FullRefresh;
            ShowMessage('������������ �������� � �������� !!!');
            end;
       end;

end;
procedure TfrmPonudi.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmPonudi.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    Caption:='������� � ��������� �� ������ �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' ������ '+IntToStr(dm.tblTenderGODINA.Value);
    cxTabSheetPonudiTabelaren.Caption:='��������� ������ �� ������ �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' ������ '+IntToStr(dm.tblTenderGODINA.Value);
    cxPageControlPonudi.ActivePage:=cxTabSheetPonudiTabelaren;
    cxGrid1.SetFocus;

    PanelGrupnoBodiranje.Top:=315;
    PanelGrupnoBodiranje.Left:=365;

    dm.tblPonudi.Close;
    dm.tblPonudi.Open;

    tblPartner.Close;
    tblPartner.ParamByName('tip').Value:='%';
    tblPartner.Open;

    dm.tblPonudiStavki.Close;
    dm.tblPonudiStavki.Open;

    dm.tblBodiranjeStavkiPonudi.Close;
    dm.tblBodiranjeStavkiPonudi.Open;

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajGridOdBaza(Name,cxGrid3DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid3DBTableView1.Name, dxComponentPrinter1Link2);

    if dm.tblTenderPONISTEN.Value = 0 then
       begin
         cxGrid1DBTableView1.OptionsData.Editing:=False;
         cxGrid2DBTableView1.OptionsData.Editing:=False;
         cxGrid3DBTableView1.OptionsData.Editing:=False;
         cxButton1.Enabled:=False;
       end;

     dm.qCountDobitnici.Close;
     dm.qCountDobitnici.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
     dm.qCountDobitnici.ExecQuery;
     if dm.qCountDobitnici.FldByName['br'].Value = 0 then
        dobitnici_count:=0
     else
        dobitnici_count:=1;
    ponuda_stavka_edit:=0;

    //------------ ZATVORI_PONUDI_STATUS -------------

    dm.qZatvoriPonudaStatus.Close;
    dm.qZatvoriPonudaStatus.ParamByName('tender_broj').Value:=dm.tblTenderBROJ.Value;
    dm.qZatvoriPonudaStatus.ExecQuery;

  if (dm.qZatvoriPonudaStatus.FldByName['zatvori_ponudi_status'].Value = 1) then
     begin
       aNov.Enabled:=False;
       aAzuriraj.Enabled:=False;
       aBrisi.Enabled:=False;
       aRefresh.Enabled:=False;
       aDodadiStavka.Enabled:=False;
       aBrisiStavka.Enabled:=False;
       aRefreshStavki.Enabled:=False;
       aAvtomatskoBodiranje.Enabled:=False;
       aZacuvajSostojbaPredAukcija.Enabled:=False;
       aAukcija.Enabled:=False;
       aDostaveniDokumenti.Enabled:=False;

       cxGrid3DBTableView1.OptionsData.Editing:=False;
       cxGrid2DBTableView1.OptionsData.Editing:=False;
     end;

   if dm.tblTenderGRUPA.Value = 1 then  // stoki
       begin
         dxBarSubItem5.Visible:=ivAlways;
       end
    //------------------------------------------------

end;
//------------------------------------------------------------------------------

procedure TfrmPonudi.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmPonudi.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmPonudi.cxGrid1DBTableView1ODBIENAPropertiesEditValueChanged(
  Sender: TObject);
begin


     dm.tblPonudi.Edit;
    // if dobitnici_count = 0 then
      // begin
        dm.tblPonudi.Post;
        dm.qCountBodiranjeStavki.Close;
        dm.qCountBodiranjeStavki.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dm.qCountBodiranjeStavki.ExecQuery;
        if dm.qCountBodiranjeStavki.FldByName['br'].Value >0 then
            begin
              dmPQ.insert6(dmPQ.pBodiranjeArt,'BROJ_TENDER', Null,Null,Null,Null,Null,
                          dm.tblTenderBROJ.Value,
                          Null, Null,Null,Null,Null);
              dm.tblBodiranjeStavkiPonudi.FullRefresh;
              ShowMessage('���������� �������� � �������� !!!');
            end;
       //end
     //else
       //begin
       // ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� ��������� �� ������ �� �������� !');
       // dm.tblPonudi.Cancel;
     //  end;
end;

procedure TfrmPonudi.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1ODBIENA.Index] = 1) then
       AStyle := dmRes.RedLight;
end;

procedure TfrmPonudi.cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
  var pom, a:Double;
begin
   //   if AValue > 100 then
     //   begin
        //   ShowMessage('������ �� �������� ��������� 100 !!!');
       // end;
end;

procedure TfrmPonudi.cxGrid3DBTableView1EditValueChanged(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
begin
     dm.tblPonudiStavki.Edit;
     dm.tblPonudiStavki.Post;
end;

procedure TfrmPonudi.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
       if (cxGrid3DBTableView1.Controller.FocusedColumn <> cxGrid3DBTableView1KOLICINA)
       and (cxGrid3DBTableView1.Controller.FocusedColumn <> cxGrid3DBTableView1CENABEZDDV)
       and (cxGrid3DBTableView1.Controller.FocusedColumn <> cxGrid3DBTableView1DANOK)
       and (cxGrid3DBTableView1.Controller.FocusedColumn <> cxGrid3DBTableView1CENA)
       and (cxGrid3DBTableView1.Controller.FocusedColumn <> cxGrid3DBTableView1CENA_PRVA_BEZDDV)then
     begin
       if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1 );
     end;
end;

procedure TfrmPonudi.cxGrid3DBTableView1ODBIENAPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPonudiStavki.Edit;
     dm.tblPonudiStavki.Post;
     ponuda_stavka_edit:=1;
//     if dobitnici_count = 0 then
//       begin
//        dm.tblPonudiStavki.Post;
//        ponuda_stavka_edit:=1;
//       end
//     else
//        begin
//          dm.tblPonudiStavki.Cancel;
//          ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� ��������� �� �������� !');
//        end;
//     dm.tblPonudiStavki.FullRefresh;
end;

procedure TfrmPonudi.cxGrid3DBTableView1ODBIENA_OBRAZLOZENIEPropertiesEditValueChanged(
  Sender: TObject);
begin
     dm.tblPonudiStavki.Edit;
     dm.tblPonudiStavki.Post;
end;

procedure TfrmPonudi.cxGrid3DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
    if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid3DBTableView1ODBIENA.Index] = 1) then
       AStyle := dmRes.RedLight;
end;

procedure TfrmPonudi.cxGrid4DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if (cxGrid4DBTableView1.Controller.FocusedColumn <> cxGrid4DBTableView1VREDNOST)
       and (cxGrid4DBTableView1.Controller.FocusedColumn <> cxGrid4DBTableView1BODOVI)then
     begin
       if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid4DBTableView1 );
     end;
end;

procedure TfrmPonudi.cxGrid4DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
   //   if AValue > 100 then
     //   begin
        //   ShowMessage('������ �� �������� ��������� 100 !!!');
       // end;
end;

procedure TfrmPonudi.DOKUMENTACIJADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblPonudi.CreateBlobStream(dm.tblPonudiDOKUMENTACIJA , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblPonudiDOKUMENTACIJA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmPonudi.dxBarButton1Click(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '������ �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' ������ '+IntToStr(dm.tblTenderGODINA.Value);

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPonudi.dxBarLargeButton18Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmPonudi.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelPonudiDetalen) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        PanelPonudiDetalen.Enabled:=false;
        cxTabSheetPonudiTabelaren.Enabled:=true;
        cxPageControlPonudi.ActivePage:=cxTabSheetPonudiTabelaren;
        cxGrid1.SetFocus;
        dm.tblPonudiStavki.Close;
        dm.tblPonudiStavki.Open;

        dm.tblBodiranjeStavkiPonudi.Close;
        dm.tblBodiranjeStavkiPonudi.Open;

    end;
  end;
end;

procedure TfrmPonudi.aZapisiGrupnoBodiranjeExecute(Sender: TObject);
var i:Integer;
begin
      with cxGrid4DBTableView1.DataController do
      for I := 0 to RecordCount - 1 do
         begin
            dmPQ.insert6(dmPQ.pGrupnoVrednuvanjeStavkiPonudi,'PONUDA_ID','VREDNOST','BODOVI','BODIRANJE_PO', Null,Null,dm.tblPonudiID.Value,GetValue(i,cxGrid4DBTableView1VREDNOST.Index),GetValue(i,cxGrid4DBTableView1BODOVI.Index),GetValue(i,cxGrid4DBTableView1BODIRANJE_PO.Index),Null,Null );
         end;
      dm.tblBodiranjeStavkiPonudi.Close;
      dm.tblBodiranjeStavkiPonudi.Open;
      PanelGrupnoBodiranje.Visible:=False;
end;
//	����� �� ���������� �� �������
procedure TfrmPonudi.aOtkaziExecute(Sender: TObject);
begin
  if PanelGrupnoBodiranje.Visible = true then
  begin
     PanelGrupnoBodiranje.Visible:=False;
  end
  else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelPonudiDetalen);
      PanelPonudiDetalen.Enabled := false;
      cxTabSheetPonudiTabelaren.Enabled:=true;
      cxPageControlPonudi.ActivePage:=cxTabSheetPonudiTabelaren;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmPonudi.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmPonudi.aPageSetupStavkiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmPonudi.aPecatiCeniPredIPotoaExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40016);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '������ �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' ������ '+IntToStr(dm.tblTenderGODINA.Value);

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ �� ����� ������� ���: ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPonudi.aPecatiTabelaStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '������ �� ������ ��� '+dm.tblPonudiBROJ.Value;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('���������: ' + TIP_PARTNER.Text + '-' + PARTNER.Text + ' ' + PARTNER_NAZIV.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmPonudi.aPIzvesstajPoGrupiIPartneriExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40039);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPIzvestuvanjeZaOtfrlenaPonudaExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40028);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    if dm.tblPonudiODBIENA.Value = 1 then
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'))
    else
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('�� � ��������� �� ���������'));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;

end;

procedure TfrmPonudi.aPListaPonudiSoBodPoGrupaArtikalExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40013);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPListaSitePonudExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40010);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPListaSitePonudiSoBodExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40012);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPListaSitePonudiSoOpisNaArtikalExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40011);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmPonudi.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPonudi.aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmPonudi.aPopUpMeniZadizajnReportExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500 );
end;

procedure TfrmPonudi.aPPocetnaRangListaZaUcestvoNaEAukcijaExecute(
  Sender: TObject);
begin
try
    if dm.tblTenderTIP_OCENA.Value = 1 then
       begin
          dmRes.Spremi('JN',40032);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupi.Close;
          dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupi.Open;
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
       end
    else if dm.tblTenderTIP_OCENA.Value = 0 then
       begin
          dmRes.Spremi('JN',40033);
          dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmKon.tblSqlReport.Open;

          dmReport.PocetnaRangListaNedeliviGrupi.Close;
          dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
          dmReport.PocetnaRangListaNedeliviGrupi.Open;
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmRes.frxReport1.ShowReport();
       end
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPPonudaOdPartnerExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40009);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aPPonudaOdPartnerSoOpisNaArtikalExecute(Sender: TObject);
begin
  try
    dmRes.Spremi('JN',40008);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmPonudi.aSnimiPecatenjeStavkiExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid3DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmPonudi.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmPonudi.aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid3DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmPonudi.aBrisiStavkaExecute(Sender: TObject);
begin
if dm.tblTenderPONISTEN.Value = 1 then
begin
     if ((cxGrid3DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid3DBTableView1.DataController.RecordCount <> 0)) then
       begin
        //  dm.qCountDobitnici.Close;
        //  dm.qCountDobitnici.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        //  dm.qCountDobitnici.ExecQuery;

        //  if dm.qCountDobitnici.FldByName['br'].Value = 0 then
        //     begin
               cxGrid3DBTableView1.DataController.DataSet.Delete();
               ponuda_stavka_edit:=1;
       //      end
    //      Else ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� ������ �� ������ �� �������� !');
       end;
end
else ShowMessage('���������� � ���������. �� � ��������� ������ ������ !');
end;

procedure TfrmPonudi.actDDogovori_RabotiExecute(Sender: TObject);
begin
       dmRes.Spremi('JN',40174);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDDogovori_StokiExecute(Sender: TObject);
begin
       dmRes.Spremi('JN',40172);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDDogovori_UslugiExecute(Sender: TObject);
begin
            dmRes.Spremi('JN',40173);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDIzvestuvanjeZaNedobitnaFirmaExecute(Sender: TObject);
begin
       dmRes.Spremi('JN',40169);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDIzvestuvanjeZaNedobitnaFirmaGrupnoExecute(
  Sender: TObject);
begin
       dmRes.Spremi('JN',40177);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute(
  Sender: TObject);
begin
       dmRes.Spremi('JN',40176);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actDogovorExecute(Sender: TObject);
begin
//
  try
    if dm.tblTenderGRUPA.Value = 1 then
       begin
         dmRes.Spremi('JN',40172);   // stoki
       end
    else if dm.tblTenderGRUPA.Value = 2 then
       begin
         dmRes.Spremi('JN',40173);  // uslugi
         dmReport.tblDogovorStavkiOdluka.Close;
         dmReport.tblDogovorStavkiOdluka.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
         dmReport.tblDogovorStavkiOdluka.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
         dmReport.tblDogovorStavkiOdluka.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
         dmReport.tblDogovorStavkiOdluka.Open;
       end
    else if dm.tblTenderGRUPA.Value = 3 then
       begin
         dmRes.Spremi('JN',40174);  // raboti
       end;
    dmKon.tblSqlReport.ParamByName('PONUDA_ID').Value:=dm.tblPonudiID.Value;
    dmKon.tblSqlReport.Open;

    dmReport.tblDogovorStavki.Close;
    dmReport.tblDogovorStavki.ParamByName('BROJ_TENDER').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmReport.tblDogovorStavki.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmReport.tblDogovorStavki.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmReport.tblDogovorStavki.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.actDogovorKreirajExecute(Sender: TObject);
begin
     dm.qCountDogovori.Close;
     dm.qCountDogovori.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
     dm.qCountDogovori.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
     dm.qCountDogovori.ParamByName('broj_tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
     dm.qCountDogovori.ExecQuery;

     if dm.qCountDogovori.FldByName['br'].Value = 0 then
        begin
          frmDogovor:=TfrmDogovor.Create(Self, false);
          frmDogovor.Tag:=1;
          frmDogovor.ShowModal;
          frmDogovor.Free;
        end
     else ShowMessage('����� ������������ ������� �� ������������� �������');
end;

procedure TfrmPonudi.actDDogovorOtstapuvanjeOpremaNaKoristenjeExecute(
  Sender: TObject);
begin
       dmRes.Spremi('JN',40179);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.actPDogovorOtstapuvanjeOpremaNaKoristenjeExecute(
  Sender: TObject);
begin
  try

    dmRes.Spremi('JN',40179);

    dmKon.tblSqlReport.ParamByName('PONUDA_ID').Value:=dm.tblPonudiID.Value;
    dmKon.tblSqlReport.Open;

    dmReport.tblDogovorStavki.Close;
    dmReport.tblDogovorStavki.ParamByName('BROJ_TENDER').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmReport.tblDogovorStavki.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmReport.tblDogovorStavki.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmReport.tblDogovorStavki.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.actPIzvestuvanjeZaNedobitnaFirmaExecute(Sender: TObject);
begin
  try
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

    dmRes.Spremi('JN',40169);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    if dm.tblPonudiODBIENA.Value = 1 then
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'))
    else
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('�� � ��������� �� ���������'));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.actPIzvestuvanjeZaNedobitnaFirmaExportPdfExecute(
  Sender: TObject);
  var file_pateka:string;
      zafakt:integer;
      i, vkupno :integer;
begin
     dlgOpenExportPdf.Execute();
     dm.tblNedobitniPdf.Close;
     dm.tblNedobitniPdf.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     dm.tblNedobitniPdf.Open;
     dm.tblNedobitniPdf.FetchAll;
     vkupno := dm.tblNedobitniPdf.RecordCount;
     if vkupno > 0 then
       begin
         dm.tblNedobitniPdf.First;
         try
           for i := 0 to vkupno - 1 do
             begin
                try
                   dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
                   dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                   dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:=dm.tblNedobitniPdfTIP_PARTNER.Value;
                   dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:=dm.tblNedobitniPdfPARTNER.Value;
                   dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

                   dmRes.Spremi('JN',40169);
                   dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
                   dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblNedobitniPdfTIP_PARTNER.Value;
                   dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblNedobitniPdfPARTNER.Value;
                   dmKon.tblSqlReport.Open;

                   dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
                   if dm.tblNedobitniPdfODBIENA.Value = 1 then
                     dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'))
                   else
                     dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('�� � ��������� �� ���������'));

                   file_pateka := dlgOpenExportPdf.FileName+IntToStr(dm.tblNedobitniPdfTIP_PARTNER.Value)+'_'+ IntToStr(dm.tblNedobitniPdfPARTNER.Value)+ '.pdf';

                   dmRes.frxPDFExport1.ShowDialog := False;
                   dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
                   dmRes.frxPDFExport1.FileName := file_pateka;
                   dmRes.frxReport1.PrepareReport(True);
                   dmRes.frxReport1.Export(dmRes.frxPDFExport1);

                  dm.tblNedobitniPdf.Next;
                except
                 ShowMessage('�� ���� �� �� ������� ���������!');
                end;
             end
         finally
            ShowMessage('������������� �� ��������� �� �������� !');
         end;
       end  else ShowMessage('���� ��������� �� �����������!');
end;

procedure TfrmPonudi.actPIzvestuvanjeZaNedobitnaFirmaGrupnoExecute(
  Sender: TObject);
begin
  try
    dmReport.tblSiteDobitniciVoIzvestajNeobitni.Close;
    dmReport.tblSiteDobitniciVoIzvestajNeobitni.ParamByName('broj_tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmReport.tblSiteDobitniciVoIzvestajNeobitni.Open;

    dmRes.Spremi('JN',40177);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    if dm.tblPonudiODBIENA.Value = 1 then
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'))
    else
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('�� � ��������� �� ���������'));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.actPIzvestuvanjeZaOtfrlenaPonudaExportPdfExecute(
  Sender: TObject);
  var file_pateka:string;
      zafakt:integer;
      i, vkupno :integer;
begin
     dlgOpenExportPdf.Execute();
     dm.tblOdbieniPdf.Close;
     dm.tblOdbieniPdf.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     dm.tblOdbieniPdf.Open;
     dm.tblOdbieniPdf.FetchAll;
     vkupno := dm.tblOdbieniPdf.RecordCount;
     if vkupno > 0 then
       begin
         dm.tblOdbieniPdf.First;
         try
           for i := 0 to vkupno - 1 do
             begin
                try
                  dmRes.Spremi('JN',40028);
                  dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
                  dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblOdbieniPdfTIP_PARTNER.Value;
                  dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblOdbieniPdfPARTNER.Value;
                  dmKon.tblSqlReport.Open;

                  dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
                  dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'));
                  file_pateka := dlgOpenExportPdf.FileName+IntToStr(dm.tblOdbieniPdfTIP_PARTNER.Value)+'_'+ IntToStr(dm.tblOdbieniPdfPARTNER.Value)+ '.pdf';

                  dmRes.frxPDFExport1.ShowDialog := False;
                  dmRes.frxPDFExport1.FileName := file_pateka;
                  dmRes.frxReport1.PrepareReport(True);
                  dmRes.frxReport1.Export(dmRes.frxPDFExport1);

                  dm.tblOdbieniPdf.Next;
                except
                 ShowMessage('�� ���� �� �� ������� ���������!');
                end;
             end
         finally
            ShowMessage('������������� �� ��������� �� �������� !');
         end;
       end  else ShowMessage('���� ��������� �� �����������!');
end;

procedure TfrmPonudi.actPIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40176);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    if dm.tblPonudiODBIENA.Value = 1 then
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('e �������� ���� ������������'))
    else
       dmRes.frxReport1.Variables.AddVariable('VAR', 'Otfrlena', QuotedStr('�� � ��������� �� ���������'));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmPonudi.aDecatiCeniPredIPotoaExecute(Sender: TObject);
begin
       dmRes.Spremi('JN',40016);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnListaPonudiSoBodPoGrupaArtikalExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40013);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnListaSitePonudExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40010);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnListaSitePonudiSoBodExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40012);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnListaSitePonudiSoOpisNaArtikalExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40011);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnPonudaOdPartnerExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40009);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDizajnPonudaOdPartnerSoOpisNaArtikalExecute(
  Sender: TObject);
begin
   dmRes.Spremi('JN',40008);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDIzvesstajPoGrupiIPartneriExecute(Sender: TObject);
begin
       dmRes.Spremi('JN',40039);
       dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDIzvestuvanjeZaOtfrlenaPonudaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40028);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmPonudi.aDodadiStavkaExecute(Sender: TObject);
begin
if dm.tblTenderPONISTEN.Value = 1 then
begin
     if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (PanelGrupnoBodiranje.Visible = false) then
       begin
          if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
             begin
                if (dm.tblPonudiodbiena.Value = 0)  then
                    begin
                       dmPQ.qCountKriteriumiBodTender.Close;
                       dmPQ.qCountKriteriumiBodTender.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                       dmPQ.qCountKriteriumiBodTender.ExecQuery;
                       if dmPQ.qCountKriteriumiBodTender.FldByName['br'].Value > 0 then
                          begin
                             frmIzborStavkiPonuda:=TfrmIzborStavkiPonuda.Create(Self, false);
                             frmIzborStavkiPonuda.ShowModal;
                             frmIzborStavkiPonuda.Free;
                             cxGrid1.SetFocus;
                          end
                       else ShowMessage('������ ������������ ���������� �� �������� !!!');
                    end
                else ShowMessage('��������� ������ � ������� !!!');
             end
          else ShowMessage('������ ������������ ������ !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end
else ShowMessage('���������� � ���������. �� � ��������� �������� �� ������ !');
end;

procedure TfrmPonudi.aDostaveniDokumentiExecute(Sender: TObject);
begin
     frmPonuduvacDostaveniDokumenti:=TfrmPonuduvacDostaveniDokumenti.Create(Application);
     frmPonuduvacDostaveniDokumenti.ShowModal;
     frmPonuduvacDostaveniDokumenti.Free;
     end;

procedure TfrmPonudi.aDPocetnaRangListaZaUcestvoNaEAukcijaExecute(
  Sender: TObject);
begin
       dmRes.Spremi('JN',40032);
       dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmPonudi.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmPonudi.aGrupnoBodiraweExecute(Sender: TObject);
begin
     PanelGrupnoBodiranje.Visible:=True;
     dm.tblGrupnoBodiranjePonudiStavki.Open;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmPonudi.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmPonudi.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
