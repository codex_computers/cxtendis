inherited frmPotrebniDokGrupi: TfrmPotrebniDokGrupi
  Caption = #1055#1086#1090#1088#1077#1073#1085#1080' '#1076#1086#1082#1091#1084#1077#1085#1085#1090#1080'  - '#1043#1088#1091#1087#1080
  ClientHeight = 644
  ClientWidth = 859
  ExplicitWidth = 875
  ExplicitHeight = 682
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 859
    Height = 242
    ExplicitWidth = 859
    ExplicitHeight = 242
    inherited cxGrid1: TcxGrid
      Width = 855
      Height = 238
      ExplicitWidth = 855
      ExplicitHeight = 238
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsSifPotrebniDokGrupi
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 51
        end
        object cxGrid1DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
          Width = 71
        end
        object cxGrid1DBTableView1tipNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'tipNaziv'
          Width = 264
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 550
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 163
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 210
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 204
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 241
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 368
    Width = 859
    Height = 253
    ExplicitTop = 368
    ExplicitWidth = 859
    ExplicitHeight = 253
    inherited Label1: TLabel
      Left = 653
      Top = 38
      Visible = False
      ExplicitLeft = 653
      ExplicitTop = 38
    end
    object Label2: TLabel [1]
      Left = 29
      Top = 173
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 709
      Top = 35
      TabStop = False
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsSifPotrebniDokGrupi
      TabOrder = 2
      Visible = False
      ExplicitLeft = 709
      ExplicitTop = 35
    end
    inherited OtkaziButton: TcxButton
      Left = 768
      Top = 213
      TabOrder = 4
      ExplicitLeft = 768
      ExplicitTop = 213
    end
    inherited ZapisiButton: TcxButton
      Left = 687
      Top = 213
      TabOrder = 3
      ExplicitLeft = 687
      ExplicitTop = 213
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 85
      Top = 170
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1075#1088#1091#1087#1072#1090#1072
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsSifPotrebniDokGrupi
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 596
    end
    object cxDBRadioGroupTipDokument: TcxDBRadioGroup
      Left = 85
      Top = 17
      TabStop = False
      Caption = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      DataBinding.DataField = 'TIP'
      DataBinding.DataSource = dm.dsSifPotrebniDokGrupi
      Properties.DefaultValue = 1
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          Value = 1
        end
        item
          Caption = #1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090
          Value = 6
        end
        item
          Caption = #1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          Value = 2
        end
        item
          Caption = #1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '
          Value = 3
        end
        item
          Caption = #1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077#1090
          Value = 4
        end
        item
          Caption = #1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072
          Value = 5
        end>
      TabOrder = 0
      Height = 136
      Width = 292
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 859
    ExplicitWidth = 859
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 621
    Width = 859
    ExplicitTop = 621
    ExplicitWidth = 859
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 200
  end
  inherited PopupMenu1: TPopupMenu
    Top = 272
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 296
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Top = 200
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41253.369293923610000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 472
  end
end
