object frmStavkiVoJavnaNabavka: TfrmStavkiVoJavnaNabavka
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 749
  ClientWidth = 1152
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1152
    749)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1152
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    ExplicitWidth = 1144
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 726
    Width = 1152
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 718
    ExplicitWidth = 1144
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1152
    Height = 367
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    ExplicitWidth = 1144
    ExplicitHeight = 359
    object Panel3: TPanel
      Left = 2
      Top = 2
      Width = 504
      Height = 363
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 496
      ExplicitHeight = 355
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 502
        Height = 41
        Align = alTop
        Color = clCream
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 494
      end
      object Panel9: TPanel
        Left = 1
        Top = 341
        Width = 502
        Height = 21
        Align = alBottom
        Alignment = taLeftJustify
        Caption = ' F8 - '#1041#1088#1080#1096#1080
        TabOrder = 1
        ExplicitTop = 333
        ExplicitWidth = 494
      end
      object cxGrid1: TcxGrid
        Left = 1
        Top = 42
        Width = 502
        Height = 299
        Align = alClient
        TabOrder = 2
        ExplicitWidth = 494
        ExplicitHeight = 291
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsTenderStavki
          DataController.KeyFieldNames = 'VID_ARTIKAL;ARTIKAL'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
          OptionsView.ExpandButtonsForEmptyDetails = False
          Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
          object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Width = 105
          end
          object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'VID_ARTIKAL'
            Visible = False
            Width = 79
          end
          object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'JN_TIP_ARTIKAL'
            Width = 22
          end
          object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'VIDNAZIV'
            Width = 66
          end
          object cxGrid1DBTableView1GENERIKA: TcxGridDBColumn
            DataBinding.FieldName = 'GENERIKA'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                ImageIndex = 0
                Value = 1
              end>
            Width = 69
          end
          object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'ARTIKAL'
            Width = 55
          end
          object cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIVARTIKAL'
            Width = 201
          end
          object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            PropertiesClassName = 'TcxRichEditProperties'
            Properties.AllowObjects = True
            Properties.ClearKey = 46
            Properties.ImeMode = imAlpha
            Properties.MemoMode = True
            Properties.OEMConvert = True
            Properties.StreamModes = [resmSelection, resmPlainRtf, resmRtfNoObjs, resmUnicode, resmTextIzed]
            Properties.WantReturns = False
            Width = 201
          end
          object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'KOLICINA'
            Width = 56
          end
          object cxGrid1DBTableView1MERKA: TcxGridDBColumn
            DataBinding.FieldName = 'MERKA'
            Width = 40
          end
          object cxGrid1DBTableView1MERKA1: TcxGridDBColumn
            DataBinding.FieldName = 'MERKA1'
            Width = 75
          end
          object cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MERKANAZIV'
            Width = 77
          end
          object cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn
            DataBinding.FieldName = 'PROZVODITEL'
            Width = 84
          end
          object cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PROIZVODITELNAZIV'
            Width = 124
          end
          object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Width = 124
          end
          object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV'
            Width = 124
          end
          object cxGrid1DBTableView1JN_CPV: TcxGridDBColumn
            DataBinding.FieldName = 'JN_CPV'
            Width = 124
          end
          object cxGrid1DBTableView1MK: TcxGridDBColumn
            DataBinding.FieldName = 'MK'
            Width = 124
          end
          object cxGrid1DBTableView1EN: TcxGridDBColumn
            DataBinding.FieldName = 'EN'
            Width = 124
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 124
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 124
          end
          object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 124
          end
          object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 124
          end
          object cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn
            DataBinding.FieldName = 'VALIDNOST'
            Visible = False
          end
        end
        object cxGrid1DBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsTenderStavkiRE
          DataController.DetailKeyFieldNames = 'VID_ARTIKAL;ARTIKAL'
          DataController.MasterKeyFieldNames = 'VID_ARTIKAL;ARTIKAL'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView2RE: TcxGridDBColumn
            DataBinding.FieldName = 'RE'
            Width = 73
          end
          object cxGrid1DBTableView2NAZIVRE: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIVRE'
            Width = 363
          end
          object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid1DBTableView2BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Width = 64
          end
          object cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'VID_ARTIKAL'
            Visible = False
          end
          object cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn
            Caption = ' '
            DataBinding.FieldName = 'ARTIKAL'
            Visible = False
          end
          object cxGrid1DBTableView2Column1: TcxGridDBColumn
            Caption = #1062#1077#1085#1072' '#1073#1077#1079' '#1076#1076#1074
            DataBinding.FieldName = 'CENABEZDDV'
          end
          object cxGrid1DBTableView2Column2: TcxGridDBColumn
            Caption = #1044#1044#1042
            DataBinding.FieldName = 'DANOK'
          end
          object cxGrid1DBTableView2Column3: TcxGridDBColumn
            Caption = #1062#1077#1085#1072
            DataBinding.FieldName = 'CENA'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
          object cxGrid1Level2: TcxGridLevel
            GridView = cxGrid1DBTableView2
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 514
      Top = 2
      Width = 636
      Height = 363
      Align = alRight
      TabOrder = 1
      ExplicitLeft = 506
      ExplicitHeight = 355
      object cxGrid2: TcxGrid
        Left = 1
        Top = 42
        Width = 634
        Height = 299
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 291
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid2DBTableView1KeyDown
          OnKeyPress = cxGrid2DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsIzborStavkiTenderOdGodPlan
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
          object cxGrid2DBTableView1GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'GODINA'
            Visible = False
            Options.Editing = False
            Width = 100
          end
          object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'JN_TIP_ARTIKAL'
            Options.Editing = False
            Width = 27
          end
          object cxGrid2DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'VIDARTIKALNAZIV'
            Options.Editing = False
            Width = 59
          end
          object cxGrid2DBTableView1GENERIKA: TcxGridDBColumn
            DataBinding.FieldName = 'GENERIKA'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                ImageIndex = 0
                Value = 1
              end>
            Options.Editing = False
            Width = 67
          end
          object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'ARTIKAL'
            Options.Editing = False
            Width = 49
          end
          object cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ARTIKALNAZIV'
            Options.Editing = False
            Width = 285
          end
          object cxGrid2DBTableView1MERKA: TcxGridDBColumn
            DataBinding.FieldName = 'MERKA'
            Options.Editing = False
            Width = 41
          end
          object cxGrid2DBTableView1SUMAKOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'SUMAKOLICINA'
            Options.Editing = False
            Width = 56
          end
          object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Options.Editing = False
            Width = 38
          end
          object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV'
            Options.Editing = False
            Width = 74
          end
          object cxGrid2DBTableView1JN_CPV: TcxGridDBColumn
            DataBinding.FieldName = 'JN_CPV'
            Options.Editing = False
            Width = 112
          end
          object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'VID_ARTIKAL'
            Visible = False
            Options.Editing = False
            Width = 28
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 634
        Height = 41
        Align = alTop
        Color = clCream
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
      end
      object cxGrid5: TcxGrid
        Left = 1
        Top = 42
        Width = 634
        Height = 299
        Align = alClient
        TabOrder = 2
        Visible = False
        ExplicitHeight = 291
        object cxGrid5DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid5DBTableView1KeyDown
          OnKeyPress = cxGrid5DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsIzborStavkiTenderOdSifArtikli
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1074#1086' '#1096#1080#1092#1088#1072#1088#1085#1080#1082#1086#1090' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1080
          object cxGrid5DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'JN_TIP_ARTIKAL'
            Options.Editing = False
            Width = 28
          end
          object cxGrid5DBTableView1VIDNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'VIDNAZIV'
            Options.Editing = False
            Width = 67
          end
          object cxGrid5DBTableView1GENERIKA: TcxGridDBColumn
            DataBinding.FieldName = 'GENERIKA'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                ImageIndex = 0
                Value = 1
              end>
            Options.Editing = False
            Width = 70
          end
          object cxGrid5DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Options.Editing = False
            Width = 56
          end
          object cxGrid5DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Options.Editing = False
            Width = 263
          end
          object cxGrid5DBTableView1MERKA: TcxGridDBColumn
            DataBinding.FieldName = 'MERKA'
            Options.Editing = False
            Width = 38
          end
          object cxGrid5DBTableView1MERKANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MERKANAZIV'
            Options.Editing = False
            Width = 82
          end
          object cxGrid5DBTableView1KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'KOLICINA'
            Styles.Content = dmRes.cxStyle120
            Width = 59
          end
          object cxGrid5DBTableView1CENA: TcxGridDBColumn
            DataBinding.FieldName = 'CENA'
            Options.Editing = False
            Width = 50
          end
          object cxGrid5DBTableView1TARIFA: TcxGridDBColumn
            DataBinding.FieldName = 'TARIFA'
            Options.Editing = False
            Width = 30
          end
          object cxGrid5DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Visible = False
            Options.Editing = False
            Width = 40
          end
          object cxGrid5DBTableView1GRUPANZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANZIV'
            Options.Editing = False
            Width = 78
          end
          object cxGrid5DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'PROIZVODITELNAZIV'
            Options.Editing = False
            Width = 100
          end
          object cxGrid5DBTableView1PROZVODITEL: TcxGridDBColumn
            DataBinding.FieldName = 'PROZVODITEL'
            Visible = False
            Options.Editing = False
            Width = 76
          end
          object cxGrid5DBTableView1JN_CPV: TcxGridDBColumn
            DataBinding.FieldName = 'JN_CPV'
            Options.Editing = False
            Width = 117
          end
          object cxGrid5DBTableView1MK: TcxGridDBColumn
            DataBinding.FieldName = 'MK'
            Options.Editing = False
            Width = 103
          end
          object cxGrid5DBTableView1EN: TcxGridDBColumn
            DataBinding.FieldName = 'EN'
            Options.Editing = False
            Width = 86
          end
          object cxGrid5DBTableView1AKTIVEN: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Visible = False
            Options.Editing = False
            Width = 100
          end
          object cxGrid5DBTableView1ARTVID: TcxGridDBColumn
            DataBinding.FieldName = 'ARTVID'
            Visible = False
            Options.Editing = False
          end
        end
        object cxGrid5Level1: TcxGridLevel
          GridView = cxGrid5DBTableView1
        end
      end
      object Panel10: TPanel
        Left = 1
        Top = 341
        Width = 634
        Height = 21
        Align = alBottom
        Alignment = taLeftJustify
        Caption = ' Insert - '#1048#1079#1073#1086#1088' '#1085#1072' '#1089#1090#1072#1074#1082#1072
        TabOrder = 3
        ExplicitTop = 333
      end
    end
    object cxSplitter2: TcxSplitter
      Left = 506
      Top = 2
      Width = 8
      Height = 363
      AlignSplitter = salRight
      Control = Panel4
      ExplicitLeft = 498
      ExplicitHeight = 355
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 501
    Width = 1152
    Height = 225
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 3
    ExplicitTop = 493
    ExplicitWidth = 1144
    object Panel5: TPanel
      Left = 2
      Top = 43
      Width = 1148
      Height = 159
      Align = alClient
      TabOrder = 0
      ExplicitWidth = 1140
      object cxGrid3: TcxGrid
        Left = 1
        Top = 1
        Width = 1146
        Height = 157
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1138
        object cxGrid3DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid3DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsNedeliviGrupi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1085#1077#1076#1077#1083#1080#1074#1080' '#1075#1088#1091#1087#1080
          object cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Width = 88
          end
          object cxGrid3DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Width = 66
          end
          object cxGrid3DBTableView1GRUPANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV'
            Width = 924
          end
          object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 162
          end
          object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 215
          end
          object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 204
          end
          object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 237
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 2
      Width = 1148
      Height = 41
      Align = alTop
      Alignment = taLeftJustify
      Caption = '      '#1053#1077#1076#1077#1083#1080#1074#1080' '#1075#1088#1091#1087#1080
      Color = clCream
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      ExplicitWidth = 1140
    end
    object Panel11: TPanel
      Left = 2
      Top = 202
      Width = 1148
      Height = 21
      Align = alBottom
      Alignment = taLeftJustify
      Caption = ' Ctrl+F8 - '#1041#1088#1080#1096#1080
      TabOrder = 2
      ExplicitWidth = 1140
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 493
    Width = 1152
    Height = 8
    AlignSplitter = salBottom
    MinSize = 250
    Control = Panel2
    ExplicitTop = 485
    ExplicitWidth = 1144
  end
  object PanelNedeliviGrupi: TPanel
    Left = 243
    Top = 279
    Width = 792
    Height = 317
    Anchors = []
    Color = 11302478
    ParentBackground = False
    TabOrder = 5
    Visible = False
    ExplicitLeft = 239
    ExplicitTop = 274
    DesignSize = (
      792
      317)
    object cxGroupBoxNedelivaGrupa: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1048#1079#1073#1077#1088#1080' '#1085#1077#1076#1077#1083#1080#1074#1072' '#1075#1088#1091#1087#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        760
        261)
      Height = 261
      Width = 760
      object cxGrid6: TcxGrid
        Left = 16
        Top = 26
        Width = 728
        Height = 219
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid6DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnEditKeyDown = cxGrid6DBTableView1EditKeyDown
          DataController.DataSource = dm.dsIzborNedeliviGrupi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080' '#1089#1086' '#1076#1077#1092#1080#1085#1080#1088#1072#1085#1072' '#1075#1088#1091#1087#1072
          OptionsView.GroupByBox = False
          object cxGrid6DBTableView1ColumnOdberi: TcxGridDBColumn
            Caption = #1054#1076#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
          end
          object cxGrid6DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Options.Editing = False
            Width = 70
          end
          object cxGrid6DBTableView1GRUPANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV'
            Options.Editing = False
            Width = 583
          end
        end
        object cxGrid6Level1: TcxGridLevel
          GridView = cxGrid6DBTableView1
        end
      end
    end
    object ButtonZapisi2: TcxButton
      Left = 32
      Top = 283
      Width = 75
      Height = 25
      Action = aOkNedeliviGrupi
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
    end
    object ButtonOtkazi2: TcxButton
      Left = 113
      Top = 283
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
    end
  end
  object PanelBrisiGrupaStavki: TPanel
    Left = 3
    Top = 459
    Width = 792
    Height = 317
    Anchors = []
    Color = 11302478
    ParentBackground = False
    TabOrder = 7
    Visible = False
    ExplicitLeft = 0
    ExplicitTop = 452
    DesignSize = (
      792
      317)
    object cxGroupBox2: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1048#1079#1073#1077#1088#1080' '#1075#1088#1091#1087#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        760
        261)
      Height = 261
      Width = 760
      object cxGrid7: TcxGrid
        Left = 16
        Top = 26
        Width = 728
        Height = 219
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid7DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsGrupaStavkiBrisi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid7DBTableView1Odberi: TcxGridDBColumn
            Caption = #1054#1076#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
          end
          object cxGrid7DBTableView1GRUPA: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPA'
            Width = 88
          end
          object cxGrid7DBTableView1GRUPANAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'GRUPANAZIV'
            Width = 638
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGrid7DBTableView1
        end
      end
    end
    object cxButtonBrsiStavkiGrupaOK: TcxButton
      Left = 32
      Top = 283
      Width = 153
      Height = 25
      Action = aOKBrisiStavkiGrupa
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
    end
    object cxButtonBrsiStavkiGrupaOtkazi: TcxButton
      Left = 191
      Top = 283
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akLeft, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
    end
  end
  object PanelInsertStavki: TPanel
    Left = 267
    Top = 444
    Width = 808
    Height = 377
    Anchors = []
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 6
    Visible = False
    ExplicitLeft = 262
    ExplicitTop = 437
    DesignSize = (
      808
      377)
    object Label2: TLabel
      Left = 16
      Top = 352
      Width = 188
      Height = 13
      Caption = 'Insert - '#1042#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080', Esc - '#1054#1090#1082#1072#1078#1080
      Color = 11302478
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        776
        330)
      Height = 330
      Width = 776
      object cxLabel2: TcxLabel
        Left = 241
        Top = 56
        Caption = #1054#1073#1112#1072#1074#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Transparent = True
      end
      object ZBIRNA: TcxTextEdit
        Left = 120
        Top = 53
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Width = 98
      end
      object OBJAVENA: TcxTextEdit
        Left = 356
        Top = 53
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 98
      end
      object cxGrid4: TcxGrid
        Left = 16
        Top = 80
        Width = 744
        Height = 236
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object cxGrid4DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid4DBTableView1KeyDown
          OnKeyPress = cxGrid4DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsVnesStavkiTenderOdGodPlan
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
          OptionsView.GroupByBox = False
          object cxGrid4DBTableView1RE: TcxGridDBColumn
            DataBinding.FieldName = 'RE'
            Options.Editing = False
            Width = 52
          end
          object cxGrid4DBTableView1RENAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'RENAZIV'
            Options.Editing = False
            Width = 381
          end
          object cxGrid4DBTableView1PLANIRANAKOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'PLANIRANAKOLICINA'
            Options.Editing = False
            Width = 118
          end
          object cxGrid4DBTableView1OBJAVENA: TcxGridDBColumn
            DataBinding.FieldName = 'OBJAVENA'
            Options.Editing = False
            Width = 105
          end
          object cxGrid4DBTableView1KOLICINA: TcxGridDBColumn
            DataBinding.FieldName = 'KOLICINA'
            Styles.Content = dmRes.cxStyle120
            Width = 74
          end
        end
        object cxGrid4Level1: TcxGridLevel
          GridView = cxGrid4DBTableView1
        end
      end
      object cxLabel1: TcxLabel
        Left = 16
        Top = 56
        Caption = #1047#1073#1080#1088#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Transparent = True
      end
      object ARTIKAL: TcxTextEdit
        Left = 154
        Top = 24
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 5
        Width = 64
      end
      object NAZIV_ARTIKAL: TcxTextEdit
        Left = 218
        Top = 24
        Anchors = [akLeft, akTop, akRight, akBottom]
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Width = 535
      end
      object cxLabel4: TcxLabel
        Left = 60
        Top = 27
        Caption = #1040#1088#1090#1080#1082#1072#1083' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Transparent = True
      end
      object VID: TcxTextEdit
        Left = 120
        Top = 24
        ParentFont = False
        Properties.ReadOnly = True
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        Width = 34
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 200
    Top = 152
  end
  object PopupMenu1: TPopupMenu
    Left = 616
    Top = 64
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 752
    Top = 32
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 372
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 617
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1053#1077#1076#1077#1083#1080#1074#1080' '#1075#1088#1091#1087#1080
      CaptionButtons = <>
      DockedLeft = 252
      DockedTop = 0
      FloatLeft = 1048
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiNedelivaGrupa
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiNedelivaGrupa
      Category = 0
      ShortCut = 16503
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiStavkiGrupa
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aAzurirajNazivMerka
      Category = 0
    end
    object btnCeni: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 440
    Top = 8
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSifArtikli: TAction
      Caption = 'aSifArtikli'
      ShortCut = 16449
      OnExecute = aSifArtikliExecute
    end
    object aDodadiNedelivaGrupa: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 90
      OnExecute = aDodadiNedelivaGrupaExecute
    end
    object aBrisiNedelivaGrupa: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 91
      OnExecute = aBrisiNedelivaGrupaExecute
    end
    object aOkNedeliviGrupi: TAction
      Caption = #1054#1050
      ImageIndex = 6
      OnExecute = aOkNedeliviGrupiExecute
    end
    object aKopirajStavkiOdPlan: TAction
      Caption = 'aKopirajStavkiOdPlan'
      ShortCut = 16464
      OnExecute = aKopirajStavkiOdPlanExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aBrisiStavkiGrupa: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1080' '#1087#1086' '#1075#1088#1091#1087#1072
      ImageIndex = 46
      OnExecute = aBrisiStavkiGrupaExecute
    end
    object aOKBrisiStavkiGrupa: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1080' '#1087#1086' '#1075#1088#1091#1087#1072
      ImageIndex = 46
      OnExecute = aOKBrisiStavkiGrupaExecute
    end
    object aAzurirajNazivMerka: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1072#1079#1080#1074' '#1080' '#1084#1077#1088#1082#1072
      ImageIndex = 12
      OnExecute = aAzurirajNazivMerkaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 656
    Top = 32
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 14000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41043.896887210650000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 616
    Top = 65528
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object qObjavenaKolicina: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(sum(ts.kolicina),0)as objavena'
      'from jn_tender_stavki ts'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'where ts.vid_artikal = :vid_artikal and ts.artikal = :artikal an' +
        'd t.godina = :godina and ts.validnost = 1')
    Left = 672
    Top = 216
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 808
    Top = 240
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 464
    Top = 264
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 232
    Top = 264
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 544
    Top = 280
  end
  object cxGridPopupMenu5: TcxGridPopupMenu
    Grid = cxGrid5
    PopupMenus = <>
    Left = 448
    Top = 344
  end
  object qOstanataKolicinaZaArtikal: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select re, (planiranakolicina - objavena) as kolicina'
      'from (select  gps.re,'
      '        coalesce(sum(gps.kolicina),0) as planiranakolicina,'
      '        (select coalesce(sum(tsr.kolicina),0) as objavena'
      '         from jn_tender_stavki_re tsr'
      
        '         inner join jn_tender_stavki ts on ts.broj_tender = tsr.' +
        'broj_tender and ts.vid_artikal = tsr.vid_artikal and ts.artikal ' +
        '= tsr.artikal'
      '         inner join jn_tender t on t.broj = ts.broj_tender'
      
        '         where tsr.artikal = :artikal and tsr.vid_artikal = :vid' +
        '_artikal and tsr.re = gps.re and t.godina = :godina)'
      'from jn_godisen_plan_sektori gps'
      
        'where gps.godina = :godina and gps.artikal = :artikal and gps.vi' +
        'd_artikal = :vid_artikal'
      'group by gps.re)'
      ''
      '')
    Left = 856
    Top = 32
  end
  object qIznosPoPlan: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select sum(ts.kolicina* gs.cena) as iznosSoDDV,'
      '       sum(ts.kolicina* gs.cenabezddv) as iznosBezDDV'
      'from jn_tender_stavki_re ts'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'inner join jn_godisen_plan_sektori gs on gs.vid_artikal = ts.vid' +
        '_artikal and'
      
        '                                         gs.artikal = ts.artikal' +
        ' and'
      
        '                                         gs.godina = t.godina an' +
        'd'
      '                                         gs.re =ts.re'
      'where t.broj = :broj_tender')
    Left = 64
    Top = 264
  end
  object qUpdateStatus: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select sum(ts.kolicina* gs.cena) as iznosSoDDV,'
      '       sum(ts.kolicina* gs.cenabezddv) as iznosBezDDV'
      'from jn_tender_stavki_re ts'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'inner join jn_godisen_plan_sektori gs on gs.vid_artikal = ts.vid' +
        '_artikal and'
      
        '                                         gs.artikal = ts.artikal' +
        ' and'
      
        '                                         gs.godina = t.godina an' +
        'd'
      '                                         gs.re =ts.re'
      'where t.broj = :broj_tender')
    Left = 72
    Top = 312
  end
end
