﻿object dm: Tdm
  OldCreateOrder = False
  Height = 826
  Width = 2833
  object tblGodPlanOdluka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_GOD_PLAN_ODLUKA'
      'SET '
      '    BROJ = :BROJ,'
      '    GODINA = :GODINA,'
      '    PREDMET = :PREDMET,'
      '    OPIS = :OPIS,'
      '    DATUM = :DATUM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_GOD_PLAN_ODLUKA'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    and GODINA = :OLD_GODINA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_GOD_PLAN_ODLUKA('
      '    BROJ,'
      '    GODINA,'
      '    PREDMET,'
      '    OPIS,'
      '    DATUM,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :GODINA,'
      '    :PREDMET,'
      '    :OPIS,'
      '    :DATUM,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select gpo.broj,'
      '       gpo.godina,'
      '       gpo.predmet,'
      '       gpo.opis,'
      '       gpo.datum,'
      '       gpo.ts_ins,'
      '       gpo.ts_upd,'
      '       gpo.usr_ins,'
      '       gpo.usr_upd'
      'from jn_god_plan_odluka gpo'
      'where(  gpo.godina like :godina'
      '     ) and (     GPO.BROJ = :OLD_BROJ'
      '    and GPO.GODINA = :OLD_GODINA'
      '     )'
      '    '
      'order by gpo.broj')
    SelectSQL.Strings = (
      'select gpo.broj,'
      '       gpo.godina,'
      '       gpo.predmet,'
      '       gpo.opis,'
      '       gpo.datum,'
      '       gpo.ts_ins,'
      '       gpo.ts_upd,'
      '       gpo.usr_ins,'
      '       gpo.usr_upd'
      'from jn_god_plan_odluka gpo'
      'where gpo.godina like :godina'
      'order by gpo.broj')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 304
    Top = 40
    object tblGodPlanOdlukaBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072
      FieldName = 'BROJ'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanOdlukaGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblGodPlanOdlukaPREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090'/'#1086#1087#1080#1089' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanOdlukaOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'OPIS'
      Size = 8
    end
    object tblGodPlanOdlukaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblGodPlanOdlukaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblGodPlanOdlukaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblGodPlanOdlukaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanOdlukaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsGodPlanOdlulka: TDataSource
    DataSet = tblGodPlanOdluka
    Left = 456
    Top = 40
  end
  object tblPodsektoriGodisenPlan: TpFIBDataSet
    SelectSQL.Strings = (
      'select mr.id,'
      '       mr.naziv,'
      '       mr.tip_partner,'
      '       mr.partner,'
      '       mr.koren,'
      '       mr.spisok,'
      '       mr.re,'
      '       mr.poteklo,'
      '       mr.rakovoditel,'
      '       mr.r,'
      '       mr.m,'
      '       mr.t,'
      '       mr.pov,'
      '       mr.prod'
      'from mat_re mr'
      'where mr.r = 1 and mr.spisok like :roditel || '#39'%'#39
      
        '      and ((mr.id in (select s.re from sys_user_re_app s where s' +
        '.username = :username and s.app = :app and :param = 1)'
      '          ) or (:param = 0))'
      'order by mr.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 304
    Top = 240
    object tblPodsektoriGodisenPlanID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPodsektoriGodisenPlanNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriGodisenPlanTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_PARTNER'
    end
    object tblPodsektoriGodisenPlanPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PARTNER'
    end
    object tblPodsektoriGodisenPlanKOREN: TFIBIntegerField
      DisplayLabel = #1050#1086#1088#1077#1085
      FieldName = 'KOREN'
    end
    object tblPodsektoriGodisenPlanSPISOK: TFIBStringField
      DisplayLabel = #1057#1087#1080#1089#1086#1082
      FieldName = 'SPISOK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriGodisenPlanRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072'/'#1057#1077#1082#1090#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RE'
    end
    object tblPodsektoriGodisenPlanPOTEKLO: TFIBStringField
      DisplayLabel = #1055#1086#1090#1077#1082#1083#1086
      FieldName = 'POTEKLO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPodsektoriGodisenPlanRAKOVODITEL: TFIBStringField
      DisplayLabel = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'RAKOVODITEL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPodsektoriGodisenPlan: TDataSource
    DataSet = tblPodsektoriGodisenPlan
    Left = 456
    Top = 240
  end
  object tblGodPlanSektori: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_GODISEN_PLAN_SEKTORI'
      'SET '
      '    BROJ = :BROJ,'
      '    GODINA = :GODINA,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    RE = :RE,'
      '    KOLICINA = :KOLICINA,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    CENA = :CENA,'
      '    CENABEZDDV = :CENABEZDDV,'
      '    DANOK = :DANOK,'
      '    TIP = :TIP,'
      '    MESEC = :MESEC,'
      '    TIP_NABAVKA = :TIP_NABAVKA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_GODISEN_PLAN_SEKTORI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_GODISEN_PLAN_SEKTORI('
      '    ID,'
      '    BROJ,'
      '    GODINA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    RE,'
      '    KOLICINA,'
      '    BROJ_TENDER,'
      '    CENA,'
      '    CENABEZDDV,'
      '    DANOK,'
      '    TIP,'
      '    MESEC,'
      '    TIP_NABAVKA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :GODINA,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :RE,'
      '    :KOLICINA,'
      '    :BROJ_TENDER,'
      '    :CENA,'
      '    :CENABEZDDV,'
      '    :DANOK,'
      '    :TIP,'
      '    :MESEC,'
      '    :TIP_NABAVKA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      '    select  gps.id,'
      '        gps.broj,'
      '        gps.godina,'
      '        gps.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        mav.naziv as vidArtikalNaziv,'
      '        gps.artikal,'
      '        ma.naziv as artikalNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mag.naziv as grupaNaziv,'
      '        ma.jn_cpv,'
      '        mc.mk,'
      '        mc.en,'
      '        ma.generika,'
      '        gps.re,'
      '        gps.kolicina,'
      '        gps.broj_tender,'
      '        gps.cena,'
      '        gps.cenabezddv,'
      '        gps.danok,'
      '        gps.tip,'
      '        gps.cena * gps.kolicina as iznosCena,'
      '        gps.cenabezddv * gps.kolicina as iznosCenBezDDV,'
      '        gps.mesec,'
      '        gps.PREDMET_NABAVKA,'
      '        gps.tip_nabavka,'
      '        tn.naziv as tipNabavkaNaziv,'
      '        gps.ts_ins,'
      '        gps.ts_upd,'
      '        gps.usr_ins,'
      '        gps.usr_upd'
      'from jn_godisen_plan_sektori gps'
      
        'inner join mtr_artikal ma on ma.id = gps.artikal and ma.artvid =' +
        ' gps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'left outer join jn_cpv mc on mc.code = ma.jn_cpv'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      'left outer join jn_tip_nabavka tn on tn.sifra = gps.tip_nabavka'
      
        'where(  gps.godina = :mas_godina and gps.broj = :mas_broj  and g' +
        'ps.re like :re'
      '     ) and (     GPS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      '    select  gps.id,'
      '        gps.broj,'
      '        gps.godina,'
      '        gps.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        mav.naziv as vidArtikalNaziv,'
      '        gps.artikal,'
      '        ma.naziv as artikalNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mag.naziv as grupaNaziv,'
      '        ma.jn_cpv,'
      '        mc.mk,'
      '        mc.en,'
      '        ma.generika,'
      '        gps.re,'
      '        gps.kolicina,'
      '        gps.broj_tender,'
      '        gps.cena,'
      '        gps.cenabezddv,'
      '        gps.danok,'
      '        gps.tip,'
      '        gps.cena * gps.kolicina as iznosCena,'
      '        gps.cenabezddv * gps.kolicina as iznosCenBezDDV,'
      '        gps.mesec,  '
      '        gps.PREDMET_NABAVKA,'
      '        gps.tip_nabavka,'
      '        tn.naziv as tipNabavkaNaziv,'
      '        gps.ts_ins,'
      '        gps.ts_upd,'
      '        gps.usr_ins,'
      '        gps.usr_upd'
      'from jn_godisen_plan_sektori gps'
      
        'inner join mtr_artikal ma on ma.id = gps.artikal and ma.artvid =' +
        ' gps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'left outer join jn_cpv mc on mc.code = ma.jn_cpv'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      'left outer join jn_tip_nabavka tn on tn.sifra = gps.tip_nabavka'
      
        'where gps.godina = :mas_godina and gps.broj = :mas_broj  and gps' +
        '.re like :re'
      'order by  ma.jn_tip_artikal, ma.naziv'
      ''
      '')
    AutoUpdateOptions.UpdateTableName = 'JN_GODISEN_PLAN_SEKTORI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_GODISEN_PLAN_SEKTORI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsGodPlanOdlulka
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 304
    Top = 104
    object tblGodPlanSektoriID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblGodPlanSektoriBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      FieldName = 'BROJ'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblGodPlanSektoriVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblGodPlanSektoriVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblGodPlanSektoriARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' '
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblGodPlanSektoriPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblGodPlanSektoriRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblGodPlanSektoriKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      OnSetText = tblGodPlanSektoriKOLICINASetText
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblGodPlanSektoriBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblGodPlanSektoriCENASetText
      DisplayFormat = '0.0000 , .'
    end
    object tblGodPlanSektoriCENABEZDDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENABEZDDV'
      OnSetText = tblGodPlanSektoriCENABEZDDVSetText
      DisplayFormat = '0.0000, .'
    end
    object tblGodPlanSektoriDANOK: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DANOK'
      OnSetText = tblGodPlanSektoriDANOKSetText
      DisplayFormat = '0.00 , .'
    end
    object tblGodPlanSektoriTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP'
    end
    object tblGodPlanSektoriTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblGodPlanSektoriTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblGodPlanSektoriUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriIZNOSCENA: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOSCENA'
      DisplayFormat = '0.0000, .'
    end
    object tblGodPlanSektoriJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblGodPlanSektoriIZNOSCENBEZDDV: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOSCENBEZDDV'
      DisplayFormat = '0.0000, .'
    end
    object tblGodPlanSektoriMESEC: TFIBStringField
      DisplayLabel = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'MESEC'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriTIP_NABAVKA: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP_NABAVKA'
    end
    object tblGodPlanSektoriTIPNABAVKANAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'TIPNABAVKANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGodPlanSektoriPREDMET_NABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET_NABAVKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsGodPlanSektori: TDataSource
    DataSet = tblGodPlanSektori
    Left = 456
    Top = 104
  end
  object tblIzborStavkiGodPlanSektori: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    KOLICINA = :KOLICINA,'
      '    CENA = :CENA,'
      '    TARIFA = :TARIFA'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      
        '-- Samo update ima i da ne se prave avtomatsko generiranje na up' +
        'date'
      'select ma.jn_tip_artikal,'
      '       ma.artvid,'
      '       mv.naziv as vidNaziv,'
      '       ma.id,'
      '       ma.naziv,'
      '       ma.generika,'
      '       ma.merka,'
      
        '/*$$IBEC$$        coalesce((select first 1 (ps.cenabezddv) as ce' +
        'na'
      '        from jn_godisen_plan_sektori ps'
      
        '        inner join jn_god_plan_odluka p on p.broj=ps.broj and  p' +
        '.godina=ps.godina'
      '        where  ps.vid_artikal = ma.artvid AND ps.artikal = ma.id'
      
        '        order by coalesce(ps.ts_upd,ps.ts_ins) desc),ma.cena) as' +
        ' cena, $$IBEC$$*/'
      '       (select first 1 p.cenabezddv'
      '                   from  jn_dogovor_stavki ds'
      
        '                   inner join jn_dogovor d on d.broj_dogovor = d' +
        's.broj_dogovor'
      
        '                   inner join jn_ponudi_stavki p on p.artikal = ' +
        'ds.artikal and p.vid_artikal = ds.vid_artikal and p.tip_partner ' +
        '= d.tip_partner and p.partner = d.partner and p.broj_tender = ds' +
        '.broj_tender'
      
        '                   where ds.vid_artikal = ma.artvid and  ds.arti' +
        'kal=ma.id'
      '                   order by ds.ts_ins  desc) as cena,'
      '       ma.grupa,'
      '       mg.naziv as grupaNziv,'
      '       cast (0.00 as numeric(15,2)) as kolicina,'
      '       (select first 1 p.danok'
      '                   from  jn_dogovor_stavki ds'
      
        '                   inner join jn_dogovor d on d.broj_dogovor = d' +
        's.broj_dogovor'
      
        '                   inner join jn_ponudi_stavki p on p.artikal = ' +
        'ds.artikal and p.vid_artikal = ds.vid_artikal and p.tip_partner ' +
        '= d.tip_partner and p.partner = d.partner and p.broj_tender = ds' +
        '.broj_tender'
      
        '                   where ds.vid_artikal = ma.artvid and  ds.arti' +
        'kal=ma.id'
      '                   order by ds.ts_ins  desc) as tarifa,'
      '       /*$$IBEC$$ coalesce((select first 1 (ps.danok) as tarifa'
      '        from jn_godisen_plan_sektori ps'
      
        '        inner join jn_god_plan_odluka p on p.broj=ps.broj and  p' +
        '.godina=ps.godina'
      
        '        where  ps.vid_artikal = ma.artvid  AND ps.artikal = ma.i' +
        'd'
      
        '        order by coalesce(ps.ts_upd,ps.ts_ins) desc), ma.tarifa)' +
        ' as tarifa, $$IBEC$$*/'
      '       ma.jn_cpv,'
      '       ma.aktiven,'
      '       case when ma.aktiven = 1 then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '             when ma.aktiven = 0 then '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '       end "AktivenNaziv"'
      ''
      'from mtr_artikal ma'
      'inner join jn_tip_artikal mv on mv.id = ma.jn_tip_artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      
        'left join jn_godisen_plan_sektori gps on gps.vid_artikal=ma.artv' +
        'id and gps.artikal=ma.id'
      'and gps.re=:re and gps.broj=:broj and gps.godina=:godina'
      'where gps.artikal is null and ma.nabaven=1 and ma.aktiven = 1'
      'order by ma.jn_tip_artikal, ma.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 304
    Top = 176
    object tblIzborStavkiGodPlanSektoriVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIzborStavkiGodPlanSektoriMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriGRUPANZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.0000 , .'
    end
    object tblIzborStavkiGodPlanSektoriTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
      DisplayFormat = '0.00 , .'
    end
    object tblIzborStavkiGodPlanSektoriKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiGodPlanSektoriAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085'/'#1053#1077#1072#1082#1090#1080#1074#1077#1085' - '#1096#1080#1092#1088#1072
      FieldName = 'AKTIVEN'
    end
    object tblIzborStavkiGodPlanSektoriAktivenNaziv: TFIBStringField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085'/'#1053#1077#1072#1082#1090#1080#1074#1077#1085
      FieldName = 'AktivenNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiGodPlanSektoriNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiGodPlanSektoriGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiGodPlanSektoriARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTVID'
    end
  end
  object dsIzborStavkiGodPlanSektori: TDataSource
    DataSet = tblIzborStavkiGodPlanSektori
    Left = 456
    Top = 176
  end
  object tblKriterium: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_KRITERIUMI'
      'SET '
      '    SIFRA = :SIFRA,'
      '    NAZIV = :NAZIV,'
      '    VREDNUVANJE = :VREDNUVANJE,'
      '    BODIRANJE = :BODIRANJE,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    SIFRA = :OLD_SIFRA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_KRITERIUMI'
      'WHERE'
      '        SIFRA = :OLD_SIFRA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_KRITERIUMI('
      '    SIFRA,'
      '    NAZIV,'
      '    VREDNUVANJE,'
      '    BODIRANJE,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :SIFRA,'
      '    :NAZIV,'
      '    :VREDNUVANJE,'
      '    :BODIRANJE,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select k.sifra,'
      '       k.naziv,'
      '       k.vrednuvanje,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       k.bodiranje,'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from jn_kriteriumi k'
      ''
      ' WHERE '
      '        K.SIFRA = :OLD_SIFRA'
      '    ')
    SelectSQL.Strings = (
      'select k.sifra,'
      '       k.naziv,'
      '       k.vrednuvanje,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       k.bodiranje,'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from jn_kriteriumi k'
      'order by k.sifra')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 40
    Top = 40
    object tblKriteriumSIFRA: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKriteriumNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKriteriumVREDNUVANJE: TFIBIntegerField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'VREDNUVANJE'
    end
    object tblKriteriumvrednuvanjeNaziv: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'vrednuvanjeNaziv'
      Size = 38
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKriteriumBODIRANJE: TFIBIntegerField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'BODIRANJE'
    end
    object tblKriteriumbodiranjeNaziv: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '
      FieldName = 'bodiranjeNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKriteriumTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblKriteriumTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblKriteriumUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKriteriumUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKriterium: TDataSource
    DataSet = tblKriterium
    Left = 136
    Top = 40
  end
  object tblKomisija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_KOMISIJA'
      'SET '
      '    ID = :ID,'
      '    IME_PREZIME = :IME_PREZIME,'
      '    ULOGA = :ULOGA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_KOMISIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_KOMISIJA('
      '    ID,'
      '    IME_PREZIME,'
      '    ULOGA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :IME_PREZIME,'
      '    :ULOGA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select k.id,'
      '       k.ime_prezime,'
      '       k.uloga,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from jn_komisija k'
      ''
      ' WHERE '
      '        K.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select k.id,'
      '       k.ime_prezime,'
      '       k.uloga,'
      '       k.ts_ins,'
      '       k.ts_upd,'
      '       k.usr_ins,'
      '       k.usr_upd'
      'from jn_komisija k'
      'order by k.ime_prezime')
    AutoUpdateOptions.UpdateTableName = 'JN_KOMISIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_KOMISIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 40
    Top = 96
    object tblKomisijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblKomisijaIME_PREZIME: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1055#1088#1077#1079#1080#1084#1077
      FieldName = 'IME_PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKomisijaULOGA: TFIBStringField
      DisplayLabel = #1059#1083#1086#1075#1072
      FieldName = 'ULOGA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKomisijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblKomisijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblKomisijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKomisijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKomisija: TDataSource
    DataSet = tblKomisija
    Left = 136
    Top = 96
  end
  object tblTipNabavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TIP_NABAVKA'
      'SET '
      '    SIFRA = :SIFRA,'
      '    NAZIV = :NAZIV,'
      '    DENOVI_ZALBA = :DENOVI_ZALBA,'
      '    OPIS = :OPIS,'
      '    TIP_GOD_PLAN = :TIP_GOD_PLAN,'
      '    ROK_DOGOVOR = :ROK_DOGOVOR,'
      '    AKTIVEN = :AKTIVEN,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    SIFRA = :OLD_SIFRA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TIP_NABAVKA'
      'WHERE'
      '        SIFRA = :OLD_SIFRA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TIP_NABAVKA('
      '    SIFRA,'
      '    NAZIV,'
      '    DENOVI_ZALBA,'
      '    OPIS,'
      '    TIP_GOD_PLAN,'
      '    ROK_DOGOVOR,'
      '    AKTIVEN,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :SIFRA,'
      '    :NAZIV,'
      '    :DENOVI_ZALBA,'
      '    :OPIS,'
      '    :TIP_GOD_PLAN,'
      '    :ROK_DOGOVOR,'
      '    :AKTIVEN,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select tn.sifra,'
      '       tn.naziv,'
      '       tn.denovi_zalba,'
      '       tn.opis,'
      '       tn.tip_god_plan,'
      '       case when tn.tip_god_plan = 1 then '#39#1053#1077#39
      '            when tn.tip_god_plan = 0 then '#39#1044#1072#39
      '       end "tip_god_plan_Naziv",'
      '       tn.rok_dogovor,'
      '       tn.aktiven,'
      '       tn.ts_ins,'
      '       tn.ts_upd,'
      '       tn.usr_ins,'
      '       tn.usr_upd'
      'from jn_tip_nabavka tn'
      ''
      ' WHERE '
      '        TN.SIFRA = :OLD_SIFRA'
      '    ')
    SelectSQL.Strings = (
      'select tn.sifra,'
      '       tn.naziv,'
      '       tn.denovi_zalba,'
      '       tn.opis,'
      '       tn.tip_god_plan,'
      '       case when tn.tip_god_plan = 1 then '#39#1053#1077#39
      '            when tn.tip_god_plan = 0 then '#39#1044#1072#39
      '       end "tip_god_plan_Naziv",'
      '       tn.rok_dogovor,'
      '       tn.aktiven,'
      '       tn.ts_ins,'
      '       tn.ts_upd,'
      '       tn.usr_ins,'
      '       tn.usr_upd'
      'from jn_tip_nabavka tn'
      'order by tn.sifra')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 40
    Top = 160
    object tblTipNabavkaSIFRA: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object tblTipNabavkaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipNabavkaDENOVI_ZALBA: TFIBIntegerField
      DisplayLabel = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1078#1072#1083#1073#1072
      FieldName = 'DENOVI_ZALBA'
    end
    object tblTipNabavkaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipNabavkaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTipNabavkaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTipNabavkaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipNabavkaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipNabavkaTIP_GOD_PLAN: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_GOD_PLAN'
    end
    object tblTipNabavkatip_god_plan_Naziv: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      FieldName = 'tip_god_plan_Naziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipNabavkaAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblTipNabavkaROK_DOGOVOR: TFIBIntegerField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'ROK_DOGOVOR'
    end
  end
  object dsTipNabavka: TDataSource
    DataSet = tblTipNabavka
    Left = 144
    Top = 160
  end
  object tblVidoviDogovori: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_VIDOVI_DOGOVORI'
      'SET '
      '    VID = :VID,'
      '    SIFRA = :SIFRA,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    VID = :OLD_VID'
      '    and SIFRA = :OLD_SIFRA'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_VIDOVI_DOGOVORI'
      'WHERE'
      '        VID = :OLD_VID'
      '    and SIFRA = :OLD_SIFRA'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_VIDOVI_DOGOVORI('
      '    VID,'
      '    SIFRA,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :VID,'
      '    :SIFRA,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select vd.vid,'
      '       vd.sifra,'
      '       case when vd.vid = 1 then '#39#1057#1090#1086#1082#1080#39
      '            when vd.vid = 2 then '#39#1059#1089#1083#1091#1075#1080#39
      '            when vd.vid = 3 then '#39#1056#1072#1073#1086#1090#1080#39
      '       end "VidNaziv",'
      '       vd.opis,'
      '       vd.ts_ins,'
      '       vd.ts_upd,'
      '       vd.usr_ins,'
      '       vd.usr_upd'
      'from jn_vidovi_dogovori vd'
      'where(  vd.vid like :vid'
      '     ) and (     VD.VID = :OLD_VID'
      '    and VD.SIFRA = :OLD_SIFRA'
      '     )'
      '    '
      'order by vd.vid, vd.sifra')
    SelectSQL.Strings = (
      'select vd.vid,'
      '       vd.sifra,'
      '       case when vd.vid = 1 then '#39#1057#1090#1086#1082#1080#39
      '            when vd.vid = 2 then '#39#1059#1089#1083#1091#1075#1080#39
      '            when vd.vid = 3 then '#39#1056#1072#1073#1086#1090#1080#39
      '       end "VidNaziv",'
      '       vd.opis,'
      '       vd.ts_ins,'
      '       vd.ts_upd,'
      '       vd.usr_ins,'
      '       vd.usr_upd'
      'from jn_vidovi_dogovori vd'
      'where vd.vid like :vid'
      'order by vd.vid, vd.sifra')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 40
    Top = 216
    object tblVidoviDogovoriVID: TFIBSmallIntField
      DisplayLabel = #1042#1080#1076' ('#1064#1080#1092#1088#1072')'
      FieldName = 'VID'
    end
    object tblVidoviDogovoriVidNaziv: TFIBStringField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VidNaziv'
      Size = 6
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidoviDogovoriSIFRA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object tblVidoviDogovoriOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidoviDogovoriTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblVidoviDogovoriTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblVidoviDogovoriUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidoviDogovoriUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsVidoviDogovori: TDataSource
    DataSet = tblVidoviDogovori
    Left = 144
    Top = 216
  end
  object tblTender: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER'
      'SET '
      '    BROJ = :BROJ,'
      '    BROJ_PART1 = :BROJ_PART1,'
      '    TIP_TENDER = :TIP_TENDER,'
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    ARHIVSKI_BROJ = :ARHIVSKI_BROJ,'
      '    PONISTEN = :PONISTEN,'
      '    OPIS = :OPIS,'
      '    INTEREN_BROJ = :INTEREN_BROJ,'
      '    ZATVOREN = :ZATVOREN,'
      '    VRSKA_PREDMET = :VRSKA_PREDMET,'
      '    IZNOS = :IZNOS,'
      '    iznos_so_ddv = :iznos_so_ddv,'
      '    DELIV = :DELIV,'
      '    OGLAS = :OGLAS,'
      '    GRUPA = :GRUPA,'
      '    GRUPA_SIFRA = :GRUPA_SIFRA,'
      '    PREDMET_NABAVKA = :PREDMET_NABAVKA,'
      '    LICE_PODGOTOVKA = :LICE_PODGOTOVKA,'
      '    LICE_REALIZACIJA = :LICE_REALIZACIJA,'
      '    LICE_KONTAKT = :LICE_KONTAKT,'
      '    TIP_BARANJA = :TIP_BARANJA,'
      '    TIP_OCENA = :TIP_OCENA,'
      '    EMAIL_KONTAKT = :EMAIL_KONTAKT,'
      '    OBRAZLOZENIE_POSTAPKA = :OBRAZLOZENIE_POSTAPKA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    KONTAKT_TEL_FAKS = :KONTAKT_TEL_FAKS,'
      '    PONISTEN_PRICINA = :PONISTEN_PRICINA,'
      '    STATUS = :STATUS,'
      '    ZATVORI_PONUDI_STATUS = :ZATVORI_PONUDI_STATUS, '
      '    ZATVORI_PONUDI_TIME = :ZATVORI_PONUDI_TIME, '
      '    ZATVORI_PONUDI_USER = :ZATVORI_PONUDI_USER'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER('
      '    BROJ,'
      '    BROJ_PART1,'
      '    TIP_TENDER,'
      '    GODINA,'
      '    DATUM,'
      '    ARHIVSKI_BROJ,      '
      '    PONISTEN,'
      '    OPIS,'
      '    INTEREN_BROJ,'
      '    ZATVOREN,'
      '    VRSKA_PREDMET,'
      '    IZNOS,'
      '    iznos_so_ddv,'
      '    DELIV,'
      '    OGLAS,'
      '    GRUPA,'
      '    GRUPA_SIFRA,'
      '    PREDMET_NABAVKA,'
      '    LICE_PODGOTOVKA,'
      '    LICE_REALIZACIJA,'
      '    LICE_KONTAKT,'
      '    TIP_BARANJA,'
      '    TIP_OCENA,'
      '    EMAIL_KONTAKT,'
      '    OBRAZLOZENIE_POSTAPKA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    KONTAKT_TEL_FAKS,'
      '    PONISTEN_PRICINA,'
      '    STATUS,'
      '    ZATVORI_PONUDI_STATUS, '
      '    ZATVORI_PONUDI_TIME, '
      '    ZATVORI_PONUDI_USER'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :BROJ_PART1,'
      '    :TIP_TENDER,'
      '    :GODINA,'
      '    :DATUM,'
      '    :ARHIVSKI_BROJ,'
      '    :PONISTEN,'
      '    :OPIS,'
      '    :INTEREN_BROJ,'
      '    :ZATVOREN,'
      '    :VRSKA_PREDMET,'
      '    :IZNOS,'
      '    :iznos_so_ddv,'
      '    :DELIV,'
      '    :OGLAS,'
      '    :GRUPA,'
      '    :GRUPA_SIFRA,'
      '    :PREDMET_NABAVKA,'
      '    :LICE_PODGOTOVKA,'
      '    :LICE_REALIZACIJA,'
      '    :LICE_KONTAKT,'
      '    :TIP_BARANJA,'
      '    :TIP_OCENA,'
      '    :EMAIL_KONTAKT,'
      '    :OBRAZLOZENIE_POSTAPKA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :KONTAKT_TEL_FAKS,'
      '    :PONISTEN_PRICINA,'
      '    :STATUS,'
      '    :ZATVORI_PONUDI_STATUS, '
      '    :ZATVORI_PONUDI_TIME, '
      '    :ZATVORI_PONUDI_USER'
      ')')
    RefreshSQL.Strings = (
      'select t.broj,'
      '       t.BROJ_PART1,'
      '       t.tip_tender,'
      '       tn.naziv as tipTenderNaziv,'
      '       tn.denovi_zalba as tipTenderDenoviZalba,'
      '       tn.tip_god_plan,'
      '       case when tn.tip_god_plan = 1 then '#39#1053#1077#39
      '            when tn.tip_god_plan = 0 then '#39#1044#1072#39
      '       end "tip_god_plan_Naziv",'
      '       t.godina,t.status,'
      '       t.datum,'
      '       t.arhivski_broj,'
      '       t.ponisten,'
      '       case when t.ponisten = 1 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '            when t.ponisten = 0 then '#39#1055#1086#1085#1080#1096#1090#1077#1085#1072#39
      '       end "ponistenNaziv",'
      '       t.opis,'
      '       t.interen_broj,'
      '       t.zatvoren,'
      '       case when t.zatvoren = -1 then '#39#1042#1086' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072#39
      '            when t.zatvoren = 0 then '#39#1054#1090#1074#1086#1088#1077#1085#39
      '            when t.zatvoren = 1 then '#39#1047#1072#1090#1074#1086#1088#1077#1085#39
      '            when t.zatvoren = 2 then '#39#1044#1077#1083#1091#1084#1085#1086' '#1079#1072#1074#1088#1096#1077#1085#39
      '       end "zatvorenNaziv",'
      '       case when t.grupa = 1 then '#39#1057#1090#1086#1082#1080#39
      '            when t.grupa = 2 then '#39#1059#1089#1083#1091#1075#1080#39
      '            when t.grupa = 3 then '#39#1056#1072#1073#1086#1090#1080#39
      '       end "GrupaNaziv",'
      '       t.vrska_predmet,'
      '       t.iznos,'
      '       t.deliv,'
      '       case when t.deliv = 1 then '#39#1044#1072#39
      '            when t.deliv = 0 then '#39#1053#1077#39
      '       end "delivNaziv",'
      '       case when t.tip_baranja = 1 then '#39#1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#39
      '            when t.tip_baranja = 0 then '#39#1055#1086' '#1087#1086#1085#1091#1076#1072#39
      '       end "tip_baranjaNaziv",'
      
        '       case when t.tip_ocena = 1 then '#39#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091 +
        #1076#1072' '#39
      '            when t.tip_ocena = 0 then '#39#1053#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072#39
      '       end "tip_ocenaNaziv",'
      '       t.oglas,'
      '       t.grupa,'
      '       vd.sifra as vidDogovorSifra,'
      '       vd.opis as vidDogovorOpis,'
      '       t.grupa_sifra,'
      '       t.predmet_nabavka,'
      '       t.lice_podgotovka,'
      '       t.lice_realizacija,'
      '       t.lice_kontakt,'
      '       t.tip_baranja,'
      '       t.tip_ocena,'
      '       t.email_kontakt,'
      '       t.obrazlozenie_postapka,'
      '       t.ponisten_pricina,'
      '       p.naziv as pricinaPonistenNaziv,'
      '       t.ts_ins,'
      '       t.ts_upd,'
      '       t.usr_ins,'
      '       t.usr_upd,'
      '       ti.broj,'
      '       ti.datum_do,'
      '       ti.datum_od,'
      '       ti.zalba,'
      '       t.kontakt_tel_faks,'
      '       case when ti.zalba = 1 then '#39#1044#1072#39
      '            when ti.zalba = 0 then '#39#1053#1077#39
      '       end "zalbaNaziv",'
      '       ti.zalba_opis,'
      '       ti.broj_zalbi,'
      '       ti.odluka_datum,'
      '       ti.odluka_broj,'
      '       ti.datum_otvaranje,'
      '       ti.osteteni_koverti,'
      '       ti.odl_nabavka_br,'
      '       ti.odl_nabavka_dat,'
      '       ti.soglasnost_br,'
      '       ti.soglasnost_dat,'
      '       ti.odl_pokana_br,'
      '       ti.odl_pokana_dat,'
      '       ti.broj_pokani,'
      '       ti.potvrda_br,'
      '       ti.potvrda_dat,'
      '       ti.potvrda_iznos,'
      '       ti.aneks_broj,'
      '       ti.aneks_datum,'
      '       ti.aneks_iznos,'
      '       ti.bodiral,'
      '       ti.vreme_bod,'
      '       ti.vreme_otvaranje,'
      '       ti.vkupno_ponudi,'
      '       ti.za_povlekuvanje,'
      '       ti.za_izmena,'
      '       ti.br_zadocneti_ponudi,'
      '       ti.datum_zapisnik,'
      '       ti.mesto_otvaranje,'
      '       ti.centralno_telo,'
      '       case when ti.centralno_telo = 1 then '#39#1044#1072#39
      '            when ti.centralno_telo = 0 then '#39#1053#1077#39
      '       end "centralnoTeloNaziv",'
      '       ti.sektorski_dogovor,'
      '       case when ti.sektorski_dogovor = 1 then '#39#1044#1072#39
      '            when ti.sektorski_dogovor = 0 then '#39#1053#1077#39
      '       end "sektorski_dogovorTeloNaziv",'
      '       ti.grupna_nabavka,'
      '       case when ti.grupna_nabavka = 1 then '#39#1044#1072#39
      '            when ti.grupna_nabavka = 0 then '#39#1053#1077#39
      '       end "grupnaNabavkaNaziv",'
      '       ti.ramkovna_spog,'
      '       case when ti.ramkovna_spog = 1 then '#39#1044#1072#39
      '            when ti.ramkovna_spog = 0 then '#39#1053#1077#39
      '       end "ramkovnaSpogodbaNaziv",'
      '       ti.rs_poveke_organi,'
      '       case when ti.rs_poveke_organi = 1 then '#39#1044#1072#39
      '            when ti.rs_poveke_organi = 0 then '#39#1053#1077#39
      '       end "rsPovekeOrganiNaziv",'
      '       ti.rs_poveke_operatori,'
      
        '       case when ti.rs_poveke_operatori = 1 then '#39#1055#1086#1074#1077#1116#1077' '#1077#1082#1086#1085#1086#1084#1089 +
        #1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088#1080#39
      
        '            when ti.rs_poveke_operatori = 0 then '#39#1045#1076#1077#1085' '#1077#1082#1086#1085#1086#1084#1089#1082#1080 +
        ' '#1086#1087#1077#1088#1072#1090#1086#1088#39
      '       end "rsPovekeOperatoriNaziv",'
      '       ti.alternativni_ponudi,'
      '       case when ti.alternativni_ponudi = 1 then '#39#1044#1072#39
      '            when ti.alternativni_ponudi = 0 then '#39#1053#1077#39
      '       end "alternativniPonudiNaziv",'
      '       ti.garancija_ponuda,'
      '       ti.garancija_kvalitet,'
      '       ti.avansno,'
      '       case when ti.avansno = 1 then '#39#1044#1072#39
      '            when ti.avansno = 0 then '#39#1053#1077#39
      '       end "avansnoNaziv",'
      '       ti.zdruzuvanje_operatori,'
      '       case when ti.zdruzuvanje_operatori = 1 then '#39#1044#1072#39
      '            when ti.zdruzuvanje_operatori = 0 then '#39#1053#1077#39
      '       end "zdruzuvanjeOperatoriNaziv",'
      '       ti.sposobnost,'
      '       ti.skrateni_rokovi,'
      '       case when ti.skrateni_rokovi = 1 then '#39#1044#1072#39
      '            when ti.skrateni_rokovi = 0 then '#39#1053#1077#39
      '       end "skratenNaziv",'
      '       ti.el_sredstva,'
      '       case when ti.el_sredstva = 1 then '#39#1044#1072#39
      '            when ti.el_sredstva = 0 then '#39#1053#1077#39
      '       end "elSredstvaNaziv",'
      '       ti.el_aukcija,'
      '       case when ti.el_aukcija = 1 then '#39#1044#1072#39
      '            when ti.el_aukcija = 0 then '#39#1053#1077#39
      '       end "elAukcijaNaziv",'
      '       case when ti.garancija_izjava = 1 then '#39#1044#1072#39
      '            when ti.garancija_izjava = 0 then '#39#1053#1077#39
      '       end "garancijaIzjavaNaziv",'
      '       ti.info_aukcija,'
      '       ti.cena_dokumentacija,'
      '       ti.info_plakjanje,'
      '       ti.denarska,'
      '       ti.devizna,'
      '       ti.validnost_ponuda,'
      '       ti.dopolnitelni_info,'
      '       ti.vremetraenje_dog,'
      '       ti.lokacija_isporaka,'
      '       ti.odluka_prosiruvanje,'
      '       ti.datum_odluka_prosiruvanje,'
      '       ti.iznos_prosiruvanje,'
      '       ti.izvor_sredstva,'
      '       ti.min_ponudi,'
      '       ti.maks_ponudi,'
      '       ti.ZABELESKA_ZAPISNIK,'
      
        '       ti.ts_ins as ti_ts_ins, ti.ts_upd as ti_ts_upd, ti.usr_in' +
        's as ti_usr_ins, ti.usr_upd as ti_usr_upd,'
      
        '       t.ZATVORI_PONUDI_STATUS, t.ZATVORI_PONUDI_TIME, t.ZATVORI' +
        '_PONUDI_USER'
      'from jn_tender t'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      
        'left outer join jn_vidovi_dogovori vd on vd.vid=t.grupa and vd.s' +
        'ifra = t.grupa_sifra'
      
        'left outer join jn_pricina_ponistuvanje p on p.id = t.ponisten_p' +
        'ricina'
      'right join jn_tender_info ti on ti.broj = t.broj'
      'where(  t.zatvoren like :status'
      '     ) and (     T.BROJ = :OLD_BROJ'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select t.broj,'
      '       t.BROJ_PART1,'
      '       t.tip_tender,'
      '       tn.naziv as tipTenderNaziv,'
      '       tn.denovi_zalba as tipTenderDenoviZalba,'
      '       tn.tip_god_plan,'
      '       case when tn.tip_god_plan = 1 then '#39#1053#1077#39
      '            when tn.tip_god_plan = 0 then '#39#1044#1072#39
      '       end "tip_god_plan_Naziv",'
      '       t.godina,t.status,'
      '       t.datum,'
      '       t.arhivski_broj,'
      '       t.ponisten,'
      '       case when t.ponisten = 1 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '            when t.ponisten = 0 then '#39#1055#1086#1085#1080#1096#1090#1077#1085#1072#39
      '       end "ponistenNaziv",'
      '       t.opis,'
      '       t.interen_broj,'
      '       t.zatvoren,'
      '       case when t.zatvoren = -1 then '#39#1042#1086' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072#39
      '            when t.zatvoren = 0 then '#39#1054#1090#1074#1086#1088#1077#1085#39
      '            when t.zatvoren = 1 then '#39#1047#1072#1090#1074#1086#1088#1077#1085#39
      '            when t.zatvoren = 2 then '#39#1044#1077#1083#1091#1084#1085#1086' '#1079#1072#1074#1088#1096#1077#1085#39
      '       end "zatvorenNaziv",'
      '       case when t.grupa = 1 then '#39#1057#1090#1086#1082#1080#39
      '            when t.grupa = 2 then '#39#1059#1089#1083#1091#1075#1080#39
      '            when t.grupa = 3 then '#39#1056#1072#1073#1086#1090#1080#39
      '       end "GrupaNaziv",'
      '       t.vrska_predmet,'
      '       t.iznos,'
      '       t.iznos_so_ddv,'
      '       t.deliv,'
      '       case when t.deliv = 1 then '#39#1044#1072#39
      '            when t.deliv = 0 then '#39#1053#1077#39
      '       end "delivNaziv",'
      '       case when t.tip_baranja = 1 then '#39#1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#39
      '            when t.tip_baranja = 0 then '#39#1055#1086' '#1087#1086#1085#1091#1076#1072#39
      
        '            when t.tip_baranja = 2 then '#39#1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1073#1077#1079 +
        ' '#1082#1086#1085#1090#1088#1086#1083#1072' '#1085#1072' '#1080#1079#1085#1086#1089' '#1086#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085#39
      '       end "tip_baranjaNaziv",'
      '       case when t.tip_ocena = 0 then '#39#1053#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072#39
      
        '            when t.tip_ocena = 1 then '#39#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091 +
        #1076#1072' '#39
      
        '            when t.tip_ocena = 2 then '#39#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091 +
        #1076#1072' '#1074#1088#1079' '#1086#1089#1085#1086#1074#1072' '#1085#1072' '#1094#1077#1085#1072#1090#1072' '#1089#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072' '#39
      
        '            when t.tip_ocena = 3 then '#39#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091 +
        #1076#1072' '#1074#1088#1079' '#1086#1089#1085#1086#1074#1072' '#1085#1072' '#1094#1077#1085#1072#1090#1072' '#1073#1077#1079' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072' '#39
      '       end "tip_ocenaNaziv",'
      '       t.oglas,'
      '       t.grupa,'
      '       vd.sifra as vidDogovorSifra,'
      '       vd.opis as vidDogovorOpis,'
      '       t.grupa_sifra,'
      '       t.predmet_nabavka,'
      '       t.lice_podgotovka,'
      '       t.lice_realizacija,'
      '       t.lice_kontakt,'
      '       t.tip_baranja,'
      '       t.tip_ocena,'
      '       t.email_kontakt,'
      '       t.obrazlozenie_postapka,'
      '       t.ponisten_pricina,'
      '       p.naziv as pricinaPonistenNaziv,'
      '       t.ts_ins,'
      '       t.ts_upd,'
      '       t.usr_ins,'
      '       t.usr_upd,'
      '       ti.broj,'
      '       ti.datum_do,'
      '       ti.datum_od,'
      '       ti.zalba,'
      '       t.kontakt_tel_faks,'
      '       case when ti.zalba = 1 then '#39#1044#1072#39
      '            when ti.zalba = 0 then '#39#1053#1077#39
      '       end "zalbaNaziv",'
      '       ti.zalba_opis,'
      '       ti.broj_zalbi,'
      '       ti.odluka_datum,'
      '       ti.odluka_broj,'
      '       ti.datum_otvaranje,'
      '       ti.osteteni_koverti,'
      '       ti.odl_nabavka_br,'
      '       ti.odl_nabavka_dat,'
      '       ti.soglasnost_br,'
      '       ti.soglasnost_dat,'
      '       ti.odl_pokana_br,'
      '       ti.odl_pokana_dat,'
      '       ti.broj_pokani,'
      '       ti.potvrda_br,'
      '       ti.potvrda_dat,'
      '       ti.potvrda_iznos,'
      '       ti.aneks_broj,'
      '       ti.aneks_datum,'
      '       ti.aneks_iznos,'
      '       ti.bodiral,'
      '       ti.vreme_bod,'
      '       ti.vreme_otvaranje,'
      '       ti.vkupno_ponudi,'
      '       ti.za_povlekuvanje,'
      '       ti.za_izmena,'
      '       ti.br_zadocneti_ponudi,'
      '       ti.datum_zapisnik,'
      '       ti.mesto_otvaranje,'
      '       ti.centralno_telo,'
      '       case when ti.centralno_telo = 1 then '#39#1044#1072#39
      '            when ti.centralno_telo = 0 then '#39#1053#1077#39
      '       end "centralnoTeloNaziv",'
      '       ti.sektorski_dogovor,'
      '       case when ti.sektorski_dogovor = 1 then '#39#1044#1072#39
      '            when ti.sektorski_dogovor = 0 then '#39#1053#1077#39
      '       end "sektorski_dogovorTeloNaziv",'
      '       ti.grupna_nabavka,'
      '       case when ti.grupna_nabavka = 1 then '#39#1044#1072#39
      '            when ti.grupna_nabavka = 0 then '#39#1053#1077#39
      '       end "grupnaNabavkaNaziv",'
      '       ti.ramkovna_spog,'
      '       case when ti.ramkovna_spog = 1 then '#39#1044#1072#39
      '            when ti.ramkovna_spog = 0 then '#39#1053#1077#39
      '       end "ramkovnaSpogodbaNaziv",'
      '       ti.rs_poveke_organi,'
      '       case when ti.rs_poveke_organi = 1 then '#39#1044#1072#39
      '            when ti.rs_poveke_organi = 0 then '#39#1053#1077#39
      '       end "rsPovekeOrganiNaziv",'
      '       ti.rs_poveke_operatori,'
      
        '       case when ti.rs_poveke_operatori = 1 then '#39#1055#1086#1074#1077#1116#1077' '#1077#1082#1086#1085#1086#1084#1089 +
        #1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088#1080#39
      
        '            when ti.rs_poveke_operatori = 0 then '#39#1045#1076#1077#1085' '#1077#1082#1086#1085#1086#1084#1089#1082#1080 +
        ' '#1086#1087#1077#1088#1072#1090#1086#1088#39
      '       end "rsPovekeOperatoriNaziv",'
      '       ti.alternativni_ponudi,'
      '       case when ti.alternativni_ponudi = 1 then '#39#1044#1072#39
      '            when ti.alternativni_ponudi = 0 then '#39#1053#1077#39
      '       end "alternativniPonudiNaziv",'
      '       ti.garancija_ponuda,'
      '       ti.garancija_kvalitet,'
      '       ti.avansno,'
      '       case when ti.avansno = 1 then '#39#1044#1072#39
      '            when ti.avansno = 0 then '#39#1053#1077#39
      '       end "avansnoNaziv",'
      '       ti.zdruzuvanje_operatori,'
      '       case when ti.zdruzuvanje_operatori = 1 then '#39#1044#1072#39
      '            when ti.zdruzuvanje_operatori = 0 then '#39#1053#1077#39
      '       end "zdruzuvanjeOperatoriNaziv",'
      '       ti.sposobnost,'
      '       ti.skrateni_rokovi,'
      '       case when ti.skrateni_rokovi = 1 then '#39#1044#1072#39
      '            when ti.skrateni_rokovi = 0 then '#39#1053#1077#39
      '       end "skratenNaziv",'
      '       ti.el_sredstva,'
      '       case when ti.el_sredstva = 1 then '#39#1044#1072#39
      '            when ti.el_sredstva = 0 then '#39#1053#1077#39
      '       end "elSredstvaNaziv",'
      '       ti.el_aukcija,'
      '       case when ti.el_aukcija = 1 then '#39#1044#1072#39
      '            when ti.el_aukcija = 0 then '#39#1053#1077#39
      '       end "elAukcijaNaziv",'
      '       case when ti.garancija_izjava = 1 then '#39#1044#1072#39
      '            when ti.garancija_izjava = 0 then '#39#1053#1077#39
      '       end "garancijaIzjavaNaziv",'
      '       ti.info_aukcija,'
      '       ti.cena_dokumentacija,'
      '       ti.info_plakjanje,'
      '       ti.denarska,'
      '       ti.devizna,'
      '       ti.validnost_ponuda,'
      '       ti.dopolnitelni_info,'
      '       ti.vremetraenje_dog,'
      '       ti.lokacija_isporaka,'
      '       ti.odluka_prosiruvanje,'
      '       ti.datum_odluka_prosiruvanje,'
      '       ti.iznos_prosiruvanje,'
      '       ti.izvor_sredstva,'
      '       ti.min_ponudi,'
      '       ti.maks_ponudi,'
      '       ti.ZABELESKA_ZAPISNIK,'
      
        '       ti.ts_ins as ti_ts_ins, ti.ts_upd as ti_ts_upd, ti.usr_in' +
        's as ti_usr_ins, ti.usr_upd as ti_usr_upd,'
      
        '       t.ZATVORI_PONUDI_STATUS, t.ZATVORI_PONUDI_TIME, t.ZATVORI' +
        '_PONUDI_USER'
      'from jn_tender t'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      
        'left outer join jn_vidovi_dogovori vd on vd.vid=t.grupa and vd.s' +
        'ifra = t.grupa_sifra'
      
        'left outer join jn_pricina_ponistuvanje p on p.id = t.ponisten_p' +
        'ricina'
      'right join jn_tender_info ti on ti.broj = t.broj'
      'where t.zatvoren like :status'
      'order by t.broj')
    BeforeDelete = TabelaBeforeDelete
    BeforePost = tblTenderBeforePost
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 624
    Top = 40
    object tblTenderGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblTenderBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblTenderTIP_TENDER: TFIBSmallIntField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_TENDER'
    end
    object tblTenderTIPTENDERNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'TIPTENDERNAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderTIPTENDERDENOVIZALBA: TFIBIntegerField
      DisplayLabel = #1041#1088'. '#1085#1072' '#1076#1077#1085#1086#1074#1080' '#1079#1072' '#1078#1072#1083#1073#1072
      FieldName = 'TIPTENDERDENOVIZALBA'
    end
    object tblTenderARHIVSKI_BROJ: TFIBStringField
      DisplayLabel = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112
      FieldName = 'ARHIVSKI_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPONISTEN: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1080#1096#1090#1077#1085#1072'/'#1042#1072#1083#1080#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PONISTEN'
    end
    object tblTenderOPIS: TFIBBlobField
      DisplayLabel = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072#1090#1072
      FieldName = 'OPIS'
      Size = 8
    end
    object tblTenderINTEREN_BROJ: TFIBStringField
      DisplayLabel = #1048#1085#1090#1077#1088#1077#1085' '#1073#1088#1086#1112
      FieldName = 'INTEREN_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderZATVOREN: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ZATVOREN'
    end
    object tblTenderVRSKA_PREDMET: TFIBStringField
      DisplayLabel = #1042#1088#1089#1082#1072' '#1089#1086' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'VRSKA_PREDMET'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderIZNOS: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' ('#1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042')'
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderDELIV: TFIBIntegerField
      DisplayLabel = #1044#1072#1083#1080' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1077' '#1076#1077#1083#1080#1074'? ('#1064#1080#1092#1088#1072')'
      FieldName = 'DELIV'
    end
    object tblTenderOGLAS: TFIBStringField
      DisplayLabel = #1054#1075#1083#1072#1089' '#1079#1072' '#1112#1072#1074#1077#1085' '#1087#1086#1074#1080#1082' '#1074#1086' '#1089#1083#1091#1078#1073#1077#1085' '#1074#1077#1089#1085#1080#1082
      FieldName = 'OGLAS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGRUPA: TFIBSmallIntField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' ('#1042#1080#1076' - '#1064#1080#1092#1088#1072')'
      FieldName = 'GRUPA'
    end
    object tblTenderVIDDOGOVOROPIS: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' - '#1054#1087#1080#1089
      FieldName = 'VIDDOGOVOROPIS'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGRUPA_SIFRA: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'GRUPA_SIFRA'
    end
    object tblTenderPREDMET_NABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET_NABAVKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderLICE_PODGOTOVKA: TFIBStringField
      DisplayLabel = #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072
      FieldName = 'LICE_PODGOTOVKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderLICE_REALIZACIJA: TFIBStringField
      FieldName = 'LICE_REALIZACIJA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderLICE_KONTAKT: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'LICE_KONTAKT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderTIP_BARANJA: TFIBSmallIntField
      DisplayLabel = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076#1085#1089#1085#1086' '#1073#1072#1088#1072#1114#1077#1090#1086' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_BARANJA'
    end
    object tblTenderTIP_OCENA: TFIBSmallIntField
      DisplayLabel = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1076#1086#1075#1086#1074#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_OCENA'
    end
    object tblTenderEMAIL_KONTAKT: TFIBStringField
      DisplayLabel = 'e-mail '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'EMAIL_KONTAKT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089' - '#1079#1072' '#1086#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'TS_INS'
    end
    object tblTenderTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089' - '#1079#1072' '#1086#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'TS_UPD'
    end
    object tblTenderUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090' - '#1079#1072' '#1086#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090' - '#1079#1072' '#1086#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderponistenNaziv: TFIBStringField
      DisplayLabel = #1055#1086#1085#1080#1096#1090#1077#1085#1072'/'#1042#1072#1083#1080#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'ponistenNaziv'
      Size = 9
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderzatvorenNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'zatvorenNaziv'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderdelivNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1077' '#1076#1077#1083#1080#1074'?'
      FieldName = 'delivNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTendertip_baranjaNaziv: TFIBStringField
      DisplayLabel = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076#1085#1089#1085#1086' '#1073#1072#1088#1072#1114#1077#1090#1086' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      FieldName = 'tip_baranjaNaziv'
      Size = 57
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTendertip_ocenaNaziv: TFIBStringField
      DisplayLabel = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'tip_ocenaNaziv'
      Size = 86
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGrupaNaziv: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'GrupaNaziv'
      Size = 6
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderVIDDOGOVORSIFRA: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1086#1074#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'VIDDOGOVORSIFRA'
    end
    object tblTenderBROJ1: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ1'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderZALBA: TFIBIntegerField
      DisplayLabel = #1044#1072#1083#1080' '#1077' '#1087#1086#1076#1085#1077#1089#1077#1085#1072' '#1078#1072#1083#1073#1072' '#1079#1072' '#1087#1088#1077#1076#1084#1077#1090'? ('#1064#1080#1092#1088#1072')'
      FieldName = 'ZALBA'
    end
    object tblTenderzalbaNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1077' '#1087#1086#1076#1085#1077#1089#1077#1085#1072' '#1078#1072#1083#1073#1072' '#1079#1072' '#1087#1088#1077#1076#1084#1077#1090'?'
      FieldName = 'zalbaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderZALBA_OPIS: TFIBStringField
      DisplayLabel = #1056#1077#1079#1091#1083#1090#1072#1090' '#1087#1086' '#1078#1072#1083#1073#1072
      FieldName = 'ZALBA_OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderBROJ_ZALBI: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1078#1072#1083#1073#1080
      FieldName = 'BROJ_ZALBI'
    end
    object tblTenderODLUKA_DATUM: TFIBDateField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1077#1085' '#1087#1086#1085#1091#1076#1091#1074#1072#1095' - '#1044#1072#1090#1091#1084
      FieldName = 'ODLUKA_DATUM'
    end
    object tblTenderODLUKA_BROJ: TFIBStringField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1077#1085' '#1087#1086#1085#1091#1076#1091#1074#1072#1095' - '#1041#1088#1086#1112
      FieldName = 'ODLUKA_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDATUM_OTVARANJE: TFIBDateField
      DisplayLabel = #1054#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1044#1072#1090#1091#1084
      FieldName = 'DATUM_OTVARANJE'
    end
    object tblTenderOSTETENI_KOVERTI: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1086#1096#1090#1077#1090#1077#1085#1080' '#1082#1086#1074#1077#1088#1090#1080
      FieldName = 'OSTETENI_KOVERTI'
    end
    object tblTenderODL_NABAVKA_BR: TFIBStringField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1041#1088#1086#1112
      FieldName = 'ODL_NABAVKA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderODL_NABAVKA_DAT: TFIBDateField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1044#1072#1090#1091#1084
      FieldName = 'ODL_NABAVKA_DAT'
    end
    object tblTenderSOGLASNOST_BR: TFIBStringField
      DisplayLabel = #1057#1086#1075#1083#1072#1089#1085#1086#1089#1090' '#1086#1076' '#1073#1080#1088#1086' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080' - '#1041#1088#1086#1112
      FieldName = 'SOGLASNOST_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderSOGLASNOST_DAT: TFIBDateField
      DisplayLabel = #1057#1086#1075#1083#1072#1089#1085#1086#1089#1090' '#1086#1076' '#1073#1080#1088#1086' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080' - '#1044#1072#1090#1091#1084
      FieldName = 'SOGLASNOST_DAT'
    end
    object tblTenderODL_POKANA_BR: TFIBStringField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072#1085#1080' - '#1041#1088#1086#1112
      FieldName = 'ODL_POKANA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderODL_POKANA_DAT: TFIBDateField
      DisplayLabel = 
        #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072#1085#1080' - '#1044#1072#1090#1091 +
        #1084
      FieldName = 'ODL_POKANA_DAT'
    end
    object tblTenderBROJ_POKANI: TFIBSmallIntField
      DisplayLabel = 
        #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072#1085#1080' - '#1041#1088#1086#1112 +
        ' '#1085#1072' '#1087#1086#1082#1072#1085#1080
      FieldName = 'BROJ_POKANI'
    end
    object tblTenderPOTVRDA_BR: TFIBStringField
      DisplayLabel = 
        #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1086#1076' '#1084#1080#1085#1080#1089#1090#1077#1088#1089#1090#1074#1086' '#1079#1072' '#1092#1080#1085#1072#1085#1089#1080#1080' - '#1041#1088#1086 +
        #1112
      FieldName = 'POTVRDA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPOTVRDA_DAT: TFIBDateField
      DisplayLabel = 
        #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1086#1076' '#1084#1080#1085#1080#1089#1090#1077#1088#1089#1090#1074#1086' '#1079#1072' '#1092#1080#1085#1072#1085#1089#1080#1080' - '#1044#1072#1090 +
        #1091#1084
      FieldName = 'POTVRDA_DAT'
    end
    object tblTenderPOTVRDA_IZNOS: TFIBFloatField
      DisplayLabel = 
        #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1086#1076' '#1084#1080#1085#1080#1089#1090#1077#1088#1089#1090#1074#1086' '#1079#1072' '#1092#1080#1085#1072#1085#1089#1080#1080' - '#1048#1079#1085 +
        #1086#1089
      FieldName = 'POTVRDA_IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderANEKS_BROJ: TFIBStringField
      DisplayLabel = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' - '#1041#1088#1086#1112
      FieldName = 'ANEKS_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderANEKS_DATUM: TFIBDateField
      DisplayLabel = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' - '#1044#1072#1090#1091#1084
      FieldName = 'ANEKS_DATUM'
    end
    object tblTenderANEKS_IZNOS: TFIBFloatField
      DisplayLabel = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' - '#1048#1079#1085#1086#1089
      FieldName = 'ANEKS_IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderBODIRAL: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077' '#1082#1086#1077' '#1080#1079#1074#1088#1096#1080#1083#1086' '#1073#1086#1076#1080#1088#1072#1114#1077
      FieldName = 'BODIRAL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderVREME_BOD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
      FieldName = 'VREME_BOD'
    end
    object tblTenderVREME_OTVARANJE: TFIBTimeField
      DisplayLabel = #1054#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1042#1088#1077#1084#1077
      FieldName = 'VREME_OTVARANJE'
    end
    object tblTenderVKUPNO_PONUDI: TFIBSmallIntField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1087#1086#1085#1091#1076#1080
      FieldName = 'VKUPNO_PONUDI'
    end
    object tblTenderZA_POVLEKUVANJE: TFIBBlobField
      DisplayLabel = #1050#1086#1074#1077#1088#1090#1080' '#1079#1072' '#1087#1086#1074#1083#1077#1082#1091#1074#1072#1114#1077' '#1087#1086#1085#1091#1076#1072' '#1089#1086' '#1080#1084#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'ZA_POVLEKUVANJE'
      Size = 8
    end
    object tblTenderZA_IZMENA: TFIBBlobField
      DisplayLabel = #1050#1086#1074#1077#1088#1090#1080' '#1079#1072' '#1080#1079#1084#1077#1085#1072' '#1080#1083#1080' '#1079#1072#1084#1077#1085#1072' '#1089#1086' '#1080#1084#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'ZA_IZMENA'
      Size = 8
    end
    object tblTenderBR_ZADOCNETI_PONUDI: TFIBSmallIntField
      DisplayLabel = #1041#1088'. '#1085#1072' '#1079#1072#1076#1086#1094#1085#1077#1090#1080' '#1087#1086#1085#1091#1076#1080
      FieldName = 'BR_ZADOCNETI_PONUDI'
    end
    object tblTenderDATUM_ZAPISNIK: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1087#1080#1089#1085#1080#1082
      FieldName = 'DATUM_ZAPISNIK'
    end
    object tblTenderMESTO_OTVARANJE: TFIBStringField
      DisplayLabel = #1054#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1052#1077#1089#1090#1086
      FieldName = 'MESTO_OTVARANJE'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderCENTRALNO_TELO: TFIBSmallIntField
      DisplayLabel = #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1112#1072' '#1089#1087#1088#1086#1074#1077#1076#1077' '#1094#1077#1085#1090#1088#1072#1083#1085#1086' '#1090#1077#1083#1086' ?  ('#1064#1080#1092#1088#1072')'
      FieldName = 'CENTRALNO_TELO'
    end
    object tblTendercentralnoTeloNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1112#1072' '#1089#1087#1088#1086#1074#1077#1076#1077' '#1094#1077#1085#1090#1088#1072#1083#1085#1086' '#1090#1077#1083#1086' ?'
      FieldName = 'centralnoTeloNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGRUPNA_NABAVKA: TFIBSmallIntField
      DisplayLabel = #1043#1088#1091#1087#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'GRUPNA_NABAVKA'
    end
    object tblTendergrupnaNabavkaNaziv: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'grupnaNabavkaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderRAMKOVNA_SPOG: TFIBSmallIntField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RAMKOVNA_SPOG'
    end
    object tblTenderramkovnaSpogodbaNaziv: TFIBStringField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072
      FieldName = 'ramkovnaSpogodbaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderRS_POVEKE_ORGANI: TFIBSmallIntField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1087#1086#1084#1077#1107#1091' '#1087#1086#1074#1077#1116#1077' '#1076#1086#1075#1086#1074#1086#1088#1085#1080' '#1086#1088#1075#1072#1085#1080' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RS_POVEKE_ORGANI'
    end
    object tblTenderrsPovekeOrganiNaziv: TFIBStringField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1087#1086#1084#1077#1107#1091' '#1087#1086#1074#1077#1116#1077' '#1076#1086#1075#1086#1074#1086#1088#1085#1080' '#1086#1088#1075#1072#1085#1080
      FieldName = 'rsPovekeOrganiNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderRS_POVEKE_OPERATORI: TFIBSmallIntField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072#1090#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1116#1077' '#1089#1077' '#1089#1082#1083#1091#1095#1080' '#1089#1086'  ('#1064#1080#1092#1088#1072')'
      FieldName = 'RS_POVEKE_OPERATORI'
    end
    object tblTenderrsPovekeOperatoriNaziv: TFIBStringField
      DisplayLabel = #1056#1072#1084#1082#1086#1074#1085#1072#1090#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1116#1077' '#1089#1077' '#1089#1082#1083#1091#1095#1080' '#1089#1086' '
      FieldName = 'rsPovekeOperatoriNaziv'
      Size = 26
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderALTERNATIVNI_PONUDI: TFIBSmallIntField
      DisplayLabel = #1044#1086#1079#1074#1086#1083#1077#1085#1086' '#1077' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1083#1090#1077#1088#1085#1072#1090#1080#1074#1085#1080' '#1087#1086#1085#1091#1076#1080' ('#1064#1080#1092#1088#1072') '
      FieldName = 'ALTERNATIVNI_PONUDI'
    end
    object tblTenderalternativniPonudiNaziv: TFIBStringField
      DisplayLabel = #1044#1086#1079#1074#1086#1083#1077#1085#1086' '#1077' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1083#1090#1077#1088#1085#1072#1090#1080#1074#1085#1080' '#1087#1086#1085#1091#1076#1080
      FieldName = 'alternativniPonudiNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGARANCIJA_PONUDA: TFIBSmallIntField
      DisplayLabel = 
        #1048#1079#1085#1086#1089' '#1085#1072' '#1075#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1082#1072#1082#1086' '#1087#1088#1086#1094#1077#1085#1090' '#1086#1076' '#1074#1088#1077#1076#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072 +
        #1090#1072
      FieldName = 'GARANCIJA_PONUDA'
    end
    object tblTenderGARANCIJA_KVALITET: TFIBSmallIntField
      DisplayLabel = 
        #1048#1079#1085#1086#1089' '#1085#1072' '#1075#1072#1088#1072#1085#1094#1080#1112#1072' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077#1090#1085#1086' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1082#1072#1082#1086' '#1087#1088 +
        #1086#1094#1077#1085#1090' '#1086#1076' '#1074#1088#1077#1076#1085#1086#1089#1090#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
      FieldName = 'GARANCIJA_KVALITET'
    end
    object tblTenderAVANSNO: TFIBSmallIntField
      DisplayLabel = #1040#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077'   ('#1064#1080#1092#1088#1072')'
      FieldName = 'AVANSNO'
    end
    object tblTenderavansnoNaziv: TFIBStringField
      DisplayLabel = #1040#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'avansnoNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderZDRUZUVANJE_OPERATORI: TFIBSmallIntField
      DisplayLabel = 
        #1047#1076#1088#1091#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1088#1091#1087#1072' '#1086#1087#1077#1088#1072#1090#1086#1088#1080' '#1074#1086' '#1087#1088#1072#1074#1085#1072' '#1092#1086#1088#1084#1072' '#1079#1072' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076 +
        #1086#1075#1086#1074#1086#1088' ('#1064#1080#1092#1088#1072')'
      FieldName = 'ZDRUZUVANJE_OPERATORI'
    end
    object tblTenderzdruzuvanjeOperatoriNaziv: TFIBStringField
      DisplayLabel = 
        #1047#1076#1088#1091#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1088#1091#1087#1072' '#1086#1087#1077#1088#1072#1090#1086#1088#1080' '#1074#1086' '#1087#1088#1072#1074#1085#1072' '#1092#1086#1088#1084#1072' '#1079#1072' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076 +
        #1086#1075#1086#1074#1086#1088
      FieldName = 'zdruzuvanjeOperatoriNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderSPOSOBNOST: TFIBBlobField
      FieldName = 'SPOSOBNOST'
      Size = 8
    end
    object tblTenderSKRATENI_ROKOVI: TFIBSmallIntField
      DisplayLabel = 
        #1055#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1089#1082#1088#1072#1090#1077#1085#1080' '#1088#1086#1082#1086#1074#1080' (' +
        #1064#1080#1092#1088#1072')'
      FieldName = 'SKRATENI_ROKOVI'
    end
    object tblTenderskratenNaziv: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1089#1082#1088#1072#1090#1077#1085#1080' '#1088#1086#1082#1086#1074#1080
      FieldName = 'skratenNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderEL_SREDSTVA: TFIBSmallIntField
      DisplayLabel = 
        #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1080' '#1089#1088#1077#1076#1089#1090#1074#1072'? ('#1064 +
        #1080#1092#1088#1072') '
      FieldName = 'EL_SREDSTVA'
    end
    object tblTenderelSredstvaNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1080' '#1089#1088#1077#1076#1089#1090#1074#1072'? '
      FieldName = 'elSredstvaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderEL_AUKCIJA: TFIBSmallIntField
      DisplayLabel = #1044#1072#1083#1080' '#1116#1077' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1080' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'? ('#1064#1080#1092#1088#1072') '
      FieldName = 'EL_AUKCIJA'
    end
    object tblTenderelAukcijaNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1116#1077' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1080' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'?'
      FieldName = 'elAukcijaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderINFO_AUKCIJA: TFIBBlobField
      DisplayLabel = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1074#1086' '#1074#1088#1089#1082#1072' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072#1090#1072' '#1072#1091#1082#1094#1080#1112#1072
      FieldName = 'INFO_AUKCIJA'
      Size = 8
    end
    object tblTenderCENA_DOKUMENTACIJA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1085#1072' '#1090#1077#1085#1076#1077#1088#1089#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      FieldName = 'CENA_DOKUMENTACIJA'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderINFO_PLAKJANJE: TFIBBlobField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1080' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'INFO_PLAKJANJE'
      Size = 8
    end
    object tblTenderDENARSKA: TFIBStringField
      DisplayLabel = #1044#1077#1085#1072#1088#1089#1082#1072' '#1089#1084#1077#1090#1082#1072
      FieldName = 'DENARSKA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDEVIZNA: TFIBStringField
      DisplayLabel = #1044#1077#1074#1080#1079#1085#1072' '#1089#1084#1077#1090#1082#1072
      FieldName = 'DEVIZNA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderVALIDNOST_PONUDA: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1072#1090#1072' '#1084#1086#1088#1072' '#1076#1072' '#1073#1080#1076#1077' '#1074#1072#1083#1080#1076#1085#1072' '#1084#1080#1085#1080#1084#1091#1084
      FieldName = 'VALIDNOST_PONUDA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDOPOLNITELNI_INFO: TFIBBlobField
      DisplayLabel = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '
      FieldName = 'DOPOLNITELNI_INFO'
      Size = 8
    end
    object tblTenderVREMETRAENJE_DOG: TFIBStringField
      DisplayLabel = #1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090'/'#1088#1072#1084#1082'. '#1089#1087#1086#1075#1086#1076#1073#1072
      FieldName = 'VREMETRAENJE_DOG'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderODLUKA_PROSIRUVANJE: TFIBStringField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090' - '#1041#1088#1086#1112
      FieldName = 'ODLUKA_PROSIRUVANJE'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDATUM_ODLUKA_PROSIRUVANJE: TFIBDateField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090' - '#1044#1072#1090#1091#1084
      FieldName = 'DATUM_ODLUKA_PROSIRUVANJE'
    end
    object tblTenderIZNOS_PROSIRUVANJE: TFIBBCDField
      DisplayLabel = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090' - '#1048#1079#1085#1086#1089
      FieldName = 'IZNOS_PROSIRUVANJE'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderIZVOR_SREDSTVA: TFIBStringField
      DisplayLabel = #1048#1079#1074#1086#1088' '#1085#1072' '#1089#1088#1077#1076#1089#1090#1074#1072' - '#1053#1072#1079#1080#1074
      FieldName = 'IZVOR_SREDSTVA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderMIN_PONUDI: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1096#1090#1086' '#1116#1077' '#1073#1080#1076#1072#1090' '#1087#1086#1082#1072#1085#1077#1090#1080' - '#1052#1080#1085#1080#1084#1091#1084
      FieldName = 'MIN_PONUDI'
    end
    object tblTenderMAKS_PONUDI: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1096#1090#1086' '#1116#1077' '#1073#1080#1076#1072#1090' '#1087#1086#1082#1072#1085#1077#1090#1080' - '#1052#1072#1082#1089#1080#1084#1091#1084
      FieldName = 'MAKS_PONUDI'
    end
    object tblTenderTI_TS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089' - '#1079#1072' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'TI_TS_INS'
    end
    object tblTenderTI_TS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089' - '#1079#1072' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'TI_TS_UPD'
    end
    object tblTenderTI_USR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090' - '#1079#1072' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'TI_USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderTI_USR_UPD: TFIBStringField
      DisplayLabel = 
        #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090' - '#1079#1072' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1087#1086#1076#1072 +
        #1090#1086#1094#1080
      FieldName = 'TI_USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderTIP_GOD_PLAN: TFIBSmallIntField
      FieldName = 'TIP_GOD_PLAN'
    end
    object tblTendertip_god_plan_Naziv: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      FieldName = 'tip_god_plan_Naziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderSEKTORSKI_DOGOVOR: TFIBSmallIntField
      DisplayLabel = 
        #1044#1072#1083#1080' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1086#1076' '#1086#1087#1092#1072#1090#1077#1085#1080#1090#1077' '#1076#1077#1112#1085#1086#1089#1090#1080' ('#1089#1077#1082#1090#1086#1088#1089#1082#1080' '#1076#1086#1075#1086#1074#1086#1088')? ('#1064#1080 +
        #1092#1088#1072')'
      FieldName = 'SEKTORSKI_DOGOVOR'
    end
    object tblTendersektorski_dogovorTeloNaziv: TFIBStringField
      DisplayLabel = #1044#1072#1083#1080' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1086#1076' '#1086#1087#1092#1072#1090#1077#1085#1080#1090#1077' '#1076#1077#1112#1085#1086#1089#1090#1080' ('#1089#1077#1082#1090#1086#1088#1089#1082#1080' '#1076#1086#1075#1086#1074#1086#1088')?'
      FieldName = 'sektorski_dogovorTeloNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderLOKACIJA_ISPORAKA: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072'/'#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077
      FieldName = 'LOKACIJA_ISPORAKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDATUM_OD: TFIBDateField
      DisplayLabel = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1087#1088#1080#1084#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080' - '#1044#1072#1090#1091#1084' '#1086#1076
      FieldName = 'DATUM_OD'
    end
    object tblTenderDATUM_DO: TFIBDateTimeField
      DisplayLabel = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1087#1088#1080#1084#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080' - '#1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblTenderKONTAKT_TEL_FAKS: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1072#1082#1090' '#1058#1077#1083'/'#1060#1072#1082#1089
      FieldName = 'KONTAKT_TEL_FAKS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderZABELESKA_ZAPISNIK: TFIBMemoField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
      FieldName = 'ZABELESKA_ZAPISNIK'
      BlobType = ftMemo
      Size = 8
    end
    object tblTenderOBRAZLOZENIE_POSTAPKA: TFIBBlobField
      DisplayLabel = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1082#1086#1088#1080#1089#1090#1077#1085#1072#1090#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'OBRAZLOZENIE_POSTAPKA'
      Size = 8
    end
    object tblTendergarancijaIzjavaNaziv: TFIBStringField
      DisplayLabel = #1048#1079#1112#1072#1074#1072' '#1079#1072' '#1089#1077#1088#1080#1086#1079#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
      FieldName = 'garancijaIzjavaNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPONISTEN_PRICINA: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'PONISTEN_PRICINA'
    end
    object fbstrngfldTenderPRICINAPONISTENNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077
      FieldName = 'PRICINAPONISTENNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderSTATUS: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'STATUS'
    end
    object tblTenderIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' ('#1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042')'
      FieldName = 'IZNOS_SO_DDV'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderZATVORI_PONUDI_STATUS: TFIBSmallIntField
      DisplayLabel = #1047#1072#1090#1074#1086#1088#1080' '#1087#1086#1085#1091#1076#1080' - '#1089#1090#1072#1090#1091#1089
      FieldName = 'ZATVORI_PONUDI_STATUS'
    end
    object tblTenderZATVORI_PONUDI_TIME: TFIBDateTimeField
      DisplayLabel = #1047#1072#1090#1074#1086#1088#1080' '#1087#1086#1085#1091#1076#1080' - '#1074#1088#1077#1084#1077
      FieldName = 'ZATVORI_PONUDI_TIME'
    end
    object tblTenderZATVORI_PONUDI_USER: TFIBStringField
      DisplayLabel = #1047#1072#1090#1074#1086#1088#1080' '#1087#1086#1085#1091#1076#1080' - '#1082#1086#1088#1080#1089#1085#1080#1082
      FieldName = 'ZATVORI_PONUDI_USER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderBROJ_PART1: TFIBSmallIntField
      DisplayLabel = #1041#1088#1086#1112' Part 1'
      FieldName = 'BROJ_PART1'
    end
  end
  object dsТender: TDataSource
    DataSet = tblTender
    Left = 744
    Top = 40
  end
  object tblTenderInfo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_INFO'
      'SET '
      '    DATUM_DO = :DATUM_DO,'
      '    DATUM_OD = :DATUM_OD,'
      '    ZALBA = :ZALBA,'
      '    ZALBA_OPIS = :ZALBA_OPIS,'
      '    BROJ_ZALBI = :BROJ_ZALBI,'
      '    ODLUKA_DATUM = :ODLUKA_DATUM,'
      '    ODLUKA_BROJ = :ODLUKA_BROJ,'
      '    DATUM_OTVARANJE = :DATUM_OTVARANJE,'
      '    OSTETENI_KOVERTI = :OSTETENI_KOVERTI,'
      '    ODL_NABAVKA_BR = :ODL_NABAVKA_BR,'
      '    ODL_NABAVKA_DAT = :ODL_NABAVKA_DAT,'
      '    SOGLASNOST_BR = :SOGLASNOST_BR,'
      '    SOGLASNOST_DAT = :SOGLASNOST_DAT,'
      '    ODL_POKANA_BR = :ODL_POKANA_BR,'
      '    ODL_POKANA_DAT = :ODL_POKANA_DAT,'
      '    BROJ_POKANI = :BROJ_POKANI,'
      '    POTVRDA_BR = :POTVRDA_BR,'
      '    POTVRDA_DAT = :POTVRDA_DAT,'
      '    POTVRDA_IZNOS = :POTVRDA_IZNOS,'
      '    ANEKS_BROJ = :ANEKS_BROJ,'
      '    ANEKS_DATUM = :ANEKS_DATUM,'
      '    ANEKS_IZNOS = :ANEKS_IZNOS,'
      '    BODIRAL = :BODIRAL,'
      '    VREME_BOD = :VREME_BOD,'
      '    VREME_OTVARANJE = :VREME_OTVARANJE,'
      '    VKUPNO_PONUDI = :VKUPNO_PONUDI,'
      '    ZA_POVLEKUVANJE = :ZA_POVLEKUVANJE,'
      '    ZA_IZMENA = :ZA_IZMENA,'
      '    BR_ZADOCNETI_PONUDI = :BR_ZADOCNETI_PONUDI,'
      '    DATUM_ZAPISNIK = :DATUM_ZAPISNIK,'
      '    MESTO_OTVARANJE = :MESTO_OTVARANJE,'
      '    CENTRALNO_TELO = :CENTRALNO_TELO,'
      '    GRUPNA_NABAVKA = :GRUPNA_NABAVKA,'
      '    RAMKOVNA_SPOG = :RAMKOVNA_SPOG,'
      '    RS_POVEKE_ORGANI = :RS_POVEKE_ORGANI,'
      '    RS_POVEKE_OPERATORI = :RS_POVEKE_OPERATORI,'
      '    ALTERNATIVNI_PONUDI = :ALTERNATIVNI_PONUDI,'
      '    GARANCIJA_PONUDA = :GARANCIJA_PONUDA,'
      '    GARANCIJA_KVALITET = :GARANCIJA_KVALITET,'
      '    AVANSNO = :AVANSNO,'
      '    ZDRUZUVANJE_OPERATORI = :ZDRUZUVANJE_OPERATORI,'
      '    SPOSOBNOST = :SPOSOBNOST,'
      '    SKRATENI_ROKOVI = :SKRATENI_ROKOVI,'
      '    EL_SREDSTVA = :EL_SREDSTVA,'
      '    EL_AUKCIJA = :EL_AUKCIJA,'
      '    INFO_AUKCIJA = :INFO_AUKCIJA,'
      '    CENA_DOKUMENTACIJA = :CENA_DOKUMENTACIJA,'
      '    INFO_PLAKJANJE = :INFO_PLAKJANJE,'
      '    DENARSKA = :DENARSKA,'
      '    DEVIZNA = :DEVIZNA,'
      '    VALIDNOST_PONUDA = :VALIDNOST_PONUDA,'
      '    DOPOLNITELNI_INFO = :DOPOLNITELNI_INFO,'
      '    VREMETRAENJE_DOG = :VREMETRAENJE_DOG,'
      '    LOKACIJA_ISPORAKA = :LOKACIJA_ISPORAKA,'
      '    ODLUKA_PROSIRUVANJE = :ODLUKA_PROSIRUVANJE,'
      '    DATUM_ODLUKA_PROSIRUVANJE = :DATUM_ODLUKA_PROSIRUVANJE,'
      '    IZNOS_PROSIRUVANJE = :IZNOS_PROSIRUVANJE,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    IZVOR_SREDSTVA = :IZVOR_SREDSTVA,'
      '    MAKS_PONUDI = :MAKS_PONUDI,'
      '    MIN_PONUDI = :MIN_PONUDI,'
      '    SEKTORSKI_DOGOVOR = :SEKTORSKI_DOGOVOR,'
      '    ZABELESKA_ZAPISNIK = :ZABELESKA_ZAPISNIK,'
      '    GARANCIJA_IZJAVA = :GARANCIJA_IZJAVA,'
      '    ARH_BR_ISP = :ARH_BR_ISP,'
      '    ARH_DATUM_ISP = :ARH_DATUM_ISP'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_INFO'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_INFO('
      '    BROJ,'
      '    DATUM_DO,'
      '    DATUM_OD,'
      '    ZALBA,'
      '    ZALBA_OPIS,'
      '    BROJ_ZALBI,'
      '    ODLUKA_DATUM,'
      '    ODLUKA_BROJ,'
      '    DATUM_OTVARANJE,'
      '    OSTETENI_KOVERTI,'
      '    ODL_NABAVKA_BR,'
      '    ODL_NABAVKA_DAT,'
      '    SOGLASNOST_BR,'
      '    SOGLASNOST_DAT,'
      '    ODL_POKANA_BR,'
      '    ODL_POKANA_DAT,'
      '    BROJ_POKANI,'
      '    POTVRDA_BR,'
      '    POTVRDA_DAT,'
      '    POTVRDA_IZNOS,'
      '    ANEKS_BROJ,'
      '    ANEKS_DATUM,'
      '    ANEKS_IZNOS,'
      '    BODIRAL,'
      '    VREME_BOD,'
      '    VREME_OTVARANJE,'
      '    VKUPNO_PONUDI,'
      '    ZA_POVLEKUVANJE,'
      '    ZA_IZMENA,'
      '    BR_ZADOCNETI_PONUDI,'
      '    DATUM_ZAPISNIK,'
      '    MESTO_OTVARANJE,'
      '    CENTRALNO_TELO,'
      '    GRUPNA_NABAVKA,'
      '    RAMKOVNA_SPOG,'
      '    RS_POVEKE_ORGANI,'
      '    RS_POVEKE_OPERATORI,'
      '    ALTERNATIVNI_PONUDI,'
      '    GARANCIJA_PONUDA,'
      '    GARANCIJA_KVALITET,'
      '    AVANSNO,'
      '    ZDRUZUVANJE_OPERATORI,'
      '    SPOSOBNOST,'
      '    SKRATENI_ROKOVI,'
      '    EL_SREDSTVA,'
      '    EL_AUKCIJA,'
      '    INFO_AUKCIJA,'
      '    CENA_DOKUMENTACIJA,'
      '    INFO_PLAKJANJE,'
      '    DENARSKA,'
      '    DEVIZNA,'
      '    VALIDNOST_PONUDA,'
      '    DOPOLNITELNI_INFO,'
      '    VREMETRAENJE_DOG,'
      '    LOKACIJA_ISPORAKA,'
      '    ODLUKA_PROSIRUVANJE,'
      '    DATUM_ODLUKA_PROSIRUVANJE,'
      '    IZNOS_PROSIRUVANJE,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    IZVOR_SREDSTVA,'
      '    MAKS_PONUDI,'
      '    MIN_PONUDI,'
      '    SEKTORSKI_DOGOVOR,'
      '    ZABELESKA_ZAPISNIK,'
      '    GARANCIJA_IZJAVA,'
      '    ARH_BR_ISP,'
      '    ARH_DATUM_ISP'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :DATUM_DO,'
      '    :DATUM_OD,'
      '    :ZALBA,'
      '    :ZALBA_OPIS,'
      '    :BROJ_ZALBI,'
      '    :ODLUKA_DATUM,'
      '    :ODLUKA_BROJ,'
      '    :DATUM_OTVARANJE,'
      '    :OSTETENI_KOVERTI,'
      '    :ODL_NABAVKA_BR,'
      '    :ODL_NABAVKA_DAT,'
      '    :SOGLASNOST_BR,'
      '    :SOGLASNOST_DAT,'
      '    :ODL_POKANA_BR,'
      '    :ODL_POKANA_DAT,'
      '    :BROJ_POKANI,'
      '    :POTVRDA_BR,'
      '    :POTVRDA_DAT,'
      '    :POTVRDA_IZNOS,'
      '    :ANEKS_BROJ,'
      '    :ANEKS_DATUM,'
      '    :ANEKS_IZNOS,'
      '    :BODIRAL,'
      '    :VREME_BOD,'
      '    :VREME_OTVARANJE,'
      '    :VKUPNO_PONUDI,'
      '    :ZA_POVLEKUVANJE,'
      '    :ZA_IZMENA,'
      '    :BR_ZADOCNETI_PONUDI,'
      '    :DATUM_ZAPISNIK,'
      '    :MESTO_OTVARANJE,'
      '    :CENTRALNO_TELO,'
      '    :GRUPNA_NABAVKA,'
      '    :RAMKOVNA_SPOG,'
      '    :RS_POVEKE_ORGANI,'
      '    :RS_POVEKE_OPERATORI,'
      '    :ALTERNATIVNI_PONUDI,'
      '    :GARANCIJA_PONUDA,'
      '    :GARANCIJA_KVALITET,'
      '    :AVANSNO,'
      '    :ZDRUZUVANJE_OPERATORI,'
      '    :SPOSOBNOST,'
      '    :SKRATENI_ROKOVI,'
      '    :EL_SREDSTVA,'
      '    :EL_AUKCIJA,'
      '    :INFO_AUKCIJA,'
      '    :CENA_DOKUMENTACIJA,'
      '    :INFO_PLAKJANJE,'
      '    :DENARSKA,'
      '    :DEVIZNA,'
      '    :VALIDNOST_PONUDA,'
      '    :DOPOLNITELNI_INFO,'
      '    :VREMETRAENJE_DOG,'
      '    :LOKACIJA_ISPORAKA,'
      '    :ODLUKA_PROSIRUVANJE,'
      '    :DATUM_ODLUKA_PROSIRUVANJE,'
      '    :IZNOS_PROSIRUVANJE,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :IZVOR_SREDSTVA,'
      '    :MAKS_PONUDI,'
      '    :MIN_PONUDI,'
      '    :SEKTORSKI_DOGOVOR,'
      '    :ZABELESKA_ZAPISNIK,'
      '    :GARANCIJA_IZJAVA,'
      '    :ARH_BR_ISP,'
      '    :ARH_DATUM_ISP'
      ')')
    RefreshSQL.Strings = (
      'select ti.broj,'
      '       ti.datum_do,'
      '       ti.datum_od,'
      '       ti.zalba,'
      '       ti.zalba_opis,'
      '       ti.broj_zalbi,'
      '       ti.odluka_datum,'
      '       ti.odluka_broj,'
      '       ti.datum_otvaranje,'
      '       ti.osteteni_koverti,'
      '       ti.odl_nabavka_br,'
      '       ti.odl_nabavka_dat,'
      '       ti.soglasnost_br,'
      '       ti.soglasnost_dat,'
      '       ti.odl_pokana_br,'
      '       ti.odl_pokana_dat,'
      '       ti.broj_pokani,'
      '       ti.potvrda_br,'
      '       ti.potvrda_dat,'
      '       ti.potvrda_iznos,'
      '       ti.aneks_broj,'
      '       ti.aneks_datum,'
      '       ti.aneks_iznos,'
      '       ti.bodiral,'
      '       ti.vreme_bod,'
      '       ti.vreme_otvaranje,'
      '       ti.vkupno_ponudi,'
      '       ti.za_povlekuvanje,'
      '       ti.za_izmena,'
      '       ti.br_zadocneti_ponudi,'
      '       ti.datum_zapisnik,'
      '       ti.mesto_otvaranje,'
      '       ti.centralno_telo,'
      '       ti.grupna_nabavka,'
      '       ti.ramkovna_spog,'
      '       ti.rs_poveke_organi,'
      '       ti.rs_poveke_operatori,'
      '       ti.alternativni_ponudi,'
      '       ti.garancija_ponuda,'
      '       ti.garancija_kvalitet,'
      '       ti.avansno,'
      '       ti.zdruzuvanje_operatori,'
      '       ti.sposobnost,'
      '       ti.skrateni_rokovi,'
      '       ti.el_sredstva,'
      '       ti.el_aukcija,'
      '       ti.info_aukcija,'
      '       ti.cena_dokumentacija,'
      '       ti.info_plakjanje,'
      '       ti.denarska,'
      '       ti.devizna,'
      '       ti.validnost_ponuda,'
      '       ti.dopolnitelni_info,'
      '       ti.vremetraenje_dog,'
      '       ti.lokacija_isporaka,'
      '       ti.odluka_prosiruvanje,'
      '       ti.datum_odluka_prosiruvanje,'
      '       ti.iznos_prosiruvanje,'
      '       ti.ts_ins,'
      '       ti.ts_upd,'
      '       ti.usr_ins,'
      '       ti.usr_upd,'
      '       ti.izvor_sredstva,'
      '       ti.maks_ponudi,'
      '       ti.min_ponudi,'
      '       ti.sektorski_dogovor,'
      '       ti.zabeleska_zapisnik,'
      '       ti.garancija_izjava,'
      '       ti.arh_br_isp, '
      '       ti.arh_datum_isp'
      'from jn_tender_info ti'
      'where(  ti.broj = :mas_broj'
      '     ) and (     TI.BROJ = :OLD_BROJ'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ti.broj,'
      '       ti.datum_do,'
      '       ti.datum_od,'
      '       ti.zalba,'
      '       ti.zalba_opis,'
      '       ti.broj_zalbi,'
      '       ti.odluka_datum,'
      '       ti.odluka_broj,'
      '       ti.datum_otvaranje,'
      '       ti.osteteni_koverti,'
      '       ti.odl_nabavka_br,'
      '       ti.odl_nabavka_dat,'
      '       ti.soglasnost_br,'
      '       ti.soglasnost_dat,'
      '       ti.odl_pokana_br,'
      '       ti.odl_pokana_dat,'
      '       ti.broj_pokani,'
      '       ti.potvrda_br,'
      '       ti.potvrda_dat,'
      '       ti.potvrda_iznos,'
      '       ti.aneks_broj,'
      '       ti.aneks_datum,'
      '       ti.aneks_iznos,'
      '       ti.bodiral,'
      '       ti.vreme_bod,'
      '       ti.vreme_otvaranje,'
      '       ti.vkupno_ponudi,'
      '       ti.za_povlekuvanje,'
      '       ti.za_izmena,'
      '       ti.br_zadocneti_ponudi,'
      '       ti.datum_zapisnik,'
      '       ti.mesto_otvaranje,'
      '       ti.centralno_telo,'
      '       ti.grupna_nabavka,'
      '       ti.ramkovna_spog,'
      '       ti.rs_poveke_organi,'
      '       ti.rs_poveke_operatori,'
      '       ti.alternativni_ponudi,'
      '       ti.garancija_ponuda,'
      '       ti.garancija_kvalitet,'
      '       ti.avansno,'
      '       ti.zdruzuvanje_operatori,'
      '       ti.sposobnost,'
      '       ti.skrateni_rokovi,'
      '       ti.el_sredstva,'
      '       ti.el_aukcija,'
      '       ti.info_aukcija,'
      '       ti.cena_dokumentacija,'
      '       ti.info_plakjanje,'
      '       ti.denarska,'
      '       ti.devizna,'
      '       ti.validnost_ponuda,'
      '       ti.dopolnitelni_info,'
      '       ti.vremetraenje_dog,'
      '       ti.lokacija_isporaka,'
      '       ti.odluka_prosiruvanje,'
      '       ti.datum_odluka_prosiruvanje,'
      '       ti.iznos_prosiruvanje,'
      '       ti.ts_ins,'
      '       ti.ts_upd,'
      '       ti.usr_ins,'
      '       ti.usr_upd,'
      '       ti.izvor_sredstva,'
      '       ti.maks_ponudi,'
      '       ti.min_ponudi,'
      '       ti.sektorski_dogovor,'
      '       ti.zabeleska_zapisnik,'
      '       ti.garancija_izjava,'
      '       ti.arh_br_isp, '
      '       ti.arh_datum_isp'
      'from jn_tender_info ti'
      'where ti.broj = :mas_broj')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 624
    Top = 96
    object tblTenderInfoBROJ: TFIBStringField
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoZALBA: TFIBIntegerField
      FieldName = 'ZALBA'
    end
    object tblTenderInfoZALBA_OPIS: TFIBStringField
      FieldName = 'ZALBA_OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoBROJ_ZALBI: TFIBSmallIntField
      FieldName = 'BROJ_ZALBI'
    end
    object tblTenderInfoODLUKA_DATUM: TFIBDateField
      FieldName = 'ODLUKA_DATUM'
    end
    object tblTenderInfoODLUKA_BROJ: TFIBStringField
      FieldName = 'ODLUKA_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoDATUM_OTVARANJE: TFIBDateField
      FieldName = 'DATUM_OTVARANJE'
    end
    object tblTenderInfoOSTETENI_KOVERTI: TFIBSmallIntField
      FieldName = 'OSTETENI_KOVERTI'
    end
    object tblTenderInfoODL_NABAVKA_BR: TFIBStringField
      FieldName = 'ODL_NABAVKA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoODL_NABAVKA_DAT: TFIBDateField
      FieldName = 'ODL_NABAVKA_DAT'
    end
    object tblTenderInfoSOGLASNOST_BR: TFIBStringField
      FieldName = 'SOGLASNOST_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoSOGLASNOST_DAT: TFIBDateField
      FieldName = 'SOGLASNOST_DAT'
    end
    object tblTenderInfoODL_POKANA_BR: TFIBStringField
      FieldName = 'ODL_POKANA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoODL_POKANA_DAT: TFIBDateField
      FieldName = 'ODL_POKANA_DAT'
    end
    object tblTenderInfoBROJ_POKANI: TFIBSmallIntField
      FieldName = 'BROJ_POKANI'
    end
    object tblTenderInfoPOTVRDA_BR: TFIBStringField
      FieldName = 'POTVRDA_BR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoPOTVRDA_DAT: TFIBDateField
      FieldName = 'POTVRDA_DAT'
    end
    object tblTenderInfoPOTVRDA_IZNOS: TFIBFloatField
      FieldName = 'POTVRDA_IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderInfoANEKS_BROJ: TFIBStringField
      FieldName = 'ANEKS_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoANEKS_DATUM: TFIBDateField
      FieldName = 'ANEKS_DATUM'
    end
    object tblTenderInfoANEKS_IZNOS: TFIBFloatField
      FieldName = 'ANEKS_IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderInfoBODIRAL: TFIBStringField
      FieldName = 'BODIRAL'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoVREME_BOD: TFIBDateTimeField
      FieldName = 'VREME_BOD'
    end
    object tblTenderInfoVREME_OTVARANJE: TFIBTimeField
      FieldName = 'VREME_OTVARANJE'
    end
    object tblTenderInfoVKUPNO_PONUDI: TFIBSmallIntField
      FieldName = 'VKUPNO_PONUDI'
    end
    object tblTenderInfoZA_POVLEKUVANJE: TFIBBlobField
      FieldName = 'ZA_POVLEKUVANJE'
      Size = 8
    end
    object tblTenderInfoZA_IZMENA: TFIBBlobField
      FieldName = 'ZA_IZMENA'
      Size = 8
    end
    object tblTenderInfoBR_ZADOCNETI_PONUDI: TFIBSmallIntField
      FieldName = 'BR_ZADOCNETI_PONUDI'
    end
    object tblTenderInfoDATUM_ZAPISNIK: TFIBDateField
      FieldName = 'DATUM_ZAPISNIK'
    end
    object tblTenderInfoMESTO_OTVARANJE: TFIBStringField
      FieldName = 'MESTO_OTVARANJE'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoCENTRALNO_TELO: TFIBSmallIntField
      FieldName = 'CENTRALNO_TELO'
    end
    object tblTenderInfoGRUPNA_NABAVKA: TFIBSmallIntField
      FieldName = 'GRUPNA_NABAVKA'
    end
    object tblTenderInfoRAMKOVNA_SPOG: TFIBSmallIntField
      FieldName = 'RAMKOVNA_SPOG'
    end
    object tblTenderInfoRS_POVEKE_ORGANI: TFIBSmallIntField
      FieldName = 'RS_POVEKE_ORGANI'
    end
    object tblTenderInfoRS_POVEKE_OPERATORI: TFIBSmallIntField
      FieldName = 'RS_POVEKE_OPERATORI'
    end
    object tblTenderInfoALTERNATIVNI_PONUDI: TFIBSmallIntField
      FieldName = 'ALTERNATIVNI_PONUDI'
    end
    object tblTenderInfoGARANCIJA_PONUDA: TFIBSmallIntField
      FieldName = 'GARANCIJA_PONUDA'
    end
    object tblTenderInfoGARANCIJA_KVALITET: TFIBSmallIntField
      FieldName = 'GARANCIJA_KVALITET'
    end
    object tblTenderInfoAVANSNO: TFIBSmallIntField
      FieldName = 'AVANSNO'
    end
    object tblTenderInfoZDRUZUVANJE_OPERATORI: TFIBSmallIntField
      FieldName = 'ZDRUZUVANJE_OPERATORI'
    end
    object tblTenderInfoSPOSOBNOST: TFIBBlobField
      FieldName = 'SPOSOBNOST'
      Size = 8
    end
    object tblTenderInfoSKRATENI_ROKOVI: TFIBSmallIntField
      FieldName = 'SKRATENI_ROKOVI'
    end
    object tblTenderInfoEL_SREDSTVA: TFIBSmallIntField
      FieldName = 'EL_SREDSTVA'
    end
    object tblTenderInfoEL_AUKCIJA: TFIBSmallIntField
      FieldName = 'EL_AUKCIJA'
    end
    object tblTenderInfoINFO_AUKCIJA: TFIBBlobField
      FieldName = 'INFO_AUKCIJA'
      Size = 8
    end
    object tblTenderInfoCENA_DOKUMENTACIJA: TFIBFloatField
      FieldName = 'CENA_DOKUMENTACIJA'
      DisplayFormat = '0.00 , .'
    end
    object tblTenderInfoINFO_PLAKJANJE: TFIBBlobField
      FieldName = 'INFO_PLAKJANJE'
      Size = 8
    end
    object tblTenderInfoDENARSKA: TFIBStringField
      FieldName = 'DENARSKA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoDEVIZNA: TFIBStringField
      FieldName = 'DEVIZNA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoVALIDNOST_PONUDA: TFIBStringField
      FieldName = 'VALIDNOST_PONUDA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoDOPOLNITELNI_INFO: TFIBBlobField
      FieldName = 'DOPOLNITELNI_INFO'
      Size = 8
    end
    object tblTenderInfoVREMETRAENJE_DOG: TFIBStringField
      FieldName = 'VREMETRAENJE_DOG'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoODLUKA_PROSIRUVANJE: TFIBStringField
      FieldName = 'ODLUKA_PROSIRUVANJE'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoDATUM_ODLUKA_PROSIRUVANJE: TFIBDateField
      FieldName = 'DATUM_ODLUKA_PROSIRUVANJE'
    end
    object tblTenderInfoIZNOS_PROSIRUVANJE: TFIBBCDField
      FieldName = 'IZNOS_PROSIRUVANJE'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderInfoTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblTenderInfoTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblTenderInfoUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoIZVOR_SREDSTVA: TFIBStringField
      FieldName = 'IZVOR_SREDSTVA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoMAKS_PONUDI: TFIBSmallIntField
      FieldName = 'MAKS_PONUDI'
    end
    object tblTenderInfoMIN_PONUDI: TFIBSmallIntField
      FieldName = 'MIN_PONUDI'
    end
    object tblTenderInfoSEKTORSKI_DOGOVOR: TFIBSmallIntField
      FieldName = 'SEKTORSKI_DOGOVOR'
    end
    object tblTenderInfoLOKACIJA_ISPORAKA: TFIBStringField
      FieldName = 'LOKACIJA_ISPORAKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoDATUM_DO: TFIBDateTimeField
      FieldName = 'DATUM_DO'
    end
    object tblTenderInfoDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblTenderInfoZABELESKA_ZAPISNIK: TFIBMemoField
      FieldName = 'ZABELESKA_ZAPISNIK'
      BlobType = ftMemo
      Size = 8
    end
    object tblTenderInfoGARANCIJA_IZJAVA: TFIBSmallIntField
      FieldName = 'GARANCIJA_IZJAVA'
    end
    object tblTenderInfoARH_BR_ISP: TFIBStringField
      DisplayLabel = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'ARH_BR_ISP'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderInfoARH_DATUM_ISP: TFIBDateField
      DisplayLabel = #1040#1088#1093#1080#1074#1089#1082#1080' '#1076#1072#1090#1091#1084' '#1085#1072' '#1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'ARH_DATUM_ISP'
    end
  end
  object dsTenderInfo: TDataSource
    DataSet = tblTenderInfo
    Left = 744
    Top = 96
  end
  object tblTenderGenBodiranje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_GEN_BODIRANJE'
      'SET '
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    BODIRANJE_PO = :BODIRANJE_PO,'
      '    MAX_BODOVI = :MAX_BODOVI,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and BODIRANJE_PO = :OLD_BODIRANJE_PO'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_GEN_BODIRANJE'
      'WHERE'
      '        BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and BODIRANJE_PO = :OLD_BODIRANJE_PO'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_GEN_BODIRANJE('
      '    BROJ_TENDER,'
      '    BODIRANJE_PO,'
      '    MAX_BODOVI,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :BODIRANJE_PO,'
      '    :MAX_BODOVI,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select tgb.broj_tender,'
      '       t.godina as tenderGodina,'
      '       tgb.bodiranje_po,'
      '       k.naziv as kriteriumNaziv,'
      '       k.vrednuvanje as kriteriumVrednuvanje,'
      '       k.bodiranje as kriteriumBodiranje,'
      '       tgb.max_bodovi,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       tgb.ts_ins,'
      '       tgb.ts_upd,'
      '       tgb.usr_ins,'
      '       tgb.usr_upd'
      'from  jn_tender_gen_bodiranje tgb'
      'inner join jn_tender t on t.broj = tgb.broj_tender'
      'inner join jn_kriteriumi k on k.sifra = tgb.bodiranje_po'
      'where(  tgb.broj_tender = :mas_broj'
      '     ) and (     TGB.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and TGB.BODIRANJE_PO = :OLD_BODIRANJE_PO'
      '     )'
      '    '
      'order by kriteriumNaziv')
    SelectSQL.Strings = (
      'select tgb.broj_tender,'
      '       t.godina as tenderGodina,'
      '       tgb.bodiranje_po,'
      '       k.naziv as kriteriumNaziv,'
      '       k.vrednuvanje as kriteriumVrednuvanje,'
      '       k.bodiranje as kriteriumBodiranje,'
      '       tgb.max_bodovi,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       tgb.ts_ins,'
      '       tgb.ts_upd,'
      '       tgb.usr_ins,'
      '       tgb.usr_upd'
      'from  jn_tender_gen_bodiranje tgb'
      'inner join jn_tender t on t.broj = tgb.broj_tender'
      'inner join jn_kriteriumi k on k.sifra = tgb.bodiranje_po'
      'where tgb.broj_tender = :mas_broj'
      'order by kriteriumNaziv')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 624
    Top = 152
    object tblTenderGenBodiranjeBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjeTENDERGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'TENDERGODINA'
    end
    object tblTenderGenBodiranjeBODIRANJE_PO: TFIBStringField
      DisplayLabel = #1050#1088#1080#1090#1077#1088#1080#1091#1084' ('#1064#1080#1092#1088#1072')'
      FieldName = 'BODIRANJE_PO'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjeKRITERIUMNAZIV: TFIBStringField
      DisplayLabel = #1050#1088#1080#1090#1077#1088#1080#1091#1084
      FieldName = 'KRITERIUMNAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjeKRITERIUMVREDNUVANJE: TFIBIntegerField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'KRITERIUMVREDNUVANJE'
    end
    object tblTenderGenBodiranjeKRITERIUMBODIRANJE: TFIBIntegerField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' ('#1064#1080#1092#1088#1072')'
      FieldName = 'KRITERIUMBODIRANJE'
    end
    object tblTenderGenBodiranjeMAX_BODOVI: TFIBBCDField
      DisplayLabel = #1052#1072#1082#1089#1080#1084#1091#1084' '#1073#1086#1076#1086#1074#1080
      FieldName = 'MAX_BODOVI'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderGenBodiranjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTenderGenBodiranjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTenderGenBodiranjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjevrednuvanjeNaziv: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'vrednuvanjeNaziv'
      Size = 38
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderGenBodiranjebodiranjeNaziv: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077
      FieldName = 'bodiranjeNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTenderGenBodiranje: TDataSource
    DataSet = tblTenderGenBodiranje
    Left = 744
    Top = 152
  end
  object tblIzborStavkiTenderOdGodPlan: TpFIBDataSet
    SelectSQL.Strings = (
      'select  gps.godina,'
      '        gps.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        mav.naziv as vidArtikalNaziv,'
      '        gps.artikal,'
      '        ma.naziv as artikalNaziv,'
      '        ma.merka,'
      '        ma.grupa,'
      '        mag.naziv as grupaNaziv,'
      '        ma.jn_cpv,'
      '        sum(gps.kolicina) as SumaKolicina,'
      '        ma.generika'
      'from jn_godisen_plan_sektori gps'
      
        'inner join mtr_artikal ma on ma.id = gps.artikal and ma.artvid =' +
        ' gps.vid_artikal'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      
        'left join jn_tender_stavki ts on ts.vid_artikal = gps.vid_artika' +
        'l and ts.artikal = gps.artikal and ts.broj_tender=:mas_broj'
      'where gps.godina = :godina and ts.artikal is null '
      
        'group by  godina, vid_artikal, jn_tip_artikal, vidArtikalNaziv, ' +
        'artikal, artikalNaziv, merka, grupa, grupaNaziv, jn_cpv, generik' +
        'a'
      ''
      'order by ma.jn_tip_artikal, ma.naziv'
      ''
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 936
    Top = 152
    object tblIzborStavkiTenderOdGodPlanGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblIzborStavkiTenderOdGodPlanVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblIzborStavkiTenderOdGodPlanVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblIzborStavkiTenderOdGodPlanARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdGodPlanSUMAKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'SUMAKOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiTenderOdGodPlanGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiTenderOdGodPlanJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
  end
  object dsIzborStavkiTenderOdGodPlan: TDataSource
    DataSet = tblIzborStavkiTenderOdGodPlan
    Left = 1096
    Top = 152
  end
  object tblVnesStavkiTenderOdGodPlan: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_GODISEN_PLAN_SEKTORI'
      'SET '
      ''
      '    KOLICINA = :KOLICINA'
      ' '
      'WHERE'
      '    ID = :OLD_ID')
    SelectSQL.Strings = (
      
        'select re, renaziv, artikal, vid_artikal, planiranakolicina, obj' +
        'avena, (planiranakolicina - objavena) as kolicina'
      'from (select  gps.re,'
      '        mr.naziv as RENaziv,'
      '        gps.artikal,'
      '        gps.vid_artikal,'
      '        coalesce(sum(gps.kolicina),0) as planiranakolicina,'
      '        (select coalesce(sum(tsr.kolicina),0) as objavena'
      '         from jn_tender_stavki_re tsr'
      
        '         inner join jn_tender_stavki ts on ts.broj_tender = tsr.' +
        'broj_tender and ts.vid_artikal = tsr.vid_artikal and ts.artikal ' +
        '= tsr.artikal'
      '         inner join jn_tender t on t.broj = ts.broj_tender'
      
        '         where tsr.artikal = :artikal and tsr.vid_artikal = :vid' +
        '_artikal and tsr.re = gps.re and t.godina = :godina and ts.valid' +
        'nost = 1)'
      'from jn_godisen_plan_sektori gps'
      'inner join mat_re mr on mr.id = gps.re'
      
        'where gps.godina = :godina and gps.artikal = :artikal and gps.vi' +
        'd_artikal = :vid_artikal'
      'group by gps.re, mr.naziv, artikal, vid_artikal'
      'order by mr.naziv)'
      ''
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 936
    Top = 208
    object tblVnesStavkiTenderOdGodPlanRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'RE'
    end
    object tblVnesStavkiTenderOdGodPlanRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVnesStavkiTenderOdGodPlanVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblVnesStavkiTenderOdGodPlanPLANIRANAKOLICINA: TFIBBCDField
      DisplayLabel = #1055#1083#1072#1085#1080#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'PLANIRANAKOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblVnesStavkiTenderOdGodPlanOBJAVENA: TFIBBCDField
      DisplayLabel = #1054#1073#1112#1072#1074#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'OBJAVENA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblVnesStavkiTenderOdGodPlanKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      OnSetText = tblVnesStavkiTenderOdGodPlanKOLICINASetText
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblVnesStavkiTenderOdGodPlanARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
  end
  object dsVnesStavkiTenderOdGodPlan: TDataSource
    DataSet = tblVnesStavkiTenderOdGodPlan
    Left = 1096
    Top = 208
  end
  object tblTenderStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_STAVKI'
      'SET '
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    KOLICINA = :KOLICINA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    VALIDNOST = :VALIDNOST,'
      '    NAZIV= :NAZIV,'
      '    MERKA= :MERKA'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_STAVKI'
      'WHERE'
      '        BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_STAVKI('
      '    BROJ_TENDER,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    KOLICINA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    VALIDNOST,'
      '    NAZIV,'
      '    MERKA'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :KOLICINA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :VALIDNOST,'
      '    :NAZIV,'
      '    :MERKA'
      ')')
    RefreshSQL.Strings = (
      'select  ts.broj_tender,'
      '        ts.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ts.artikal,'
      '        ma.naziv as nazivArtikal,'
      '        ts.kolicina,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.generika,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ts.ts_ins,'
      '        ts.ts_upd,'
      '        ts.usr_ins,'
      '        ts.usr_upd,'
      '        ts.validnost,'
      '        ts.NAZIV,'
      '        ts.MERKA'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      '    '
      'where(  ts.broj_tender = :mas_broj'
      '     ) and (     TS.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and TS.VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and TS.ARTIKAL = :OLD_ARTIKAL'
      '     )'
      '    '
      'order by ma.jn_tip_artikal, ma.naziv ')
    SelectSQL.Strings = (
      'select  ts.broj_tender,'
      '        ts.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ts.artikal,'
      '        ma.naziv as nazivArtikal,'
      '        ts.kolicina,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.generika,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ts.ts_ins,'
      '        ts.ts_upd,'
      '        ts.usr_ins,'
      '        ts.usr_upd,'
      '        ts.validnost,'
      '        ts.naziv,'
      '        ts.MERKA'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      'where ts.broj_tender = :mas_broj'
      'order by ma.jn_tip_artikal, ma.naziv')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 936
    Top = 40
    object tblTenderStavkiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblTenderStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblTenderStavkiNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTenderStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTenderStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblTenderStavkiPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblTenderStavkiJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblTenderStavkiVALIDNOST: TFIBIntegerField
      DisplayLabel = #1042#1072#1083#1080#1076#1085#1086#1089#1090
      FieldName = 'VALIDNOST'
    end
    object tblTenderStavkiNAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiMERKA1: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'MERKA1'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTenderStavki: TDataSource
    DataSet = tblTenderStavki
    Left = 1088
    Top = 40
  end
  object tblTenderStavkiRE: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_STAVKI_RE'
      'WHERE'
      '        BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and ARTIKAL = :OLD_ARTIKAL'
      '    and RE = :OLD_RE'
      '    ')
    SelectSQL.Strings = (
      ''
      ''
      'select tsr.broj_tender,'
      '       tsr.vid_artikal,'
      '       tsr.artikal,'
      '      tsr.re,'
      '      mr.naziv as nazivRE,'
      '       tsr.kolicina  , gs.cena, gs.cenabezddv,gs.danok'
      'from jn_tender_stavki_re tsr'
      'inner join jn_tender t on t.broj = tsr.broj_tender'
      
        'inner join jn_godisen_plan_sektori gs on gs.vid_artikal = tsr.vi' +
        'd_artikal and'
      
        '                                         gs.artikal = tsr.artika' +
        'l and'
      
        '                                         gs.godina = t.godina an' +
        'd'
      '                                         gs.re =tsr.re'
      'inner join mat_re mr on mr.id = tsr.re'
      'where tsr.broj_tender = :mas_broj')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 936
    Top = 96
    object tblTenderStavkiREBROJ_TENDER: TFIBStringField
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiREVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblTenderStavkiREARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblTenderStavkiREKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblTenderStavkiRERE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblTenderStavkiRENAZIVRE: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'NAZIVRE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderStavkiRECENA: TFIBFloatField
      FieldName = 'CENA'
      DisplayFormat = '0.0000 , .'
    end
    object tblTenderStavkiRECENABEZDDV: TFIBFloatField
      FieldName = 'CENABEZDDV'
      DisplayFormat = '0.0000 , .'
    end
    object tblTenderStavkiREDANOK: TFIBFloatField
      FieldName = 'DANOK'
    end
  end
  object dsTenderStavkiRE: TDataSource
    DataSet = tblTenderStavkiRE
    Left = 1096
    Top = 96
  end
  object tblIzborStavkiTenderOdSifArtikli: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    JN_TIP_ARTIKAL = :JN_TIP_ARTIKAL,'
      '    ARTVID = :ARTVID,'
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    GENERIKA = :GENERIKA,'
      '    MERKA = :MERKA,'
      '    GRUPA = :GRUPA,'
      '    JN_CPV = :JN_CPV,'
      '    AKTIVEN = :AKTIVEN'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select ma.jn_tip_artikal,'
      '       ma.artvid,'
      '       mv.naziv as vidNaziv,'
      '       ma.id,'
      '       ma.naziv,'
      '       ma.generika,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.cena,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNziv,'
      '       cast (0.00 as numeric(15,2)) as kolicina,'
      '       ma.tarifa,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       ma.aktiven,'
      '       case when ma.aktiven = 1 then '#39#1040#1082#1090#1080#1074#1077#1085#39
      '             when ma.aktiven = 0 then '#39#1053#1077#1072#1082#1090#1080#1074#1077#1085#39
      '       end "AktivenNaziv"'
      ''
      'from mtr_artikal ma'
      'inner join jn_tip_artikal mv on mv.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left join jn_tender_stavki ts on ts.vid_artikal=ma.artvid and ts' +
        '.artikal=ma.id and ts.broj_tender=:mas_broj'
      'where ts.artikal is null and ma.nabaven=1 '
      'order by ma.jn_tip_artikal, ma.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 936
    Top = 264
    object tblIzborStavkiTenderOdSifArtikliJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiTenderOdSifArtikliARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTVID'
    end
    object tblIzborStavkiTenderOdSifArtikliVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblIzborStavkiTenderOdSifArtikliNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiTenderOdSifArtikliMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' ('#1064#1080#1092#1088#1072')'
      FieldName = 'PROZVODITEL'
    end
    object tblIzborStavkiTenderOdSifArtikliPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliGRUPANZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiTenderOdSifArtikliJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiTenderOdSifArtikliAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085'/'#1053#1077#1072#1082#1090#1080#1074#1077#1085' '
      FieldName = 'AKTIVEN'
    end
    object tblIzborStavkiTenderOdSifArtikliCENA: TFIBBCDField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.0000 , .'
      Size = 2
    end
    object tblIzborStavkiTenderOdSifArtikliTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
      DisplayFormat = '0.00 , .'
    end
  end
  object dsIzborStavkiTenderOdSifArtikli: TDataSource
    DataSet = tblIzborStavkiTenderOdSifArtikli
    Left = 1096
    Top = 264
  end
  object tblTenderKomisija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_KOMISIJA'
      'SET '
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    CLEN = :CLEN,'
      '    FUNKCIJA = :FUNKCIJA,'
      '    REDBR = :REDBR,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    PRISUTEN = :PRISUTEN'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and CLEN = :OLD_CLEN'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_KOMISIJA'
      'WHERE'
      '        BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and CLEN = :OLD_CLEN'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_KOMISIJA('
      '    BROJ_TENDER,'
      '    CLEN,'
      '    FUNKCIJA,'
      '    REDBR,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    PRISUTEN'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :CLEN,'
      '    :FUNKCIJA,'
      '    :REDBR,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :PRISUTEN'
      ')')
    RefreshSQL.Strings = (
      'select tk.broj_tender,'
      '       tk.clen,'
      '       k.ime_prezime,'
      '       k.uloga,'
      '       tk.funkcija,'
      '       tk.redbr,'
      '       tk.ts_ins,'
      '       tk.ts_upd,'
      '       tk.usr_ins,'
      '       tk.usr_upd,'
      '       tk.prisuten'
      'from jn_tender_komisija tk'
      'inner join jn_komisija k on k.id = tk.clen'
      'where(  tk.broj_tender = :mas_broj'
      '     ) and (     TK.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and TK.CLEN = :OLD_CLEN'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select tk.broj_tender,'
      '       tk.clen,'
      '       k.ime_prezime,'
      '       k.uloga,'
      '       tk.funkcija,'
      '       tk.redbr,'
      '       tk.ts_ins,'
      '       tk.ts_upd,'
      '       tk.usr_ins,'
      '       tk.usr_upd,'
      '       tk.prisuten'
      'from jn_tender_komisija tk'
      'inner join jn_komisija k on k.id = tk.clen'
      'where tk.broj_tender = :mas_broj'
      'order by tk.redbr')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 624
    Top = 208
    object tblTenderKomisijaBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaCLEN: TFIBSmallIntField
      DisplayLabel = #1063#1083#1077#1085' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072' ('#1064#1080#1092#1088#1072')'
      FieldName = 'CLEN'
    end
    object tblTenderKomisijaIME_PREZIME: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1055#1088#1077#1079#1080#1084#1077
      FieldName = 'IME_PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaULOGA: TFIBStringField
      DisplayLabel = #1059#1083#1086#1075#1072
      FieldName = 'ULOGA'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaFUNKCIJA: TFIBStringField
      DisplayLabel = #1060#1091#1085#1082#1094#1080#1112#1072
      FieldName = 'FUNKCIJA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaREDBR: TFIBSmallIntField
      DisplayLabel = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
      FieldName = 'REDBR'
    end
    object tblTenderKomisijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTenderKomisijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTenderKomisijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderKomisijaPRISUTEN: TFIBSmallIntField
      DisplayLabel = #1055#1088#1080#1089#1091#1090#1077#1085
      FieldName = 'PRISUTEN'
    end
  end
  object dsTenderKomisija: TDataSource
    DataSet = tblTenderKomisija
    Left = 744
    Top = 208
  end
  object tblNedeliviGrupi: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_NEDELIVI_GRUPI'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and GRUPA = :OLD_GRUPA'
      '    ')
    RefreshSQL.Strings = (
      '  select ng.broj_tender,'
      '         ng.grupa,'
      '         ag.naziv as grupaNaziv,'
      '         ng.ts_ins,'
      '         ng.ts_upd,'
      '         ng.usr_ins,'
      '         ng.usr_upd'
      '  from jn_nedelivi_grupi ng'
      '  inner join mtr_artgrupa ag on ag.id = ng.grupa'
      '  where(  ng.broj_tender = :mas_broj'
      '     ) and (     NG.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and NG.GRUPA = :OLD_GRUPA'
      '     )'
      '    '
      '  ')
    SelectSQL.Strings = (
      '  select ng.broj_tender,'
      '         ng.grupa,'
      '         ag.naziv as grupaNaziv,'
      '         ng.ts_ins,'
      '         ng.ts_upd,'
      '         ng.usr_ins,'
      '         ng.usr_upd'
      '  from jn_nedelivi_grupi ng'
      '  inner join mtr_artgrupa ag on ag.id = ng.grupa'
      '  where ng.broj_tender = :mas_broj'
      '  order by ag.naziv')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 936
    Top = 320
    object tblNedeliviGrupiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNedeliviGrupiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNedeliviGrupiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNedeliviGrupiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblNedeliviGrupiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object tblNedeliviGrupiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNedeliviGrupiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNedeliviGrupi: TDataSource
    DataSet = tblNedeliviGrupi
    Left = 1096
    Top = 320
  end
  object tblIzborNedeliviGrupi: TpFIBDataSet
    RefreshSQL.Strings = (
      'select  distinct ma.grupa,'
      '                 mg.naziv as grupaNaziv'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.artvid = ts.vid_artikal and ma.i' +
        'd = ts.artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      
        'where ts.broj_tender = :mas_broj and  ma.grupa is not null and m' +
        'a.grupa not in (select ng.grupa from jn_nedelivi_grupi ng where ' +
        'ng.broj_tender = :mas_broj)'
      'order by ma.grupa')
    SelectSQL.Strings = (
      'select  distinct ma.grupa,'
      '                 mg.naziv as grupaNaziv'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.artvid = ts.vid_artikal and ma.i' +
        'd = ts.artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      
        'where ts.broj_tender = :mas_broj and  ma.grupa is not null and m' +
        'a.grupa not in (select ng.grupa from jn_nedelivi_grupi ng where ' +
        'ng.broj_tender = :mas_broj)'
      'order by ma.grupa')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    Left = 936
    Top = 376
    object tblIzborNedeliviGrupiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNedeliviGrupiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborNedeliviGrupi: TDataSource
    DataSet = tblIzborNedeliviGrupi
    Left = 1096
    Top = 376
  end
  object tblPonudi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDI'
      'SET '
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    DATUM_PONUDA = :DATUM_PONUDA,'
      '    KOMPLETNOST = :KOMPLETNOST,'
      '    ZABELESKA = :ZABELESKA,'
      '    GARANCII_POPUST = :GARANCII_POPUST,'
      '    DOKUMENTACIJA = :DOKUMENTACIJA,'
      '    PRETSTAVNIK = :PRETSTAVNIK,'
      '    ODBIENA = :ODBIENA,'
      '    BROJ = :BROJ,'
      '    BROJ_PONUDUVAC = :BROJ_PONUDUVAC,'
      '    PRIMENA_VO_ROK = :PRIMENA_VO_ROK,'
      '    ZATVOREN_KOVERT = :ZATVOREN_KOVERT,'
      '    OZNAKA_NE_OTVORAJ = :OZNAKA_NE_OTVORAJ,'
      '    POVLEKUVANJE_PONUDA = :POVLEKUVANJE_PONUDA,'
      '    IZMENA_PONUDA = :IZMENA_PONUDA,'
      '    ROK_VAZNOST = :ROK_VAZNOST,'
      '    VALUTA = :VALUTA,'
      '    GARANCIJA_IZNOS = :GARANCIJA_IZNOS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    GARANCIJA_DATUM = :GARANCIJA_DATUM,'
      '    ROK_FAKTURA = :ROK_FAKTURA,'
      '    ROK_ISPORAKA = :ROK_ISPORAKA,'
      '    GARANTEN_ROK = :GARANTEN_ROK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_PONUDI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_PONUDI('
      '    ID,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    BROJ_TENDER,'
      '    DATUM_PONUDA,'
      '    KOMPLETNOST,'
      '    ZABELESKA,'
      '    GARANCII_POPUST,'
      '    DOKUMENTACIJA,'
      '    PRETSTAVNIK,'
      '    ODBIENA,'
      '    BROJ,'
      '    BROJ_PONUDUVAC,'
      '    PRIMENA_VO_ROK,'
      '    ZATVOREN_KOVERT,'
      '    OZNAKA_NE_OTVORAJ,'
      '    POVLEKUVANJE_PONUDA,'
      '    IZMENA_PONUDA,'
      '    ROK_VAZNOST,'
      '    VALUTA,'
      '    GARANCIJA_IZNOS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    GARANCIJA_DATUM,'
      '    ROK_FAKTURA,'
      '    ROK_ISPORAKA,'
      '    GARANTEN_ROK'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :BROJ_TENDER,'
      '    :DATUM_PONUDA,'
      '    :KOMPLETNOST,'
      '    :ZABELESKA,'
      '    :GARANCII_POPUST,'
      '    :DOKUMENTACIJA,'
      '    :PRETSTAVNIK,'
      '    :ODBIENA,'
      '    :BROJ,'
      '    :BROJ_PONUDUVAC,'
      '    :PRIMENA_VO_ROK,'
      '    :ZATVOREN_KOVERT,'
      '    :OZNAKA_NE_OTVORAJ,'
      '    :POVLEKUVANJE_PONUDA,'
      '    :IZMENA_PONUDA,'
      '    :ROK_VAZNOST,'
      '    :VALUTA,'
      '    :GARANCIJA_IZNOS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :GARANCIJA_DATUM,'
      '    :ROK_FAKTURA,'
      '    :ROK_ISPORAKA,'
      '    :GARANTEN_ROK'
      ')')
    RefreshSQL.Strings = (
      'select jp.id,'
      '       jp.tip_partner,'
      '       jp.partner,'
      '       tp.naziv as tipPartenrNaziv,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten as partnerSkratenNaziv,'
      '       jp.broj_tender,'
      '       jp.datum_ponuda,'
      '       jp.kompletnost,'
      '       jp.zabeleska,'
      '       jp.garancii_popust,'
      '       jp.dokumentacija,'
      '       jp.pretstavnik,'
      '       jp.odbiena,'
      '       case when jp.odbiena = 1 then '#39#1054#1076#1073#1080#1077#1085#1072#39
      '            when jp.odbiena = 0 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '       end "odbienaNaziv",'
      '       jp.broj,'
      '       jp.broj_ponuduvac,'
      '       jp.primena_vo_rok,'
      '       jp.zatvoren_kovert,'
      '       jp.oznaka_ne_otvoraj,'
      '       jp.povlekuvanje_ponuda,'
      '       jp.izmena_ponuda,'
      '       jp.rok_vaznost,'
      '       jp.valuta,'
      '       jp.garancija_iznos,'
      '       jp.ts_ins,'
      '       jp.ts_upd,'
      '       jp.usr_ins,'
      '       jp.usr_upd,'
      '       jp.garancija_datum,'
      '       jp.rok_faktura,'
      '       jp.rok_isporaka,'
      '       jp.GARANTEN_ROK'
      'from jn_ponudi jp'
      
        'inner join mat_partner mp on mp.id = jp.partner and mp.tip_partn' +
        'er = jp.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'where(   jp.broj_tender = :mas_broj'
      '     ) and (     JP.ID = :OLD_ID'
      '     )'
      '    '
      '    '
      '    ')
    SelectSQL.Strings = (
      'select jp.id,'
      '       jp.tip_partner,'
      '       jp.partner,'
      '       tp.naziv as tipPartenrNaziv,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten as partnerSkratenNaziv,'
      '       jp.broj_tender,'
      '       jp.datum_ponuda,'
      '       jp.kompletnost,'
      '       jp.zabeleska,'
      '       jp.garancii_popust,'
      '       jp.dokumentacija,'
      '       jp.pretstavnik,'
      '       jp.odbiena,'
      '       case when jp.odbiena = 1 then '#39#1054#1076#1073#1080#1077#1085#1072#39
      '            when jp.odbiena = 0 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '       end "odbienaNaziv",'
      '       jp.broj,'
      '       jp.broj_ponuduvac,'
      '       jp.primena_vo_rok,'
      '       jp.zatvoren_kovert,'
      '       jp.oznaka_ne_otvoraj,'
      '       jp.povlekuvanje_ponuda,'
      '       jp.izmena_ponuda,'
      '       jp.rok_vaznost,'
      '       jp.valuta,'
      '       jp.garancija_iznos,'
      '       jp.ts_ins,'
      '       jp.ts_upd,'
      '       jp.usr_ins,'
      '       jp.usr_upd,'
      '       jp.garancija_datum,'
      '       jp.rok_faktura,'
      '       jp.rok_isporaka,'
      '       jp.GARANTEN_ROK'
      'from jn_ponudi jp'
      
        'inner join mat_partner mp on mp.id = jp.partner and mp.tip_partn' +
        'er = jp.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'where  jp.broj_tender = :mas_broj'
      '    '
      '    ')
    AutoUpdateOptions.UpdateTableName = 'JN_PONUDI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_PONUDA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 1280
    Top = 40
    object tblPonudiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPonudiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblPonudiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblPonudiPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiPARTNERSKRATENNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1089#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074
      FieldName = 'PARTNERSKRATENNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiDATUM_PONUDA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM_PONUDA'
    end
    object tblPonudiKOMPLETNOST: TFIBIntegerField
      DisplayLabel = #1050#1086#1084#1087#1083#1077#1090#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      FieldName = 'KOMPLETNOST'
    end
    object tblPonudiZABELESKA: TFIBBlobField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
      FieldName = 'ZABELESKA'
      Size = 8
    end
    object tblPonudiGARANCII_POPUST: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1075#1072#1088#1072#1085#1094#1080#1080' '#1080' '#1087#1086#1087#1091#1089#1090#1080
      FieldName = 'GARANCII_POPUST'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiDOKUMENTACIJA: TFIBBlobField
      DisplayLabel = #1055#1088#1080#1083#1086#1078#1077#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      FieldName = 'DOKUMENTACIJA'
      Size = 8
    end
    object tblPonudiPRETSTAVNIK: TFIBStringField
      DisplayLabel = #1055#1088#1077#1090#1089#1090#1072#1074#1085#1080#1082
      FieldName = 'PRETSTAVNIK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiBROJ_PONUDUVAC: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095')'
      FieldName = 'BROJ_PONUDUVAC'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPonudiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPonudiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiodbienaNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'odbienaNaziv'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiODBIENA: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ODBIENA'
    end
    object tblPonudiTIPPARTENRNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTENRNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiPRIMENA_VO_ROK: TFIBSmallIntField
      DisplayLabel = #1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1087#1088#1080#1084#1077#1085#1072' '#1074#1086' '#1088#1086#1082
      FieldName = 'PRIMENA_VO_ROK'
    end
    object tblPonudiZATVOREN_KOVERT: TFIBSmallIntField
      DisplayLabel = #1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1077' '#1079#1072#1090#1074#1086#1088#1077#1085
      FieldName = 'ZATVOREN_KOVERT'
    end
    object tblPonudiOZNAKA_NE_OTVORAJ: TFIBSmallIntField
      DisplayLabel = #1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1080#1084#1072' '#1086#1079#1085#1072#1082#1072' '#8222#1085#1077' '#1086#1090#1074#1086#1088#1072#1112#8220
      FieldName = 'OZNAKA_NE_OTVORAJ'
    end
    object tblPonudiPOVLEKUVANJE_PONUDA: TFIBSmallIntField
      DisplayLabel = #1055#1086#1074#1083#1077#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'POVLEKUVANJE_PONUDA'
    end
    object tblPonudiIZMENA_PONUDA: TFIBSmallIntField
      DisplayLabel = #1048#1079#1084#1077#1085#1072' '#1080#1083#1080' '#1079#1072#1084#1077#1085#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      FieldName = 'IZMENA_PONUDA'
    end
    object tblPonudiROK_VAZNOST: TFIBStringField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
      FieldName = 'ROK_VAZNOST'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072' '#1085#1072' '#1087#1085#1091#1076#1072#1090#1072
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiGARANCIJA_IZNOS: TFIBBCDField
      DisplayLabel = #1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1080#1079#1085#1086#1089
      FieldName = 'GARANCIJA_IZNOS'
      Size = 2
    end
    object tblPonudiROK_FAKTURA: TFIBStringField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'ROK_FAKTURA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiROK_ISPORAKA: TFIBStringField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      FieldName = 'ROK_ISPORAKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiGARANCIJA_DATUM: TFIBDateField
      DisplayLabel = #1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1087#1086#1085#1091#1076#1072' - '#1076#1072#1090#1091#1084
      FieldName = 'GARANCIJA_DATUM'
    end
    object tblPonudiGARANTEN_ROK: TFIBStringField
      DisplayLabel = #1043#1072#1088#1072#1085#1090#1077#1085' '#1056#1086#1082
      FieldName = 'GARANTEN_ROK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudi: TDataSource
    DataSet = tblPonudi
    Left = 1408
    Top = 40
  end
  object tblPonudiStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDI_STAVKI'
      'SET '
      '    ID = :ID,'
      '    PONUDA_ID = :PONUDA_ID,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    KOLICINA = :KOLICINA,'
      '    OPIS = :OPIS,'
      '    CENA = :CENA,'
      '    CENABEZDDV = :CENABEZDDV,'
      '    DANOK = :DANOK,'
      '    ODBIENA = :ODBIENA,'
      '    ODBIENA_OBRAZLOZENIE = :ODBIENA_OBRAZLOZENIE,'
      '    CENA_PRVA_BEZDDV = :CENA_PRVA_BEZDDV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_PONUDI_STAVKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_PONUDI_STAVKI('
      '    ID,'
      '    PONUDA_ID,'
      '    BROJ_TENDER,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    KOLICINA,'
      '    OPIS,'
      '    CENA,'
      '    CENABEZDDV,'
      '    DANOK,'
      '    ODBIENA,'
      '    ODBIENA_OBRAZLOZENIE,'
      '    CENA_PRVA_BEZDDV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :PONUDA_ID,'
      '    :BROJ_TENDER,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :KOLICINA,'
      '    :OPIS,'
      '    :CENA,'
      '    :CENABEZDDV,'
      '    :DANOK,'
      '    :ODBIENA,'
      '    :ODBIENA_OBRAZLOZENIE,'
      '    :CENA_PRVA_BEZDDV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select ps.id,'
      '       ps.ponuda_id,'
      '       ps.broj_tender,'
      '       ps.tip_partner,'
      '       ps.partner,'
      '       ps.vid_artikal,'
      '       ta.naziv as vidArtikalNaziv,'
      '       ps.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka, '
      '       mm.naziv as merkaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       jc.mk, jc.en,'
      '       ma.jn_tip_artikal,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ta.naziv as jn_tip_artikal_Naziv,'
      '       ps.kolicina,'
      '       ps.opis,'
      '       ps.cena,'
      '       ps.cenabezddv,'
      '       ps.danok,'
      '       ps.odbiena,'
      '       case when ps.odbiena = 1 then '#39#1054#1076#1073#1080#1077#1085#1072#39
      '            when ps.odbiena = 0 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '       end "odbienaNaziv",'
      '       ps.odbiena_obrazlozenie,'
      '       ps.cena_prva_bezddv,'
      '       ps.cena * ps.kolicina as iznosCena,'
      '       ps.cenabezddv * ps.kolicina as iznosCenBezDDV,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      'from jn_ponudi_stavki ps'
      
        'inner join jn_ponudi p on p.id = ps.ponuda_id and p.broj_tender ' +
        '= ps.broj_tender and p.tip_partner = ps.tip_partner and p.partne' +
        'r = ps.partner'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = ps.broj_tende' +
        'r and ts.vid_artikal = ps.vid_artikal and ts.artikal = ps.artika' +
        'l'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ma.artvid = ' +
        'ps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'left outer join jn_cpv jc on jc.code = ma.jn_cpv'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'where(  ps.ponuda_id = :mas_id'
      '     ) and (     PS.ID = :OLD_ID'
      '     )'
      'order by ma.jn_tip_artikal, ma.naziv    ')
    SelectSQL.Strings = (
      'select ps.id,'
      '       ps.ponuda_id,'
      '       ps.broj_tender,'
      '       ps.tip_partner,'
      '       ps.partner,'
      '       ps.vid_artikal,'
      '       ta.naziv as vidArtikalNaziv,'
      '       ps.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka, '
      '       mm.naziv as merkaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       jc.mk, jc.en,'
      '       ma.jn_tip_artikal,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ta.naziv as jn_tip_artikal_Naziv,'
      '       ps.kolicina,'
      '       ps.opis,'
      '       ps.cena,'
      '       ps.cenabezddv,'
      '       ps.danok,'
      '       ps.odbiena,'
      '       case when ps.odbiena = 1 then '#39#1054#1076#1073#1080#1077#1085#1072#39
      '            when ps.odbiena = 0 then '#39#1042#1072#1083#1080#1076#1085#1072#39
      '       end "odbienaNaziv",'
      '       ps.odbiena_obrazlozenie,'
      '       ps.cena_prva_bezddv,'
      '       ps.cena * ps.kolicina as iznosCena,'
      '       ps.cenabezddv * ps.kolicina as iznosCenBezDDV,'
      '       ps.ts_ins,'
      '       ps.ts_upd,'
      '       ps.usr_ins,'
      '       ps.usr_upd,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      'from jn_ponudi_stavki ps'
      
        'inner join jn_ponudi p on p.id = ps.ponuda_id and p.broj_tender ' +
        '= ps.broj_tender and p.tip_partner = ps.tip_partner and p.partne' +
        'r = ps.partner'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = ps.broj_tende' +
        'r and ts.vid_artikal = ps.vid_artikal and ts.artikal = ps.artika' +
        'l'
      
        'inner join mtr_artikal ma on ma.id = ps.artikal and ma.artvid = ' +
        'ps.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'left outer join jn_cpv jc on jc.code = ma.jn_cpv'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'where ps.ponuda_id = :mas_id'
      'order by ma.jn_tip_artikal, ma.naziv')
    AutoUpdateOptions.UpdateTableName = 'JN_PONUDI_STAVKI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_PONUDI_STAVKI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudi
    Left = 1280
    Top = 104
    object tblPonudiStavkiID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblPonudiStavkiPONUDA_ID: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PONUDA_ID'
    end
    object tblPonudiStavkiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblPonudiStavkiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblPonudiStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblPonudiStavkiVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblPonudiStavkiARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblPonudiStavkiJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblPonudiStavkiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblPonudiStavkiPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiJN_TIP_ARTIKAL_NAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      OnSetText = tblPonudiStavkiKOLICINASetText
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblPonudiStavkiOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblPonudiStavkiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      OnSetText = tblPonudiStavkiCENASetText
      DisplayFormat = '0.0000 , .'
    end
    object tblPonudiStavkiCENABEZDDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENABEZDDV'
      OnSetText = tblPonudiStavkiCENABEZDDVSetText
      DisplayFormat = '0.0000 , .'
    end
    object tblPonudiStavkiDANOK: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DANOK'
      OnSetText = tblPonudiStavkiDANOKSetText
      DisplayFormat = '0.00 , .'
    end
    object tblPonudiStavkiodbienaNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'odbienaNaziv'
      Size = 7
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiCENA_PRVA_BEZDDV: TFIBFloatField
      DisplayLabel = #1055#1088#1074#1072' '#1094#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_PRVA_BEZDDV'
      DisplayFormat = '0.0000, .'
    end
    object tblPonudiStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPonudiStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPonudiStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiODBIENA: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'ODBIENA'
    end
    object tblPonudiStavkiIZNOSCENA: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOSCENA'
      DisplayFormat = '0.0000, .'
    end
    object tblPonudiStavkiIZNOSCENBEZDDV: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOSCENBEZDDV'
      DisplayFormat = '0.0000, .'
    end
    object tblPonudiStavkiODBIENA_OBRAZLOZENIE: TFIBStringField
      DisplayLabel = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1085#1077#1087#1088#1080#1092#1072#1090#1083#1080#1074#1086#1089#1090
      FieldName = 'ODBIENA_OBRAZLOZENIE'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonudiStavkiSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonudiStavki: TDataSource
    DataSet = tblPonudiStavki
    Left = 1408
    Top = 104
  end
  object tblIzborStavkiZaPonuda: TpFIBDataSet
    RefreshSQL.Strings = (
      'select  ts.broj_tender,'
      '        ts.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ts.artikal,'
      '        ma.naziv as nazivArtikal,'
      '        ts.kolicina,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.generika,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.tarifa,'
      '        ts.validnost,'
      '        ts.naziv as stavka_naziv,'
      '        ts.merka as stavka_merka'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left join jn_ponudi_stavki ps on ps.vid_artikal = ts.vid_artikal' +
        ' and ps.artikal = ts.artikal and ps.broj_tender=:mas_broj_tender' +
        ' and ps.tip_partner = :mas_tip_partner and ps.partner = :mas_par' +
        'tner'
      'where(  ts.broj_tender = :mas_broj_tender and ps.artikal is null'
      '     ) and (     TS.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and TS.VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and TS.ARTIKAL = :OLD_ARTIKAL'
      '     )'
      'order by ma.jn_tip_artikal, ma.naziv    ')
    SelectSQL.Strings = (
      'select  ts.broj_tender,'
      '        ts.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ts.artikal,'
      '        ma.naziv as nazivArtikal,'
      '        ts.kolicina,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.generika,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.tarifa,'
      '        ts.validnost,'
      '        ts.naziv as stavka_naziv,'
      '        ts.merka as stavka_merka'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left join jn_ponudi_stavki ps on ps.vid_artikal = ts.vid_artikal' +
        ' and ps.artikal = ts.artikal and ps.broj_tender=:mas_broj_tender' +
        ' and ps.tip_partner = :mas_tip_partner and ps.partner = :mas_par' +
        'tner'
      'where ts.broj_tender = :mas_broj_tender and ps.artikal is null'
      'order by ma.jn_tip_artikal, ma.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsPonudi
    Left = 1280
    Top = 168
    object tblIzborStavkiZaPonudaBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblIzborStavkiZaPonudaJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiZaPonudaVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblIzborStavkiZaPonudaNAZIVARTIKAL: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIVARTIKAL'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblIzborStavkiZaPonudaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblIzborStavkiZaPonudaPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiZaPonudaJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaTARIFA: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'TARIFA'
    end
    object tblIzborStavkiZaPonudaVALIDNOST: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1080#1096#1090#1077#1085
      FieldName = 'VALIDNOST'
    end
    object tblIzborStavkiZaPonudaSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaPonudaSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborStavkiZaPonuda: TDataSource
    DataSet = tblIzborStavkiZaPonuda
    Left = 1408
    Top = 168
  end
  object tblBodiranjeStavkiPonudi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDI_STAVKI_BODIRANI'
      'SET '
      '    VREDNOST = :VREDNOST,'
      '    BODOVI = :BODOVI'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select pb.id,'
      '       pb.ponudi_stavki_id,'
      '       pb.broj_tender,'
      '       pb.tip_partner,'
      '       pb.partner,'
      '       pb.vid_artikal,'
      '       pb.artikal,'
      '       pb.bodiranje_po,'
      '       pb.vrednost,'
      '       pb.bodovi,'
      '       k.naziv as kriteriumNaziv,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       k.bodiranje,'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       b.max_bodovi,'
      '       pb.ts_ins,'
      '       pb.ts_upd,'
      '       pb.usr_ins,'
      '       pb.usr_upd'
      'from jn_ponudi_stavki_bodirani pb'
      'inner join jn_ponudi_stavki ps on ps.id =pb.ponudi_stavki_id and'
      
        '                               ps.broj_tender = pb.broj_tender a' +
        'nd'
      
        '                               ps.tip_partner = pb.tip_partner a' +
        'nd'
      '                               ps.partner = pb.partner and'
      
        '                               ps.vid_artikal = pb.vid_artikal a' +
        'nd'
      '                               ps.artikal = pb.artikal'
      
        'inner join jn_tender_gen_bodiranje b on b.broj_tender = pb.broj_' +
        'tender and'
      
        '                                     b.bodiranje_po = pb.bodiran' +
        'je_po'
      'inner join jn_kriteriumi k on k.sifra = b.bodiranje_po'
      ''
      'where pb.ponudi_stavki_id = :mas_id and'
      '      pb.broj_tender = :mas_broj_tender and'
      '      pb.tip_partner = :mas_tip_partner and'
      '      pb.partner = :mas_partner and'
      '      pb.vid_artikal = :mas_vid_artikal and'
      '      pb.artikal = :mas_artikal')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudiStavki
    Left = 1280
    Top = 232
    object tblBodiranjeStavkiPonudiID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072' - '#1073#1086#1076#1080#1088#1072#1114#1077
      FieldName = 'ID'
      Size = 0
    end
    object tblBodiranjeStavkiPonudiPONUDI_STAVKI_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PONUDI_STAVKI_ID'
      Size = 0
    end
    object tblBodiranjeStavkiPonudiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblBodiranjeStavkiPonudiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblBodiranjeStavkiPonudiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' - '#1096#1080#1092#1088#1072
      FieldName = 'VID_ARTIKAL'
    end
    object tblBodiranjeStavkiPonudiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' - '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTIKAL'
    end
    object tblBodiranjeStavkiPonudiBODIRANJE_PO: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '#1087#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'BODIRANJE_PO'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiKRITERIUMNAZIV: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '#1087#1086
      FieldName = 'KRITERIUMNAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiMAX_BODOVI: TFIBBCDField
      DisplayLabel = #1052#1072#1093' '#1073#1086#1076#1086#1074#1080
      FieldName = 'MAX_BODOVI'
      DisplayFormat = '0.0000 , .'
      Size = 2
    end
    object tblBodiranjeStavkiPonudiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblBodiranjeStavkiPonudiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblBodiranjeStavkiPonudiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudivrednuvanjeNaziv: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'vrednuvanjeNaziv'
      Size = 38
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiBODIRANJE: TFIBIntegerField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'BODIRANJE'
    end
    object tblBodiranjeStavkiPonudibodiranjeNaziv: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '
      FieldName = 'bodiranjeNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBodiranjeStavkiPonudiVREDNOST: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'VREDNOST'
      DisplayFormat = '0.0000 , .'
    end
    object tblBodiranjeStavkiPonudiBODOVI: TFIBFloatField
      DisplayLabel = #1041#1086#1076#1086#1074#1080
      FieldName = 'BODOVI'
      DisplayFormat = '0.0000 , .'
    end
  end
  object dsBodiranjeStavkiPonudi: TDataSource
    DataSet = tblBodiranjeStavkiPonudi
    Left = 1408
    Top = 232
  end
  object tblGrupnoBodiranjePonudiStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDI_STAVKI_BODIRANI'
      'SET '
      '    PONUDI_STAVKI_ID = :PONUDI_STAVKI_ID,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    BODIRANJE_PO = :BODIRANJE_PO,'
      '    VREDNOST = :VREDNOST,'
      '    BODOVI = :BODOVI'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select pb.id,'
      '       pb.ponudi_stavki_id,'
      '       pb.broj_tender,'
      '       pb.tip_partner,'
      '       pb.partner,'
      '       pb.bodiranje_po,'
      '       pb.vrednost,'
      '       pb.bodovi,'
      '       k.naziv as kriteriumNaziv,'
      
        '       case when k.vrednuvanje = 1 then '#39#1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' ' +
        #1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      
        '            when k.vrednuvanje = 0 then '#39#1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074 +
        #1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080#39
      '       end "vrednuvanjeNaziv",'
      '       k.bodiranje,'
      '       case when k.bodiranje = 1 then '#39#1044#1072#39
      '            when k.bodiranje = 0 then '#39#1053#1077#39
      '       end "bodiranjeNaziv",'
      '       b.max_bodovi'
      'from jn_ponudi_stavki_bodirani pb'
      
        'inner join jn_tender_gen_bodiranje b on b.broj_tender = pb.broj_' +
        'tender and'
      
        '                                   b.bodiranje_po = pb.bodiranje' +
        '_po'
      'inner join jn_kriteriumi k on k.sifra = b.bodiranje_po'
      ''
      'where pb.ponudi_stavki_id = :mas_id and'
      '      pb.broj_tender = :mas_broj_tender and'
      '      pb.tip_partner = :mas_tip_partner and'
      '      pb.partner = :mas_partner')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudiStavki
    Left = 1280
    Top = 296
    object tblGrupnoBodiranjePonudiStavkiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupnoBodiranjePonudiStavkiTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblGrupnoBodiranjePonudiStavkiBODIRANJE_PO: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '#1087#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'BODIRANJE_PO'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupnoBodiranjePonudiStavkiPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblGrupnoBodiranjePonudiStavkiKRITERIUMNAZIV: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '#1087#1086
      FieldName = 'KRITERIUMNAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupnoBodiranjePonudiStavkivrednuvanjeNaziv: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077
      FieldName = 'vrednuvanjeNaziv'
      Size = 38
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupnoBodiranjePonudiStavkibodiranjeNaziv: TFIBStringField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' '
      FieldName = 'bodiranjeNaziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupnoBodiranjePonudiStavkiBODIRANJE: TFIBIntegerField
      DisplayLabel = #1041#1086#1076#1080#1088#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'BODIRANJE'
    end
    object tblGrupnoBodiranjePonudiStavkiMAX_BODOVI: TFIBBCDField
      DefaultExpression = '0'
      DisplayLabel = #1052#1072#1093' '#1073#1086#1076#1086#1074#1080
      FieldName = 'MAX_BODOVI'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.00'
      Size = 2
    end
    object tblGrupnoBodiranjePonudiStavkiVREDNOST: TFIBFloatField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'VREDNOST'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.0000'
    end
    object tblGrupnoBodiranjePonudiStavkiBODOVI: TFIBFloatField
      DisplayLabel = #1041#1086#1076#1086#1074#1080
      FieldName = 'BODOVI'
      DisplayFormat = '0.00 , .'
      EditFormat = '0.0000'
    end
  end
  object dsGrupnoBodiranjePonudiStavki: TDataSource
    DataSet = tblGrupnoBodiranjePonudiStavki
    Left = 1408
    Top = 296
  end
  object tblDobitniciTender: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_DOBITNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'Select d.id,'
      '       d.id_ponudi_stavki,'
      '       d.broj_tender,'
      '       d.tip_partner,'
      '       tp.naziv as tipPartnerNaziv,'
      '       d.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten,'
      '       d.vid_artikal,'
      '       mv.naziv as vidNaziv,'
      '       d.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.generika,'
      '       ma.jn_tip_artikal,'
      '       d.kolicina,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      'from jn_tender_dobitnici d'
      
        'inner join mtr_artikal ma on ma.artvid = d.vid_artikal and ma.id' +
        ' = d.artikal'
      'inner join jn_tip_artikal mv on mv.id = ma.jn_tip_artikal'
      
        'inner join mat_partner mp on mp.id = d.partner and mp.tip_partne' +
        'r = d.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = d.broj_tender' +
        ' and ts.vid_artikal = d.vid_artikal and ts.artikal = d.artikal'
      ''
      'where(  d.broj_tender = :mas_broj'
      '     ) and (     D.ID = :OLD_ID'
      '     )'
      'order by ma.jn_tip_artikal,ma.naziv    ')
    SelectSQL.Strings = (
      'Select d.id,'
      '       d.id_ponudi_stavki,'
      '       d.broj_tender,'
      '       d.tip_partner,'
      '       tp.naziv as tipPartnerNaziv,'
      '       d.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten,'
      '       d.vid_artikal,'
      '       mv.naziv as vidNaziv,'
      '       d.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.generika,'
      '       ma.jn_tip_artikal,'
      '       d.kolicina,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,'
      '       ts.naziv as stavka_naziv,'
      '       ts.merka as stavka_merka'
      'from jn_tender_dobitnici d'
      
        'inner join mtr_artikal ma on ma.artvid = d.vid_artikal and ma.id' +
        ' = d.artikal'
      'inner join jn_tip_artikal mv on mv.id = ma.jn_tip_artikal'
      
        'inner join mat_partner mp on mp.id = d.partner and mp.tip_partne' +
        'r = d.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      
        'inner join jn_tender_stavki ts on ts.broj_tender = d.broj_tender' +
        ' and ts.vid_artikal = d.vid_artikal and ts.artikal = d.artikal'
      'where d.broj_tender = :mas_broj'
      'order by ma.jn_tip_artikal,ma.naziv')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 1576
    Top = 40
    object tblDobitniciTenderID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblDobitniciTenderID_PONUDI_STAVKI: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1086#1076' '#1087#1086#1085#1091#1076#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ID_PONUDI_STAVKI'
      Size = 0
    end
    object tblDobitniciTenderBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblDobitniciTenderTIPPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDobitniciTenderPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'-'#1074#1080#1076
      FieldName = 'VID_ARTIKAL'
    end
    object tblDobitniciTenderVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblDobitniciTenderARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblDobitniciTenderTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDobitniciTenderTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDobitniciTenderUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblDobitniciTenderJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblDobitniciTenderGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      DisplayWidth = 10000
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciTenderSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDobitniciTender: TDataSource
    DataSet = tblDobitniciTender
    Left = 1720
    Top = 40
  end
  object tblDobitniciIzborPonuduvac: TpFIBDataSet
    SelectSQL.Strings = (
      'select coalesce(sum(sb.bodovi),0) as sumabodovi,'
      '       sb.ponudi_stavki_id,'
      '       sb.broj_tender,'
      '       sb.tip_partner,'
      '       tp.naziv as tipPartnerNaziv,'
      '       sb.partner,'
      '       mp.naziv_skraten as partberNaziv,'
      '       sb.vid_artikal,'
      '       sb.artikal'
      ''
      'from jn_ponudi_stavki_bodirani sb'
      
        'inner join mat_partner mp on  mp.id = sb.partner and mp.tip_part' +
        'ner = sb.tip_partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      
        'inner join jn_ponudi_stavki ps on ps.id = sb.ponudi_stavki_id an' +
        'd ps.odbiena = 0'
      
        'where sb.broj_tender = :mas_broj and sb.artikal = :artikal and s' +
        'b.vid_artikal = :vid_artikal'
      
        'group by sb.ponudi_stavki_id, sb.broj_tender, sb.tip_partner,tp.' +
        'naziv,sb.partner, mp.naziv_skraten,sb.vid_artikal, sb.artikal'
      'order by sumabodovi desc'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 1576
    Top = 104
    object tblDobitniciIzborPonuduvacPONUDI_STAVKI_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1086#1076' '#1087#1086#1085#1091#1076#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PONUDI_STAVKI_ID'
      Size = 0
    end
    object tblDobitniciIzborPonuduvacBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciIzborPonuduvacTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblDobitniciIzborPonuduvacTIPPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciIzborPonuduvacPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDobitniciIzborPonuduvacPARTBERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTBERNAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciIzborPonuduvacVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID_ARTIKAL'
    end
    object tblDobitniciIzborPonuduvacARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblDobitniciIzborPonuduvacSUMABODOVI: TFIBFloatField
      DisplayLabel = #1041#1086#1076#1086#1074#1080
      FieldName = 'SUMABODOVI'
      DisplayFormat = '0.0000 , .'
    end
  end
  object dsDobitniciIzborPonuduvac: TDataSource
    DataSet = tblDobitniciIzborPonuduvac
    Left = 1720
    Top = 104
  end
  object dsDobitniciIzborArtikal: TDataSource
    DataSet = tblDobitniciIzborArtikal
    Left = 1720
    Top = 168
  end
  object tblDobitniciIzborArtikal: TpFIBDataSet
    SelectSQL.Strings = (
      'Select 0 as generika,'
      '       ma.jn_tip_artikal as vid,'
      '       ta.naziv as vidNaziv,'
      '       ga.artikal,'
      '       ma.naziv,'
      '       ga.vid_artikal as artvid'
      'from mtr_generika_artikal ga'
      
        'inner join mtr_artikal ma on ma.id = ga.artikal and ma.artvid = ' +
        'ga.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'where ga.vid_generika = :vid_artikal and ga.generika = :artikal'
      ''
      'union'
      'select 1 as generika,'
      '       ma.jn_tip_artikal as vid,'
      '       ta.naziv as vidNaziv,'
      '       ma.id as artikal,'
      '       ma.naziv,'
      '       ma.artvid'
      'from mtr_artikal ma'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'where ma.artvid = :vid_artikal and ma.id = :artikal'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1576
    Top = 168
    object tblDobitniciIzborArtikalVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID'
    end
    object tblDobitniciIzborArtikalVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciIzborArtikalARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblDobitniciIzborArtikalNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDobitniciIzborArtikalGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblDobitniciIzborArtikalARTVID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'_'#1074#1080#1076
      FieldName = 'ARTVID'
    end
  end
  object tblDogovori: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_DOGOVOR'
      'SET '
      '    BROJ_DOGOVOR = :BROJ_DOGOVOR,'
      '    DATUM = :DATUM,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    OPIS = :OPIS,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    DATUM_VAZENJE = :DATUM_VAZENJE,'
      '    PERIOD = :PERIOD,'
      '    ROK_ISPORAKA = :ROK_ISPORAKA,'
      '    ROK_FAKTURA = :ROK_FAKTURA,'
      '    GARANTEN_ROK = :GARANTEN_ROK,'
      '    SMETKA = :SMETKA,'
      '    DIREKTOR = :DIREKTOR,'
      '    TIP = :TIP,'
      '    VRSKA_DOGOVOR = :VRSKA_DOGOVOR,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    IZNOS = :IZNOS'
      'WHERE'
      '    BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_DOGOVOR'
      'WHERE'
      '        BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_DOGOVOR('
      '    BROJ_DOGOVOR,'
      '    DATUM,'
      '    BROJ_TENDER,'
      '    OPIS,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    DATUM_VAZENJE,'
      '    PERIOD,'
      '    ROK_ISPORAKA,'
      '    ROK_FAKTURA,'
      '    GARANTEN_ROK,'
      '    SMETKA,'
      '    DIREKTOR,'
      '    TIP,'
      '    VRSKA_DOGOVOR,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    IZNOS'
      ')'
      'VALUES('
      '    :BROJ_DOGOVOR,'
      '    :DATUM,'
      '    :BROJ_TENDER,'
      '    :OPIS,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :DATUM_VAZENJE,'
      '    :PERIOD,'
      '    :ROK_ISPORAKA,'
      '    :ROK_FAKTURA,'
      '    :GARANTEN_ROK,'
      '    :SMETKA,'
      '    :DIREKTOR,'
      '    :TIP,'
      '    :VRSKA_DOGOVOR,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :IZNOS'
      ')')
    RefreshSQL.Strings = (
      'select d.broj_dogovor,'
      '       d.datum,'
      '       d.broj_tender,'
      '       d.opis,'
      '       d.tip_partner,'
      '       tp.naziv as tipPartnerNaziv,'
      '       d.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten, '
      '       d.datum_vazenje,'
      '       d.period,'
      '       d.rok_isporaka,'
      '       d.rok_faktura,'
      '       d.garanten_rok,'
      '       d.smetka, '
      '       d.direktor,'
      '       d.tip,'
      '       case when d.tip = 1 then '#39#1044#1086#1075#1086#1074#1086#1088#39
      '            when d.tip = -1 then '#39#1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#39
      '       end "tipNaziv",'
      '       d.vrska_dogovor,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,d.iznos'
      'from jn_dogovor d'
      
        'left outer join mat_partner mp on mp.tip_partner = d.tip_partner' +
        ' and mp.id = d.partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'where(  d.broj_tender = :mas_broj'
      '     ) and (     D.BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select d.broj_dogovor,'
      '       d.datum,'
      '       d.broj_tender,'
      '       d.opis,'
      '       d.tip_partner,'
      '       tp.naziv as tipPartnerNaziv,'
      '       d.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten,'
      '       d.datum_vazenje,'
      '       d.period,'
      '       d.rok_isporaka,'
      '       d.rok_faktura,'
      '       d.garanten_rok,'
      '       d.smetka, '
      '       d.direktor,'
      '       d.tip,'
      '       case when d.tip = 1 then '#39#1044#1086#1075#1086#1074#1086#1088#39
      '            when d.tip = -1 then '#39#1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#39
      '       end "tipNaziv",'
      '       d.vrska_dogovor,'
      '       d.ts_ins,'
      '       d.ts_upd,'
      '       d.usr_ins,'
      '       d.usr_upd,d.iznos'
      'from jn_dogovor d'
      
        'left outer join mat_partner mp on mp.tip_partner = d.tip_partner' +
        ' and mp.id = d.partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'where d.broj_tender = :mas_broj'
      'order by d.datum')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 1856
    Top = 40
    object tblDogovoriBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DATUM'
    end
    object tblDogovoriBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblDogovoriTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblDogovoriTIPPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblDogovoriPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1112' '#1074#1072#1078#1080' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
      FieldName = 'DATUM_VAZENJE'
    end
    object tblDogovoriPERIOD: TFIBStringField
      DisplayLabel = #1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' - '#1086#1087#1080#1089#1085#1086
      FieldName = 'PERIOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriROK_ISPORAKA: TFIBIntegerField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1085#1072' '#1089#1090#1086#1082#1072
      FieldName = 'ROK_ISPORAKA'
    end
    object tblDogovoriROK_FAKTURA: TFIBIntegerField
      DisplayLabel = #1056#1086#1082' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072
      FieldName = 'ROK_FAKTURA'
    end
    object tblDogovoriGARANTEN_ROK: TFIBStringField
      DisplayLabel = #1043#1072#1088#1072#1085#1090#1077#1085' '#1088#1086#1082
      FieldName = 'GARANTEN_ROK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriSMETKA: TFIBIntegerField
      DisplayLabel = #1057#1084#1077#1090#1082#1072' '#1085#1072' '#1076#1086#1073#1072#1074#1091#1074#1072#1095#1086#1090
      FieldName = 'SMETKA'
    end
    object tblDogovoriDIREKTOR: TFIBStringField
      DisplayLabel = #1044#1080#1088#1077#1082#1090#1086#1088' '#1085#1072' '#1092#1080#1088#1084#1072#1090#1072' '
      FieldName = 'DIREKTOR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblDogovoritipNaziv: TFIBStringField
      DisplayLabel = #1058#1080#1087' '
      FieldName = 'tipNaziv'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriVRSKA_DOGOVOR: TFIBStringField
      DisplayLabel = #1042#1088#1089#1082#1072' ('#1072#1085#1077#1082#1089'/'#1076#1086#1075#1086#1074#1086#1088')'
      FieldName = 'VRSKA_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogovoriTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogovoriUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovoriIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
  end
  object dsDogovori: TDataSource
    DataSet = tblDogovori
    Left = 1976
    Top = 40
  end
  object tblDogovorStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_DOGOVOR_STAVKI'
      'SET '
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    KOLICINA = :KOLICINA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '    and RED_BR = :OLD_RED_BR'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_DOGOVOR_STAVKI'
      'WHERE'
      '        BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '    and RED_BR = :OLD_RED_BR'
      '    ')
    RefreshSQL.Strings = (
      'select ds.broj_dogovor,'
      '       ds.red_br,'
      '       ds.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ds.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       ds.kolicina,'
      '       ds.ts_ins,'
      '       ds.ts_upd,'
      '       ds.usr_ins,'
      '       ds.usr_upd,'
      '       psb.cenabezddv,'
      '       psb.danok,'
      '       psb.cena ,'
      '      (psb.cena*ds.KOLICINA) as iznos ,'
      '      (psb.cenabezddv*ds.KOLICINA) as iznosbezddv ,'
      '      tsa.naziv as stavka_naziv,'
      '      tsa.merka as stavka_merka'
      ''
      'from jn_dogovor_stavki ds'
      
        'inner join mtr_artikal ma on ma.id = ds.artikal and ma.artvid = ' +
        'ds.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      'inner join jn_dogovor d on d.broj_dogovor = ds.broj_dogovor'
      
        'left outer join jn_ponudi_stavki psb on (psb.artikal=ds.artikal ' +
        'and'
      
        'psb.vid_artikal=ds.vid_artikal and psb.broj_tender=ds.broj_tende' +
        'r and'
      'psb.tip_partner=d.tip_partner and psb.partner=d.partner )'
      
        'inner join jn_tender_stavki tsa on tsa.broj_tender = ds.broj_ten' +
        'der and tsa.artikal = ds.artikal and tsa.vid_artikal = ds.vid_ar' +
        'tikal'
      'where(  ds.broj_dogovor = :mas_broj_dogovor'
      '     ) and (     DS.BROJ_DOGOVOR = :OLD_BROJ_DOGOVOR'
      '    and DS.RED_BR = :OLD_RED_BR'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ds.broj_dogovor,'
      '       ds.red_br,'
      '       ds.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ds.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       ds.kolicina,'
      '       ds.ts_ins,'
      '       ds.ts_upd,'
      '       ds.usr_ins,'
      '       ds.usr_upd,'
      '       psb.cenabezddv,'
      '       psb.danok,'
      '       psb.cena ,'
      '      (psb.cena*ds.KOLICINA) as iznos ,'
      '      (psb.cenabezddv*ds.KOLICINA) as iznosbezddv,'
      '      tsa.naziv as stavka_naziv,'
      '      tsa.merka as stavka_merka'
      ''
      'from jn_dogovor_stavki ds'
      
        'inner join mtr_artikal ma on ma.id = ds.artikal and ma.artvid = ' +
        'ds.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      'inner join jn_dogovor d on d.broj_dogovor = ds.broj_dogovor'
      
        'left outer join jn_ponudi_stavki psb on (psb.artikal=ds.artikal ' +
        'and'
      
        'psb.vid_artikal=ds.vid_artikal and psb.broj_tender=ds.broj_tende' +
        'r and'
      'psb.tip_partner=d.tip_partner and psb.partner=d.partner )'
      
        'inner join jn_tender_stavki tsa on tsa.broj_tender = ds.broj_ten' +
        'der and tsa.artikal = ds.artikal and tsa.vid_artikal = ds.vid_ar' +
        'tikal'
      'where ds.broj_dogovor = :mas_broj_dogovor'
      'order by ma.jn_tip_artikal,artikalNaziv')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsDogovori
    Left = 1856
    Top = 104
    object tblDogovorStavkiBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiRED_BR: TFIBIntegerField
      DisplayLabel = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
      FieldName = 'RED_BR'
    end
    object tblDogovorStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblDogovorStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblDogovorStavkiVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblDogovorStavkiARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblDogovorStavkiPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblDogovorStavkiJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDogovorStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblDogovorStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiDANOK: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DANOK'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiCENABEZDDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENABEZDDV'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiIZNOS: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiIZNOSBEZDDV: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOSBEZDDV'
      DisplayFormat = '0.00 , .'
    end
    object tblDogovorStavkiSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDogovorStavkiSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDogovorStavki: TDataSource
    DataSet = tblDogovorStavki
    Left = 1984
    Top = 104
  end
  object tblIzborStavkiZaDogovor: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ds.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ds.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       ds.kolicina,'
      '       ds.ts_ins,'
      '       ds.ts_upd,'
      '       ds.usr_ins,'
      '       ds.usr_upd,'
      '      tsa.naziv as stavka_naziv,'
      '      tsa.merka as stavka_merka'
      'from jn_tender_dobitnici ds'
      
        'inner join mtr_artikal ma on ma.id = ds.artikal and ma.artvid = ' +
        'ds.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left join jn_dogovor_stavki d on d.vid_artikal = ds.vid_artikal ' +
        'and d.artikal = ds.artikal and d.broj_tender = ds.broj_tender'
      
        'inner join jn_tender_stavki tsa on tsa.broj_tender = ds.broj_ten' +
        'der and tsa.artikal = ds.artikal and tsa.vid_artikal = ds.vid_ar' +
        'tikal'
      
        'where d.artikal is null and ds.broj_tender = :mas_broj_tender an' +
        'd ds.tip_partner = :mas_tip_partner and ds.partner = :partner'
      'order by ma.jn_tip_artikal,artikalNaziv')
    SelectSQL.Strings = (
      'select ds.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ds.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       ds.kolicina,'
      '       ds.ts_ins,'
      '       ds.ts_upd,'
      '       ds.usr_ins,'
      '       ds.usr_upd,'
      '      tsa.naziv as stavka_naziv,'
      '      tsa.merka as stavka_merka'
      'from jn_tender_dobitnici ds'
      
        'inner join mtr_artikal ma on ma.id = ds.artikal and ma.artvid = ' +
        'ds.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'left join jn_dogovor_stavki d on d.vid_artikal = ds.vid_artikal ' +
        'and d.artikal = ds.artikal and d.broj_tender = ds.broj_tender'
      
        'inner join jn_tender_stavki tsa on tsa.broj_tender = ds.broj_ten' +
        'der and tsa.artikal = ds.artikal and tsa.vid_artikal = ds.vid_ar' +
        'tikal'
      
        'where d.artikal is null and ds.broj_tender = :mas_broj_tender an' +
        'd ds.tip_partner = :mas_tip_partner and ds.partner = :partner'
      'order by ma.jn_tip_artikal,artikalNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsDogovori
    Left = 1856
    Top = 168
    object tblIzborStavkiZaDogovorVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblIzborStavkiZaDogovorJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiZaDogovorVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblIzborStavkiZaDogovorARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblIzborStavkiZaDogovorPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiZaDogovorJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiZaDogovorTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblIzborStavkiZaDogovorTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblIzborStavkiZaDogovorUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiZaDogovorSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborStavkiZaDogovor: TDataSource
    DataSet = tblIzborStavkiZaDogovor
    Left = 1992
    Top = 168
  end
  object tblBaranja: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_BARANJE'
      'SET '
      '    BROJ = :BROJ,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER,'
      '    DATUM = :DATUM,'
      '    RE = :RE,'
      '    BROJ_DOGOVOR = :BROJ_DOGOVOR,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    ZABELESKA = :ZABELESKA'
      'WHERE'
      '    BROJ = :OLD_BROJ'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_BARANJE'
      'WHERE'
      '        BROJ = :OLD_BROJ'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_BARANJE('
      '    BROJ,'
      '    BROJ_TENDER,'
      '    TIP_PARTNER,'
      '    PARTNER,'
      '    DATUM,'
      '    RE,'
      '    BROJ_DOGOVOR,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    ZABELESKA'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :BROJ_TENDER,'
      '    :TIP_PARTNER,'
      '    :PARTNER,'
      '    :DATUM,'
      '    :RE,'
      '    :BROJ_DOGOVOR,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :ZABELESKA'
      ')')
    RefreshSQL.Strings = (
      'select b.broj,'
      '       b.broj_tender,'
      '       b.tip_partner,'
      '       mtp.naziv as tipPartnerNaziv,'
      '       b.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten,'
      '       b.datum,'
      '       b.re,'
      '       mr.naziv as reNaziv,'
      '       b.broj_dogovor,'
      '       b.ts_ins,'
      '       b.ts_upd,'
      '       b.usr_ins,'
      '       b.usr_upd,'
      '       b.zabeleska,'
      '       t.tip_baranja,'
      '       t.status'
      'from jn_baranje b'
      
        'inner join mat_partner mp on mp.id = b.partner and mp.tip_partne' +
        'r = b.tip_partner'
      'inner join mat_tip_partner mtp on mtp.id = mp.tip_partner'
      'inner join jn_tender t on t.broj = b.broj_tender'
      'left outer join mat_re mr on mr.id = b.re'
      
        'where(  b.broj_tender like :broj_tender and b.broj_dogovor like ' +
        ':broj_dogovor'
      
        '      and ((mr.id in (select s.re from sys_user_re_app s where s' +
        '.username = :username and s.app = :app /*$$IBEC$$ and s.prava = ' +
        '0 $$IBEC$$*/ and :param = 1)'
      '          ) or (:param = 0))'
      '     ) and (     B.BROJ = :OLD_BROJ'
      '     )'
      'order by b.datum')
    SelectSQL.Strings = (
      'select b.broj,'
      '       b.broj_tender,'
      '       b.tip_partner,'
      '       mtp.naziv as tipPartnerNaziv,'
      '       b.partner,'
      '       mp.naziv as partnerNaziv,'
      '       mp.naziv_skraten,'
      '       b.datum,'
      '       b.re,'
      '       mr.naziv as reNaziv,'
      '       b.broj_dogovor,'
      '       b.ts_ins,'
      '       b.ts_upd,'
      '       b.usr_ins,'
      '       b.usr_upd,'
      '       b.zabeleska,'
      '       t.tip_baranja,'
      '       t.status'
      'from jn_baranje b'
      
        'inner join mat_partner mp on mp.id = b.partner and mp.tip_partne' +
        'r = b.tip_partner'
      'inner join mat_tip_partner mtp on mtp.id = mp.tip_partner'
      'inner join jn_tender t on t.broj = b.broj_tender'
      'left outer join mat_re mr on mr.id = b.re'
      
        'where b.broj_tender like :broj_tender and b.broj_dogovor like :b' +
        'roj_dogovor'
      
        '      and ((mr.id in (select s.re from sys_user_re_app s where s' +
        '.username = :username and s.app = :app /*$$IBEC$$ and s.prava = ' +
        '0 $$IBEC$$*/ and :param = 1)'
      '          ) or (:param = 0))'
      'order by b.datum')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 2120
    Top = 40
    object tblBaranjaBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1114#1077
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087' '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblBaranjaTIPPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
      FieldName = 'TIPPARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaPARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblBaranjaPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblBaranjaRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1064#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblBaranjaRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblBaranjaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblBaranjaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaZABELESKA: TFIBStringField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'ZABELESKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaTIP_BARANJA: TFIBSmallIntField
      FieldName = 'TIP_BARANJA'
    end
    object tblBaranjaSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
  end
  object dsBaranja: TDataSource
    DataSet = tblBaranja
    Left = 2264
    Top = 40
  end
  object tblReBaranja: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct tsr.re,'
      '       m.naziv'
      'from jn_tender_stavki_re tsr'
      'inner join mat_re m on m.id = tsr.re'
      'where tsr.broj_tender like :broj_tender'
      
        '      and ((m.id in (select s.re from sys_user_re_app s where s.' +
        'username = :username and s.app = :app /*$$IBEC$$ and s.prava = 0' +
        ' $$IBEC$$*/ and :param = 1)'
      '          ) or (:param = 0))'
      '     '
      'order by m.naziv'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 2120
    Top = 104
    object tblReBaranjaRE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblReBaranjaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsReBaranja: TDataSource
    DataSet = tblReBaranja
    Left = 2272
    Top = 104
  end
  object tblBaranjaStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_BARANJE_STAVKI'
      'SET '
      '    BROJ = :BROJ,'
      '    ARTIKAL = :ARTIKAL,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    KOLICINA = :KOLICINA,'
      '    CENA = :CENA,'
      '    RE = :RE,'
      '    ID = :ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_BARANJE_STAVKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_BARANJE_STAVKI('
      '    BROJ,'
      '    ARTIKAL,'
      '    VID_ARTIKAL,'
      '    KOLICINA,'
      '    CENA,'
      '    RE,'
      '    ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :BROJ,'
      '    :ARTIKAL,'
      '    :VID_ARTIKAL,'
      '    :KOLICINA,'
      '    :CENA,'
      '    :RE,'
      '    :ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select '
      '       bs.broj,'
      '       bs.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ts.naziv stavkaNaziv,'
      '       bs.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       bs.kolicina,'
      '       bs.cena,'
      '       bs.re,'
      '       mr.naziv as reNaziv,'
      '       bs.id,'
      '       bs.ts_ins, bs.ts_upd, bs.usr_ins, bs.usr_upd'
      'from jn_baranje_stavki bs'
      'inner join jn_baranje b on b.broj=bs.broj'
      
        'inner join jn_tender_stavki  ts on ts.broj_tender=b.broj_tender ' +
        'and ts.vid_artikal=bs.vid_artikal and ts.artikal=bs.artikal'
      
        'inner join mtr_artikal ma on ma.id = bs.artikal and ma.artvid = ' +
        'bs.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      'left outer join mat_re mr on mr.id = bs.re'
      'where(  bs.broj = :mas_broj'
      '     ) and (     BS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select '
      '       bs.broj,'
      '       bs.artikal,'
      '       ma.naziv as artikalNaziv,'
      '       ts.naziv stavkaNaziv,'
      '       bs.vid_artikal,'
      '       ma.jn_tip_artikal,'
      '       ta.naziv as vidNaziv,'
      '       ma.merka,'
      '       mm.naziv as merkaNaziv,'
      '       ma.prozvoditel,'
      '       mp.naziv as proizvoditelNaziv,'
      '       ma.grupa,'
      '       mg.naziv as grupaNaziv,'
      '       ma.generika,'
      '       ma.jn_cpv,'
      '       cpv.mk, cpv.en,'
      '       bs.kolicina,'
      '       bs.cena,'
      '       bs.re,'
      '       mr.naziv as reNaziv,'
      '       bs.id,'
      '       bs.ts_ins, bs.ts_upd, bs.usr_ins, bs.usr_upd'
      'from jn_baranje_stavki bs'
      'inner join jn_baranje b on b.broj=bs.broj'
      
        'inner join jn_tender_stavki  ts on ts.broj_tender=b.broj_tender ' +
        'and ts.vid_artikal=bs.vid_artikal and ts.artikal=bs.artikal'
      
        'inner join mtr_artikal ma on ma.id = bs.artikal and ma.artvid = ' +
        'bs.vid_artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      'left outer join mat_re mr on mr.id = bs.re'
      'where bs.broj = :mas_broj'
      'order by ma.jn_tip_artikal, ma.naziv')
    AutoUpdateOptions.UpdateTableName = 'JN_BARANJE_STAVKI'
    AutoUpdateOptions.GeneratorName = 'GEN_JN_BARANJE_STAVKI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsBaranja
    Left = 2120
    Top = 168
    object tblBaranjaStavkiBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblBaranjaStavkiARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblBaranjaStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblBaranjaStavkiVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblBaranjaStavkiPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblBaranjaStavkiJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblBaranjaStavkiRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblBaranjaStavkiRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblBaranjaStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblBaranjaStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblBaranjaStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblBaranjaStavkiCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblBaranjaStavkiSTAVKANAZIV: TFIBStringField
      FieldName = 'STAVKANAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsBaranjaStavki: TDataSource
    DataSet = tblBaranjaStavki
    Left = 2272
    Top = 168
  end
  object tblIzborStavkiBaranjePoKolicina: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '       vid,'
      '       sifra,'
      '       kolicina,'
      '       (select coalesce(sum(bs.kolicina),0) as pobarana_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor and ((bs.re = :re) or (:re ' +
        '= -99999))'
      '                    ),'
      
        '       kolicina - (select coalesce(sum(bs.kolicina),0) as pobara' +
        'na_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor and ((bs.re = :re) or (:re ' +
        '= -99999))'
      '                    ) as dozvolena_kolicina,'
      '       artikalNaziv,'
      '       jn_tip_artikal,'
      '       vidNaziv,'
      '       merka,'
      '       merkaNaziv,'
      '       grupa,'
      '       grupaNaziv,'
      '       prozvoditel,'
      '       proizvoditelNaziv,'
      '       jn_cpv,'
      '       mk,en,'
      '       generika'
      'from'
      '(select ds.vid_artikal as vid,'
      '        ds.artikal as sifra,'
      '        ds.kolicina,'
      '        ma.naziv as artikalNaziv,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.generika'
      'from jn_dogovor_stavki ds'
      
        'inner join mtr_artikal ma on ma.artvid = ds.vid_artikal and ma.i' +
        'd = ds.artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'inner join jn_tender_stavki_re ts on ts.broj_tender = ds.broj_te' +
        'nder and ts.vid_artikal = ds.vid_artikal and ts.artikal = ds.art' +
        'ikal and ((ts.re = :re) or (:re = -99999))'
      'where ds.broj_dogovor = :broj_dogovor) p')
    SelectSQL.Strings = (
      'select distinct'
      '       vid,'
      '       sifra,'
      '       kolicina,'
      '       (select coalesce(sum(bs.kolicina),0) as pobarana_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor'
      '                    ),'
      
        '       kolicina - (select coalesce(sum(bs.kolicina),0) as pobara' +
        'na_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor'
      '                    ) as dozvolena_kolicina,'
      ''
      '       kolicina_sektor,'
      
        '       kolicina_sektor - (select coalesce(sum(bs.kolicina),0) as' +
        ' pobarana_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor and ((bs.re = :re) or (:re ' +
        '= -99999))'
      '                    ) as dozvolena_kolicina_sektor,'
      '       (select coalesce(sum(bs.kolicina),0) as pobarana_kolicina'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = vid and artikal = sifr' +
        'a and b.broj_dogovor = :broj_dogovor and ((bs.re = :re) or (:re ' +
        '= -99999))'
      '                    ) as pobarana_kolicina_sektor,'
      '       artikalNaziv,'
      '       jn_tip_artikal,'
      '       vidNaziv,'
      '       merka,'
      '       merkaNaziv,'
      '       grupa,'
      '       grupaNaziv,'
      '       prozvoditel,'
      '       proizvoditelNaziv,'
      '       jn_cpv,'
      '       mk,en,'
      '       generika'
      'from'
      '(select ds.vid_artikal as vid,'
      '        ds.artikal as sifra,'
      '        ds.kolicina,'
      '        ts.kolicina as kolicina_sektor,'
      '        ma.naziv as artikalNaziv,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.generika'
      'from jn_dogovor_stavki ds'
      
        'inner join mtr_artikal ma on ma.artvid = ds.vid_artikal and ma.i' +
        'd = ds.artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'inner join jn_tender_stavki_re ts on ts.broj_tender = ds.broj_te' +
        'nder and ts.vid_artikal = ds.vid_artikal and ts.artikal = ds.art' +
        'ikal and ((ts.re = :re) or (:re = -99999))'
      'where ds.broj_dogovor = :broj_dogovor) p'
      'order by jn_tip_artikal, artikalNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 2120
    Top = 232
    object tblIzborStavkiBaranjePoKolicinaKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
    end
    object tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA: TFIBBCDField
      DisplayLabel = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'POBARANA_KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA: TFIBFloatField
      DisplayLabel = #1054#1089#1090#1072#1085#1072#1090#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'DOZVOLENA_KOLICINA'
      DisplayFormat = '0.00 , .'
    end
    object tblIzborStavkiBaranjePoKolicinaARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiBaranjePoKolicinaVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblIzborStavkiBaranjePoKolicinaPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiBaranjePoKolicinaVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID'
    end
    object tblIzborStavkiBaranjePoKolicinaSIFRA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object tblIzborStavkiBaranjePoKolicinaEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoKolicinaKOLICINA_SEKTOR: TFIBBCDField
      FieldName = 'KOLICINA_SEKTOR'
      Size = 2
    end
    object tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA_SEKTOR: TFIBBCDField
      FieldName = 'DOZVOLENA_KOLICINA_SEKTOR'
      Size = 2
    end
    object tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA_SEKTOR: TFIBBCDField
      FieldName = 'POBARANA_KOLICINA_SEKTOR'
      Size = 2
    end
  end
  object dsIzborStavkiBaranjePoKolicina: TDataSource
    DataSet = tblIzborStavkiBaranjePoKolicina
    Left = 2280
    Top = 232
  end
  object tblIzborStavkiBaranjePoCena: TpFIBDataSet
    RefreshSQL.Strings = (
      ' select ss.godina,'
      '       ss.vid,'
      '       ss.sifra,'
      '       ss.kolicina,'
      '       ss.cena,'
      '       (select coalesce(d.iznos,0) iznos_dogovor'
      '        from jn_dogovor d'
      
        '        where d.tip_partner = :mas_tip_partner and d.partner = :' +
        'mas_partner and d.broj_tender = :mas_broj_tender),'
      '       ((select coalesce(d.iznos,0) iznos_dogovor'
      '        from jn_dogovor d'
      
        '        where d.tip_partner = :mas_tip_partner and d.partner = :' +
        'mas_partner and d.broj_tender = :mas_broj_tender) -'
      '        (select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where b.tip_partner = :mas_tip_partner and b.' +
        'partner = :mas_partner and b.broj_tender = :mas_broj_tender'
      '                    )) as ostanat_iznos_dogvor,'
      '       (select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = ss.vid and bs.artikal ' +
        '= ss.sifra and b.tip_partner = :mas_tip_partner and b.partner = ' +
        ':mas_partner and b.broj_tender = :mas_broj_tender and bs.re like' +
        ' :re'
      '                    ),'
      '       ( (select coalesce(sum(gps.cena), 0) as planirana_cena'
      '          from jn_godisen_plan_sektori gps'
      
        '          where gps.vid_artikal = ss.vid and gps.artikal = ss.si' +
        'fra and gps.re like :re and gps.godina = ss.godina'
      '                    )'
      '         -(select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '           from jn_baranje_stavki bs'
      '           inner join jn_baranje b on b.broj = bs.broj'
      
        '           where bs.vid_artikal = ss.vid and bs.artikal = ss.sif' +
        'ra and b.tip_partner = :mas_tip_partner and b.partner = :mas_par' +
        'tner and b.broj_tender = :mas_broj_tender and bs.re like :re'
      '        )) as dozvolena_cena,'
      '        (select coalesce(sum(gps.cena), 0) as planirana_cena'
      '          from jn_godisen_plan_sektori gps'
      
        '          where gps.vid_artikal = ss.vid and gps.artikal = ss.si' +
        'fra and gps.re like :re and gps.godina = ss.godina),'
      '       ss.artikalNaziv,'
      '       ss.jn_tip_artikal,'
      '       ss.vidNaziv,'
      '       ss.merka,'
      '       ss.merkaNaziv,'
      '       ss.grupa,'
      '       ss.grupaNaziv,'
      '       ss.prozvoditel,'
      '       ss.proizvoditelNaziv,'
      '       ss.jn_cpv,'
      '       ss.mk,en,'
      '       ss.generika'
      'from'
      '(select ds.vid_artikal as vid,'
      '        ds.artikal as sifra,'
      '        coalesce(ds.cena,0) as cena,'
      '        ma.naziv as artikalNaziv,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.generika,'
      '        t.godina,'
      '        ds.kolicina'
      'from jn_ponudi_stavki ds'
      
        'inner join mtr_artikal ma on ma.artvid = ds.vid_artikal and ma.i' +
        'd = ds.artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'inner join jn_tender_stavki_re ts on ts.broj_tender = ds.broj_te' +
        'nder and ts.vid_artikal = ds.vid_artikal and ts.artikal = ds.art' +
        'ikal and ts.re like :re'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'where ds.tip_partner = :mas_tip_partner and ds.partner = :mas_pa' +
        'rtner and ds.broj_tender = :mas_broj_tender) ss'
      'order by jn_tip_artikal, artikalNaziv')
    SelectSQL.Strings = (
      ' select ss.godina,'
      '       ss.vid,'
      '       ss.sifra,'
      '       ss.kolicina,'
      '       ss.cena,'
      '       (select coalesce(d.iznos,0) iznos_dogovor'
      '        from jn_dogovor d'
      
        '        where d.tip_partner = :mas_tip_partner and d.partner = :' +
        'mas_partner and d.broj_tender = :mas_broj_tender),'
      '       cast(((select coalesce(d.iznos,0) iznos_dogovor'
      '        from jn_dogovor d'
      
        '        where d.tip_partner = :mas_tip_partner and d.partner = :' +
        'mas_partner and d.broj_tender = :mas_broj_tender) -'
      '        (select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where b.tip_partner = :mas_tip_partner and b.' +
        'partner = :mas_partner and b.broj_tender = :mas_broj_tender'
      
        '                    ))as double precision) as ostanat_iznos_dogv' +
        'or,'
      '       (select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '                   from jn_baranje_stavki bs'
      '                   inner join jn_baranje b on b.broj = bs.broj'
      
        '                   where bs.vid_artikal = ss.vid and bs.artikal ' +
        '= ss.sifra and b.tip_partner = :mas_tip_partner and b.partner = ' +
        ':mas_partner and b.broj_tender = :mas_broj_tender and bs.re like' +
        ' :re'
      '                    ),'
      '       ( (select coalesce(sum(gps.cena), 0) as planirana_cena'
      '          from jn_godisen_plan_sektori gps'
      
        '          where gps.vid_artikal = ss.vid and gps.artikal = ss.si' +
        'fra and gps.re like :re and gps.godina = ss.godina'
      '                    )'
      '         -(select coalesce(sum(bs.cena), 0) as pobarana_cena'
      '           from jn_baranje_stavki bs'
      '           inner join jn_baranje b on b.broj = bs.broj'
      
        '           where bs.vid_artikal = ss.vid and bs.artikal = ss.sif' +
        'ra and b.tip_partner = :mas_tip_partner and b.partner = :mas_par' +
        'tner and b.broj_tender = :mas_broj_tender and bs.re like :re'
      '        )) as dozvolena_cena,'
      '        (select coalesce(sum(gps.cena), 0) as planirana_cena'
      '          from jn_godisen_plan_sektori gps'
      
        '          where gps.vid_artikal = ss.vid and gps.artikal = ss.si' +
        'fra and gps.re like :re and gps.godina = ss.godina),'
      '       ss.artikalNaziv,'
      '       ss.jn_tip_artikal,'
      '       ss.vidNaziv,'
      '       ss.merka,'
      '       ss.merkaNaziv,'
      '       ss.grupa,'
      '       ss.grupaNaziv,'
      '       ss.prozvoditel,'
      '       ss.proizvoditelNaziv,'
      '       ss.jn_cpv,'
      '       ss.mk,en,'
      '       ss.generika'
      'from'
      '(select ds.vid_artikal as vid,'
      '        ds.artikal as sifra,'
      '        coalesce(ds.cena,0) as cena,'
      '        ma.naziv as artikalNaziv,'
      '        ma.jn_tip_artikal,'
      '        ta.naziv as vidNaziv,'
      '        ma.merka,'
      '        mm.naziv as merkaNaziv,'
      '        ma.grupa,'
      '        mg.naziv as grupaNaziv,'
      '        ma.prozvoditel,'
      '        mp.naziv as proizvoditelNaziv,'
      '        ma.jn_cpv,'
      '        cpv.mk, cpv.en,'
      '        ma.generika,'
      '        t.godina,'
      '        ds.kolicina'
      'from jn_ponudi_stavki ds'
      
        'inner join mtr_artikal ma on ma.artvid = ds.vid_artikal and ma.i' +
        'd = ds.artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join jn_cpv cpv on cpv.code = ma.jn_cpv'
      
        'inner join jn_tender_stavki_re ts on ts.broj_tender = ds.broj_te' +
        'nder and ts.vid_artikal = ds.vid_artikal and ts.artikal = ds.art' +
        'ikal and ts.re like :re'
      'inner join jn_tender t on t.broj = ts.broj_tender'
      
        'where ds.tip_partner = :mas_tip_partner and ds.partner = :mas_pa' +
        'rtner and ds.broj_tender = :mas_broj_tender) ss'
      'order by jn_tip_artikal, artikalNaziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsBaranja
    Left = 2120
    Top = 296
    object tblIzborStavkiBaranjePoCenaARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblIzborStavkiBaranjePoCenaVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaMERKANAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'MERKANAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaPROZVODITEL: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'PROZVODITEL'
    end
    object tblIzborStavkiBaranjePoCenaPROIZVODITELNAZIV: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083
      FieldName = 'PROIZVODITELNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaMK: TFIBStringField
      DisplayLabel = #1052#1072#1082#1077#1076#1086#1085#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'MK'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaEN: TFIBStringField
      DisplayLabel = #1040#1085#1075#1083#1080#1089#1082#1080' '#1085#1072#1079#1080#1074
      FieldName = 'EN'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborStavkiBaranjePoCenaGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074' '
      FieldName = 'GENERIKA'
    end
    object tblIzborStavkiBaranjePoCenaCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'CENA'
      DisplayFormat = '0.0000, .'
    end
    object tblIzborStavkiBaranjePoCenaVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID'
    end
    object tblIzborStavkiBaranjePoCenaSIFRA: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object tblIzborStavkiBaranjePoCenaGODINA: TFIBSmallIntField
      FieldName = 'GODINA'
    end
    object tblIzborStavkiBaranjePoCenaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiBaranjePoCenaIZNOS_DOGOVOR: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'IZNOS_DOGOVOR'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblIzborStavkiBaranjePoCenaOSTANAT_IZNOS_DOGVOR: TFIBFloatField
      DisplayLabel = #1054#1089#1090#1072#1085#1072#1090' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'OSTANAT_IZNOS_DOGVOR'
      DisplayFormat = '0.00 , .'
    end
    object tblIzborStavkiBaranjePoCenaPOBARANA_CENA: TFIBFloatField
      DisplayLabel = #1055#1086#1073#1072#1088#1072#1085#1072' '#1094#1077#1085#1072
      FieldName = 'POBARANA_CENA'
      DisplayFormat = '0.0000 , .'
    end
    object tblIzborStavkiBaranjePoCenaDOZVOLENA_CENA: TFIBFloatField
      DisplayLabel = #1044#1086#1079#1074#1086#1083#1077#1085#1072' '#1094#1077#1085#1072
      FieldName = 'DOZVOLENA_CENA'
      DisplayFormat = '0.0000 , .'
    end
    object tblIzborStavkiBaranjePoCenaPLANIRANA_CENA: TFIBFloatField
      FieldName = 'PLANIRANA_CENA'
      DisplayFormat = '0.0000 , .'
    end
  end
  object dsIzborStavkiBaranjePoCena: TDataSource
    DataSet = tblIzborStavkiBaranjePoCena
    Left = 2280
    Top = 296
  end
  object dsDinamikaRealizacija: TDataSource
    DataSet = tblDinamikaRealizacija
    Left = 456
    Top = 304
  end
  object tblDinamikaRealizacija: TpFIBDataSet
    SelectSQL.Strings = (
      'select  coalesce(sum(gps.kolicina),0) as kolicina,'
      '        gps.godina,'
      '        gps.vid_artikal,'
      '        ma.jn_tip_artikal,'
      '        mav.naziv as vidArtikalNaziv,'
      '        gps.artikal,'
      '        ma.naziv as artikalNaziv,'
      '        ma.merka,'
      '        ma.grupa,'
      '        mag.naziv as grupaNaziv,'
      '        ma.jn_cpv,'
      '        ma.generika,'
      
        '        gps.tip_nabavka,tn.naziv as tipNabavkaNaziv, gps.mesec, ' +
        'gps.predmet_nabavka'
      'from jn_godisen_plan_sektori gps'
      
        'inner join mtr_artikal ma on ma.id = gps.artikal and ma.artvid =' +
        ' gps.vid_artikal'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      'left outer join jn_tip_nabavka tn on tn.sifra = gps.tip_nabavka'
      'where gps.godina = :mas_godina'
      
        'group by gps.godina, gps.vid_artikal, ma.jn_tip_artikal, vidArti' +
        'kalNaziv,'
      '         gps.artikal,artikalNaziv, ma.merka,'
      
        '         ma.grupa,grupaNaziv, ma.jn_cpv, ma.generika,gps.tip_nab' +
        'avka,tn.naziv, gps.mesec, gps.predmet_nabavka'
      'order by  ma.jn_tip_artikal, ma.grupa')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsGodPlanOdlulka
    Left = 304
    Top = 304
    object tblDinamikaRealizacijaGODINA: TFIBSmallIntField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblDinamikaRealizacijaVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076'_'#1040#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblDinamikaRealizacijaJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
    object tblDinamikaRealizacijaVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblDinamikaRealizacijaARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' '
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaGRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaJN_CPV: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1089#1087#1086#1088#1077#1076' '#1054#1055#1032#1053
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaGENERIKA: TFIBIntegerField
      DisplayLabel = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
      FieldName = 'GENERIKA'
    end
    object tblDinamikaRealizacijaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblDinamikaRealizacijaTIP_NABAVKA: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP_NABAVKA'
    end
    object tblDinamikaRealizacijaTIPNABAVKANAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'TIPNABAVKANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaMESEC: TFIBStringField
      DisplayLabel = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      FieldName = 'MESEC'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDinamikaRealizacijaPREDMET_NABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET_NABAVKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblTipNabavkaAktivni: TpFIBDataSet
    SelectSQL.Strings = (
      'select tn.sifra,'
      '       tn.naziv,'
      '       tn.denovi_zalba,'
      '       tn.opis,'
      '       tn.tip_god_plan,'
      '       case when tn.tip_god_plan = 1 then '#39#1053#1077#39
      '            when tn.tip_god_plan = 0 then '#39#1044#1072#39
      '       end "tip_god_plan_Naziv",'
      '       tn.aktiven,'
      '       tn.ts_ins,'
      '       tn.ts_upd,'
      '       tn.usr_ins,'
      '       tn.usr_upd'
      'from jn_tip_nabavka tn'
      'where tn.aktiven = 1'
      'order by tn.sifra')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 40
    Top = 272
    object FIBSmallIntField1: TFIBSmallIntField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'SIFRA'
    end
    object FIBStringField5: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField6: TFIBIntegerField
      DisplayLabel = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1078#1072#1083#1073#1072
      FieldName = 'DENOVI_ZALBA'
    end
    object FIBStringField6: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBDateTimeField1: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object FIBDateTimeField2: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object FIBStringField7: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField8: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBSmallIntField2: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1064#1080#1092#1088#1072')'
      FieldName = 'TIP_GOD_PLAN'
    end
    object FIBStringField9: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      FieldName = 'tip_god_plan_Naziv'
      Size = 2
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBSmallIntField3: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
  end
  object dsTipNabavkaAktivni: TDataSource
    DataSet = tblTipNabavkaAktivni
    Left = 144
    Top = 272
  end
  object tblIzborBrojDogovorZaBaranje: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.broj_dogovor,'
      '       d.broj_tender,'
      '       d.partner,'
      '       d.tip_partner'
      'from jn_dogovor d'
      
        'where d.broj_tender like :broj_tender and d.broj_dogovor like :b' +
        'roj_dogovor')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 2120
    Top = 360
    object tblIzborBrojDogovorZaBaranjeBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborBrojDogovorZaBaranjeBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborBrojDogovorZaBaranjePARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblIzborBrojDogovorZaBaranjeTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
  end
  object dsIzborBrojDogovorZaBaranje: TDataSource
    DataSet = tblIzborBrojDogovorZaBaranje
    Left = 2280
    Top = 360
  end
  object tblIzborBrojTenderZaBaranje: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.broj'
      'from jn_tender t'
      
        'where t.ponisten = 1 and t.zatvoren <> 1 and t.status = 1 and t.' +
        'broj like :broj_tender')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 2120
    Top = 416
    object tblIzborBrojTenderZaBaranjeBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborBrojTenderZaBaranje: TDataSource
    DataSet = tblIzborBrojTenderZaBaranje
    Left = 2280
    Top = 416
  end
  object tblVidDokument: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_VID_DOKUMENT'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    PATEKA = :PATEKA,'
      '    TEMPLATE_FRF = :TEMPLATE_FRF,'
      '    BROJ_FRF = :BROJ_FRF,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_VID_DOKUMENT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_VID_DOKUMENT('
      '    ID,'
      '    NAZIV,'
      '    PATEKA,'
      '    TEMPLATE_FRF,'
      '    BROJ_FRF,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :PATEKA,'
      '    :TEMPLATE_FRF,'
      '    :BROJ_FRF,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select vd.id,'
      '       vd.naziv,'
      '       vd.pateka,'
      '       vd.template_frf,'
      '       vd.broj_frf,'
      '       vd.ts_ins,'
      '       vd.ts_upd,'
      '       vd.usr_ins,'
      '       vd.usr_upd'
      'from jn_vid_dokument vd'
      ''
      ' WHERE '
      '        VD.ID = :OLD_ID'
      'order by vd.naziv    ')
    SelectSQL.Strings = (
      'select vd.id,'
      '       vd.naziv,'
      '       vd.pateka,'
      '       vd.template_frf,'
      '       vd.broj_frf,'
      '       vd.ts_ins,'
      '       vd.ts_upd,'
      '       vd.usr_ins,'
      '       vd.usr_upd'
      'from jn_vid_dokument vd'
      'order by vd.naziv')
    AutoUpdateOptions.UpdateTableName = 'JN_VID_DOKUMENT'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_VID_DOKUMENT_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 56
    Top = 344
    object tblVidDokumentID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblVidDokumentNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidDokumentTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblVidDokumentTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblVidDokumentUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidDokumentUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidDokumentPATEKA: TFIBStringField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1090#1077#1088#1082
      FieldName = 'PATEKA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVidDokumentTEMPLATE_FRF: TFIBSmallIntField
      FieldName = 'TEMPLATE_FRF'
    end
    object tblVidDokumentBROJ_FRF: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112' sys_report'
      FieldName = 'BROJ_FRF'
    end
  end
  object dsVidDokument: TDataSource
    DataSet = tblVidDokument
    Left = 144
    Top = 336
  end
  object tblTenderDokumenti: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_DOKUMENTI'
      'SET '
      '    ID = :ID,'
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    VID_DOKUMENT = :VID_DOKUMENT,'
      '    TEMPLATE = :TEMPLATE,'
      '    DATUM = :DATUM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    KONTROLIRAL = :KONTROLIRAL,'
      '    RED_BR = :RED_BR'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_DOKUMENTI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_DOKUMENTI('
      '    ID,'
      '    BROJ_TENDER,'
      '    VID_DOKUMENT,'
      '    TEMPLATE,'
      '    DATUM,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    KONTROLIRAL,'
      '    RED_BR'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ_TENDER,'
      '    :VID_DOKUMENT,'
      '    :TEMPLATE,'
      '    :DATUM,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :KONTROLIRAL,'
      '    :RED_BR'
      ')')
    RefreshSQL.Strings = (
      'select td.id,'
      '       td.broj_tender,'
      '       td.vid_dokument,'
      '       vd.naziv as vidDokumentNaziv,'
      '       td.template,'
      '       td.datum,'
      '       td.ts_ins,'
      '       td.ts_upd,'
      '       td.usr_ins,'
      '       td.usr_upd,'
      '       td.kontroliral,'
      '       td.red_br'
      'from jn_tender_dokumenti td'
      'inner join jn_vid_dokument vd on vd.id = td.vid_dokument'
      'where(  td.broj_tender = :broj_tender'
      '     ) and (     TD.ID = :OLD_ID'
      '     )'
      'order by td.red_br   ')
    SelectSQL.Strings = (
      'select td.id,'
      '       td.broj_tender,'
      '       td.vid_dokument,'
      '       vd.naziv as vidDokumentNaziv,'
      '       td.template,'
      '       td.datum,'
      '       td.ts_ins,'
      '       td.ts_upd,'
      '       td.usr_ins,'
      '       td.usr_upd,'
      '       td.kontroliral,'
      '       td.red_br'
      'from jn_tender_dokumenti td'
      'inner join jn_vid_dokument vd on vd.id = td.vid_dokument'
      'where td.broj_tender = :broj_tender'
      'order by td.red_br')
    AutoUpdateOptions.UpdateTableName = 'JN_TENDER_DOKUMENTI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_TENDER_DOKUMENTI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 624
    Top = 272
    object tblTenderDokumentiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTenderDokumentiBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDokumentiVID_DOKUMENT: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'VID_DOKUMENT'
    end
    object tblTenderDokumentiVIDDOKUMENTNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'VIDDOKUMENTNAZIV'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDokumentiTEMPLATE: TFIBBlobField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'TEMPLATE'
      Size = 8
    end
    object tblTenderDokumentiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblTenderDokumentiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTenderDokumentiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTenderDokumentiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDokumentiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDokumentiKONTROLIRAL: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1083
      FieldName = 'KONTROLIRAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderDokumentiRED_BR: TFIBIntegerField
      DisplayLabel = #1056#1077#1076'. '#1041#1088#1086#1112
      FieldName = 'RED_BR'
    end
  end
  object dsTenderDokumenti: TDataSource
    DataSet = tblTenderDokumenti
    Left = 744
    Top = 272
  end
  object tblPotrebniDokumneti: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_POTREBNI_DOKUMENTI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TIP = :TIP,'
      '    GRUPA = :GRUPA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_POTREBNI_DOKUMENTI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_POTREBNI_DOKUMENTI('
      '    ID,'
      '    NAZIV,'
      '    TIP,'
      '    GRUPA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TIP,'
      '    :GRUPA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select pd.id,'
      '       pd.naziv,'
      '       pd.tip,'
      '       case when pd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when pd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073 +
        #1072#39
      
        '            when pd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086 +
        #1089#1086#1073#1085#1086#1089#1090#39
      
        '            when pd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090 +
        #1077#1090#39
      
        '            when pd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074 +
        #1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when pd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089 +
        #1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39'        '
      '        end "tipNaziv",'
      '       pd.grupa,'
      '       g.naziv as grupaNaziv,'
      '       pd.ts_ins,'
      '       pd.ts_upd,'
      '       pd.usr_ins,'
      '       pd.usr_upd'
      'from jn_potrebni_dokumenti pd'
      
        'left join jn_tender_potrebni_dok gps on gps.potrebni_dokumenti_i' +
        'd =pd.id and gps.broj_tender = :mas_broj'
      
        'left outer join jn_potrebni_dok_grupa g on g.id = pd.grupa and g' +
        '.tip = pd.tip'
      'where(  (gps.broj_tender is null and :param = 1)or (:param = 0)'
      '     ) and (     PD.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select pd.id,'
      '       pd.naziv,'
      '       pd.tip,'
      '       case when pd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when pd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073 +
        #1072#39
      
        '            when pd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086 +
        #1089#1086#1073#1085#1086#1089#1090#39
      
        '            when pd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090 +
        #1077#1090#39
      
        '            when pd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074 +
        #1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when pd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089 +
        #1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39'        '
      '        end "tipNaziv",'
      '       pd.grupa,'
      '       g.naziv as grupaNaziv,'
      '       pd.ts_ins,'
      '       pd.ts_upd,'
      '       pd.usr_ins,'
      '       pd.usr_upd'
      'from jn_potrebni_dokumenti pd'
      
        'left join jn_tender_potrebni_dok gps on gps.potrebni_dokumenti_i' +
        'd =pd.id and gps.broj_tender = :mas_broj'
      
        'left outer join jn_potrebni_dok_grupa g on g.id = pd.grupa and g' +
        '.tip = pd.tip'
      'where (gps.broj_tender is null and :param = 1)or (:param = 0)'
      'order by pd.tip,pd.grupa, pd.naziv')
    AutoUpdateOptions.UpdateTableName = 'JN_POTREBNI_DOKUMENTI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_POTREBNI_DOKUMENTI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 408
    object tblPotrebniDokumnetiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPotrebniDokumnetiTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblPotrebniDokumnetitipNaziv: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'tipNaziv'
      Size = 45
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPotrebniDokumnetiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPotrebniDokumnetiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPotrebniDokumnetiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPotrebniDokumnetiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPotrebniDokumnetiNAZIV: TFIBMemoField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'NAZIV'
      BlobType = ftMemo
      Size = 8
    end
    object tblPotrebniDokumnetiGRUPA: TFIBIntegerField
      DisplayLabel = #1043#1088#1091#1087#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'GRUPA'
    end
    object tblPotrebniDokumnetiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPotrebniDokumneti: TDataSource
    DataSet = tblPotrebniDokumneti
    Left = 144
    Top = 408
  end
  object tblTenderPotrebniDok: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_POTREBNI_DOK'
      'SET '
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    POTREBNI_DOKUMENTI_ID = :POTREBNI_DOKUMENTI_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and POTREBNI_DOKUMENTI_ID = :OLD_POTREBNI_DOKUMENTI_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_POTREBNI_DOK'
      'WHERE'
      '        BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and POTREBNI_DOKUMENTI_ID = :OLD_POTREBNI_DOKUMENTI_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TENDER_POTREBNI_DOK('
      '    BROJ_TENDER,'
      '    POTREBNI_DOKUMENTI_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :POTREBNI_DOKUMENTI_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select tpd.broj_tender,'
      '       tpd.potrebni_dokumenti_id,'
      '       pd.naziv,'
      '       pd.tip,'
      '       case when pd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when pd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073 +
        #1072#39
      
        '            when pd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086 +
        #1089#1086#1073#1085#1086#1089#1090#39
      
        '            when pd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090 +
        #1077#1090#39
      
        '            when pd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074 +
        #1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when pd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089 +
        #1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39
      '       end "tipNaziv",'
      '       g.naziv as grupaNaziv,'
      '       tpd.ts_ins,'
      '       tpd.ts_upd,'
      '       tpd.usr_ins,'
      '       tpd.usr_upd'
      'from jn_tender_potrebni_dok tpd'
      
        'inner join jn_potrebni_dokumenti pd on tpd.potrebni_dokumenti_id' +
        '= pd.id'
      'left outer join jn_potrebni_dok_grupa g on g.id = pd.grupa'
      'where(  tpd.broj_tender = :mas_broj'
      '     ) and (     TPD.BROJ_TENDER = :OLD_BROJ_TENDER'
      '    and TPD.POTREBNI_DOKUMENTI_ID = :OLD_POTREBNI_DOKUMENTI_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select tpd.broj_tender,'
      '       tpd.potrebni_dokumenti_id,'
      '       pd.naziv,'
      '       pd.tip,'
      '       case when pd.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when pd.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073 +
        #1072#39
      
        '            when pd.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086 +
        #1089#1086#1073#1085#1086#1089#1090#39
      
        '            when pd.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090 +
        #1077#1090#39
      
        '            when pd.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074 +
        #1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when pd.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089 +
        #1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39
      '       end "tipNaziv",'
      '       g.naziv as grupaNaziv,'
      '       tpd.ts_ins,'
      '       tpd.ts_upd,'
      '       tpd.usr_ins,'
      '       tpd.usr_upd'
      'from jn_tender_potrebni_dok tpd'
      
        'inner join jn_potrebni_dokumenti pd on tpd.potrebni_dokumenti_id' +
        '= pd.id'
      'left outer join jn_potrebni_dok_grupa g on g.id = pd.grupa'
      'where tpd.broj_tender = :mas_broj'
      'order by pd.tip, pd.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 624
    Top = 336
    object tblTenderPotrebniDokBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPotrebniDokPOTREBNI_DOKUMENTI_ID: TFIBIntegerField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'POTREBNI_DOKUMENTI_ID'
    end
    object tblTenderPotrebniDokNAZIV: TFIBMemoField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV'
      BlobType = ftMemo
      Size = 8
    end
    object tblTenderPotrebniDokTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblTenderPotrebniDoktipNaziv: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'tipNaziv'
      Size = 45
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPotrebniDokTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTenderPotrebniDokTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTenderPotrebniDokUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPotrebniDokUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTenderPotrebniDokGRUPANAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPANAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTenderPotrebniDok: TDataSource
    DataSet = tblTenderPotrebniDok
    Left = 752
    Top = 336
  end
  object tblPonuduvacPotrebniDok: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PONUDUVAC_DOK'
      'SET '
      '    PONUDA_ID = :PONUDA_ID,'
      '    DOSTAVEN_DOKUMENT = :DOSTAVEN_DOKUMENT,'
      '    DOSTAVEN = :DOSTAVEN,'
      '    VALIDNOST = :VALIDNOST,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    PONUDA_ID = :OLD_PONUDA_ID'
      '    and DOSTAVEN_DOKUMENT = :OLD_DOSTAVEN_DOKUMENT'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_PONUDUVAC_DOK'
      'WHERE'
      '        PONUDA_ID = :OLD_PONUDA_ID'
      '    and DOSTAVEN_DOKUMENT = :OLD_DOSTAVEN_DOKUMENT'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_PONUDUVAC_DOK('
      '    PONUDA_ID,'
      '    DOSTAVEN_DOKUMENT,'
      '    DOSTAVEN,'
      '    VALIDNOST,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :PONUDA_ID,'
      '    :DOSTAVEN_DOKUMENT,'
      '    :DOSTAVEN,'
      '    :VALIDNOST,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select pd.ponuda_id,'
      '       pd.dostaven_dokument,'
      '       d.id,'
      '       d.naziv,'
      '       d.tip,'
      '       case when d.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when d.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072 +
        #39
      
        '            when d.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089 +
        #1086#1073#1085#1086#1089#1090#39
      
        '            when d.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077 +
        #1090#39
      
        '            when d.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086 +
        #1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when d.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080 +
        #1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39
      '       end "tipNaziv",'
      '       pd.dostaven,'
      '       pd.validnost,'
      '       pd.ts_ins,'
      '       pd.ts_upd,'
      '       pd.usr_ins,'
      '       pd.usr_upd'
      'from jn_ponuduvac_dok pd'
      
        'inner join jn_potrebni_dokumenti d on d.id = pd.dostaven_dokumen' +
        't'
      'where(  pd.ponuda_id = :mas_id'
      '     ) and (     PD.PONUDA_ID = :OLD_PONUDA_ID'
      '    and PD.DOSTAVEN_DOKUMENT = :OLD_DOSTAVEN_DOKUMENT'
      '     )'
      '    '
      'order by d.tip, d.naziv')
    SelectSQL.Strings = (
      'select pd.ponuda_id,'
      '       pd.dostaven_dokument,'
      '       d.id,'
      '       d.naziv,'
      '       d.tip,'
      '       case when d.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when d.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072 +
        #39
      
        '            when d.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089 +
        #1086#1073#1085#1086#1089#1090#39
      
        '            when d.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077 +
        #1090#39
      
        '            when d.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086 +
        #1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when d.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080 +
        #1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39
      '       end "tipNaziv",'
      '       pd.dostaven,'
      '       pd.validnost,'
      '       pd.ts_ins,'
      '       pd.ts_upd,'
      '       pd.usr_ins,'
      '       pd.usr_upd'
      'from jn_ponuduvac_dok pd'
      
        'inner join jn_potrebni_dokumenti d on d.id = pd.dostaven_dokumen' +
        't'
      'where pd.ponuda_id = :mas_id'
      'order by d.tip, d.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPonudi
    Left = 1280
    Top = 352
    object tblPonuduvacPotrebniDokPONUDA_ID: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'PONUDA_ID'
    end
    object tblPonuduvacPotrebniDokDOSTAVEN_DOKUMENT: TFIBIntegerField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'DOSTAVEN_DOKUMENT'
    end
    object tblPonuduvacPotrebniDokNAZIV: TFIBMemoField
      DisplayLabel = #1044#1086#1082#1091#1084#1077#1085#1090' - '#1085#1072#1079#1080#1074
      FieldName = 'NAZIV'
      BlobType = ftMemo
      Size = 8
    end
    object tblPonuduvacPotrebniDokTIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblPonuduvacPotrebniDoktipNaziv: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      FieldName = 'tipNaziv'
      Size = 42
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonuduvacPotrebniDokDOSTAVEN: TFIBIntegerField
      DisplayLabel = #1044#1086#1089#1090#1072#1074#1077#1085
      FieldName = 'DOSTAVEN'
    end
    object tblPonuduvacPotrebniDokTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPonuduvacPotrebniDokTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPonuduvacPotrebniDokUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonuduvacPotrebniDokUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPonuduvacPotrebniDokVALIDNOST: TFIBStringField
      DisplayLabel = #1042#1072#1083#1080#1076#1085#1086#1089#1090
      FieldName = 'VALIDNOST'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPonuduvacPotrebniDok: TDataSource
    DataSet = tblPonuduvacPotrebniDok
    Left = 1416
    Top = 352
  end
  object tblRealizacijaPlanNabavkaRe: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '      rt.broj_tender,'
      '      rt.artikal,'
      '      rt.vid_artikal,'
      '      rt.katalog,'
      '      rt.artnaziv,'
      '      rt.vidnaziv,'
      '      rt.re,'
      '      rt.renaziv,'
      '      rt.planirana_kolicina,'
      '      rt.cena, rt.iznos_plan,'
      '      rt.raspishana_kolicina,'
      '      rt.ponudenacena,'
      '      rt.iznos_ponuda_edinicen,'
      '      rt.tip_partner, rt.partner,'
      '      rt.dobiena_kolicina,'
      '      rt.broj_dogovor, rt.datum_dogovor,'
      '      rt.datum_vazenje, rt.iznos_dogovor,'
      '      rt.isporacana_kolicina, rt.pobarana_kolicina,'
      '      rt.iznos_pobaran_edinicen, rt.cena_baranje,'
      '      rt.partnernaziv, rt.merka, rt.ponudeniznos,'
      '      rt.iznos_fakturiran, rt.iznos_pobaran,'
      '      rt.iznos_odluka, rt.vkupno_plan_iznos,'
      '      rt.iznos_dogovor_stavki, rt.stavka_naziv,'
      '      rt.stavka_merka'
      ' from proc_jn_realizacija_tender(:mas_broj)    rt'
      '/*select distinct  ts.broj_tender,'
      
        '      ts.artikal,ts.vid_artikal,a.katalog, a.naziv artnaziv, v.n' +
        'aziv vidnaziv,'
      '      ts.re, r.naziv as reNaziv,'
      
        '      gps.kolicina planirana_kolicina, gps.cena, (gps.kolicina *' +
        ' gps.cena) as iznos_plan,'
      '      ts.kolicina raspishana_kolicina,'
      '      ps.cena ponudenacena,'
      '      (ps.cena * td.kolicina)as iznos_ponuda_edinicen,'
      '      td.tip_partner,td.partner,td.kolicina dobiena_kolicina,'
      '      d.broj_dogovor,'
      
        '      dog.datum datum_dogovor,dog.datum_vazenje, dog.iznos as iz' +
        'nos_dogovor,'
      
        '      coalesce(tp.kolicina,0)+coalesce(tpl.kolicina,0) isporacan' +
        'a_kolicina,'
      '      tb.kolicina pobarana_kolicina,'
      '      tb.iznos  as iznos_pobaran_edinicen,'
      '      tb.cena as cena_baranje,'
      '      p.naziv partnernaziv,a.merka,'
      '      cast (ip.ponuden as numeric(15,2)) ponudeniznos,'
      
        '      cast (ifa.iznos_fakturiran as numeric(15,2)) iznos_fakturi' +
        'ran ,'
      '      cast(ipo.pobaran as numeric (15,2)) iznos_pobaran ,'
      '      coalesce(t.iznos,0)iznos_odluka,'
      '      vp.vkupno_plan_iznos,'
      '      ids.iznos_dogovor_stavki,'
      '      tsa.naziv as stavka_naziv,'
      '      tsa.merka as stavka_merka'
      ' from jn_tender_stavki_re ts'
      ' inner join jn_tender t on (t.broj=ts.broj_tender)'
      ' inner join mat_re r on r.id=ts.re'
      
        ' inner join mtr_artikal a on (a.artvid=ts.vid_artikal and a.id=t' +
        's.artikal)'
      ' inner join jn_tip_artikal v on (v.id = a.jn_tip_artikal)'
      
        ' inner join jn_godisen_plan_sektori gps on gps.vid_artikal=ts.vi' +
        'd_artikal and gps.artikal=ts.artikal and gps.re=ts.re and gps.go' +
        'dina=t.godina'
      
        ' inner join view_jn_vkupno_plan vp on vp.re=ts.re and vp.godina=' +
        't.godina'
      
        ' left outer join jn_tender_dobitnici td on (td.broj_tender=ts.br' +
        'oj_tender and td.artikal=ts.artikal and td.vid_artikal=ts.vid_ar' +
        'tikal)'
      
        ' left outer join view_jn_iznos_ponuden ip on (ts.broj_tender=ip.' +
        'broj and ip.partner = td.partner and ip.tip_partner = td.tip_par' +
        'tner)'
      
        ' left outer join jn_ponudi_stavki ps on ps.broj_tender=td.broj_t' +
        'ender and ps.tip_partner=td.tip_partner and ps.partner=td.partne' +
        'r and ps.vid_artikal=td.vid_artikal and ps.artikal=td.artikal'
      
        ' left outer join jn_dogovor_stavki d on d.vid_artikal=ts.vid_art' +
        'ikal AND D.artikal=ts.artikal and d.broj_tender = ts.broj_tender'
      
        ' left outer join jn_dogovor dog on dog.broj_dogovor=d.broj_dogov' +
        'or and dog.broj_tender=td.broj_tender and dog.tip_partner=td.tip' +
        '_partner and dog.partner=td.partner'
      
        ' left outer join view_jn_iznos_dogovor ids on ids.broj_tender = ' +
        'ts.broj_tender and ids.partner = td.partner and ids.tip_partner ' +
        '= td.tip_partner and ids.broj_dogovor = dog.broj_dogovor'
      
        ' left outer join view_jn_iznos_pobaran ipo on (ipo.broj=ts.broj_' +
        'tender and ipo.re = ts.re and ipo.partner = ids.partner and ipo.' +
        'tip_partner = ids.tip_partner and ipo.broj_dogovor = ids.broj_do' +
        'govor)'
      
        ' left outer join view_jn_barano tb on (tb.tender=ts.broj_tender ' +
        'and tb.artikal=ts.artikal and tb.vid_artikal=ts.vid_artikal and ' +
        'tb.partner=ids.partner and tb.tip_partner=ids.tip_partner and tb' +
        '.re=ts.re)'
      
        ' left outer join view_jn_primeno tp on (tp.artikal=ts.artikal an' +
        'd tp.vid_artikal=ts.vid_artikal and'
      
        '                                        tp.tender=ts.broj_tender' +
        ' and tp.partner=td.partner and tp.tip_partner=td.tip_partner  an' +
        'd tp.re=ts.re)'
      
        ' left outer join ten_primeno_lani tpl on (tpl.artikal=ts.artikal' +
        ' and'
      
        '                                  tpl.vid_artikal=ts.vid_artikal' +
        ' and tpl.tender=ts.broj_tender and'
      
        '                                  tpl.partner=td.partner and tpl' +
        '.tip_partner=td.tip_partner )'
      
        ' left outer join view_jn_iznos_fakturiran ifa on (ts.broj_tender' +
        '=ifa.broj)'
      
        ' left outer  join mat_partner p on (p.id=td.partner and p.tip_pa' +
        'rtner=td.tip_partner)'
      
        ' inner join jn_tender_stavki tsa on tsa.broj_tender = ts.broj_te' +
        'nder and tsa.artikal = ts.artikal and tsa.vid_artikal = ts.vid_a' +
        'rtikal'
      ''
      ' where ts.broj_tender=:mas_broj'
      ' order by ts.re, td.tip_partner, td.partner,a.artvid'
      ' */')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 616
    Top = 400
    object tblRealizacijaPlanNabavkaReBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblRealizacijaPlanNabavkaReVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID_ARTIKAL'
    end
    object tblRealizacijaPlanNabavkaReKATALOG: TFIBStringField
      DisplayLabel = #1050#1072#1090#1083'. '#1073#1088'.'
      FieldName = 'KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReARTNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReVIDNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'VIDNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblRealizacijaPlanNabavkaReRENAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1045#1076#1080#1085#1080#1094#1072
      FieldName = 'RENAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaRePLANIRANA_KOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' - '#1055#1083#1072#1085
      FieldName = 'PLANIRANA_KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaRePONUDENACENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042' - '#1055#1086#1085#1091#1076#1072
      FieldName = 'PONUDENACENA'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReRASPISHANA_KOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' - '#1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'RASPISHANA_KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1087#1087#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP_PARTNER'
    end
    object tblRealizacijaPlanNabavkaRePARTNER: TFIBIntegerField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
      FieldName = 'PARTNER'
    end
    object tblRealizacijaPlanNabavkaReDOBIENA_KOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' - '#1044#1086#1073#1080#1090#1085#1080#1082
      FieldName = 'DOBIENA_KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReBROJ_DOGOVOR: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'BROJ_DOGOVOR'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReDATUM_DOGOVOR: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DATUM_DOGOVOR'
    end
    object tblRealizacijaPlanNabavkaReDATUM_VAZENJE: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      FieldName = 'DATUM_VAZENJE'
    end
    object tblRealizacijaPlanNabavkaReISPORACANA_KOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' - '#1048#1089#1087#1086#1088#1072#1082#1072
      FieldName = 'ISPORACANA_KOLICINA'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaRePARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReMERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaRePONUDENIZNOS: TFIBBCDField
      DisplayLabel = #1042#1082#1091#1087#1077#1085' '#1080#1079#1085#1086#1089' - '#1055#1086#1085#1091#1076#1072'('#1044#1086#1073#1080#1090#1085#1072')'
      FieldName = 'PONUDENIZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReIZNOS_FAKTURIRAN: TFIBBCDField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' - '#1060#1072#1082#1090#1091#1088#1080#1088#1072#1085
      FieldName = 'IZNOS_FAKTURIRAN'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReIZNOS_POBARAN: TFIBBCDField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' - '#1041#1072#1088#1072#1114#1077
      FieldName = 'IZNOS_POBARAN'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReIZNOS_ODLUKA: TFIBBCDField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' - '#1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'IZNOS_ODLUKA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReIZNOS_PLAN: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042' - '#1055#1083#1072#1085
      FieldName = 'IZNOS_PLAN'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042' - '#1055#1083#1072#1085
      FieldName = 'CENA'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReVKUPNO_PLAN_IZNOS: TFIBFloatField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' - '#1055#1083#1072#1085
      FieldName = 'VKUPNO_PLAN_IZNOS'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReIZNOS_PONUDA_EDINICEN: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042' - '#1055#1086#1085#1091#1076#1072
      FieldName = 'IZNOS_PONUDA_EDINICEN'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReIZNOS_DOGOVOR_STAVKI: TFIBFloatField
      DisplayLabel = #1042#1082#1091#1087#1085#1086' '#1080#1079#1085#1086#1089' - '#1044#1086#1075#1086#1074#1086#1088
      FieldName = 'IZNOS_DOGOVOR_STAVKI'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReIZNOS_DOGOVOR: TFIBBCDField
      FieldName = 'IZNOS_DOGOVOR'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReIZNOS_POBARAN_EDINICEN: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042' - '#1041#1072#1088#1072#1114#1077
      FieldName = 'IZNOS_POBARAN_EDINICEN'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaRePOBARANA_KOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072' - '#1041#1072#1088#1072#1114#1077
      FieldName = 'POBARANA_KOLICINA'
      DisplayFormat = '0.00 , .'
      Size = 2
    end
    object tblRealizacijaPlanNabavkaReCENA_BARANJE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042' - '#1041#1072#1088#1072#1114#1077
      FieldName = 'CENA_BARANJE'
      DisplayFormat = '0.00 , .'
    end
    object tblRealizacijaPlanNabavkaReSTAVKA_NAZIV: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
      FieldName = 'STAVKA_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblRealizacijaPlanNabavkaReSTAVKA_MERKA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'STAVKA_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRealizacijaPlanNabavkaRe: TDataSource
    DataSet = tblRealizacijaPlanNabavkaRe
    Left = 760
    Top = 400
  end
  object tblEAukcija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_E_AUKCIJA'
      'SET '
      '    BROJ_TENDER = :BROJ_TENDER,'
      '    DATUM = :DATUM,'
      '    BROJ = :BROJ,'
      '    ZABELESKA = :ZABELESKA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_E_AUKCIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_E_AUKCIJA('
      '    ID,'
      '    BROJ_TENDER,'
      '    DATUM,'
      '    BROJ,'
      '    ZABELESKA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ_TENDER,'
      '    :DATUM,'
      '    :BROJ,'
      '    :ZABELESKA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select a.id,'
      '       a.broj_tender,'
      '       a.datum,'
      '       a.broj,'
      '       a.zabeleska,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd'
      'from jn_e_aukcija a'
      'where(  a.broj_tender = :mas_broj'
      '     ) and (     A.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select a.id,'
      '       a.broj_tender,'
      '       a.datum,'
      '       a.broj,'
      '       a.zabeleska,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd'
      'from jn_e_aukcija a'
      'where a.broj_tender = :mas_broj')
    AutoUpdateOptions.UpdateTableName = 'JN_E_AUKCIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_E_AUKCIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsТender
    Left = 1272
    Top = 424
    object tblEAukcijaBROJ_TENDER: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEAukcijaDATUM: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblEAukcijaBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEAukcijaZABELESKA: TFIBBlobField
      DisplayLabel = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1077'-'#1040#1091#1082#1094#1080#1112#1072
      FieldName = 'ZABELESKA'
      Size = 8
    end
    object tblEAukcijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblEAukcijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblEAukcijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEAukcijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEAukcijaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
  end
  object dsEAukcija: TDataSource
    DataSet = tblEAukcija
    Left = 1416
    Top = 424
  end
  object tblSifPotrebniDokGrupi: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_POTREBNI_DOK_GRUPA'
      'SET '
      '    TIP = :TIP,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_POTREBNI_DOK_GRUPA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_POTREBNI_DOK_GRUPA('
      '    ID,'
      '    TIP,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select g.id,'
      '       g.tip,'
      '       case when g.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when g.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072 +
        #39
      
        '            when g.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089 +
        #1086#1073#1085#1086#1089#1090#39
      
        '            when g.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077 +
        #1090#39
      
        '            when g.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086 +
        #1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when g.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080 +
        #1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39'        '
      '       end "tipNaziv",'
      '       g.naziv,'
      '       g.ts_ins,'
      '       g.ts_upd,'
      '       g.usr_ins,'
      '       g.usr_upd'
      'from jn_potrebni_dok_grupa g'
      'where(  g.tip like :tip'
      '     ) and (     G.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select g.id,'
      '       g.tip,'
      '       case when g.tip = 1 then '#39#1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072#39
      
        '            when g.tip = 2 then '#39#1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072 +
        #39
      
        '            when g.tip = 3 then '#39#1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089 +
        #1086#1073#1085#1086#1089#1090#39
      
        '            when g.tip = 4 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077 +
        #1090#39
      
        '            when g.tip = 5 then '#39#1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086 +
        #1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072#39
      
        '            when g.tip = 6 then '#39#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080 +
        #1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090#39'        '
      '       end "tipNaziv",'
      '       g.naziv,'
      '       g.ts_ins,'
      '       g.ts_upd,'
      '       g.usr_ins,'
      '       g.usr_upd'
      'from jn_potrebni_dok_grupa g'
      'where g.tip like :tip'
      'order by g.tip, g.naziv')
    AutoUpdateOptions.UpdateTableName = 'JN_POTREBNI_DOK_GRUPA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_POTREBNI_DOK_GRUPA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 472
    object tblSifPotrebniDokGrupiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblSifPotrebniDokGrupiTIP: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblSifPotrebniDokGrupiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 2000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSifPotrebniDokGrupiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblSifPotrebniDokGrupiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblSifPotrebniDokGrupiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSifPotrebniDokGrupiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSifPotrebniDokGrupitipNaziv: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'tipNaziv'
      Size = 45
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSifPotrebniDokGrupi: TDataSource
    DataSet = tblSifPotrebniDokGrupi
    Left = 144
    Top = 472
  end
  object tblProcTenderZatvori: TpFIBDataSet
    SelectSQL.Strings = (
      'select proc_jn_zatvori_tender.broj_tender_o'
      'from proc_jn_zatvori_tender(0)')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 616
    Top = 464
    object tblProcTenderZatvoriBROJ_TENDER_O: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER_O'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsProcTenderZatvori: TDataSource
    DataSet = tblProcTenderZatvori
    Left = 752
    Top = 464
  end
  object tblTrebovanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TREBUVANJE'
      'SET '
      '    GODINA = :GODINA,'
      '    RE = :RE,'
      '    BROJ = :BROJ,'
      '    DATUM = :DATUM,'
      '    OPIS = :OPIS,'
      '    RE2 = :RE2,'
      '    STATUS = :STATUS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TREBUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TREBUVANJE('
      '    ID,'
      '    GODINA,'
      '    RE,'
      '    BROJ,'
      '    DATUM,'
      '    OPIS,'
      '    RE2,'
      '    STATUS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :RE,'
      '    :BROJ,'
      '    :DATUM,'
      '    :OPIS,'
      '    :RE2,'
      '    :STATUS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select t.id,'
      '       t.godina,'
      '       t.re,'
      '       r.naziv as RE_NAZIV,'
      '       t.broj,'
      '       t.datum,'
      '       t.opis,'
      '       t.re2,'
      '       r2.naziv as RE2_naziv,'
      '       t.status,'
      '       case when t.status = 0 then '#39#1054#1090#1074#1086#1088#1077#1085#1086#39
      '            when t.status = 1 then '#39#1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1086#39
      '            when t.status = 2 then '#39#1057#1090#1086#1088#1085#1080#1088#1072#1085#1086#39
      '       end "StatusNaziv",'
      '       t.ts_ins,'
      '       t.ts_upd,'
      '       t.usr_ins,'
      '       t.usr_upd'
      'from jn_trebuvanje t'
      'inner join mat_re r on r.id = t.re'
      'left outer join mat_re r2 on r2.id = t.re2'
      
        'where(  t.godina = :godina and t.re like :re and t.status like :' +
        'status'
      '     ) and (     T.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select t.id,'
      '       t.godina,'
      '       t.re,'
      '       r.naziv as RE_NAZIV,'
      '       t.broj,'
      '       t.datum,'
      '       t.opis,'
      '       t.re2,'
      '       r2.naziv as RE2_naziv,'
      '       t.status,'
      '       case when t.status = 0 then '#39#1054#1090#1074#1086#1088#1077#1085#1086#39
      '            when t.status = 1 then '#39#1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1086#39
      '            when t.status = 2 then '#39#1057#1090#1086#1088#1085#1080#1088#1072#1085#1086#39
      '       end "StatusNaziv",'
      '       t.ts_ins,'
      '       t.ts_upd,'
      '       t.usr_ins,'
      '       t.usr_upd'
      'from jn_trebuvanje t'
      'inner join mat_re r on r.id = t.re'
      'left outer join mat_re r2 on r2.id = t.re2'
      
        'where t.godina = :godina and t.re like :re and t.status like :st' +
        'atus')
    AutoUpdateOptions.UpdateTableName = 'JN_TREBUVANJE'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_TREBUVANJE_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 2456
    Top = 48
    object tblTrebovanjeID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblTrebovanjeGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblTrebovanjeRE: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblTrebovanjeRE_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' - '#1085#1072#1079#1080#1074
      FieldName = 'RE_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblTrebovanjeDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblTrebovanjeOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeRE2: TFIBIntegerField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' 2 - '#1096#1080#1092#1088#1072
      FieldName = 'RE2'
    end
    object tblTrebovanjeRE2_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' 2 - '#1085#1072#1079#1080#1074
      FieldName = 'RE2_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblTrebovanjeStatusNaziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'StatusNaziv'
      Size = 11
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTrebovanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTrebovanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTrebovanje: TDataSource
    DataSet = tblTrebovanje
    Left = 2536
    Top = 48
  end
  object qMAXBrojTrebovanje: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(t.broj),0) broj'
      'from jn_trebuvanje t'
      'where t.godina = :godina and t.re = :re')
    Left = 2456
    Top = 112
  end
  object tblTrebovanjeStavki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TREBUVANJE_STAVKI'
      'SET '
      '    TREBUVANJE_ID = :TREBUVANJE_ID,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    KOLICINA = :KOLICINA,'
      '    OPIS = :OPIS,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_TREBUVANJE_STAVKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_TREBUVANJE_STAVKI('
      '    ID,'
      '    TREBUVANJE_ID,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    KOLICINA,'
      '    OPIS,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :TREBUVANJE_ID,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :KOLICINA,'
      '    :OPIS,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select ts.id,'
      '       ts.trebuvanje_id,'
      '       ts.vid_artikal,'
      '       ts.artikal,'
      '       ma.naziv as artikal_naziv,'
      '       ma.jn_tip_artikal,'
      '       mm.naziv as merka_naziv,'
      '       mav.naziv jn_grupa_naziv,'
      '       ts.kolicina,'
      '       ts.opis,'
      '       ts.ts_ins,'
      '       ts.ts_upd,'
      '       ts.usr_ins,'
      '       ts.usr_upd'
      'from jn_trebuvanje_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      'where(  ts.trebuvanje_id = :mas_id'
      '     ) and (     TS.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ts.id,'
      '       ts.trebuvanje_id,'
      '       ts.vid_artikal,'
      '       ts.artikal,'
      '       ma.naziv as artikal_naziv,'
      '       ma.jn_tip_artikal,'
      '       mm.naziv as merka_naziv,'
      '       mav.naziv jn_grupa_naziv,'
      '       ts.kolicina,'
      '       ts.opis,'
      '       ts.ts_ins,'
      '       ts.ts_upd,'
      '       ts.usr_ins,'
      '       ts.usr_upd'
      'from jn_trebuvanje_stavki ts'
      
        'inner join mtr_artikal ma on ma.id = ts.artikal and ma.artvid = ' +
        'ts.vid_artikal'
      'inner join mat_merka mm on mm.id = ma.merka'
      'left outer join mat_proizvoditel mp on mp.id = ma.prozvoditel'
      'left outer join mtr_artgrupa mag on mag.id = ma.grupa'
      'inner join jn_tip_artikal mav on mav.id = ma.jn_tip_artikal'
      'where ts.trebuvanje_id = :mas_id')
    AutoUpdateOptions.UpdateTableName = 'JN_TREBUVANJE_STAVKI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_TREBUVANJE_STAVKI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsTrebovanje
    Left = 2464
    Top = 176
    object tblTrebovanjeStavkiID: TFIBBCDField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
      Size = 0
    end
    object tblTrebovanjeStavkiTREBUVANJE_ID: TFIBBCDField
      DisplayLabel = #1058#1088#1077#1073#1086#1074#1072#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'TREBUVANJE_ID'
      Size = 0
    end
    object tblTrebovanjeStavkiVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' - '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'VID_ARTIKAL'
    end
    object tblTrebovanjeStavkiARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblTrebovanjeStavkiARTIKAL_NAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'ARTIKAL_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiMERKA_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072
      FieldName = 'MERKA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiJN_GRUPA_NAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076' - '#1085#1072#1079#1080#1074
      FieldName = 'JN_GRUPA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblTrebovanjeStavkiOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTrebovanjeStavkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblTrebovanjeStavkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTrebovanjeStavkiJN_TIP_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'JN_TIP_ARTIKAL'
    end
  end
  object dsTrebovanjeStavki: TDataSource
    DataSet = tblTrebovanjeStavki
    Left = 2568
    Top = 176
  end
  object tblPricinaPonistuvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_PRICINA_PONISTUVANJE'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    JN_PRICINA_PONISTUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO JN_PRICINA_PONISTUVANJE('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.naziv,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3'
      'from jn_pricina_ponistuvanje p'
      ''
      ' WHERE '
      '        P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.naziv,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3'
      'from jn_pricina_ponistuvanje p')
    AutoUpdateOptions.UpdateTableName = 'JN_PRICINA_PONISTUVANJE'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_JN_PRICINA_PONISTUVANJE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 536
    object tblPricinaPonistuvanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPricinaPonistuvanjeNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPricinaPonistuvanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPricinaPonistuvanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPricinaPonistuvanjeCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPricinaPonistuvanje: TDataSource
    DataSet = tblPricinaPonistuvanje
    Left = 144
    Top = 536
  end
  object qCountBrTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(t.broj) br'
      'from jn_tender t'
      'where t.broj = :broj')
    Left = 2690
    Top = 56
  end
  object qCountPonisteniStavki: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(t.broj_tender) br'
      'from jn_tender_stavki t'
      'where t.broj_tender = :broj and t.validnost = 0')
    Left = 2690
    Top = 120
  end
  object qCountStavkiTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(t.broj_tender) br'
      'from jn_tender_stavki t'
      'where t.broj_tender = :broj')
    Left = 2690
    Top = 184
  end
  object dsSetUp: TDataSource
    DataSet = tblSetUp
    Left = 144
    Top = 600
  end
  object tblSetUp: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_SETUP'
      'SET '
      '    P1 = :P1,'
      '    P2 = :P2,'
      '    V1 = :V1,'
      '    V2 = :V2,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE P1 = :OLD_P1 and '
      '      P2 = :OLD_P2 '
      '    ')
    RefreshSQL.Strings = (
      'select s.p1,'
      '       s.p2,'
      '       s.v1,'
      '       s.v2,'
      '       s.opis,'
      '       s.custom1,'
      '       s.custom2,'
      '       s.custom3,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd'
      'from jn_setup s'
      'WHERE P1 = :OLD_P1 and '
      '      P2 = :OLD_P2 ')
    SelectSQL.Strings = (
      'select s.p1,'
      '       s.p2,'
      '       s.v1,'
      '       s.v2,'
      '       s.opis,'
      '       s.custom1,'
      '       s.custom2,'
      '       s.custom3,'
      '       s.ts_ins,'
      '       s.ts_upd,'
      '       s.usr_ins,'
      '       s.usr_upd'
      'from jn_setup s')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 600
    object tblSetUpCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblSetUpTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblSetUpUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpP1: TFIBStringField
      DisplayLabel = #1055#1072#1088#1072#1084#1077#1090#1072#1088' 1'
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpP2: TFIBStringField
      DisplayLabel = #1055#1072#1088#1072#1084#1077#1090#1072#1088' 2'
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpV1: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090' 1'
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpV2: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090' 2'
      FieldName = 'V2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblGrupaStavkiBrisi: TpFIBDataSet
    SelectSQL.Strings = (
      'select  distinct ma.grupa,'
      '                 mg.naziv as grupaNaziv'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.artvid = ts.vid_artikal and ma.i' +
        'd = ts.artikal'
      'left outer join mtr_artgrupa mg on mg.id = ma.grupa'
      'where ts.broj_tender = :mas_broj and  ma.grupa is not null'
      'order by ma.grupa')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsТender
    Left = 936
    Top = 448
    object tblGrupaStavkiBrisiGRUPA: TFIBStringField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'GRUPA'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblGrupaStavkiBrisiGRUPANAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'GRUPANAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsGrupaStavkiBrisi: TDataSource
    DataSet = tblGrupaStavkiBrisi
    Left = 1088
    Top = 448
  end
  object qTenderStatus: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  t.status, t.zatvoren, t.ponisten'
      'from jn_tender t'
      'where t.broj = :broj')
    Left = 2688
    Top = 240
  end
  object qBrisiDobitnik: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'Select count(d.broj_dogovor) br'
      'from jn_dogovor d'
      
        'inner join jn_dogovor_stavki ds on ds.broj_dogovor = d.broj_dogo' +
        'vor'
      
        'where ds.vid_artikal = :vid_artikal and ds.artikal = :artikal an' +
        'd d.partner = :partner and d.tip_partner = :tip_partner and d.br' +
        'oj_tender = :broj_tender')
    Left = 1712
    Top = 360
  end
  object qSporedbaPlanPonuda: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select'
      '       ((select sum(ps.cenabezddv * tsr.kolicina)'
      '         from jn_godisen_plan_sektori ps'
      
        '         inner join  jn_tender_stavki_re tsr on ps.artikal = tsr' +
        '.artikal and ps.vid_artikal = tsr.vid_artikal'
      
        '                                                and tsr.re = ps.' +
        're and tsr.broj_tender =:tender and ps.godina = t.godina'
      
        '         where ps.godina = t.godina and ps.artikal = ts.artikal ' +
        'and ps.vid_artikal = ts.vid_artikal)'
      ''
      '       ) as iznos_bezddv_odlukajn,'
      
        '       ps.kolicina * ps.cenabezddv as iznos_bezddv_odluka_majpov' +
        'olen'
      ''
      ''
      ''
      'from jn_tender_stavki ts'
      'inner join jn_tender t on t.broj=ts.broj_tender'
      
        'inner join jn_ponudi_stavki ps on ps.broj_tender = ts.broj_tende' +
        'r'
      
        '                                  and ps.partner = :partner and ' +
        'ps.tip_partner =:tip_partner'
      
        '                                  and ts.vid_artikal = ps.vid_ar' +
        'tikal and ts.artikal = ps.artikal'
      
        'where ts.broj_tender=:tender and ts.vid_artikal =:vid_artikal an' +
        'd ts.artikal = :artikal')
    Left = 2688
    Top = 304
  end
  object qCountRaspisanaStavka: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(s.artikal) as br'
      'from jn_tender_stavki_re s'
      'inner join jn_tender t on t.broj = s.broj_tender'
      
        'where s.vid_artikal = :vid_artikal and s.artikal = :artikal and ' +
        's.re = :re  and t.godina = :godina')
    Left = 296
    Top = 384
  end
  object qCountRaspisanaStavkaOtvorenaP: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(s.artikal) as br'
      'from jn_tender_stavki_re s'
      'inner join jn_tender t on t.broj = s.broj_tender'
      'where s.vid_artikal = :vid_artikal and s.artikal = :artikal and '
      '      s.re = :re  and t.godina = :godina and t.ZATVOREN = 0')
    Left = 952
    Top = 520
  end
  object qCountPonudiPoTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) as br'
      'from jn_ponudi p'
      'where p.broj_tender = :tender')
    Left = 952
    Top = 584
  end
  object qCountBodiranjeStavki: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id)as br'
      'from jn_ponudi_stavki_bodirani p'
      'where p.broj_tender = :tender and p.vrednost is not null')
    Left = 1272
    Top = 496
  end
  object qCountDobitnici: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id)as br'
      'from jn_tender_dobitnici p'
      'where p.broj_tender = :tender')
    Left = 1272
    Top = 552
  end
  object qCountBaranjePoDogovor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(b.broj) as br'
      'from jn_baranje b'
      'where b.broj_dogovor = :broj_dogovor')
    Left = 1856
    Top = 232
  end
  object qCountBaranjeStavkiPoDogovor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(bs.id) as br'
      'from jn_baranje_stavki bs'
      'inner join jn_baranje b on b.broj = bs.broj'
      
        'where b.broj_dogovor = :broj_dogovor and bs.artikal = :artikal a' +
        'nd bs.vid_artikal = :vid_artikal')
    Left = 1856
    Top = 296
  end
  object qZemiPovtoruvackiPodatoci: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 t.datum,'
      '       t.lice_kontakt,'
      '       t.email_kontakt,'
      '       t.kontakt_tel_faks,'
      '       ti.lokacija_isporaka,'
      '       t.lice_realizacija,'
      '       t.lice_podgotovka,'
      '       ti.mesto_otvaranje,'
      '       ti.IZVOR_SREDSTVA ,'
      '       ti.VREMETRAENJE_DOG'
      'from jn_tender t'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      'order by t.datum  desc')
    Left = 616
    Top = 584
  end
  object qCountDogovori: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count (d.broj_dogovor) as br'
      'from jn_dogovor d'
      
        'where d.partner = :partner and d.tip_partner = :tip_partner and ' +
        'd.broj_tender = :broj_tender')
    Left = 1368
    Top = 552
  end
  object tblProcTenderZatvoriPred1Mesec: TpFIBDataSet
    SelectSQL.Strings = (
      'select PROC_JN_ZATVORI_TEN_1_MESEC.broj_tender_o'
      'from PROC_JN_ZATVORI_TEN_1_MESEC')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 616
    Top = 528
    object FIBStringField1: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'BROJ_TENDER_O'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dstblProcTenderZatvoriPred1Mesec: TDataSource
    DataSet = tblProcTenderZatvoriPred1Mesec
    Left = 768
    Top = 528
  end
  object qSetUpJN_frm_rok_na_vazenje_god: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1'
      'from jn_setup s'
      'where p1 = '#39'frm_rok_na_vazenje_god'#39)
    Left = 80
    Top = 664
  end
  object tblPredlogPlanJN1: TpFIBDataSet
    SelectSQL.Strings = (
      'select  proc_jn_godisenplan_od_jn.predmet_nabavka,'
      '        proc_jn_godisenplan_od_jn.iznos,'
      '        proc_jn_godisenplan_od_jn.grupa,'
      '        proc_jn_godisenplan_od_jn.datum_out,'
      '        proc_jn_godisenplan_od_jn.datum,'
      '        proc_jn_godisenplan_od_jn.mesec,'
      '        tn.naziv as tip_tender'
      'from PROC_JN_GODISENPLAN_OD_JN(:godina)'
      
        'inner join jn_tip_nabavka tn on tn.sifra= proc_jn_godisenplan_od' +
        '_jn.tip_tender'
      'order by  3, 5')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 40
    Top = 720
    object tblPredlogPlanJN1PREDMET_NABAVKA: TFIBStringField
      DisplayLabel = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      FieldName = 'PREDMET_NABAVKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlogPlanJN1IZNOS: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1085#1072' '#1076#1086#1075'./'#1088#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075'. '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS'
      DisplayFormat = '0.0000, .'
    end
    object tblPredlogPlanJN1DATUM_OUT: TFIBDateField
      FieldName = 'DATUM_OUT'
    end
    object tblPredlogPlanJN1DATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblPredlogPlanJN1MESEC: TFIBStringField
      DisplayLabel = #1054#1095#1077#1082#1091#1074#1072#1085' '#1087#1086#1095#1077#1090#1086#1082' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' ('#1052#1077#1089#1077#1094')'
      FieldName = 'MESEC'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlogPlanJN1GRUPA: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPredlogPlanJN1TIP_TENDER: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      FieldName = 'TIP_TENDER'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPredlogPlanJN1: TDataSource
    DataSet = tblPredlogPlanJN1
    Left = 144
    Top = 720
  end
  object qZemiPretsavnikPonuda: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 p.pretstavnik'
      'from  jn_ponudi p'
      'where p.tip_partner = :tip_partner and p.partner= :partner'
      'order by p.ts_ins desc')
    Left = 1272
    Top = 608
  end
  object qZatvoriPonudaStatus: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select t.zatvori_ponudi_status, t.zatvori_ponudi_user, t.zatvori' +
        '_ponudi_time'
      'from jn_tender t'
      'where t.broj = :tender_broj')
    Left = 960
    Top = 648
  end
  object tblDobitniciPdf: TpFIBDataSet
    SelectSQL.Strings = (
      'select  distinct td.tip_partner, td.partner'
      'from jn_tender_dobitnici td'
      'where td.broj_tender = :broj_tender')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1576
    Top = 240
    object tblDobitniciPdfTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblDobitniciPdfPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
  object dsDobitniciPdf: TDataSource
    DataSet = tblDobitniciPdf
    Left = 1720
    Top = 240
  end
  object tblOdbieniPdf: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.tip_partner, p.partner'
      'from jn_ponudi p'
      'where p.broj_tender = :broj_tender and p.odbiena = 1')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1272
    Top = 680
    object tblOdbieniPdfTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblOdbieniPdfPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
  object dsOdbieniPdf: TDataSource
    DataSet = tblOdbieniPdf
    Left = 1360
    Top = 680
  end
  object tblNedobitniPdf: TpFIBDataSet
    SelectSQL.Strings = (
      ' select distinct p.tip_partner, p.partner, p.odbiena'
      'from jn_ponudi_stavki p'
      'inner join jn_ponudi pp on pp.id = p.ponuda_id'
      
        'left join jn_tender_dobitnici td on  td.broj_tender = p.broj_ten' +
        'der and td.tip_partner = p.tip_partner and td.partner = p.partne' +
        'r'
      
        'where p.broj_tender = :broj_tender and td.id_ponudi_stavki is nu' +
        'll')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 1264
    Top = 736
    object tblNedobitniPdfTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblNedobitniPdfPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblNedobitniPdfODBIENA: TFIBIntegerField
      FieldName = 'ODBIENA'
    end
  end
  object dsNedobitniPdf: TDataSource
    Left = 1360
    Top = 736
  end
end
