﻿object frmJavnaNabavka: TfrmJavnaNabavka
  Left = 128
  Top = 212
  VertScrollBar.Smooth = True
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = '\'
  ClientHeight = 693
  ClientWidth = 1276
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1276
    693)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1276
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = #1055#1088#1077#1073#1072#1088#1072#1112' '#1087#1086
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          Caption = #1055#1086#1089#1090#1072#1087#1082#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1076#1086#1075#1086#1074#1086#1088
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 670
    Width = 1276
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', F' +
          '10 - '#1055#1088#1086#1076#1086#1083#1078#1080
        Width = 406
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080
        Width = 120
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel, Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094 +
          #1080#1112#1072' '
        Width = 345
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControlTabelarenDetalen: TcxPageControl
    Left = 0
    Top = 126
    Width = 1276
    Height = 544
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = cxTabSheetTabelaren
    Properties.CustomButtons.Buttons = <>
    OnPageChanging = cxPageControlTabelarenDetalenPageChanging
    ClientRectBottom = 544
    ClientRectRight = 1276
    ClientRectTop = 24
    object cxTabSheetTabelaren: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 0
      object PanelTabelarenGrid: TPanel
        Left = 0
        Top = 0
        Width = 1276
        Height = 520
        Align = alClient
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 1274
          Height = 518
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsТender
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              DataBinding.FieldName = 'GODINA'
              Options.Editing = False
              Width = 45
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Options.Editing = False
              Width = 74
            end
            object cxGrid1DBTableView1BROJ_PART1: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_PART1'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Options.Editing = False
              Width = 67
            end
            object cxGrid1DBTableView1ZATVOREN: TcxGridDBColumn
              DataBinding.FieldName = 'ZATVOREN'
              Visible = False
              Options.Editing = False
              Width = 86
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  Description = #1053#1077' '#1077' '#1072#1082#1090#1080#1074#1077#1085
                  ImageIndex = 3
                  Value = 0
                end
                item
                  Description = #1040#1082#1090#1080#1074#1077#1085
                  ImageIndex = 2
                  Value = 1
                end>
            end
            object cxGrid1DBTableView1zatvorenNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'zatvorenNaziv'
              Options.Editing = False
              Width = 88
            end
            object cxGrid1DBTableView1INTEREN_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'INTEREN_BROJ'
              Visible = False
              Options.Editing = False
              Width = 77
            end
            object cxGrid1DBTableView1ARHIVSKI_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'ARHIVSKI_BROJ'
              Visible = False
              Options.Editing = False
              Width = 81
            end
            object cxGrid1DBTableView1OGLAS: TcxGridDBColumn
              DataBinding.FieldName = 'OGLAS'
              Visible = False
              Options.Editing = False
              Width = 73
            end
            object cxGrid1DBTableView1LICE_KONTAKT: TcxGridDBColumn
              DataBinding.FieldName = 'LICE_KONTAKT'
              Visible = False
              Options.Editing = False
              Width = 138
            end
            object cxGrid1DBTableView1EMAIL_KONTAKT: TcxGridDBColumn
              DataBinding.FieldName = 'EMAIL_KONTAKT'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1KONTAKT_TEL_FAKS: TcxGridDBColumn
              DataBinding.FieldName = 'KONTAKT_TEL_FAKS'
              Options.Editing = False
              Width = 113
            end
            object cxGrid1DBTableView1PONISTEN: TcxGridDBColumn
              DataBinding.FieldName = 'PONISTEN'
              Visible = False
              Options.Editing = False
              Width = 103
            end
            object cxGrid1DBTableView1ponistenNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'ponistenNaziv'
              Visible = False
              Options.Editing = False
              Width = 101
            end
            object cxGrid1DBTableView1VRSKA_PREDMET: TcxGridDBColumn
              DataBinding.FieldName = 'VRSKA_PREDMET'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1PONISTEN_PRICINA: TcxGridDBColumn
              DataBinding.FieldName = 'PONISTEN_PRICINA'
              Visible = False
              Width = 185
            end
            object cxGrid1DBTableView1PRICINAPONISTENNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PRICINAPONISTENNAZIV'
              Width = 140
            end
            object cxGrid1DBTableView1TIP_BARANJA: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_BARANJA'
              Visible = False
              Options.Editing = False
              Width = 308
            end
            object cxGrid1DBTableView1tip_baranjaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'tip_baranjaNaziv'
              Visible = False
              Options.Editing = False
              Width = 311
            end
            object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPA'
              Options.Editing = False
              Width = 84
            end
            object cxGrid1DBTableView1GrupaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'GrupaNaziv'
              Options.Editing = False
              Width = 87
            end
            object cxGrid1DBTableView1GRUPA_SIFRA: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPA_SIFRA'
              Visible = False
              Options.Editing = False
              Width = 83
            end
            object cxGrid1DBTableView1VIDDOGOVOROPIS: TcxGridDBColumn
              DataBinding.FieldName = 'VIDDOGOVOROPIS'
              Options.Editing = False
              Width = 256
            end
            object cxGrid1DBTableView1PREDMET_NABAVKA: TcxGridDBColumn
              DataBinding.FieldName = 'PREDMET_NABAVKA'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 201
            end
            object cxGrid1DBTableView1TIP_TENDER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_TENDER'
              Visible = False
              Options.Editing = False
              Width = 90
            end
            object cxGrid1DBTableView1TIPTENDERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIPTENDERNAZIV'
              Options.Editing = False
              Width = 259
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Visible = False
              Options.Editing = False
              Width = 220
            end
            object cxGrid1DBTableView1SEKTORSKI_DOGOVOR: TcxGridDBColumn
              DataBinding.FieldName = 'SEKTORSKI_DOGOVOR'
              Visible = False
              Options.Editing = False
            end
            object cxGrid1DBTableView1sektorski_dogovorTeloNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'sektorski_dogovorTeloNaziv'
              Options.Editing = False
              Width = 339
            end
            object cxGrid1DBTableView1CENTRALNO_TELO: TcxGridDBColumn
              DataBinding.FieldName = 'CENTRALNO_TELO'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1centralnoTeloNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'centralnoTeloNaziv'
              Options.Editing = False
              Width = 265
            end
            object cxGrid1DBTableView1GRUPNA_NABAVKA: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPNA_NABAVKA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1grupnaNabavkaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'grupnaNabavkaNaziv'
              Options.Editing = False
              Width = 89
            end
            object cxGrid1DBTableView1DELIV: TcxGridDBColumn
              DataBinding.FieldName = 'DELIV'
              Visible = False
              Options.Editing = False
              Width = 56
            end
            object cxGrid1DBTableView1delivNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'delivNaziv'
              Options.Editing = False
              Width = 198
            end
            object cxGrid1DBTableView1RAMKOVNA_SPOG: TcxGridDBColumn
              DataBinding.FieldName = 'RAMKOVNA_SPOG'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1ramkovnaSpogodbaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'ramkovnaSpogodbaNaziv'
              Options.Editing = False
              Width = 106
            end
            object cxGrid1DBTableView1RS_POVEKE_ORGANI: TcxGridDBColumn
              DataBinding.FieldName = 'RS_POVEKE_ORGANI'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1rsPovekeOrganiNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'rsPovekeOrganiNaziv'
              Options.Editing = False
              Width = 278
            end
            object cxGrid1DBTableView1RS_POVEKE_OPERATORI: TcxGridDBColumn
              DataBinding.FieldName = 'RS_POVEKE_OPERATORI'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1rsPovekeOperatoriNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'rsPovekeOperatoriNaziv'
              Options.Editing = False
              Width = 197
            end
            object cxGrid1DBTableView1ALTERNATIVNI_PONUDI: TcxGridDBColumn
              DataBinding.FieldName = 'ALTERNATIVNI_PONUDI'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1alternativniPonudiNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'alternativniPonudiNaziv'
              Options.Editing = False
              Width = 273
            end
            object cxGrid1DBTableView1VREMETRAENJE_DOG: TcxGridDBColumn
              DataBinding.FieldName = 'VREMETRAENJE_DOG'
              Options.Editing = False
              Width = 233
            end
            object cxGrid1DBTableView1LOKACIJA_ISPORAKA: TcxGridDBColumn
              DataBinding.FieldName = 'LOKACIJA_ISPORAKA'
              Options.Editing = False
              Width = 172
            end
            object cxGrid1DBTableView1LICE_REALIZACIJA: TcxGridDBColumn
              Caption = 
                #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1088#1077#1072#1083#1080#1079#1080#1088#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076' '#1080#1079#1074#1088#1096#1077#1085#1072#1090#1072' '#1112#1072#1074#1085#1072' '#1085 +
                #1072#1073#1072#1074#1082#1072
              DataBinding.FieldName = 'LICE_REALIZACIJA'
              Options.Editing = False
              Width = 402
            end
            object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS'
              Options.Editing = False
              Width = 200
            end
            object cxGrid1DBTableView1IZNOS_SO_DDV: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS_SO_DDV'
              Width = 200
            end
            object cxGrid1DBTableView1TIPTENDERDENOVIZALBA: TcxGridDBColumn
              DataBinding.FieldName = 'TIPTENDERDENOVIZALBA'
              Visible = False
              Options.Editing = False
              Width = 124
            end
            object cxGrid1DBTableView1SKRATENI_ROKOVI: TcxGridDBColumn
              DataBinding.FieldName = 'SKRATENI_ROKOVI'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1skratenNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'skratenNaziv'
              Options.Editing = False
              Width = 350
            end
            object cxGrid1DBTableView1TIP_OCENA: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_OCENA'
              Visible = False
              Options.Editing = False
              Width = 189
            end
            object cxGrid1DBTableView1tip_ocenaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'tip_ocenaNaziv'
              Options.Editing = False
              Width = 194
            end
            object cxGrid1DBTableView1OBRAZLOZENIE_POSTAPKA: TcxGridDBColumn
              DataBinding.FieldName = 'OBRAZLOZENIE_POSTAPKA'
              Visible = False
              Options.Editing = False
              Width = 216
            end
            object cxGrid1DBTableView1LICE_PODGOTOVKA: TcxGridDBColumn
              DataBinding.FieldName = 'LICE_PODGOTOVKA'
              Options.Editing = False
              Width = 261
            end
            object cxGrid1DBTableView1EL_SREDSTVA: TcxGridDBColumn
              DataBinding.FieldName = 'EL_SREDSTVA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1elSredstvaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'elSredstvaNaziv'
              Options.Editing = False
              Width = 349
            end
            object cxGrid1DBTableView1EL_AUKCIJA: TcxGridDBColumn
              DataBinding.FieldName = 'EL_AUKCIJA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1elAukcijaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'elAukcijaNaziv'
              Options.Editing = False
              Width = 225
            end
            object cxGrid1DBTableView1INFO_AUKCIJA: TcxGridDBColumn
              DataBinding.FieldName = 'INFO_AUKCIJA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1GARANCIJA_PONUDA: TcxGridDBColumn
              DataBinding.FieldName = 'GARANCIJA_PONUDA'
              Options.Editing = False
              Width = 357
            end
            object cxGrid1DBTableView1GARANCIJA_KVALITET: TcxGridDBColumn
              DataBinding.FieldName = 'GARANCIJA_KVALITET'
              Options.Editing = False
              Width = 537
            end
            object cxGrid1DBTableView1garancijaIzjavaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'garancijaIzjavaNaziv'
              Width = 100
            end
            object cxGrid1DBTableView1AVANSNO: TcxGridDBColumn
              DataBinding.FieldName = 'AVANSNO'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1avansnoNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'avansnoNaziv'
              Options.Editing = False
              Width = 95
            end
            object cxGrid1DBTableView1ZDRUZUVANJE_OPERATORI: TcxGridDBColumn
              DataBinding.FieldName = 'ZDRUZUVANJE_OPERATORI'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1zdruzuvanjeOperatoriNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'zdruzuvanjeOperatoriNaziv'
              Options.Editing = False
              Width = 406
            end
            object cxGrid1DBTableView1CENA_DOKUMENTACIJA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA_DOKUMENTACIJA'
              Options.Editing = False
              Width = 182
            end
            object cxGrid1DBTableView1INFO_PLAKJANJE: TcxGridDBColumn
              DataBinding.FieldName = 'INFO_PLAKJANJE'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1DENARSKA: TcxGridDBColumn
              DataBinding.FieldName = 'DENARSKA'
              Options.Editing = False
              Width = 97
            end
            object cxGrid1DBTableView1DEVIZNA: TcxGridDBColumn
              DataBinding.FieldName = 'DEVIZNA'
              Options.Editing = False
              Width = 98
            end
            object cxGrid1DBTableView1VALIDNOST_PONUDA: TcxGridDBColumn
              DataBinding.FieldName = 'VALIDNOST_PONUDA'
              Options.Editing = False
              Width = 217
            end
            object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_OD'
              Options.Editing = False
              Width = 219
            end
            object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_DO'
              Options.Editing = False
              Width = 233
            end
            object cxGrid1DBTableView1DATUM_OTVARANJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_OTVARANJE'
              Options.Editing = False
              Width = 158
            end
            object cxGrid1DBTableView1VREME_OTVARANJE: TcxGridDBColumn
              DataBinding.FieldName = 'VREME_OTVARANJE'
              Options.Editing = False
              Width = 161
            end
            object cxGrid1DBTableView1MESTO_OTVARANJE: TcxGridDBColumn
              DataBinding.FieldName = 'MESTO_OTVARANJE'
              Options.Editing = False
              Width = 161
            end
            object cxGrid1DBTableView1DOPOLNITELNI_INFO: TcxGridDBColumn
              DataBinding.FieldName = 'DOPOLNITELNI_INFO'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.ReadOnly = True
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1ODL_NABAVKA_BR: TcxGridDBColumn
              DataBinding.FieldName = 'ODL_NABAVKA_BR'
              Options.Editing = False
              Width = 136
            end
            object cxGrid1DBTableView1ODL_NABAVKA_DAT: TcxGridDBColumn
              DataBinding.FieldName = 'ODL_NABAVKA_DAT'
              Options.Editing = False
              Width = 149
            end
            object cxGrid1DBTableView1SOGLASNOST_BR: TcxGridDBColumn
              DataBinding.FieldName = 'SOGLASNOST_BR'
              Options.Editing = False
              Width = 227
            end
            object cxGrid1DBTableView1SOGLASNOST_DAT: TcxGridDBColumn
              DataBinding.FieldName = 'SOGLASNOST_DAT'
              Options.Editing = False
              Width = 241
            end
            object cxGrid1DBTableView1ODLUKA_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'ODLUKA_BROJ'
              Options.Editing = False
              Width = 254
            end
            object cxGrid1DBTableView1ODLUKA_DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'ODLUKA_DATUM'
              Options.Editing = False
              Width = 269
            end
            object cxGrid1DBTableView1ODLUKA_PROSIRUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'ODLUKA_PROSIRUVANJE'
              Options.Editing = False
              Width = 221
            end
            object cxGrid1DBTableView1DATUM_ODLUKA_PROSIRUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_ODLUKA_PROSIRUVANJE'
              Options.Editing = False
              Width = 235
            end
            object cxGrid1DBTableView1IZNOS_PROSIRUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS_PROSIRUVANJE'
              Options.Editing = False
              Width = 225
            end
            object cxGrid1DBTableView1ODL_POKANA_BR: TcxGridDBColumn
              DataBinding.FieldName = 'ODL_POKANA_BR'
              Options.Editing = False
              Width = 357
            end
            object cxGrid1DBTableView1ODL_POKANA_DAT: TcxGridDBColumn
              DataBinding.FieldName = 'ODL_POKANA_DAT'
              Options.Editing = False
              Width = 360
            end
            object cxGrid1DBTableView1BROJ_POKANI: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_POKANI'
              Options.Editing = False
              Width = 408
            end
            object cxGrid1DBTableView1MIN_PONUDI: TcxGridDBColumn
              DataBinding.FieldName = 'MIN_PONUDI'
              Options.Editing = False
              Width = 283
            end
            object cxGrid1DBTableView1MAKS_PONUDI: TcxGridDBColumn
              DataBinding.FieldName = 'MAKS_PONUDI'
              Options.Editing = False
              Width = 287
            end
            object cxGrid1DBTableView1OSTETENI_KOVERTI: TcxGridDBColumn
              DataBinding.FieldName = 'OSTETENI_KOVERTI'
              Options.Editing = False
              Width = 146
            end
            object cxGrid1DBTableView1ANEKS_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'ANEKS_BROJ'
              Options.Editing = False
              Width = 134
            end
            object cxGrid1DBTableView1ANEKS_DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'ANEKS_DATUM'
              Options.Editing = False
              Width = 141
            end
            object cxGrid1DBTableView1ANEKS_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'ANEKS_IZNOS'
              Options.Editing = False
              Width = 141
            end
            object cxGrid1DBTableView1ZALBA: TcxGridDBColumn
              DataBinding.FieldName = 'ZALBA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1zalbaNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'zalbaNaziv'
              Options.Editing = False
              Width = 202
            end
            object cxGrid1DBTableView1BROJ_ZALBI: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_ZALBI'
              Options.Editing = False
              Width = 77
            end
            object cxGrid1DBTableView1ZALBA_OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'ZALBA_OPIS'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 108
            end
            object cxGrid1DBTableView1POTVRDA_BR: TcxGridDBColumn
              DataBinding.FieldName = 'POTVRDA_BR'
              Options.Editing = False
              Width = 367
            end
            object cxGrid1DBTableView1POTVRDA_DAT: TcxGridDBColumn
              DataBinding.FieldName = 'POTVRDA_DAT'
              Options.Editing = False
              Width = 376
            end
            object cxGrid1DBTableView1POTVRDA_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'POTVRDA_IZNOS'
              Options.Editing = False
              Width = 366
            end
            object cxGrid1DBTableView1IZVOR_SREDSTVA: TcxGridDBColumn
              DataBinding.FieldName = 'IZVOR_SREDSTVA'
              Options.Editing = False
              Width = 149
            end
            object cxGrid1DBTableView1DATUM_ZAPISNIK: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_ZAPISNIK'
              Options.Editing = False
              Width = 107
            end
            object cxGrid1DBTableView1VKUPNO_PONUDI: TcxGridDBColumn
              DataBinding.FieldName = 'VKUPNO_PONUDI'
              Options.Editing = False
              Width = 94
            end
            object cxGrid1DBTableView1BR_ZADOCNETI_PONUDI: TcxGridDBColumn
              DataBinding.FieldName = 'BR_ZADOCNETI_PONUDI'
              Options.Editing = False
              Width = 140
            end
            object cxGrid1DBTableView1ZA_POVLEKUVANJE: TcxGridDBColumn
              DataBinding.FieldName = 'ZA_POVLEKUVANJE'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1ZA_IZMENA: TcxGridDBColumn
              DataBinding.FieldName = 'ZA_IZMENA'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1ZABELESKA_ZAPISNIK: TcxGridDBColumn
              DataBinding.FieldName = 'ZABELESKA_ZAPISNIK'
              PropertiesClassName = 'TcxRichEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 200
            end
            object cxGrid1DBTableView1BODIRAL: TcxGridDBColumn
              DataBinding.FieldName = 'BODIRAL'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1VREME_BOD: TcxGridDBColumn
              DataBinding.FieldName = 'VREME_BOD'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1tip_god_plan_Naziv: TcxGridDBColumn
              DataBinding.FieldName = 'tip_god_plan_Naziv'
              Visible = False
              Options.Editing = False
              Width = 170
            end
            object cxGrid1DBTableView1ZATVORI_PONUDI_STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'ZATVORI_PONUDI_STATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxSmallImages
              Properties.Items = <
                item
                  Description = #1054#1090#1074#1086#1088#1077#1085
                  ImageIndex = 104
                  Value = 0
                end
                item
                  Description = #1047#1072#1090#1074#1086#1088#1077#1085
                  ImageIndex = 84
                  Value = 1
                end>
              Width = 250
            end
            object cxGrid1DBTableView1ZATVORI_PONUDI_TIME: TcxGridDBColumn
              DataBinding.FieldName = 'ZATVORI_PONUDI_TIME'
              Width = 250
            end
            object cxGrid1DBTableView1ZATVORI_PONUDI_USER: TcxGridDBColumn
              DataBinding.FieldName = 'ZATVORI_PONUDI_USER'
              Width = 250
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1TI_TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TI_TS_INS'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1TI_TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TI_TS_UPD'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1TI_USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TI_USR_INS'
              Visible = False
              Options.Editing = False
              Width = 100
            end
            object cxGrid1DBTableView1TI_USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TI_USR_UPD'
              Visible = False
              Options.Editing = False
              Width = 100
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
        object pnlPromenetiCeni: TPanel
          Left = 175
          Top = 143
          Width = 439
          Height = 240
          BevelInner = bvLowered
          BevelOuter = bvSpace
          Color = 11302478
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label78: TLabel
            Left = 2
            Top = 190
            Width = 435
            Height = 35
            Align = alBottom
            AutoSize = False
            Caption = 
              #1047#1072#1073#1077#1083#1077#1096#1082#1072': '#1047#1072' '#1076#1072' '#1089#1077' '#1087#1088#1077#1089#1084#1077#1090#1072' '#1087#1086#1074#1090#1086#1088#1085#1086' '#1087#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1090#1088#1077#1073#1072' '#1076 +
              #1072' '#1089#1077' '#1086#1090#1074#1086#1088#1080' '#1092#1086#1088#1084#1072#1090#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            WordWrap = True
            ExplicitLeft = -73
            ExplicitTop = 184
          end
          object Label79: TLabel
            Left = 2
            Top = 225
            Width = 435
            Height = 13
            Align = alBottom
            Caption = ' Esc - '#1054#1090#1082#1072#1078#1080
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 69
          end
          object Label80: TLabel
            Left = 2
            Top = 2
            Width = 435
            Height = 35
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 
              #1032#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080' '#1079#1072' '#1082#1086#1080' '#1080#1084#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1094#1077#1085#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083 +
              #1072#1085
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
            ExplicitLeft = -65
          end
          object cxGrid5: TcxGrid
            Left = 2
            Top = 37
            Width = 435
            Height = 153
            Align = alClient
            TabOrder = 0
            object cxGrid5DBTableView1: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dsPromenetiCeni
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsView.GroupByBox = False
              object cxGrid5DBTableView1BROJ_TENDER: TcxGridDBColumn
                DataBinding.FieldName = 'BROJ_TENDER'
                Width = 313
              end
            end
            object cxGrid5Level1: TcxGridLevel
              GridView = cxGrid5DBTableView1
            end
          end
        end
        object pnlExportExcel: TPanel
          Left = 720
          Top = 24
          Width = 513
          Height = 229
          TabOrder = 2
          Visible = False
          object cxGrid6: TcxGrid
            Left = 1
            Top = 1
            Width = 511
            Height = 227
            Align = alClient
            TabOrder = 0
            object cxGrid6DBTableView1: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dsIzborNajpovolnaPonudaCena
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              object cxGrid6DBTableView1PARTNERNAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'PARTNERNAZIV'
                Width = 250
              end
              object cxGrid6DBTableView1VID_NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'VID_NAZIV'
                Width = 86
              end
              object cxGrid6DBTableView1GRUPA_NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'GRUPA_NAZIV'
                Width = 219
              end
              object cxGrid6DBTableView1ARTIKAL: TcxGridDBColumn
                DataBinding.FieldName = 'ARTIKAL'
                Width = 109
              end
              object cxGrid6DBTableView1JN_CPV: TcxGridDBColumn
                DataBinding.FieldName = 'JN_CPV'
                Width = 80
              end
              object cxGrid6DBTableView1ARTIKALNAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'ARTIKALNAZIV'
                Width = 300
              end
              object cxGrid6DBTableView1KOLICINA: TcxGridDBColumn
                DataBinding.FieldName = 'KOLICINA'
                Width = 71
              end
              object cxGrid6DBTableView1MERKA: TcxGridDBColumn
                DataBinding.FieldName = 'MERKA'
                Width = 89
              end
              object cxGrid6DBTableView1CENABEZDDV: TcxGridDBColumn
                DataBinding.FieldName = 'CENABEZDDV'
                Width = 110
              end
              object cxGrid6DBTableView1DANOK: TcxGridDBColumn
                DataBinding.FieldName = 'DANOK'
                Width = 47
              end
              object cxGrid6DBTableView1CENA: TcxGridDBColumn
                DataBinding.FieldName = 'CENA'
                Width = 93
              end
              object cxGrid6DBTableView1IZNOS: TcxGridDBColumn
                DataBinding.FieldName = 'IZNOS'
                Width = 111
              end
              object cxGrid6DBTableView1IZNOOSBEZDDV: TcxGridDBColumn
                DataBinding.FieldName = 'IZNOOSBEZDDV'
                Width = 129
              end
              object cxGrid6DBTableView1DELIVA: TcxGridDBColumn
                DataBinding.FieldName = 'DELIVA'
                Width = 250
              end
            end
            object cxGrid6Level1: TcxGridLevel
              GridView = cxGrid6DBTableView1
            end
          end
        end
      end
    end
    object cxTabSheetDetalen: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object cxPageControlDetalenPodelen: TcxPageControl
        Left = 0
        Top = 0
        Width = 1276
        Height = 520
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Properties.ActivePage = cxTabSheetOsnovniPodatoci
        Properties.CustomButtons.Buttons = <>
        Properties.MultiLine = True
        Properties.Rotate = True
        Properties.TabCaptionAlignment = taLeftJustify
        Properties.TabHeight = 40
        Properties.TabPosition = tpLeft
        Properties.TabWidth = 250
        OnPageChanging = cxPageControlDetalenPodelenPageChanging
        ClientRectBottom = 520
        ClientRectLeft = 256
        ClientRectRight = 1276
        ClientRectTop = 0
        object cxTabSheetOsnovniPodatoci: TcxTabSheet
          Caption = ' '#1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
          object cxScrollBoxOsnovniPodatoci: TcxScrollBox
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            AutoScroll = False
            HorzScrollBar.Range = 1002
            TabOrder = 0
            VertScrollBar.Range = 733
            object PanelOsnovniPodatoci: TPanel
              Left = 0
              Top = 0
              Width = 1002
              Height = 733
              Align = alClient
              BevelInner = bvLowered
              BevelOuter = bvSpace
              Enabled = False
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              DesignSize = (
                1002
                733)
              object Label4: TLabel
                Left = 0
                Top = 85
                Width = 67
                Height = 21
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1044#1072#1090#1091#1084' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label2: TLabel
                Left = -14
                Top = 59
                Width = 81
                Height = 13
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1041#1088#1086#1112' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label3: TLabel
                Left = 17
                Top = 32
                Width = 50
                Height = 13
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1043#1086#1076#1080#1085#1072' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Label15: TLabel
                Left = 17
                Top = 367
                Width = 119
                Height = 18
                AutoSize = False
                Caption = #1051#1080#1094#1077' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label16: TLabel
                Left = 17
                Top = 403
                Width = 119
                Height = 18
                AutoSize = False
                Caption = 'e-mail '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label6: TLabel
                Left = 17
                Top = 235
                Width = 94
                Height = 16
                AutoSize = False
                Caption = #1048#1085#1090#1077#1088#1077#1085' '#1073#1088#1086#1112' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label7: TLabel
                Left = 12
                Top = 271
                Width = 94
                Height = 16
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label10: TLabel
                Left = 17
                Top = 306
                Width = 98
                Height = 17
                AutoSize = False
                Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1075#1083#1072#1089' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label8: TLabel
                Left = 17
                Top = 537
                Width = 117
                Height = 15
                AutoSize = False
                Caption = #1042#1088#1089#1082#1072' '#1089#1086' '#1085#1072#1073#1072#1074#1082#1072' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label75: TLabel
                Left = 17
                Top = 440
                Width = 212
                Height = 18
                AutoSize = False
                Caption = #1058#1077#1083#1077#1092#1086#1085'/'#1092#1072#1082#1089' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object lbl1: TLabel
                Left = 17
                Top = 578
                Width = 240
                Height = 18
                AutoSize = False
                Caption = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object DATUM: TcxDBDateEdit
                Left = 73
                Top = 83
                DataBinding.DataField = 'DATUM'
                DataBinding.DataSource = dm.dsТender
                TabOrder = 2
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 156
              end
              object BROJ: TcxDBTextEdit
                Left = 73
                Top = 56
                BeepOnEnter = False
                DataBinding.DataField = 'BROJ'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Font.Charset = DEFAULT_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.Shadow = False
                Style.IsFontAssigned = True
                TabOrder = 1
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 156
              end
              object GODINA: TcxDBComboBox
                Tag = 1
                Left = 73
                Top = 29
                DataBinding.DataField = 'GODINA'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.DropDownListStyle = lsFixedList
                Properties.Items.Strings = (
                  '2010'
                  '2011'
                  '2012'
                  '2013'
                  '2014'
                  '2015'
                  '2016'
                  '2017'
                  '2018'
                  '2019'
                  '2020'
                  '2021'
                  '2022'
                  '2023'
                  '2024'
                  '2025'
                  '2026'
                  '2027'
                  '2028'
                  '2029'
                  '2030'
                  '2031'
                  '2032'
                  '2033'
                  '2034'
                  '2035'
                  '2036'
                  '2037'
                  '2038'
                  '2039'
                  '2040'
                  '2041'
                  '2042'
                  '2043'
                  '2044'
                  '2045'
                  '2046'
                  '2047'
                  '2048'
                  '2049'
                  '2050')
                Properties.OnEditValueChanged = GODINAPropertiesEditValueChanged
                Style.Font.Charset = RUSSIAN_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = [fsBold]
                Style.IsFontAssigned = True
                TabOrder = 0
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 63
              end
              object cxGroupBoxPredmetNaDogovor: TcxGroupBox
                Left = 339
                Top = 12
                Anchors = [akLeft, akTop, akRight]
                Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                TabOrder = 14
                DesignSize = (
                  631
                  655)
                Height = 655
                Width = 631
                object Label11: TLabel
                  Left = 92
                  Top = 78
                  Width = 98
                  Height = 18
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label12: TLabel
                  Left = 52
                  Top = 102
                  Width = 138
                  Height = 18
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label53: TLabel
                  Left = 16
                  Top = 531
                  Width = 174
                  Height = 29
                  Alignment = taRightJustify
                  AutoSize = False
                  BiDiMode = bdLeftToRight
                  Caption = #1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090'/'#1088#1072#1084#1082'. '#1089#1087#1086#1075#1086#1076#1073#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentBiDiMode = False
                  ParentFont = False
                  WordWrap = True
                end
                object Label5: TLabel
                  Left = 47
                  Top = 139
                  Width = 143
                  Height = 35
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label14: TLabel
                  Left = 15
                  Top = 586
                  Width = 175
                  Height = 30
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1088#1077#1072#1083#1080#1079#1080#1088#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label63: TLabel
                  Left = 45
                  Top = 559
                  Width = 145
                  Height = 32
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072'/'#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label9: TLabel
                  Left = 56
                  Top = 613
                  Width = 134
                  Height = 32
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' ('#1073#1077#1079' '#1044#1044#1042') :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label77: TLabel
                  Left = 312
                  Top = 613
                  Width = 134
                  Height = 32
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' ('#1089#1086' '#1044#1044#1042') :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object cxDBRadioGroupVID: TcxDBRadioGroup
                  Left = 196
                  Top = 26
                  TabStop = False
                  Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  DataBinding.DataField = 'GRUPA'
                  DataBinding.DataSource = dm.dsТender
                  Properties.Columns = 3
                  Properties.ImmediatePost = True
                  Properties.Items = <
                    item
                      Caption = #1057#1090#1086#1082#1080
                      Value = 1
                    end
                    item
                      Caption = #1059#1089#1083#1091#1075#1080
                      Value = 2
                    end
                    item
                      Caption = #1056#1072#1073#1086#1090#1080
                      Value = 3
                    end>
                  Properties.OnEditValueChanged = cxDBRadioGroupVIDPropertiesEditValueChanged
                  Style.BorderStyle = ebsUltraFlat
                  Style.TransparentBorder = True
                  TabOrder = 0
                  OnExit = cxDBRadioGroupVIDExit
                  Height = 43
                  Width = 413
                end
                object GRUPA: TcxDBTextEdit
                  Tag = 1
                  Left = 196
                  Top = 75
                  Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'GRUPA'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 29
                end
                object GRUPA_SIFRA: TcxDBTextEdit
                  Tag = 1
                  Left = 224
                  Top = 75
                  Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'GRUPA_SIFRA'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 50
                end
                object GRUPA_OPIS: TcxDBLookupComboBox
                  Tag = 1
                  Left = 273
                  Top = 75
                  Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1086#1087#1080#1089
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  DataBinding.DataField = 'GRUPA_SIFRA'
                  DataBinding.DataSource = dm.dsТender
                  Properties.DropDownListStyle = lsFixedList
                  Properties.DropDownSizeable = True
                  Properties.KeyFieldNames = 'SIFRA'
                  Properties.ListColumns = <
                    item
                      Width = 120
                      FieldName = 'VidNaziv'
                    end
                    item
                      Width = 120
                      FieldName = 'SIFRA'
                    end
                    item
                      Width = 800
                      FieldName = 'OPIS'
                    end>
                  Properties.ListFieldIndex = 2
                  Properties.ListSource = dm.dsVidoviDogovori
                  TabOrder = 3
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 336
                end
                object PREDMET_NABAVKA: TcxDBMemo
                  Tag = 1
                  Left = 196
                  Top = 99
                  Hint = 
                    #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' ' +
                    #1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'PREDMET_NABAVKA'
                  DataBinding.DataSource = dm.dsТender
                  Properties.WantReturns = False
                  TabOrder = 4
                  OnDblClick = PREDMET_NABAVKADblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 34
                  Width = 413
                end
                object cxDBRadioGroupRAMKOVNA_SPOG: TcxDBRadioGroup
                  Left = 196
                  Top = 335
                  TabStop = False
                  Caption = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '
                  DataBinding.DataField = 'RAMKOVNA_SPOG'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.ImmediatePost = True
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 6
                  OnClick = cxDBRadioGroupRAMKOVNA_SPOGClick
                  Height = 43
                  Width = 413
                end
                object cxDBRadioGroupRS_POVEKE_ORGANI: TcxDBRadioGroup
                  Left = 196
                  Top = 384
                  TabStop = False
                  Caption = #1056#1072#1084#1082#1086#1074#1085#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1087#1086#1084#1077#1107#1091' '#1087#1086#1074#1077#1116#1077' '#1076#1086#1075#1086#1074#1086#1088#1085#1080' '#1086#1088#1075#1072#1085#1080
                  DataBinding.DataField = 'RS_POVEKE_ORGANI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 7
                  Height = 43
                  Width = 413
                end
                object cxDBRadioGroupRS_POVEKE_OPERATORI: TcxDBRadioGroup
                  Left = 196
                  Top = 433
                  TabStop = False
                  Caption = #1056#1072#1084#1082#1086#1074#1085#1072#1090#1072' '#1089#1087#1086#1075#1086#1076#1073#1072' '#1116#1077' '#1089#1077' '#1089#1082#1083#1091#1095#1080' '#1089#1086' '
                  DataBinding.DataField = 'RS_POVEKE_OPERATORI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1045#1076#1077#1085' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088
                      Value = 0
                    end
                    item
                      Caption = #1055#1086#1074#1077#1116#1077' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088#1080
                      Value = 1
                    end>
                  TabOrder = 8
                  Height = 43
                  Width = 413
                end
                object cxDBRadioGroupCentralnoTelo: TcxDBRadioGroup
                  Left = 196
                  Top = 237
                  TabStop = False
                  Caption = #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1112#1072' '#1089#1087#1088#1086#1074#1077#1076#1077' '#1094#1077#1085#1090#1088#1072#1083#1085#1086' '#1090#1077#1083#1086'?'
                  DataBinding.DataField = 'CENTRALNO_TELO'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 9
                  Height = 43
                  Width = 413
                end
                object cxDBRadioGroupDELIV: TcxDBRadioGroup
                  Left = 316
                  Top = 286
                  TabStop = False
                  Caption = #1044#1072#1083#1080' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1077' '#1076#1077#1083#1080#1074'?'
                  DataBinding.DataField = 'DELIV'
                  DataBinding.DataSource = dm.dsТender
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 10
                  Height = 43
                  Width = 293
                end
                object cxDBRadioGroupGrupnaNabavka: TcxDBRadioGroup
                  Left = 196
                  Top = 286
                  TabStop = False
                  Caption = #1043#1088#1091#1087#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  DataBinding.DataField = 'GRUPNA_NABAVKA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 11
                  Height = 43
                  Width = 114
                end
                object cxDBRadioGroupALTERNATIVNI_PONUDI: TcxDBRadioGroup
                  Left = 196
                  Top = 482
                  TabStop = False
                  Caption = #1044#1086#1079#1074#1086#1083#1077#1085#1086' '#1077' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1083#1090#1077#1088#1085#1072#1090#1080#1074#1085#1080' '#1087#1086#1085#1091#1076#1080
                  DataBinding.DataField = 'ALTERNATIVNI_PONUDI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 12
                  Height = 43
                  Width = 413
                end
                object VREMETRAENJE_DOG: TcxDBTextEdit
                  Left = 196
                  Top = 536
                  Anchors = [akLeft, akTop, akRight]
                  BeepOnEnter = False
                  DataBinding.DataField = 'VREMETRAENJE_DOG'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Style.Shadow = False
                  TabOrder = 13
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 413
                end
                object OPIS: TcxDBRichEdit
                  Left = 196
                  Top = 139
                  Hint = 
                    #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1087#1088#1077#1076#1084#1077#1090#1086#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' ' +
                    #1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'OPIS'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 5
                  OnDblClick = OPISDblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 43
                  Width = 413
                end
                object LICE_REALIZACIJA: TcxDBTextEdit
                  Left = 196
                  Top = 590
                  Hint = 
                    #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1088#1077#1072#1083#1080#1079#1080#1088#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076' '#1080#1079#1074#1088#1096#1077#1085#1072#1090#1072' '#1112#1072#1074#1085#1072' '#1085 +
                    #1072#1073#1072#1074#1082#1072
                  Anchors = [akLeft, akTop, akRight]
                  BeepOnEnter = False
                  DataBinding.DataField = 'LICE_REALIZACIJA'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Style.Shadow = False
                  TabOrder = 15
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 413
                end
                object LOKACIJA_ISPORAKA: TcxDBTextEdit
                  Left = 196
                  Top = 563
                  Hint = 
                    #1052#1077#1089#1090#1086' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1085#1072' '#1089#1090#1086#1082#1080#1090#1077' '#1080#1083#1080' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1091#1089#1083#1091#1075#1080#1090#1077' '#1080#1083#1080' '#1083#1086#1082#1072 +
                    #1094#1080#1112#1072' '#1085#1072' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1088#1072#1073#1086#1090#1080#1090#1077
                  Anchors = [akLeft, akTop, akRight]
                  BeepOnEnter = False
                  DataBinding.DataField = 'LOKACIJA_ISPORAKA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Style.Shadow = False
                  TabOrder = 14
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 413
                end
                object IZNOS: TcxDBTextEdit
                  Left = 196
                  Top = 617
                  Hint = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'IZNOS'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 16
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 117
                end
                object cxDBRadioGroup3: TcxDBRadioGroup
                  Left = 196
                  Top = 188
                  TabStop = False
                  Caption = #1044#1072#1083#1080' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1077' '#1086#1076' '#1086#1087#1092#1072#1090#1077#1085#1080#1090#1077' '#1076#1077#1112#1085#1086#1089#1090#1080' ('#1089#1077#1082#1090#1086#1088#1089#1082#1080' '#1076#1086#1075#1086#1074#1086#1088')?'
                  DataBinding.DataField = 'SEKTORSKI_DOGOVOR'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 17
                  Height = 43
                  Width = 413
                end
                object IZNOS_SO_DDV: TcxDBTextEdit
                  Left = 452
                  Top = 617
                  Hint = #1055#1088#1086#1094#1077#1085#1077#1090#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'IZNOS_SO_DDV'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 18
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 117
                end
              end
              object LICE_KONTAKT: TcxDBTextEdit
                Left = 17
                Top = 380
                BeepOnEnter = False
                DataBinding.DataField = 'LICE_KONTAKT'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Style.Shadow = False
                TabOrder = 7
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object EMAIL_KONTAKT: TcxDBTextEdit
                Left = 17
                Top = 416
                BeepOnEnter = False
                DataBinding.DataField = 'EMAIL_KONTAKT'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Style.Shadow = False
                TabOrder = 8
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object INTEREN_BROJ: TcxDBTextEdit
                Left = 17
                Top = 249
                BeepOnEnter = False
                DataBinding.DataField = 'INTEREN_BROJ'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Shadow = False
                TabOrder = 4
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object ARHIVSKI_BROJ: TcxDBTextEdit
                Left = 17
                Top = 285
                BeepOnEnter = False
                DataBinding.DataField = 'ARHIVSKI_BROJ'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Shadow = False
                TabOrder = 5
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object OGLAS: TcxDBTextEdit
                Left = 17
                Top = 320
                Hint = #1054#1075#1083#1072#1089' '#1079#1072' '#1112#1072#1074#1077#1085' '#1087#1086#1074#1080#1082' '#1074#1086' '#1089#1083#1091#1078#1073#1077#1085' '#1074#1077#1089#1085#1080#1082
                BeepOnEnter = False
                DataBinding.DataField = 'OGLAS'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Shadow = False
                TabOrder = 6
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object cxDBRadioGroupPONISTEN: TcxDBRadioGroup
                Left = 17
                Top = 488
                TabStop = False
                Caption = #1055#1086#1085#1080#1096#1090#1077#1085#1072'/'#1042#1072#1083#1080#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                DataBinding.DataField = 'PONISTEN'
                DataBinding.DataSource = dm.dsТender
                Properties.Columns = 2
                Properties.Items = <
                  item
                    Caption = #1055#1086#1085#1080#1096#1090#1077#1085#1072
                    Value = 0
                  end
                  item
                    Caption = #1042#1072#1083#1080#1076#1085#1072
                    Value = 1
                  end>
                TabOrder = 10
                OnExit = cxDBRadioGroupPONISTENExit
                Height = 43
                Width = 192
              end
              object cxDBRadioGroupZATVOREN: TcxDBRadioGroup
                Left = 73
                Top = 112
                TabStop = False
                Caption = #1057#1090#1072#1090#1091#1089' '
                DataBinding.DataField = 'ZATVOREN'
                DataBinding.DataSource = dm.dsТender
                Properties.Items = <
                  item
                    Caption = #1042#1086' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
                    Value = -1
                  end
                  item
                    Caption = #1054#1090#1074#1086#1088#1077#1085
                    Value = 0
                  end
                  item
                    Caption = #1047#1072#1090#1074#1086#1088#1077#1085
                    Value = 1
                  end
                  item
                    Caption = #1044#1077#1083#1091#1084#1085#1086' '#1079#1072#1074#1088#1096#1077#1085
                    Value = 2
                  end>
                TabOrder = 3
                Height = 97
                Width = 156
              end
              object VRSKA_PREDMET: TcxDBTextEdit
                Left = 17
                Top = 551
                Hint = 
                  #1054#1074#1072' '#1087#1086#1083#1077' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1080' '#1074#1086' '#1089#1083#1091#1095#1072#1112' '#1085#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072#1090#1072'. '#1058#1091#1082#1072' ' +
                  #1089#1077' '#1074#1085#1077#1089#1091#1074#1072' '#1073#1088#1086#1112#1086#1090' '#1085#1072' '#1085#1086#1074#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1088#1072#1089#1087#1080#1096#1077' '#1079#1072' '#1080#1089#1090#1072#1090#1072 +
                  ' '#1085#1072#1073#1072#1074#1082#1072
                TabStop = False
                BeepOnEnter = False
                DataBinding.DataField = 'VRSKA_PREDMET'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Shadow = False
                TabOrder = 11
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 192
              end
              object cxDBRadioGroupTIP_BARANJA: TcxDBRadioGroup
                Left = 17
                Top = 622
                Hint = 
                  #1053#1072#1095#1080#1085' '#1085#1072' '#1089#1087#1088#1086#1074#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088', '#1086#1076#1085#1086#1089#1085#1086' '#1073#1072#1088#1072#1114#1072#1090#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1086 +
                  #1076' '#1076#1086#1073#1072#1074#1091#1074#1072#1095#1080#1090#1077' '
                TabStop = False
                Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076#1085#1089#1085#1086' '#1073#1072#1088#1072#1114#1077#1090#1086' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
                DataBinding.DataField = 'TIP_BARANJA'
                DataBinding.DataSource = dm.dsТender
                Properties.Items = <
                  item
                    Caption = #1055#1086' '#1087#1086#1085#1091#1076#1072
                    Value = 0
                  end
                  item
                    Caption = #1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
                    Value = 1
                  end
                  item
                    Caption = #1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1073#1077#1079' '#1082#1086#1085#1090#1088#1086#1083#1072' '#1085#1072' '#1080#1079#1085#1086#1089' '#1086#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
                    Value = 2
                  end>
                Properties.WordWrap = True
                TabOrder = 15
                Height = 89
                Width = 316
              end
              object ButtonOtkazi2: TcxButton
                Left = 896
                Top = 686
                Width = 75
                Height = 25
                Hint = #1054#1090#1082#1072#1078#1080' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1086#1089#1085#1086#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                Action = aOtkazi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 18
              end
              object ButtonProdolzi_2: TcxButton
                Left = 720
                Top = 686
                Width = 89
                Height = 25
                Hint = 
                  #1042#1085#1077#1089#1080' '#1075#1080' '#1086#1089#1085#1086#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1080' '#1087#1088#1086#1076#1086#1083#1078#1080' '#1089#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080#1090 +
                  #1077' '#1079#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                Action = aOsnovniProdolzi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 16
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object ButtonZapisi2: TcxButton
                Left = 815
                Top = 686
                Width = 75
                Height = 25
                Hint = #1042#1085#1077#1089#1080' '#1075#1080' '#1086#1089#1085#1086#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aZapisi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 17
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object KONTAKT_TEL_FAKS: TcxDBTextEdit
                Left = 17
                Top = 453
                BeepOnEnter = False
                DataBinding.DataField = 'KONTAKT_TEL_FAKS'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Style.Shadow = False
                TabOrder = 9
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 251
              end
              object NAZIVPONISTEN_PRICINA: TcxDBTextEdit
                Left = 17
                Top = 593
                Hint = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1064#1080#1092#1088#1072
                BeepOnEnter = False
                DataBinding.DataField = 'PONISTEN_PRICINA'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.BeepOnError = True
                Properties.CharCase = ecUpperCase
                Style.Shadow = False
                TabOrder = 12
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 30
              end
              object PONISTEN_PRICINA_NAZIV: TcxDBLookupComboBox
                Left = 48
                Top = 593
                Hint = #1055#1088#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                DataBinding.DataField = 'PONISTEN_PRICINA'
                DataBinding.DataSource = dm.dsТender
                ParentFont = False
                Properties.ClearKey = 46
                Properties.DropDownListStyle = lsFixedList
                Properties.DropDownSizeable = True
                Properties.KeyFieldNames = 'ID'
                Properties.ListColumns = <
                  item
                    FieldName = 'NAZIV'
                  end>
                Properties.ListSource = dm.dsPricinaPonistuvanje
                Style.Font.Charset = RUSSIAN_CHARSET
                Style.Font.Color = clWindowText
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.IsFontAssigned = True
                TabOrder = 13
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 285
              end
              object cxDBCheckBox2: TcxDBCheckBox
                Left = 142
                Top = 29
                Caption = #1040#1082#1090#1080#1074#1077#1085
                DataBinding.DataField = 'STATUS'
                DataBinding.DataSource = dm.dsТender
                Properties.ImmediatePost = True
                Properties.ValueChecked = 1
                Properties.ValueUnchecked = 0
                Style.TextStyle = [fsBold]
                TabOrder = 19
                Width = 73
              end
            end
          end
        end
        object cxTabSheetPostapka: TcxTabSheet
          Caption = ' '#1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
          object cxScrollBoxPostapka: TcxScrollBox
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            AutoScroll = False
            HorzScrollBar.Range = 1002
            TabOrder = 0
            VertScrollBar.Position = 25
            VertScrollBar.Range = 733
            object PanelPostapka: TPanel
              Left = 0
              Top = -25
              Width = 1002
              Height = 733
              Align = alClient
              Enabled = False
              TabOrder = 0
              DesignSize = (
                1002
                733)
              object cxGroupBoxPostapka: TcxGroupBox
                Left = 16
                Top = 16
                Anchors = [akLeft, akTop, akRight]
                Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
                TabOrder = 0
                DesignSize = (
                  953
                  355)
                Height = 355
                Width = 953
                object Label1: TLabel
                  Left = 23
                  Top = 26
                  Width = 103
                  Height = 18
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label17: TLabel
                  Left = 23
                  Top = 186
                  Width = 284
                  Height = 23
                  AutoSize = False
                  Caption = #1054#1073#1088#1072#1079#1083#1086#1078#1077#1085#1080#1077' '#1079#1072' '#1082#1086#1088#1080#1089#1090#1077#1085#1072#1090#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label13: TLabel
                  Left = 24
                  Top = 303
                  Width = 314
                  Height = 18
                  AutoSize = False
                  Caption = #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object TIP_TENDER_NAZIV: TcxDBLookupComboBox
                  Tag = 1
                  Left = 90
                  Top = 42
                  Hint = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1053#1072#1079#1080#1074
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'TIP_TENDER'
                  DataBinding.DataSource = dm.dsТender
                  Properties.DropDownListStyle = lsFixedList
                  Properties.DropDownSizeable = True
                  Properties.KeyFieldNames = 'SIFRA'
                  Properties.ListColumns = <
                    item
                      Width = 100
                      FieldName = 'SIFRA'
                    end
                    item
                      Width = 600
                      FieldName = 'NAZIV'
                    end
                    item
                      Width = 120
                      FieldName = 'DENOVI_ZALBA'
                    end
                    item
                      Width = 300
                      FieldName = 'tip_god_plan_Naziv'
                    end>
                  Properties.ListFieldIndex = 1
                  Properties.ListSource = dm.dsTipNabavkaAktivni
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 839
                end
                object OBRAZLOZENIE_POSTAPKA: TcxDBRichEdit
                  Left = 23
                  Top = 208
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'OBRAZLOZENIE_POSTAPKA'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -13
                  Style.Font.Name = 'Arial'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 2
                  OnDblClick = OBRAZLOZENIE_POSTAPKADblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 89
                  Width = 906
                end
                object TIP_TENDER: TcxDBTextEdit
                  Tag = 1
                  Left = 23
                  Top = 42
                  Hint = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1064#1080#1092#1088#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'TIP_TENDER'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 68
                end
                object cxDBRadioGroupSKRATENI_ROKOVI: TcxDBRadioGroup
                  Left = 23
                  Top = 69
                  TabStop = False
                  Caption = #1055#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1089#1082#1088#1072#1090#1077#1085#1080' '#1088#1086#1082#1086#1074#1080
                  DataBinding.DataField = 'SKRATENI_ROKOVI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 3
                  Height = 43
                  Width = 409
                end
                object cxDBRadioGroupTIP_OCENA: TcxDBRadioGroup
                  Left = 23
                  Top = 118
                  TabStop = False
                  Caption = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  DataBinding.DataField = 'TIP_OCENA'
                  DataBinding.DataSource = dm.dsТender
                  Properties.Columns = 4
                  Properties.ImmediatePost = True
                  Properties.Items = <
                    item
                      Caption = #1053#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
                      Value = 0
                    end
                    item
                      Caption = #1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' '
                      Value = 1
                    end
                    item
                      Caption = 
                        #1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1074#1088#1079' '#1086#1089#1085#1086#1074#1072' '#1085#1072' '#1094#1077#1085#1072#1090#1072' '#1073#1077#1079' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085 +
                        #1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072' '
                      Value = 2
                    end
                    item
                      Caption = 
                        #1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1074#1088#1079' '#1086#1089#1085#1086#1074#1072' '#1085#1072' '#1094#1077#1085#1072#1090#1072' '#1089#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072 +
                        ' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072' '
                      Value = 3
                    end>
                  Properties.WordWrap = True
                  TabOrder = 4
                  Height = 62
                  Width = 906
                end
                object LICE_PODGOTOVKA: TcxDBTextEdit
                  Left = 24
                  Top = 317
                  Hint = 
                    #1054#1076#1075#1086#1074#1086#1088#1085#1086' '#1083#1080#1094#1077' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086 +
                    #1075#1086#1074#1086#1088
                  BeepOnEnter = False
                  DataBinding.DataField = 'LICE_PODGOTOVKA'
                  DataBinding.DataSource = dm.dsТender
                  ParentFont = False
                  Style.Shadow = False
                  TabOrder = 5
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 409
                end
              end
              object cxGroupBoxElektronskiSistem: TcxGroupBox
                Left = 16
                Top = 377
                Anchors = [akLeft, akTop, akRight]
                Caption = #1045#1083#1077#1082#1090#1088#1086#1085#1089#1082#1080' '#1089#1080#1089#1090#1077#1084' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080' '#1045#1057#1032#1053
                TabOrder = 1
                DesignSize = (
                  953
                  291)
                Height = 291
                Width = 953
                object Label54: TLabel
                  Left = 24
                  Top = 128
                  Width = 202
                  Height = 17
                  AutoSize = False
                  Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object cxDBRadioGroup1: TcxDBRadioGroup
                  Left = 23
                  Top = 26
                  TabStop = False
                  Caption = #1044#1072#1083#1080' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072' '#1116#1077' '#1073#1080#1076#1077' '#1089#1086' '#1082#1086#1088#1080#1089#1090#1077#1114#1077' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1080' '#1089#1088#1077#1076#1089#1090#1074#1072'?'
                  DataBinding.DataField = 'EL_SREDSTVA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 3
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 0
                  Height = 43
                  Width = 409
                end
                object cxDBRadioGroup2: TcxDBRadioGroup
                  Left = 24
                  Top = 75
                  TabStop = False
                  Caption = #1044#1072#1083#1080' '#1116#1077' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1080' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072' '#1072#1091#1082#1094#1080#1112#1072'?'
                  DataBinding.DataField = 'EL_AUKCIJA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 3
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 1
                  Height = 43
                  Width = 409
                end
                object INFO_AUKCIJA: TcxDBRichEdit
                  Left = 23
                  Top = 143
                  Hint = 
                    #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' '#1074#1086' '#1074#1088#1089#1082#1072' '#1089#1086' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072#1090#1072' '#1072#1091#1082#1094#1080#1112#1072' *'#1050#1083#1080#1082#1085 +
                    #1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'INFO_AUKCIJA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 2
                  OnDblClick = INFO_AUKCIJADblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 130
                  Width = 906
                end
              end
              object ButtonZapisi3: TcxButton
                Left = 815
                Top = 686
                Width = 75
                Height = 25
                Hint = #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                Action = aZapisi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 3
              end
              object ButtonOtkazi3: TcxButton
                Left = 896
                Top = 686
                Width = 75
                Height = 25
                Hint = #1054#1090#1082#1072#1078#1080' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                Action = aOtkazi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 4
              end
              object ButtonProdolzi_3: TcxButton
                Left = 720
                Top = 686
                Width = 89
                Height = 25
                Hint = 
                  #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '#1080' '#1087#1088#1086#1076#1086#1083#1078#1080' '#1089#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1088#1072#1074#1085 +
                  #1080', '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aOsnovniProdolzi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 2
              end
            end
          end
        end
        object cxTabSheetFinansiskiTehnicki: TcxTabSheet
          Caption = ' '#1055#1088#1072#1074#1085#1080', '#1045#1082#1086#1085#1086#1084#1089#1082#1080' '#1080' '#1060#1080#1085#1072#1085#1089#1080#1089#1082#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
          object cxScrollBoxFinansiski: TcxScrollBox
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            AutoScroll = False
            HorzScrollBar.Range = 1002
            TabOrder = 0
            VertScrollBar.Position = 25
            VertScrollBar.Range = 733
            object PanelFinansiski: TPanel
              Left = 0
              Top = -25
              Width = 1002
              Height = 733
              Align = alClient
              Enabled = False
              TabOrder = 0
              DesignSize = (
                1002
                733)
              object cxGroupBoxGaranciiAvansnoPlakjanje: TcxGroupBox
                Left = 16
                Top = 16
                Caption = #1043#1072#1088#1072#1085#1094#1080#1080' '#1080' '#1072#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077
                TabOrder = 0
                Height = 241
                Width = 713
                object Label48: TLabel
                  Left = 16
                  Top = 28
                  Width = 134
                  Height = 19
                  AutoSize = False
                  Caption = #1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label49: TLabel
                  Left = 197
                  Top = 29
                  Width = 178
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = '% '#1086#1076' '#1074#1088#1077#1076#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label50: TLabel
                  Left = 16
                  Top = 53
                  Width = 316
                  Height = 19
                  AutoSize = False
                  Caption = #1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077#1090#1085#1086' '#1080#1079#1074#1088#1096#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090':'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label52: TLabel
                  Left = 377
                  Top = 53
                  Width = 170
                  Height = 19
                  AutoSize = False
                  Caption = '% '#1086#1076' '#1074#1088#1077#1076#1085#1086#1089#1090#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object GARANCIJA_PONUDA: TcxDBTextEdit
                  Left = 152
                  Top = 26
                  BeepOnEnter = False
                  DataBinding.DataField = 'GARANCIJA_PONUDA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 41
                end
                object GARANCIJA_KVALITET: TcxDBTextEdit
                  Left = 330
                  Top = 50
                  BeepOnEnter = False
                  DataBinding.DataField = 'GARANCIJA_KVALITET'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 41
                end
                object cxDBRadioGroupAVANSNO: TcxDBRadioGroup
                  Left = 17
                  Top = 114
                  TabStop = False
                  Caption = #1040#1074#1072#1085#1089#1085#1086' '#1087#1083#1072#1116#1072#1114#1077
                  DataBinding.DataField = 'AVANSNO'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 3
                  Height = 48
                  Width = 129
                end
                object cxDBRadioGroupZDRUZUVANJE_OPERATORI: TcxDBRadioGroup
                  Left = 16
                  Top = 174
                  TabStop = False
                  Caption = 
                    #1047#1076#1088#1091#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1088#1091#1087#1072' '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1086#1087#1077#1088#1072#1090#1086#1088#1080' '#1074#1086' '#1087#1088#1072#1074#1085#1072' '#1092#1086#1088#1084#1072' '#1079#1072' '#1080#1079#1074#1088#1096 +
                    #1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
                  DataBinding.DataField = 'ZDRUZUVANJE_OPERATORI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.Columns = 2
                  Properties.Items = <
                    item
                      Caption = #1044#1072
                      Value = 1
                    end
                    item
                      Caption = #1053#1077
                      Value = 0
                    end>
                  TabOrder = 4
                  Height = 48
                  Width = 481
                end
                object cxDBCheckBox1: TcxDBCheckBox
                  Left = 13
                  Top = 75
                  Caption = #1048#1079#1112#1072#1074#1072' '#1079#1072' '#1089#1077#1088#1080#1086#1079#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
                  DataBinding.DataField = 'GARANCIJA_IZJAVA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentColor = False
                  ParentFont = False
                  Properties.Alignment = taRightJustify
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 1
                  Properties.ValueUnchecked = 0
                  Style.TextColor = clNavy
                  Style.TextStyle = [fsBold]
                  TabOrder = 2
                  Transparent = True
                  Width = 226
                end
              end
              object ButtonZapisi4: TcxButton
                Left = 815
                Top = 686
                Width = 75
                Height = 25
                Hint = #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1088#1072#1074#1085#1080#1090#1077', '#1077#1082#1086#1085#1086#1084#1089#1082#1080#1090#1077' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aZapisi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 2
              end
              object ButtonOtkazi4: TcxButton
                Left = 896
                Top = 686
                Width = 75
                Height = 25
                Hint = 
                  #1054#1090#1082#1072#1078#1080' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1088#1072#1074#1085#1080#1090#1077', '#1077#1082#1086#1085#1086#1084#1089#1082#1080#1090#1077' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094 +
                  #1080
                Action = aOtkazi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 3
              end
              object ButtonProdolzi_4: TcxButton
                Left = 720
                Top = 686
                Width = 89
                Height = 25
                Hint = 
                  #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1088#1072#1074#1085#1080#1090#1077', '#1077#1082#1086#1085#1086#1084#1089#1082#1080#1090#1077' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1080' '#1087#1088#1086#1076#1086#1083#1078 +
                  #1080' '#1089#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1072#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aOsnovniProdolzi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 1
              end
            end
          end
        end
        object cxTabSheetAdministrativni: TcxTabSheet
          Caption = ' '#1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
          object cxScrollBoxAdministrativni: TcxScrollBox
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            AutoScroll = False
            HorzScrollBar.Range = 1002
            TabOrder = 0
            VertScrollBar.Position = 25
            VertScrollBar.Range = 733
            object PanelAdministrativni: TPanel
              Left = 0
              Top = -25
              Width = 1002
              Height = 733
              Align = alClient
              Enabled = False
              TabOrder = 0
              DesignSize = (
                1002
                733)
              object Label59: TLabel
                Left = 33
                Top = 499
                Width = 89
                Height = 52
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object Label29: TLabel
                Left = 16
                Top = 285
                Width = 257
                Height = 19
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1055#1086#1085#1091#1076#1072#1090#1072' '#1084#1086#1088#1072' '#1076#1072' '#1073#1080#1076#1077' '#1074#1072#1083#1080#1076#1085#1072' '#1084#1080#1085#1080#1084#1091#1084' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object cxGroupBoxAdministrativniInformacii: TcxGroupBox
                Left = 16
                Top = 16
                Caption = 
                  #1059#1089#1083#1086#1074#1080' '#1079#1072' '#1076#1086#1073#1080#1074#1072#1114#1077' '#1085#1072' '#1090#1077#1085#1076#1077#1088#1089#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' '#1080' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1076#1086#1082 +
                  #1091#1084#1077#1085#1090#1080
                TabOrder = 0
                Height = 248
                Width = 641
                object Label55: TLabel
                  Left = 14
                  Top = 25
                  Width = 116
                  Height = 28
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1062#1077#1085#1072' '#1085#1072' '#1090#1077#1085#1076#1077#1088#1089#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label57: TLabel
                  Left = 14
                  Top = 186
                  Width = 116
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1077#1085#1072#1088#1089#1082#1072' '#1089#1084#1077#1090#1082#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label58: TLabel
                  Left = 25
                  Top = 213
                  Width = 105
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1077#1074#1080#1079#1085#1072' '#1089#1084#1077#1090#1082#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label56: TLabel
                  Left = 40
                  Top = 60
                  Width = 90
                  Height = 37
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1053#1072#1095#1080#1085' '#1080' '#1091#1089#1083#1086#1074#1080' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object CENA_DOKUMENTACIJA: TcxDBTextEdit
                  Left = 136
                  Top = 30
                  Hint = #1062#1077#1085#1072' '#1079#1072' '#1087#1086#1076#1080#1075#1072#1114#1077' '#1085#1072' '#1090#1077#1085#1076#1077#1088#1089#1082#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'CENA_DOKUMENTACIJA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 481
                end
                object DENARSKA: TcxDBTextEdit
                  Left = 136
                  Top = 183
                  BeepOnEnter = False
                  DataBinding.DataField = 'DENARSKA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 481
                end
                object DEVIZNA: TcxDBTextEdit
                  Left = 136
                  Top = 210
                  BeepOnEnter = False
                  DataBinding.DataField = 'DEVIZNA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Style.Shadow = False
                  TabOrder = 3
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 481
                end
                object INFO_PLAKJANJE: TcxDBRichEdit
                  Left = 136
                  Top = 57
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  DataBinding.DataField = 'INFO_PLAKJANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 1
                  OnDblClick = INFO_PLAKJANJEDblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 120
                  Width = 481
                end
              end
              object cxGroupBoxOtvarawePonudi: TcxGroupBox
                Left = 16
                Top = 390
                Hint = 
                  #1032#1072#1074#1085#1086#1090#1086' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077' '#1116#1077' '#1089#1077' '#1086#1076#1088#1078#1080' '#1085#1072' '#1076#1077#1085#1086#1090' '#1080' '#1074#1086' '#1095#1072#1089#1086#1090' '#1086#1087#1088 +
                  #1077#1076#1077#1083#1077#1085' '#1082#1072#1082#1086' '#1082#1088#1072#1077#1085' '#1088#1086#1082' '#1079#1072' '#1076#1086#1089#1090#1072#1074#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077' '#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1086#1090#1086' '#1084 +
                  #1077#1089#1090#1086
                Caption = #1059#1089#1083#1086#1074#1080' '#1079#1072' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077
                TabOrder = 3
                DesignSize = (
                  641
                  90)
                Height = 90
                Width = 641
                object Label20: TLabel
                  Left = 49
                  Top = 29
                  Width = 57
                  Height = 21
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label21: TLabel
                  Left = 330
                  Top = 29
                  Width = 50
                  Height = 18
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1042#1088#1077#1084#1077' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label22: TLabel
                  Left = 63
                  Top = 56
                  Width = 43
                  Height = 18
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1052#1077#1089#1090#1086' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object VREME_OTVARANJE: TcxDBTimeEdit
                  Left = 386
                  Top = 26
                  DataBinding.DataField = 'VREME_OTVARANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object DATUM_OTVARANJE: TcxDBDateEdit
                  Left = 112
                  Top = 26
                  DataBinding.DataField = 'DATUM_OTVARANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object MESTO_OTVARANJE: TcxDBTextEdit
                  Left = 112
                  Top = 53
                  Anchors = [akLeft, akTop, akRight]
                  BeepOnEnter = False
                  DataBinding.DataField = 'MESTO_OTVARANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 505
                end
              end
              object DOPOLNITELNI_INFO: TcxDBRichEdit
                Left = 128
                Top = 496
                Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                DataBinding.DataField = 'DOPOLNITELNI_INFO'
                DataBinding.DataSource = dm.dsTenderInfo
                ParentFont = False
                Properties.WantReturns = False
                Style.Font.Charset = RUSSIAN_CHARSET
                Style.Font.Color = clBlack
                Style.Font.Height = -11
                Style.Font.Name = 'Tahoma'
                Style.Font.Style = []
                Style.IsFontAssigned = True
                TabOrder = 4
                OnDblClick = DOPOLNITELNI_INFODblClick
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Height = 185
                Width = 505
              end
              object cxGroupBoxPeriodNaPrimawePonudi: TcxGroupBox
                Left = 16
                Top = 314
                Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1087#1088#1080#1084#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080'/'#1087#1088#1080#1112#1072#1074#1080
                TabOrder = 2
                Height = 63
                Width = 641
                object Label18: TLabel
                  Left = 39
                  Top = 29
                  Width = 67
                  Height = 21
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label19: TLabel
                  Left = 312
                  Top = 29
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object DATUM_OD: TcxDBDateEdit
                  Left = 112
                  Top = 26
                  DataBinding.DataField = 'DATUM_OD'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object DATUM_DO: TcxDBDateEdit
                  Left = 386
                  Top = 26
                  DataBinding.DataField = 'DATUM_DO'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.DisplayFormat = 'dd.mm.yyyy hh:mm'
                  Properties.Kind = ckDateTime
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 143
                end
              end
              object VALIDNOST_PONUDA: TcxDBTextEdit
                Left = 279
                Top = 282
                BeepOnEnter = False
                DataBinding.DataField = 'VALIDNOST_PONUDA'
                DataBinding.DataSource = dm.dsTenderInfo
                ParentFont = False
                Properties.BeepOnError = True
                Style.Shadow = False
                TabOrder = 1
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 354
              end
              object ButtonZapisi5: TcxButton
                Left = 815
                Top = 686
                Width = 75
                Height = 25
                Hint = #1042#1085#1077#1089#1080' '#1075#1080' '#1072#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aZapisi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 6
              end
              object ButtonOtkazi5: TcxButton
                Left = 896
                Top = 686
                Width = 75
                Height = 25
                Hint = #1054#1090#1082#1072#1078#1080' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1072#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080
                Action = aOtkazi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 7
              end
              object ButtonProdolzi_5: TcxButton
                Left = 720
                Top = 686
                Width = 89
                Height = 25
                Hint = 
                  #1042#1085#1077#1089#1080' '#1075#1080' '#1072#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1080#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1080' '#1087#1088#1086#1076#1086#1083#1078#1080' '#1089#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1086 +
                  #1076#1083#1091#1082#1080'/'#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090#1080
                Action = aOsnovniProdolzi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 5
              end
            end
          end
        end
        object cxTabSheetOdlukiSoglasnosti: TcxTabSheet
          Caption = ' '#1054#1076#1083#1091#1082#1080'/'#1057#1086#1075#1083#1072#1089#1085#1086#1089#1090#1080
          object cxScrollBoxOdlukiSoglasnosti: TcxScrollBox
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            AutoScroll = False
            HorzScrollBar.Range = 1002
            TabOrder = 0
            VertScrollBar.Position = 11
            VertScrollBar.Range = 733
            object PanelOdluki: TPanel
              Left = 0
              Top = -11
              Width = 1002
              Height = 733
              Align = alClient
              BevelInner = bvLowered
              BevelOuter = bvSpace
              Enabled = False
              TabOrder = 0
              DesignSize = (
                1002
                733)
              object Label62: TLabel
                Left = 425
                Top = 249
                Width = 82
                Height = 27
                Alignment = taRightJustify
                AutoSize = False
                Caption = #1048#1079#1074#1086#1088' '#1085#1072' '#1089#1088#1077#1076#1089#1090#1074#1072' :'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
                WordWrap = True
              end
              object cxGroupBoxZalbiZaPredmet: TcxGroupBox
                Left = 415
                Top = 26
                Anchors = [akLeft, akTop, akRight]
                Caption = #1046#1072#1083#1073#1080' '#1079#1072' '#1087#1088#1077#1076#1084#1077#1090
                TabOrder = 8
                DesignSize = (
                  560
                  118)
                Height = 118
                Width = 560
                object Label60: TLabel
                  Left = 248
                  Top = 28
                  Width = 89
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' '#1085#1072' '#1078#1072#1083#1073#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label61: TLabel
                  Left = 27
                  Top = 53
                  Width = 65
                  Height = 34
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1056#1077#1079#1091#1083#1072#1090' '#1087#1086' '#1078#1072#1083#1073#1072' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object cxDBCheckBoxZALBA: TcxDBCheckBox
                  Left = 98
                  Top = 25
                  TabStop = False
                  Caption = #1055#1086#1076#1085#1077#1089#1077#1085#1072' '#1078#1072#1083#1073#1072
                  DataBinding.DataField = 'ZALBA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.ImmediatePost = True
                  Properties.ValueChecked = 1
                  Properties.ValueUnchecked = 0
                  TabOrder = 0
                  Transparent = True
                  Width = 114
                end
                object BROJ_ZALBI: TcxDBTextEdit
                  Left = 343
                  Top = 25
                  BeepOnEnter = False
                  DataBinding.DataField = 'BROJ_ZALBI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object ZALBA_OPIS: TcxDBMemo
                  Left = 98
                  Top = 52
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'ZALBA_OPIS'
                  DataBinding.DataSource = dm.dsTenderInfo
                  Properties.WantReturns = False
                  TabOrder = 2
                  OnDblClick = ZALBA_OPISDblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 50
                  Width = 444
                end
              end
              object cxGroupBoxAneksDogovor: TcxGroupBox
                Left = 18
                Top = 560
                Caption = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
                TabOrder = 7
                Height = 88
                Width = 358
                object Label37: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label38: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label39: TLabel
                  Left = 15
                  Top = 56
                  Width = 67
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1048#1079#1085#1086#1089' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object ANEKS_DATUM: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  DataBinding.DataField = 'ANEKS_DATUM'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ANEKS_BROJ: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  BeepOnEnter = False
                  DataBinding.DataField = 'ANEKS_BROJ'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object ANEKS_IZNOS: TcxDBTextEdit
                  Left = 88
                  Top = 53
                  BeepOnEnter = False
                  DataBinding.DataField = 'ANEKS_IZNOS'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxIzvorSredstva: TcxGroupBox
                Left = 415
                Top = 150
                Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1086#1073#1077#1079#1073#1077#1076#1077#1085#1080' '#1089#1088#1077#1076#1089#1090#1074#1072' '#1086#1076' '#1084#1080#1085#1080#1089#1090#1077#1088#1089#1090#1074#1086' '#1079#1072' '#1092#1080#1085#1072#1085#1089#1080#1080
                TabOrder = 9
                Height = 88
                Width = 560
                object Label47: TLabel
                  Left = 23
                  Top = 56
                  Width = 67
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1048#1079#1085#1086#1089' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label45: TLabel
                  Left = 50
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label46: TLabel
                  Left = 212
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object POTVRDA_DAT: TcxDBDateEdit
                  Left = 269
                  Top = 26
                  DataBinding.DataField = 'POTVRDA_DAT'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object POTVRDA_BR: TcxDBTextEdit
                  Left = 98
                  Top = 26
                  BeepOnEnter = False
                  DataBinding.DataField = 'POTVRDA_BR'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object POTVRDA_IZNOS: TcxDBTextEdit
                  Left = 98
                  Top = 53
                  BeepOnEnter = False
                  DataBinding.DataField = 'POTVRDA_IZNOS'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxSoglasnostOdBiroto: TcxGroupBox
                Left = 16
                Top = 94
                Caption = #1057#1086#1075#1083#1072#1089#1085#1086#1089#1090' '#1086#1076' '#1073#1080#1088#1086' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080
                TabOrder = 1
                Height = 62
                Width = 358
                object Label32: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label33: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object SOGLASNOST_DAT: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090' '#1086#1076' '#1073#1080#1088#1086#1090#1086' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080
                  DataBinding.DataField = 'SOGLASNOST_DAT'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object SOGLASNOST_BR: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = #1041#1088#1086#1112' '#1085#1072' '#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090' '#1086#1076' '#1073#1080#1088#1086#1090#1086' '#1079#1072' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080
                  BeepOnEnter = False
                  DataBinding.DataField = 'SOGLASNOST_BR'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxOdlukaNajpovolenPonuduvac: TcxGroupBox
                Left = 16
                Top = 325
                Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1077#1085' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
                TabOrder = 4
                Height = 62
                Width = 358
                object Label43: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label44: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object ODLUKA_DATUM: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1077#1085' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
                  DataBinding.DataField = 'ODLUKA_DATUM'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ODLUKA_BROJ: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1077#1085' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
                  BeepOnEnter = False
                  DataBinding.DataField = 'ODLUKA_BROJ'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxOdlukaZgolemuvanjeIznos: TcxGroupBox
                Left = 18
                Top = 465
                Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090
                TabOrder = 6
                Height = 89
                Width = 358
                object Label42: TLabel
                  Left = 39
                  Top = 56
                  Width = 43
                  Height = 19
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1048#1079#1085#1086#1089' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label40: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label41: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object DATUM_ODLUKA_PROSIRUVANJE: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090' '#1085#1072' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072
                  DataBinding.DataField = 'DATUM_ODLUKA_PROSIRUVANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ODLUKA_PROSIRUVANJE: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1079#1075#1086#1083#1077#1084#1091#1074#1072#1114#1077' '#1085#1072' '#1080#1079#1085#1086#1089#1086#1090' '#1085#1072' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'ODLUKA_PROSIRUVANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object IZNOS_PROSIRUVANJE: TcxDBTextEdit
                  Left = 88
                  Top = 53
                  Hint = #1048#1079#1085#1086#1089' '#1079#1072' '#1082#1086#1112' '#1089#1077' '#1087#1088#1086#1096#1080#1088#1091#1074#1072' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072' '
                  BeepOnEnter = False
                  DataBinding.DataField = 'IZNOS_PROSIRUVANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxOdlukaPokani: TcxGroupBox
                Left = 16
                Top = 162
                Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072#1085#1080
                TabOrder = 2
                Height = 89
                Width = 358
                object Label34: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label35: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label36: TLabel
                  Left = 11
                  Top = 48
                  Width = 71
                  Height = 27
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088'. '#1087#1080#1089#1084#1077#1085#1080' '#1087#1086#1082#1072#1085#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object ODL_POKANA_DAT: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = 
                    #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072 +
                    #1085#1080
                  DataBinding.DataField = 'ODL_POKANA_DAT'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ODL_POKANA_BR: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = 
                    #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1076#1086' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1087#1086#1082#1072#1085 +
                    #1080
                  BeepOnEnter = False
                  DataBinding.DataField = 'ODL_POKANA_BR'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object BROJ_POKANI: TcxDBTextEdit
                  Left = 88
                  Top = 53
                  Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1082#1072#1085#1080' '#1082#1086#1080' '#1116#1077' '#1089#1077' '#1076#1086#1089#1090#1072#1074#1072#1090' '#1076#1086' '#1086#1076#1088#1077#1076#1077#1085#1080' '#1082#1072#1085#1076#1080#1076#1072#1090#1080
                  BeepOnEnter = False
                  DataBinding.DataField = 'BROJ_POKANI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxOdlukiZaNabavka: TcxGroupBox
                Left = 16
                Top = 26
                Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
                TabOrder = 0
                Height = 62
                Width = 358
                object Label30: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label31: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object ODL_NABAVKA_DAT: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1086#1090#1087#1086#1095#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  DataBinding.DataField = 'ODL_NABAVKA_DAT'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ODL_NABAVKA_BR: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = #1041#1088#1086#1112' '#1085#1072' '#1086#1076#1083#1091#1082#1072' '#1079#1072' '#1086#1090#1087#1086#1095#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'ODL_NABAVKA_BR'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
              object cxGroupBoxBrKandidatiPokaneti: TcxGroupBox
                Left = 16
                Top = 257
                Caption = #1041#1088#1086#1112' '#1085#1072' '#1082#1072#1085#1076#1080#1076#1072#1090#1080' '#1096#1090#1086' '#1116#1077' '#1073#1080#1076#1072#1090' '#1087#1086#1082#1072#1085#1077#1090#1080
                TabOrder = 3
                Height = 62
                Width = 358
                object Label65: TLabel
                  Left = 16
                  Top = 29
                  Width = 66
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1052#1080#1085#1080#1084#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label66: TLabel
                  Left = 184
                  Top = 29
                  Width = 71
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1052#1072#1082#1089#1080#1084#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object MIN_PONUDI: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  BeepOnEnter = False
                  DataBinding.DataField = 'MIN_PONUDI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
                object MAKS_PONUDI: TcxDBTextEdit
                  Left = 261
                  Top = 26
                  BeepOnEnter = False
                  DataBinding.DataField = 'MAKS_PONUDI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
              end
              object cxGroupBoxZapisnikOtvaranjePonudi: TcxGroupBox
                Left = 415
                Top = 282
                Anchors = [akLeft, akTop, akRight]
                Caption = #1047#1072' '#1079#1072#1087#1080#1089#1085#1080#1082' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1087#1086#1085#1091#1076#1080
                TabOrder = 11
                DesignSize = (
                  560
                  294)
                Height = 294
                Width = 560
                object Label23: TLabel
                  Left = 28
                  Top = 18
                  Width = 77
                  Height = 29
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1087#1080#1089#1085#1080#1082' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label24: TLabel
                  Left = 24
                  Top = 50
                  Width = 81
                  Height = 32
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1042#1082#1091#1087#1085#1086' '#1087#1086#1085#1091#1076#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label25: TLabel
                  Left = 194
                  Top = 49
                  Width = 101
                  Height = 30
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088'. '#1085#1072' '#1079#1072#1076#1086#1094#1085#1077#1090#1080' '#1087#1086#1085#1091#1076#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label26: TLabel
                  Left = 383
                  Top = 49
                  Width = 109
                  Height = 32
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' '#1085#1072' '#1086#1096#1090#1077#1090#1077#1085#1080' '#1082#1086#1074#1077#1088#1090#1080' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label27: TLabel
                  Left = 16
                  Top = 92
                  Width = 89
                  Height = 61
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1050#1086#1074#1077#1088#1090#1080' '#1079#1072' '#1087#1086#1074#1083#1077#1082'. '#1087#1086#1085#1091#1076#1072' '#1089#1086' '#1080#1084#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074'. :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label28: TLabel
                  Left = 16
                  Top = 159
                  Width = 89
                  Height = 68
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1050#1086#1074#1077#1088#1090#1080' '#1079#1072' '#1080#1079#1084#1077#1085#1072' '#1080#1083#1080' '#1079#1072#1084#1077#1085#1072' '#1089#1086' '#1080#1084#1077' '#1085#1072' '#1087#1086#1085#1091#1076'. :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object Label76: TLabel
                  Left = 11
                  Top = 230
                  Width = 94
                  Height = 64
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077' : '
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                  WordWrap = True
                end
                object DATUM_ZAPISNIK: TcxDBDateEdit
                  Left = 111
                  Top = 26
                  DataBinding.DataField = 'DATUM_ZAPISNIK'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object VKUPNO_PONUDI: TcxDBTextEdit
                  Left = 111
                  Top = 53
                  BeepOnEnter = False
                  DataBinding.DataField = 'VKUPNO_PONUDI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 40
                end
                object BR_ZADOCNETI_PONUDI: TcxDBTextEdit
                  Left = 301
                  Top = 53
                  BeepOnEnter = False
                  DataBinding.DataField = 'BR_ZADOCNETI_PONUDI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 40
                end
                object ZA_POVLEKUVANJE: TcxDBRichEdit
                  Left = 111
                  Top = 88
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'ZA_POVLEKUVANJE'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 4
                  OnDblClick = ZA_POVLEKUVANJEDblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 65
                  Width = 430
                end
                object ZA_IZMENA: TcxDBRichEdit
                  Left = 111
                  Top = 159
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'ZA_IZMENA'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 5
                  OnDblClick = ZA_IZMENADblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 65
                  Width = 431
                end
                object OSTETENI_KOVERTI: TcxDBTextEdit
                  Tag = 1
                  Left = 498
                  Top = 53
                  BeepOnEnter = False
                  DataBinding.DataField = 'OSTETENI_KOVERTI'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 3
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 40
                end
                object ZABELESKA_ZAPISNIK: TcxDBRichEdit
                  Left = 111
                  Top = 230
                  Hint = 
                    #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1085#1072' '#1086#1074#1083#1072#1089#1090#1077#1085#1080#1090#1077' '#1087#1088#1077#1090#1089#1090#1072#1074#1085#1080#1094#1080' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077' *'#1050#1083#1080#1082#1085#1077#1090#1077' ' +
                    #1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
                  Anchors = [akLeft, akTop, akRight]
                  DataBinding.DataField = 'ZABELESKA_ZAPISNIK'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.WantReturns = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clBlack
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 6
                  OnDblClick = ZABELESKA_ZAPISNIKDblClick
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 47
                  Width = 431
                end
              end
              object ButtonZapisi6: TcxButton
                Left = 815
                Top = 686
                Width = 75
                Height = 25
                Hint = #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1086#1076#1083#1091#1082#1080'/'#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090#1080
                Action = aZapisi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 13
              end
              object ButtonOtkazi6: TcxButton
                Left = 896
                Top = 686
                Width = 75
                Height = 25
                Hint = #1054#1090#1082#1072#1078#1080' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1086#1076#1083#1091#1082#1080'/'#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090#1080
                Action = aOtkazi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 14
              end
              object ButtonProdolzi_6: TcxButton
                Left = 720
                Top = 686
                Width = 89
                Height = 25
                Hint = 
                  #1042#1085#1077#1089#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1079#1072' '#1086#1076#1083#1091#1082#1080'/'#1089#1086#1075#1083#1072#1089#1085#1086#1089#1090#1080' '#1080' '#1087#1088#1086#1076#1086#1083#1078#1080' '#1089#1086' '#1074#1085#1077#1089#1091#1074#1072#1114 +
                  #1077' '#1085#1072' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
                Action = aOsnovniProdolzi
                Anchors = [akTop, akRight]
                Colors.Pressed = clGradientActiveCaption
                TabOrder = 12
              end
              object IZVOR_SREDSTVA: TcxDBComboBox
                Left = 513
                Top = 252
                Hint = #1048#1079#1074#1086#1088' '#1085#1072' '#1089#1088#1077#1076#1089#1090#1074#1072
                DataBinding.DataField = 'IZVOR_SREDSTVA'
                DataBinding.DataSource = dm.dsTenderInfo
                Properties.DropDownSizeable = True
                Properties.Items.Strings = (
                  #1041#1091#1119#1077#1090' '#1085#1072' '#1056#1052
                  #1041#1091#1119#1077#1090' '#1085#1072' '#1045#1051#1057
                  #1057#1086#1087#1089#1090#1074#1077#1085#1080' '#1087#1088#1080#1093#1086#1076#1080
                  #1044#1086#1085#1072#1094#1080#1080
                  #1050#1086#1084#1073#1080#1085#1080#1088#1072#1085#1080' '#1080#1079#1074#1086#1088#1080' '#1085#1072' '#1092#1080#1085#1072#1085#1089#1080#1088#1072#1114#1077
                  #1044#1088#1091#1075#1080' '#1080#1079#1074#1086#1088#1080' '#1085#1072' '#1092#1080#1085#1072#1085#1089#1080#1088#1072#1114#1077)
                TabOrder = 10
                Width = 440
              end
              object cxGroupBoxIzvestaOdSprovedenaPostapka: TcxGroupBox
                Left = 16
                Top = 397
                Caption = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' '#1079#1072' '#1080#1079#1074#1077#1096#1090#1072#1112' '#1086#1076' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                TabOrder = 5
                Height = 62
                Width = 358
                object lbl2: TLabel
                  Left = 42
                  Top = 29
                  Width = 40
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1041#1088#1086#1112' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object lbl3: TLabel
                  Left = 204
                  Top = 29
                  Width = 51
                  Height = 17
                  Alignment = taRightJustify
                  AutoSize = False
                  Caption = #1044#1072#1090#1091#1084' :'
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clNavy
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object ARH_DATUM_ISP: TcxDBDateEdit
                  Left = 261
                  Top = 26
                  Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1112' '#1086#1076' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                  DataBinding.DataField = 'ARH_DATUM_ISP'
                  DataBinding.DataSource = dm.dsTenderInfo
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 80
                end
                object ARH_BR_ISP: TcxDBTextEdit
                  Left = 88
                  Top = 26
                  Hint = #1040#1088#1093#1080#1074#1089#1082#1080' '#1073#1088#1086#1112' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1112' '#1086#1076' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
                  BeepOnEnter = False
                  DataBinding.DataField = 'ARH_BR_ISP'
                  DataBinding.DataSource = dm.dsTenderInfo
                  ParentFont = False
                  Properties.BeepOnError = True
                  Properties.CharCase = ecUpperCase
                  Style.Shadow = False
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 85
                end
              end
            end
          end
        end
        object cxTabSheetKriteriumBodirawe: TcxTabSheet
          Caption = ' '#1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
          object PanelKriterium: TPanel
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            TabOrder = 0
            DesignSize = (
              1020
              520)
            object cxGrid2: TcxGrid
              Left = 16
              Top = 16
              Width = 986
              Height = 257
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 0
              object cxGrid2DBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = dm.dsTenderGenBodiranje
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '0.00 , .'
                    Kind = skSum
                    OnGetText = cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                    Column = cxGrid2DBTableView1MAX_BODOVI
                  end>
                DataController.Summary.SummaryGroups = <>
                FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
                FilterRow.Visible = True
                OptionsBehavior.IncSearch = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
                OptionsView.Footer = True
                object cxGrid2DBTableView1TENDERGODINA: TcxGridDBColumn
                  DataBinding.FieldName = 'TENDERGODINA'
                  Visible = False
                  Options.Editing = False
                  Width = 65
                end
                object cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn
                  DataBinding.FieldName = 'BROJ_TENDER'
                  Options.Editing = False
                  Width = 93
                end
                object cxGrid2DBTableView1BODIRANJE_PO: TcxGridDBColumn
                  DataBinding.FieldName = 'BODIRANJE_PO'
                  Visible = False
                  Options.Editing = False
                  Width = 107
                end
                object cxGrid2DBTableView1KRITERIUMNAZIV: TcxGridDBColumn
                  DataBinding.FieldName = 'KRITERIUMNAZIV'
                  Options.Editing = False
                  Width = 290
                end
                object cxGrid2DBTableView1KRITERIUMVREDNUVANJE: TcxGridDBColumn
                  DataBinding.FieldName = 'KRITERIUMVREDNUVANJE'
                  Visible = False
                  Options.Editing = False
                  Width = 115
                end
                object cxGrid2DBTableView1vrednuvanjeNaziv: TcxGridDBColumn
                  DataBinding.FieldName = 'vrednuvanjeNaziv'
                  Options.Editing = False
                  Width = 249
                end
                object cxGrid2DBTableView1KRITERIUMBODIRANJE: TcxGridDBColumn
                  DataBinding.FieldName = 'KRITERIUMBODIRANJE'
                  Visible = False
                  Options.Editing = False
                  Width = 105
                end
                object cxGrid2DBTableView1bodiranjeNaziv: TcxGridDBColumn
                  DataBinding.FieldName = 'bodiranjeNaziv'
                  Options.Editing = False
                  Width = 125
                end
                object cxGrid2DBTableView1MAX_BODOVI: TcxGridDBColumn
                  DataBinding.FieldName = 'MAX_BODOVI'
                  Options.Editing = False
                  Styles.Content = dmRes.cxStyle120
                  Width = 95
                end
                object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_INS'
                  Visible = False
                  Options.Editing = False
                  Width = 158
                end
                object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_UPD'
                  Visible = False
                  Options.Editing = False
                  Width = 166
                end
                object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_INS'
                  Visible = False
                  Options.Editing = False
                  Width = 197
                end
                object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_UPD'
                  Visible = False
                  Options.Editing = False
                  Width = 237
                end
              end
              object cxGrid2Level1: TcxGridLevel
                GridView = cxGrid2DBTableView1
              end
            end
            object kriteriumButtonDodadi: TcxButton
              Left = 16
              Top = 286
              Width = 105
              Height = 25
              Action = aDodadiKriterium
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 1
              OnKeyDown = EnterKakoTab
            end
            object kriteriumButtonIzvadi: TcxButton
              Left = 127
              Top = 286
              Width = 105
              Height = 25
              Action = aIzvadiStar
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 2
              OnKeyDown = EnterKakoTab
            end
          end
        end
        object cxTabSheetKomisija: TcxTabSheet
          Caption = ' '#1050#1086#1084#1080#1089#1080#1112#1072
          ImageIndex = 6
          object PanelKomisija: TPanel
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            TabOrder = 0
            DesignSize = (
              1020
              520)
            object cxGrid3: TcxGrid
              Left = 16
              Top = 16
              Width = 986
              Height = 257
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object cxGridDBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = dm.dsTenderKomisija
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '0.00 , .'
                    Kind = skSum
                  end>
                DataController.Summary.SummaryGroups = <>
                FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
                FilterRow.Visible = True
                OptionsBehavior.IncSearch = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1082#1086#1084#1080#1089#1080#1112#1072
                OptionsView.Footer = True
                object cxGridDBTableView1BROJ_TENDER: TcxGridDBColumn
                  DataBinding.FieldName = 'BROJ_TENDER'
                  Options.Editing = False
                  Width = 93
                end
                object cxGridDBTableView1CLEN: TcxGridDBColumn
                  DataBinding.FieldName = 'CLEN'
                  Visible = False
                  Options.Editing = False
                  Width = 141
                end
                object cxGridDBTableView1REDBR: TcxGridDBColumn
                  DataBinding.FieldName = 'REDBR'
                  Options.Editing = False
                  Width = 65
                end
                object cxGridDBTableView1IME_PREZIME: TcxGridDBColumn
                  DataBinding.FieldName = 'IME_PREZIME'
                  Options.Editing = False
                  Width = 248
                end
                object cxGridDBTableView1FUNKCIJA: TcxGridDBColumn
                  DataBinding.FieldName = 'FUNKCIJA'
                  Options.Editing = False
                  Width = 267
                end
                object cxGridDBTableView1PRISUTEN: TcxGridDBColumn
                  DataBinding.FieldName = 'PRISUTEN'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Properties.ValueChecked = 1
                  Properties.ValueUnchecked = 0
                  Properties.OnChange = cxGridDBTableView1PRISUTENPropertiesChange
                end
                object cxGridDBTableView1TS_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_INS'
                  Visible = False
                  Options.Editing = False
                  Width = 156
                end
                object cxGridDBTableView1TS_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_UPD'
                  Visible = False
                  Options.Editing = False
                  Width = 213
                end
                object cxGridDBTableView1USR_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_INS'
                  Visible = False
                  Options.Editing = False
                  Width = 205
                end
                object cxGridDBTableView1USR_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_UPD'
                  Visible = False
                  Options.Editing = False
                  Width = 242
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = cxGridDBTableView1
              end
            end
            object ButtonDodadiKомисија: TcxButton
              Left = 16
              Top = 286
              Width = 105
              Height = 25
              Action = aDodadiKomisija
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 1
              OnKeyDown = EnterKakoTab
            end
            object ButtonIzvadiKомисија: TcxButton
              Left = 127
              Top = 286
              Width = 105
              Height = 25
              Action = aIzvadiKomisija
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 2
              OnKeyDown = EnterKakoTab
            end
          end
        end
        object cxTabSheetPotrebniDokumenti: TcxTabSheet
          Hint = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080#1090#1077'    '
          Caption = #1050#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090#1072'  '
          ImageIndex = 7
          object PanelPotrebniDokumenti: TPanel
            Left = 0
            Top = 0
            Width = 1020
            Height = 520
            Align = alClient
            TabOrder = 0
            DesignSize = (
              1020
              520)
            object cxGrid4: TcxGrid
              Left = 16
              Top = 16
              Width = 986
              Height = 449
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 0
              object cxGridDBTableView2: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = dm.dsTenderPotrebniDok
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Format = '0.00 , .'
                    Kind = skSum
                  end>
                DataController.Summary.SummaryGroups = <>
                FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
                FilterRow.Visible = True
                OptionsBehavior.IncSearch = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1082#1086#1084#1080#1089#1080#1112#1072
                OptionsView.Footer = True
                object cxGridDBTableView2BROJ_TENDER: TcxGridDBColumn
                  DataBinding.FieldName = 'BROJ_TENDER'
                  Visible = False
                  Width = 117
                end
                object cxGridDBTableView2TIP: TcxGridDBColumn
                  DataBinding.FieldName = 'TIP'
                  Visible = False
                  Width = 138
                end
                object cxGridDBTableView2POTREBNI_DOKUMENTI_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'POTREBNI_DOKUMENTI_ID'
                  Visible = False
                  Width = 105
                end
                object cxGridDBTableView2tipNaziv: TcxGridDBColumn
                  DataBinding.FieldName = 'tipNaziv'
                  Width = 209
                end
                object cxGridDBTableView2GRUPANAZIV: TcxGridDBColumn
                  DataBinding.FieldName = 'GRUPANAZIV'
                  Width = 224
                end
                object cxGridDBTableView2NAZIV: TcxGridDBColumn
                  DataBinding.FieldName = 'NAZIV'
                  Width = 500
                end
                object cxGridDBTableView2TS_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_INS'
                  Visible = False
                  Width = 161
                end
                object cxGridDBTableView2TS_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'TS_UPD'
                  Visible = False
                  Width = 215
                end
                object cxGridDBTableView2USR_INS: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_INS'
                  Visible = False
                  Width = 203
                end
                object cxGridDBTableView2USR_UPD: TcxGridDBColumn
                  DataBinding.FieldName = 'USR_UPD'
                  Visible = False
                  Width = 238
                end
              end
              object cxGridLevel2: TcxGridLevel
                GridView = cxGridDBTableView2
              end
            end
            object cxButtonDodadiDokument: TcxButton
              Left = 16
              Top = 500
              Width = 105
              Height = 25
              Action = aDodadiPotrebenDok
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 1
              OnKeyDown = EnterKakoTab
            end
            object cxButtonIzvadiDokument: TcxButton
              Left = 127
              Top = 500
              Width = 105
              Height = 25
              Action = aIzvadiPotrebenDokument
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 2
              OnKeyDown = EnterKakoTab
            end
          end
        end
      end
    end
  end
  object PanelVnesOsnovniPodatoci: TPanel
    Left = 264
    Top = 309
    Width = 676
    Height = 308
    Anchors = []
    BevelInner = bvLowered
    BevelOuter = bvSpace
    BiDiMode = bdLeftToRight
    BorderWidth = 1
    Color = 11302478
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBiDiMode = False
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    Visible = False
    DesignSize = (
      676
      308)
    object cxGroupBox2: TcxGroupBox
      Left = 8
      Top = 10
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1042#1085#1077#1089#1080' '#1075#1080' '#1086#1089#1085#1086#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ParentFont = False
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.Shadow = False
      Style.TransparentBorder = False
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        646
        278)
      Height = 278
      Width = 646
      object Label51: TLabel
        Left = 68
        Top = 32
        Width = 50
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1043#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label68: TLabel
        Left = 37
        Top = 59
        Width = 81
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label69: TLabel
        Left = 20
        Top = 135
        Width = 98
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label70: TLabel
        Left = 20
        Top = 162
        Width = 98
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label71: TLabel
        Left = 15
        Top = 202
        Width = 103
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object GODINA_1: TcxDBComboBox
        Tag = 1
        Left = 124
        Top = 29
        DataBinding.DataField = 'GODINA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          '2010'
          '2011'
          '2012'
          '2013'
          '2014'
          '2015'
          '2016'
          '2017'
          '2018'
          '2019'
          '2020'
          '2021'
          '2022'
          '2023'
          '2024'
          '2025'
          '2026'
          '2027'
          '2028'
          '2029'
          '2030'
          '2031'
          '2032'
          '2033'
          '2034'
          '2035'
          '2036'
          '2037'
          '2038'
          '2039'
          '2040'
          '2041'
          '2042'
          '2043'
          '2044'
          '2045'
          '2046'
          '2047'
          '2048'
          '2049'
          '2050')
        Properties.OnEditValueChanged = GODINA_1PropertiesEditValueChanged
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 156
      end
      object BROJ_1: TcxDBTextEdit
        Left = 124
        Top = 56
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.Shadow = False
        Style.IsFontAssigned = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 156
      end
      object GRUPA_1: TcxDBTextEdit
        Tag = 1
        Left = 124
        Top = 132
        Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        BeepOnEnter = False
        DataBinding.DataField = 'GRUPA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 29
      end
      object GRUPA_SIFRA_1: TcxDBTextEdit
        Tag = 1
        Left = 152
        Top = 132
        Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'GRUPA_SIFRA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 40
      end
      object GRUPA_OPIS_1: TcxDBLookupComboBox
        Tag = 1
        Left = 191
        Top = 132
        Hint = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1086#1087#1080#1089
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'GRUPA_SIFRA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'SIFRA'
        Properties.ListColumns = <
          item
            Width = 120
            FieldName = 'VidNaziv'
          end
          item
            Width = 120
            FieldName = 'SIFRA'
          end
          item
            Width = 800
            FieldName = 'OPIS'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListSource = dm.dsVidoviDogovori
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 434
      end
      object cxDBRadioGroupGRUPA_1: TcxDBRadioGroup
        Left = 124
        Top = 83
        TabStop = False
        Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        DataBinding.DataField = 'GRUPA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.Columns = 3
        Properties.ImmediatePost = True
        Properties.Items = <
          item
            Caption = #1057#1090#1086#1082#1080
            Value = 1
          end
          item
            Caption = #1059#1089#1083#1091#1075#1080
            Value = 2
          end
          item
            Caption = #1056#1072#1073#1086#1090#1080
            Value = 3
          end>
        Properties.OnEditValueChanged = cxDBRadioGroupVIDPropertiesEditValueChanged
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TransparentBorder = True
        Style.IsFontAssigned = True
        TabOrder = 2
        OnExit = cxDBRadioGroupGRUPA_1Exit
        Height = 43
        Width = 501
      end
      object PREDMET_NABAVKA_1: TcxDBMemo
        Tag = 1
        Left = 124
        Top = 159
        Hint = 
          #1055#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' ' +
          #1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'PREDMET_NABAVKA'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.WantReturns = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        OnDblClick = PREDMET_NABAVKA_1DblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 34
        Width = 501
      end
      object TIP_TENDER_1: TcxDBTextEdit
        Tag = 1
        Left = 124
        Top = 199
        Hint = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1064#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'TIP_TENDER'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 68
      end
      object TIP_TENDER_NAZIV_1: TcxDBLookupComboBox
        Tag = 1
        Left = 192
        Top = 199
        Hint = #1042#1080#1076' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1053#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'TIP_TENDER'
        DataBinding.DataSource = dm.dsТender
        ParentFont = False
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'SIFRA'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'SIFRA'
          end
          item
            Width = 600
            FieldName = 'NAZIV'
          end
          item
            Width = 120
            FieldName = 'DENOVI_ZALBA'
          end
          item
            Width = 300
            FieldName = 'tip_god_plan_Naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsTipNabavkaAktivni
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 433
      end
      object ButtonProdolzi_1: TcxButton
        Left = 453
        Top = 238
        Width = 89
        Height = 25
        Hint = 
          #1042#1085#1077#1089#1080' '#1075#1080' '#1086#1089#1085#1086#1074#1085#1080#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1080' '#1087#1088#1086#1076#1086#1083#1078#1080' '#1089#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1086#1089#1090#1072#1085#1072#1090#1080#1090 +
          #1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1112#1072#1074#1085#1072#1090#1072' '#1085#1072#1073#1072#1074#1082#1072
        Action = aOsnovniProdolzi
        Anchors = [akTop, akRight]
        Colors.Pressed = clGradientActiveCaption
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object ButtonOtkazi1: TcxButton
        Left = 548
        Top = 234
        Width = 75
        Height = 25
        Hint = #1054#1090#1082#1072#1078#1080' '#1079#1072#1087#1080#1089' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        Action = aOtkazi
        Anchors = [akTop, akRight]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 10
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object PanelVnesiKomisija: TPanel
    Left = 592
    Top = 395
    Width = 587
    Height = 166
    Anchors = [akLeft, akTop, akRight]
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 4
    Visible = False
    DesignSize = (
      587
      166)
    object Label67: TLabel
      Left = 20
      Top = 143
      Width = 147
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Enter - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitTop = 239
    end
    object cxGroupBox3: TcxGroupBox
      Left = 14
      Top = 14
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1048#1079#1073#1077#1088#1080' '#1095#1083#1077#1085' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        559
        121)
      Height = 121
      Width = 559
      object Label72: TLabel
        Left = 16
        Top = 29
        Width = 74
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label73: TLabel
        Left = 48
        Top = 56
        Width = 42
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1063#1083#1077#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label74: TLabel
        Left = 28
        Top = 83
        Width = 62
        Height = 15
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1060#1091#1085#1082#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object CLEN: TcxDBLookupComboBox
        Tag = 1
        Left = 96
        Top = 53
        Hint = #1050#1083#1080#1082#1085#1077#1090#1077' Insert '#1079#1072' '#1074#1085#1077#1089' '#1085#1072' '#1085#1086#1074' '#1095#1083#1077#1085' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'CLEN'
        DataBinding.DataSource = dm.dsTenderKomisija
        ParentFont = False
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'ID'
          end
          item
            Width = 400
            FieldName = 'IME_PREZIME'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dm.dsKomisija
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 447
      end
      object REDBR: TcxDBTextEdit
        Left = 96
        Top = 26
        DataBinding.DataField = 'REDBR'
        DataBinding.DataSource = dm.dsTenderKomisija
        Style.TextStyle = []
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object FUNKCIJA: TcxDBComboBox
        Left = 96
        Top = 80
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'FUNKCIJA'
        DataBinding.DataSource = dm.dsTenderKomisija
        ParentFont = False
        Properties.Items.Strings = (
          #1055#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083
          #1063#1083#1077#1085
          #1047#1072#1084#1077#1085#1080#1082' '#1087#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083
          #1047#1072#1084#1077#1085#1080#1082' '#1095#1083#1077#1085
          #1057#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082)
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 447
      end
    end
  end
  object PanelMaxPoeni: TPanel
    Left = 525
    Top = 295
    Width = 193
    Height = 108
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 5
    Visible = False
    DesignSize = (
      193
      108)
    object Label64: TLabel
      Left = 20
      Top = 87
      Width = 147
      Height = 13
      Caption = 'Enter - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight]
      Caption = #1052#1072#1082#1089#1080#1084#1091#1084' '#1073#1086#1076#1086#1074#1080
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      Height = 65
      Width = 161
      object MAX_BODOVI: TcxDBTextEdit
        Tag = 1
        Left = 26
        Top = 26
        BeepOnEnter = False
        DataBinding.DataField = 'MAX_BODOVI'
        DataBinding.DataSource = dm.dsTenderGenBodiranje
        ParentFont = False
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clGrayText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 111
      end
    end
  end
  object DateTimePicker1: TDateTimePicker
    Left = 776
    Top = 512
    Width = 1
    Height = 21
    Date = 41086.466728877310000000
    Time = 41086.466728877310000000
    TabOrder = 6
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 32
    Top = 576
  end
  object PopupMenu1: TPopupMenu
    Left = 176
    Top = 592
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 144
    Top = 504
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 173
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 1127
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1173
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 990
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 119
          Visible = True
          ItemName = 'cbStatus'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 460
      DockedTop = 0
      FloatLeft = 1155
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1055#1086#1089#1090#1072#1087#1082#1072' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1076#1086#1075#1086#1074#1086#1088
      CaptionButtons = <>
      DockedLeft = 657
      DockedTop = 0
      FloatLeft = 1292
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      CaptionButtons = <>
      DockedLeft = 912
      DockedTop = 0
      FloatLeft = 1292
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080' '
      CaptionButtons = <>
      DockedLeft = 990
      DockedTop = 0
      FloatLeft = 1292
      FloatTop = 7
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object cbGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      Items.Strings = (
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017')
      ItemIndex = -1
    end
    object cbStatus: TdxBarCombo
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      OnChange = cbStatusChange
      Items.Strings = (
        #1042#1086' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
        #1054#1090#1074#1086#1088#1077#1085
        #1047#1072#1090#1074#1086#1088#1077#1085
        #1044#1077#1083#1091#1084#1085#1086' '#1079#1072#1074#1088#1096#1077#1085)
      ItemIndex = -1
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aStavkiVoJavnaNabavka
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 60
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPonudi
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aDobitnici
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aDogovori
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarButton33'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarButton36'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end
        item
          Visible = True
          ItemName = 'dxBarButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarButton27'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton28'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = aPSpecifikacijaJavnaNabavka
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aPSpecifikacijaPoSektori
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aPSpecifikacijaPoArtikli
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBaranjaIsporaka
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aDokumenti
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aOdlukaZaNabavka
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aOglasJN
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Action = aPZapisnikOtvoranjePonudi
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Action = aPOdlukaIzborNajpovolnaPonuda
      Category = 0
    end
    object dxBarButton9: TdxBarButton
      Action = aPOdlukaZaPonistuvanjeNaStavki
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = aPRealizacijaJN
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = aPRealizacijaPlanNabavkaPoRE
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aRealizacijaPlanNabavkaRe
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = aPOdlukaZaGodisenPlan
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = aPIzvestajSprovedenaPostapka
      Category = 0
    end
    object dxBarButton14: TdxBarButton
      Action = aPPocetnaRangListaZaUcestvoNaEAukcija
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton16: TdxBarButton
      Action = aPOdlukaIzborNajpovolnaPonudaCena
      Category = 0
    end
    object dxBarButton17: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton18: TdxBarButton
      Action = actPOdlukaZaPonistuvanjePostapka
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = actPrevzemiPonisteniStavki
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aPIznosNaDogovorPoPartnerGddini
      Category = 0
    end
    object dxBarButton19: TdxBarButton
      Action = aPPocetnaRangListaPoKriteriumi
      Category = 0
    end
    object dxBarButton20: TdxBarButton
      Action = aPNedobitniStavki
      Category = 0
    end
    object dxBarButton21: TdxBarButton
      Action = aPPokanaKonecnaCena
      Category = 0
    end
    object dxBarButton22: TdxBarButton
      Action = aPDobitniStavki
      Category = 0
    end
    object dxBarButton23: TdxBarButton
      Action = aPTehnickaSpecifikacija
      Category = 0
    end
    object dxBarButton24: TdxBarButton
      Action = aPOdlukaJN_SO_CENA
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1089#1086' '#1080#1079#1085#1086#1089')'
      Category = 0
    end
    object dxBarButton25: TdxBarButton
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      Category = 0
      Hint = #1058#1077#1093#1085#1080#1095#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      Visible = ivAlways
      ImageIndex = 19
      OnClick = dxBarButton25Click
    end
    object dxBarButton26: TdxBarButton
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      Category = 0
      Hint = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      Visible = ivAlways
      ImageIndex = 19
      OnClick = dxBarButton26Click
    end
    object dxBarSubItem3: TdxBarSubItem
      Action = actPSporedbaOdlukaJN_OdlukaIzborNajpovolen
      Category = 0
      ItemLinks = <>
    end
    object dxBarButton27: TdxBarButton
      Action = actPSporedbaOdlukaJN_OdlukaIzborNajpovolen
      Category = 0
    end
    object dxBarButton28: TdxBarButton
      Action = actPSpecifikacijaPoSektoriSoPlan
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 14
      LargeImageIndex = 14
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton29'
        end
        item
          Visible = True
          ItemName = 'dxBarButton31'
        end
        item
          Visible = True
          ItemName = 'dxBarButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarButton32'
        end>
    end
    object dxBarButton29: TdxBarButton
      Action = act_S_VoPodgotovka
      Category = 0
    end
    object dxBarButton30: TdxBarButton
      Action = act_S_Zatvoren
      Category = 0
    end
    object dxBarButton31: TdxBarButton
      Action = act_S_Otvoren
      Category = 0
    end
    object dxBarButton32: TdxBarButton
      Action = act_S_DelumnoZavrsen
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = actGodisenPlanJN
      Category = 0
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSeparator1: TdxBarSeparator
      Caption = 'New Separator'
      Category = 0
      Hint = 'New Separator'
      Visible = ivAlways
    end
    object dxBarButton33: TdxBarButton
      Action = actExportToExcelOdlukaNajpovolna
      Category = 0
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089' '#1079#1072' '#1087#1086#1085#1091#1076#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 14
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton34'
        end
        item
          Visible = True
          ItemName = 'dxBarButton35'
        end>
    end
    object dxBarButton34: TdxBarButton
      Caption = #1054#1090#1074#1086#1088#1077#1085
      Category = 0
      Hint = #1054#1090#1074#1086#1088#1077#1085
      Visible = ivAlways
      OnClick = dxBarButton34Click
    end
    object dxBarSubItem7: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton35: TdxBarButton
      Caption = #1047#1072#1090#1074#1086#1088#1077#1085
      Category = 0
      Hint = #1047#1072#1090#1074#1086#1088#1077#1085
      Visible = ivAlways
      OnClick = dxBarButton35Click
    end
    object dxBarButton36: TdxBarButton
      Action = actPNevoobicaenaNiskaCena
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 584
    Top = 512
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aDodadiKriterium: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 50
      OnExecute = aDodadiKriteriumExecute
    end
    object aIzvadiStar: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 51
      OnExecute = aIzvadiStarExecute
    end
    object aZapisiKriteriumPanel: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1082#1088#1080#1090#1077#1088#1080#1091#1084
      OnExecute = aZapisiKriteriumPanelExecute
    end
    object aStavkiVoJavnaNabavka: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076'/'#1045#1074#1080#1076#1077#1085#1094#1080#1112#1072
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aStavkiVoJavnaNabavkaExecute
    end
    object aOsnovniProdolzi: TAction
      Caption = #1055#1088#1086#1076#1086#1083#1078#1080
      ImageIndex = 47
      ShortCut = 121
      OnExecute = aOsnovniProdolziExecute
    end
    object aDodadiKomisija: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 50
      OnExecute = aDodadiKomisijaExecute
    end
    object aIzvadiKomisija: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 51
      OnExecute = aIzvadiKomisijaExecute
    end
    object aZapisiKomisija: TAction
      Caption = 'aZapisiKomisija'
      OnExecute = aZapisiKomisijaExecute
    end
    object aPonudi: TAction
      Caption = #1055#1086#1085#1091#1076#1080
      ImageIndex = 1
      OnExecute = aPonudiExecute
    end
    object aDobitnici: TAction
      Caption = #1044#1086#1073#1080#1090#1085#1080#1094#1080
      ImageIndex = 58
      OnExecute = aDobitniciExecute
    end
    object aDogovori: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088#1080
      ImageIndex = 8
      OnExecute = aDogovoriExecute
    end
    object aPSpecifikacijaJavnaNabavka: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aPSpecifikacijaJavnaNabavkaExecute
    end
    object aDizajnSpecifikacijaJavnaNabavka: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      OnExecute = aDizajnSpecifikacijaJavnaNabavkaExecute
    end
    object aPopUpMenu: TAction
      Caption = 'aPopUpMenu'
      ShortCut = 24697
      OnExecute = aPopUpMenuExecute
    end
    object aPSpecifikacijaPoSektori: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
      ImageIndex = 19
      OnExecute = aPSpecifikacijaPoSektoriExecute
    end
    object aDizajnSpecifikacijaPoSektori: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
      OnExecute = aDizajnSpecifikacijaPoSektoriExecute
    end
    object aPSpecifikacijaPoArtikli: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
      ImageIndex = 19
      OnExecute = aPSpecifikacijaPoArtikliExecute
    end
    object aDizajnSpecifikacijaPoArtikli: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
      OnExecute = aDizajnSpecifikacijaPoArtikliExecute
    end
    object aBaranjaIsporaka: TAction
      Caption = #1041#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 16
      OnExecute = aBaranjaIsporakaExecute
    end
    object aDokumenti: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076'/'#1045#1074#1080#1076#1077#1085#1094#1080#1112#1072
      ImageIndex = 60
      OnExecute = aDokumentiExecute
    end
    object aDodadiPotrebenDok: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 50
      OnExecute = aDodadiPotrebenDokExecute
    end
    object aIzvadiPotrebenDokument: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 51
      OnExecute = aIzvadiPotrebenDokumentExecute
    end
    object aOdlukaZaNabavka: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aOdlukaZaNabavkaExecute
    end
    object aDOdlukaZaNabavka: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aDOdlukaZaNabavkaExecute
    end
    object aOglasJN: TAction
      Caption = #1054#1075#1083#1072#1089' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aOglasJNExecute
    end
    object aDOglasJN: TAction
      Caption = #1054#1075#1083#1072#1089' '#1079#1072' '#1076#1086#1076#1077#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aDOglasJNExecute
    end
    object aPZapisnikOtvoranjePonudi: TAction
      Caption = #1047#1072#1087#1080#1089#1085#1080#1082' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077#1090#1086' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077
      ImageIndex = 19
      OnExecute = aPZapisnikOtvoranjePonudiExecute
    end
    object aDZapisnikOtvoranjePonudi: TAction
      Caption = #1047#1072#1087#1080#1089#1085#1080#1082' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077#1090#1086' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077
      ImageIndex = 19
      OnExecute = aDZapisnikOtvoranjePonudiExecute
    end
    object aPOdlukaIzborNajpovolnaPonuda: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1082#1086#1083#1080#1095#1080#1085#1072') '
      ImageIndex = 19
      OnExecute = aPOdlukaIzborNajpovolnaPonudaExecute
    end
    object aDOdlukaIzborNajpovolnaPonuda: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1082#1086#1083#1080#1095#1080#1085#1072') '
      ImageIndex = 19
      OnExecute = aDOdlukaIzborNajpovolnaPonudaExecute
    end
    object aPOdlukaZaPonistuvanjeNaStavki: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1077#1083' '#1086#1076' '#1089#1090#1072#1074#1082#1080#1090#1077
      ImageIndex = 19
      OnExecute = aPOdlukaZaPonistuvanjeNaStavkiExecute
    end
    object aDOdlukaZaPonistuvanjeNaStavki: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1077#1083' '#1086#1076' '#1089#1090#1072#1074#1082#1080#1090#1077
      ImageIndex = 19
      OnExecute = aDOdlukaZaPonistuvanjeNaStavkiExecute
    end
    object aPRealizacijaJN: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' j'#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aPRealizacijaJNExecute
    end
    object aDRealizacijaJN: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' j'#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 19
      OnExecute = aDRealizacijaJNExecute
    end
    object aPRealizacijaPlanNabavkaPoRE: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1087#1083#1072#1085' '#1080' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      ImageIndex = 19
      OnExecute = aPRealizacijaPlanNabavkaPoREExecute
    end
    object aDRealizacijaPlanNabavkaPoRE: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '#1085#1072' '#1087#1083#1072#1085' '#1080' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      ImageIndex = 19
      OnExecute = aDRealizacijaPlanNabavkaPoREExecute
    end
    object aRealizacijaPlanNabavkaRe: TAction
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072' '
      ImageIndex = 5
      OnExecute = aRealizacijaPlanNabavkaReExecute
    end
    object aPOdlukaZaGodisenPlan: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aPOdlukaZaGodisenPlanExecute
    end
    object aDOdlukaZaGodisenPlan: TAction
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = aDOdlukaZaGodisenPlanExecute
    end
    object aPIzvestajSprovedenaPostapka: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      ImageIndex = 19
      OnExecute = aPIzvestajSprovedenaPostapkaExecute
    end
    object aDIzvestajSprovedenaPostapka: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1077#1082#1086#1085#1086#1084#1089#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072'  '
      ImageIndex = 19
      OnExecute = aDIzvestajSprovedenaPostapkaExecute
    end
    object aPPocetnaRangListaZaUcestvoNaEAukcija: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072
      ImageIndex = 19
      OnExecute = aPPocetnaRangListaZaUcestvoNaEAukcijaExecute
    end
    object aDPocetnaRangListaZaUcestvoNaEAukcija: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072' - '#1077#1082#1086#1085#1086#1084#1089#1082#1080' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = aDPocetnaRangListaZaUcestvoNaEAukcijaExecute
    end
    object aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCena: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072' - '#1085#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
      ImageIndex = 19
      OnExecute = aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCenaExecute
    end
    object aPOdlukaIzborNajpovolnaPonudaCena: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1094#1077#1085#1072') '
      ImageIndex = 19
      OnExecute = aPOdlukaIzborNajpovolnaPonudaCenaExecute
    end
    object aDOdlukaIzborNajpovolnaPonudaCena: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1094#1077#1085#1072') '
      ImageIndex = 19
      OnExecute = aDOdlukaIzborNajpovolnaPonudaCenaExecute
    end
    object aDIzvestajSprovedenaPostapkaNajniskaCena: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1085#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
    end
    object actPOdlukaZaPonistuvanjePostapka: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      ImageIndex = 19
      OnExecute = actPOdlukaZaPonistuvanjePostapkaExecute
    end
    object actDOdlukaZaPonistuvanjePostapka: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      OnExecute = actDOdlukaZaPonistuvanjePostapkaExecute
    end
    object actPrevzemiPonisteniStavki: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1087#1086#1085#1080#1096#1090#1077#1085#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 93
      OnExecute = actPrevzemiPonisteniStavkiExecute
    end
    object aPIznosNaDogovorPoPartnerGddini: TAction
      Caption = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1087#1086' '#1075#1086#1076#1080#1085#1080' '#1080' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 19
      OnExecute = aPIznosNaDogovorPoPartnerGddiniExecute
    end
    object aDIznosNaDogovorPoPartnerGddini: TAction
      Caption = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1087#1086' '#1075#1086#1076#1080#1085#1080' '#1080' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 19
      OnExecute = aDIznosNaDogovorPoPartnerGddiniExecute
    end
    object aPPocetnaRangListaPoKriteriumi: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072' '#1087#1086' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
      ImageIndex = 19
      OnExecute = aPPocetnaRangListaPoKriteriumiExecute
    end
    object aDPocetnaRangListaPoKriteriumi: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072' '#1087#1086' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
      ImageIndex = 19
      OnExecute = aDPocetnaRangListaPoKriteriumiExecute
    end
    object aDSpecifikacijaJavnaNabavkaUslugi: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1059#1089#1083#1091#1075#1080
      OnExecute = aDSpecifikacijaJavnaNabavkaUslugiExecute
    end
    object aPNedobitniStavki: TAction
      Caption = #1053#1077#1076#1086#1073#1080#1090#1085#1080' '#1089#1090#1072#1074#1082#1080' - '#1054#1090#1092#1088#1083#1077#1085#1072' '#1087#1086#1085#1091#1076#1072'/'#1053#1077#1084#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = aPNedobitniStavkiExecute
    end
    object aDNedobitniStavki: TAction
      Caption = #1053#1077#1076#1086#1073#1080#1090#1085#1080' '#1089#1090#1072#1074#1082#1080' - '#1053#1077#1087#1088#1080#1092#1072#1090#1077#1085#1080'/'#1041#1077#1079' '#1087#1086#1085#1091#1076#1072
      OnExecute = aDNedobitniStavkiExecute
    end
    object aPPokanaKonecnaCena: TAction
      Caption = #1055#1086#1082#1072#1085#1072' '#1079#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1082#1086#1085#1077#1095#1085#1072' '#1094#1077#1085#1072
      ImageIndex = 19
      OnExecute = aPPokanaKonecnaCenaExecute
    end
    object aDPokanaKonecnaCena: TAction
      Caption = #1055#1086#1082#1072#1085#1072' '#1079#1072' '#1087#1086#1076#1085#1077#1089#1091#1074#1072#1114#1077' '#1082#1086#1085#1077#1095#1085#1072' '#1094#1077#1085#1072
      OnExecute = aDPokanaKonecnaCenaExecute
    end
    object aPDobitniStavki: TAction
      Caption = #1044#1086#1073#1080#1090#1085#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 19
      OnExecute = aPDobitniStavkiExecute
    end
    object aDDobitniStavki: TAction
      Caption = #1044#1086#1073#1080#1090#1085#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 19
      OnExecute = aDDobitniStavkiExecute
    end
    object aPTehnickaSpecifikacija: TAction
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072
      ImageIndex = 19
      OnExecute = aPTehnickaSpecifikacijaExecute
    end
    object aDTehnickaSpecifikacija: TAction
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072
      OnExecute = aDTehnickaSpecifikacijaExecute
    end
    object aPOdlukaJN_SO_CENA: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1089#1086' '#1094#1077#1085#1072')'
      ImageIndex = 19
      OnExecute = aPOdlukaJN_SO_CENAExecute
    end
    object aDOdlukaJN_SO_CENA: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1089#1086' '#1080#1079#1085#1086#1089')'
      OnExecute = aDOdlukaJN_SO_CENAExecute
    end
    object actPSporedbaOdlukaJN_OdlukaIzborNajpovolen: TAction
      Caption = 
        #1057#1087#1086#1088#1077#1076#1073#1072' '#1085#1072' '#1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1080' '#1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086 +
        #1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = actPSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute
    end
    object actDSporedbaOdlukaJN_OdlukaIzborNajpovolen: TAction
      Caption = 
        #1057#1087#1086#1088#1077#1076#1073#1072' '#1085#1072' '#1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1080' '#1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086 +
        #1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      OnExecute = actDSporedbaOdlukaJN_OdlukaIzborNajpovolenExecute
    end
    object actPSpecifikacijaPoSektoriSoPlan: TAction
      Caption = 
        #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080' - '#1087#1086#1074#1088#1079#1072#1085#1086' '#1089 +
        #1086' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 19
      OnExecute = actPSpecifikacijaPoSektoriSoPlanExecute
    end
    object actDSpecifikacijaPoSektoriSoPlan: TAction
      Caption = 
        #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080' - '#1087#1086#1074#1088#1079#1072#1085#1086' '#1089 +
        #1086' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      OnExecute = actDSpecifikacijaPoSektoriSoPlanExecute
    end
    object act_S_VoPodgotovka: TAction
      Caption = #1042#1086' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
      OnExecute = act_S_VoPodgotovkaExecute
    end
    object act_S_Otvoren: TAction
      Caption = #1054#1090#1074#1086#1088#1077#1085
      OnExecute = act_S_OtvorenExecute
    end
    object act_S_Zatvoren: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1077#1085
      OnExecute = act_S_ZatvorenExecute
    end
    object act_S_DelumnoZavrsen: TAction
      Caption = #1044#1077#1083#1091#1084#1085#1086' '#1079#1072#1074#1088#1096#1077#1085
      OnExecute = act_S_DelumnoZavrsenExecute
    end
    object actGodisenPlanJN: TAction
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      ImageIndex = 15
      OnExecute = actGodisenPlanJNExecute
    end
    object actExportToExcelOdlukaNajpovolna: TAction
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072' ('#1094#1077#1085#1072') - Export to Excel '
      ImageIndex = 31
      OnExecute = actExportToExcelOdlukaNajpovolnaExecute
    end
    object actPNevoobicaenaNiskaCena: TAction
      Caption = #1053#1077#1074#1086#1086#1073#1080#1095#1072#1077#1085#1086' '#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
      ImageIndex = 19
      OnExecute = actPNevoobicaenaNiskaCenaExecute
    end
    object actDNevoobicaenaNiskaCena: TAction
      Caption = 'actDNevoobicaenaNiskaCena'
      OnExecute = actDNevoobicaenaNiskaCenaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 48
    Top = 632
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 14000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44455.547970104170000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 104
    Top = 576
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 144
  end
  object qRedBrClenKomisija: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  coalesce(max(k.redbr),0) +1 as redBr'
      'from jn_tender_komisija k'
      'where k.broj_tender = :broj_tender')
    Left = 992
    Top = 632
  end
  object PopupMenu2: TPopupMenu
    Left = 872
    Top = 624
    object N10: TMenuItem
      Action = aDOdlukaZaGodisenPlan
    end
    object N5: TMenuItem
      Action = aDOdlukaZaNabavka
    end
    object N6: TMenuItem
      Action = aDOglasJN
    end
    object N7: TMenuItem
      Action = aDZapisnikOtvoranjePonudi
    end
    object aDOdlukaIzborNajpovolnaPonuda1: TMenuItem
      Action = aDOdlukaIzborNajpovolnaPonuda
    end
    object N14: TMenuItem
      Action = aDOdlukaIzborNajpovolnaPonudaCena
    end
    object N16: TMenuItem
      Action = actDOdlukaZaPonistuvanjePostapka
    end
    object N8: TMenuItem
      Action = aDOdlukaZaPonistuvanjeNaStavki
    end
    object N19: TMenuItem
      Action = aDSpecifikacijaJavnaNabavkaUslugi
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1059#1089#1083#1091#1075#1080
    end
    object N23: TMenuItem
      Caption = 
        #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' - '#1059#1089#1083#1091#1075#1080' ('#1073#1077#1079' '#1092#1091#1090#1077 +
        #1088')'
      OnClick = N23Click
    end
    object N2: TMenuItem
      Action = aDizajnSpecifikacijaJavnaNabavka
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
    end
    object N24: TMenuItem
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      OnClick = N24Click
    end
    object N3: TMenuItem
      Action = aDizajnSpecifikacijaPoSektori
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086#1076#1088#1077#1076#1077#1085#1072' '#1087#1086' '#1089#1077#1082#1090#1086#1088#1080
    end
    object N4: TMenuItem
      Action = aDizajnSpecifikacijaPoArtikli
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1083#1080
    end
    object j1: TMenuItem
      Action = aDRealizacijaJN
    end
    object N9: TMenuItem
      Action = aDRealizacijaPlanNabavkaPoRE
    end
    object N11: TMenuItem
      Action = aDIzvestajSprovedenaPostapka
    end
    object N12: TMenuItem
      Action = aDPocetnaRangListaZaUcestvoNaEAukcija
    end
    object N13: TMenuItem
      Action = aDPocetnaRangListaZaUcestvoNaEAukcijaNajniskaCena
    end
    object N15: TMenuItem
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' - '#1085#1072#1112#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
      OnClick = N15Click
    end
    object N17: TMenuItem
      Action = aDIznosNaDogovorPoPartnerGddini
    end
    object N18: TMenuItem
      Action = aDPocetnaRangListaPoKriteriumi
    end
    object aDNedobitniStavki1: TMenuItem
      Action = aDNedobitniStavki
    end
    object Action21: TMenuItem
      Action = aDDobitniStavki
    end
    object N20: TMenuItem
      Action = aDPokanaKonecnaCena
    end
    object N21: TMenuItem
      Action = aDTehnickaSpecifikacija
    end
    object N25: TMenuItem
      Caption = #1058#1077#1093#1085#1080#1095#1082#1072' '#1089#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' ('#1073#1077#1079' '#1092#1091#1090#1077#1088')'
      OnClick = N25Click
    end
    object N22: TMenuItem
      Action = aDOdlukaJN_SO_CENA
    end
    object N26: TMenuItem
      Action = actDSporedbaOdlukaJN_OdlukaIzborNajpovolen
    end
    object actDSpecifikacijaPoSektoriSoBaranje1: TMenuItem
      Action = actDSpecifikacijaPoSektoriSoPlan
    end
    object actDNevoobicaenaNiskaCena1: TMenuItem
      Action = actDNevoobicaenaNiskaCena
      Caption = #1053#1077#1074#1086#1086#1073#1080#1095#1072#1077#1085#1086' '#1085#1080#1089#1082#1072' '#1094#1077#1085#1072
    end
  end
  object qPromenetiCeni: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct sr.broj_tender'
      'from jn_tender_stavki_re sr'
      'where sr.promena <>0')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 840
    Top = 504
    object qPromenetiCeniBROJ_TENDER: TFIBStringField
      FieldName = 'BROJ_TENDER'
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPromenetiCeni: TDataSource
    DataSet = qPromenetiCeni
    Left = 704
    Top = 512
  end
  object tblIzborNajpovolnaPonudaCena: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select d.tip_partner, d.partner, mp.naziv_skraten as skratenNazi' +
        'vPartner, mp.naziv as partnerNaziv,'
      '       d.artikal,'
      '       ma.jn_cpv,'
      '       ma.grupa,'
      '       ps.cenabezddv,'
      '       ps.kolicina,'
      '       ps.danok,'
      '       ps.cena,'
      '       ps.kolicina * ps.cena as iznos,'
      '       ps.kolicina * ps.cenabezddv as iznoosbezddv,'
      '       jts.naziv as artikalNaziv,'
      '       jts.merka,'
      '       ti.vremetraenje_dog,'
      '       tn.naziv as postapkanaziv,'
      '       tn.denovi_zalba,'
      
        '       ti.odluka_datum, ti.odluka_broj, ti.odl_nabavka_br, ti.od' +
        'l_nabavka_dat,tn.rok_dogovor,'
      '        case when (select  count(p.id)'
      '                  from jn_ponudi p'
      
        '                  where p.broj_tender = :tender) = 1 then '#39#1079#1072' '#1082#1086 +
        #1085#1077#1095#1085#1072' '#1094#1077#1085#1072#39
      '            else '#39#1086#1076' '#1090#1077#1082#1086#1090' '#1085#1072' '#1077#1083#1077#1082#1090#1088#1086#1085#1089#1082#1072#1090#1072' '#1072#1091#1082#1094#1080#1112#1072#39
      '       end tekst1,'
      '       t.interen_broj,'
      '       g.naziv as grupa_naziv,'
      '       v.naziv as vid_naziv,'
      '       (case when t.deliv=1 and  ng.grupa is null  then '#39#39' else'
      
        '        case when t.deliv=1 and  ng.grupa is not  null then '#39'('#1051#1086 +
        #1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39
      '        else '#39'('#1051#1086#1090#1086#1090' '#1085#1077' '#1077' '#1076#1077#1083#1080#1074')'#39' end end) deliva'
      'from jn_tender_dobitnici d'
      'inner join jn_tender t on t.broj = d.broj_tender'
      'inner join jn_tip_nabavka tn on tn.sifra = t.tip_tender'
      'inner join jn_tender_info ti on ti.broj = t.broj'
      
        'inner join mat_partner mp on mp.tip_partner = d.tip_partner and ' +
        'mp.id = d.partner'
      
        'inner join mtr_artikal ma on ma.id = d.artikal and ma.artvid = d' +
        '.vid_artikal'
      'inner join jn_tip_artikal v on v.id=ma.jn_tip_artikal'
      'left outer join mtr_artgrupa g on g.id=ma.grupa'
      
        'left outer join jn_nedelivi_grupi ng on ng.grupa=g.id   and ng.b' +
        'roj_tender=t.broj'
      
        'inner join jn_ponudi_stavki ps on ps.id = d.id_ponudi_stavki and' +
        ' ps.broj_tender = d.broj_tender'
      
        '                                  and ps.partner = d.partner and' +
        ' ps.tip_partner = d.tip_partner'
      
        ' join jn_tender_stavki jts on jts.vid_artikal=d.vid_artikal and ' +
        'jts.artikal=d.artikal    and  jts.broj_tender=d.broj_tender'
      'where d.broj_tender = :tender'
      
        'order by d.tip_partner, d.partner,ma.jn_tip_artikal,ma.grupa,ma.' +
        'artvid, ma.id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 808
    Top = 182
    object tblIzborNajpovolnaPonudaCenaSKRATENNAZIVPARTNER: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095' ('#1089#1082#1088#1072#1090#1077#1085' '#1085#1072#1079#1080#1074')'
      FieldName = 'SKRATENNAZIVPARTNER'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaPARTNERNAZIV: TFIBStringField
      DisplayLabel = #1055#1086#1085#1091#1076#1091#1074#1072#1095
      FieldName = 'PARTNERNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTIKAL'
    end
    object tblIzborNajpovolnaPonudaCenaJN_CPV: TFIBStringField
      DisplayLabel = #1047#1055#1032#1053' '#1064#1080#1092#1088#1072
      FieldName = 'JN_CPV'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaCENABEZDDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENABEZDDV'
    end
    object tblIzborNajpovolnaPonudaCenaKOLICINA: TFIBBCDField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      Size = 2
    end
    object tblIzborNajpovolnaPonudaCenaDANOK: TFIBFloatField
      DisplayLabel = #1044#1044#1042
      FieldName = 'DANOK'
    end
    object tblIzborNajpovolnaPonudaCenaCENA: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA'
    end
    object tblIzborNajpovolnaPonudaCenaIZNOS: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS'
    end
    object tblIzborNajpovolnaPonudaCenaIZNOOSBEZDDV: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOOSBEZDDV'
    end
    object tblIzborNajpovolnaPonudaCenaARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTIKALNAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaMERKA: TFIBStringField
      DisplayLabel = #1045#1076'. '#1052#1077#1088#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaGRUPA_NAZIV: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1072
      FieldName = 'GRUPA_NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaVID_NAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID_NAZIV'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzborNajpovolnaPonudaCenaDELIVA: TFIBStringField
      DisplayLabel = #1044#1077#1083#1080#1074#1072
      FieldName = 'DELIVA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzborNajpovolnaPonudaCena: TDataSource
    DataSet = tblIzborNajpovolnaPonudaCena
    Left = 1008
    Top = 238
  end
end
