unit SifPotrebniDokumenti;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxGroupBox, cxRadioGroup, cxMemo, cxBlobEdit, cxCheckBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxSkinOffice2013White, cxNavigator, System.Actions;

type
  TfrmPotrebniDokumenti = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1tipNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    cxDBRadioGroupTipDokument: TcxDBRadioGroup;
    NAZIV: TcxDBMemo;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    Panel1: TPanel;
    cxButton1: TcxButton;
    aPrevzemiGiSite: TAction;
    Label3: TLabel;
    Label71: TLabel;
    GRUPA: TcxDBTextEdit;
    GRUPA_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    procedure NAZIVDblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure aPrevzemiGiSiteExecute(Sender: TObject);
    procedure cxDBRadioGroupTipDokumentClick(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
    _sifra_kluc :array[1..10] of string;
  protected
     procedure prefrli;
     procedure prefrlipoveke;
  public
    { Public declarations }
    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
  end;

var
  frmPotrebniDokumenti: TfrmPotrebniDokumenti;

implementation

{$R *.dfm}

uses dmUnit, Notepad, dmProcedureQuey;




procedure TfrmPotrebniDokumenti.aAzurirajExecute(Sender: TObject);
begin
 // inherited;
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
    dm.tblSifPotrebniDokGrupi.ParamByName('tip').Value:=dm.tblPotrebniDokumnetiTIP.Value;
    dm.tblSifPotrebniDokGrupi.FullRefresh;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');


end;

procedure TfrmPotrebniDokumenti.aPrevzemiGiSiteExecute(Sender: TObject);
var i,flag:integer;
begin
  with cxGrid1DBTableView1.DataController do
 for I := 0 to cxGrid1DBTableView1.DataController.Controller.SelectedRecordCount -1 do  //izvrti gi site stiklirani - ne samo filtered
      begin
         flag:=cxGrid1DBTableView1.Controller.SelectedRecords[i].RecordIndex;
         dmPQ.insert6(dmPQ.pKopirajPotrebniDokVoTender, 'BROJ_TENDER','POTREBNI_DOKUMENTI_ID',Null, Null, Null,Null,dm.tblTenderBROJ.Value, GetValue(flag,cxGrid1DBTableView1ID.Index),Null,Null,Null,Null);
      end;
  dm.tblTenderPotrebniDok.FullRefresh;
  dm.tblPotrebniDokumneti.FullRefresh;
  close;
end;

procedure TfrmPotrebniDokumenti.cxDBRadioGroupTipDokumentClick(Sender: TObject);
begin
  inherited;
  dm.tblSifPotrebniDokGrupi.ParamByName('tip').Value:=dm.tblPotrebniDokumnetiTIP.Value;
  dm.tblSifPotrebniDokGrupi.FullRefresh;
end;

procedure TfrmPotrebniDokumenti.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
    dm.tblSifPotrebniDokGrupi.ParamByName('tip').Value:=dm.tblPotrebniDokumnetiTIP.Value;
    dm.tblSifPotrebniDokGrupi.FullRefresh;
end;

procedure TfrmPotrebniDokumenti.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    if(Ord(Key) = VK_RETURN) then
    prefrli;
end;

procedure TfrmPotrebniDokumenti.NAZIVDblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblPotrebniDokumnetiNAZIV.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblPotrebniDokumnetiNAZIV.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;

end;

procedure TfrmPotrebniDokumenti.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
procedure TfrmPotrebniDokumenti.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblSifPotrebniDokGrupi.Close;
  dm.tblSifPotrebniDokGrupi.ParamByName('tip').Value:='%';
  dm.tblSifPotrebniDokGrupi.Open;
  if tag = 0 then
     begin
        dm.tblPotrebniDokumneti.Close;
        dm.tblPotrebniDokumneti.ParamByName('param').Value:=0;
        dm.tblPotrebniDokumneti.Open;
     end;
  if tag = 1 then
     begin
        Panel1.Visible:=true;
     end;
end;

function TfrmPotrebniDokumenti.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmPotrebniDokumenti.prefrli;
begin
    ModalResult := mrOk;
    SetSifra(0,intTostr(dm.tblPotrebniDokumnetiID.Value));
//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra

end;
procedure TfrmPotrebniDokumenti.prefrlipoveke;
var i:integer;
begin
    //ModalResult := mrOk;
    
//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra

end;
end.
