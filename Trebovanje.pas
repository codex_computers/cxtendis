unit Trebovanje;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 04.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, cxPCdxBarPopupMenu, dxCustomHint, cxHint, cxRichEdit,
  cxDBRichEdit, cxBlobEdit, FIBDataSet, pFIBDataSet, cxLabel, cxDBLabel,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxTrackBar, dxdbtrel,
  cxSplitter, FIBQuery, pFIBQuery, cxImageComboBox,dmResources,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu,
  cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmTrebovanje = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1BarOdlukaNabavka: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabelaTrebovanja: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelTrebovanje: TPanel;
    PanelStavki: TPanel;
    cxPageControlTrebovanje: TcxPageControl;
    cxTabSheetTabelaren: TcxTabSheet;
    cxTabSheetDetalen: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cbGodina: TdxBarCombo;
    PanelDetalenPrikaz: TPanel;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    GODINA: TcxDBComboBox;
    DATUM: TcxDBDateEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxHintStyleController1: TcxHintStyleController;
    PanelTabelarenPrikaz: TPanel;
    Label6: TLabel;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarManager1Bar: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavki: TAction;
    aBrisiStavka: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    aZacuvajExcelStavki: TAction;
    aPecatiTabelaStavki: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxBarButton1: TdxBarButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    aPodesuvanjePecatenjeStavki: TAction;
    aPageSetupStavki: TAction;
    aSnimiPecatenjeStavki: TAction;
    aBrisiPodesuvanjePecatenjeStavki: TAction;
    aSnimiIzgledStavki: TAction;
    aBrisiIzgledStavki: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarSubItem2: TdxBarSubItem;
    cxBarEditItem1: TcxBarEditItem;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    aRefreshGodisenPlan: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    cxSplitter1: TcxSplitter;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RE_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1RE2: TcxGridDBColumn;
    cxGrid1DBTableView1RE2_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1StatusNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxDBRadioGroupStatus: TcxDBRadioGroup;
    Label5: TLabel;
    RE2: TcxDBTextEdit;
    RE2_NAZIV: TcxDBExtLookupComboBox;
    SIFRA: TcxDBTextEdit;
    Opis: TcxDBMemo;
    Panel1: TPanel;
    Label8: TLabel;
    cbPodsektori: TcxTextEdit;
    cbPodsektoriTree: TdxLookupTreeView;
    qCountRe: TpFIBQuery;
    cxPageControlStavki: TcxPageControl;
    cxTabSheetTabelarenStavki: TcxTabSheet;
    cxTabSheetDetalenStavki: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    PanelDetalenStavki: TPanel;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1TREBUVANJE_ID: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1JN_GRUPA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1OPIS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    SifraStavka: TcxDBTextEdit;
    Label1: TLabel;
    OpisStavka: TcxDBMemo;
    Label7: TLabel;
    KOLICINA: TcxDBTextEdit;
    Label9: TLabel;
    A_VID: TcxDBTextEdit;
    A_SIFRA: TcxDBTextEdit;
    ZapisiStavkaButton: TcxButton;
    OtkaziStavkaButton: TcxButton;
    A_NAZIV: TcxExtLookupComboBox;
    dxBarLargeButton29: TdxBarLargeButton;
    aAzurirajStavki: TAction;
    dxBarCombo1: TdxBarCombo;
    dxBarCombo2: TdxBarCombo;
    dxBarManager1Bar1: TdxBar;
    ComboGodina: TdxBarCombo;
    ComboStatus: TdxBarCombo;
    dxBarLargeButton30: TdxBarLargeButton;
    aPregledajPecatiTrebovanje: TAction;
    aDizajnTrebovanje: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaTrebovanjaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure PREDMETDblClick(Sender: TObject);
    procedure aDodadiStavkiExecute(Sender: TObject);
    procedure aBrisiStavkaExecute(Sender: TObject);
    procedure aRefreshGodisenPlanSektorExecute(Sender: TObject);
    procedure aZacuvajExcelStavkiExecute(Sender: TObject);
    procedure aPecatiTabelaStavkiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aPageSetupStavkiExecute(Sender: TObject);
    procedure aSnimiPecatenjeStavkiExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiExecute(Sender: TObject);
    procedure aBrisiIzgledStavkiExecute(Sender: TObject);
    procedure aKreirajPredlogPlanOtkaziExecute(Sender: TObject);
    procedure aRefreshGodisenPlanExecute(Sender: TObject);
    procedure aPGodisenPlanPoSektoriExecute(Sender: TObject);
    procedure cbPodsektoriTreeSetDisplayItemText(Sender: TObject;
      var DisplayText: string);
    procedure aPGodisenPlanOdJNExecute(Sender: TObject);
    procedure aDOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZapisiButtonClick(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure dxBarLargeButton2Click(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure cbPodsektoriPropertiesEditValueChanged(Sender: TObject);
    procedure cbPodsektoriTreeExit(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure A_NAZIVPropertiesCloseUp(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid2DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aAzurirajStavkiExecute(Sender: TObject);
    procedure ComboGodinaChange(Sender: TObject);
    procedure ComboStatusChange(Sender: TObject);
    procedure aPregledajPecatiTrebovanjeExecute(Sender: TObject);
    procedure aDizajnTrebovanjeExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    pom_godina, pom_status:Integer;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    procedure popolni_broj(godina, re: Integer);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmTrebovanje: TfrmTrebovanje;
  pom_godina :Integer;
  rData : TRepositoryData;
implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmUnit, Notepad,
  IzborStavkiGodisenPlan, dmProcedureQuey, DinamikaRealizacija, dmMaticni,
  Nurko, NurkoRepository;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmTrebovanje.ComboGodinaChange(Sender: TObject);
begin
      if pom_godina = 1 then
         begin
           dm.tblTrebovanje.Close;

           if ComboGodina.Text <> '' then dm.tblTrebovanje.ParamByName('godina').Value:=StrToInt(ComboGodina.Text)
           else  dm.tblTrebovanje.ParamByName('godina').Value:=dmKon.godina;

           if ComboStatus.Text = '��������' then
              dm.tblTrebovanje.ParamByName('status').Value:=0
           else if ComboStatus.Text = '�����������' then
              dm.tblTrebovanje.ParamByName('status').Value:=1
           else if ComboStatus.Text = '����������' then
              dm.tblTrebovanje.ParamByName('status').Value:=2
           else if ComboStatus.Text = '' then
              dm.tblTrebovanje.ParamByName('status').Value:='%';

           if cbPodsektoriTree.Text <> '' then dm.tblTrebovanje.ParamByName('re').Value:=cbPodsektori.EditValue
           else dm.tblTrebovanje.ParamByName('re').Value:='DTF';

           dm.tblTrebovanje.Open;
           dm.tblTrebovanjeStavki.Close;
           dm.tblTrebovanjeStavki.Open;
         end
      else pom_godina:=1;
end;

procedure TfrmTrebovanje.ComboStatusChange(Sender: TObject);
begin
      if pom_status = 1 then
         begin
           dm.tblTrebovanje.Close;

           if ComboGodina.Text <> '' then dm.tblTrebovanje.ParamByName('godina').Value:=StrToInt(ComboGodina.Text)
           else  dm.tblTrebovanje.ParamByName('godina').Value:=dmKon.godina;

           if ComboStatus.Text = '��������' then
              dm.tblTrebovanje.ParamByName('status').Value:=0
           else if ComboStatus.Text = '�����������' then
              dm.tblTrebovanje.ParamByName('status').Value:=1
           else if ComboStatus.Text = '����������' then
              dm.tblTrebovanje.ParamByName('status').Value:=2
           else if ComboStatus.Text = '' then
              dm.tblTrebovanje.ParamByName('status').Value:='%';

           if cbPodsektoriTree.Text <> '' then dm.tblTrebovanje.ParamByName('re').Value:=cbPodsektori.EditValue
           else dm.tblTrebovanje.ParamByName('re').Value:='DTF';

           dm.tblTrebovanje.Open;
           dm.tblTrebovanjeStavki.Close;
           dm.tblTrebovanjeStavki.Open;
         end
      else pom_status:=1;
end;

constructor TfrmTrebovanje.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmTrebovanje.aNovExecute(Sender: TObject);
begin
if cbPodsektori.Text <> '' then
begin
  if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse))then
  begin
    cxPageControlTrebovanje.ActivePage:=cxTabSheetDetalen;
    PanelDetalenPrikaz.Enabled:=true;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    RE2.SetFocus;
    if ComboGodina.Text  <> '' then
       dm.tblTrebovanjeGODINA.Value:=StrToInt(ComboGodina.Text)
    else
       dm.tblTrebovanjeGODINA.Value:=dmKon.godina;
    dm.tblTrebovanjeDATUM.Value:=Now;
    dm.tblTrebovanjeRE.Value:=cbPodsektori.EditValue;
    popolni_broj(dm.tblTrebovanjeGODINA.Value, dm.tblTrebovanjeRE.Value);
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end
else ShowMessage('�������� ������� ������� !!!');

end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmTrebovanje.popolni_broj(godina, re: Integer);
begin
     if ((re <> Null) and (godina <> Null)) then
        begin
          dm.qMAXBrojTrebovanje.close;
          dm.qMAXBrojTrebovanje.ParamByName('re').Value:=re;
          dm.qMAXBrojTrebovanje.ParamByName('godina').Value:=godina;
          dm.qMAXBrojTrebovanje.ExecQuery;
          dm.tblTrebovanjeBROJ.Value:=dm.qMAXBrojTrebovanje.FldByName['broj'].Value + 1;
        end;
end;

//	����� �� ������ �� ������������� �����
procedure TfrmTrebovanje.aAzurirajExecute(Sender: TObject);
begin
if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse))then
  begin
    cxPageControlTrebovanje.ActivePage:=cxTabSheetDetalen;
    PanelDetalenPrikaz.Enabled:=true;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
    RE2.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');

end;

procedure TfrmTrebovanje.aAzurirajStavkiExecute(Sender: TObject);
begin
if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse))then
  begin
      cxPageControlStavki.ActivePage:=cxTabSheetDetalenStavki;
      PanelDetalenStavki.Enabled:=true;
      cxGrid2DBTableView1.DataController.DataSet.Edit;
      A_VID.SetFocus;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');

end;

procedure TfrmTrebovanje.aBrisiExecute(Sender: TObject);
begin
  if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmTrebovanje.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmTrebovanje.aBrisiIzgledStavkiExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmTrebovanje.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmTrebovanje.aRefreshGodisenPlanExecute(Sender: TObject);
begin
    cxGrid2DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
    cxGrid2DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmTrebovanje.aRefreshGodisenPlanSektorExecute(Sender: TObject);
begin
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmTrebovanje.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmTrebovanje.aKreirajPredlogPlanOtkaziExecute(Sender: TObject);
begin

end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmTrebovanje.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmTrebovanje.aSnimiIzgledStavkiExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmTrebovanje.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '���������');
end;

procedure TfrmTrebovanje.aZacuvajExcelStavkiExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������ �� ���������');
end;

procedure TfrmTrebovanje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(PanelDetalenPrikaz) = false) then
      begin
          popolni_broj(dm.tblTrebovanjeGODINA.Value, dm.tblTrebovanjeRE.Value);
          cxGrid1DBTableView1.DataController.DataSet.Post;
          PanelDetalenPrikaz.Enabled:=False;
          cxPageControlTrebovanje.ActivePage:=cxTabSheetTabelaren;
          cxGrid1.SetFocus;
      end;
    end;
  end
  else if(cxGrid2DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    st := cxGrid2DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(PanelDetalenStavki) = false) then
      begin
          cxGrid2DBTableView1.DataController.DataSet.Post;
          PanelDetalenStavki.Enabled:=False;
          cxPageControlStavki.ActivePage:=cxTabSheetTabelarenStavki;
          cxGrid2.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmTrebovanje.A_NAZIVPropertiesCloseUp(Sender: TObject);
begin
if(cxGrid2DBTableView1.DataController.DataSource.State in [dsEdit,dsInsert])then
  begin
      if A_NAZIV.Text <> '' then
         begin
           dm.tblTrebovanjeStavkiVID_ARTIKAL.Value:=A_NAZIV.EditValue[0];
           dm.tblTrebovanjeStavkiARTIKAL.Value:=A_NAZIV.EditValue[1];
         end
      else
         begin
           A_VID.Clear;
           A_SIFRA.Clear;
         end;
  end;
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmTrebovanje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if (kom = RE2_NAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
              RE2_NAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);

            if RE2_NAZIV.EditValue <> Null then
               dm.tblTrebovanjeRE2.Value:=RE2_NAZIV.EditValue;

            frmNurkoRepository.Free;
       	  end
          else if (kom = A_NAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
               A_NAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            frmNurkoRepository.Free;
       	  end
	      end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmTrebovanje.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmTrebovanje.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
       if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if ((Sender as TWinControl)= A_VID) or ((Sender as TWinControl)= A_SIFRA) then
           begin
               ZemiImeDvoenKluc(A_VID,A_SIFRA, A_NAZIV);
           end
       end;
end;

procedure TfrmTrebovanje.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmTrebovanje.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmTrebovanje.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmTrebovanje.PREDMETDblClick(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmTrebovanje.prefrli;
begin
end;

procedure TfrmTrebovanje.FormClose(Sender: TObject; var Action: TCloseAction);
begin
       //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dmRes.FreeRepository(rData);
            Action := caFree;
        end
        else
          if (Validacija(PanelDetalenPrikaz) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dmRes.FreeRepository(rData);
            Action := caFree;
          end
          else Action := caNone;
    end
    else if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then dmRes.FreeRepository(rData);
end;

procedure TfrmTrebovanje.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();

  dm.tblPodsektoriGodisenPlan.Close;
  dm.tblPodsektoriGodisenPlan.ParamByName('roditel').Value:=dmKon.firma_id;
  dm.tblPodsektoriGodisenPlan.ParamByName('username').Value:=dmKon.user;
  dm.tblPodsektoriGodisenPlan.ParamByName('APP').Value:=dmKon.aplikacija;
  if prava_re = 1 then
     dm.tblPodsektoriGodisenPlan.ParamByName('param').Value:=1
  else
     dm.tblPodsektoriGodisenPlan.ParamByName('param').Value:=0;
  dm.tblPodsektoriGodisenPlan.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmTrebovanje.FormShow(Sender: TObject);
begin
  // ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    pom_godina:=0;
    pom_status:=0;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);

    sobrano := true;

    ComboGodina.Text:=IntToStr(dmKon.godina);
    ComboStatus.Text:='��������';
    cxPageControlTrebovanje.ActivePage:=cxTabSheetTabelaren;
    cxPageControlStavki.ActivePage:=cxTabSheetTabelarenStavki;
    dmMat.tblNurko.Open;
   	RE2_NAZIV.RepositoryItem := dmRes.InitRepository(-3, 'RE2_NAZIV', Name, rData);
    A_NAZIV.RepositoryItem := dmRes.InitRepository(40001, 'A_NAZIV', Name, rData);
  	procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);


    if not dm.tblPodsektoriGodisenPlan.IsEmpty then
       cbPodsektori.EditValue:=dmKon.firma_id;

    if cbPodsektori.Text <> '' then
       begin
        dm.tblTrebovanje.Close;
        dm.tblTrebovanje.ParamByName('re').Value:=cbPodsektori.EditValue;
        dm.tblTrebovanje.ParamByName('godina').Value:=dmKon.godina;
        dm.tblTrebovanje.ParamByName('status').Value:=0;
        dm.tblTrebovanje.Open;
       end;
     dm.tblTrebovanjeStavki.Close;
     dm.tblTrebovanjeStavki.Open;

     ZemiImeDvoenKluc(A_VID,A_SIFRA, A_NAZIV);

end;

//------------------------------------------------------------------------------

procedure TfrmTrebovanje.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmTrebovanje.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTrebovanje.cxGrid2DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     ZemiImeDvoenKluc(A_VID,A_SIFRA,A_NAZIV);
end;

procedure TfrmTrebovanje.dxBarLargeButton2Click(Sender: TObject);
begin
 
end;

//  ����� �� �����
procedure TfrmTrebovanje.cbGodinaChange(Sender: TObject);
begin
{if pom_godina = 1 then
   begin
        if cbGodina.Text  <> '' then
           dm.tblTrebovanjeGODINA.Value:=StrToInt(cbGodina.Text)
        else
           dm.tblTrebovanjeGODINA.Value:=dmKon.godina;
        dm.tblTrebovanje.FullRefresh;
   end
else pom_godina:=1;  }
end;

procedure TfrmTrebovanje.cbPodsektoriPropertiesEditValueChanged(
  Sender: TObject);
begin
    if cbPodsektori.Text <> '' then
     begin
     qCountRe.Close;
     qCountRe.ParamByName('roditel').Value:=dmKon.firma_id;
     qCountRe.ParamByName('username').Value:=dmKon.user;
     qCountRe.ParamByName('APP').Value:=dmKon.aplikacija;
     if dmKon.prava <> 0 then
        qCountRe.ParamByName('param').Value:=1
     else
        qCountRe.ParamByName('param').Value:=0;
     qCountRe.ParamByName('re').Value:=cbPodsektori.EditValue;
     qCountRe.ExecQuery;

     if qCountRe.FldByName['cRE'].Value = 1 then
        begin
           dm.tblPodsektoriGodisenPlan.Locate('RE',cbPodsektori.EditValue,[]);
           cbPodsektoriTree.Text:=dm.tblPodsektoriGodisenPlanNAZIV.Value;
        end
     else if qCountRe.FldByName['cRE'].Value = 0 then
        cbPodsektoriTree.Text:='';

     if cbPodsektoriTree.Text <> '' then
        begin
          dm.tblTrebovanje.Close;
          dm.tblTrebovanje.ParamByName('re').Value:=cbPodsektori.EditValue;
          if ComboGodina.Text  <> '' then
             dm.tblTrebovanje.ParamByName('godina').Value:=StrToInt(ComboGodina.Text)
          else
             dm.tblTrebovanje.ParamByName('godina').Value:=dmKon.godina;
          dm.tblTrebovanje.Open;
          dm.tblTrebovanjeStavki.Close;
          dm.tblTrebovanjeStavki.Open;
        end;
     end
    else
      begin
        cbPodsektoriTree.Text:='';
        dm.tblTrebovanje.Close;
        dm.tblTrebovanje.ParamByName('re').Value:='DTF';
        if ComboGodina.Text  <> '' then
           dm.tblTrebovanje.ParamByName('godina').Value:=StrToInt(ComboGodina.Text)
        else
           dm.tblTrebovanje.ParamByName('godina').Value:=dmKon.godina;
        dm.tblTrebovanje.Open;

        dm.tblTrebovanjeStavki.Close;
        dm.tblTrebovanjeStavki.Open;
      end;
end;

procedure TfrmTrebovanje.cbPodsektoriTreeExit(Sender: TObject);
begin
   TEdit(Sender).Color:=clWhite;
   if cbPodsektoriTree.Text <> '' then
        begin
           cbPodsektori.EditValue:=dm.tblPodsektoriGodisenPlanRE.Value
        end
     else cbPodsektori.Clear;
end;

procedure TfrmTrebovanje.cbPodsektoriTreeSetDisplayItemText(Sender: TObject;
  var DisplayText: string);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmTrebovanje.aOtkaziExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse))then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else if (cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalenPrikaz);
      PanelDetalenPrikaz.Enabled:=False;
      cxPageControlTrebovanje.ActivePage:=cxTabSheetTabelaren;
      cxGrid1.SetFocus;
  end
  else if (cxGrid2DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
      cxGrid2DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalenStavki);

      cxPageControlStavki.ActivePage:=cxTabSheetTabelarenStavki;
      cxGrid2.SetFocus;
      PanelDetalenStavki.Enabled:=False;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmTrebovanje.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmTrebovanje.aPageSetupStavkiExecute(Sender: TObject);
begin
   dxComponentPrinter1Link2.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmTrebovanje.aPecatiTabelaTrebovanjaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '���������';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + ComboGodina.Text);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + ComboStatus.Text);
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmTrebovanje.aPecatiTabelaStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������ �� ���������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������: ' + intToStr(dm.tblTrebovanjeGODINA.Value));
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� ���������: ' + intToStr(dm.tblTrebovanjeBROJ.Value));
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� ������� �� : ' + dm.tblTrebovanjeRE_NAZIV.Value);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� ������� �� : ' + dm.tblTrebovanjeRE2_NAZIV.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmTrebovanje.aPGodisenPlanOdJNExecute(Sender: TObject);
begin

end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmTrebovanje.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmTrebovanje.aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmTrebovanje.aPregledajPecatiTrebovanjeExecute(Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40042);
    dmKon.tblSqlReport.ParamByName('id').Value:=dm.tblTrebovanjeID.Text;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(dm.tblTrebovanjeGODINA.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTrebovanjeBROJ.Text));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmTrebovanje.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmTrebovanje.aSnimiPecatenjeStavkiExecute(Sender: TObject);
begin
   zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmTrebovanje.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmTrebovanje.aBrisiPodesuvanjePecatenjeStavkiExecute(
  Sender: TObject);
begin
   brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmTrebovanje.aBrisiStavkaExecute(Sender: TObject);
begin
   if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid2DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmTrebovanje.aDizajnTrebovanjeExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',40042);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmTrebovanje.aDodadiStavkiExecute(Sender: TObject);
begin
 if((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse)
     and (cxGrid1DBTableView1.DataController.RecordCount <> 0))then
   begin
      cxPageControlStavki.ActivePage:=cxTabSheetDetalenStavki;
      PanelDetalenStavki.Enabled:=true;
      cxGrid2DBTableView1.DataController.DataSet.Insert;
      dm.tblTrebovanjeStavkiTREBUVANJE_ID.Value:=dm.tblTrebovanjeID.Value;
      A_VID.SetFocus;
   end
 else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmTrebovanje.aDOdlukaKreiranjeIzmenaGOExecute(Sender: TObject);
begin

end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmTrebovanje.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmTrebovanje.aPGodisenPlanPoSektoriExecute(Sender: TObject);
begin
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmTrebovanje.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

procedure TfrmTrebovanje.ZapisiButtonClick(Sender: TObject);
begin

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmTrebovanje.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmTrebovanje.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

end.
