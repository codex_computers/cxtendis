object frmIzborStavkiPonuda: TfrmIzborStavkiPonuda
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1048#1079#1073#1086#1088' '#1080' '#1074#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1072
  ClientHeight = 695
  ClientWidth = 1049
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1049
    695)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1049
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    ExplicitWidth = 928
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 672
    Width = 1049
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Insert - '#1048#1079#1073#1086#1088' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083', F7 - '#1054#1089#1074#1077#1078#1080', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078 +
          #1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 627
    ExplicitWidth = 928
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 200
    Width = 1049
    Height = 472
    Align = alClient
    TabOrder = 2
    ExplicitWidth = 928
    ExplicitHeight = 427
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsIzborStavkiZaPonuda
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1080#1079#1073#1086#1088
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ_TENDER'
        Visible = False
        Width = 121
      end
      object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Width = 42
      end
      object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDNAZIV'
        Width = 73
      end
      object cxGrid1DBTableView1GENERIKA: TcxGridDBColumn
        DataBinding.FieldName = 'GENERIKA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 1
          end>
        Width = 71
      end
      object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKAL'
        Width = 65
      end
      object cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIVARTIKAL'
        Width = 250
      end
      object cxGrid1DBTableView1STAVKA_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'STAVKA_NAZIV'
        Width = 250
      end
      object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
        Width = 59
      end
      object cxGrid1DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Width = 41
      end
      object cxGrid1DBTableView1STAVKA_MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'STAVKA_MERKA'
        Width = 91
      end
      object cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Width = 81
      end
      object cxGrid1DBTableView1TARIFA: TcxGridDBColumn
        DataBinding.FieldName = 'TARIFA'
        Width = 38
      end
      object cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn
        DataBinding.FieldName = 'PROZVODITEL'
        Width = 125
      end
      object cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PROIZVODITELNAZIV'
        Width = 250
      end
      object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Width = 54
      end
      object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Width = 142
      end
      object cxGrid1DBTableView1JN_CPV: TcxGridDBColumn
        DataBinding.FieldName = 'JN_CPV'
        Width = 115
      end
      object cxGrid1DBTableView1MK: TcxGridDBColumn
        DataBinding.FieldName = 'MK'
        Width = 50
      end
      object cxGrid1DBTableView1EN: TcxGridDBColumn
        DataBinding.FieldName = 'EN'
        Width = 50
      end
      object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'VID_ARTIKAL'
        Visible = False
        Width = 50
      end
      object cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn
        DataBinding.FieldName = 'VALIDNOST'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1042#1072#1083#1080#1076#1077#1085
            ImageIndex = 0
            Value = 1
          end
          item
            Description = #1055#1086#1085#1080#1096#1090#1077#1085
            ImageIndex = 1
            Value = 0
          end>
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 126
    Width = 1049
    Height = 74
    Align = alTop
    Alignment = taLeftJustify
    BevelInner = bvLowered
    BevelOuter = bvNone
    Color = clCream
    ParentBackground = False
    TabOrder = 3
    ExplicitWidth = 928
    object Label8: TLabel
      Left = 14
      Top = 10
      Width = 195
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1085#1091#1076#1072' '#1073#1088#1086#1112' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Left = 14
      Top = 48
      Width = 195
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1072#1074#1082#1080' '#1086#1076' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 14
      Top = 29
      Width = 195
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
      Color = 10901821
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object cxDBLabel1: TcxDBLabel
      Left = 215
      Top = 9
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 154
    end
    object cxDBLabel2: TcxDBLabel
      Left = 215
      Top = 48
      DataBinding.DataField = 'BROJ_TENDER'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 19
      Width = 1034
    end
    object cxDBLabel3: TcxDBLabel
      Left = 215
      Top = 29
      AutoSize = True
      DataBinding.DataField = 'PARTNERNAZIV'
      DataBinding.DataSource = dm.dsPonudi
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clNavy
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
  end
  object PanelVnesArtikal: TPanel
    Left = 133
    Top = 279
    Width = 705
    Height = 306
    Anchors = []
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 4
    Visible = False
    DesignSize = (
      705
      306)
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        673
        273)
      Height = 273
      Width = 673
      object Label6: TLabel
        Left = 49
        Top = 178
        Width = 50
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1087#1080#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object cxLabel4: TcxLabel
        Left = 40
        Top = 43
        Caption = #1040#1088#1090#1080#1082#1072#1083' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel1: TcxLabel
        Left = 35
        Top = 70
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clNavy
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel2: TcxLabel
        Left = 6
        Top = 97
        Caption = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clNavy
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel3: TcxLabel
        Left = 62
        Top = 124
        Caption = #1044#1044#1042' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel5: TcxLabel
        Left = 59
        Top = 150
        Caption = #1062#1077#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clNavy
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object VID_ARTIKAL: TcxDBTextEdit
        Left = 610
        Top = 69
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsPonudiStavki
        Style.TextStyle = []
        TabOrder = 5
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 43
      end
      object ARTIKAL: TcxDBTextEdit
        Left = 146
        Top = 42
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsPonudiStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 43
      end
      object NAZIV_ARTIKAL: TcxDBTextEdit
        Left = 187
        Top = 42
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'STAVKA_NAZIV'
        DataBinding.DataSource = dm.dsPonudiStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 471
      end
      object KOLICINA: TcxDBTextEdit
        Left = 105
        Top = 69
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsPonudiStavki
        Style.TextStyle = []
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object CENA_BEZ_DDV: TcxDBTextEdit
        Left = 105
        Top = 96
        BeepOnEnter = False
        DataBinding.DataField = 'CENABEZDDV'
        DataBinding.DataSource = dm.dsPonudiStavki
        Style.TextStyle = []
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object DDV: TcxDBTextEdit
        Tag = 1
        Left = 105
        Top = 123
        BeepOnEnter = False
        DataBinding.DataField = 'DANOK'
        DataBinding.DataSource = dm.dsPonudiStavki
        Style.TextStyle = []
        TabOrder = 10
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object CENA: TcxDBTextEdit
        Left = 105
        Top = 150
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsPonudiStavki
        Style.TextStyle = []
        TabOrder = 11
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object OPIS: TcxDBRichEdit
        Left = 105
        Top = 177
        Hint = ' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072'  '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsPonudiStavki
        ParentFont = False
        Properties.WantReturns = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 12
        OnDblClick = OPISDblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 54
        Width = 553
      end
      object ZapisiButton: TcxButton
        Left = 105
        Top = 237
        Width = 75
        Height = 25
        Action = aZapisi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 13
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object OtkaziButton: TcxButton
        Left = 186
        Top = 237
        Width = 75
        Height = 25
        Action = aOtkazi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 14
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxCheckBoxKreirajArtikal: TcxCheckBox
        Left = 195
        Top = 15
        Caption = #1050#1088#1077#1080#1088#1072#1112' '#1072#1088#1090#1080#1082#1072#1083
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        Properties.ReadOnly = False
        Style.Color = clBtnFace
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 15
        Transparent = True
        Width = 109
      end
      object cxCheckBoxOpstNaziv: TcxCheckBox
        Left = 105
        Top = 15
        Caption = #1054#1087#1096#1090' '#1085#1072#1079#1080#1074
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        Properties.ReadOnly = True
        Style.Color = clBtnFace
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 16
        Transparent = True
        Width = 84
      end
      object MERKA: TcxDBTextEdit
        Left = 195
        Top = 69
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'MERKANAZIV'
        DataBinding.DataSource = dm.dsPonudiStavki
        Enabled = False
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.TextColor = clWindowText
        TabOrder = 17
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 86
      end
      object cxLabel6: TcxLabel
        Left = 201
        Top = 150
        Caption = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = []
        Style.IsFontAssigned = True
        StyleDisabled.TextStyle = []
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 194
        Top = 99
        Caption = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = []
        Style.IsFontAssigned = True
        StyleDisabled.TextStyle = []
        Transparent = True
      end
      object IZNOSCENBEZDDV: TcxDBTextEdit
        Left = 285
        Top = 96
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'IZNOSCENBEZDDV'
        DataBinding.DataSource = dm.dsPonudiStavki
        Enabled = False
        Style.TextStyle = []
        StyleDisabled.TextColor = clBackground
        TabOrder = 20
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object IZNOSCENA: TcxDBTextEdit
        Left = 286
        Top = 150
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'IZNOSCENA'
        DataBinding.DataSource = dm.dsPonudiStavki
        Enabled = False
        Style.TextStyle = []
        StyleDisabled.TextColor = clBackground
        TabOrder = 21
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
    end
    object JN_TIP_ARTIKAL: TcxTextEdit
      Left = 121
      Top = 58
      Enabled = False
      Style.TextStyle = [fsBold]
      StyleDisabled.TextColor = clWindowText
      TabOrder = 1
      Text = 'JN_TIP_ARTIKAL'
      Width = 43
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 320
    Top = 616
  end
  object PopupMenu1: TPopupMenu
    Left = 520
    Top = 616
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 504
    Top = 600
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 197
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 640
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 512
    Top = 24
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 760
    Top = 224
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 104
  end
end
