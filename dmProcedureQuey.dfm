object dmPQ: TdmPQ
  OldCreateOrder = False
  Height = 1556
  Width = 694
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 80
    Top = 16
  end
  object pInsStavkiVoPlan: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_INS_STAVKIVOPLAN (?BROJ, ?GODINA, ?VID' +
        '_ARTIKAL, ?ARTIKAL, ?RE, ?KOLICINA, ?CENABEZDDV, ?DANOK)')
    StoredProcName = 'PROC_JN_INS_STAVKIVOPLAN'
    Left = 80
    Top = 72
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pMaxVidDogovor: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_JN_MAX_VIDDOGOVOR (?VID)')
    StoredProcName = 'PROC_JN_MAX_VIDDOGOVOR'
    Left = 232
    Top = 16
  end
  object pCountOdlukiPoGodina: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_JN_COUNTODLUKI (?GODINA)')
    StoredProcName = 'PROC_JN_COUNTODLUKI'
    Left = 232
    Top = 72
  end
  object pPredlogPlan: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_PREDLOGPLAN (?FLAG, ?GODINA, ?RE, ?BRO' +
        'J)')
    StoredProcName = 'PROC_JN_PREDLOGPLAN'
    Left = 80
    Top = 120
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qInsertStavkiVoTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO JN_TENDER_STAVKI('
      '    BROJ_TENDER,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    KOLICINA,'
      '    NAZIV,'
      '    MERKA'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :KOLICINA,'
      '    :ARTIKAL_NAZIV,'
      '    :ARTIKAL_MERKA'
      ')'
      '')
    Left = 80
    Top = 184
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qInsertStavkiREVoTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO JN_TENDER_STAVKI_RE('
      '    BROJ_TENDER,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    RE,'
      '    KOLICINA'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :RE,'
      '    :KOLICINA'
      ')'
      '')
    Left = 80
    Top = 240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qInsertNedeliviGrupiTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO JN_NEDELIVI_GRUPI('
      '    BROJ_TENDER,'
      '    GRUPA'
      ')'
      'VALUES('
      '    :BROJ_TENDER,'
      '    :GRUPA'
      ')')
    Left = 80
    Top = 304
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pGrupnoVrednuvanjeStavkiPonudi: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_GRUPNOVREDNUVANJE (?PONUDA_ID, ?VREDNO' +
        'ST, ?BODOVI, ?BODIRANJE_PO)')
    StoredProcName = 'PROC_JN_GRUPNOVREDNUVANJE'
    Left = 80
    Top = 368
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pInsertArtikal: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_INSERTARTIKAL (?NAZIV, ?ID, ?VID_ARTIK' +
        'AL, ?ARTIKAL)')
    StoredProcName = 'PROC_JN_INSERTARTIKAL'
    Left = 80
    Top = 432
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pInsertDobitnici: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_INSERT_DOBITNICI (?GEN, ?ID_PONUDI_STA' +
        'VKI, ?VID_ARTIKAL, ?ARTIKAL)')
    StoredProcName = 'PROC_JN_INSERT_DOBITNICI'
    Left = 72
    Top = 496
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qCountPonudiZaTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) as c'
      'from jn_ponudi p'
      'where p.broj_tender = :broj_tender')
    Left = 232
    Top = 136
  end
  object pInsertStavkiDogovor: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_INSERT_STAVKIDOGOVOR (?BROJ_DOGOVOR, ?' +
        'VID_ARTIKAL, ?ARTIKAL,?BROJ_TENDER,?KOLICINA)')
    StoredProcName = 'PROC_JN_INSERT_STAVKIDOGOVOR'
    Left = 72
    Top = 560
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qTipBaranje: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      '  select t.tip_baranja'
      '  from jn_tender t'
      '  where t.broj = :broj_tender'
      '  ')
    Left = 240
    Top = 192
  end
  object pDinamikaRealizacija: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_DINAMIKAREALIZACIJA (?MESEC, ?TIP_NABA' +
        'VKA, ?ARTVID, ?ART_GRUPA, ?PARAM, ?GODINA, ?ARTIKAL,?VID_ARTIKAL' +
        ',?PREDMET_NABAVKA)')
    StoredProcName = 'PROC_JN_DINAMIKAREALIZACIJA'
    Left = 72
    Top = 624
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pBodiranjeArt: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_JN_BODIRANJE (?BROJ_TENDER)')
    StoredProcName = 'PROC_JN_BODIRANJE'
    Left = 64
    Top = 680
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteDokumetTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update  jn_tender_dokumenti'
      'set template = null'
      'where id = :id')
    Left = 64
    Top = 736
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pKopirajStavkiOdPlanVoNabavka: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_KOPIRAJ_PLAN_NABAVKA (?VID_ARTIKAL, ?A' +
        'RTIKAL, ?GODINA, ?BROJ_TENDER)')
    StoredProcName = 'PROC_JN_KOPIRAJ_PLAN_NABAVKA'
    Left = 64
    Top = 800
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pPopolniPrvaCena: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_POPOLNI_PRVA_CENA (?BROJ_TENDER, ?TIP_' +
        'PARTNER, ?PARTNER)')
    StoredProcName = 'PROC_JN_POPOLNI_PRVA_CENA'
    Left = 64
    Top = 864
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object peAukcija: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_E_AUKCIJA (?PONUDA_ID, ?GRUPA, ?PROCEN' +
        'T)')
    StoredProcName = 'PROC_JN_E_AUKCIJA'
    Left = 56
    Top = 928
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pKopirajPotrebniDokVoTender: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_KOPIRAJPOTREBNIDOK(?BROJ_TENDER, ?POTR' +
        'EBNI_DOKUMENTI_ID)'
      '')
    StoredProcName = 'PROC_JN_KOPIRAJPOTREBNIDOK'
    Left = 64
    Top = 984
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateDostavenDokumentPonuda: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE JN_PONUDUVAC_DOK'
      'SET '
      '    DOSTAVEN = :DOSTAVEN'
      'WHERE'
      '    PONUDA_ID = :PONUDA_ID')
    Left = 56
    Top = 1048
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateValidnostTenderStavka: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update jn_tender_stavki t'
      'set t.validnost = :validnost,'
      '    t.pricina_ponistena = :pricina'
      'where t.broj_tender = :broj_tender and'
      '      t.artikal = :artikal and'
      '      t.vid_artikal = :vid_artikal')
    Left = 64
    Top = 1112
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pPrevzemiKriteriumi: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_PREVZEMI_KRITERIUMI (?BROJ_TENDER, ?PO' +
        'NUDA_ID)')
    StoredProcName = 'PROC_JN_PREVZEMI_KRITERIUMI'
    Left = 56
    Top = 1176
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pZatvoriTenderi: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_JN_ZATVORI_TENDER (1)')
    StoredProcName = 'PROC_JN_ZATVORI_TENDER'
    Left = 56
    Top = 1240
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qRakovoditelSektor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select m.rakovoditel'
      'from mat_re m'
      'where m.re = :re')
    Left = 248
    Top = 264
  end
  object PROC_JN_PREVZEMI_PONISTENI: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_PREVZEMI_PONISTENI (?BROJ_TENDER, ?BRO' +
        'J_VRSKA)')
    StoredProcName = 'PROC_JN_PREVZEMI_PONISTENI'
    Left = 56
    Top = 1304
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteStavkiTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER'
      'WHERE'
      '        BROJ = :BROJ')
    Left = 48
    Top = 1376
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDaliEVrska: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select Count(t.broj) br'
      'from jn_tender t'
      'where t.vrska_predmet = :vrska')
    Left = 240
    Top = 344
  end
  object qDeleteStavkREiTender: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    JN_TENDER_STAVKI_RE'
      'WHERE'
      '    BROJ_TENDER = :BROJ'
      '')
    Left = 48
    Top = 1432
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qSetupBrojSluzbenVesnik: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select s.v1'
      'from jn_setup s'
      'where s.p1 = '#39'broj_sluzben_vesnik'#39)
    Left = 230
    Top = 416
  end
  object qBrisiStavkiGrupi: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'delete from jn_tender_stavki ts'
      ''
      'where exists (select *'
      '              from jn_tender_stavki jts'
      
        '              inner join mtr_artikal ma on ma.artvid = jts.vid_a' +
        'rtikal and ma.id = jts.artikal'
      '              inner join mtr_artgrupa g on g.id = ma.grupa'
      
        '              where jts.broj_tender =  :BROJ_TENDER and ma.grupa' +
        ' = :GRUPA'
      
        '                    and jts.artikal = ts.artikal and jts.vid_art' +
        'ikal = ts.vid_artikal and jts.broj_tender = ts.broj_tender)')
    Left = 232
    Top = 488
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qCountPonisteniStavkiTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(ts.broj_tender)as br'
      'from jn_tender_stavki ts'
      'where ts.broj_tender = :broj_tender and ts.validnost = 0')
    Left = 232
    Top = 552
  end
  object qCountPonudiStavkiZaTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.id) as br'
      'from jn_ponudi_stavki p'
      'where p.broj_tender = :broj_tender')
    Left = 224
    Top = 624
  end
  object qCountKriteriumiBodTender: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(p.broj_tender) as br'
      'from jn_tender_gen_bodiranje p'
      'where p.broj_tender = :broj_tender')
    Left = 232
    Top = 704
  end
  object pPrevzemiNazivArtikal: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_ZEMI_NAZIV_STAVKI_TEN  (?BROJ_TENDER_I' +
        'N)')
    Left = 184
    Top = 1432
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pInsertDogovorStavkiAvtomatski: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_INSERT_DOGOVOR_STAVKI (?TIP_PARTNER, ?' +
        'PARTNER, ?BROJ_TENDER, ?BROJ_DOGOVOR)')
    StoredProcName = 'PROC_JN_INSERT_DOGOVOR_STAVKI'
    Left = 344
    Top = 1432
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pBrojPostapka: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_JN_BROJ_POSTAPKA (?TAG, ?GODINA, ?BROJ_IN' +
        ', ?BROJ_PART1_IN)')
    StoredProcName = 'PROC_JN_BROJ_POSTAPKA'
    Left = 368
    Top = 24
  end
end
