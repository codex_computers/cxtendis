object frmIzborStavkiBaranja: TfrmIzborStavkiBaranja
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = ' '
  ClientHeight = 786
  ClientWidth = 1004
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1004
    786)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1004
    Height = 195
    Align = alTop
    Color = clCream
    ParentBackground = False
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 14
      Width = 154
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 41
      Top = 39
      Width = 121
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 41
      Top = 64
      Width = 121
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1114#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxDBLabel1: TcxDBLabel
      Left = 168
      Top = 14
      DataBinding.DataField = 'BROJ_TENDER'
      DataBinding.DataSource = dm.dsBaranja
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 21
      Width = 481
    end
    object cxDBLabel2: TcxDBLabel
      Left = 168
      Top = 39
      DataBinding.DataField = 'BROJ_DOGOVOR'
      DataBinding.DataSource = dm.dsBaranja
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 21
      Width = 545
    end
    object cxDBLabel3: TcxDBLabel
      Left = 168
      Top = 64
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsBaranja
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
      Height = 21
      Width = 497
    end
    object cxDBRadioGroupTIP_BARANJA: TcxDBRadioGroup
      Left = 17
      Top = 91
      Hint = 
        #1053#1072#1095#1080#1085' '#1085#1072' '#1089#1087#1088#1086#1074#1077#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088', '#1086#1076#1085#1086#1089#1085#1086' '#1073#1072#1088#1072#1114#1072#1090#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1086 +
        #1076' '#1076#1086#1073#1072#1074#1091#1074#1072#1095#1080#1090#1077' '
      TabStop = False
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1086#1076#1085#1089#1085#1086' '#1073#1072#1088#1072#1114#1077#1090#1086' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      DataBinding.DataField = 'TIP_BARANJA'
      DataBinding.DataSource = dm.dsBaranja
      Enabled = False
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1055#1086' '#1087#1086#1085#1091#1076#1072
          Value = 0
        end
        item
          Caption = #1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
          Value = 1
        end
        item
          Caption = #1055#1086' '#1080#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1073#1077#1079' '#1082#1086#1085#1090#1088#1086#1083#1072' '#1085#1072' '#1080#1079#1085#1086#1089' '#1086#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
          Value = 2
        end>
      StyleDisabled.BorderColor = clWindowFrame
      StyleDisabled.TextColor = clWindowText
      TabOrder = 3
      Height = 54
      Width = 712
    end
    object cxLabel9: TcxLabel
      Left = 22
      Top = 154
      Caption = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
    object IZNOS_DOGOVOR: TcxDBTextEdit
      Tag = 1
      Left = 142
      Top = 151
      BeepOnEnter = False
      DataBinding.DataField = 'IZNOS_DOGOVOR'
      DataBinding.DataSource = dm.dsIzborStavkiBaranjePoCena
      Enabled = False
      Properties.ReadOnly = False
      Style.TextStyle = [fsBold]
      StyleDisabled.TextColor = clWindowText
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 115
    end
    object POBARAN_IZNOS_DOGOVOR: TcxDBTextEdit
      Tag = 1
      Left = 384
      Top = 151
      BeepOnEnter = False
      DataBinding.DataField = 'OSTANAT_IZNOS_DOGVOR'
      DataBinding.DataSource = dm.dsIzborStavkiBaranjePoCena
      Enabled = False
      Properties.ReadOnly = False
      Style.TextStyle = [fsBold]
      StyleDisabled.TextColor = clWindowText
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 115
    end
    object cxLabel10: TcxLabel
      Left = 263
      Top = 154
      Caption = #1055#1088#1077#1086#1089#1090#1072#1085#1072#1090' '#1080#1079#1085#1086#1089' :'
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextColor = clBlack
      Style.TextStyle = [fsBold]
      Style.IsFontAssigned = True
      Transparent = True
    end
  end
  object cxGrid0: TcxGrid
    Left = 59
    Top = 359
    Width = 928
    Height = 200
    TabOrder = 1
    object cxGrid0DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid0DBTableView1KeyDown
      OnKeyPress = cxGrid0DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsIzborStavkiBaranjePoCena
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1080#1079#1073#1086#1088
      object cxGrid0DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Options.Editing = False
        Width = 32
      end
      object cxGrid0DBTableView1VIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDNAZIV'
        Options.Editing = False
        Width = 68
      end
      object cxGrid0DBTableView1GENERIKA: TcxGridDBColumn
        DataBinding.FieldName = 'GENERIKA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 71
      end
      object cxGrid0DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'SIFRA'
        Options.Editing = False
        Width = 52
      end
      object cxGrid0DBTableView1ARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKALNAZIV'
        Options.Editing = False
        Width = 208
      end
      object cxGrid0DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Options.Editing = False
        Width = 42
      end
      object cxGrid0DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Options.Editing = False
        Width = 80
      end
      object cxGrid0DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
        Options.Editing = False
      end
      object cxGrid0DBTableView1POBARANA_CENA: TcxGridDBColumn
        DataBinding.FieldName = 'POBARANA_CENA'
        Options.Editing = False
        Width = 89
      end
      object cxGrid0DBTableView1DOZVOLENA_CENA: TcxGridDBColumn
        DataBinding.FieldName = 'DOZVOLENA_CENA'
        Options.Editing = False
        Width = 92
      end
      object cxGrid0DBTableView1IZNOS_DOGOVOR: TcxGridDBColumn
        DataBinding.FieldName = 'IZNOS_DOGOVOR'
        Visible = False
        Width = 97
      end
      object cxGrid0DBTableView1OSTANAT_IZNOS_DOGVOR: TcxGridDBColumn
        DataBinding.FieldName = 'OSTANAT_IZNOS_DOGVOR'
        Visible = False
        Width = 142
      end
      object cxGrid0DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Options.Editing = False
        Width = 44
      end
      object cxGrid0DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Options.Editing = False
        Width = 144
      end
      object cxGrid0DBTableView1PROZVODITEL: TcxGridDBColumn
        DataBinding.FieldName = 'PROZVODITEL'
        Options.Editing = False
        Width = 123
      end
      object cxGrid0DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PROIZVODITELNAZIV'
        Options.Editing = False
        Width = 84
      end
      object cxGrid0DBTableView1JN_CPV: TcxGridDBColumn
        DataBinding.FieldName = 'JN_CPV'
        Options.Editing = False
        Width = 114
      end
      object cxGrid0DBTableView1MK: TcxGridDBColumn
        DataBinding.FieldName = 'MK'
        Options.Editing = False
        Width = 100
      end
      object cxGrid0DBTableView1EN: TcxGridDBColumn
        DataBinding.FieldName = 'EN'
        Options.Editing = False
        Width = 92
      end
      object cxGrid0DBTableView1VID_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'VID'
        Visible = False
        Options.Editing = False
        Width = 78
      end
    end
    object cxGridLevel2: TcxGridLevel
      GridView = cxGrid0DBTableView1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 763
    Width = 1004
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1042#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
        Width = 245
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090', Shift+Ctrl+D - '#1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072 +
          #1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076' '
        Width = 400
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGrid1: TcxGrid
    Left = 8
    Top = 386
    Width = 881
    Height = 200
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsIzborStavkiBaranjePoKolicina
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1080#1079#1073#1086#1088
      object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'JN_TIP_ARTIKAL'
        Options.Editing = False
        Width = 37
      end
      object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDNAZIV'
        Options.Editing = False
        Width = 72
      end
      object cxGrid1DBTableView1GENERIKA: TcxGridDBColumn
        DataBinding.FieldName = 'GENERIKA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            ImageIndex = 0
            Value = 1
          end>
        Options.Editing = False
        Width = 71
      end
      object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'SIFRA'
        Options.Editing = False
        Width = 56
      end
      object cxGrid1DBTableView1ARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKALNAZIV'
        Options.Editing = False
        Width = 223
      end
      object cxGrid1DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Options.Editing = False
        Width = 43
      end
      object cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MERKANAZIV'
        Options.Editing = False
        Width = 80
      end
      object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
        Options.Editing = False
        Width = 62
      end
      object cxGrid1DBTableView1POBARANA_KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'POBARANA_KOLICINA'
        Options.Editing = False
        Width = 107
      end
      object cxGrid1DBTableView1DOZVOLENA_KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'DOZVOLENA_KOLICINA'
        Options.Editing = False
        Width = 118
      end
      object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPA'
        Options.Editing = False
        Width = 41
      end
      object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'GRUPANAZIV'
        Options.Editing = False
        Width = 83
      end
      object cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn
        DataBinding.FieldName = 'PROZVODITEL'
        Options.Editing = False
        Width = 130
      end
      object cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'PROIZVODITELNAZIV'
        Options.Editing = False
        Width = 82
      end
      object cxGrid1DBTableView1JN_CPV: TcxGridDBColumn
        DataBinding.FieldName = 'JN_CPV'
        Options.Editing = False
        Width = 113
      end
      object cxGrid1DBTableView1MK: TcxGridDBColumn
        DataBinding.FieldName = 'MK'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1EN: TcxGridDBColumn
        DataBinding.FieldName = 'EN'
        Options.Editing = False
        Width = 100
      end
      object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'VID'
        Visible = False
        Options.Editing = False
        Width = 75
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PanelVnesArtikalKolicina: TPanel
    Left = 168
    Top = 217
    Width = 753
    Height = 240
    Anchors = []
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 4
    Visible = False
    DesignSize = (
      753
      240)
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1042#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      ExplicitHeight = 214
      DesignSize = (
        721
        207)
      Height = 207
      Width = 721
      object Label4: TLabel
        Left = 29
        Top = 57
        Width = 110
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object cxLabel4: TcxLabel
        Left = 80
        Top = 30
        Caption = #1040#1088#1090#1080#1082#1072#1083' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel1: TcxLabel
        Left = 16
        Top = 83
        Caption = #1055#1086#1085#1091#1076#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel2: TcxLabel
        Left = 300
        Top = 84
        Caption = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel3: TcxLabel
        Left = 75
        Top = 137
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object VID_ARTIKAL: TcxDBTextEdit
        Left = 622
        Top = 43
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Style.TextStyle = []
        TabOrder = 9
        Visible = False
        Width = 43
      end
      object ARTIKAL: TcxDBTextEdit
        Left = 185
        Top = 27
        Hint = #1064#1080#1092#1088#1072
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 10
        Width = 43
      end
      object NAZIV_ARTIKAL: TcxDBTextEdit
        Left = 226
        Top = 27
        Hint = #1053#1072#1079#1080#1074
        TabStop = False
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ARTIKALNAZIV'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 11
        Width = 479
      end
      object KOLICINA: TcxDBTextEdit
        Tag = 1
        Left = 145
        Top = 135
        Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1089#1072#1082#1072#1090#1077' '#1076#1072' '#1074#1080' '#1089#1077' '#1080#1089#1087#1086#1088#1072#1095#1072
        BeepOnEnter = False
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Style.TextStyle = []
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 84
      end
      object ZapisiButton: TcxButton
        Left = 145
        Top = 168
        Width = 75
        Height = 25
        Action = aZapisi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 3
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object OtkaziButton: TcxButton
        Left = 226
        Top = 168
        Width = 75
        Height = 25
        Action = aOtkazi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 4
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object JN_TIP_ARTIKAL: TcxTextEdit
        Left = 145
        Top = 27
        Hint = #1042#1080#1076
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 12
        Width = 43
      end
      object RE_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 185
        Top = 54
        Hint = #1053#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'RE'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'RE'
          end
          item
            Width = 500
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsRE
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 520
      end
      object RE: TcxDBTextEdit
        Tag = 1
        Left = 145
        Top = 54
        Hint = #1064#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Properties.ReadOnly = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 43
      end
      object PONUDENA_KOLICINA: TcxTextEdit
        Left = 146
        Top = 81
        Hint = #1055#1086#1085#1091#1076#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1086#1076' '#1076#1086#1073#1080#1090#1085#1080#1082' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 13
        Width = 84
      end
      object POBARANA_KOLICINA: TcxTextEdit
        Left = 430
        Top = 81
        Hint = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1087#1088#1077#1076#1093#1086#1076#1085#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1080' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 14
        Width = 84
      end
      object cxLabel11: TcxLabel
        Left = 15
        Top = 111
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1079#1072' '#1089#1077#1082#1090#1086#1088' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object KOLICINA_SEKTOR: TcxTextEdit
        Left = 145
        Top = 108
        Hint = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1080#1079#1073#1088#1072#1085' '#1089#1077#1082#1090#1086#1088
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 16
        Width = 84
      end
      object cxLabel12: TcxLabel
        Left = 240
        Top = 111
        Caption = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1079#1072' '#1089#1077#1082#1090#1086#1088' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
        OnClick = cxLabel12Click
      end
      object POBARANA_KOLICINA_SEKTOR: TcxTextEdit
        Left = 430
        Top = 108
        Hint = #1055#1086#1073#1072#1088#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1080#1079#1073#1088#1072#1085' '#1089#1077#1082#1090#1086#1088
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 18
        Width = 84
      end
    end
  end
  object PanelVnesArtikalCena: TPanel
    Left = 128
    Top = 506
    Width = 753
    Height = 240
    Anchors = []
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 11302478
    ParentBackground = False
    TabOrder = 5
    Visible = False
    DesignSize = (
      753
      240)
    object cxGroupBox2: TcxGroupBox
      Left = 16
      Top = 16
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1042#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      DesignSize = (
        721
        207)
      Height = 207
      Width = 721
      object Label5: TLabel
        Left = 29
        Top = 57
        Width = 110
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object cxLabel5: TcxLabel
        Left = 80
        Top = 30
        Caption = #1040#1088#1090#1080#1082#1072#1083' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel6: TcxLabel
        Left = 38
        Top = 84
        Caption = #1055#1083#1072#1085#1080#1088#1072#1085#1072' '#1094#1077#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 39
        Top = 111
        Caption = #1055#1086#1073#1072#1088#1072#1085#1072' '#1094#1077#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlack
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel8: TcxLabel
        Left = 99
        Top = 138
        Caption = #1062#1077#1085#1072' :'
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
      end
      object VID_ARTIKAL_1: TcxDBTextEdit
        Left = 622
        Top = 43
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Style.TextStyle = []
        TabOrder = 9
        Visible = False
        Width = 43
      end
      object ARTIKAL_1: TcxDBTextEdit
        Left = 185
        Top = 27
        Hint = #1064#1080#1092#1088#1072
        TabStop = False
        BeepOnEnter = False
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 10
        Width = 43
      end
      object NAZIV_ARTIKAL_1: TcxDBTextEdit
        Left = 226
        Top = 27
        Hint = #1053#1072#1079#1080#1074
        TabStop = False
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ARTIKALNAZIV'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 11
        Width = 479
      end
      object CENA: TcxDBTextEdit
        Tag = 1
        Left = 145
        Top = 135
        Hint = #1062#1077#1085#1072
        BeepOnEnter = False
        DataBinding.DataField = 'CENA'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Style.TextStyle = []
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 156
      end
      object cxButton1: TcxButton
        Left = 145
        Top = 169
        Width = 75
        Height = 25
        Action = aZapisi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 3
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxButton2: TcxButton
        Left = 226
        Top = 169
        Width = 75
        Height = 25
        Action = aOtkazi
        Anchors = [akRight, akBottom]
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 4
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object JN_TIP_ARTIKAL_1: TcxTextEdit
        Left = 145
        Top = 27
        Hint = #1042#1080#1076
        TabStop = False
        Enabled = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 12
        Width = 43
      end
      object RE_NAZIV_1: TcxDBLookupComboBox
        Tag = 1
        Left = 185
        Top = 54
        Hint = #1053#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Properties.KeyFieldNames = 'RE'
        Properties.ListColumns = <
          item
            Width = 200
            FieldName = 'RE'
          end
          item
            Width = 800
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = dsRE
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 520
      end
      object RE_1: TcxDBTextEdit
        Tag = 1
        Left = 145
        Top = 54
        Hint = #1064#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'RE'
        DataBinding.DataSource = dm.dsBaranjaStavki
        Properties.ReadOnly = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 43
      end
      object PLANIRANA_CENA: TcxDBTextEdit
        Tag = 1
        Left = 145
        Top = 81
        Hint = #1055#1086#1085#1091#1076#1077#1085#1072' '#1094#1077#1085#1072' '#1086#1076' '#1076#1086#1073#1080#1090#1085#1080#1082' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
        BeepOnEnter = False
        DataBinding.DataField = 'PLANIRANA_CENA'
        DataBinding.DataSource = dm.dsIzborStavkiBaranjePoCena
        Enabled = False
        Properties.ReadOnly = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 13
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 156
      end
      object POBARANA_CENA: TcxDBTextEdit
        Tag = 1
        Left = 144
        Top = 108
        Hint = #1055#1086#1073#1072#1088#1072#1085#1072' '#1094#1077#1085#1072' '#1074#1086' '#1087#1088#1077#1076#1093#1086#1076#1085#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1080' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
        BeepOnEnter = False
        DataBinding.DataField = 'POBARANA_CENA'
        DataBinding.DataSource = dm.dsIzborStavkiBaranjePoCena
        Enabled = False
        Properties.ReadOnly = False
        Style.TextStyle = [fsBold]
        StyleDisabled.TextColor = clWindowText
        TabOrder = 14
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 157
      end
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1004
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 6
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid0
    PopupMenus = <>
    Left = 640
    Top = 408
  end
  object PopupMenu1: TPopupMenu
    Left = 760
    Top = 360
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 672
    Top = 376
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      SecondaryShortCuts.Strings = (
        '')
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      SecondaryShortCuts.Strings = (
        'Shift+Ctrl+D')
      OnExecute = aBrisiIzgledExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 768
    Top = 416
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43593.345989988420000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid0
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43593.345990023150000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet2
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 768
    Top = 360
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 720
    Top = 376
  end
  object tblRE: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct  tsr.re,'
      '       m.naziv'
      'from jn_tender_stavki_re tsr'
      'inner join mat_re m on m.id = tsr.re'
      'where tsr.broj_tender like :broj_tender'
      '      and tsr.re like :re'
      
        '      and tsr.artikal = :artikal and tsr.vid_artikal = :vid_arti' +
        'kal'
      
        '      and ((m.id in (select s.re from sys_user_re_app s where s.' +
        'username = :username and s.app = :app and :param = 1)'
      '          ) or (:param = 0))'
      '     '
      'order by m.naziv'
      '')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dm.dsIzborStavkiBaranjePoKolicina
    Left = 488
    Top = 88
    object tblRERE: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'RE'
    end
    object tblRENAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRE: TDataSource
    DataSet = tblRE
    Left = 560
    Top = 88
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 104
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 760
    Top = 392
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 102
      FloatClientHeight = 76
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 197
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 141
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 174
      FloatClientHeight = 113
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 424
    Top = 40
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle19: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle20: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle21: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle22: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle23: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle24: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle25: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle26: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
    object dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet
      Caption = 'PrintStyleSheet'
      Styles.BandHeader = cxStyle14
      Styles.Caption = cxStyle15
      Styles.CardCaptionRow = cxStyle16
      Styles.CardRowCaption = cxStyle17
      Styles.Content = cxStyle18
      Styles.ContentEven = cxStyle19
      Styles.ContentOdd = cxStyle20
      Styles.FilterBar = cxStyle21
      Styles.Footer = cxStyle22
      Styles.Group = cxStyle23
      Styles.Header = cxStyle24
      Styles.Preview = cxStyle25
      Styles.Selection = cxStyle26
      BuiltIn = True
    end
  end
end
