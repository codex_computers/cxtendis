inherited frmPotrebniDokumenti: TfrmPotrebniDokumenti
  Caption = #1055#1086#1090#1088#1077#1073#1085#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1080' '#1079#1072' '#1091#1090#1074#1088#1076#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
  ClientHeight = 745
  ClientWidth = 973
  ExplicitWidth = 981
  ExplicitHeight = 776
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 973
    Height = 258
    ExplicitWidth = 973
    ExplicitHeight = 258
    inherited cxGrid1: TcxGrid
      Width = 969
      Height = 215
      ExplicitWidth = 969
      ExplicitHeight = 215
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsPotrebniDokumneti
        OptionsSelection.HideFocusRectOnExit = True
        OptionsSelection.MultiSelect = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Visible = False
          Options.Editing = False
          Width = 73
        end
        object cxGrid1DBTableView1tipNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'tipNaziv'
          Options.Editing = False
          Width = 243
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Visible = False
        end
        object cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Width = 293
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 571
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 162
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 214
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 202
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 234
        end
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 217
      Width = 969
      Height = 39
      Align = alBottom
      TabOrder = 1
      Visible = False
      DesignSize = (
        969
        39)
      object Label3: TLabel
        Left = 259
        Top = 14
        Width = 278
        Height = 13
        AutoSize = False
        Caption = '* '#1052#1086#1078#1077' '#1076#1072' '#1089#1077' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1072#1090' '#1087#1086#1074#1077#1116#1077' '#1089#1090#1072#1074#1082#1080' '#1086#1076#1077#1076#1085#1072#1096
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxButton1: TcxButton
        Left = 13
        Top = 8
        Width = 240
        Height = 25
        Action = aPrevzemiGiSite
        Anchors = [akLeft, akBottom]
        Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1075#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1090#1077' '#1082#1088#1080#1090#1077#1088#1080#1091#1084#1080
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 0
      end
    end
  end
  inherited dPanel: TPanel
    Top = 384
    Width = 973
    Height = 338
    ExplicitTop = 384
    ExplicitWidth = 973
    ExplicitHeight = 338
    inherited Label1: TLabel
      Top = -2
      Visible = False
      ExplicitTop = -2
    end
    object Label2: TLabel [1]
      Left = 12
      Top = 205
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label71: TLabel [2]
      Left = 13
      Top = 178
      Width = 49
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1088#1091#1087#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Top = -5
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPotrebniDokumneti
      TabOrder = 4
      Visible = False
      ExplicitTop = -5
    end
    inherited OtkaziButton: TcxButton
      Left = 882
      Top = 298
      TabOrder = 6
      ExplicitLeft = 882
      ExplicitTop = 298
    end
    inherited ZapisiButton: TcxButton
      Left = 801
      Top = 298
      TabOrder = 5
      OnKeyDown = EnterKakoTab
      ExplicitLeft = 801
      ExplicitTop = 298
    end
    object cxDBRadioGroupTipDokument: TcxDBRadioGroup
      Left = 69
      Top = 22
      Caption = #1058#1080#1087' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      DataBinding.DataField = 'TIP'
      DataBinding.DataSource = dm.dsPotrebniDokumneti
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1083#1080#1095#1085#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          Value = 0
        end
        item
          Caption = #1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '#1079#1072' '#1074#1088#1096#1077#1114#1077' '#1085#1072' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1076#1077#1112#1085#1086#1089#1090
          Value = 1
        end
        item
          Caption = #1077#1082#1086#1085#1086#1084#1089#1082#1072' '#1080' '#1092#1080#1085#1072#1085#1089#1080#1089#1082#1072' '#1089#1086#1089#1090#1086#1112#1073#1072
          Value = 2
        end
        item
          Caption = #1090#1077#1093#1085#1080#1095#1082#1072' '#1080#1083#1080' '#1087#1088#1086#1092#1077#1089#1080#1086#1085#1072#1083#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1086#1089#1090' '
          Value = 3
        end
        item
          Caption = #1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1089#1080#1089#1090#1077#1084#1080' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077#1090
          Value = 4
        end
        item
          Caption = #1089#1090#1072#1085#1076#1072#1088#1076#1080' '#1079#1072' '#1091#1087#1088#1072#1074#1091#1074#1072#1114#1077' '#1089#1086' '#1078#1080#1074#1086#1090#1085#1072' '#1089#1088#1077#1076#1080#1085#1072
          Value = 5
        end>
      TabOrder = 0
      OnClick = cxDBRadioGroupTipDokumentClick
      Height = 136
      Width = 300
    end
    object NAZIV: TcxDBMemo
      Tag = 1
      Left = 68
      Top = 202
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsPotrebniDokumneti
      Properties.WantReturns = False
      TabOrder = 3
      OnDblClick = NAZIVDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 69
      Width = 709
    end
    object GRUPA: TcxDBTextEdit
      Left = 68
      Top = 175
      Hint = #1043#1088#1091#1087#1072' - '#1064#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'GRUPA'
      DataBinding.DataSource = dm.dsPotrebniDokumneti
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 68
    end
    object GRUPA_NAZIV: TcxDBLookupComboBox
      Left = 136
      Top = 175
      Hint = #1043#1088#1091#1087#1072' - '#1053#1072#1079#1080#1074
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'GRUPA'
      DataBinding.DataSource = dm.dsPotrebniDokumneti
      ParentFont = False
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 100
          FieldName = 'ID'
        end
        item
          Width = 600
          FieldName = 'tipNaziv'
        end
        item
          Width = 900
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListSource = dm.dsSifPotrebniDokGrupi
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 641
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 973
    ExplicitWidth = 973
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 722
    Width = 973
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'nter-'#1047#1072#1087#1080#1096#1080' '#1075#1086' '#1085#1072#1079#1085#1072#1095#1077#1085#1080#1086#1090' '#1082#1088#1080#1090#1077#1088#1080#1091#1084', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 722
    ExplicitWidth = 973
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 256
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    object aPrevzemiGiSite: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1075#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1090#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
      ImageIndex = 50
      OnExecute = aPrevzemiGiSiteExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41067.523471516200000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
