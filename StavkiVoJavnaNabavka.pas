unit StavkiVoJavnaNabavka;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, cxLabel, FIBQuery, pFIBQuery, dxCustomHint, cxHint,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxSplitter,
  cxImageComboBox, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxRichEdit;

type
//  niza = Array[1..5] of Variant;

  TfrmStavkiVoJavnaNabavka = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid2DBTableView1SUMAKOLICINA: TcxGridDBColumn;
    Panel7: TPanel;
    Panel8: TPanel;
    qObjavenaKolicina: TpFIBQuery;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid5: TcxGrid;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid5DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid5DBTableView1ID: TcxGridDBColumn;
    cxGrid5DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid5DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid5DBTableView1MERKA: TcxGridDBColumn;
    cxGrid5DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid5DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid5DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid5DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid5DBTableView1GRUPANZIV: TcxGridDBColumn;
    cxGrid5DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid5DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid5DBTableView1MK: TcxGridDBColumn;
    cxGrid5DBTableView1EN: TcxGridDBColumn;
    cxGrid5DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid5DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1GENERIKA: TcxGridDBColumn;
    Panel9: TPanel;
    Panel10: TPanel;
    cxGrid5DBTableView1CENA: TcxGridDBColumn;
    cxGrid5DBTableView1TARIFA: TcxGridDBColumn;
    aSifArtikli: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridPopupMenu4: TcxGridPopupMenu;
    cxGridPopupMenu5: TcxGridPopupMenu;
    Panel11: TPanel;
    cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid3DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiNedelivaGrupa: TAction;
    aBrisiNedelivaGrupa: TAction;
    aOkNedeliviGrupi: TAction;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    PanelNedeliviGrupi: TPanel;
    cxGroupBoxNedelivaGrupa: TcxGroupBox;
    cxGrid6: TcxGrid;
    cxGrid6DBTableView1: TcxGridDBTableView;
    cxGrid6DBTableView1ColumnOdberi: TcxGridDBColumn;
    cxGrid6DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid6DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid6Level1: TcxGridLevel;
    ButtonZapisi2: TcxButton;
    ButtonOtkazi2: TcxButton;
    PanelInsertStavki: TPanel;
    Label2: TLabel;
    cxGroupBox1: TcxGroupBox;
    cxLabel2: TcxLabel;
    ZBIRNA: TcxTextEdit;
    OBJAVENA: TcxTextEdit;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4DBTableView1RE: TcxGridDBColumn;
    cxGrid4DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1PLANIRANAKOLICINA: TcxGridDBColumn;
    cxGrid4DBTableView1OBJAVENA: TcxGridDBColumn;
    cxGrid4DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid4Level1: TcxGridLevel;
    cxLabel1: TcxLabel;
    ARTIKAL: TcxTextEdit;
    NAZIV_ARTIKAL: TcxTextEdit;
    cxLabel4: TcxLabel;
    VID: TcxTextEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1MK: TcxGridDBColumn;
    cxGrid1DBTableView1EN: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView2BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2RE: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIVRE: TcxGridDBColumn;
    aKopirajStavkiOdPlan: TAction;
    qOstanataKolicinaZaArtikal: TpFIBQuery;
    cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn;
    dxBarLargeButton19: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    aBrisiStavkiGrupa: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    PanelBrisiGrupaStavki: TPanel;
    cxGroupBox2: TcxGroupBox;
    cxGrid7: TcxGrid;
    cxGridLevel1: TcxGridLevel;
    cxButtonBrsiStavkiGrupaOK: TcxButton;
    cxButtonBrsiStavkiGrupaOtkazi: TcxButton;
    cxGrid7DBTableView1: TcxGridDBTableView;
    cxGrid7DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid7DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid7DBTableView1Odberi: TcxGridDBColumn;
    aOKBrisiStavkiGrupa: TAction;
    qIznosPoPlan: TpFIBQuery;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA1: TcxGridDBColumn;
    dxBarLargeButton21: TdxBarLargeButton;
    aAzurirajNazivMerka: TAction;
    btnCeni: TdxBarLargeButton;
    cxGrid1DBTableView2Column1: TcxGridDBColumn;
    cxGrid1DBTableView2Column2: TcxGridDBColumn;
    cxGrid1DBTableView2Column3: TcxGridDBColumn;
    qUpdateStatus: TpFIBQuery;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid4DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid5DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aSifArtikliExecute(Sender: TObject);
    procedure cxGrid1DBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid4DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid5DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aDodadiNedelivaGrupaExecute(Sender: TObject);
    procedure aBrisiNedelivaGrupaExecute(Sender: TObject);
    procedure cxLabel5Click(Sender: TObject);
    procedure cxGrid6DBTableView1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure aOkNedeliviGrupiExecute(Sender: TObject);
    procedure aKopirajStavkiOdPlanExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aBrisiStavkiGrupaExecute(Sender: TObject);
    procedure aOKBrisiStavkiGrupaExecute(Sender: TObject);
    procedure aAzurirajNazivMerkaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    promena_stavka: Integer;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmStavkiVoJavnaNabavka: TfrmStavkiVoJavnaNabavka;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmProcedureQuey,
  Artikal, AzurirajNazivMerkaStavka;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmStavkiVoJavnaNabavka.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmStavkiVoJavnaNabavka.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmStavkiVoJavnaNabavka.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmStavkiVoJavnaNabavka.aAzurirajNazivMerkaExecute(Sender: TObject);
begin
   if dm.tblTenderZATVOREN.Value=-1 then
   begin
    frmAzurirajStavkiNabavka:=TfrmAzurirajStavkiNabavka.Create(Application);
    frmAzurirajStavkiNabavka.ShowModal();
    frmAzurirajStavkiNabavka.Free;
   end
   else ShowMessage('��������� � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
end;

procedure TfrmStavkiVoJavnaNabavka.aBrisiExecute(Sender: TObject);
begin
if dm.tblTenderZATVOREN.Value=-1 then
   begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
         begin
            cxGrid1DBTableView1.DataController.DataSet.Delete();
            promena_stavka:=1;
            if dm.tblTenderTIP_GOD_PLAN.Value = 1 then
               dm.tblIzborStavkiTenderOdSifArtikli.FullRefresh
            else
               dm.tblIzborStavkiTenderOdGodPlan.FullRefresh;
         end;
   end
else ShowMessage('������ � ��������� ���� �� ����� ������� �� ������ "�� ����������"');

end;

procedure TfrmStavkiVoJavnaNabavka.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmStavkiVoJavnaNabavka.aBrisiNedelivaGrupaExecute(Sender: TObject);
begin
 if dm.tblTenderZATVOREN.Value=-1 then
   begin
     if (cxGrid3DBTableView1.DataController.RecordCount <> 0) then
         cxGrid3DBTableView1.DataController.DataSet.Delete();
   end
 else ShowMessage('������ � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
end;

//	����� �� ���������� �� ����������
procedure TfrmStavkiVoJavnaNabavka.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmStavkiVoJavnaNabavka.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmStavkiVoJavnaNabavka.aKopirajStavkiOdPlanExecute(Sender: TObject);
var br,i, flag:integer;
    status: TStatusWindowHandle;
begin
   status := cxCreateStatusWindow();
   try
     br:=0;
     with cxGrid2DBTableView1.DataController do
     for I := 0 to cxGrid2DBTableView1.Controller.SelectedRecordCount - 1 do
        begin
          flag:=cxGrid2DBTableView1.Controller.SelectedRecords[i].RecordIndex;
          dmPQ.insert6(dmPQ.pKopirajStavkiOdPlanVoNabavka,'VID_ARTIKAL','ARTIKAL','GODINA','BROJ_TENDER',Null,Null,
                              GetValue(flag,cxGrid2DBTableView1VID_ARTIKAL.Index),
                              GetValue(flag,cxGrid2DBTableView1ARTIKAL.Index),
                              GetValue(flag,cxGrid2DBTableView1GODINA.Index), dm.tblTenderBROJ.Value,Null,Null);
          br:=1;
          promena_stavka:=1;
        end;
   if br = 1 then
      begin
         ShowMessage('�������� � �������� �� �������� �� ��������� ���� !!!');
         dm.tblTenderStavki.Close;
         dm.tblTenderStavki.Open;
         dm.tblIzborStavkiTenderOdGodPlan.Close;
         dm.tblIzborStavkiTenderOdGodPlan.Open;
         dm.tblTenderStavkiRE.Close;
         dm.tblTenderStavkiRE.Open;
         cxGrid2.SetFocus;
      end;
  finally
   	cxRemoveStatusWindow(status);
  end;

end;

procedure TfrmStavkiVoJavnaNabavka.aSifArtikliExecute(Sender: TObject);
begin
     if dmKon.prava = 0 then
        begin
          frmArtikal:=TfrmArtikal.Create(self,false);
          frmArtikal.ShowModal;
          if (frmArtikal.ModalResult = mrOK) then
              begin
                dm.tblIzborStavkiTenderOdSifArtikli.FullRefresh;
                dm.tblIzborStavkiTenderOdSifArtikli.Locate('ARTVID; ID',  VarArrayOf([StrToInt(frmArtikal.GetSifra(0)), StrToInt(frmArtikal.GetSifra(1))]) , []);
              end;
          frmArtikal.Free;
        end
     else
       ShowMessage('������ ����� �� �������� �������� !!!');
end;

procedure TfrmStavkiVoJavnaNabavka.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmStavkiVoJavnaNabavka.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� ����� ������� ���: '+dm.tblTenderBROJ.Value+' ������: '+ IntToStr(dm.tblTenderGodina.Value));
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmStavkiVoJavnaNabavka.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmStavkiVoJavnaNabavka.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmStavkiVoJavnaNabavka.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmStavkiVoJavnaNabavka.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmStavkiVoJavnaNabavka.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmStavkiVoJavnaNabavka.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmStavkiVoJavnaNabavka.prefrli;
begin
end;

procedure TfrmStavkiVoJavnaNabavka.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
      //if promena_stavka =1 then
       //  begin
       if dm.tblTenderZATVOREN.Value=-1 then
       begin
           qIznosPoPlan.Close;
           qIznosPoPlan.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
           qIznosPoPlan.ExecQuery;
           dm.tblTender.Edit;
           dm.tblTenderIZNOS.Value:=qIznosPoPlan.FldByName['iznosBezDDV'].Value;
           dm.tblTenderIZNOS_SO_DDV.Value:=qIznosPoPlan.FldByName['iznosSoDDV'].Value;
           dm.tblTender.Post;
       end;
       //  end;
end;
procedure TfrmStavkiVoJavnaNabavka.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmStavkiVoJavnaNabavka.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    dxComponentPrinter1Link1.ReportTitle.Text := '������ �� ����� �������';
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

     Caption:='������ �� ����� �������: ������ - '+intToStr(dm.tblTenderGODINA.Value)+', ��Σ - ' + dm.tblTenderBROJ.Value;
     Panel7.Caption:='������ �� ����� �������: ������ - '+intToStr(dm.tblTenderGODINA.Value)+', ��Σ - ' + dm.tblTenderBROJ.Value;

     dm.tblTenderStavki.close;
     dm.tblTenderStavki.Open;

     dm.tblTenderStavkiRE.close;
     dm.tblTenderStavkiRE.Open;

     dm.tblNedeliviGrupi.Close;
     dm.tblNedeliviGrupi.Open;

     dm.tblIzborNedeliviGrupi.Close;
     dm.tblIzborNedeliviGrupi.Open;

     dm.tblGrupaStavkiBrisi.Close;
     dm.tblGrupaStavkiBrisi.Open;

     if dm.tblTenderTIP_GOD_PLAN.Value = 1 then
        begin
          cxGrid2.Visible:=false;
          cxgrid5.Visible:=true;
          Panel8.Caption:='������ �� ��������� �� �������';
          Panel10.Caption:=' Insert - ���� �� ������, CTRL+A - ������ ��������� �� �������';
          dm.tblIzborStavkiTenderOdSifArtikli.close;
          dm.tblIzborStavkiTenderOdSifArtikli.Open;
        end
     else
        begin
          aSifArtikli.Enabled:=false;
          Panel8.Caption:='������ �� ������� ���� �� '+intToStr(dm.tblTenderGODINA.Value)+' ������';
          Panel10.Caption:=' Insert - ����� �� ������, Ctrl+P - ������ �� ������������ ������ �� ������� ����';
          dm.tblIzborStavkiTenderOdGodPlan.close;
          dm.tblIzborStavkiTenderOdGodPlan.ParamByName('godina').Value:=dm.tblTenderGODINA.Value;
          dm.tblIzborStavkiTenderOdGodPlan.Open;
        end;
     PanelInsertStavki.Top:=260;
     PanelInsertStavki.Left:=103;

     PanelNedeliviGrupi.Top:=291;
     PanelNedeliviGrupi.Left:=119;

     sobrano := true;

     dmPQ.qDaliEVrska.Close;
     dmPQ.qDaliEVrska.ParamByName('vrska').Value:=dm.tblTenderBROJ.Value;
     dmPQ.qDaliEVrska.ExecQuery;
     if (dmPQ.qDaliEVrska.FldByName['br'].Value = 1) or (dm.tblTenderPONISTEN.Value = 0) then
        begin
           aKopirajStavkiOdPlan.Enabled:=False;
           aSifArtikli.Enabled:=False;
           cxGrid2.Enabled:=False;
           cxGrid5.Enabled:=False;
        end;

     promena_stavka:=0;

end;
//------------------------------------------------------------------------------

procedure TfrmStavkiVoJavnaNabavka.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmStavkiVoJavnaNabavka.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1VALIDNOST.Index] = 0) then
       AStyle := dmRes.RedLight;
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid1DBTableView2KeyPress(Sender: TObject;
  var Key: Char);
begin
//     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView2);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
case Key of
VK_INSERT:
 begin
   if dm.tblTenderZATVOREN.Value=-1 then
    begin
      if ((PanelInsertStavki.Visible = false) and (PanelNedeliviGrupi.Visible = False) and (PanelBrisiGrupaStavki.Visible = False))then
        begin
         if dm.tblIzborStavkiTenderOdGodPlanSUMAKOLICINA.Value > 0 then
          begin
           PanelInsertStavki.Visible:=True;

           VID.Text:=IntToStr(dm.tblIzborStavkiTenderOdGodPlanJN_TIP_ARTIKAL.Value);
           ARTIKAL.Text:=IntToStr(dm.tblIzborStavkiTenderOdGodPlanARTIKAL.Value);
           NAZIV_ARTIKAL.Text:=dm.tblIzborStavkiTenderOdGodPlanARTIKALNAZIV.Value;
           NAZIV_ARTIKAL.Hint:=dm.tblIzborStavkiTenderOdGodPlanARTIKALNAZIV.Value;
           ZBIRNA.Text:=FloatToStr(dm.tblIzborStavkiTenderOdGodPlanSUMAKOLICINA.Value);

           qObjavenaKolicina.Close;
           qObjavenaKolicina.ParamByName('artikal').Value:=dm.tblIzborStavkiTenderOdGodPlanARTIKAL.Value;
           qObjavenaKolicina.ParamByName('vid_artikal').Value:=dm.tblIzborStavkiTenderOdGodPlanVID_ARTIKAL.Value;
           qObjavenaKolicina.ParamByName('godina').Value:=dm.tblTenderGODINA.Value;
           qObjavenaKolicina.ExecQuery;

           OBJAVENA.Text:=FloatToStr(qObjavenaKolicina.FldByName['objavena'].Value);

           dm.tblVnesStavkiTenderOdGodPlan.Close;
           dm.tblVnesStavkiTenderOdGodPlan.ParamByName('godina').Value:=dm.tblIzborStavkiTenderOdGodPlanGODINA.Value;
           dm.tblVnesStavkiTenderOdGodPlan.ParamByName('artikal').Value:=dm.tblIzborStavkiTenderOdGodPlanARTIKAL.Value;
           dm.tblVnesStavkiTenderOdGodPlan.ParamByName('vid_artikal').Value:=dm.tblIzborStavkiTenderOdGodPlanVID_ARTIKAL.Value;
           dm.tblVnesStavkiTenderOdGodPlan.Open;

           cxGrid4.SetFocus;
          end
         else ShowMessage('�������� �� �� �������� ��������, ������ �������� ��� �������� 0 !!!');
        end
      else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
    end
    else ShowMessage('�������� � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
 end;
end;
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
    if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid4DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var i  :Integer;
kolicina:Double;
begin
     case Key of
       VK_INSERT:
        begin
          kolicina:=0;
          with cxGrid4DBTableView1.DataController do
          for I := 0 to RecordCount - 1 do
              begin
                if (GetValue(i,cxGrid4DBTableView1KOLICINA.Index)> 0) then
                   begin
                    kolicina:=kolicina + GetValue(i,cxGrid4DBTableView1KOLICINA.Index);
                   end;
              end;
          if kolicina > 0 then
             begin
              dmPQ.qInsertStavkiVoTender.close;
              dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL').Value:=StrToInt(ARTIKAL.Text);
              dmPQ.qInsertStavkiVoTender.ParamByName('VID_ARTIKAL').Value:=dm.tblIzborStavkiTenderOdGodPlanVID_ARTIKAL.Value;
              dmPQ.qInsertStavkiVoTender.ParamByName('KOLICINA').Value:=kolicina;
              dmPQ.qInsertStavkiVoTender.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
              dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL_NAZIV').Value:=dm.tblIzborStavkiTenderOdGodPlanARTIKALNAZIV.Value;
              dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL_MERKA').Value:=dm.tblIzborStavkiTenderOdGodPlanMERKA.Value;
              dmPQ.qInsertStavkiVoTender.ExecQuery;
              promena_stavka:=1;
              with cxGrid4DBTableView1.DataController do
              for I := 0 to RecordCount - 1 do
               begin
                if (GetValue(i,cxGrid4DBTableView1KOLICINA.Index)> 0) then
                   begin
                    dmPQ.qInsertStavkiREVoTender.close;
                    dmPQ.qInsertStavkiREVoTender.ParamByName('ARTIKAL').Value:=StrToInt(ARTIKAL.Text);
                    dmPQ.qInsertStavkiREVoTender.ParamByName('VID_ARTIKAL').Value:=dm.tblIzborStavkiTenderOdGodPlanVID_ARTIKAL.Value;
                    dmPQ.qInsertStavkiREVoTender.ParamByName('KOLICINA').Value:=GetValue(i,cxGrid4DBTableView1KOLICINA.Index);
                    dmPQ.qInsertStavkiREVoTender.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
                    dmPQ.qInsertStavkiREVoTender.ParamByName('RE').Value:=GetValue(i,cxGrid4DBTableView1RE.Index);
                    dmPQ.qInsertStavkiREVoTender.ExecQuery;
                  end;
               end;
              dm.tblTenderStavki.Close;
              dm.tblTenderStavki.Open;
              dm.tblIzborStavkiTenderOdGodPlan.FullRefresh;
              dm.tblTenderStavkiRE.Close;
              dm.tblTenderStavkiRE.Open;
              PanelInsertStavki.Visible:=false;
              If cxGrid2DBTableView1.DataController.RecordCount > 0 then  cxGrid2DBTableView1.Controller.FocusedRowIndex :=0;
                 cxGrid2.SetFocus;
             end
          else
             begin
               ShowMessage('���������� �� ���� ������ � 0 !!!');
               PanelInsertStavki.Visible:=false;
               cxGrid2.SetFocus;
             end;
        end;
    end;
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid4DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if (cxGrid4DBTableView1.Controller.FocusedColumn <> cxGrid4DBTableView1KOLICINA) then
         if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid4DBTableView1);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid5DBTableView1KeyDown(Sender: TObject;
       var Key: Word; Shift: TShiftState);
var i, k_nula:Integer;
begin
case Key of
 VK_INSERT:
  begin
   if dm.tblTenderZATVOREN.Value=-1 then
     begin
       if PanelNedeliviGrupi.Visible = false then
        begin
           k_nula:=0;
           with cxGrid5DBTableView1.DataController do
           for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
             begin
               if (GetValue(i,cxGrid5DBTableView1KOLICINA.Index) > 0) and (GetValue(i,cxGrid5DBTableView1TARIFA.Index) <> Null)then
                 begin
                    dmPQ.qInsertStavkiVoTender.close;
                    dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL').Value:=GetValue(i,cxGrid5DBTableView1ID.Index);
                    dmPQ.qInsertStavkiVoTender.ParamByName('VID_ARTIKAL').Value:=GetValue(i,cxGrid5DBTableView1ARTVID.Index);
                    dmPQ.qInsertStavkiVoTender.ParamByName('KOLICINA').Value:=GetValue(i,cxGrid5DBTableView1KOLICINA.Index);
                    dmPQ.qInsertStavkiVoTender.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
                    dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL_NAZIV').Value:=GetValue(i,cxGrid5DBTableView1NAZIV.Index);
                    dmPQ.qInsertStavkiVoTender.ParamByName('ARTIKAL_MERKA').Value:=GetValue(i,cxGrid5DBTableView1MERKA.Index);
                    dmPQ.qInsertStavkiVoTender.ExecQuery;
                    promena_stavka:=1;
                    k_nula:=1;
                 end;
             end;
           if k_nula = 1 then
              begin
                dm.tblTenderStavki.FullRefresh;
                dm.tblIzborStavkiTenderOdSifArtikli.FullRefresh;
              end
           else
              begin
                ShowMessage('���������� �� ���� ������ � 0 !!!');
              end;
        end
       else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
     end
   else ShowMessage('�������� � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
  end;
end;
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid5DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
   if (cxGrid5DBTableView1.Controller.FocusedColumn <> cxGrid5DBTableView1KOLICINA) then
         if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid5DBTableView1);
end;

procedure TfrmStavkiVoJavnaNabavka.cxGrid6DBTableView1EditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
   var i, stikla:Integer;
  begin
     case Key of
       VK_INSERT:
        begin
          stikla:=0;
          with cxGrid6DBTableView1.DataController do
          for I := 0 to RecordCount - 1 do
              begin
                if (GetValue(i,cxGrid6DBTableView1ColumnOdberi.Index)=true) then
                   begin
                      stikla:=1;
                      dmPQ.qInsertNedeliviGrupiTender.close;
                      dmPQ.qInsertNedeliviGrupiTender.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
                      dmPQ.qInsertNedeliviGrupiTender.ParamByName('GRUPA').Value:=GetValue(i,cxGrid6DBTableView1GRUPA.Index);
                      dmPQ.qInsertNedeliviGrupiTender.ExecQuery;
                   end;
              end;
          if stikla = 0 then
             ShowMessage('�� �������� ���� ���� ����� !!!')
          else
             PanelNedeliviGrupi.Visible:=false;
          dm.tblNedeliviGrupi.FullRefresh;
        end;
     end;
end;

procedure TfrmStavkiVoJavnaNabavka.cxLabel5Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmStavkiVoJavnaNabavka.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmStavkiVoJavnaNabavka.aOKBrisiStavkiGrupaExecute(Sender: TObject);
var i, stikla:Integer;
begin
          stikla:=0;
          with cxGrid7DBTableView1.DataController do
          for I := 0 to RecordCount - 1 do
              begin
                if (GetValue(i,cxGrid7DBTableView1Odberi.Index)=true) then
                   begin
                      stikla:=1;
                      dmPQ.qBrisiStavkiGrupi.close;
                      dmPQ.qBrisiStavkiGrupi.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
                      dmPQ.qBrisiStavkiGrupi.ParamByName('GRUPA').Value:=GetValue(i,cxGrid6DBTableView1GRUPA.Index);
                      dmPQ.qBrisiStavkiGrupi.ExecQuery;
                      promena_stavka:=1;
                   end;
              end;
          if stikla = 0 then
             ShowMessage('�� �������� ���� ���� ����� !!!')
          else
           begin
             PanelBrisiGrupaStavki.Visible:=false;

             dm.tblTenderStavki.Close;
             dm.tblTenderStavki.Open;

             dm.tblTenderStavkiRE.Close;
             dm.tblTenderStavkiRE.Open;

             if dm.tblTenderTIP_GOD_PLAN.Value = 1 then
                dm.tblIzborStavkiTenderOdSifArtikli.FullRefresh
             else
                dm.tblIzborStavkiTenderOdGodPlan.FullRefresh;

             dm.tblGrupaStavkiBrisi.FullRefresh;
           end;

end;

procedure TfrmStavkiVoJavnaNabavka.aOkNedeliviGrupiExecute(Sender: TObject);
var i, stikla:Integer;
begin
     stikla:=0;
          with cxGrid6DBTableView1.DataController do
          for I := 0 to RecordCount - 1 do
              begin
                if (GetValue(i,cxGrid6DBTableView1ColumnOdberi.Index)=true) then
                   begin
                      stikla:=1;
                      dmPQ.qInsertNedeliviGrupiTender.close;
                      dmPQ.qInsertNedeliviGrupiTender.ParamByName('BROJ_TENDER').Value:=dm.tblTenderBROJ.Value;
                      dmPQ.qInsertNedeliviGrupiTender.ParamByName('GRUPA').Value:=GetValue(i,cxGrid6DBTableView1GRUPA.Index);
                      dmPQ.qInsertNedeliviGrupiTender.ExecQuery;
                   end;
              end;
          if stikla = 0 then
             ShowMessage('�� �������� ���� ���� ����� !!!')
          else
             PanelNedeliviGrupi.Visible:=false;
          dm.tblNedeliviGrupi.FullRefresh;
end;

procedure TfrmStavkiVoJavnaNabavka.aOtkaziExecute(Sender: TObject);
begin
    if PanelInsertStavki.Visible = true then
       begin
         PanelInsertStavki.Visible:=false;
         cxGrid2.SetFocus;
       end
    else if PanelNedeliviGrupi.Visible = true then
       begin
         PanelNedeliviGrupi.Visible:=false;
         cxGrid1.SetFocus;
       end
    else if PanelBrisiGrupaStavki.Visible = true then
       begin
         PanelBrisiGrupaStavki.Visible:=false;
         cxGrid1.SetFocus;
       end
    else
       begin
         ModalResult := mrCancel;
         Close();
       end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmStavkiVoJavnaNabavka.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmStavkiVoJavnaNabavka.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('���: ' + dm.tblTenderBROJ.Value);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������: ' + dm.tblTenderBROJ.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmStavkiVoJavnaNabavka.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmStavkiVoJavnaNabavka.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmStavkiVoJavnaNabavka.aSpustiSoberiExecute(Sender: TObject);
begin
     if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmStavkiVoJavnaNabavka.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmStavkiVoJavnaNabavka.aBrisiStavkiGrupaExecute(Sender: TObject);
begin
if dm.tblTenderZATVOREN.Value=-1 then
   begin
     if ((PanelInsertStavki.Visible = false) and (PanelNedeliviGrupi.Visible = False) and (PanelBrisiGrupaStavki.Visible = False))then
        begin
           dm.tblGrupaStavkiBrisi.FullRefresh;
           PanelBrisiGrupaStavki.Visible:=true;
           cxGrid7.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
    end
else ShowMessage('������ � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
end;

procedure TfrmStavkiVoJavnaNabavka.aDodadiNedelivaGrupaExecute(Sender: TObject);
begin
 if dm.tblTenderZATVOREN.Value=-1 then
   begin
    if ((PanelInsertStavki.Visible = false) and (PanelNedeliviGrupi.Visible = False) and (PanelBrisiGrupaStavki.Visible = False))then
        begin
           dm.tblIzborNedeliviGrupi.FullRefresh;
           PanelNedeliviGrupi.Visible:=true;
           cxGrid6.SetFocus;
        end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
   end
 else ShowMessage('�������� �������� ����� � ��������� ���� �� ����� ������� �� ������ "�� ����������"');
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmStavkiVoJavnaNabavka.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmStavkiVoJavnaNabavka.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmStavkiVoJavnaNabavka.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
