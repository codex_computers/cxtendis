inherited frmVidoviDogovor: TfrmVidoviDogovor
  Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
  ClientHeight = 611
  ClientWidth = 663
  ExplicitWidth = 679
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 663
    Height = 228
    ExplicitWidth = 663
    ExplicitHeight = 228
    inherited cxGrid1: TcxGrid
      Width = 659
      Height = 224
      ExplicitWidth = 659
      ExplicitHeight = 224
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsVidoviDogovori
        OptionsData.Editing = True
        object cxGrid1DBTableView1VID: TcxGridDBColumn
          DataBinding.FieldName = 'VID'
          Visible = False
          Options.Editing = False
          Width = 92
        end
        object cxGrid1DBTableView1VidNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'VidNaziv'
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView1SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
          Options.Editing = False
          Width = 81
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 478
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 150
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 354
    Width = 663
    Height = 234
    ExplicitTop = 296
    ExplicitWidth = 663
    ExplicitHeight = 234
    inherited Label1: TLabel
      Left = 29
      Top = 78
      ExplicitLeft = 29
      ExplicitTop = 78
    end
    object Label2: TLabel [1]
      Left = 29
      Top = 105
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 85
      Top = 75
      DataBinding.DataField = 'SIFRA'
      DataBinding.DataSource = dm.dsVidoviDogovori
      TabOrder = 1
      ExplicitLeft = 85
      ExplicitTop = 75
      ExplicitWidth = 204
      Width = 204
    end
    inherited OtkaziButton: TcxButton
      Left = 572
      Top = 194
      TabOrder = 4
      ExplicitLeft = 572
      ExplicitTop = 194
    end
    inherited ZapisiButton: TcxButton
      Left = 491
      Top = 194
      TabOrder = 3
      ExplicitLeft = 491
      ExplicitTop = 194
    end
    object rgVidDogovor: TcxDBRadioGroup
      Left = 85
      Top = 18
      TabStop = False
      Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      DataBinding.DataField = 'VID'
      DataBinding.DataSource = dm.dsVidoviDogovori
      Properties.Columns = 3
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1090#1086#1082#1080
          Value = 1
        end
        item
          Caption = #1059#1089#1083#1091#1075#1080
          Value = 2
        end
        item
          Caption = #1056#1072#1073#1086#1090#1080
          Value = 3
        end>
      Properties.OnEditValueChanged = rgVidDogovorPropertiesEditValueChanged
      TabOrder = 0
      Height = 51
      Width = 204
    end
    object Opis: TcxDBMemo
      Left = 85
      Top = 102
      Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsVidoviDogovori
      Properties.WantReturns = False
      TabOrder = 2
      OnDblClick = OpisDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 86
      Width = 388
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 663
    ExplicitWidth = 663
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 588
    Width = 663
    ExplicitTop = 588
    ExplicitWidth = 663
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 264
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40921.378920219900000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.AnimationDelay = 300
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
