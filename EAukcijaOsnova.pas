unit EAukcijaOsnova;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, Vcl.Menus, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, Data.DB, cxDBData, Vcl.ComCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.StdCtrls, cxButtons, cxMemo, cxRichEdit,
  cxDBRichEdit, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  Vcl.ExtCtrls, Vcl.ActnList, cxLabel, cxDBLabel, dxSkinOffice2013White,
  cxNavigator, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmEAukcijaOsnova = class(TForm)
    ActionList1: TActionList;
    aGenerirajCeni_1: TAction;
    aGenerirajCeni_2: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aNov: TAction;
    PanelNovaAukcija: TPanel;
    PanelDetalen: TPanel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DATUM: TcxDBDateEdit;
    BROJ: TcxDBTextEdit;
    ZABELESKA: TcxDBRichEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    PanelTabela: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    MainMenu1: TMainMenu;
    StatusBar1: TStatusBar;
    aEAukcija: TAction;
    Panel1: TPanel;
    Label8: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    aBrisi: TAction;
    aAzuriraj: TAction;
    N5: TMenuItem;
    aPListaSitePonudiSoBod: TAction;
    aPListaPonudiSoBodPoGrupaArtikal: TAction;
    N6: TMenuItem;
    N7: TMenuItem;
    aDListaSitePonudiSoBod: TAction;
    aDListaPonudiSoBodPoGrupaArtikal: TAction;
    PopupMenu1: TPopupMenu;
    N8: TMenuItem;
    N9: TMenuItem;
    aPopUp: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aEAukcijaExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure ZABELESKADblClick(Sender: TObject);
    procedure aPecatiCeniPredIPotoaExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure aBrisiExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aPListaSitePonudiSoBodExecute(Sender: TObject);
    procedure aPListaPonudiSoBodPoGrupaArtikalExecute(Sender: TObject);
    procedure aDListaSitePonudiSoBodExecute(Sender: TObject);
    procedure aPopUpExecute(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure aDListaPonudiSoBodPoGrupaArtikalExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEAukcijaOsnova: TfrmEAukcijaOsnova;

implementation

{$R *.dfm}

uses dmUnit, Utils, eAukcija, Notepad, dmKonekcija, dmResources;

procedure TfrmEAukcijaOsnova.aAzurirajExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)then
  begin
    PanelDetalen.Enabled:=True;
    PanelTabela.Enabled:=false;

    Broj.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmEAukcijaOsnova.aBrisiExecute(Sender: TObject);
begin
if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmEAukcijaOsnova.aDListaPonudiSoBodPoGrupaArtikalExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40034);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmEAukcijaOsnova.aDListaSitePonudiSoBodExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40035);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmEAukcijaOsnova.aEAukcijaExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
       begin
          if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
             begin
                frmAukcija:=TfrmAukcija.Create(Application);
                frmAukcija.ShowModal;
                frmAukcija.Free;
              end
          else ShowMessage('������ ������������ � - ������ !!!');
       end
     else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmEAukcijaOsnova.aNovExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse)then
  begin
    PanelDetalen.Enabled:=True;
    PanelTabela.Enabled:=false;

    cxGrid1DBTableView1.DataController.DataSet.Insert;
    DATUM.SetFocus;
    dm.tblEAukcijaDATUM.Value:=Now;
    dm.tblEAukcijaBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

procedure TfrmEAukcijaOsnova.aOtkaziExecute(Sender: TObject);
begin
 if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalen);
      PanelDetalen.Enabled:=false;
      PanelTabela.Enabled:=true;
      cxGrid1.SetFocus;
  end;
end;

procedure TfrmEAukcijaOsnova.aPecatiCeniPredIPotoaExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40016);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblPonudiTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblPonudiPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmEAukcijaOsnova.aPListaPonudiSoBodPoGrupaArtikalExecute(
  Sender: TObject);
begin
try
    dmRes.Spremi('JN',40034);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('e_aukcija').Value:=dm.tblEAukcijaID.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Datum', QuotedStr(dateToStr(dm.tblEAukcijaDATUM.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmEAukcijaOsnova.aPListaSitePonudiSoBodExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40035);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblPonudiBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('e_aukcija').Value:=dm.tblEAukcijaID.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Datum', QuotedStr(dateToStr(dm.tblEAukcijaDATUM.Value)));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmEAukcijaOsnova.aPopUpExecute(Sender: TObject);
begin
     PopupMenu1.Popup(500,500);
end;

procedure TfrmEAukcijaOsnova.aZapisiExecute(Sender: TObject);
var  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDetalen) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        PanelDetalen.Enabled:=false;
        PanelTabela.Enabled:=true;
        cxGrid1.SetFocus;
    end;
  end;
end;

procedure TfrmEAukcijaOsnova.FormCreate(Sender: TObject);
begin
     dm.tblEAukcija.Close;
     dm.tblEAukcija.Open;
end;

procedure TfrmEAukcijaOsnova.N10Click(Sender: TObject);
begin
     dmRes.Spremi('JN',40034);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmEAukcijaOsnova.ZABELESKADblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblEAukcija.CreateBlobStream(dm.tblEAukcijaZABELESKA , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblEAukcijaZABELESKA as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmEAukcijaOsnova.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmEAukcijaOsnova.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEAukcijaOsnova.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;
end.
