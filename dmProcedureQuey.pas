unit dmProcedureQuey;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 10.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}


interface

uses
  SysUtils, Classes, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDatabase,
  pFIBDatabase,  Variants;

type
  TdmPQ = class(TDataModule)
    TransakcijaP: TpFIBTransaction;
    pInsStavkiVoPlan: TpFIBStoredProc;
    pMaxVidDogovor: TpFIBStoredProc;
    pCountOdlukiPoGodina: TpFIBStoredProc;
    pPredlogPlan: TpFIBStoredProc;
    qInsertStavkiVoTender: TpFIBQuery;
    qInsertStavkiREVoTender: TpFIBQuery;
    qInsertNedeliviGrupiTender: TpFIBQuery;
    pGrupnoVrednuvanjeStavkiPonudi: TpFIBStoredProc;
    pInsertArtikal: TpFIBStoredProc;
    pInsertDobitnici: TpFIBStoredProc;
    qCountPonudiZaTender: TpFIBQuery;
    pInsertStavkiDogovor: TpFIBStoredProc;
    qTipBaranje: TpFIBQuery;
    pDinamikaRealizacija: TpFIBStoredProc;
    pBodiranjeArt: TpFIBStoredProc;
    qDeleteDokumetTender: TpFIBQuery;
    pKopirajStavkiOdPlanVoNabavka: TpFIBStoredProc;
    pPopolniPrvaCena: TpFIBStoredProc;
    peAukcija: TpFIBStoredProc;
    pKopirajPotrebniDokVoTender: TpFIBStoredProc;
    qUpdateDostavenDokumentPonuda: TpFIBQuery;
    qUpdateValidnostTenderStavka: TpFIBQuery;
    pPrevzemiKriteriumi: TpFIBStoredProc;
    pZatvoriTenderi: TpFIBStoredProc;
    qRakovoditelSektor: TpFIBQuery;
    PROC_JN_PREVZEMI_PONISTENI: TpFIBStoredProc;
    qDeleteStavkiTender: TpFIBQuery;
    qDaliEVrska: TpFIBQuery;
    qDeleteStavkREiTender: TpFIBQuery;
    qSetupBrojSluzbenVesnik: TpFIBQuery;
    qBrisiStavkiGrupi: TpFIBQuery;
    qCountPonisteniStavkiTender: TpFIBQuery;
    qCountPonudiStavkiZaTender: TpFIBQuery;
    qCountKriteriumiBodTender: TpFIBQuery;
    pPrevzemiNazivArtikal: TpFIBStoredProc;
    pInsertDogovorStavkiAvtomatski: TpFIBStoredProc;
    pBrojPostapka: TpFIBStoredProc;
    procedure insert12(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,p12, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,v12: Variant);
    function zemiRezultat5(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):Variant;
    procedure insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);

 function zemiRezultat5Str(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):String; private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmPQ: TdmPQ;

implementation

uses dmKonekcija;

{$R *.dfm}

procedure TdmPQ.insert12(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, p7,P8,p9,p10,p11,p12, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,v12 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         if(not VarIsNull(p7)) then
           Proc.ParamByName(VarToStr(p7)).Value := v7;
         if(not VarIsNull(p8)) then
           Proc.ParamByName(VarToStr(p8)).Value := v8;
         if(not VarIsNull(p9)) then
           Proc.ParamByName(VarToStr(p9)).Value := v9;
         if(not VarIsNull(p10)) then
           Proc.ParamByName(VarToStr(p10)).Value := v10;
         if(not VarIsNull(p11)) then
           Proc.ParamByName(VarToStr(p11)).Value := v11;
         if(not VarIsNull(p12)) then
           Proc.ParamByName(VarToStr(p12)).Value := v12;
         Proc.ExecProc;
    end;
procedure TdmPQ.insert6(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,p6, v1, v2, v3, v4, v5, v6 : Variant);
    var
      ret : Extended;
    begin
         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         if(not VarIsNull(p6)) then
           Proc.ParamByName(VarToStr(p6)).Value := v6;
         Proc.ExecProc;
    end;

function TdmPQ.zemiRezultat5(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):Variant;
    var
      ret : Extended;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := 0
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

function TdmPQ.zemiRezultat5Str(Proc : TpFIBStoredProc; p1,  p2, p3, p4, p5,v1, v2, v3,v4, v5,broj : Variant):String;
    var
      ret : String;
    begin

         if(not VarIsNull(p1)) then
           Proc.ParamByName(VarToStr(p1)).Value := v1;
         if(not VarIsNull(p2)) then
           Proc.ParamByName(VarToStr(p2)).Value := v2;
         if(not VarIsNull(p3)) then
           Proc.ParamByName(VarToStr(p3)).Value := v3;
         if(not VarIsNull(p4)) then
           Proc.ParamByName(VarToStr(p4)).Value := v4;
         if(not VarIsNull(p5)) then
           Proc.ParamByName(VarToStr(p5)).Value := v5;
         Proc.ExecProc;
         if(Proc.ParamByName(broj).Value = Null) then
           ret := '0'
         else
           ret := Proc.ParamByName(broj).Value;
         result := ret;
    end;

end.
