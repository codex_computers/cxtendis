unit Baranja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxRichEdit, cxPCdxBarPopupMenu, cxDBRichEdit, cxLabel, cxDBLabel, cxSplitter,
  dxCustomHint, cxHint, cxImageComboBox, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxBarBuiltInMenu, FIBQuery, pFIBQuery, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmBaranja = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelDetail: TPanel;
    Panel1: TPanel;
    Label12: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    PanelMaster: TPanel;
    cxPageControlBaranja: TcxPageControl;
    cxTabSheetTabelaren: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheetDetalen: TcxTabSheet;
    PanelDetalen: TPanel;
    cxGroupBox3: TcxGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    BROJ: TcxDBTextEdit;
    DATUM: TcxDBDateEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1RENAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    Label1: TLabel;
    RE_Naziv: TcxDBLookupComboBox;
    ZABELESKA: TcxDBMemo;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid2DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid2DBTableView1MK: TcxGridDBColumn;
    cxGrid2DBTableView1EN: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1RENAZIV: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavka: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    aPodesuvanjePecatenjeStavki: TAction;
    aPageSetupStavki: TAction;
    aSnimiPecatenjeStavki: TAction;
    aBrisiPodesuvanjePecatenjeStavki: TAction;
    aSnimiIzgledStavki: TAction;
    aBrisiIzgledStavki: TAction;
    aZacuvajExcelStavki: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aPecatiStavki: TAction;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    RE: TcxDBTextEdit;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxHintStyleController1: TcxHintStyleController;
    aBrisiStavka: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    aRefreshStavki: TAction;
    BROJ_TENDER: TcxDBLookupComboBox;
    Label3: TLabel;
    Label5: TLabel;
    cxSplitter1: TcxSplitter;
    dxBarLargeButton29: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton30: TdxBarLargeButton;
    aPBaranjeZaIsporaka: TAction;
    BROJ_DOGOVOR: TcxDBLookupComboBox;
    PopupMenu2: TPopupMenu;
    aDBaranjeZaIsporaka: TAction;
    aPopUpMenu: TAction;
    N2: TMenuItem;
    cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    qAktivenDogovor: TpFIBQuery;
    dxBarLargeButton31: TdxBarLargeButton;
    aPBaranjaZaArtikal: TAction;
    aDBaranjaZaArtikal: TAction;
    N3: TMenuItem;
    cxGrid2DBTableView1STAVKANAZIV: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure ZABELESKADblClick(Sender: TObject);
    procedure aDodadiStavkaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aPageSetupStavkiExecute(Sender: TObject);
    procedure aSnimiPecatenjeStavkiExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aZacuvajExcelStavkiExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiExecute(Sender: TObject);
    procedure dxBarLargeButton24Click(Sender: TObject);
    procedure aBrisiIzgledStavkiExecute(Sender: TObject);
    procedure aPecatiStavkiExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aBrisiStavkaExecute(Sender: TObject);
    procedure aRefreshStavkiExecute(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure aPBaranjeZaIsporakaExecute(Sender: TObject);
    procedure aDBaranjeZaIsporakaExecute(Sender: TObject);
    procedure aPopUpMenuExecute(Sender: TObject);
    procedure BROJ_TENDERPropertiesEditValueChanged(Sender: TObject);
    procedure aPBaranjaZaArtikalExecute(Sender: TObject);
    procedure aDBaranjaZaArtikalExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmBaranja: TfrmBaranja;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, Notepad,
  IzborStavkiBaranje, dmProcedureQuey;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmBaranja.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmBaranja.aNovExecute(Sender: TObject);
begin
if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
begin
 if ((Tag = 0) or (((tag = 1)or (Tag=2))and (dm.tblTenderSTATUS.Value=1))) then
  begin
    if ((Tag = 0) or (((tag = 1)or (Tag=2))and (dm.tblTenderZATVOREN.Value<>1))) then
       begin
         if ((Tag = 0) or (((tag = 1)or (Tag=2))and (dm.tblTenderPONISTEN.Value=1))) then
          begin
          cxPageControlBaranja.ActivePage:=cxTabSheetDetalen;
          PanelDetalen.Enabled:=true;
          cxGrid1DBTableView1.DataController.DataSet.Insert;
          if tag = 2 then
            begin
               qAktivenDogovor.Close;
               qAktivenDogovor.ParamByName('broj_dogovor').Value:= dm.tblDogovoriBROJ_DOGOVOR.Value;
               qAktivenDogovor.ExecQuery;

               if qAktivenDogovor.FldByName['aktiven'].Value = 0 then
                  begin
                    ShowMessage('������� �� ������� �� ��������� � ������� !!!');
                    BROJ_DOGOVOR.Clear;
                  end
               else
                  begin
                    BROJ.SetFocus;
                    dm.tblBaranjaBROJ_TENDER.Value:=dm.tblDogovoriBROJ_TENDER.Value;
                    dm.tblBaranjaBROJ_DOGOVOR.Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
                    dm.tblBaranjaPARTNER.Value:=dm.tblDogovoriPARTNER.Value;
                    dm.tblBaranjaTIP_PARTNER.Value:=dm.tblDogovoriTIP_PARTNER.Value;
                  end
            end
          else if tag = 1 then
            begin
              BROJ_DOGOVOR.SetFocus;
              dm.tblBaranjaBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
            end
          else if tag = 0 then
              BROJ_TENDER.SetFocus;
          end
         else  ShowMessage('���������� �� ����� ������� � ��������� !!!')
       end
     else  ShowMessage('���������� �� ����� ������� � �������� !!!')
  end
 else ShowMessage('���������� �� ����� ������� � ��������� !!!')
end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');

end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmBaranja.aAzurirajExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
    begin
      dm.qTenderStatus.Close;
      dm.qTenderStatus.ParamByName('broj').Value:=BROJ_TENDER.EditValue;
      dm.qTenderStatus.ExecQuery;
      if (((Tag = 0)and(dm.qTenderStatus.FldByName['status'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderSTATUS.Value=1))) then
        begin
          if ((((Tag = 0)and (dm.qTenderStatus.FldByName['zatvoren'].Value<>1))) or (((tag = 1)or (Tag=2))and (dm.tblTenderZATVOREN.Value<>1))) then
            begin
              if (((Tag = 0)and (dm.qTenderStatus.FldByName['ponisten'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderPONISTEN.Value=1))) then
                begin
                  cxPageControlBaranja.ActivePage:=cxTabSheetDetalen;
                  PanelDetalen.Enabled:=true;
                  BROJ.SetFocus;
                  cxGrid1DBTableView1.DataController.DataSet.Edit;
                  if (Tag = 1) then BROJ_DOGOVOR.SetFocus
                  else if (tag = 0) then BROJ_TENDER.SetFocus;
                end
              else  ShowMessage('���������� �� ����� ������� � ��������� !!!')
            end
          else  ShowMessage('���������� �� ����� ������� � ��������!!!')
        end
      else ShowMessage('���������� �� ����� ������� � ��������� !!!');
    end
 else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmBaranja.aPBaranjeZaIsporakaExecute(Sender: TObject);
begin
    dmpq.qRakovoditelSektor.Close;
    dmpq.qRakovoditelSektor.ParamByName('re').Value:=dm.tblBaranjaRE.Value;
    dmpq.qRakovoditelSektor.ExecQuery;
  try
   if dmpq.qRakovoditelSektor.FldByName['rakovoditel'].Value <>  Null then
    begin
    if dm.tblBaranjaTIP_BARANJA.Value = 0 then
        dmRes.Spremi('JN',40030)
    else
        dmRes.Spremi('JN',40040);
    dmKon.tblSqlReport.ParamByName('baranje').Value:=dm.tblBaranjaBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

    dmRes.frxReport1.Variables.AddVariable('VAR', 'rakovoditelNaziv', QuotedStr(dmpq.qRakovoditelSektor.FldByName['rakovoditel'].Value));
    dmRes.frxReport1.ShowReport();
    end
   else  ShowMessage('������ ������������ ����������� !');
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmBaranja.aPBaranjaZaArtikalExecute(Sender: TObject);
begin
   try

    dmRes.Spremi('JN',40055);
    dmKon.tblSqlReport.ParamByName('artikal').Value:=dm.tblBaranjaStavkiARTIKAL.Value;
    dmKon.tblSqlReport.ParamByName('artvid').Value:=dm.tblBaranjaStavkiVID_ARTIKAL.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
   // dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));

   // dmRes.frxReport1.Variables.AddVariable('VAR', 'rakovoditelNaziv', QuotedStr(dmpq.qRakovoditelSektor.FldByName['rakovoditel'].Value));
    dmRes.frxReport1.ShowReport();

  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmBaranja.aBrisiExecute(Sender: TObject);
begin
 dm.qTenderStatus.Close;
 dm.qTenderStatus.ParamByName('broj').Value:=BROJ_TENDER.EditValue;
 dm.qTenderStatus.ExecQuery;
 if(((Tag = 0)and(dm.qTenderStatus.FldByName['status'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderSTATUS.Value=1))) then
   begin
      if (((Tag = 0)and (dm.qTenderStatus.FldByName['zatvoren'].Value<>1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderZATVOREN.Value<>1))) then
         begin
           if (((Tag = 0)and (dm.qTenderStatus.FldByName['ponisten'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderPONISTEN.Value=1))) then
              begin
               if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
                   cxGrid1DBTableView1.DataController.DataSet.Delete();
              end
           else  ShowMessage('���������� �� ����� ������� � ��������� !!!')
         end
      else ShowMessage('���������� �� ����� ������� � �������� !!!')
    end
 else ShowMessage('���������� �� ����� ������� � ��������� !!!');
end;

procedure TfrmBaranja.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmBaranja.aBrisiIzgledStavkiExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid2DBTableView1);
     BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmBaranja.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmBaranja.aRefreshStavkiExecute(Sender: TObject);
begin
    cxGrid2DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
    cxGrid2DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmBaranja.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmBaranja.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmBaranja.aSnimiIzgledStavkiExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
     ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmBaranja.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, '������ �� ��������');
end;

procedure TfrmBaranja.aZacuvajExcelStavkiExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������ �� ������ �� ��������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmBaranja.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmBaranja.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmBaranja.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
  begin
    if (Sender = BROJ_DOGOVOR) and (BROJ_DOGOVOR.Text <> '') and ((tag = 0)or(tag =1)) then
       begin
         dm.tblBaranjaPARTNER.Value:=dm.tblIzborBrojDogovorZaBaranjePARTNER.Value;
         dm.tblBaranjaTIP_PARTNER.Value:=dm.tblIzborBrojDogovorZaBaranjeTIP_PARTNER.Value;
       end;

    if (Sender = BROJ_DOGOVOR) and (BROJ_DOGOVOR.Text <> '') then
       begin
           qAktivenDogovor.Close;
           qAktivenDogovor.ParamByName('broj_dogovor').Value:= BROJ_DOGOVOR.EditValue;
           qAktivenDogovor.ExecQuery;

           if qAktivenDogovor.FldByName['aktiven'].Value = 0 then
           begin
             ShowMessage('������� �� ������� �� ��������� � ������� !!!');
             BROJ_DOGOVOR.Clear;
           end;
       end;
  end;
end;

procedure TfrmBaranja.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmBaranja.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmBaranja.ZABELESKADblClick(Sender: TObject);
begin
     frmNotepad :=TfrmNotepad.Create(Application);
     frmnotepad.LoadText(dm.tblBaranjaZABELESKA.Value);
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
          dm.tblBaranjaZABELESKA.Value := frmNotepad.ReturnText;
        end;
     frmNotepad.Free;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmBaranja.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmBaranja.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmBaranja.prefrli;
begin
end;

procedure TfrmBaranja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(PanelDetalen) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmBaranja.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmBaranja.FormDblClick(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TfrmBaranja.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    caption:='������ �� ��������';
    cxPageControlBaranja.ActivePage:=cxTabSheetTabelaren;
    cxTabSheetTabelaren.Caption:='��������� ������ �� ������ �� ��������';

    dm.tblIzborBrojTenderZaBaranje.close;
    dm.tblIzborBrojDogovorZaBaranje.close;
    dm.tblBaranja.close;
    dm.tblReBaranja.Close;
    if tag = 2 then
       begin
         dm.tblIzborBrojTenderZaBaranje.ParamByName('broj_tender').Value:=dm.tblDogovoriBROJ_TENDER.Value;
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_dogovor').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_tender').Value:=dm.tblDogovoriBROJ_TENDER.Value;
         dm.tblBaranja.ParamByName('broj_tender').Value:= dm.tblDogovoriBROJ_TENDER.Value;
         dm.tblBaranja.ParamByName('broj_dogovor').Value:= dm.tblDogovoriBROJ_DOGOVOR.Value;
         dm.tblReBaranja.ParamByName('broj_tender').Value:= dm.tblDogovoriBROJ_TENDER.Value;
       end
    else if tag = 1  then
       begin
         dm.tblIzborBrojTenderZaBaranje.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_dogovor').Value:='%';
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
         dm.tblBaranja.ParamByName('broj_tender').Value:= dm.tblTenderBROJ.Value;
         dm.tblBaranja.ParamByName('broj_dogovor').Value:= '%';
         dm.tblReBaranja.ParamByName('broj_tender').Value:= dm.tblTenderBROJ.Value;
       end
    else
       begin
         dm.tblIzborBrojTenderZaBaranje.ParamByName('broj_tender').Value:='%';
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_dogovor').Value:='%';
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_tender').Value:='%';
         dm.tblBaranja.ParamByName('broj_tender').Value:= '%';
         dm.tblBaranja.ParamByName('broj_dogovor').Value:= '%';
         dm.tblReBaranja.ParamByName('broj_tender').Value:='%';
       end;

    dm.tblIzborBrojTenderZaBaranje.Open;
    dm.tblIzborBrojDogovorZaBaranje.Open;

    dm.tblReBaranja.ParamByName('username').Value:=dmKon.user;
    dm.tblReBaranja.ParamByName('APP').Value:=dmKon.aplikacija;

    dm.tblBaranja.ParamByName('username').Value:=dmKon.user;
    dm.tblBaranja.ParamByName('APP').Value:=dmKon.aplikacija;
    if dmKon.prava <> 0 then
     begin
        dm.tblBaranja.ParamByName('param').Value:=1;
        dm.tblReBaranja.ParamByName('param').Value:=1;
     end
    else
     begin
        dm.tblBaranja.ParamByName('param').Value:=0;
        dm.tblReBaranja.ParamByName('param').Value:=0;
     end;

    if ((Tag = 1) or (tag= 2)) then
       begin
          dm.qTenderStatus.Close;
          dm.qTenderStatus.ParamByName('broj').Value:=dm.tblTenderBROJ.Value;
          dm.qTenderStatus.ExecQuery;
       end;

    dm.tblBaranja.Open;
    dm.tblReBaranja.Open;

    dm.tblBaranjaStavki.close;
    dm.tblBaranjaStavki.Open;

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;
//------------------------------------------------------------------------------

procedure TfrmBaranja.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmBaranja.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmBaranja.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
   if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmBaranja.dxBarLargeButton24Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmBaranja.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDetalen) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        PanelDetalen.Enabled:=false;
        cxPageControlBaranja.ActivePage:=cxTabSheetTabelaren;
        cxGrid1.SetFocus;
     end;
  end;
end;

procedure TfrmBaranja.BROJ_TENDERPropertiesEditValueChanged(Sender: TObject);
begin
     if (tag = 0) and (BROJ_TENDER.Text <> '')and ((cxGrid1DBTableView1.DataController.DataSource.State in [dsEdit,dsInsert])) then
       begin
         dm.tblIzborBrojDogovorZaBaranje.Close;
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_tender').Value:=BROJ_TENDER.EditValue;
         dm.tblIzborBrojDogovorZaBaranje.ParamByName('broj_dogovor').Value:= '%';
         dm.tblIzborBrojDogovorZaBaranje.Open;
         dm.tblReBaranja.Close;
         dm.tblReBaranja.ParamByName('broj_tender').Value:= BROJ_TENDER.EditValue;
         dm.tblReBaranja.Open;
         dm.tblBaranjaBROJ_DOGOVOR.Clear;
         dm.tblBaranjaRE.Clear;
       end;
end;

//	����� �� ���������� �� �������
procedure TfrmBaranja.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalen);
      PanelDetalen.Enabled:=false;
      cxPageControlBaranja.ActivePage:=cxTabSheetTabelaren;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmBaranja.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmBaranja.aPageSetupStavkiExecute(Sender: TObject);
begin
   dxComponentPrinter1Link2.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmBaranja.aPecatiStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������ �� ������ �� ��������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� �� ������ �� ��������: ' + dm.tblBaranjaBROJ.Value);


  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmBaranja.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '������ �� ��������';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  if tag = 2 then
     dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('��� �� ������� �� ����� �������: ' + dm.tblDogovoriBROJ_DOGOVOR.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;


procedure TfrmBaranja.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmBaranja.aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmBaranja.aPopUpMenuExecute(Sender: TObject);
begin
      PopupMenu2.Popup(500,500);
end;

procedure TfrmBaranja.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmBaranja.aSnimiPecatenjeStavkiExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmBaranja.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmBaranja.aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmBaranja.aBrisiStavkaExecute(Sender: TObject);
begin
   dm.qTenderStatus.Close;
   dm.qTenderStatus.ParamByName('broj').Value:=BROJ_TENDER.EditValue;
   dm.qTenderStatus.ExecQuery;
  if (((Tag = 0)and(dm.qTenderStatus.FldByName['status'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderSTATUS.Value=1))) then
    begin
      if (((Tag = 0)and (dm.qTenderStatus.FldByName['zatvoren'].Value<>1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderZATVOREN.Value<>1) )) then
         begin
           if (((Tag = 0)and (dm.qTenderStatus.FldByName['ponisten'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderPONISTEN.Value=1))) then
              begin
                if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and(cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
                    cxGrid2DBTableView1.DataController.DataSet.Delete();
              end
           else  ShowMessage('���������� �� ����� ������� � ��������� !!!')
         end
      else ShowMessage('���������� �� ����� ������� � �������� !!!')
    end
  else ShowMessage('���������� �� ����� ������� � ��������� !!!');
end;

procedure TfrmBaranja.aDBaranjaZaArtikalExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40055);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmBaranja.aDBaranjeZaIsporakaExecute(Sender: TObject);
begin
     if dm.tblBaranjaTIP_BARANJA.Value = 0 then
        dmRes.Spremi('JN',40030)
     else
        dmRes.Spremi('JN',40040);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmBaranja.aDodadiStavkaExecute(Sender: TObject);
begin
      dm.qTenderStatus.Close;
      dm.qTenderStatus.ParamByName('broj').Value:=BROJ_TENDER.EditValue;
      dm.qTenderStatus.ExecQuery;
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
        begin
          if (((Tag = 0)and(dm.qTenderStatus.FldByName['status'].Value=1))or(((tag = 1)or (Tag=2))and (dm.tblTenderSTATUS.Value=1))) then
             begin
               if (((Tag = 0)and (dm.qTenderStatus.FldByName['zatvoren'].Value<>1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderZATVOREN.Value<>1))) then
                  begin
                    if (((Tag = 0)and (dm.qTenderStatus.FldByName['ponisten'].Value=1)) or (((tag = 1)or (Tag=2))and (dm.tblTenderPONISTEN.Value=1))) then
                      begin
                        frmIzborStavkiBaranja:=TfrmIzborStavkiBaranja.Create(self,false);
                        frmIzborStavkiBaranja.ShowModal;
                        frmIzborStavkiBaranja.Free;
                      end
                    else  ShowMessage('���������� �� ����� ������� � ��������� !!!')
                  end
                else ShowMessage('���������� �� ����� ������� � �������� !!!')
             end
          else ShowMessage('���������� �� ����� ������� � ��������� !!!');
        end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmBaranja.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmBaranja.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmBaranja.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
