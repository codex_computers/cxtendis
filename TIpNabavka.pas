unit TIpNabavka;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 13.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxBlobEdit, cxMemo,
  dxCustomHint, cxHint, cxGroupBox, cxRadioGroup, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, cxCheckBox;

type
  TfrmTipNabavka = class(TfrmMaster)
    cxGrid1DBTableView1SIFRA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DENOVI_ZALBA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    NAZIV: TcxDBTextEdit;
    Label2: TLabel;
    Label4: TLabel;
    OPIS: TcxDBMemo;
    Label3: TLabel;
    DENOVI_ZALBA: TcxDBTextEdit;
    cxHintStyleController1: TcxHintStyleController;
    cxDBRadioGroup1: TcxDBRadioGroup;
    cxGrid1DBTableView1TIP_GOD_PLAN: TcxGridDBColumn;
    cxGrid1DBTableView1tip_god_plan_Naziv: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    AKTIVEN: TcxDBCheckBox;
    Label5: TLabel;
    ROK_DOGOVOR: TcxDBTextEdit;
    cxGrid1DBTableView1ROK_DOGOVOR: TcxGridDBColumn;
    procedure OPISDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipNabavka: TfrmTipNabavka;

implementation

uses dmUnit, Notepad;

{$R *.dfm}

procedure TfrmTipNabavka.OPISDblClick(Sender: TObject);
begin
  inherited;
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dm.tblTipNabavkaOPIS.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
     begin
       dm.tblTipNabavkaOPIS.Value := frmNotepad.ReturnText;
     end;
 frmNotepad.Free;
end;

end.
