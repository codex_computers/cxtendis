object frmAzurirajStavkiNabavka: TfrmAzurirajStavkiNabavka
  Left = 0
  Top = 0
  Align = alClient
  Caption = #1040#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1072#1074#1082#1072
  ClientHeight = 607
  ClientWidth = 1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 49
    Width = 1077
    Height = 558
    Align = alClient
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      OnKeyPress = cxGrid1DBTableView1KeyPress
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsStavkiNabavka
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsBehavior.IncSearch = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.CellAutoHeight = True
      OptionsView.ColumnAutoWidth = True
      Styles.Content = dmRes.cxStyle303
      object cxGrid1DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'VIDARTIKALNAZIV'
        Options.Editing = False
        Options.AutoWidthSizable = False
        Width = 53
      end
      object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
        DataBinding.FieldName = 'ARTIKAL'
        Options.Editing = False
        Options.AutoWidthSizable = False
        Width = 66
      end
      object cxGrid1DBTableView1SIF_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'SIF_NAZIV'
        Options.Editing = False
        Width = 301
      end
      object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        PropertiesClassName = 'TcxRichEditProperties'
        Width = 291
      end
      object cxGrid1DBTableView1SIF_MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'SIF_MERKA'
        Options.Editing = False
        Options.AutoWidthSizable = False
        Width = 176
      end
      object cxGrid1DBTableView1MERKA: TcxGridDBColumn
        DataBinding.FieldName = 'MERKA'
        Options.IncSearch = False
        Options.AutoWidthSizable = False
        Width = 137
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1077
    Height = 49
    Align = alTop
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 32
      Top = 12
      Width = 305
      Height = 25
      Action = aPrevzemiStavki
      OptionsImage.ImageIndex = 50
      TabOrder = 0
    end
  end
  object tblStavkiNabavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JN_TENDER_STAVKI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    MERKA = :MERKA'
      'WHERE'
      '    ARTIKAL = :OLD_ARTIKAL'
      '    and VID_ARTIKAL = :OLD_VID_ARTIKAL'
      '    and broj_tender = :OLD_broj_tender'
      '    ')
    SelectSQL.Strings = (
      'select ts.naziv,'
      '       ts.merka,'
      '       ma.naziv as sif_naziv,'
      '       ma.merka as sif_merka,'
      '       ts.artikal,'
      '       ts.vid_artikal,'
      '       ta.naziv as vidArtikalNaziv'
      'from jn_tender_stavki ts'
      
        'inner join mtr_artikal ma on ma.artvid = ts.vid_artikal and ma.i' +
        'd = ts.artikal'
      'inner join jn_tip_artikal ta on ta.id = ma.jn_tip_artikal'
      'where ts.broj_tender = :broj_tender')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 232
    Top = 88
    object tblStavkiNabavkaNAZIV: TFIBStringField
      DisplayLabel = #1053#1086#1074' '#1085#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1072#1074#1082#1072
      FieldName = 'NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiNabavkaMERKA: TFIBStringField
      DisplayLabel = #1053#1086#1074#1072' '#1084#1077#1088#1082#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1072
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiNabavkaSIF_NAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1086#1076' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
      FieldName = 'SIF_NAZIV'
      Size = 10000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiNabavkaSIF_MERKA: TFIBStringField
      DisplayLabel = #1052#1077#1088#1082#1072' '#1086#1076' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
      FieldName = 'SIF_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStavkiNabavkaARTIKAL: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ARTIKAL'
    end
    object tblStavkiNabavkaVID_ARTIKAL: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID_ARTIKAL'
    end
    object tblStavkiNabavkaVIDARTIKALNAZIV: TFIBStringField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VIDARTIKALNAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStavkiNabavka: TDataSource
    DataSet = tblStavkiNabavka
    Left = 336
    Top = 88
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 544
    Top = 216
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 440
    Top = 8
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072
      ImageIndex = 11
      ShortCut = 119
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
    end
    object aSifArtikli: TAction
      Caption = 'aSifArtikli'
      ShortCut = 16449
    end
    object aDodadiNedelivaGrupa: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 90
    end
    object aBrisiNedelivaGrupa: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 91
    end
    object aOkNedeliviGrupi: TAction
      Caption = #1054#1050
      ImageIndex = 6
    end
    object aKopirajStavkiOdPlan: TAction
      Caption = 'aKopirajStavkiOdPlan'
      ShortCut = 16464
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
    end
    object aBrisiStavkiGrupa: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1080' '#1087#1086' '#1075#1088#1091#1087#1072
      ImageIndex = 46
    end
    object aOKBrisiStavkiGrupa: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1080' '#1087#1086' '#1075#1088#1091#1087#1072
      ImageIndex = 46
    end
    object aAzurirajNazivMerka: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1072#1079#1080#1074' '#1080' '#1084#1077#1088#1082#1072
      ImageIndex = 12
    end
    object aNotepad: TAction
      Caption = #1053#1086#1090#1077#1087#1072#1076
      ShortCut = 16429
      OnExecute = aNotepadExecute
    end
    object aPrevzemiStavki: TAction
      Caption = #1055#1088#1077#1074#1079#1077#1084#1080' '#1085#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1086#1076' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080
      OnExecute = aPrevzemiStavkiExecute
    end
  end
end
