unit Main;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 04.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, cxCheckBox, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxTrackBar, FIBQuery, pFIBQuery, dxSkinOffice2013White, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.ImageList,
  dxSkinscxPCPainter, cxImageList;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1BarGodisenPlan: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxRibbon1TabSifrarnici: TdxRibbonTab;
    dxBarLargeButton6: TdxBarLargeButton;
    aGodisenPlan: TAction;
    dxBarManager1Bar2: TdxBar;
    dxBarLargeButton7: TdxBarLargeButton;
    aSifArtikal: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aSifArtikalVid: TAction;
    aSifArtGrupa: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    aSifMerka: TAction;
    aSifProizvoditel: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton12: TdxBarLargeButton;
    aSifDrzavi: TAction;
    aSifValuta: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton14: TdxBarLargeButton;
    aKriteriumiZaBodiranje: TAction;
    aSifKomisija: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aTipNabavka: TAction;
    aVidoviDogovor: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aJavnaNabavka: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aArtikalGenerika: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aBaranjaZaIsporaka: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    aSifVidDokument: TAction;
    aSifPotrebniDokumenti: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    qPravaRe: TpFIBQuery;
    dxBarLargeButton23: TdxBarLargeButton;
    aSifPotrebniDokGrupa: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    aSifRabEdinici: TAction;
    dxBarLargeButton25: TdxBarLargeButton;
    aSifPartner: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    aSifTipPartner: TAction;
    aSifMesto: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    aTrebovanje: TAction;
    dxBarLargeButton29: TdxBarLargeButton;
    actSifPricinaPonistuvanje: TAction;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton30: TdxBarLargeButton;
    aSetup: TAction;
    dxRibbon1TabPreglediReporter: TdxRibbonTab;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    aSifKategorija: TAction;
    aSifCPV: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aGodisenPlanExecute(Sender: TObject);
    procedure aSifArtikalExecute(Sender: TObject);
    procedure aSifArtikalVidExecute(Sender: TObject);
    procedure aSifArtGrupaExecute(Sender: TObject);
    procedure aSifProizvoditelExecute(Sender: TObject);
    procedure aSifMerkaExecute(Sender: TObject);
    procedure aSifDrzaviExecute(Sender: TObject);
    procedure aSifValutaExecute(Sender: TObject);
    procedure aKriteriumiZaBodiranjeExecute(Sender: TObject);
    procedure aSifKomisijaExecute(Sender: TObject);
    procedure aTipNabavkaExecute(Sender: TObject);
    procedure aVidoviDogovorExecute(Sender: TObject);
    procedure aJavnaNabavkaExecute(Sender: TObject);
    procedure aArtikalGenerikaExecute(Sender: TObject);
    procedure aBaranjaZaIsporakaExecute(Sender: TObject);
    procedure aSifVidDokumentExecute(Sender: TObject);
    procedure aSifPotrebniDokumentiExecute(Sender: TObject);
    procedure aSifPotrebniDokGrupaExecute(Sender: TObject);
    procedure aSifRabEdiniciExecute(Sender: TObject);
    procedure aSifPartnerExecute(Sender: TObject);
    procedure aSifTipPartnerExecute(Sender: TObject);
    procedure aSifMestoExecute(Sender: TObject);
    procedure aTrebovanjeExecute(Sender: TObject);
    procedure actSifPricinaPonistuvanjeExecute(Sender: TObject);
    procedure aSetupExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aSifKategorijaExecute(Sender: TObject);
    procedure aSifCPVExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, GodisenPlan, MK, Artikal,
  cxConstantsMak, ArtVid, ArtGrupa, Merka, Proizvoditel, Drzava, Valuta, dmUnit,
  Kriteriumi, Komisija, TIpNabavka, VidoviDogovor, JavnaNabavka, ArtGenerika,
  Baranja, VidoviNaDokumenti, dmReportUnit, SifPotrebniDokumenti,
  PotrebniDokGrupi, ZatvoreniTenderi, dmProcedureQuey, RabotniEdinici, Partner,
  TipPartner, Mesto, Trebovanje, PriciniPonishtuvanje, Setup, ArtKategorija,
  JN_CPV, PostapkiPredIstekNaDogovor;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aSifArtGrupaExecute(Sender: TObject);
begin
     frmArtGrupa:=TfrmArtGrupa.Create(self,true);
     frmArtGrupa.ShowModal();
     frmArtGrupa.Free;
end;

procedure TfrmMain.aSifArtikalVidExecute(Sender: TObject);
begin
   frmArtVid:=TfrmArtVid.Create(self,true);
   frmArtVid.ShowModal();
   frmArtVid.Free;
end;

procedure TfrmMain.aSifDrzaviExecute(Sender: TObject);
begin
   frmDrzava:=TfrmDrzava.Create(self,true);
   frmDrzava.ShowModal();
   frmDrzava.Free;
end;

procedure TfrmMain.aSifKategorijaExecute(Sender: TObject);
begin
   frmArtKategorija:=TfrmArtKategorija.Create(self,true);
   frmArtKategorija.ShowModal();
   frmArtKategorija.Free;
end;

procedure TfrmMain.aSifKomisijaExecute(Sender: TObject);
begin
   frmKomisija:=TfrmKomisija.Create(self,true);
   frmKomisija.ShowModal();
   frmKomisija.Free;
end;

procedure TfrmMain.aSifMerkaExecute(Sender: TObject);
begin
   frmMerka:=TfrmMerka.Create(self,true);
   frmMerka.ShowModal();
   frmMerka.Free;
end;

procedure TfrmMain.aSifMestoExecute(Sender: TObject);
begin
     frmMesto:=TfrmMesto.Create(self,true);
     frmMesto.ShowModal();
     frmMesto.Free;
end;

procedure TfrmMain.aSifPotrebniDokumentiExecute(Sender: TObject);
begin
      frmPotrebniDokumenti:=TfrmPotrebniDokumenti.Create(self,true);
      frmPotrebniDokumenti.ShowModal();
      frmPotrebniDokumenti.Free;
end;

procedure TfrmMain.aSifProizvoditelExecute(Sender: TObject);
begin
   frmProizvoditel:=TfrmProizvoditel.Create(self,true);
   frmProizvoditel.ShowModal();
   frmProizvoditel.Free;
end;

procedure TfrmMain.aSifRabEdiniciExecute(Sender: TObject);
begin
     frmRE:=TfrmRE.Create(self,true);
     frmRE.ShowModal();
     frmRE.Free;
end;

procedure TfrmMain.aSifTipPartnerExecute(Sender: TObject);
begin
     frmTipPartner:=TfrmTipPartner.Create(self,true);
     frmTipPartner.ShowModal();
     frmTipPartner.Free;
end;

procedure TfrmMain.aSifValutaExecute(Sender: TObject);
begin
   frmValuta:=TfrmValuta.Create(self,true);
   frmValuta.ShowModal();
   frmValuta.Free;
end;

procedure TfrmMain.aSifVidDokumentExecute(Sender: TObject);
begin
     frmVidDokumet:=TfrmVidDokumet.Create(self,true);
     frmVidDokumet.ShowModal();
     frmVidDokumet.Free;
end;

procedure TfrmMain.aTipNabavkaExecute(Sender: TObject);
begin
   frmTipNabavka:=TfrmTipNabavka.Create(self,true);
   frmTipNabavka.ShowModal();
   frmTipNabavka.Free;
end;

procedure TfrmMain.aTrebovanjeExecute(Sender: TObject);
begin
      frmTrebovanje:=TfrmTrebovanje.Create(self,false);
      frmTrebovanje.ShowModal;
      frmTrebovanje.Free;
end;

procedure TfrmMain.aVidoviDogovorExecute(Sender: TObject);
begin
   frmVidoviDogovor:=TfrmVidoviDogovor.Create(self,true);
   frmVidoviDogovor.ShowModal();
   frmVidoviDogovor.Free;
end;

procedure TfrmMain.aArtikalGenerikaExecute(Sender: TObject);
begin
     frmArtGenerika:=TfrmArtGenerika.Create(self,false);
     frmArtGenerika.ShowModal;
     frmArtGenerika.Free;
end;

procedure TfrmMain.aBaranjaZaIsporakaExecute(Sender: TObject);
begin
      frmBaranja:=TfrmBaranja.Create(self,false);
      frmBaranja.ShowModal;
      frmBaranja.Free;
end;

procedure TfrmMain.actSifPricinaPonistuvanjeExecute(Sender: TObject);
begin
    frmPricinaPonistuvanje:=TfrmPricinaPonistuvanje.Create(self,true);
    frmPricinaPonistuvanje.ShowModal();
    frmPricinaPonistuvanje.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aGodisenPlanExecute(Sender: TObject);
begin
     frmGodisenPlan:=TfrmGodisenPlan.Create(self,false);
     frmGodisenPlan.ShowModal;
     frmGodisenPlan.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aJavnaNabavkaExecute(Sender: TObject);
begin
     frmJavnaNabavka:=TfrmJavnaNabavka.Create(Application);
     frmJavnaNabavka.ShowModal();
     frmJavnaNabavka.Free;
end;

procedure TfrmMain.aKriteriumiZaBodiranjeExecute(Sender: TObject);
begin
    frmKriteriumi:=TfrmKriteriumi.Create(self,true);
    frmKriteriumi.ShowModal();
    frmKriteriumi.Free;
end;

procedure TfrmMain.aSifPotrebniDokGrupaExecute(Sender: TObject);
begin
     frmPotrebniDokGrupi:=TfrmPotrebniDokGrupi.Create(self,true);
     frmPotrebniDokGrupi.ShowModal();
     frmPotrebniDokGrupi.Free;
end;

procedure TfrmMain.aSifPartnerExecute(Sender: TObject);
begin
     frmPartner:=TfrmPartner.Create(self,true);
     frmPartner.ShowModal();
     frmPartner.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSetupExecute(Sender: TObject);
begin
     frmSetup:=TfrmSetup.Create(self,false);
     frmSetup.ShowModal();
     frmSetup.Free;

     dmPQ.qSetupBrojSluzbenVesnik.Close;
     dmPQ.qSetupBrojSluzbenVesnik.ExecQuery;
     broj_sluzben_vesnik:=dmPQ.qSetupBrojSluzbenVesnik.FldByName['v1'].Value;
end;

procedure TfrmMain.aSifArtikalExecute(Sender: TObject);
begin
     frmArtikal:=TfrmArtikal.Create(self,true);
     frmArtikal.ShowModal();
     frmArtikal.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (dmKon.reporter_dll) then
       begin
         dmRes.FreeDLL;
       end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  WindowState:=wsMaximized;

 // cxSetEureka('danica.stojkova@codex.mk');

  if (dmKon.reporter_dll) then
   begin
		dmRes.ReporterInit;
		dmRes.GenerateReportsRibbon(dxRibbon1,dxRibbon1TabPreglediReporter,dxBarManager1);
   end;

  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;

  dmReport.qSetupD.Close;
  dmReport.qSetupD.ExecQuery;
  if (not dmReport.qSetupD.FldByName['v1'].IsNull) then
     pat_dokumenti:=dmReport.qSetupD.FldByName['v1'].Value
  else
     pat_dokumenti:='pat_dokumenti';

  dmReport.qSetupUpravnik.close;
  dmReport.qSetupUpravnik.ExecQuery;
  if ((not dmReport.qSetupUpravnik['v1'].IsNull) and (not dmReport.qSetupUpravnik['v2'].IsNull))then
     begin
        upravitelZvanje:=dmReport.qSetupUpravnik['v1'].Value;
        upravitelNaziv:=dmReport.qSetupUpravnik['v2'].Value;
     end
  else
     begin
        upravitelZvanje:='upravitelZvanje';
        upravitelNaziv:='upravitelNaziv';
     end;

  dmReport.qSetupKategorijaDogOrgan.close;
  dmReport.qSetupKategorijaDogOrgan.ExecQuery;
  if (not dmReport.qSetupKategorijaDogOrgan['v1'].IsNull) then
     kategorija_do_organ:=dmReport.qSetupKategorijaDogOrgan['v1'].Value
  else
     kategorija_do_organ:='kategorija_do_organ';

  dmPQ.qSetupBrojSluzbenVesnik.Close;
  dmPQ.qSetupBrojSluzbenVesnik.ExecQuery;
  if (not dmPQ.qSetupBrojSluzbenVesnik.FldByName['v1'].IsNull) then
     broj_sluzben_vesnik:=dmPQ.qSetupBrojSluzbenVesnik.FldByName['v1'].Value
  else
     broj_sluzben_vesnik:='broj_sluzben_vesnik';

  dm.tblProcTenderZatvori.Close;
  dm.tblProcTenderZatvori.Open;
  if (not dm.tblProcTenderZatvori.IsEmpty) and (prava_re = 0)then
     begin
      frmZatvoreniTenderi:=TfrmZatvoreniTenderi.Create(Application);
      frmZatvoreniTenderi.ShowModal();
      frmZatvoreniTenderi.Free;
     end;

  dm.qSetUpJN_frm_rok_na_vazenje_god.Close;
  dm.qSetUpJN_frm_rok_na_vazenje_god.ExecQuery;
  if (dm.qSetUpJN_frm_rok_na_vazenje_god.FldByName['v1'].Value=1) then
     begin
      dm.tblProcTenderZatvoriPred1Mesec.Close;
      dm.tblProcTenderZatvoriPred1Mesec.Open;
      if (not dm.tblProcTenderZatvoriPred1Mesec.IsEmpty)then
        begin
          frmPostapkiPredIstekNaDogovor:=TfrmPostapkiPredIstekNaDogovor.Create(Application);
          frmPostapkiPredIstekNaDogovor.ShowModal();
          frmPostapkiPredIstekNaDogovor.Free;
        end;
     end;

end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���
   dmMat.tblArtikal.Open;
   dmMat.tblArtVid.Open;
   dmMat.tblArtGrupa.Open;
   dmMat.tblMerka.Open;
   dmMat.tblProizvoditel.Open;
   dmMat.tblDrzava.Open;
   dmMat.tblOpstina.Open;
   dmMat.tblValuta.Open;
   dmMat.tblPartner.Open;
   dmMat.tblTipPartner.Open;
   dmMat.tblGenerika.Open;
   dmMat.tblListaArtikli.Open;
   dmMat.tblMapiranje.Open;
   dmMat.tblTipArtikal.Open;
   dmMat.tblArtKategorija.Open;
   dmMat.tblRE.Open;
   dmMat.tblMesto.Open;
   dmMat.tblJN_CPV.Open;

   //Sifrarnici
   dm.tblKriterium.Open;
   dm.tblKomisija.Open;
   dm.tblTipNabavka.Open;
   dm.tblVidoviDogovori.ParamByName('vid').Value:='%';
   dm.tblVidoviDogovori.Open;
   dm.tblVidDokument.Open;
   dm.tblPricinaPonistuvanje.Open;

   //SYS_PRAVA
   qPravaRe.ParamByName('korisnik').Value:=dmKon.user;
   qPravaRe.ExecQuery;
   if not qPravaRe.FldByName['prava']. IsNull then
      prava_re:=qPravaRe.FldByName['prava'].Value
   else
      prava_re:=1;



end;

procedure TfrmMain.aSifCPVExecute(Sender: TObject);
begin
   frmJN_CPV:=TfrmJN_CPV.Create(self,true);
   frmJN_CPV.ShowModal();
   frmJN_CPV.Free;
end;

//TODO


end.
