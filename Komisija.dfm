inherited frmKomisija: TfrmKomisija
  Caption = #1050#1086#1084#1080#1089#1080#1112#1072
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 254
    ExplicitHeight = 254
    inherited cxGrid1: TcxGrid
      Height = 250
      ExplicitHeight = 250
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsKomisija
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 68
        end
        object cxGrid1DBTableView1IME_PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'IME_PREZIME'
          Width = 262
        end
        object cxGrid1DBTableView1ULOGA: TcxGridDBColumn
          DataBinding.FieldName = 'ULOGA'
          Width = 366
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 380
    Height = 150
    ExplicitTop = 380
    ExplicitHeight = 150
    inherited Label1: TLabel
      Left = 55
      ExplicitLeft = 55
    end
    object Label2: TLabel [1]
      Left = 13
      Top = 49
      Width = 92
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1048#1084#1077' '#1055#1088#1077#1079#1080#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 55
      Top = 76
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1059#1083#1086#1075#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 111
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsKomisija
      ExplicitLeft = 111
    end
    inherited OtkaziButton: TcxButton
      Top = 110
      TabOrder = 4
      ExplicitTop = 110
    end
    inherited ZapisiButton: TcxButton
      Top = 110
      TabOrder = 3
      ExplicitTop = 110
    end
    object IMEPREZIME: TcxDBTextEdit
      Left = 111
      Top = 46
      BeepOnEnter = False
      DataBinding.DataField = 'IME_PREZIME'
      DataBinding.DataSource = dm.dsKomisija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 436
    end
    object ULOGA: TcxDBTextEdit
      Left = 111
      Top = 73
      BeepOnEnter = False
      DataBinding.DataField = 'ULOGA'
      DataBinding.DataSource = dm.dsKomisija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 436
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40920.667999768520000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
