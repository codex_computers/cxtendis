inherited frmTipNabavka: TfrmTipNabavka
  Caption = #1058#1080#1087' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
  ClientHeight = 664
  ClientWidth = 1137
  ExplicitWidth = 1153
  ExplicitHeight = 702
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1137
    Height = 269
    ExplicitWidth = 1060
    ExplicitHeight = 269
    inherited cxGrid1: TcxGrid
      Width = 1133
      Height = 265
      ExplicitWidth = 1056
      ExplicitHeight = 265
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsTipNabavka
        OptionsData.Editing = True
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Width = 56
        end
        object cxGrid1DBTableView1SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
          Options.Editing = False
          Width = 46
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 341
        end
        object cxGrid1DBTableView1DENOVI_ZALBA: TcxGridDBColumn
          DataBinding.FieldName = 'DENOVI_ZALBA'
          Options.Editing = False
          Width = 91
        end
        object cxGrid1DBTableView1ROK_DOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'ROK_DOGOVOR'
          Options.Editing = False
          Width = 173
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Properties.ReadOnly = True
          Width = 268
        end
        object cxGrid1DBTableView1TIP_GOD_PLAN: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_GOD_PLAN'
          Visible = False
        end
        object cxGrid1DBTableView1tip_god_plan_Naziv: TcxGridDBColumn
          DataBinding.FieldName = 'tip_god_plan_Naziv'
          Options.Editing = False
          Width = 170
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 395
    Width = 1137
    Height = 246
    ExplicitTop = 395
    ExplicitWidth = 1060
    ExplicitHeight = 246
    inherited Label1: TLabel
      Left = 77
      ExplicitLeft = 77
    end
    object Label2: TLabel [1]
      Left = 77
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [2]
      Left = 77
      Top = 100
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 18
      Top = 76
      Width = 109
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1078#1072#1083#1073#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 234
      Top = 76
      Width = 191
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1086#1082' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 133
      DataBinding.DataField = 'SIFRA'
      DataBinding.DataSource = dm.dsTipNabavka
      ExplicitLeft = 133
    end
    inherited OtkaziButton: TcxButton
      Left = 1038
      Top = 198
      TabOrder = 6
      ExplicitLeft = 961
      ExplicitTop = 198
    end
    inherited ZapisiButton: TcxButton
      Left = 957
      Top = 198
      TabOrder = 5
      ExplicitLeft = 880
      ExplicitTop = 198
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 133
      Top = 46
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipNabavka
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 740
    end
    object OPIS: TcxDBMemo
      Left = 133
      Top = 100
      Hint = 
        #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1090#1080#1087' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072'. *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077 +
        #1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsTipNabavka
      Properties.WantReturns = False
      TabOrder = 4
      OnDblClick = OPISDblClick
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 54
      Width = 740
    end
    object DENOVI_ZALBA: TcxDBTextEdit
      Left = 133
      Top = 73
      Hint = #1044#1077#1085#1086#1074#1080' '#1079#1072' '#1078#1072#1083#1073#1072' '#1074#1086' '#1079#1072#1074#1080#1089#1085#1086#1089#1090' '#1086#1076' '#1087#1086#1089#1090#1072#1087#1082#1072#1090#1072
      BeepOnEnter = False
      DataBinding.DataField = 'DENOVI_ZALBA'
      DataBinding.DataSource = dm.dsTipNabavka
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxDBRadioGroup1: TcxDBRadioGroup
      Left = 133
      Top = 160
      Caption = #1044#1072#1083#1080' '#1080#1084#1072' '#1082#1086#1085#1090#1088#1086#1083#1072' '#1089#1087#1086#1088#1077#1076' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ?'
      DataBinding.DataField = 'TIP_GOD_PLAN'
      DataBinding.DataSource = dm.dsTipNabavka
      Properties.Columns = 2
      Properties.Items = <
        item
          Caption = #1044#1072
          Value = 0
        end
        item
          Caption = #1053#1077
          Value = 1
        end>
      TabOrder = 7
      Height = 58
      Width = 244
    end
    object AKTIVEN: TcxDBCheckBox
      Left = 219
      Top = 19
      TabStop = False
      Caption = #1040#1082#1090#1080#1074#1077#1085
      DataBinding.DataField = 'AKTIVEN'
      DataBinding.DataSource = dm.dsTipNabavka
      Properties.ImmediatePost = True
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      TabOrder = 8
      Transparent = True
      Width = 70
    end
    object ROK_DOGOVOR: TcxDBTextEdit
      Left = 431
      Top = 73
      Hint = #1056#1086#1082' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      BeepOnEnter = False
      DataBinding.DataField = 'ROK_DOGOVOR'
      DataBinding.DataSource = dm.dsTipNabavka
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1137
    ExplicitWidth = 1060
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 641
    Width = 1137
    ExplicitTop = 641
    ExplicitWidth = 1060
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited PopupMenu1: TPopupMenu
    Top = 248
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 240
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Top = 232
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40921.354550844910000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.AnimationDelay = 300
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
