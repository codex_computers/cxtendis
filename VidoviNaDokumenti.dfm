inherited frmVidDokumet: TfrmVidDokumet
  Caption = #1042#1080#1076#1086#1074#1080' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
  ClientHeight = 681
  ClientWidth = 784
  ExplicitWidth = 800
  ExplicitHeight = 720
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 784
    Height = 376
    ExplicitWidth = 784
    ExplicitHeight = 376
    inherited cxGrid1: TcxGrid
      Width = 780
      Height = 372
      ExplicitWidth = 780
      ExplicitHeight = 372
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsVidDokument
        Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 80
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 419
        end
        object cxGrid1DBTableView1PATEKA: TcxGridDBColumn
          DataBinding.FieldName = 'PATEKA'
          Width = 211
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 220
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 220
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 220
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 220
        end
        object cxGrid1DBTableView1TEMPLATE_FRF: TcxGridDBColumn
          DataBinding.FieldName = 'TEMPLATE_FRF'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 502
    Width = 784
    Height = 156
    ExplicitTop = 502
    ExplicitWidth = 784
    ExplicitHeight = 156
    inherited Label1: TLabel
      Left = 76
      ExplicitLeft = 76
    end
    object Label2: TLabel [1]
      Left = 76
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 76
      Width = 113
      Height = 16
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1090#1077#1088#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Left = 132
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsVidDokument
      ExplicitLeft = 132
      ExplicitWidth = 53
      Width = 53
    end
    inherited OtkaziButton: TcxButton
      Left = 693
      Top = 116
      TabOrder = 3
      ExplicitLeft = 693
      ExplicitTop = 116
    end
    inherited ZapisiButton: TcxButton
      Left = 612
      Top = 116
      TabOrder = 2
      ExplicitLeft = 612
      ExplicitTop = 116
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 132
      Top = 46
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsVidDokument
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 469
    end
    object PATEKA: TcxDBTextEdit
      Left = 132
      Top = 73
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'PATEKA'
      DataBinding.DataSource = dm.dsVidDokument
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 415
    end
    object cxButton1: TcxButton
      Left = 553
      Top = 71
      Width = 48
      Height = 24
      Hint = #1055#1088#1086#1095#1080#1090#1072#1112' '#1112#1072' '#1087#1072#1090#1077#1082#1072#1090#1072' '#1076#1086' '#1083#1086#1082#1072#1094#1080#1112#1072#1090#1072' '#1085#1072' '#1090#1077#1088#1082#1086#1090
      Anchors = [akRight, akBottom]
      Caption = '...'
      TabOrder = 5
      OnClick = aOpenPatekaExecute
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 784
    ExplicitWidth = 784
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 658
    Width = 784
    ExplicitTop = 658
    ExplicitWidth = 784
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    object aOpenPateka: TAction
      Caption = '...'
      Hint = #1055#1088#1086#1095#1080#1090#1072#1112' '#1112#1072' '#1087#1072#1090#1077#1082#1072#1090#1072' '#1076#1086' '#1083#1086#1082#1072#1094#1080#1112#1072#1090#1072' '#1085#1072' '#1090#1077#1088#1082#1086#1090
      OnExecute = aOpenPatekaExecute
    end
    object aDizajnReport: TAction
      AutoCheck = True
      Caption = 'aDizajnReport'
      Checked = True
      ShortCut = 16505
      OnExecute = aDizajnReportExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41047.409553506940000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Rich Text  Format|*.rtf'
    InitialDir = '...\JN_DOKUMENTI\Template'
    Title = #1048#1079#1073#1077#1088#1077#1090#1077' '#1090#1077#1088#1082' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090
    Left = 544
    Top = 120
  end
end
