object frmIzborStavkiGodisenPlan: TfrmIzborStavkiGodisenPlan
  Left = 0
  Top = 0
  Caption = #1048#1079#1073#1086#1088' '#1080' '#1074#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '
  ClientHeight = 732
  ClientWidth = 1044
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object topPanell: TPanel
    Left = 0
    Top = 0
    Width = 1044
    Height = 105
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = clCream
    ParentBackground = False
    TabOrder = 0
    object Label2: TLabel
      Left = 25
      Top = 75
      Width = 121
      Height = 19
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = 10901821
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object labelRabotnaEdinica: TLabel
      Left = 152
      Top = 76
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cxGroupBox1: TcxGroupBox
      Left = 25
      Top = 19
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      TabOrder = 0
      Transparent = True
      Height = 50
      Width = 289
      object Label1: TLabel
        Left = 105
        Top = 23
        Width = 49
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 3
        Top = 23
        Width = 62
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1043#1086#1076#1080#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object labelGodisenPlanBroj: TLabel
        Left = 160
        Top = 24
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object labelGodisenPlanGodina: TLabel
        Left = 71
        Top = 24
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 712
    Width = 1044
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1042#1085#1077#1089' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
        Width = 210
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'CTRL+A - '#1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1080
        Width = 215
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Shift+Ctrl+S - '#1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090', Shift+Ctrl+D - '#1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072 +
          #1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076' '
        Width = 410
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1048#1079#1083#1077#1079
        Width = 20
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object gridPanel: TPanel
    Left = 0
    Top = 105
    Width = 1044
    Height = 570
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 1040
      Height = 566
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzborStavkiGodPlanSektori
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'JN_TIP_ARTIKAL'
          Options.Editing = False
          Width = 33
        end
        object cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDNAZIV'
          Options.Editing = False
          Width = 67
        end
        object cxGrid1DBTableView1GENERIKA: TcxGridDBColumn
          DataBinding.FieldName = 'GENERIKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 1
            end>
          Options.Editing = False
          Width = 72
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Options.Editing = False
          Width = 60
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Options.Editing = False
          Width = 299
        end
        object cxGrid1DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Options.Editing = False
          Width = 37
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 65
        end
        object cxGrid1DBTableView1TARIFA: TcxGridDBColumn
          DataBinding.FieldName = 'TARIFA'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 40
        end
        object cxGrid1DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Options.IncSearch = False
          Styles.Content = dmRes.cxStyle120
          Width = 82
        end
        object cxGrid1DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Options.Editing = False
          Width = 40
        end
        object cxGrid1DBTableView1GRUPANZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANZIV'
          Options.Editing = False
          Width = 77
        end
        object cxGrid1DBTableView1JN_CPV: TcxGridDBColumn
          DataBinding.FieldName = 'JN_CPV'
          Options.Editing = False
          Width = 110
        end
        object cxGrid1DBTableView1AktivenNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'AktivenNaziv'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
          Visible = False
          Options.Editing = False
          Width = 50
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object panelBottom: TPanel
    Left = 0
    Top = 675
    Width = 1044
    Height = 37
    Align = alBottom
    ParentBackground = False
    TabOrder = 3
    DesignSize = (
      1044
      37)
    object Label4: TLabel
      Left = 276
      Top = 12
      Width = 491
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 
        '* '#1042#1086' '#1089#1083#1091#1095#1072#1112' '#1085#1072' '#1085#1072#1084#1072#1083#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1083#1072#1085' '#1074#1088#1077#1076#1085#1086#1089#1090#1072' '#1085#1072' '#1087#1086#1083#1077#1090#1086' '#1079#1072' '#1082#1086#1083#1080#1095#1080#1085 +
        #1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1073#1080#1076#1077' '#1085#1077#1075#1072#1090#1080#1074#1085#1072' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 160
    end
    object ZapisiButton: TcxButton
      Left = 789
      Top = 6
      Width = 216
      Height = 25
      Action = aVnesNaArtikliVoPlan
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 632
    Top = 32
    object aIzlez: TAction
      Caption = 'aIzlez'
      ShortCut = 27
      OnExecute = aIzlezExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aBrisiIzgled: TAction
      Caption = 'aBrisiIzgled'
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aVnesNaArtikliVoPlan: TAction
      Caption = #1042#1085#1077#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      Hint = 
        #1042#1085#1077#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1080' '#1074#1086' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1089#1077' '#1074#1085#1077#1089#1091#1074#1072#1072#1090' '#1072#1088#1090#1080#1082#1083#1080#1090#1077' '#1079#1072' '#1082#1086#1080' '#1080 +
        #1084#1072' '#1087#1086#1087#1086#1083#1085#1077#1090#1086' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1088#1072#1079#1083#1080#1095#1085#1072' '#1086#1076' '#1085#1091#1083#1072')'
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aVnesNaArtikliVoPlanExecute
    end
    object aSifArtikli: TAction
      Caption = #1054#1090#1074#1086#1088#1080' '#1096#1080#1092#1088#1072#1088#1085#1080#1082' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1080
      ShortCut = 16449
      OnExecute = aSifArtikliExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 400
    Top = 48
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 736
    Top = 376
  end
end
