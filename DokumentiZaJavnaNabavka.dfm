inherited frmDokumentiZaJavnaNabavka: TfrmDokumentiZaJavnaNabavka
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
  ClientHeight = 627
  ClientWidth = 1029
  ExplicitWidth = 1045
  ExplicitHeight = 666
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 1029
    Height = 310
    ExplicitWidth = 1029
    ExplicitHeight = 310
    inherited cxGrid1: TcxGrid
      Width = 1025
      Height = 306
      ExplicitWidth = 1025
      ExplicitHeight = 306
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsTenderDokumenti
        OptionsView.NoDataToDisplayInfoText = #1053#1072#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
        OptionsView.Footer = False
        object cxGrid1DBTableView1RED_BR: TcxGridDBColumn
          DataBinding.FieldName = 'RED_BR'
        end
        object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_TENDER'
          Visible = False
          Width = 120
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 55
        end
        object cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn
          DataBinding.FieldName = 'VID_DOKUMENT'
          Visible = False
          Width = 142
        end
        object cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDDOKUMENTNAZIV'
          Width = 550
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 76
        end
        object cxGrid1DBTableView1KONTROLIRAL: TcxGridDBColumn
          DataBinding.FieldName = 'KONTROLIRAL'
          Width = 200
        end
        object cxGrid1DBTableView1TEMPLATE: TcxGridDBColumn
          DataBinding.FieldName = 'TEMPLATE'
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 162
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 215
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 197
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 242
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 436
    Width = 1029
    Height = 168
    ExplicitTop = 436
    ExplicitWidth = 1029
    ExplicitHeight = 168
    inherited Label1: TLabel
      Left = 437
      Top = 8
      Visible = False
      ExplicitLeft = 437
      ExplicitTop = 8
    end
    object Label2: TLabel [1]
      Left = 51
      Top = 77
      Width = 50
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 27
      Top = 44
      Width = 74
      Height = 26
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1042#1080#1076' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel [3]
      Left = 16
      Top = 104
      Width = 85
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 16
      Top = 23
      Width = 85
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1077#1076'. '#1041#1088'. :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 493
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTenderDokumenti
      TabOrder = 3
      Visible = False
      ExplicitLeft = 493
      ExplicitTop = 6
      ExplicitWidth = 60
      Width = 60
    end
    inherited OtkaziButton: TcxButton
      Left = 938
      Top = 128
      TabOrder = 7
      ExplicitLeft = 938
      ExplicitTop = 128
    end
    inherited ZapisiButton: TcxButton
      Left = 857
      Top = 128
      TabOrder = 6
      ExplicitLeft = 857
      ExplicitTop = 128
    end
    object DATUM: TcxDBDateEdit
      Left = 107
      Top = 74
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsTenderDokumenti
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object VID_DOKUMENT: TcxDBTextEdit
      Tag = 1
      Left = 107
      Top = 47
      BeepOnEnter = False
      DataBinding.DataField = 'VID_DOKUMENT'
      DataBinding.DataSource = dm.dsTenderDokumenti
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object VID_DOKUMENT_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 166
      Top = 47
      Anchors = [akLeft, akTop, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'VID_DOKUMENT'
      DataBinding.DataSource = dm.dsTenderDokumenti
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'NAZIV'
        end
        item
          FieldName = 'PATEKA'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dm.dsVidDokument
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 697
    end
    object RED_BR: TcxDBTextEdit
      Left = 107
      Top = 20
      BeepOnEnter = False
      DataBinding.DataField = 'RED_BR'
      DataBinding.DataSource = dm.dsTenderDokumenti
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object KONTROLIRAL: TcxDBLookupComboBox
      Left = 107
      Top = 101
      BeepOnEnter = False
      DataBinding.DataField = 'KONTROLIRAL'
      DataBinding.DataSource = dm.dsTenderDokumenti
      Properties.DropDownListStyle = lsEditList
      Properties.KeyFieldNames = 'KONTROLIRAL'
      Properties.ListColumns = <
        item
          FieldName = 'KONTROLIRAL'
        end>
      Properties.ListSource = dsKontroliral
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 278
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1029
    ExplicitWidth = 1029
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 604
    Width = 1029
    ExplicitTop = 604
    ExplicitWidth = 1029
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 240
    Top = 232
  end
  inherited PopupMenu1: TPopupMenu
    Left = 392
    Top = 344
  end
  inherited dxBarManager1: TdxBarManager
    Left = 552
    Top = 368
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = '_'
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 563
      FloatClientWidth = 102
      FloatClientHeight = 76
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 808
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      CaptionButtons = <>
      DockedLeft = 438
      DockedTop = 0
      FloatLeft = 787
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    inherited dxBarLargeButton3: TdxBarLargeButton
      SyncImageIndex = False
      ImageIndex = 11
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aBrisiDokument
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aKreirajDokument
      Category = 0
      SyncImageIndex = False
      ImageIndex = 26
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aAzurirajDokument
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aPregledaj
      Category = 0
      SyncImageIndex = False
      ImageIndex = 27
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPIzvestajSprovedPostPonudiPredmetEvaluacija
      Category = 0
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1086#1076' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton2: TdxBarButton
      Action = aPIzvestajSprovedPostPonudiPredmetEvaluacija
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aPIzvestajSprovedPostPonudiPredlogNajpovolnaPonuda
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton5: TdxBarButton
      Action = aIzvestajSprovedenaPostapka
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aTendDokObrazecListaCeni
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aKreirajDokument
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 248
    object aKreirajDokumentTemplate: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112
      ImageIndex = 10
      OnExecute = aKreirajDokumentTemplateExecute
    end
    object aAzurirajDokument: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajDokumentExecute
    end
    object aBrisiDokument: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiDokumentExecute
    end
    object aPregledaj: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112
      ImageIndex = 19
      OnExecute = aPregledajExecute
    end
    object aPIzvestajSprovedPostPonudiPredmetEvaluacija: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1082#1086#1080' '#1089#1077' '#1087#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1077#1074#1072#1083#1091#1072#1094#1080#1112#1072
      ImageIndex = 19
    end
    object aDIzvestajSprovedPostPonudiPredmetEvaluacija: TAction
      Caption = #1055#1086#1085#1091#1076#1080' '#1082#1086#1080' '#1089#1077' '#1087#1088#1077#1076#1084#1077#1090' '#1085#1072' '#1077#1074#1072#1083#1091#1072#1094#1080#1112#1072
      ImageIndex = 19
    end
    object aDizajnPopupMenu: TAction
      Caption = 'aDizajnPopupMenu'
      ShortCut = 16505
      OnExecute = aDizajnPopupMenuExecute
    end
    object aPIzvestajSprovedPostPonudiPredlogNajpovolnaPonuda: TAction
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
    end
    object aDIzvestajSprovedPostPonudiPredlogNajpovolnaPonuda: TAction
      Caption = #1055#1088#1077#1076#1083#1086#1075' '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1085#1072#1112#1087#1086#1074#1086#1083#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
    end
    object aIzvestajSprovedenaPostapka: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      ImageIndex = 19
      OnExecute = aIzvestajSprovedenaPostapkaExecute
    end
    object aDIzvestajSprovedenaPostapka: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1089#1087#1088#1086#1074#1077#1076#1077#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072
      ImageIndex = 19
      OnExecute = aDIzvestajSprovedenaPostapkaExecute
    end
    object aTendDokObrazecListaCeni: TAction
      Caption = #1058#1077#1085#1076#1077#1088#1089#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' - '#1054#1073#1088#1072#1079#1077#1094' '#1079#1072' '#1083#1080#1089#1090#1072' '#1085#1072' '#1094#1077#1085#1080' '#1085#1072' '#1087#1086#1076#1075#1088#1091#1087#1080' '
      ImageIndex = 19
      OnExecute = aTendDokObrazecListaCeniExecute
    end
    object aDTendDokObrazecListaCeni: TAction
      Caption = #1058#1077#1085#1076#1077#1088#1089#1082#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' - '#1054#1073#1088#1072#1079#1077#1094' '#1079#1072' '#1083#1080#1089#1090#1072' '#1085#1072' '#1094#1077#1085#1080' '#1085#1072' '#1087#1086#1076#1075#1088#1091#1087#1080' '
      ImageIndex = 19
      OnExecute = aDTendDokObrazecListaCeniExecute
    end
    object aKreirajDokumentFRF: TAction
      Caption = 'aKreirajDokumentFRF'
      ImageIndex = 19
      OnExecute = aKreirajDokumentFRFExecute
    end
    object aKreirajDokument: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112
      ImageIndex = 10
      OnExecute = aKreirajDokumentExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41047.431987129630000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Top = 160
    PixelsPerInch = 96
  end
  object PopupMenu2: TPopupMenu
    Left = 544
    Top = 272
    object N4: TMenuItem
      Action = aDIzvestajSprovedenaPostapka
    end
    object N2: TMenuItem
      Action = aDTendDokObrazecListaCeni
    end
  end
  object qRed_br: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(max(td.red_br),0) as red_br'
      'from jn_tender_dokumenti td'
      'where td.broj_tender = :broj_tender')
    Left = 464
    Top = 320
  end
  object tblKontroliral: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct td.kontroliral'
      'from jn_tender_dokumenti td')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 656
    Top = 232
    object tblKontroliralKONTROLIRAL: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1083
      FieldName = 'KONTROLIRAL'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsKontroliral: TDataSource
    DataSet = tblKontroliral
    Left = 744
    Top = 232
  end
end
