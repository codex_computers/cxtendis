inherited frmSetup: TfrmSetup
  Caption = 'SETUP'
  ClientHeight = 634
  ExplicitHeight = 673
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 242
    ExplicitHeight = 242
    inherited cxGrid1: TcxGrid
      Height = 238
      ExplicitHeight = 238
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsSetUp
        object cxGrid1DBTableView1P1: TcxGridDBColumn
          DataBinding.FieldName = 'P1'
          Width = 107
        end
        object cxGrid1DBTableView1V1: TcxGridDBColumn
          DataBinding.FieldName = 'V1'
          Width = 216
        end
        object cxGrid1DBTableView1V2: TcxGridDBColumn
          DataBinding.FieldName = 'V2'
          Width = 165
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 223
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 163
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 210
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 199
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 207
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 368
    Height = 243
    ExplicitTop = 368
    ExplicitHeight = 243
    inherited Label1: TLabel
      Width = 81
      Caption = #1055#1072#1088#1072#1084#1077#1090#1072#1088' 1 :'
      ExplicitWidth = 81
    end
    object Label3: TLabel [1]
      Left = 39
      Top = 109
      Width = 73
      Height = 13
      Caption = #1042#1088#1077#1076#1085#1086#1089#1090' 1 :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [2]
      Left = 79
      Top = 48
      Width = 33
      Height = 13
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [3]
      Left = 39
      Top = 136
      Width = 73
      Height = 13
      Caption = #1042#1088#1077#1076#1085#1086#1089#1090' 2 :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 118
      DataBinding.DataField = 'P1'
      DataBinding.DataSource = dm.dsSetUp
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      StyleDisabled.TextStyle = [fsBold]
      TabOrder = 1
      ExplicitLeft = 118
      ExplicitWidth = 141
      Width = 141
    end
    inherited OtkaziButton: TcxButton
      Top = 203
      TabOrder = 5
      ExplicitTop = 203
    end
    inherited ZapisiButton: TcxButton
      Top = 203
      TabOrder = 4
      ExplicitTop = 203
    end
    object V1: TcxDBTextEdit
      Left = 118
      Top = 106
      BeepOnEnter = False
      DataBinding.DataField = 'V1'
      DataBinding.DataSource = dm.dsSetUp
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 599
    end
    object memoOpis: TcxDBMemo
      Left = 118
      Top = 45
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsSetUp
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      StyleDisabled.TextStyle = [fsBold]
      TabOrder = 2
      Height = 47
      Width = 599
    end
    object V2: TcxDBTextEdit
      Left = 118
      Top = 133
      BeepOnEnter = False
      DataBinding.DataField = 'V2'
      DataBinding.DataSource = dm.dsSetUp
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 3
      OnKeyDown = EnterKakoTab
      Width = 599
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 611
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 611
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited PopupMenu1: TPopupMenu
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Left = 456
    Top = 232
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 248
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41634.420474884260000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
