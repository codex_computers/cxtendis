unit Dogovor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxSplitter, cxPCdxBarPopupMenu, cxRichEdit, cxDBRichEdit, FIBDataSet,
  pFIBDataSet, dxCustomHint, cxHint, cxLabel, cxDBLabel, cxImageComboBox,
  dxSkinOffice2013White, cxNavigator, System.Actions, dxBarBuiltInMenu,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmDogovor = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelMaster: TPanel;
    PanelDetail: TPanel;
    cxSplitter1: TcxSplitter;
    cxPageControlDogovori: TcxPageControl;
    cxTabSheetTabelaren: TcxTabSheet;
    cxTabSheetDetalen: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1PERIOD: TcxGridDBColumn;
    cxGrid1DBTableView1ROK_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView1ROK_FAKTURA: TcxGridDBColumn;
    cxGrid1DBTableView1GARANTEN_ROK: TcxGridDBColumn;
    cxGrid1DBTableView1SMETKA: TcxGridDBColumn;
    cxGrid1DBTableView1DIREKTOR: TcxGridDBColumn;
    cxGrid1DBTableView1TIP: TcxGridDBColumn;
    cxGrid1DBTableView1tipNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1VRSKA_DOGOVOR: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    PanelDetalen: TPanel;
    cxGroupBox1: TcxGroupBox;
    Label69: TLabel;
    Label3: TLabel;
    TIP_PARTNER: TcxDBTextEdit;
    PARTNER: TcxDBTextEdit;
    PARTNER_NAZIV: TcxLookupComboBox;
    DIREKTOR: TcxDBTextEdit;
    SMETKA: TcxDBLookupComboBox;
    Label74: TLabel;
    cxGroupBox2: TcxGroupBox;
    Label5: TLabel;
    ROK_ISPORAKA: TcxDBTextEdit;
    Label9: TLabel;
    ROK_FAKTURA: TcxDBTextEdit;
    Label10: TLabel;
    GARANTEN_ROK: TcxDBTextEdit;
    tblPartner: TpFIBDataSet;
    tblPartnerTIP_PARTNER: TFIBIntegerField;
    tblPartnerTIP_NAZIV: TFIBStringField;
    tblPartnerID: TFIBIntegerField;
    tblPartnerNAZIV: TFIBStringField;
    tblPartnerNAZIV_SKRATEN: TFIBStringField;
    tblPartnerRE: TFIBIntegerField;
    tblPartnerSTATUS: TFIBIntegerField;
    dsPartner: TDataSource;
    Label8: TLabel;
    Label11: TLabel;
    cxGroupBox3: TcxGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    BROJ_DOGOVOR: TcxDBTextEdit;
    DATUM: TcxDBDateEdit;
    PERIOD: TcxDBTextEdit;
    DATUM_VAZENJE: TcxDBDateEdit;
    OPIS: TcxDBRichEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid2DBTableView1BROJ_DOGOVOR: TcxGridDBColumn;
    cxGrid2DBTableView1RED_BR: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid2DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid2DBTableView1MK: TcxGridDBColumn;
    cxGrid2DBTableView1EN: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    Label12: TLabel;
    cxDBLabel1: TcxDBLabel;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDodadiStavki: TAction;
    aBrisiStavki: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    aBaranjaIsporaka: TAction;
    dxBarManager1Bar7: TdxBar;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    aPecatiTabelaStavki: TAction;
    aRefreshStavki: TAction;
    aZacuvajExcelStavki: TAction;
    aPodesuvanjePecatenjeStavki: TAction;
    aPageSetupStavki: TAction;
    aSnimiPecatenjeStavki: TAction;
    aBrisiPodesuvanjePecatenjeStavki: TAction;
    aSnimiIzgledStavki: TAction;
    aBrisiIzgledStvaki: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxGridPopupMenu2: TcxGridPopupMenu;
    aPDogovorVrabotuvanjeTerk: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    aDokumentiPregled: TAction;
    dxBarManager1Bar9: TdxBar;
    dxBarManager1Bar10: TdxBar;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    aAneksNaDogovor: TAction;
    aDogovorNaAneks: TAction;
    cxGroupBoxIznosDogovor: TcxGroupBox;
    Label13: TLabel;
    IZNOS: TcxDBTextEdit;
    cxGrid2DBTableView1CENABEZDDV: TcxGridDBColumn;
    cxGrid2DBTableView1DANOK: TcxGridDBColumn;
    cxGrid2DBTableView1CENA: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOSBEZDDV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1STAVKA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1STAVKA_MERKA: TcxGridDBColumn;
    dxBarLargeButton33: TdxBarLargeButton;
    actPregledajDogovor: TAction;
    pm1: TPopupMenu;
    actDizajn: TAction;
    actDizajnDogovori: TAction;
    N2: TMenuItem;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure OPISDblClick(Sender: TObject);
    procedure aDodadiStavkiExecute(Sender: TObject);
    procedure aBrisiStavkiExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aBaranjaIsporakaExecute(Sender: TObject);
    procedure aPecatiTabelaStavkiExecute(Sender: TObject);
    procedure aBrisiIzgledStvakiExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aSnimiPecatenjeStavkiExecute(Sender: TObject);
    procedure aPageSetupStavkiExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
    procedure aZacuvajExcelStavkiExecute(Sender: TObject);
    procedure aRefreshStavkiExecute(Sender: TObject);
    procedure aPDogovorVrabotuvanjeTerkExecute(Sender: TObject);
    procedure aDokumentiPregledExecute(Sender: TObject);
    procedure aAneksNaDogovorExecute(Sender: TObject);
    procedure aDogovorNaAneksExecute(Sender: TObject);
    procedure actPregledajDogovorExecute(Sender: TObject);
    procedure actDizajnDogovoriExecute(Sender: TObject);
    procedure actDizajnExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDogovor: TfrmDogovor;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmMaticni,
  Notepad, IzborStavkiDogovor, Baranja, DokumentiZaJavnaNabavka, dmReportUnit, dmProcedureQuey;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmDogovor.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

procedure TfrmDogovor.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;
//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmDogovor.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    cxPageControlDogovori.ActivePage:=cxTabSheetDetalen;
    PanelDetalen.Enabled:=true;
    BROJ_DOGOVOR.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblDogovoriBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
    dm.tblDogovoriTIP.Value:=1;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmDogovor.aAneksNaDogovorExecute(Sender: TObject);
var vrska_dogovor:String;
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
         (cxGrid1DBTableView1.Controller.SelectedRecordCount <> 0))  then
        begin
          if dm.tblDogovoriTIP.Value = 1 then
             begin
                vrska_dogovor:=dm.tblDogovoriBROJ_DOGOVOR.Value;
                cxPageControlDogovori.ActivePage:=cxTabSheetDetalen;
                PanelDetalen.Enabled:=true;
                BROJ_DOGOVOR.SetFocus;
                cxGrid1DBTableView1.DataController.DataSet.Insert;
                dm.tblDogovoriBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
                dm.tblDogovoriTIP.Value:=-1;
                dm.tblDogovoriVRSKA_DOGOVOR.Value:=vrska_dogovor;
             end
          else  if dm.tblDogovoriTIP.Value = -1 then ShowMessage('�������� �� �������� ����� �� ����� �� ������� �� ��� '+ dm.tblDogovoriBROJ_DOGOVOR.Value +' !!!');
        end;
end;

procedure TfrmDogovor.aAzurirajExecute(Sender: TObject);
begin
 if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
   // dm.qCountBaranjePoDogovor.Close;
   // dm.qCountBaranjePoDogovor.ParamByName('broj_dogovor').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
   // dm.qCountBaranjePoDogovor.ExecQuery;
   // if dm.qCountBaranjePoDogovor.FldByName['br'].Value = 0 then
     //  begin
         cxPageControlDogovori.ActivePage:=cxTabSheetDetalen;
         PanelDetalen.Enabled:=true;
         BROJ_DOGOVOR.SetFocus;
         cxGrid1DBTableView1.DataController.DataSet.Edit;
      // end
    //else  ShowMessage('�� � ��������� ��������� �� ������������� �������. ����� ������������ ������ �� �������� !');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDogovor.aBaranjaIsporakaExecute(Sender: TObject);
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
         (cxGrid1DBTableView1.Controller.SelectedRecordCount <> 0))  then
        begin
          frmBaranja:=TfrmBaranja.Create(self,false);
          frmBaranja.Tag:=2;
          frmBaranja.ShowModal;
          frmBaranja.Free;
        end;
end;

procedure TfrmDogovor.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmDogovor.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmDogovor.aBrisiIzgledStvakiExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid2DBTableView1);
     BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmDogovor.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmDogovor.aRefreshStavkiExecute(Sender: TObject);
begin
     cxGrid2DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
     cxGrid2DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDogovor.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDogovor.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmDogovor.aSnimiIzgledStavkiExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
     ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDogovor.aZacuvajExcelExecute(Sender: TObject);
begin
      zacuvajVoExcel(cxGrid1, '�������� �� ����� �������');
end;

procedure TfrmDogovor.aZacuvajExcelStavkiExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid2, '������ �� ������� �� ����� �������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDogovor.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDogovor.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDogovor.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
   begin
    if Sender = PARTNER_NAZIV then
       begin
         dm.tblDogovoriTIP_PARTNER.Value:=tblPartnerTIP_PARTNER.Value;
         dm.tblDogovoriPARTNER.Value:=tblPartnerID.Value;
         dmMat.tblSmetkiP.Close;
         dmMat.tblSmetkiP.Open;
       end
    else if ((Sender as TWinControl)= TIP_PARTNER) or ((Sender as TWinControl)= PARTNER) then
         begin
             ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNER_NAZIV);
         end;
   end;
end;

procedure TfrmDogovor.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     ZemiImeDvoenKluc(TIP_PARTNER,PARTNER,PARTNER_NAZIV);
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDogovor.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDogovor.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmDogovor.OPISDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblDogovori.CreateBlobStream(dm.tblDogovoriOPIS , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblDogovoriOPIS as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmDogovor.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmDogovor.prefrli;
begin
end;

procedure TfrmDogovor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmDogovor.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmDogovor.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

    Caption:='�������� �� ����� ������� ��� '+ dm.tblTenderBROJ.Value;
    cxPageControlDogovori.ActivePage:=cxTabSheetTabelaren;
    cxTabSheetTabelaren.Caption:='��������� ������ �� �������� �� ����� ������� ��� '+ dm.tblTenderBROJ.Value;
    cxGrid1.SetFocus;
    dm.tblDogovori.Close;
    dm.tblDogovori.Open;

    tblPartner.Close;
    tblPartner.Open;

    dm.tblDogovorStavki.Close;
    dm.tblDogovorStavki.Open;

    dmMat.tblSmetkiP.DataSource:=dsPartner;
    dmMat.tblSmetkiP.Close;
    dmMat.tblSmetkiP.Open;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);

    if dm.tblTenderTIP_BARANJA.Value = 0 then
       cxGroupBoxIznosDogovor.Visible:=False;

    if Tag=1 then
      begin
        aNov.Execute();
        dm.tblDogovoriTIP_PARTNER.Value:=dm.tblPonudiTIP_PARTNER.Value;
        dm.tblDogovoriPARTNER.Value:=dm.tblPonudiPARTNER.Value;

        ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNER_NAZIV);
      end;
end;
//------------------------------------------------------------------------------

procedure TfrmDogovor.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDogovor.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmDogovor.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
      if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

//  ����� �� �����
procedure TfrmDogovor.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDetalen) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        PanelDetalen.Enabled:=false;
        cxPageControlDogovori.ActivePage:=cxTabSheetTabelaren;
        cxGrid1.SetFocus;

        if Tag = 1 then
           begin
             dmPQ.insert6(dmpq.pInsertDogovorStavkiAvtomatski,'TIP_PARTNER', 'PARTNER','BROJ_TENDER', 'BROJ_DOGOVOR',Null, Null,
                                               dm.tblDogovoriTIP_PARTNER.Value, dm.tblDogovoriPARTNER.Value,
                                               dm.tblDogovoriBROJ_TENDER.Value,dm.tblDogovoriBROJ_DOGOVOR.Value,Null, Null);
             dm.tblDogovorStavki.Close;
             dm.tblDogovorStavki.Open;
             ShowMessage('���������� �� �������� ���� ������� ������ �� ��������� !');

          end;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmDogovor.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDetalen);
      PanelDetalen.Enabled:=false;
      cxPageControlDogovori.ActivePage:=cxTabSheetTabelaren;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDogovor.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmDogovor.aPageSetupStavkiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmDogovor.aPDogovorVrabotuvanjeTerkExecute(Sender: TObject);
begin
try
    dmRes.Spremi('JN',40031);
    dmKon.tblSqlReport.ParamByName('baranje').Value:=dm.tblBaranjaBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ������� �� ������
procedure TfrmDogovor.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmDogovor.aPecatiTabelaStavkiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '������ �� ������� �� ����� �������';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('��� �� ������� �� ����������� : ' + dm.tblDogovoriBROJ_DOGOVOR.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDogovor.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmDogovor.aPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
     dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmDogovor.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmDogovor.aSnimiPecatenjeStavkiExecute(Sender: TObject);
begin
     zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDogovor.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmDogovor.aBrisiPodesuvanjePecatenjeStavkiExecute(Sender: TObject);
begin
     brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmDogovor.aBrisiStavkiExecute(Sender: TObject);
begin
   if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
      begin
         dm.qCountBaranjeStavkiPoDogovor.Close;
         dm.qCountBaranjeStavkiPoDogovor.ParamByName('broj_dogovor').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
         dm.qCountBaranjeStavkiPoDogovor.ParamByName('artikal').Value:=dm.tblDogovorStavkiARTIKAL.Value;
         dm.qCountBaranjeStavkiPoDogovor.ParamByName('vid_artikal').Value:=dm.tblDogovorStavkiVID_ARTIKAL.Value;
         dm.qCountBaranjeStavkiPoDogovor.ExecQuery;
         if dm.qCountBaranjeStavkiPoDogovor.FldByName['br'].Value = 0 then
            cxGrid2DBTableView1.DataController.DataSet.Delete()
         else ShowMessage('�� � ��������� ������ �� ������������� ������. ����� ������������ ������ �� �������� �� ������ !');
      end;
end;

procedure TfrmDogovor.actDizajnDogovoriExecute(Sender: TObject);
begin
    if dm.tblTenderGRUPA.Value = 1 then
       begin
         dmRes.Spremi('JN',40172);   // stoki
       end
    else if dm.tblTenderGRUPA.Value = 2 then
       begin
         dmRes.Spremi('JN',40173);  // uslugi
       end
    else if dm.tblTenderGRUPA.Value = 3 then
       begin
         dmRes.Spremi('JN',40174);  // raboti
       end;

    dmRes.frxReport1.DesignReport();
end;

procedure TfrmDogovor.actDizajnExecute(Sender: TObject);
begin
     pm1.Popup(500,500 );
end;

procedure TfrmDogovor.actPregledajDogovorExecute(Sender: TObject);
begin
//
  try
    if dm.tblTenderGRUPA.Value = 1 then
       begin
         dmRes.Spremi('JN',40172);   // stoki
       end
    else if dm.tblTenderGRUPA.Value = 2 then
       begin
         dmRes.Spremi('JN',40173);  // uslugi
       end
    else if dm.tblTenderGRUPA.Value = 3 then
       begin
         dmRes.Spremi('JN',40174);  // raboti
       end;
    dmKon.tblSqlReport.ParamByName('BROJ_DOGOVOR').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
    dmKon.tblSqlReport.Open;

    dmReport.tblDogovorStavki.Close;
    dmReport.tblDogovorStavki.ParamByName('broj_dogovor').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
    dmReport.tblDogovorStavki.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmDogovor.aDokumentiPregledExecute(Sender: TObject);
Var
      status: TStatusWindowHandle;
begin
      status := cxCreateStatusWindow();
	   try
        frmDokumentiZaJavnaNabavka:=TfrmDokumentiZaJavnaNabavka.Create(Self, false);
        frmDokumentiZaJavnaNabavka.ShowModal;
        frmDokumentiZaJavnaNabavka.Free;
     finally
        cxRemoveStatusWindow(status);
     end;
end;

procedure TfrmDogovor.aDodadiStavkiExecute(Sender: TObject);
begin
     frmIzborStavkiDogovor:=TfrmIzborStavkiDogovor.Create(self,false);
     frmIzborStavkiDogovor.ShowModal;
     frmIzborStavkiDogovor.Free;
end;

procedure TfrmDogovor.aDogovorNaAneksExecute(Sender: TObject);
var vrska_dogovor:String;
begin
     if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
         (cxGrid1DBTableView1.Controller.SelectedRecordCount <> 0))  then
        begin
          if dm.tblDogovoriTIP.Value = -1 then
             begin
                vrska_dogovor:=dm.tblDogovoriBROJ_DOGOVOR.Value;
                cxPageControlDogovori.ActivePage:=cxTabSheetDetalen;
                PanelDetalen.Enabled:=true;
                BROJ_DOGOVOR.SetFocus;
                cxGrid1DBTableView1.DataController.DataSet.Insert;
                dm.tblDogovoriBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
                dm.tblDogovoriTIP.Value:=1;
                dm.tblDogovoriVRSKA_DOGOVOR.Value:=vrska_dogovor;
             end
          else  if dm.tblDogovoriTIP.Value = 1 then ShowMessage('�������� �� �������� ������� �� ������� �� ��� '+ dm.tblDogovoriBROJ_DOGOVOR.Value +' !!!');
        end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDogovor.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDogovor.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDogovor.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
