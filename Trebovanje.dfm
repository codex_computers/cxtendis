object frmTrebovanje: TfrmTrebovanje
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1058#1088#1077#1073#1086#1074#1072#1114#1072
  ClientHeight = 713
  ClientWidth = 1169
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1169
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1BarOdlukaNabavka'
        end
        item
          Caption = #1057#1090#1072#1074#1082#1080
          ToolbarName = 'dxBarManager1Bar'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 690
    Width = 1169
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F8 - '#1041#1088#1080#1096#1080
        Width = 180
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'Ctrl+F5 - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F6 - '#1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072', Ctrl+F8 - '#1041#1088 +
          #1080#1096#1080' '#1089#1090#1072#1074#1082#1072'  '
        Width = 390
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F9 - '#1047#1072#1087#1080#1096#1080
        Width = 65
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelTrebovanje: TPanel
    Left = 0
    Top = 126
    Width = 1169
    Height = 211
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object cxPageControlTrebovanje: TcxPageControl
      Left = 2
      Top = 49
      Width = 1165
      Height = 160
      Align = alClient
      TabOrder = 0
      TabStop = False
      Properties.ActivePage = cxTabSheetTabelaren
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 160
      ClientRectRight = 1165
      ClientRectTop = 24
      object cxTabSheetTabelaren: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1058#1088#1077#1073#1086#1074#1072#1114#1072
        ImageIndex = 0
        object PanelTabelarenPrikaz: TPanel
          Left = 0
          Top = 0
          Width = 1165
          Height = 136
          Align = alClient
          TabOrder = 0
          object cxGrid1: TcxGrid
            Left = 1
            Top = 1
            Width = 1163
            Height = 134
            Align = alClient
            TabOrder = 0
            object cxGrid1DBTableView1: TcxGridDBTableView
              OnKeyPress = cxGrid1DBTableView1KeyPress
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = dm.dsTrebovanje
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
              FilterRow.Visible = True
              OptionsBehavior.IncSearch = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
              OptionsView.GroupByBox = False
              object cxGrid1DBTableView1ID: TcxGridDBColumn
                DataBinding.FieldName = 'ID'
                Visible = False
                Width = 63
              end
              object cxGrid1DBTableView1GODINA: TcxGridDBColumn
                DataBinding.FieldName = 'GODINA'
                Visible = False
              end
              object cxGrid1DBTableView1STATUS: TcxGridDBColumn
                DataBinding.FieldName = 'STATUS'
                Visible = False
                Width = 55
              end
              object cxGrid1DBTableView1StatusNaziv: TcxGridDBColumn
                DataBinding.FieldName = 'StatusNaziv'
                Width = 94
              end
              object cxGrid1DBTableView1BROJ: TcxGridDBColumn
                DataBinding.FieldName = 'BROJ'
                Width = 80
              end
              object cxGrid1DBTableView1DATUM: TcxGridDBColumn
                DataBinding.FieldName = 'DATUM'
                Width = 80
              end
              object cxGrid1DBTableView1RE: TcxGridDBColumn
                DataBinding.FieldName = 'RE'
                Width = 61
              end
              object cxGrid1DBTableView1RE_NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'RE_NAZIV'
                Width = 299
              end
              object cxGrid1DBTableView1RE2: TcxGridDBColumn
                DataBinding.FieldName = 'RE2'
                Width = 50
              end
              object cxGrid1DBTableView1RE2_NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'RE2_NAZIV'
                Width = 390
              end
              object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
                DataBinding.FieldName = 'TS_INS'
                Visible = False
                Width = 158
              end
              object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
                DataBinding.FieldName = 'TS_UPD'
                Visible = False
                Width = 215
              end
              object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
                DataBinding.FieldName = 'USR_INS'
                Visible = False
                Width = 203
              end
              object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
                DataBinding.FieldName = 'USR_UPD'
                Visible = False
                Width = 251
              end
              object cxGrid1DBTableView1OPIS: TcxGridDBColumn
                DataBinding.FieldName = 'OPIS'
                Width = 207
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBTableView1
            end
          end
        end
      end
      object cxTabSheetDetalen: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object PanelDetalenPrikaz: TPanel
          Left = 0
          Top = 0
          Width = 1165
          Height = 136
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            1165
            136)
          object Label2: TLabel
            Left = 31
            Top = 49
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label3: TLabel
            Left = 31
            Top = 22
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1043#1086#1076#1080#1085#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 14
            Top = 76
            Width = 67
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label6: TLabel
            Left = 411
            Top = 49
            Width = 44
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label5: TLabel
            Left = 344
            Top = 22
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1072#1073'. '#1045#1076#1080#1085#1080#1094#1072' - '#1076#1086' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object BROJ: TcxDBTextEdit
            Tag = 1
            Left = 87
            Top = 46
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsTrebovanje
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object GODINA: TcxDBComboBox
            Tag = 1
            Left = 87
            Top = 19
            Hint = #1043#1086#1076#1080#1085#1072' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            DataBinding.DataField = 'GODINA'
            DataBinding.DataSource = dm.dsTrebovanje
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              '2010'
              '2011'
              '2012'
              '2013'
              '2014'
              '2015'
              '2016'
              '2017'
              '2018'
              '2019'
              '2020'
              '2021'
              '2022'
              '2023'
              '2024'
              '2025'
              '2026'
              '2027'
              '2028'
              '2029'
              '2030'
              '2031'
              '2032'
              '2033'
              '2034'
              '2035'
              '2036'
              '2037'
              '2038'
              '2039'
              '2040'
              '2041'
              '2042'
              '2043'
              '2044'
              '2045'
              '2046'
              '2047'
              '2048'
              '2049'
              '2050')
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object DATUM: TcxDBDateEdit
            Tag = 1
            Left = 87
            Top = 73
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            DataBinding.DataField = 'DATUM'
            DataBinding.DataSource = dm.dsTrebovanje
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object ZapisiButton: TcxButton
            Left = 960
            Top = 89
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 8
          end
          object OtkaziButton: TcxButton
            Left = 1041
            Top = 89
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 9
          end
          object cxDBRadioGroupStatus: TcxDBRadioGroup
            Left = 212
            Top = 9
            Hint = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsTrebovanje
            Properties.Items = <
              item
                Caption = #1054#1090#1074#1086#1088#1077#1085#1086
                Value = 0
              end
              item
                Caption = #1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1086
                Value = 1
              end
              item
                Caption = #1057#1090#1086#1088#1085#1080#1088#1072#1085#1086
                Value = 2
              end>
            TabOrder = 4
            Height = 88
            Width = 119
          end
          object RE2: TcxDBTextEdit
            Left = 461
            Top = 19
            Hint = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1044#1054' '#1082#1086#1112#1072' '#1077' '#1085#1072#1084#1077#1085#1077#1090#1086' '#1090#1088#1077#1073#1086#1074#1072#1114#1077#1090#1086
            BeepOnEnter = False
            DataBinding.DataField = 'RE2'
            DataBinding.DataSource = dm.dsTrebovanje
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 42
          end
          object RE2_NAZIV: TcxDBExtLookupComboBox
            Left = 504
            Top = 19
            Hint = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1044#1054' '#1082#1086#1112#1072' '#1077' '#1085#1072#1084#1077#1085#1077#1090#1086' '#1090#1088#1077#1073#1086#1074#1072#1114#1077#1090#1086
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'RE2'
            DataBinding.DataSource = dm.dsTrebovanje
            Properties.ClearKey = 46
            Properties.DropDownSizeable = True
            TabOrder = 6
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 612
          end
          object SIFRA: TcxDBTextEdit
            Left = 87
            Top = 100
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            BeepOnEnter = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dm.dsTrebovanje
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object Opis: TcxDBMemo
            Left = 461
            Top = 46
            Hint = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1079#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsTrebovanje
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 37
            Width = 655
          end
        end
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 2
      Width = 1165
      Height = 47
      Align = alTop
      TabOrder = 1
      DesignSize = (
        1165
        47)
      object Label8: TLabel
        Left = 16
        Top = 16
        Width = 118
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbPodsektori: TcxTextEdit
        Left = 136
        Top = 14
        BeepOnEnter = False
        Properties.OnEditValueChanged = cbPodsektoriPropertiesEditValueChanged
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 65
      end
      object cbPodsektoriTree: TdxLookupTreeView
        Left = 201
        Top = 14
        Width = 756
        Height = 21
        CanSelectParents = True
        ParentColor = False
        TabOrder = 1
        TabStop = True
        Text = ''
        TreeViewColor = clWindow
        TreeViewCursor = crDefault
        TreeViewFont.Charset = DEFAULT_CHARSET
        TreeViewFont.Color = clWindowText
        TreeViewFont.Height = -11
        TreeViewFont.Name = 'Tahoma'
        TreeViewFont.Style = []
        TreeViewIndent = 19
        TreeViewReadOnly = True
        TreeViewShowButtons = True
        TreeViewShowHint = False
        TreeViewShowLines = True
        TreeViewShowRoot = True
        TreeViewSortType = stNone
        OnEnter = cxDBTextEditAllEnter
        OnExit = cbPodsektoriTreeExit
        OnKeyDown = EnterKakoTab
        Anchors = [akLeft, akTop, akRight, akBottom]
        DisplayField = 'ID;NAZIV'
        DividedChar = '.'
        ListSource = dm.dsPodsektoriGodisenPlan
        KeyField = 'ID'
        ListField = 'NAZIV'
        Options = [trDBCanDelete, trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
        ParentField = 'KOREN'
        RootValue = Null
        TextStyle = tvtsShort
        Alignment = taLeftJustify
      end
    end
  end
  object PanelStavki: TPanel
    Left = 0
    Top = 345
    Width = 1169
    Height = 345
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 3
    object cxPageControlStavki: TcxPageControl
      Left = 2
      Top = 2
      Width = 1165
      Height = 341
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelarenStavki
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 341
      ClientRectRight = 1165
      ClientRectTop = 24
      object cxTabSheetTabelarenStavki: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1057#1090#1072#1074#1082#1080
        ImageIndex = 0
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 1165
          Height = 317
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid2DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsTrebovanjeStavki
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.0000, .'
                Position = spFooter
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00 , .'
                Kind = skSum
              end
              item
                Format = '0.00, .'
                Kind = skSum
              end
              item
                Format = '0.00, .'
                Kind = skSum
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
            OptionsView.Footer = True
            object cxGrid2DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid2DBTableView1TREBUVANJE_ID: TcxGridDBColumn
              DataBinding.FieldName = 'TREBUVANJE_ID'
              Visible = False
              Width = 112
            end
            object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'JN_TIP_ARTIKAL'
              Width = 40
            end
            object cxGrid2DBTableView1JN_GRUPA_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'JN_GRUPA_NAZIV'
              Width = 78
            end
            object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKAL'
            end
            object cxGrid2DBTableView1ARTIKAL_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKAL_NAZIV'
              Width = 242
            end
            object cxGrid2DBTableView1MERKA_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'MERKA_NAZIV'
              Width = 64
            end
            object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
              DataBinding.FieldName = 'KOLICINA'
            end
            object cxGrid2DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              Width = 248
            end
            object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'VID_ARTIKAL'
              Visible = False
              Width = 81
            end
            object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 163
            end
            object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 223
            end
            object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 204
            end
            object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 240
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
      object cxTabSheetDetalenStavki: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1057#1090#1072#1074#1082#1080
        ImageIndex = 1
        object PanelDetalenStavki: TPanel
          Left = 0
          Top = 0
          Width = 1165
          Height = 317
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            1165
            317)
          object Label1: TLabel
            Left = 51
            Top = 81
            Width = 44
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label7: TLabel
            Left = 30
            Top = 57
            Width = 65
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = -16
            Top = 30
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1040#1088#1090#1080#1082#1072#1083' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object SifraStavka: TcxDBTextEdit
            Left = 239
            Top = 54
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
            BeepOnEnter = False
            DataBinding.DataField = 'ID'
            DataBinding.DataSource = dm.dsTrebovanjeStavki
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 3
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object OpisStavka: TcxDBMemo
            Left = 101
            Top = 81
            Hint = #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1079#1072' '#1089#1090#1072#1074#1082#1072
            Anchors = [akLeft, akTop, akRight]
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsTrebovanjeStavki
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 37
            Width = 655
          end
          object KOLICINA: TcxDBTextEdit
            Tag = 1
            Left = 101
            Top = 54
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            BeepOnEnter = False
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dm.dsTrebovanjeStavki
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object A_VID: TcxDBTextEdit
            Tag = 1
            Left = 101
            Top = 27
            Hint = #1040#1088#1090#1080#1082#1072#1083' - '#1074#1080#1076
            BeepOnEnter = False
            DataBinding.DataField = 'VID_ARTIKAL'
            DataBinding.DataSource = dm.dsTrebovanjeStavki
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 42
          end
          object A_SIFRA: TcxDBTextEdit
            Tag = 1
            Left = 144
            Top = 27
            Hint = #1040#1088#1090#1080#1082#1072#1083' - '#1096#1080#1092#1088#1072
            BeepOnEnter = False
            DataBinding.DataField = 'ARTIKAL'
            DataBinding.DataSource = dm.dsTrebovanjeStavki
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 42
          end
          object ZapisiStavkaButton: TcxButton
            Left = 592
            Top = 137
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 6
          end
          object OtkaziStavkaButton: TcxButton
            Left = 673
            Top = 137
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 7
          end
          object A_NAZIV: TcxExtLookupComboBox
            Left = 186
            Top = 27
            Hint = #1040#1088#1090#1080#1082#1072#1083' - '#1085#1072#1079#1080#1074
            Anchors = [akLeft, akTop, akRight]
            Properties.ClearKey = 46
            Properties.DropDownSizeable = True
            Properties.OnCloseUp = A_NAZIVPropertiesCloseUp
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 570
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 337
    Width = 1169
    Height = 8
    AlignSplitter = salTop
    MinSize = 180
    Control = PanelTrebovanje
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 968
    Top = 328
  end
  object PopupMenu1: TPopupMenu
    Left = 520
    Top = 304
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 768
    Top = 344
    PixelsPerInch = 96
    object dxBarManager1BarOdlukaNabavka: TdxBar
      Caption = #1058#1088#1077#1073#1086#1074#1072#1114#1072
      CaptionButtons = <>
      DockedLeft = 157
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 521
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 953
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar: TdxBar
      Caption = #1057#1090#1072#1074#1082#1080
      CaptionButtons = <>
      DockedLeft = 332
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1075#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      CaptionButtons = <>
      DockedLeft = 929
      DockedTop = 0
      FloatLeft = 1044
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1203
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ComboGodina'
        end
        item
          Visible = True
          ItemName = 'ComboStatus'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1072
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabelaTrebovanja
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1090#1088#1077#1073#1086#1074#1072#1114#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object cbGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      Items.Strings = (
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017')
      ItemIndex = -1
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavki
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavka
      Caption = #1041#1088#1080#1096#1080' '
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1090#1072#1074#1082#1072
      Category = 0
      Visible = ivAlways
      ShortCut = 16501
      LargeImageIndex = 12
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = #1054#1089#1074#1077#1078#1080
      Category = 0
      Visible = ivAlways
      ShortCut = 16502
      OnClick = aRefreshGodisenPlanSektorExecute
      LargeImageIndex = 52
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aZacuvajExcelStavki
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabelaStavki
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aPageSetupStavki
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aSnimiPecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSnimiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aBrisiIzgledStavki
      Category = 0
      LargeImageIndex = 56
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1087#1088#1077#1076#1083#1086#1075' '#1087#1083#1072#1085' '#1079#1072' '#1089#1083#1077#1076#1085#1072#1090#1072' '#1075#1086#1076#1080#1085#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 8
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxTrackBarProperties'
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aRefreshGodisenPlan
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Caption = #1044#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
      Category = 0
      Visible = ivAlways
      ShortCut = 16452
      LargeImageIndex = 41
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = False
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end>
    end
    object dxBarButton2: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxBarButton3: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1087#1086' '#1088#1072#1073#1086#1090#1085#1080' '#1077#1076#1080#1085#1080#1094#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      OnClick = aPGodisenPlanPoSektoriExecute
    end
    object dxBarButton4: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1080#1079#1073#1088#1072#1085#1072' '#1088#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxBarButton5: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1087#1086#1076#1089#1077#1082#1090#1086#1088#1080'/'#1086#1076#1077#1083#1077#1085#1080#1112#1072' '#1085#1072' '#1089#1077#1082#1090#1086#1088
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxBarButton6: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080')'
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      OnClick = aPGodisenPlanOdJNExecute
    end
    object dxBarButton7: TdxBarButton
      Caption = #1054#1076#1083#1091#1082#1072' '#1079#1072' '#1082#1088#1077#1080#1088#1072#1114#1077' '#1080' '#1080#1079#1084#1077#1085#1072' '#1085#1072' '#1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxBarButton8: TdxBarButton
      Caption = #1043#1086#1076#1080#1096#1077#1085' '#1087#1083#1072#1085' ('#1076#1080#1085#1072#1084#1080#1082#1072' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072')'
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aAzurirajStavki
      Category = 0
    end
    object dxBarCombo1: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ItemIndex = -1
    end
    object dxBarCombo2: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ItemIndex = -1
    end
    object ComboGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = ComboGodinaChange
      Items.Strings = (
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      ItemIndex = -1
    end
    object ComboStatus: TdxBarCombo
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      OnChange = ComboStatusChange
      Items.Strings = (
        #1054#1090#1074#1086#1088#1077#1085#1086
        #1056#1077#1072#1083#1080#1079#1080#1088#1072#1085#1086
        #1057#1090#1086#1088#1085#1080#1088#1072#1085#1086)
      ItemIndex = -1
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aPregledajPecatiTrebovanje
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 888
    Top = 88
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1090#1088#1077#1073#1086#1074#1072#1114#1072
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabelaTrebovanja: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '
      ImageIndex = 30
      OnExecute = aPecatiTabelaTrebovanjaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aDodadiStavki: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 70
      ShortCut = 16500
      OnExecute = aDodadiStavkiExecute
    end
    object aBrisiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 71
      ShortCut = 16503
      OnExecute = aBrisiStavkaExecute
    end
    object aZacuvajExcelStavki: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1089#1086' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcelStavkiExecute
    end
    object aPecatiTabelaStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1090#1088#1077#1073#1086#1074#1072#1114#1077
      Checked = True
      ImageIndex = 30
      OnExecute = aPecatiTabelaStavkiExecute
    end
    object aPodesuvanjePecatenjeStavki: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeStavkiExecute
    end
    object aPageSetupStavki: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupStavkiExecute
    end
    object aSnimiPecatenjeStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeStavkiExecute
    end
    object aBrisiPodesuvanjePecatenjeStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeStavkiExecute
    end
    object aSnimiIzgledStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledStavkiExecute
    end
    object aBrisiIzgledStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      OnExecute = aBrisiIzgledStavkiExecute
    end
    object aRefreshGodisenPlan: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 52
      ShortCut = 16502
      OnExecute = aRefreshGodisenPlanExecute
    end
    object aAzurirajStavki: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '
      ImageIndex = 72
      ShortCut = 16501
      OnExecute = aAzurirajStavkiExecute
    end
    object aPregledajPecatiTrebovanje: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      ImageIndex = 19
      OnExecute = aPregledajPecatiTrebovanjeExecute
    end
    object aDizajnTrebovanje: TAction
      Caption = 'aDizajnTrebovanje'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDizajnTrebovanjeExecute
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 648
    Top = 344
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1024
    Top = 80
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 1136
    Top = 56
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 15000
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42989.567224814810000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 20000
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41599.453453888890000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    PopupMenus = <>
    Left = 768
    Top = 128
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 208
    Top = 520
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'StyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object qCountRe: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      ''
      ''
      'select count(mr.id) as cRe'
      'from mat_re mr'
      
        'where mr.id = :re and mr.r = 1 and mr.spisok like :roditel || '#39'%' +
        #39
      
        '      and ((mr.id in (select s.re from sys_user_re_app s where s' +
        '.username = :username and s.app = :APP  and :param = 1)'
      '          ) or (:param = 0))'
      '')
    Left = 1072
    Top = 160
  end
end
