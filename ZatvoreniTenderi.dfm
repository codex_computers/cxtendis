object frmZatvoreniTenderi: TfrmZatvoreniTenderi
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1032#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080
  ClientHeight = 439
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 329
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 14
      Width = 292
      Height = 26
      Caption = 
        #1057#1090#1072#1090#1091#1089#1086#1090' '#1085#1072' '#1086#1074#1080#1077' '#1112#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080' '#1090#1088#1077#1073#1072' '#1076#1072' '#1087#1088#1077#1084#1080#1085#1077' '#1074#1086' '#1047#1040#1058#1042#1054#1056#1045#1053' '#1073#1080#1076#1077 +
        #1112#1116#1080' '#1088#1086#1082#1086#1090' '#1085#1072' '#1085#1080#1074#1085#1080#1090#1077' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1077' '#1080#1089#1090#1077#1095#1077#1085
      WordWrap = True
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 57
    Width = 329
    Height = 322
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dsProcTenderZatvori
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1BROJ_TENDER_O: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ_TENDER_O'
        Width = 323
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 379
    Width = 329
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      329
      41)
    object cxButton1: TcxButton
      Left = 48
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1075#1080
      OptionsImage.ImageIndex = 6
      OptionsImage.Images = dmRes.cxSmallImages
      TabOrder = 0
      OnClick = cxButton1Click
    end
    object cxButton2: TcxButton
      Left = 169
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1054#1090#1082#1072#1078#1080
      OptionsImage.ImageIndex = 21
      OptionsImage.Images = dmRes.cxSmallImages
      TabOrder = 1
      OnClick = cxButton2Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 420
    Width = 329
    Height = 19
    Panels = <
      item
        Text = ' Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
        Width = 50
      end>
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 544
    Top = 144
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 96
    Top = 136
  end
  object ActionList1: TActionList
    Left = 248
    Top = 192
    object ZacuvajExcel: TAction
      Caption = 'ZacuvajExcel'
      ShortCut = 16453
      OnExecute = ZacuvajExcelExecute
    end
  end
end
