inherited frmPricinaPonistuvanje: TfrmPricinaPonistuvanje
  Caption = #1055#1088#1080#1095#1080#1085#1080' '#1079#1072' '#1087#1086#1085#1080#1096#1090#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1089#1090#1072#1087#1082#1072' '
  ExplicitWidth = 749
  ExplicitHeight = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 274
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Height = 270
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPricinaPonistuvanje
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 647
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 161
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 214
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 203
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 244
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 64
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 64
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 64
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Height = 130
    ExplicitTop = 400
    ExplicitHeight = 130
    object lbl1: TLabel [1]
      Left = 31
      Top = 50
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPricinaPonistuvanje
      TabOrder = 1
    end
    inherited OtkaziButton: TcxButton
      Top = 90
      TabOrder = 3
      ExplicitTop = 90
    end
    inherited ZapisiButton: TcxButton
      Top = 90
      TabOrder = 2
      ExplicitTop = 90
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsPricinaPonistuvanje
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 634
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    Top = 240
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41621.442287442130000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
