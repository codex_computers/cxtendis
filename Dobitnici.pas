unit Dobitnici;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 20.03.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  FIBDataSet, pFIBDataSet, dxCustomHint, cxHint, cxLabel, cxDBLabel, FIBQuery,
  pFIBQuery, cxSplitter, cxImageComboBox, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, Vcl.Grids, Vcl.DBGrids;

type
//  niza = Array[1..5] of Variant;

  TfrmDobitnici = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    Panel1: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1ID_PONUDI_STAVKI: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid2DBTableView1PARTNERNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid2DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid2DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid2DBTableView1USR_UPD: TcxGridDBColumn;
    cxGridPopupMenu2: TcxGridPopupMenu;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tblTenderStavki: TpFIBDataSet;
    tblTenderStavkiBROJ_TENDER: TFIBStringField;
    tblTenderStavkiVID_ARTIKAL: TFIBIntegerField;
    tblTenderStavkiARTIKAL: TFIBIntegerField;
    tblTenderStavkiNAZIVARTIKAL: TFIBStringField;
    tblTenderStavkiKOLICINA: TFIBBCDField;
    tblTenderStavkiVIDNAZIV: TFIBStringField;
    tblTenderStavkiMERKA: TFIBStringField;
    tblTenderStavkiMERKANAZIV: TFIBStringField;
    tblTenderStavkiPROZVODITEL: TFIBIntegerField;
    tblTenderStavkiPROIZVODITELNAZIV: TFIBStringField;
    tblTenderStavkiGRUPA: TFIBStringField;
    tblTenderStavkiGRUPANAZIV: TFIBStringField;
    tblTenderStavkiGENERIKA: TFIBIntegerField;
    tblTenderStavkiJN_CPV: TFIBStringField;
    tblTenderStavkiMK: TFIBStringField;
    tblTenderStavkiEN: TFIBStringField;
    tblTenderStavkiJN_TIP_ARTIKAL: TFIBIntegerField;
    dsTenderStavki: TDataSource;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1MK: TcxGridDBColumn;
    cxGrid1DBTableView1EN: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    Label8: TLabel;
    cxDBLabel2: TcxDBLabel;
    Label1: TLabel;
    aOK: TAction;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGridPopupMenu3: TcxGridPopupMenu;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aSnimiIzgledStavkiJN: TAction;
    aBrisiIzgledStavkiJN: TAction;
    cxGrid2DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1ColumnOdberi: TcxGridDBColumn;
    Panel8: TPanel;
    CheckBox1: TcxCheckBox;
    dsPonuduvaciZaSite: TDataSource;
    tblPonuduvaciZaSite: TpFIBDataSet;
    tblPonuduvaciZaSiteTIP_PARTNER: TFIBIntegerField;
    tblPonuduvaciZaSitePARTNER: TFIBIntegerField;
    tblPonuduvaciZaSitePARTNERNAZIV: TFIBStringField;
    tblPonuduvaciZaSiteTIPPARTNERNAZIV: TFIBStringField;
    qCountPonudiZaStavka: TpFIBQuery;
    cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    dxBarLargeButton20: TdxBarLargeButton;
    cxSplitter1: TcxSplitter;
    aIzborStavka: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    aPDobitniciJavnaNabavka: TAction;
    aDizajnDobitniciJavnaNabavka: TAction;
    aDizajnPopupMenu: TAction;
    PopupMenu2: TPopupMenu;
    dxBarButton1: TdxBarButton;
    N2: TMenuItem;
    aPIzvestuvanjeZaDobivanjeTender: TAction;
    aDizajnIzvestuvanjeZaDobivanjeTender: TAction;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    N3: TMenuItem;
    cxGrid2DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn;
    dxBarLargeButton21: TdxBarLargeButton;
    aZacuvajExcelTabelaStavki: TAction;
    Panel2: TPanel;
    cxButton6: TcxButton;
    PanelIzborGenerikaPonuduvac: TPanel;
    Panel6: TPanel;
    cxGrid4: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid4DBTableView1VID: TcxGridDBColumn;
    cxGrid4DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid4DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid4DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid4Level1: TcxGridLevel;
    Panel7: TPanel;
    cxGrid5: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    PanelPonuduvacMulti: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    cxGrid6: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBTableView2TIP_PARTNER: TcxGridDBColumn;
    cxGridDBTableView2TIPPARTNERNAZIV: TcxGridDBColumn;
    cxGridDBTableView2PARTNER: TcxGridDBColumn;
    cxGridDBTableView2PARTNERNAZIV: TcxGridDBColumn;
    cxGridDBTableView2SVBODOVI: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    PanelIzborPonuduvac: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1PONUDI_STAVKI_ID: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGrid3DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid3DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1PARTBERNAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1VREDNOST: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    btnOK: TcxButton;
    cxButton1: TcxButton;
    aOznaciPonusteniStavki: TAction;
    cxButton8: TcxButton;
    aOznaciValidniStavki: TAction;
    cxButton7: TcxButton;
    tblTenderStavkiVALIDNOST: TFIBIntegerField;
    cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_SKRATEN: TcxGridDBColumn;
    tblPonuduvaciZaSiteSVBODOVI: TFIBFloatField;
    tblTenderStavkiPRICINA_PONISTENA: TFIBIntegerField;
    qPricina: TpFIBQuery;
    dsPricinaPonistuvanje: TDataSource;
    tblPricinaPonistuvanje: TpFIBDataSet;
    tblPricinaPonistuvanjeID: TFIBIntegerField;
    tblPricinaPonistuvanjeNAZIV: TFIBStringField;
    tblPricinaPonistuvanjeTS_INS: TFIBDateTimeField;
    tblPricinaPonistuvanjeTS_UPD: TFIBDateTimeField;
    tblPricinaPonistuvanjeUSR_INS: TFIBStringField;
    tblPricinaPonistuvanjeUSR_UPD: TFIBStringField;
    tblPricinaPonistuvanjeCUSTOM1: TFIBStringField;
    tblPricinaPonistuvanjeCUSTOM2: TFIBStringField;
    tblPricinaPonistuvanjeCUSTOM3: TFIBStringField;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TS_INS: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1USR_INS: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1CUSTOM3: TcxGridDBColumn;
    Panel9: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxExtLookupComboBox1: TcxExtLookupComboBox;
    aOznaciPonusteniStavkiPanel: TAction;
    cxButton9: TcxButton;
    cxButton10: TcxButton;
    aOtkaziPonistuvanje: TAction;
    tblTenderStavkiPRICINA_PONISTENA_NAZIV: TFIBStringField;
    cxGrid1DBTableView1PRICINA_PONISTENA_NAZIV: TcxGridDBColumn;
    CheckBox2: TcxCheckBox;
    qGrupa: TpFIBQuery;
    dxBarButton4: TdxBarButton;
    actPIzvestuvanjeZaDobivanjeTenderGrupno: TAction;
    actDIzvestuvanjeZaDobivanjeTenderGrupno: TAction;
    N4: TMenuItem;
    dxBarButton5: TdxBarButton;
    actIzvestuvanjeZaDobitniciExportPdf: TAction;
    dlgOpenExportPdf: TOpenDialog;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOKExecute(Sender: TObject);
    procedure aSnimiIzgledStavkiJNExecute(Sender: TObject);
    procedure aBrisiIzgledStavkiJNExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure aIzborStavkaExecute(Sender: TObject);
    procedure aDizajnPopupMenuExecute(Sender: TObject);
    procedure aPDobitniciJavnaNabavkaExecute(Sender: TObject);
    procedure aDizajnDobitniciJavnaNabavkaExecute(Sender: TObject);
    procedure aDizajnIzvestuvanjeZaDobivanjeTenderExecute(Sender: TObject);
    procedure aPIzvestuvanjeZaDobivanjeTenderExecute(Sender: TObject);
    procedure aZacuvajExcelTabelaStavkiExecute(Sender: TObject);
    procedure aOznaciValidniStavkiExecute(Sender: TObject);
    procedure aOznaciPonusteniStavkiExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid1DBTableView1PRICINA_PONISTENAPropertiesCloseUp(
      Sender: TObject);
    procedure cxGridViewRepository1DBTableView1SelectionChanged(
      Sender: TcxCustomGridTableView);
    procedure cxGridViewRepository1DBTableView1MouseLeave(Sender: TObject);
    procedure aOznaciPonusteniStavkiPanelExecute(Sender: TObject);
    procedure aOtkaziPonistuvanjeExecute(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure actPIzvestuvanjeZaDobivanjeTenderGrupnoExecute(Sender: TObject);
    procedure actDIzvestuvanjeZaDobivanjeTenderGrupnoExecute(Sender: TObject);
    procedure actIzvestuvanjeZaDobitniciExportPdfExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmDobitnici: TfrmDobitnici;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmProcedureQuey,
  dmReportUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmDobitnici.CheckBox1Click(Sender: TObject);
var i:Integer;
begin
     if CheckBox1.Checked then
       begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ColumnOdberi.Index]:=  true;
            end;
        end
      else
        begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ColumnOdberi.Index]:= false;
            end;
     end
end;

constructor TfrmDobitnici.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmDobitnici.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmDobitnici.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmDobitnici.aBrisiExecute(Sender: TObject);
begin
   if dm.tblTenderPONISTEN.Value = 1 then
     begin
       dm.qBrisiDobitnik.Close;
       dm.qBrisiDobitnik.ParamByName('vid_artikal').Value:=dm.tblDobitniciTenderVID_ARTIKAL.Value;
       dm.qBrisiDobitnik.ParamByName('artikal').Value:=dm.tblDobitniciTenderARTIKAL.Value;
       dm.qBrisiDobitnik.ParamByName('partner').Value:=dm.tblDobitniciTenderPARTNER.Value;
       dm.qBrisiDobitnik.ParamByName('tip_partner').Value:=dm.tblDobitniciTenderTIP_PARTNER.Value;
       dm.qBrisiDobitnik.ParamByName('broj_tender').Value:=dm.tblDobitniciTenderBROJ_TENDER.Value;
       dm.qBrisiDobitnik.ExecQuery;

       if dm.qBrisiDobitnik.FldByName['br'].Value = 0  then
         begin
           if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
            begin
              cxGrid2DBTableView1.DataController.DataSet.Delete();
              tblTenderStavki.Close;
              tblTenderStavki.Open;
            end;
         end
       else ShowMessage('������������� ������ � ������� �� ������� � ������ �� �� ������� !');
     end
   else ShowMessage('���������� � ���������. �� � ��������� ������ �� ��������� !');
end;

procedure TfrmDobitnici.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid2DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmDobitnici.aBrisiIzgledStavkiJNExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid1DBTableView1);
     BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmDobitnici.aRefreshExecute(Sender: TObject);
begin
  cxGrid2DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid2DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmDobitnici.aIzborStavkaExecute(Sender: TObject);
var br,i:integer;
begin

               br:=0;
               with cxGrid1DBTableView1.DataController do
               for I := 0 to FilteredRecordCount - 1 do
                  begin
                     if (GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true) and (GetValue(i,cxGrid1DBTableView1VALIDNOST.Index) = 1)then
                        br:=br+1;
                  end;
               if br = 0 then
                  begin
                     ShowMessage('������ ���������� ���� ���� ������� ������ !!!');
                     cxGrid1.SetFocus;
                  end
               else if br=1 then
                  begin
                     with cxGrid1DBTableView1.DataController do
                     for I := 0 to FilteredRecordCount - 1 do
                         begin
                           if (GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true) and (GetValue(i,cxGrid1DBTableView1Validnost.Index)= 1) then
                              begin
                                 if GetValue(i,cxGrid1DBTableView1GENERIKA.Index) = 0 then
                                    begin
                                      PanelIzborPonuduvac.Visible:=true;
                                      dm.tblDobitniciIzborPonuduvac.Close;
                                      dm.tblDobitniciIzborPonuduvac.ParamByName('ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                                      dm.tblDobitniciIzborPonuduvac.ParamByName('VID_ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                                      dm.tblDobitniciIzborPonuduvac.Open;
                                      cxGrid3.SetFocus;
                                   end
                                 else  if GetValue(i,cxGrid1DBTableView1GENERIKA.Index) = 1 then
                                   begin
                                     PanelIzborGenerikaPonuduvac.Visible:=true;
                                     dm.tblDobitniciIzborPonuduvac.Close;
                                     dm.tblDobitniciIzborPonuduvac.ParamByName('ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                                     dm.tblDobitniciIzborPonuduvac.ParamByName('VID_ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                                     dm.tblDobitniciIzborPonuduvac.Open;

                                     dm.tblDobitniciIzborArtikal.Close;
                                     dm.tblDobitniciIzborArtikal.ParamByName('ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                                     dm.tblDobitniciIzborArtikal.ParamByName('VID_ARTIKAL').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                                     dm.tblDobitniciIzborArtikal.Open;
                                     cxGrid4.SetFocus;
                                   end
                              end
                         end
                  end
               else if br>1 then
                  begin
                     PanelPonuduvacMulti.Visible:=true;
                     cxGrid6.SetFocus;
                     tblPonuduvaciZaSite.Close;
                     tblPonuduvaciZaSite.Open;
                  end

end;

procedure TfrmDobitnici.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmDobitnici.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
  ZacuvajFormaIzgled(self);
end;

procedure TfrmDobitnici.aSnimiIzgledStavkiJNExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmDobitnici.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid2, Caption);
end;

procedure TfrmDobitnici.aZacuvajExcelTabelaStavkiExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, '������ �� �� �� ��� ���� �������� ��������');
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmDobitnici.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmDobitnici.CheckBox2Click(Sender: TObject);
var i:Integer;
begin
//     ShowMessage(tblTenderStavkiGRUPANAZIV.Value);
     //������� ���� ������������� ����� � ��������
     qGrupa.Close;
     qGrupa.ParamByName('broj_tender').Value:=tblTenderStavkiBROJ_TENDER.Value;
     qGrupa.ParamByName('grupa').Value:=tblTenderStavkiGRUPA.Value;
     qGrupa.ExecQuery;
     if qGrupa.FldByName['br'].Value = 1 then
        begin
          if CheckBox2.Checked then
            begin
               with cxGrid1DBTableView1.DataController do
               for I := 0 to FilteredRecordCount - 1 do
                  begin
                     if Values[FilteredRecordIndex[i] , cxGrid1DBTableView1GRUPA.Index] = tblTenderStavkiGRUPA.Value then
                        Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ColumnOdberi.Index]:=  true;
                  end;
            end
          else
            begin
               with cxGrid1DBTableView1.DataController do
               for I := 0 to FilteredRecordCount - 1 do
                  begin
                     Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ColumnOdberi.Index]:= false;
                  end;
             end
        end
     else
       begin
         ShowMessage('������������� ����� �� � ��������');
      //   CheckBox2.Checked:=False;
       end;
end;

procedure TfrmDobitnici.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmDobitnici.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmDobitnici.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmDobitnici.cxGrid1DBTableView1PRICINA_PONISTENAPropertiesCloseUp(
  Sender: TObject);
begin
     qPricina.Close;
     qPricina.ParamByName('PRICINA_PONISTENA').Value:=tblPricinaPonistuvanjeID.Value ;
  //  qPricina.ParamByName('PRICINA_PONISTENA').Value:=cxGridViewRepository1DBTableView1ID.EditValue;
     qPricina.ParamByName('OLD_VID_ARTIKAL').Value:=tblTenderStavkiVID_ARTIKAL.Value ;
     qPricina.ParamByName('OLD_ARTIKAL').Value:=tblTenderStavkiARTIKAL.Value ;
     qPricina.ParamByName('OLD_BROJ_TENDER').Value:=tblTenderStavkiBROJ_TENDER.Value ;
     qPricina.ExecQuery;
  //   cxGrid1DBTableView1.Refresh;
    // tblTenderStavki.Edit;
//        tblTenderStavkiPRICINA_PONISTENA.Value:=tblPricinaPonistuvanjeID.Value;
//        tblTenderStavki.Post;
end;

procedure TfrmDobitnici.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1VALIDNOST.Index] = 0) then
       AStyle := dmRes.RedLight;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmDobitnici.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmDobitnici.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmDobitnici.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmDobitnici.prefrli;
begin
end;

procedure TfrmDobitnici.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmDobitnici.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmDobitnici.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    caption:='��������� �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' ������ '+IntToStr(dm.tblTenderGODINA.Value);
    Label8.Caption:='������ �� ����� ������� ��� '+dm.tblTenderBROJ.Value +' �� ��� ���� �������� ���������';
    Label1.Caption:='��������� �� ����� ������� ��� '+dm.tblTenderBROJ.Value;

    PanelIzborPonuduvac.Top:=300;
    PanelIzborPonuduvac.Left:=130;

    PanelPonuduvacMulti.Top:=300;
    PanelPonuduvacMulti.Left:=130;

    PanelIzborGenerikaPonuduvac.Top:=300;
    PanelIzborGenerikaPonuduvac.Left:=300;

    dm.tblDobitniciTender.Close;
    dm.tblDobitniciTender.Open;
    tblPricinaPonistuvanje.Open;

    tblTenderStavki.Close;
    tblTenderStavki.Open;
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link2);
end;
//------------------------------------------------------------------------------

procedure TfrmDobitnici.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmDobitnici.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmDobitnici.cxGridViewRepository1DBTableView1MouseLeave(
  Sender: TObject);
begin
         ShowMessage(intToStr(tblPricinaPonistuvanjeID.Value));
end;

procedure TfrmDobitnici.cxGridViewRepository1DBTableView1SelectionChanged(
  Sender: TcxCustomGridTableView);
begin
    
end;

//  ����� �� �����
procedure TfrmDobitnici.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmDobitnici.aOKExecute(Sender: TObject);
var i, pom:integer;
    nadminat_iznos:string;
begin
     if PanelIzborPonuduvac.Visible = true then
        begin
           dm.qSporedbaPlanPonuda.Close;
           dm.qSporedbaPlanPonuda.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
           dm.qSporedbaPlanPonuda.ParamByName('tip_partner').Value:=dm.tblDobitniciIzborPonuduvacTIP_PARTNER.Value;
           dm.qSporedbaPlanPonuda.ParamByName('partner').Value:=dm.tblDobitniciIzborPonuduvacPARTNER.Value;
           dm.qSporedbaPlanPonuda.ParamByName('vid_artikal').Value:=dm.tblDobitniciIzborPonuduvacVID_ARTIKAL.Value;
           dm.qSporedbaPlanPonuda.ParamByName('artikal').Value:=dm.tblDobitniciIzborPonuduvacARTIKAL.Value;
           dm.qSporedbaPlanPonuda.ExecQuery;
           if (dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odluka_majpovolen'].Value > dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odlukajn'].Value) then
             begin
               frmDaNe := TfrmDaNe.Create(self, '���������� �� ��������', '���������� ����� � ������� �� ����������� !!!. ���� ��������� ������ �� �� �������� ����������?', 1);
               if (frmDaNe.ShowModal = mrYes) then
                 begin
                    dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI', 'VID_ARTIKAL','ARTIKAL',Null,Null, 0, dm.tblDobitniciIzborPonuduvacPONUDI_STAVKI_ID.Value, dm.tblDobitniciIzborPonuduvacVID_ARTIKAL.Value,dm.tblDobitniciIzborPonuduvacARTIKAL.Value,Null,Null);

                    dm.tblDobitniciTender.Close;
                    dm.tblDobitniciTender.Open;

                    tblTenderStavki.Close;
                    tblTenderStavki.Open;
                    PanelIzborPonuduvac.Visible:=false;
                 end
               else PanelIzborPonuduvac.Visible:=false;
             end
          else
              begin
                    dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI', 'VID_ARTIKAL','ARTIKAL',Null,Null, 0, dm.tblDobitniciIzborPonuduvacPONUDI_STAVKI_ID.Value, dm.tblDobitniciIzborPonuduvacVID_ARTIKAL.Value,dm.tblDobitniciIzborPonuduvacARTIKAL.Value,Null,Null);

                    dm.tblDobitniciTender.Close;
                    dm.tblDobitniciTender.Open;

                    tblTenderStavki.Close;
                    tblTenderStavki.Open;
                    PanelIzborPonuduvac.Visible:=false;
              end
        end
     else if PanelIzborGenerikaPonuduvac.Visible = true then
        begin
           dm.qSporedbaPlanPonuda.Close;
           dm.qSporedbaPlanPonuda.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
           dm.qSporedbaPlanPonuda.ParamByName('tip_partner').Value:=dm.tblDobitniciIzborPonuduvacTIP_PARTNER.Value;
           dm.qSporedbaPlanPonuda.ParamByName('partner').Value:=dm.tblDobitniciIzborPonuduvacPARTNER.Value;
           dm.qSporedbaPlanPonuda.ParamByName('vid_artikal').Value:=dm.tblDobitniciIzborPonuduvacVID_ARTIKAL.Value;
           dm.qSporedbaPlanPonuda.ParamByName('artikal').Value:=dm.tblDobitniciIzborPonuduvacARTIKAL.Value;
           dm.qSporedbaPlanPonuda.ExecQuery;
           if (dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odluka_majpovolen'].Value > dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odlukajn'].Value) then
             begin
               frmDaNe := TfrmDaNe.Create(self, '���������� �� ��������', '���������� ����� � ������� �� ����������� !!!. ���� ��������� ������ �� �� �������� ����������?', 1);
               if (frmDaNe.ShowModal = mrYes) then
                 begin
                   dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI','VID_ARTIKAL','ARTIKAL',Null,Null, 0, dm.tblDobitniciIzborPonuduvacPONUDI_STAVKI_ID.Value,dm.tblDobitniciIzborArtikalARTVID.Value, dm.tblDobitniciIzborArtikalARTIKAL.Value,Null,Null);

                   dm.tblDobitniciTender.Close;
                   dm.tblDobitniciTender.Open;

                   tblTenderStavki.Close;
                   tblTenderStavki.Open;
                   PanelIzborGenerikaPonuduvac.Visible:=false;
                 end
               else PanelIzborGenerikaPonuduvac.Visible:=false;
             end
          else
              begin
                dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI','VID_ARTIKAL','ARTIKAL',Null,Null, 0, dm.tblDobitniciIzborPonuduvacPONUDI_STAVKI_ID.Value,dm.tblDobitniciIzborArtikalARTVID.Value, dm.tblDobitniciIzborArtikalARTIKAL.Value,Null,Null);

                dm.tblDobitniciTender.Close;
                dm.tblDobitniciTender.Open;

                tblTenderStavki.Close;
                tblTenderStavki.Open;
                PanelIzborGenerikaPonuduvac.Visible:=false;
              end

        end
     else if PanelPonuduvacMulti.Visible = true then
        begin
           pom:=0;
           nadminat_iznos:='';
           with cxGrid1DBTableView1.DataController do
           for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
            begin
              if (GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true) and (GetValue(i,cxGrid1DBTableView1VALIDNOST.Index) = 1) then
                begin
                   qCountPonudiZaStavka.Close;
                   qCountPonudiZaStavka.ParamByName('partner').Value:=tblPonuduvaciZaSitePARTNER.Value;
                   qCountPonudiZaStavka.ParamByName('tip_partner').Value:=tblPonuduvaciZaSiteTIP_PARTNER.Value;
                   qCountPonudiZaStavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                   qCountPonudiZaStavka.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                   qCountPonudiZaStavka.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                   qCountPonudiZaStavka.ExecQuery;


                   if qCountPonudiZaStavka.FldByName['c'].Value = 1 then
                      begin
                         dm.qSporedbaPlanPonuda.Close;
                         dm.qSporedbaPlanPonuda.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
                         dm.qSporedbaPlanPonuda.ParamByName('tip_partner').Value:=tblPonuduvaciZaSiteTIP_PARTNER.Value;;
                         dm.qSporedbaPlanPonuda.ParamByName('partner').Value:=tblPonuduvaciZaSitePARTNER.Value;
                         dm.qSporedbaPlanPonuda.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                         dm.qSporedbaPlanPonuda.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                         dm.qSporedbaPlanPonuda.ExecQuery;
                         if (dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odluka_majpovolen'].Value > dm.qSporedbaPlanPonuda.FldByName['iznos_bezddv_odlukajn'].Value) then
                           begin
                            frmDaNe := TfrmDaNe.Create(self, '���������� �� ��������', '���������� ����� � ������� �� ����������� �� ������ '+IntToStr(GetValue(i,cxGrid1DBTableView1ARTIKAL.Index))+' !!!. ���� ��������� ������ �� �� �������� ���������� ?', 1);
                            if (frmDaNe.ShowModal = mrYes) then
                               dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI','VID_ARTIKAL','ARTIKAL',Null,Null, 0, qCountPonudiZaStavka.FldByName['id'].Value,GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index), GetValue(i,cxGrid1DBTableView1ARTIKAL.Index),Null,Null);
                           end
                         else dmPQ.insert6(dmPQ.pInsertDobitnici,'GEN','ID_PONUDI_STAVKI','VID_ARTIKAL','ARTIKAL',Null,Null, 0, qCountPonudiZaStavka.FldByName['id'].Value,GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index), GetValue(i,cxGrid1DBTableView1ARTIKAL.Index),Null,Null);
                      end
                   else
                      begin
                         SetValue(i,cxGrid1DBTableView1ColumnOdberi.Index,true);
                         pom:=1;
                      end;
                end;
            end;


          dm.tblDobitniciTender.Close;
          dm.tblDobitniciTender.Open;

          tblTenderStavki.Close;
          tblTenderStavki.Open;
          PanelPonuduvacMulti.Visible:=false;

           if pom = 1 then
              begin
                 ShowMessage('�� ����� �� ������������� ������ ������ ������������ ������ �� ��������� ��������� !!!');
              end;
           CheckBox1.Checked:=false;
        end;
end;

procedure TfrmDobitnici.aOtkaziExecute(Sender: TObject);
begin
  if PanelIzborPonuduvac.Visible = true then
     PanelIzborPonuduvac.Visible:=false
  else if PanelPonuduvacMulti.Visible = true then
     PanelPonuduvacMulti.Visible:=false
  else  if PanelIzborGenerikaPonuduvac.Visible = true then
     PanelIzborGenerikaPonuduvac.Visible:=false
  else
    begin
      ModalResult := mrCancel;
      Close();
    end
end;

procedure TfrmDobitnici.aOtkaziPonistuvanjeExecute(Sender: TObject);
begin
      Panel9.Visible:=False;
end;

procedure TfrmDobitnici.aOznaciPonusteniStavkiExecute(Sender: TObject);
var I:integer;
begin

 if not dm.tblTenderVRSKA_PREDMET.IsNull then
    begin
      dm.qCountStavkiTender.Close;
      dm.qCountStavkiTender.ParamByName('broj').Value:=dm.tblTenderVRSKA_PREDMET.Value;
      dm.qCountStavkiTender.ExecQuery;
      if (dm.qCountStavkiTender.FldByName['br'].Value = 0) then
          begin

            with cxGrid1DBTableView1.DataController do
            for I := 0 to FilteredRecordCount - 1 do
               begin
                  if GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true then
                     begin
                        dmPQ.qUpdateValidnostTenderStavka.Close;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('validnost').Value:=0;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('pricina').Value:=tblPricinaPonistuvanjeID.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ExecQuery
                      end;
                end;
            tblTenderStavki.FullRefresh;
            CheckBox1.Checked:=False;
          end
      else ShowMessage('����������� ������ ��� �� ��������� �� ����� ��������. �� � ��������� ������� �� ������ !!!');
    end
 else
    begin
      with cxGrid1DBTableView1.DataController do
            for I := 0 to FilteredRecordCount - 1 do
               begin

                  if GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true then
                     begin
                        dmPQ.qUpdateValidnostTenderStavka.Close;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('validnost').Value:=0;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('pricina').Value:=tblPricinaPonistuvanjeID.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ExecQuery
                      end;
                end;
            tblTenderStavki.FullRefresh;
            CheckBox1.Checked:=False;
    end;
    Panel9.Visible:=false ;
end;

procedure TfrmDobitnici.aOznaciPonusteniStavkiPanelExecute(Sender: TObject);
begin
      Panel9.Visible:=True;
end;

procedure TfrmDobitnici.aOznaciValidniStavkiExecute(Sender: TObject);
var I:integer;
begin
 if not dm.tblTenderVRSKA_PREDMET.IsNull then
    begin
      dm.qCountStavkiTender.Close;
      dm.qCountStavkiTender.ParamByName('broj').Value:=dm.tblTenderVRSKA_PREDMET.Value;
      dm.qCountStavkiTender.ExecQuery;
      if (dm.qCountStavkiTender.FldByName['br'].Value = 0) then
          begin
            with cxGrid1DBTableView1.DataController do
            for I := 0 to FilteredRecordCount - 1 do
               begin
                  if GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true then
                     begin
                        dmPQ.qUpdateValidnostTenderStavka.Close;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('validnost').Value:=1;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('pricina').Value:=null;
                        dmPQ.qUpdateValidnostTenderStavka.ExecQuery
                      end;
                end;
            tblTenderStavki.FullRefresh;
            CheckBox1.Checked:=False;
          end
      else ShowMessage('����������� ������ ��� �� ��������� �� ����� ��������. �� � ��������� ������� �� ������ !!!');
    end
 else
    begin
      with cxGrid1DBTableView1.DataController do
            for I := 0 to FilteredRecordCount - 1 do
               begin
                  if GetValue(i,cxGrid1DBTableView1ColumnOdberi.Index) = true then
                     begin
                        dmPQ.qUpdateValidnostTenderStavka.Close;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('vid_artikal').Value:=GetValue(i,cxGrid1DBTableView1VID_ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('artikal').Value:=GetValue(i,cxGrid1DBTableView1ARTIKAL.Index);
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('validnost').Value:=1;
                        dmPQ.qUpdateValidnostTenderStavka.ParamByName('pricina').Value:=null;
                        dmPQ.qUpdateValidnostTenderStavka.ExecQuery
                      end;
                end;
            tblTenderStavki.FullRefresh;
            CheckBox1.Checked:=False;
    end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmDobitnici.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmDobitnici.aPDobitniciJavnaNabavkaExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('JN',40014);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblDobitniciTenderBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ������� �� ������
procedure TfrmDobitnici.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmDobitnici.aPIzvestuvanjeZaDobivanjeTenderExecute(Sender: TObject);
begin
try
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:=dm.tblDobitniciTenderTIP_PARTNER.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:=dm.tblDobitniciTenderPARTNER.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

    dmRes.Spremi('JN',40015);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblDobitniciTenderBROJ_TENDER.Value;
    dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblDobitniciTenderTIP_PARTNER.Value;
    dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblDobitniciTenderPARTNER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmDobitnici.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.DesignReport();
end;

procedure TfrmDobitnici.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmDobitnici.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmDobitnici.actDIzvestuvanjeZaDobivanjeTenderGrupnoExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40175);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmDobitnici.actIzvestuvanjeZaDobitniciExportPdfExecute(
  Sender: TObject);
  var file_pateka:string;
      zafakt:integer;
      i, vkupno :integer;
begin
 //if (pat_dokumenti <> '') then
   //begin
     dlgOpenExportPdf.Execute();
     dm.tblDobitniciPdf.Close;
     dm.tblDobitniciPdf.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     dm.tblDobitniciPdf.Open;
     dm.tblDobitniciPdf.FetchAll;
     vkupno := dm.tblDobitniciPdf.RecordCount;
     if vkupno > 0 then
       begin
         dm.tblDobitniciPdf.First;
         try
           for i := 0 to vkupno - 1 do
             begin
                try
                  dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
                  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
                  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:=dm.tblDobitniciPdfTIP_PARTNER.Value;
                  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:=dm.tblDobitniciPdfPARTNER.Value;
                  dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

                  dmRes.Spremi('JN',40015);
                  dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblDobitniciTenderBROJ_TENDER.Value;
                  dmKon.tblSqlReport.ParamByName('tip').Value:=dm.tblDobitniciPdfTIP_PARTNER.Value;
                  dmKon.tblSqlReport.ParamByName('partner').Value:=dm.tblDobitniciPdfPARTNER.Value;
                  dmKon.tblSqlReport.Open;

                 // file_pateka := pat_dokumenti +'\'+dmKon.user+'\IzvesuvanjeDobitnik_' +IntToStr(dm.tblDobitniciPdfTIP_PARTNER.Value)+'_'+ IntToStr(dm.tblDobitniciPdfPARTNER.Value)+ '.pdf';
                  file_pateka := dlgOpenExportPdf.FileName+IntToStr(dm.tblDobitniciPdfTIP_PARTNER.Value)+'_'+ IntToStr(dm.tblDobitniciPdfPARTNER.Value)+ '.pdf';

                  dmRes.frxPDFExport1.ShowDialog := False;
                  dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
                  dmRes.frxPDFExport1.FileName := file_pateka;
                  dmRes.frxReport1.PrepareReport(True);
                  dmRes.frxReport1.Export(dmRes.frxPDFExport1);

                  dm.tblDobitniciPdf.Next;
                except
                 ShowMessage('�� ���� �� �� ������� ���������!');
                end;
             end
         finally
            ShowMessage('������������� �� ��������� �� �������� !');
         end;
       end  else ShowMessage('���� ��������� �� �����������!');
   //end    else    ShowMessage('������ ���������� ������ �� ���������� �� �����������');
end;

procedure TfrmDobitnici.actPIzvestuvanjeZaDobivanjeTenderGrupnoExecute(
  Sender: TObject);
begin
   try
    dmRes.Spremi('JN',40175);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblDobitniciTenderBROJ_TENDER.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmDobitnici.aDizajnDobitniciJavnaNabavkaExecute(Sender: TObject);
begin
     dmRes.Spremi('JN',40014);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmDobitnici.aDizajnIzvestuvanjeZaDobivanjeTenderExecute(
  Sender: TObject);
begin
     dmRes.Spremi('JN',40015);
     dmRes.frxReport1.DesignReport();
end;

procedure TfrmDobitnici.aDizajnPopupMenuExecute(Sender: TObject);
begin
     PopupMenu2.Popup(500,500 );
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmDobitnici.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmDobitnici.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link2);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmDobitnici.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link2.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link2);
  finally
    AStream.Free;
  end;
end;


end.
