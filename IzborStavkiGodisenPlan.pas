unit IzborStavkiGodisenPlan;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 10.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxStatusBarPainter, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, StdCtrls,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxStatusBar, ExtCtrls, ActnList,
  cxContainer, cxGroupBox, cxGridCustomPopupMenu, cxGridPopupMenu, Menus,
  cxButtons, dxScreenTip, dxCustomHint, cxHint, cxCheckBox, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, cxImageComboBox, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, cxNavigator,
  System.Actions;

type
  TfrmIzborStavkiGodisenPlan = class(TForm)
    topPanell: TPanel;
    dxStatusBar1: TdxStatusBar;
    gridPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label2: TLabel;
    labelRabotnaEdinica: TLabel;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANZIV: TcxGridDBColumn;
    ActionList1: TActionList;
    aIzlez: TAction;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    labelGodisenPlanBroj: TLabel;
    labelGodisenPlanGodina: TLabel;
    aSnimiIzgled: TAction;
    aBrisiIzgled: TAction;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid1DBTableView1CENA: TcxGridDBColumn;
    cxGrid1DBTableView1TARIFA: TcxGridDBColumn;
    aVnesNaArtikliVoPlan: TAction;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1AktivenNaziv: TcxGridDBColumn;
    panelBottom: TPanel;
    ZapisiButton: TcxButton;
    aSifArtikli: TAction;
    Label4: TLabel;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1ARTVID: TcxGridDBColumn;
    aFormConfig: TAction;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aIzlezExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aVnesNaArtikliVoPlanExecute(Sender: TObject);
    procedure aSifArtikliExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzborStavkiGodisenPlan: TfrmIzborStavkiGodisenPlan;

implementation

uses dmUnit, dmMaticni, Utils, dmProcedureQuey, dmResources, Artikal,
  dmKonekcija, FormConfig, GodisenPlan;

{$R *.dfm}

procedure TfrmIzborStavkiGodisenPlan.aBrisiIzgledExecute(Sender: TObject);
begin
     brisiGridVoBaza(Name,cxGrid1DBTableView1);
     BrisiFormaIzgled(self);
end;

procedure TfrmIzborStavkiGodisenPlan.aFormConfigExecute(Sender: TObject);
begin
     frmFormConfig:=TfrmFormConfig.Create(Application);
     frmFormConfig.formPtr:= Addr(Self);
     frmFormConfig.ShowModal;
     frmFormConfig.Free;
end;

procedure TfrmIzborStavkiGodisenPlan.aIzlezExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmIzborStavkiGodisenPlan.aSifArtikliExecute(Sender: TObject);
begin
    if prava_re = 0 then
        begin
          frmArtikal:=TfrmArtikal.Create(self,false);
          frmArtikal.ShowModal;
          if (frmArtikal.ModalResult = mrOK) then
              begin
                dm.tblIzborStavkiGodPlanSektori.FullRefresh;
                dm.tblIzborStavkiGodPlanSektori.Locate('ARTVID; ID',  VarArrayOf([StrToInt(frmArtikal.GetSifra(0)), StrToInt(frmArtikal.GetSifra(1))]) , []);
              end;
          frmArtikal.Free;
        end
     else
       ShowMessage('������ ����� �� �������� �������� !!!');
end;

procedure TfrmIzborStavkiGodisenPlan.aSnimiIzgledExecute(Sender: TObject);
begin
     zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

procedure TfrmIzborStavkiGodisenPlan.aVnesNaArtikliVoPlanExecute(
  Sender: TObject);
var i, nula_cd,nula_k :Integer;
begin
      nula_cd:=0;
      nula_k:=0;
      with cxGrid1DBTableView1.DataController do
          for I := 0 to RecordCount - 1 do  //izvrti gi site stiklirani - ne samo filtered
            begin
              if (GetValue(i,cxGrid1DBTableView1KOLICINA.Index) <> 0) and (GetValue(i,cxGrid1DBTableView1TARIFA.Index) <> Null)then
                begin
                  if (GetValue(i,cxGrid1DBTableView1CENA.Index) = 0) or (GetValue(i,cxGrid1DBTableView1TARIFA.Index) = 0)then
                     nula_cd:=1;
                  dmPQ.insert12(dmPQ.pInsStavkiVoPlan, 'BROJ', 'GODINA', 'VID_ARTIKAL', 'ARTIKAL', 'RE', 'KOLICINA', 'CENABEZDDV', 'DANOK',Null, Null, Null, Null,
                                dm.tblGodPlanOdlukaBROJ.Value, dm.tblGodPlanOdlukaGODINA.Value,
                                GetValue(i,cxGrid1DBTableView1ARTVID.Index), GetValue(i,cxGrid1DBTableView1ID.Index),
                                tag, GetValue(i,cxGrid1DBTableView1KOLICINA.Index),GetValue(i,cxGrid1DBTableView1CENA.Index),
                                GetValue(i,cxGrid1DBTableView1TARIFA.Index), Null,Null,Null,Null);
                  nula_k:=1;
                end;
            end;
      if nula_k = 1 then
         begin
            if nula_cd = 1 then
               ShowMessage('����� �� ��������� ������ ����� ���� ���� ��� ��� ���� !!!');
            close;
         end
      else ShowMessage('���������� �� ���� ������ � 0 !!!');
end;

procedure TfrmIzborStavkiGodisenPlan.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
     if (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1KOLICINA)
         and (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1TARIFA)
         and (cxGrid1DBTableView1.Controller.FocusedColumn <> cxGrid1DBTableView1CENA)then
     begin
       if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
     end;

end;

procedure TfrmIzborStavkiGodisenPlan.FormShow(Sender: TObject);
begin
     SpremiForma(self);

     procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);

     dm.tblIzborStavkiGodPlanSektori.close;
     dm.tblIzborStavkiGodPlanSektori.ParamByName('broj').Value:=dm.tblGodPlanOdlukaBROJ.Value;
     dm.tblIzborStavkiGodPlanSektori.ParamByName('godina').Value:=dm.tblGodPlanOdlukaGODINA.Value;
     dm.tblIzborStavkiGodPlanSektori.ParamByName('re').Value:=tag;
     dm.tblIzborStavkiGodPlanSektori.Open;

     labelGodisenPlanBroj.Caption:=dm.tblGodPlanOdlukaBROJ.Value;
     labelGodisenPlanGodina.Caption:=intTostr(dm.tblGodPlanOdlukaGODINA.Value);
     labelRabotnaEdinica.Caption:=frmGodisenPlan.cbPodsektoriTree.Text;
end;

end.
