unit DokumentiZaJavnaNabavka;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit,
  cxCalendar, frxDesgn, frxClass, frxDBSet, frxRich,
  frxCross, frxEditDataBand, FIBQuery, pFIBQuery, FIBDataSet, pFIBDataSet,
  dxSkinOffice2013White, cxNavigator, System.Actions, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm;

type
  TfrmDokumentiZaJavnaNabavka = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1VID_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1VIDDOKUMENTNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TEMPLATE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    DATUM: TcxDBDateEdit;
    Label2: TLabel;
    VID_DOKUMENT: TcxDBTextEdit;
    VID_DOKUMENT_NAZIV: TcxDBLookupComboBox;
    Label3: TLabel;
    aKreirajDokumentTemplate: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aAzurirajDokument: TAction;
    aBrisiDokument: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    aPregledaj: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarManager1Bar6: TdxBar;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    aPIzvestajSprovedPostPonudiPredmetEvaluacija: TAction;
    aDIzvestajSprovedPostPonudiPredmetEvaluacija: TAction;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    PopupMenu2: TPopupMenu;
    aDizajnPopupMenu: TAction;
    aPIzvestajSprovedPostPonudiPredlogNajpovolnaPonuda: TAction;
    aDIzvestajSprovedPostPonudiPredlogNajpovolnaPonuda: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    aIzvestajSprovedenaPostapka: TAction;
    aDIzvestajSprovedenaPostapka: TAction;
    N4: TMenuItem;
    dxBarButton6: TdxBarButton;
    aTendDokObrazecListaCeni: TAction;
    aDTendDokObrazecListaCeni: TAction;
    N2: TMenuItem;
    Label4: TLabel;
    aKreirajDokumentFRF: TAction;
    cxGrid1DBTableView1KONTROLIRAL: TcxGridDBColumn;
    cxGrid1DBTableView1RED_BR: TcxGridDBColumn;
    Label5: TLabel;
    RED_BR: TcxDBTextEdit;
    qRed_br: TpFIBQuery;
    tblKontroliral: TpFIBDataSet;
    dsKontroliral: TDataSource;
    tblKontroliralKONTROLIRAL: TFIBStringField;
    KONTROLIRAL: TcxDBLookupComboBox;
    aKreirajDokument: TAction;
    dxBarButton7: TdxBarButton;
    dxBarLargeButton18: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aKreirajDokumentTemplateExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure aBrisiDokumentExecute(Sender: TObject);
    procedure aAzurirajDokumentExecute(Sender: TObject);
    procedure aPregledajExecute(Sender: TObject);
    procedure aDizajnPopupMenuExecute(Sender: TObject);
    procedure aIzvestajSprovedenaPostapkaExecute(Sender: TObject);
    procedure aDIzvestajSprovedenaPostapkaExecute(Sender: TObject);
    procedure aTendDokObrazecListaCeniExecute(Sender: TObject);
    procedure aDTendDokObrazecListaCeniExecute(Sender: TObject);
    procedure aKreirajDokumentFRFExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aKreirajDokumentExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDokumentiZaJavnaNabavka: TfrmDokumentiZaJavnaNabavka;
  pateka_terk:String;
  pom_state_inser :Integer;
implementation

{$R *.dfm}

uses dmUnit, dmReportUnit, DaNe, dmProcedureQuey, dmResources, dmKonekcija,
  Utils;

procedure TfrmDokumentiZaJavnaNabavka.aAzurirajDokumentExecute(Sender: TObject);
  var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
     if not dm.tblTenderDokumentiTEMPLATE.IsNull then
          begin
             RptStream := dm.tblTenderDokumenti.CreateBlobStream(dm.tblTenderDokumentiTEMPLATE, bmRead);
             dmReport.frxReport1.LoadFromStream(RptStream);

             dmReport.frxReport1.DesignReport();
             frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
             if (frmDaNe.ShowModal = mrYes) then
                 dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 1);
           end
       else ShowMessage('������ �������� ��������');

end;

procedure TfrmDokumentiZaJavnaNabavka.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  tblKontroliral.Close;
  tblKontroliral.Open;
end;

procedure TfrmDokumentiZaJavnaNabavka.aBrisiDokumentExecute(Sender: TObject);
begin
  inherited;
  frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
     if (frmDaNe.ShowModal <> mrYes) then
        Abort
     else
        begin
            dmPQ.qDeleteDokumetTender.Close;
            dmPQ.qDeleteDokumetTender.ParamByName('id').Value:=dm.tblTenderDokumentiID.Value;
            dmPQ.qDeleteDokumetTender.ExecQuery;
            dm.tblTenderDokumenti.Refresh;
        end;
end;

procedure TfrmDokumentiZaJavnaNabavka.aDTendDokObrazecListaCeniExecute(Sender: TObject);
begin
   dmRes.Spremi('JN',40017);
   dmRes.frxReport1.DesignReport();
end;

procedure TfrmDokumentiZaJavnaNabavka.aDizajnPopupMenuExecute(Sender: TObject);
begin
  inherited;
   PopupMenu2.Popup(500,500 );
end;

procedure TfrmDokumentiZaJavnaNabavka.aDIzvestajSprovedenaPostapkaExecute(
  Sender: TObject);
begin
  inherited;
  dmRes.Spremi('JN',40019);
  dmRes.frxReport1.DesignReport();
end;

procedure TfrmDokumentiZaJavnaNabavka.aIzvestajSprovedenaPostapkaExecute(
  Sender: TObject);
begin
  inherited;
   try
   // dmRes.Spremi('JN',40019);
    if dm.tblTenderTIP_OCENA.Value = 1 then
       dmRes.Spremi('JN',40019)
    else if dm.tblTenderTIP_OCENA.Value = 0 then
       dmRes.Spremi('JN',40041);

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.ShowReport();

    dmReport.PonudiPredmetNaEvaluacija.Close;
    dmReport.PonudiPredmetNaEvaluacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.PonudiPredmetNaEvaluacija.open;

    dmReport.IzborNajpovolnaPonudaPoGrupi.Close;
    dmReport.IzborNajpovolnaPonudaPoGrupi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.IzborNajpovolnaPonudaPoGrupi.Open;

    dmReport.KomisijaZaJavnaNabavka.Close;
    dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.KomisijaZaJavnaNabavka.open;

    dmReport.TenderskaDokumentacija.Close;
    dmReport.TenderskaDokumentacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.TenderskaDokumentacija.open;

    dmReport.ObrazlozenieNeprifateniPonudi.Close;
    dmReport.ObrazlozenieNeprifateniPonudi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.ObrazlozenieNeprifateniPonudi.Open;

    dmReport.FinansiskaPonuda_1.Close;
    dmReport.FinansiskaPonuda_1.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.FinansiskaPonuda_1.Open;

    dmReport.PokanetiPonuduvaci.Close;
    dmReport.PokanetiPonuduvaci.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.PokanetiPonuduvaci.Open;

    dmReport.tblDobitnici.Close;
    dmReport.tblDobitnici.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.tblDobitnici.Open;

    dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:='%';
    dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:='%';
    dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

    dmReport.KriteriumiUtvrduvanjeSposobnost.Close;
    dmReport.KriteriumiUtvrduvanjeSposobnost.Open;

    dmReport.OdlukaJavnaNabavka.Close;
    dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
    dmReport.OdlukaJavnaNabavka.Open;

    dmReport.PocetnaRangListaNedeliviGrupi.Close;
    dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.PocetnaRangListaNedeliviGrupi.Open;

    dmReport.tblPocetnaRangLista.Close;
    dmReport.tblPocetnaRangLista.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmReport.tblPocetnaRangLista.Open;
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmDokumentiZaJavnaNabavka.aKreirajDokumentTemplateExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.PokanaZaPodnesuvanjePonuda.Close;
  dmReport.PokanaZaPodnesuvanjePonuda.Open;

  dmReport.Template.Close;
  dmReport.Template.ParamByName('broj').Value:=1;
  dmReport.Template.Open;

  if not dm.tblTenderDokumentiTEMPLATE.IsNull then
     begin
       RptStream := dm.tblTenderDokumenti.CreateBlobStream(dm.tblTenderDokumentiTEMPLATE, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       dmReport.frxReport1.ShowReport;
     end
  else
     begin
       if pateka_terk = '' then
          begin
            dmReport.OpenDialog1.FileName:='';
            dmReport.OpenDialog1.InitialDir:=pat_dokumenti;
            dmReport.OpenDialog1.Execute();
            pateka_terk:=dmReport.OpenDialog1.FileName;
          end;
       if pateka_terk <> '' then
       begin
       RptStream := dmReport.Template.CreateBlobStream(dmReport.TemplateREPORT, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       RichView := TfrxRichView(dmReport.frxReport1.FindObject( 'richFile' ) );
       If RichView <> Nil Then
          begin
           RichView.RichEdit.Lines.LoadFromFile( pateka_terk );
           RichView.DataSet:=dmReport.frxPokanaZaPodnesuvanjePonuda;
         end;
       MasterData:=TfrxMasterData(dmReport.frxReport1.FindObject( 'MasterData1' ));
       if MasterData <> Nil then
          MasterData.DataSet:=dmReport.frxPokanaZaPodnesuvanjePonuda;

       dmReport.frxReport1.ShowReport();

       frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
           dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 1);
       end;
     end;
end;

procedure TfrmDokumentiZaJavnaNabavka.aKreirajDokumentExecute(Sender: TObject);
begin
  inherited;
  if dm.tblVidDokumentTEMPLATE_FRF.Value = 0 then
     aKreirajDokumentTemplate.Execute()
  else
     aKreirajDokumentFRF.Execute();
end;

procedure TfrmDokumentiZaJavnaNabavka.aKreirajDokumentFRFExecute(
  Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin
  dmReport.frxReport1.Clear;
  dmReport.frxReport1.Script.Clear;

  dmReport.TemplateFRF.close;
  dmReport.TemplateFRF.ParamByName('br').value:=dm.tblVidDokumentBROJ_FRF.Value;
  dmReport.TemplateFRF.ParamByName('app').Value:='JN';
  dmReport.TemplateFRF.open;

  dmRes.Spremi('JN',+ dm.tblVidDokumentBROJ_FRF.Value);
  dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
  dmKon.tblSqlReport.Open;

  if dm.tblVidDokumentBROJ_FRF.Value = 40020 then
     begin
        dmReport.OdlukaJavnaNabavka.Close;
        dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.OdlukaJavnaNabavka.Open;

        dmReport.KriteriumSposobnostiZaJN.Close;
        dmReport.KriteriumSposobnostiZaJN.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.KriteriumSposobnostiZaJN.Open;
     end;
  if dm.tblVidDokumentBROJ_FRF.Value = 40022 then
     begin
        dmReport.ZapisnikOtvoranjePonudi.Close;
        dmReport.ZapisnikOtvoranjePonudi.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.ZapisnikOtvoranjePonudi.Open;
     end;
  if ((dm.tblVidDokumentBROJ_FRF.Value = 40023) or (dm.tblVidDokumentBROJ_FRF.Value = 40036)or (dm.tblVidDokumentBROJ_FRF.Value = 40054))  then
     begin
        dmReport.OdlukaJavnaNabavka.Close;
        dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.OdlukaJavnaNabavka.Open;
     end;
  if ((dm.tblVidDokumentBROJ_FRF.Value = 40032) or (dm.tblVidDokumentBROJ_FRF.Value = 40033)) then
     begin
        dmReport.PocetnaRangListaNedeliviGrupi.Close;
        dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.PocetnaRangListaNedeliviGrupi.Open;

        dmReport.OdlukaJavnaNabavka.Close;
        dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.OdlukaJavnaNabavka.Open;
     end;

  if ((dm.tblVidDokumentBROJ_FRF.Value = 40019) or (dm.tblVidDokumentBROJ_FRF.Value = 40041)) then
     begin

        dmReport.PonudiPredmetNaEvaluacija.Close;
        dmReport.PonudiPredmetNaEvaluacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.PonudiPredmetNaEvaluacija.open;

        dmReport.IzborNajpovolnaPonudaPoGrupi.Close;
        dmReport.IzborNajpovolnaPonudaPoGrupi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.IzborNajpovolnaPonudaPoGrupi.Open;

        dmReport.KomisijaZaJavnaNabavka.Close;
        dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.KomisijaZaJavnaNabavka.open;

        dmReport.TenderskaDokumentacija.Close;
        dmReport.TenderskaDokumentacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.TenderskaDokumentacija.open;

        dmReport.ObrazlozenieNeprifateniPonudi.Close;
        dmReport.ObrazlozenieNeprifateniPonudi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.ObrazlozenieNeprifateniPonudi.Open;

        dmReport.FinansiskaPonuda_1.Close;
        dmReport.FinansiskaPonuda_1.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.FinansiskaPonuda_1.Open;

        dmReport.PokanetiPonuduvaci.Close;
        dmReport.PokanetiPonuduvaci.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.PokanetiPonuduvaci.Open;

        dmReport.tblDobitnici.Close;
        dmReport.tblDobitnici.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.tblDobitnici.Open;

        dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
        dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:='%';
        dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:='%';
        dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

        dmReport.KriteriumiUtvrduvanjeSposobnost.Close;
        dmReport.KriteriumiUtvrduvanjeSposobnost.Open;

        dmReport.OdlukaJavnaNabavka.Close;
        dmReport.OdlukaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
        dmReport.OdlukaJavnaNabavka.Open;

        dmReport.PocetnaRangListaNedeliviGrupi.Close;
        dmReport.PocetnaRangListaNedeliviGrupi.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.PocetnaRangListaNedeliviGrupi.Open;

        dmReport.tblPocetnaRangLista.Close;
        dmReport.tblPocetnaRangLista.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
        dmReport.tblPocetnaRangLista.Open;
     end;

  if not dm.tblTenderDokumentiTEMPLATE.IsNull then
     begin
       RptStream := dm.tblTenderDokumenti.CreateBlobStream(dm.tblTenderDokumentiTEMPLATE, bmRead);
       dmReport.frxReport1.LoadFromStream(RptStream);

       dmReport.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'Kontroliral', QuotedStr(dm.tblTenderDokumentiKONTROLIRAL.Value));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
       dmReport.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
       if not dm.tblTenderDokumentiUSR_UPD.IsNull then
          dmReport.frxReport1.Variables.AddVariable('VAR', 'datum_vreme', QuotedStr(DateTimeToStr(dm.tblTenderDokumentiTS_UPD.Value)))
       else
          dmReport.frxReport1.Variables.AddVariable('VAR', 'datum_vreme', QuotedStr(DateTimeToStr(dm.tblTenderDokumentiTS_INS.Value)));
       if not dm.tblTenderDokumentiUSR_UPD.IsNull then
          begin
            // dmReport.qUserImePrezime.Close;
           //  dmReport.qUserImePrezime.ParamByName('username').Value:=dm.tblTenderDokumentiUSR_UPD.Value;
           //  dmReport.qUserImePrezime.ExecQuery;
          //   dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          end
       else
          begin
            // dmReport.qUserImePrezime.Close;
           //  dmReport.qUserImePrezime.ParamByName('username').Value:=dm.tblTenderDokumentiUSR_INS.Value;
           //  dmReport.qUserImePrezime.ExecQuery;
           //  dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          end;
        dmReport.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
        dmReport.frxReport1.ShowReport;
     end
  else
     begin
          RptStream := dmReport.TemplateFRF.CreateBlobStream(dmReport.TemplateFRFDATA, bmRead);
          dmReport.frxReport1.LoadFromStream(RptStream);

          dmReport.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'Kontroliral', QuotedStr(dm.tblTenderDokumentiKONTROLIRAL.Value));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'postapka', QuotedStr(dm.tblTenderTIPTENDERNAZIV.Value));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'upravitelZvanje', QuotedStr(upravitelZvanje));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'upravitelNaziv', QuotedStr(upravitelNaziv));
          dmReport.frxReport1.Variables.AddVariable('VAR', 'Godina', QuotedStr(intToStr(dm.tblTenderGODINA.Value)));
          if not dm.tblTenderDokumentiUSR_UPD.IsNull then
             dmReport.frxReport1.Variables.AddVariable('VAR', 'datum_vreme', QuotedStr(DateTimeToStr(dm.tblTenderDokumentiTS_UPD.Value)))
          else
             dmReport.frxReport1.Variables.AddVariable('VAR', 'datum_vreme', QuotedStr(DateTimeToStr(dm.tblTenderDokumentiTS_INS.Value)));
          if not dm.tblTenderDokumentiUSR_UPD.IsNull then
             begin
               // dmReport.qUserImePrezime.Close;
                //dmReport.qUserImePrezime.ParamByName('username').Value:=dm.tblTenderDokumentiUSR_UPD.Value;
               // dmReport.qUserImePrezime.ExecQuery;
              //  dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
             end
          else
             begin
               // dmReport.qUserImePrezime.Close;
               // dmReport.qUserImePrezime.ParamByName('username').Value:=dm.tblTenderDokumentiUSR_INS.Value;
               // dmReport.qUserImePrezime.ExecQuery;
              //  dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
             end;

          dmReport.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
          dmReport.frxReport1.ShowReport();

          frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� � ������� �� ������. ���� ������ �� �� ������?', 1);
          if (frmDaNe.ShowModal = mrYes) then
             dmReport.frxDesignSaveReport(dmReport.frxReport1, true, 1);
     end;
end;

procedure TfrmDokumentiZaJavnaNabavka.aNovExecute(Sender: TObject);
begin
  inherited;
  dm.tblTenderDokumentiBROJ_TENDER.Value:=dm.tblTenderBROJ.Value;
  dm.tblTenderDokumentiDATUM.Value:=Now;
  tblKontroliral.Close;
  tblKontroliral.Open;
  qRed_br.close;
  qRed_br.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  qRed_br.ExecQuery;
  dm.tblTenderDokumentiRED_BR.Value:=qRed_br.FldByName['red_br'].Value + 1;
  VID_DOKUMENT.SetFocus;
  pom_state_inser:=1;
end;

procedure TfrmDokumentiZaJavnaNabavka.aPregledajExecute(Sender: TObject);
var
  RptStream: TStream;
  SqlStream: TStream;
  sl: TStringList;
  value:Variant;
  RichView: TfrxRichView;
  MasterData: TfrxMasterData;
begin

   if dm.tblVidDokumentTEMPLATE_FRF.Value = 0 then
     aKreirajDokumentTemplate.Execute()
   else
     aKreirajDokumentFRF.Execute();
end;

procedure TfrmDokumentiZaJavnaNabavka.aTendDokObrazecListaCeniExecute(
  Sender: TObject);
begin
  try
    dmRes.Spremi('JN',40017);
    dmKon.tblSqlReport.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
    dmKon.tblSqlReport.Open;

    dmRes.frxReport1.Variables.AddVariable('VAR', 'broj_sluzben_vesnik', QuotedStr(broj_sluzben_vesnik));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Korisnik', QuotedStr(dmKon.imeprezime));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Broj', QuotedStr(dm.tblTenderBROJ.Value));
    dmRes.frxReport1.Variables.AddVariable('VAR', 'Predmet', QuotedStr(dm.tblTenderPREDMET_NABAVKA.Value));
    dmRes.frxReport1.ShowReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmDokumentiZaJavnaNabavka.aZapisiExecute(Sender: TObject);
begin
  inherited;
  if pom_state_inser = 1 then
     begin
        if dm.tblVidDokumentTEMPLATE_FRF.Value = 0 then
          aKreirajDokumentTemplate.Execute()
        else
          aKreirajDokumentFRF.Execute();
        pom_state_inser:=0;
     end;
end;

procedure TfrmDokumentiZaJavnaNabavka.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
  if (Sender = VID_DOKUMENT)or (Sender = VID_DOKUMENT_NAZIV) then
     begin
        if not dm.tblVidDokumentPATEKA.IsNull then
           pateka_terk:=dm.tblVidDokumentPATEKA.Value
        else pateka_terk:='';
     end;
end;

procedure TfrmDokumentiZaJavnaNabavka.FormCreate(Sender: TObject);
var
  status: TStatusWindowHandle;
begin
  inherited;
  status := cxCreateStatusWindow();
  try
     dm.tblTenderDokumenti.Close;
     dm.tblTenderDokumenti.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     dm.tblTenderDokumenti.Open;
  finally
     cxRemoveStatusWindow(status);
  end;
  {dmReport.PonudiPredmetNaEvaluacija.Close;
  dmReport.PonudiPredmetNaEvaluacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
  dmReport.PonudiPredmetNaEvaluacija.open;

  dmReport.IzborNajpovolnaPonudaPoGrupi.Close;
  dmReport.IzborNajpovolnaPonudaPoGrupi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  dmReport.IzborNajpovolnaPonudaPoGrupi.Open;

  dmReport.KomisijaZaJavnaNabavka.Close;
  dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
  dmReport.KomisijaZaJavnaNabavka.open;

  dmReport.TenderskaDokumentacija.Close;
  dmReport.TenderskaDokumentacija.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
  dmReport.TenderskaDokumentacija.open;

  dmReport.ObrazlozenieNeprifateniPonudi.Close;
  dmReport.ObrazlozenieNeprifateniPonudi.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  dmReport.ObrazlozenieNeprifateniPonudi.Open;

  dmReport.FinansiskaPonuda_1.Close;
  dmReport.FinansiskaPonuda_1.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  dmReport.FinansiskaPonuda_1.Open;

  dmReport.PokanetiPonuduvaci.Close;
  dmReport.PokanetiPonuduvaci.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  dmReport.PokanetiPonuduvaci.Open;

  dmReport.ObrazlozenieNeprifateniPonudiStavki.Close;
  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('tip_partner').Value:='%';
  dmReport.ObrazlozenieNeprifateniPonudiStavki.ParamByName('partner').Value:='%';
  dmReport.ObrazlozenieNeprifateniPonudiStavki.Open;

  dmReport.KriteriumiUtvrduvanjeSposobnost.Close;
  dmReport.KriteriumiUtvrduvanjeSposobnost.Open;}


  dmReport.KomisijaZaJavnaNabavka.Close;
  dmReport.KomisijaZaJavnaNabavka.ParamByName('mas_broj').Value:=dm.tblTenderBROJ.Value;
  dmReport.KomisijaZaJavnaNabavka.open;

  dmReport.DogovorZaJN.Close;
  dmReport.DogovorZaJN.ParamByName('broj_dogovor').Value:=dm.tblDogovoriBROJ_DOGOVOR.Value;
  dmReport.DogovorZaJN.Open;

  Caption:='��������� �� ����� ������� ��� '+dm.tblTenderBROJ.Value;
end;

end.
