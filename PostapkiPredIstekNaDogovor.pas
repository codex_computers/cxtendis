unit PostapkiPredIstekNaDogovor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.ExtCtrls, Vcl.Menus, Vcl.StdCtrls, cxButtons, Vcl.ActnList,
  Vcl.ComCtrls, cxGridCustomPopupMenu, cxGridPopupMenu, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, cxNavigator, System.Actions;

type
  TfrmPostapkiPredIstekNaDogovor = class(TForm)
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Panel2: TPanel;
    cxGrid1DBTableView1BROJ_TENDER_O: TcxGridDBColumn;
    cxButton1: TcxButton;
    Label1: TLabel;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGridPopupMenu2: TcxGridPopupMenu;
    StatusBar1: TStatusBar;
    ActionList1: TActionList;
    ZacuvajExcel: TAction;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ZacuvajExcelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPostapkiPredIstekNaDogovor: TfrmPostapkiPredIstekNaDogovor;

implementation

{$R *.dfm}

uses dmUnit, dmResources, dmProcedureQuey, Utils;

procedure TfrmPostapkiPredIstekNaDogovor.cxButton1Click(Sender: TObject);
begin
     dmPQ.pZatvoriTenderi.ExecProc;
     Close;
end;

procedure TfrmPostapkiPredIstekNaDogovor.cxButton2Click(Sender: TObject);
begin
     Close;
end;

procedure TfrmPostapkiPredIstekNaDogovor.ZacuvajExcelExecute(Sender: TObject);
begin
     zacuvajVoExcel(cxGrid1, '����� ������� ��� ����� �� �� ��������');
end;

end.
