object frmPonudi: TfrmPonudi
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 693
  ClientWidth = 1276
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    1276
    693)
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1276
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar10'
        end
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar11'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1072
          ToolbarName = 'dxBarManager1Bar7'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 670
    Width = 1276
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080
        Width = 320
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080', Ctrl+F8 - '#1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F7 - '#1054#1089#1074#1077#1078#1080
        Width = 340
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControlPonudi: TcxPageControl
    Left = 0
    Top = 126
    Width = 1276
    Height = 277
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = cxTabSheetPonudiTabelaren
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 277
    ClientRectRight = 1276
    ClientRectTop = 24
    object cxTabSheetPonudiTabelaren: TcxTabSheet
      Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1276
        Height = 253
        Align = alClient
        Caption = 'Panel3'
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 1274
          Height = 251
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPonudi
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1085#1091#1076#1072
            Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
            object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_TENDER'
              Visible = False
              Options.Editing = False
              Width = 119
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Options.Editing = False
              Width = 52
            end
            object cxGrid1DBTableView1ODBIENA: TcxGridDBColumn
              DataBinding.FieldName = 'ODBIENA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.ImmediatePost = True
              Properties.Items = <
                item
                  Description = #1042#1072#1083#1080#1076#1085#1072
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = #1054#1076#1073#1080#1077#1085#1072
                  Value = 1
                end>
              Properties.OnEditValueChanged = cxGrid1DBTableView1ODBIENAPropertiesEditValueChanged
              Width = 71
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Options.Editing = False
              Width = 87
            end
            object cxGrid1DBTableView1DATUM_PONUDA: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_PONUDA'
              Options.Editing = False
              Width = 82
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Options.Editing = False
              Width = 89
            end
            object cxGrid1DBTableView1TIPPARTENRNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIPPARTENRNAZIV'
              Visible = False
              Options.Editing = False
              Width = 104
            end
            object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER'
              Options.Editing = False
              Width = 110
            end
            object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNERNAZIV'
              Visible = False
              Options.Editing = False
              Width = 295
            end
            object cxGrid1DBTableView1PARTNERSKRATENNAZIV: TcxGridDBColumn
              Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095
              DataBinding.FieldName = 'PARTNERSKRATENNAZIV'
              Options.Editing = False
              Width = 295
            end
            object cxGrid1DBTableView1BROJ_PONUDUVAC: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_PONUDUVAC'
              Options.Editing = False
              Width = 216
            end
            object cxGrid1DBTableView1PRETSTAVNIK: TcxGridDBColumn
              DataBinding.FieldName = 'PRETSTAVNIK'
              Options.Editing = False
              Width = 163
            end
            object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
              DataBinding.FieldName = 'ZABELESKA'
              PropertiesClassName = 'TcxRichEditProperties'
              Properties.MemoMode = True
              Properties.ReadOnly = True
              Properties.VisibleLineCount = 2
              Visible = False
              Options.Editing = False
              Width = 134
            end
            object cxGrid1DBTableView1PRIMENA_VO_ROK: TcxGridDBColumn
              DataBinding.FieldName = 'PRIMENA_VO_ROK'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Options.Editing = False
              Width = 148
            end
            object cxGrid1DBTableView1ZATVOREN_KOVERT: TcxGridDBColumn
              DataBinding.FieldName = 'ZATVOREN_KOVERT'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Options.Editing = False
              Width = 180
            end
            object cxGrid1DBTableView1OZNAKA_NE_OTVORAJ: TcxGridDBColumn
              DataBinding.FieldName = 'OZNAKA_NE_OTVORAJ'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Options.Editing = False
              Width = 249
            end
            object cxGrid1DBTableView1POVLEKUVANJE_PONUDA: TcxGridDBColumn
              DataBinding.FieldName = 'POVLEKUVANJE_PONUDA'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Options.Editing = False
              Width = 134
            end
            object cxGrid1DBTableView1IZMENA_PONUDA: TcxGridDBColumn
              DataBinding.FieldName = 'IZMENA_PONUDA'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Options.Editing = False
              Width = 159
            end
            object cxGrid1DBTableView1KOMPLETNOST: TcxGridDBColumn
              DataBinding.FieldName = 'KOMPLETNOST'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = 1
              Properties.ValueUnchecked = 0
              Options.Editing = False
              Width = 142
            end
            object cxGrid1DBTableView1DOKUMENTACIJA: TcxGridDBColumn
              DataBinding.FieldName = 'DOKUMENTACIJA'
              PropertiesClassName = 'TcxRichEditProperties'
              Properties.MemoMode = True
              Properties.ReadOnly = True
              Properties.VisibleLineCount = 2
              Visible = False
              Options.Editing = False
              Width = 153
            end
            object cxGrid1DBTableView1VALUTA: TcxGridDBColumn
              DataBinding.FieldName = 'VALUTA'
              Options.Editing = False
              Width = 105
            end
            object cxGrid1DBTableView1ROK_VAZNOST: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_VAZNOST'
              Options.Editing = False
              Width = 157
            end
            object cxGrid1DBTableView1GARANCII_POPUST: TcxGridDBColumn
              DataBinding.FieldName = 'GARANCII_POPUST'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Options.Editing = False
              Width = 182
            end
            object cxGrid1DBTableView1GARANCIJA_IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'GARANCIJA_IZNOS'
              Options.Editing = False
              Width = 153
            end
            object cxGrid1DBTableView1GARANCIJA_DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'GARANCIJA_DATUM'
              Width = 153
            end
            object cxGrid1DBTableView1ROK_FAKTURA: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_FAKTURA'
              Width = 112
            end
            object cxGrid1DBTableView1ROK_ISPORAKA: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_ISPORAKA'
              Width = 250
            end
            object cxGrid1DBTableView1GARANTEN_ROK: TcxGridDBColumn
              DataBinding.FieldName = 'GARANTEN_ROK'
              Width = 200
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Options.Editing = False
              Width = 160
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Options.Editing = False
              Width = 214
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Options.Editing = False
              Width = 199
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Options.Editing = False
              Width = 237
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
    object cxTabSheetPonudiDetalen: TcxTabSheet
      Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079' '#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 1
      object PanelPonudiDetalen: TPanel
        Left = 0
        Top = 0
        Width = 1276
        Height = 253
        Align = alClient
        Enabled = False
        TabOrder = 0
        DesignSize = (
          1276
          253)
        object lbl6: TLabel
          Left = 1303
          Top = 29
          Width = 215
          Height = 18
          AutoSize = False
          Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1077#1085' '#1090#1077#1082#1089#1090' '#1087#1086#1076' '#1090#1072#1073#1077#1083#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object cxGroupBoxPonuduvac: TcxGroupBox
          Left = 17
          Top = 122
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
          TabOrder = 1
          Height = 114
          Width = 488
          object Label69: TLabel
            Left = 32
            Top = 54
            Width = 78
            Height = 18
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label72: TLabel
            Left = 5
            Top = 27
            Width = 105
            Height = 18
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label74: TLabel
            Left = 23
            Top = 81
            Width = 87
            Height = 15
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1088#1077#1090#1089#1090#1072#1074#1085#1080#1082' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object TIP_PARTNER: TcxDBTextEdit
            Tag = 1
            Left = 116
            Top = 51
            Hint = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
            BeepOnEnter = False
            DataBinding.DataField = 'TIP_PARTNER'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 40
          end
          object PARTNER: TcxDBTextEdit
            Tag = 1
            Left = 155
            Top = 51
            Hint = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
            BeepOnEnter = False
            DataBinding.DataField = 'PARTNER'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 40
          end
          object BROJ_PONUDUVAC: TcxDBTextEdit
            Left = 116
            Top = 24
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072' '#1085#1072#1079#1085#1072#1095#1077#1085' '#1086#1076' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ_PONUDUVAC'
            DataBinding.DataSource = dm.dsPonudi
            Style.TextStyle = []
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 121
          end
          object PRETSTAVNIK: TcxDBTextEdit
            Left = 116
            Top = 78
            Hint = #1055#1088#1077#1090#1089#1090#1072#1074#1085#1080#1082' - '#1085#1072#1079#1085#1072#1095#1077#1085#1086' '#1083#1080#1094#1077' '#1086#1076' '#1082#1086#1084#1087#1072#1085#1080#1112#1072#1090#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
            BeepOnEnter = False
            DataBinding.DataField = 'PRETSTAVNIK'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Shadow = False
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 357
          end
          object PARTNER_NAZIV: TcxLookupComboBox
            Left = 194
            Top = 51
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
            BeepOnEnter = False
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'TIP_PARTNER;ID'
            Properties.ListColumns = <
              item
                Width = 100
                FieldName = 'TIP_PARTNER'
              end
              item
                Width = 100
                FieldName = 'ID'
              end
              item
                Width = 600
                FieldName = 'NAZIV_SKRATEN'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dsPartner
            Properties.OnEditValueChanged = PARTNER_NAZIVPropertiesEditValueChanged
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 279
          end
        end
        object ZapisiButton: TcxButton
          Left = 1103
          Top = 208
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akTop, akRight]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 4
        end
        object OtkaziButton: TcxButton
          Left = 1184
          Top = 207
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akTop, akRight]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 5
        end
        object cxGroupBoxPonuda: TcxGroupBox
          Left = 17
          Top = 7
          Caption = #1054#1087#1096#1090#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1085#1091#1076#1072
          TabOrder = 0
          Height = 116
          Width = 488
          object Label5: TLabel
            Left = 60
            Top = 27
            Width = 50
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1088#1086#1112' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 277
            Top = 27
            Width = 67
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label2: TLabel
            Left = 38
            Top = 75
            Width = 72
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object BROJ: TcxDBTextEdit
            Tag = 1
            Left = 116
            Top = 24
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1086#1085#1091#1076#1072
            BeepOnEnter = False
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.BeepOnError = True
            Properties.CharCase = ecUpperCase
            Style.Shadow = False
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 123
          end
          object DATUM_PONUDA: TcxDBDateEdit
            Left = 350
            Top = 24
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1085#1091#1076#1072
            BeepOnEnter = False
            DataBinding.DataField = 'DATUM_PONUDA'
            DataBinding.DataSource = dm.dsPonudi
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 123
          end
          object ZABELESKA: TcxDBRichEdit
            Left = 116
            Top = 72
            Hint = 
              #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1079#1072#1073#1077#1083#1077#1096#1082#1080' '#1079#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072'  '#1076#1072' '#1075#1086' ' +
              #1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            DataBinding.DataField = 'ZABELESKA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.WantReturns = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clBlack
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 3
            OnDblClick = ZABELESKADblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 30
            Width = 357
          end
          object cxDBCheckBoxValidna: TcxDBCheckBox
            Left = 116
            Top = 51
            Hint = #1044#1072#1083#1080' '#1087#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1074#1072#1083#1080#1076#1085#1072' ?'
            TabStop = False
            Caption = #1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1074#1072#1083#1080#1076#1085#1072
            DataBinding.DataField = 'ODBIENA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taLeftJustify
            Properties.ValueChecked = 0
            Properties.ValueUnchecked = 1
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 2
            Transparent = True
            Width = 129
          end
        end
        object cxGroupBoxOtvorenaPonuda: TcxGroupBox
          Left = 511
          Top = 7
          Hint = 
            #1058#1072#1073#1077#1083#1072' '#1089#1086' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1077#1083#1077#1084#1077#1085#1090#1080#1090#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077' '#1082#1086#1080' '#1077' '#1095#1080#1090#1072#1072#1090' '#1085#1072' '#1086#1090#1074 +
            #1086#1088#1072#1114#1077#1090#1086' '#1085#1072' '#1087#1086#1085#1091#1076#1080#1090#1077
          Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1086#1076' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
          TabOrder = 2
          Height = 229
          Width = 786
          object Label6: TLabel
            Left = 15
            Top = 152
            Width = 269
            Height = 13
            AutoSize = False
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1087#1088#1080#1083#1086#1078#1077#1085#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label13: TLabel
            Left = 335
            Top = 23
            Width = 185
            Height = 18
            AutoSize = False
            Caption = #1056#1086#1082' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object Label1: TLabel
            Left = 335
            Top = 64
            Width = 57
            Height = 18
            AutoSize = False
            Caption = #1042#1072#1083#1091#1090#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object lbl3: TLabel
            Left = 527
            Top = 23
            Width = 102
            Height = 15
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object lbl4: TLabel
            Left = 527
            Top = 65
            Width = 99
            Height = 15
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1056#1086#1082' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object lbl5: TLabel
            Left = 335
            Top = 105
            Width = 102
            Height = 15
            AutoSize = False
            Caption = #1043#1072#1088#1072#1085#1090#1077#1085' '#1088#1086#1082' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object PRIMENA_VO_ROK: TcxDBCheckBox
            Left = 15
            Top = 24
            TabStop = False
            Caption = #1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1087#1088#1080#1084#1077#1085#1072' '#1074#1086' '#1088#1086#1082
            DataBinding.DataField = 'PRIMENA_VO_ROK'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 0
            Transparent = True
            Width = 164
          end
          object ZATVOREN_KOVERT: TcxDBCheckBox
            Left = 15
            Top = 44
            TabStop = False
            Caption = #1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1077' '#1079#1072#1090#1074#1086#1088#1077#1085
            DataBinding.DataField = 'ZATVOREN_KOVERT'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 1
            Transparent = True
            Width = 198
          end
          object OZNAKA_NE_OTVORAJ: TcxDBCheckBox
            Left = 15
            Top = 64
            TabStop = False
            Caption = #1053#1072#1076#1074#1086#1088#1077#1096#1085#1080#1086#1090' '#1082#1086#1074#1077#1088#1090' '#1080#1084#1072' '#1086#1079#1085#1072#1082#1072' '#8222#1085#1077' '#1086#1090#1074#1086#1088#1072#1112#8220
            DataBinding.DataField = 'OZNAKA_NE_OTVORAJ'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 2
            Transparent = True
            Width = 263
          end
          object POVLEKUVANJE_PONUDA: TcxDBCheckBox
            Left = 15
            Top = 84
            TabStop = False
            Caption = #1055#1086#1074#1083#1077#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1087#1086#1085#1091#1076#1072
            DataBinding.DataField = 'POVLEKUVANJE_PONUDA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 3
            Transparent = True
            Width = 149
          end
          object IZMENA_PONUDA: TcxDBCheckBox
            Left = 15
            Top = 104
            TabStop = False
            Caption = #1048#1079#1084#1077#1085#1072' '#1080#1083#1080' '#1079#1072#1084#1077#1085#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
            DataBinding.DataField = 'IZMENA_PONUDA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 4
            Transparent = True
            Width = 174
          end
          object KOMPLETNOST: TcxDBCheckBox
            Left = 15
            Top = 124
            Hint = 
              #1055#1086#1085#1091#1076#1072#1090#1072' '#1077' '#1076#1086#1089#1090#1072#1074#1077#1085#1072' '#1074#1086' '#1086#1088#1080#1075#1080#1085#1072#1083' '#1080' '#1086#1085#1086#1083#1082#1091' '#1082#1086#1087#1080#1080' '#1082#1086#1083#1082#1091' '#1096#1090#1086' '#1089#1077' '#1073#1072#1088 +
              #1072#1083#1077' '#1074#1086' '#1090#1077#1085#1076#1077#1088#1089#1082#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
            TabStop = False
            Caption = #1050#1086#1084#1087#1083#1077#1090#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
            DataBinding.DataField = 'KOMPLETNOST'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Properties.Alignment = taRightJustify
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.TextColor = clWindowText
            Style.TextStyle = []
            Style.IsFontAssigned = True
            TabOrder = 5
            Transparent = True
            Width = 158
          end
          object DOKUMENTACIJA: TcxDBRichEdit
            Left = 15
            Top = 167
            Hint = 
              #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1087#1088#1080#1083#1086#1078#1077#1085#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072' : *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076 +
              #1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
            DataBinding.DataField = 'DOKUMENTACIJA'
            DataBinding.DataSource = dm.dsPonudi
            Properties.WantReturns = False
            TabOrder = 6
            OnDblClick = DOKUMENTACIJADblClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 46
            Width = 298
          end
          object ROK_VAZNOST: TcxDBTextEdit
            Left = 335
            Top = 38
            Hint = #1056#1086#1082' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
            BeepOnEnter = False
            DataBinding.DataField = 'ROK_VAZNOST'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Shadow = False
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 186
          end
          object VALUTA: TcxDBLookupComboBox
            Left = 335
            Top = 78
            Hint = #1042#1072#1083#1091#1090#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072#1090#1072
            DataBinding.DataField = 'VALUTA'
            DataBinding.DataSource = dm.dsPonudi
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Width = 100
                FieldName = 'ID'
              end
              item
                Width = 200
                FieldName = 'KURS'
              end
              item
                Width = 400
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListSource = dmMat.dsValuta
            TabOrder = 9
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 186
          end
          object grp1: TGroupBox
            Left = 335
            Top = 148
            Width = 194
            Height = 63
            Caption = #1043#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
            TabOrder = 12
            object lbl1: TLabel
              Left = 10
              Top = 19
              Width = 46
              Height = 14
              AutoSize = False
              Caption = #1048#1079#1085#1086#1089' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object lbl2: TLabel
              Left = 87
              Top = 19
              Width = 46
              Height = 14
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object NAZIVGARANCIJA_IZNOS: TcxDBTextEdit
              Left = 10
              Top = 34
              Hint = #1048#1079#1085#1086#1089' '#1085#1072' '#1075#1072#1088#1072#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1072
              BeepOnEnter = False
              DataBinding.DataField = 'GARANCIJA_IZNOS'
              DataBinding.DataSource = dm.dsPonudi
              ParentFont = False
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 71
            end
            object GARANCIJA_DATUM: TcxDBDateEdit
              Left = 87
              Top = 34
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1086#1085#1091#1076#1072
              BeepOnEnter = False
              DataBinding.DataField = 'GARANCIJA_DATUM'
              DataBinding.DataSource = dm.dsPonudi
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 106
            end
          end
          object ROK_ISPORAKA: TcxDBTextEdit
            Left = 527
            Top = 38
            Hint = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1085#1072' '#1089#1090#1086#1082#1072' '#1074#1086' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080
            BeepOnEnter = False
            DataBinding.DataField = 'ROK_ISPORAKA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Shadow = False
            TabOrder = 8
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 242
          end
          object ROK_FAKTURA: TcxDBTextEdit
            Left = 527
            Top = 78
            Hint = #1056#1086#1082' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1080' '#1074#1086' '#1076#1077#1085#1086#1074#1080
            BeepOnEnter = False
            DataBinding.DataField = 'ROK_FAKTURA'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Shadow = False
            TabOrder = 10
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 242
          end
          object GARANTEN_ROK: TcxDBTextEdit
            Left = 335
            Top = 121
            Hint = #1043#1072#1088#1072#1085#1090#1077#1085' '#1088#1086#1082' ('#1086#1087#1080#1089#1085#1086')'
            BeepOnEnter = False
            DataBinding.DataField = 'GARANTEN_ROK'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Shadow = False
            TabOrder = 11
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 186
          end
        end
        object GARANCII_POPUST: TcxDBMemo
          Left = 1303
          Top = 45
          Hint = 
            #1047#1072#1073#1077#1083#1077#1096#1082#1080' '#1079#1072' '#1075#1072#1088#1072#1085#1094#1080#1080' '#1080' '#1087#1086#1087#1091#1089#1090#1080' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076 +
            #1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
          DataBinding.DataField = 'GARANCII_POPUST'
          DataBinding.DataSource = dm.dsPonudi
          Properties.WantReturns = False
          TabOrder = 3
          OnDblClick = GARANCII_POPUSTDblClick
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 100
          Width = 242
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 403
    Width = 1276
    Height = 5
    AlignSplitter = salTop
    MinSize = 263
    Control = cxPageControlPonudi
  end
  object Panel10: TPanel
    Left = 0
    Top = 408
    Width = 1276
    Height = 262
    Align = alClient
    TabOrder = 4
    object Panel4: TPanel
      Left = 922
      Top = 1
      Width = 353
      Height = 260
      Align = alRight
      TabOrder = 0
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 351
        Height = 52
        Align = alTop
        Color = clCream
        ParentBackground = False
        TabOrder = 0
        object Label9: TLabel
          Left = 6
          Top = 20
          Width = 129
          Height = 14
          Caption = #1041#1086#1076#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1072#1074#1082#1080
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = 10901821
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 207
        Width = 351
        Height = 52
        Align = alBottom
        TabOrder = 1
        DesignSize = (
          351
          52)
        object cxButton1: TcxButton
          Left = 15
          Top = 14
          Width = 321
          Height = 25
          Action = aGrupnoBodirawe
          Anchors = [akLeft, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
        end
      end
      object cxGrid2: TcxGrid
        Left = 1
        Top = 53
        Width = 351
        Height = 154
        Align = alClient
        TabOrder = 2
        object cxGrid2DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsBodiranjeStavkiPonudi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.0000 , .'
              Kind = skSum
              OnGetText = cxGrid2DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
              Column = cxGrid2DBTableView1BODOVI
            end
            item
              Format = '0.0000 , .'
              Kind = skSum
              Column = cxGrid2DBTableView1MAX_BODOVI
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
          OptionsView.Footer = True
          object cxGrid2DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid2DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Options.Editing = False
          end
          object cxGrid2DBTableView1PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Visible = False
            Options.Editing = False
          end
          object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'VID_ARTIKAL'
            Visible = False
            Options.Editing = False
          end
          object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
            DataBinding.FieldName = 'ARTIKAL'
            Visible = False
            Options.Editing = False
          end
          object cxGrid2DBTableView1BODIRANJE_PO: TcxGridDBColumn
            DataBinding.FieldName = 'BODIRANJE_PO'
            Visible = False
            Options.Editing = False
          end
          object cxGrid2DBTableView1KRITERIUMNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'KRITERIUMNAZIV'
            Options.Editing = False
            Width = 157
          end
          object cxGrid2DBTableView1vrednuvanjeNaziv: TcxGridDBColumn
            DataBinding.FieldName = 'vrednuvanjeNaziv'
            Visible = False
            Options.Editing = False
            Width = 121
          end
          object cxGrid2DBTableView1bodiranjeNaziv: TcxGridDBColumn
            DataBinding.FieldName = 'bodiranjeNaziv'
            Visible = False
            Options.Editing = False
            Width = 106
          end
          object cxGrid2DBTableView1MAX_BODOVI: TcxGridDBColumn
            DataBinding.FieldName = 'MAX_BODOVI'
            Options.Editing = False
            Width = 69
          end
          object cxGrid2DBTableView1BODOVI: TcxGridDBColumn
            DataBinding.FieldName = 'BODOVI'
            Styles.Content = dmRes.cxStyle120
            Width = 49
          end
          object cxGrid2DBTableView1VREDNOST: TcxGridDBColumn
            DataBinding.FieldName = 'VREDNOST'
            Styles.Content = dmRes.cxStyle120
            Width = 58
          end
          object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Options.Editing = False
            Width = 64
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
    object cxSplitter3: TcxSplitter
      Left = 917
      Top = 1
      Width = 5
      Height = 260
      AlignSplitter = salRight
      MinSize = 200
      Control = Panel4
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 916
      Height = 260
      Align = alClient
      Caption = 'Panel1'
      TabOrder = 2
      object Panel2: TPanel
        Left = 1
        Top = 1
        Width = 914
        Height = 258
        Align = alClient
        Alignment = taLeftJustify
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = clCream
        ParentBackground = False
        TabOrder = 0
        object cxGrid3: TcxGrid
          Left = 1
          Top = 52
          Width = 912
          Height = 205
          Align = alClient
          TabOrder = 0
          object cxGrid3DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid3DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            OnEditValueChanged = cxGrid3DBTableView1EditValueChanged
            DataController.DataSource = dm.dsPonudiStavki
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00, .'
                Kind = skSum
                Column = cxGrid3DBTableView1IZNOSCENA
              end
              item
                Format = '0.00, .'
                Kind = skSum
                Column = cxGrid3DBTableView1IZNOSCENBEZDDV
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
            OptionsView.Footer = True
            Styles.OnGetContentStyle = cxGrid3DBTableView1StylesGetContentStyle
            object cxGrid3DBTableView1BROJ_TENDER: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_TENDER'
              Visible = False
              Options.Editing = False
              Width = 118
            end
            object cxGrid3DBTableView1PONUDA_ID: TcxGridDBColumn
              DataBinding.FieldName = 'PONUDA_ID'
              Visible = False
              Options.Editing = False
              Width = 95
            end
            object cxGrid3DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Visible = False
              Options.Editing = False
              Width = 91
            end
            object cxGrid3DBTableView1PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER'
              Visible = False
              Options.Editing = False
              Width = 107
            end
            object cxGrid3DBTableView1ID: TcxGridDBColumn
              DataBinding.FieldName = 'ID'
              Visible = False
              Options.Editing = False
              Width = 90
            end
            object cxGrid3DBTableView1ODBIENA: TcxGridDBColumn
              DataBinding.FieldName = 'ODBIENA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.ImmediateDropDownWhenKeyPressed = False
              Properties.ImmediatePost = True
              Properties.Items = <
                item
                  Description = #1042#1072#1083#1080#1076#1085#1072
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = #1054#1076#1073#1080#1077#1085#1072
                  Value = 1
                end>
              Properties.OnEditValueChanged = cxGrid3DBTableView1ODBIENAPropertiesEditValueChanged
            end
            object cxGrid3DBTableView1ODBIENA_OBRAZLOZENIE: TcxGridDBColumn
              DataBinding.FieldName = 'ODBIENA_OBRAZLOZENIE'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.OnEditValueChanged = cxGrid3DBTableView1ODBIENA_OBRAZLOZENIEPropertiesEditValueChanged
              Width = 189
            end
            object cxGrid3DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'JN_TIP_ARTIKAL'
              Options.Editing = False
              Width = 54
            end
            object cxGrid3DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'VIDARTIKALNAZIV'
              Options.Editing = False
              Width = 79
            end
            object cxGrid3DBTableView1GENERIKA: TcxGridDBColumn
              DataBinding.FieldName = 'GENERIKA'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.Items = <
                item
                  ImageIndex = 0
                  Value = 1
                end>
              Options.Editing = False
              Width = 68
            end
            object cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKAL'
              Options.Editing = False
              Width = 64
            end
            object cxGrid3DBTableView1ARTIKALNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKALNAZIV'
              Options.Editing = False
              Width = 200
            end
            object cxGrid3DBTableView1STAVKA_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'STAVKA_NAZIV'
              PropertiesClassName = 'TcxRichEditProperties'
              Width = 200
            end
            object cxGrid3DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              PropertiesClassName = 'TcxRichEditProperties'
              Properties.MemoMode = True
              Options.Editing = False
              Width = 208
            end
            object cxGrid3DBTableView1MERKA: TcxGridDBColumn
              DataBinding.FieldName = 'MERKA'
              Options.Editing = False
              Width = 38
            end
            object cxGrid3DBTableView1STAVKA_MERKA: TcxGridDBColumn
              DataBinding.FieldName = 'STAVKA_MERKA'
              Width = 79
            end
            object cxGrid3DBTableView1MERKANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'MERKANAZIV'
              Options.Editing = False
              Width = 79
            end
            object cxGrid3DBTableView1KOLICINA: TcxGridDBColumn
              DataBinding.FieldName = 'KOLICINA'
              Options.IncSearch = False
              Styles.Content = dmRes.cxStyle120
              Width = 58
            end
            object cxGrid3DBTableView1CENA_PRVA_BEZDDV: TcxGridDBColumn
              DataBinding.FieldName = 'CENA_PRVA_BEZDDV'
              Options.Editing = False
              Options.IncSearch = False
              Width = 107
            end
            object cxGrid3DBTableView1CENABEZDDV: TcxGridDBColumn
              DataBinding.FieldName = 'CENABEZDDV'
              Options.IncSearch = False
              Styles.Content = dmRes.cxStyle120
              Width = 77
            end
            object cxGrid3DBTableView1DANOK: TcxGridDBColumn
              DataBinding.FieldName = 'DANOK'
              Options.IncSearch = False
              Styles.Content = dmRes.cxStyle120
              Width = 39
            end
            object cxGrid3DBTableView1CENA: TcxGridDBColumn
              DataBinding.FieldName = 'CENA'
              Options.IncSearch = False
              Styles.Content = dmRes.cxStyle120
            end
            object cxGrid3DBTableView1IZNOSCENBEZDDV: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOSCENBEZDDV'
              Options.Editing = False
              Width = 88
            end
            object cxGrid3DBTableView1IZNOSCENA: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOSCENA'
              Options.Editing = False
              Width = 87
            end
            object cxGrid3DBTableView1GRUPA: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPA'
              Options.Editing = False
              Width = 44
            end
            object cxGrid3DBTableView1GRUPANAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'GRUPANAZIV'
              Options.Editing = False
              Width = 150
            end
            object cxGrid3DBTableView1PROZVODITEL: TcxGridDBColumn
              DataBinding.FieldName = 'PROZVODITEL'
              Options.Editing = False
              Width = 78
            end
            object cxGrid3DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PROIZVODITELNAZIV'
              Options.Editing = False
              Width = 134
            end
            object cxGrid3DBTableView1JN_CPV: TcxGridDBColumn
              DataBinding.FieldName = 'JN_CPV'
              Options.Editing = False
              Width = 111
            end
            object cxGrid3DBTableView1MK: TcxGridDBColumn
              DataBinding.FieldName = 'MK'
              Options.Editing = False
              Width = 50
            end
            object cxGrid3DBTableView1EN: TcxGridDBColumn
              DataBinding.FieldName = 'EN'
              Options.Editing = False
              Width = 50
            end
            object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Options.Editing = False
              Width = 161
            end
            object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Options.Editing = False
              Width = 214
            end
            object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Options.Editing = False
              Width = 201
            end
            object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Options.Editing = False
              Width = 234
            end
            object cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn
              DataBinding.FieldName = 'VID_ARTIKAL'
              Visible = False
              Options.Editing = False
            end
          end
          object cxGrid3Level1: TcxGridLevel
            GridView = cxGrid3DBTableView1
          end
        end
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 912
          Height = 51
          Align = alTop
          TabOrder = 1
          object Label8: TLabel
            Left = 14
            Top = 10
            Width = 155
            Height = 19
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1072' '#1073#1088#1086#1112' :'
            Color = clBtnFace
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = 10901821
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object Label7: TLabel
            Left = 14
            Top = 29
            Width = 155
            Height = 19
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
            Color = clBtnFace
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = 10901821
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
          end
          object cxDBLabel1: TcxDBLabel
            Left = 175
            Top = 9
            AutoSize = True
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clNavy
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Transparent = True
          end
          object cxDBLabel2: TcxDBLabel
            Left = 175
            Top = 29
            AutoSize = True
            DataBinding.DataField = 'PARTNERNAZIV'
            DataBinding.DataSource = dm.dsPonudi
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clNavy
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Transparent = True
          end
        end
      end
    end
  end
  object PanelGrupnoBodiranje: TPanel
    Left = 398
    Top = 392
    Width = 553
    Height = 264
    Anchors = []
    BevelOuter = bvNone
    Color = 11302478
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 5
    Visible = False
    DesignSize = (
      553
      264)
    object Panel8: TPanel
      Left = 0
      Top = 41
      Width = 553
      Height = 178
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      TabOrder = 0
      object cxGrid4: TcxGrid
        Left = 2
        Top = 2
        Width = 549
        Height = 174
        Align = alClient
        TabOrder = 0
        ExplicitLeft = -78
        ExplicitTop = 229
        object cxGrid4DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid4DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsGrupnoBodiranjePonudiStavki
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
              Column = cxGrid4DBTableView1MAX_BODOVI
            end
            item
              Kind = skSum
              Position = spFooter
              Column = cxGrid4DBTableView1BODOVI
            end
            item
              Kind = skSum
              Position = spFooter
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00 , .'
              Kind = skSum
              OnGetText = cxGrid4DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
              Column = cxGrid4DBTableView1BODOVI
            end
            item
              Format = '0.00 , .'
              Kind = skSum
              Column = cxGrid4DBTableView1MAX_BODOVI
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid4DBTableView1BROJ_TENDER: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ_TENDER'
            Visible = False
            Options.Editing = False
            Width = 117
          end
          object cxGrid4DBTableView1TIP_PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_PARTNER'
            Visible = False
            Options.Editing = False
          end
          object cxGrid4DBTableView1PARTNER: TcxGridDBColumn
            DataBinding.FieldName = 'PARTNER'
            Visible = False
            Options.Editing = False
          end
          object cxGrid4DBTableView1BODIRANJE_PO: TcxGridDBColumn
            DataBinding.FieldName = 'BODIRANJE_PO'
            Visible = False
            Options.Editing = False
            Width = 120
          end
          object cxGrid4DBTableView1BODIRANJE: TcxGridDBColumn
            DataBinding.FieldName = 'BODIRANJE'
            Visible = False
            Options.Editing = False
            Width = 95
          end
          object cxGrid4DBTableView1KRITERIUMNAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'KRITERIUMNAZIV'
            Options.Editing = False
            Width = 171
          end
          object cxGrid4DBTableView1vrednuvanjeNaziv: TcxGridDBColumn
            DataBinding.FieldName = 'vrednuvanjeNaziv'
            Options.Editing = False
            Width = 150
          end
          object cxGrid4DBTableView1bodiranjeNaziv: TcxGridDBColumn
            DataBinding.FieldName = 'bodiranjeNaziv'
            Visible = False
            Options.Editing = False
            Width = 77
          end
          object cxGrid4DBTableView1MAX_BODOVI: TcxGridDBColumn
            DataBinding.FieldName = 'MAX_BODOVI'
            Options.Editing = False
            Width = 72
          end
          object cxGrid4DBTableView1BODOVI: TcxGridDBColumn
            DataBinding.FieldName = 'BODOVI'
            Options.IncSearch = False
            Styles.Content = dmRes.cxStyle120
          end
          object cxGrid4DBTableView1VREDNOST: TcxGridDBColumn
            DataBinding.FieldName = 'VREDNOST'
          end
        end
        object cxGrid4Level1: TcxGridLevel
          GridView = cxGrid4DBTableView1
        end
      end
    end
    object btnOK: TcxButton
      Left = 8
      Top = 229
      Width = 91
      Height = 25
      Action = aZapisiGrupnoBodiranje
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 1
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton2: TcxButton
      Left = 105
      Top = 229
      Width = 91
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 553
      Height = 41
      Align = alTop
      Caption = #1043#1088#1091#1087#1085#1086' '#1073#1086#1076#1080#1088#1072#1114#1077
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 800
    Top = 448
  end
  object PopupMenu1: TPopupMenu
    Left = 832
    Top = 120
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 888
    Top = 552
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1086#1085#1091#1076#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 892
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1165
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1080
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 234
      DockedTop = 0
      FloatLeft = 1034
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1080
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1259
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 929
      DockedTop = 0
      FloatLeft = 1259
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      CaptionButtons = <>
      DockedLeft = 313
      DockedTop = 0
      FloatLeft = 1318
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = '   '
      CaptionButtons = <>
      DockedLeft = 670
      DockedTop = 0
      FloatLeft = 1318
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar10: TdxBar
      Caption = '  '
      CaptionButtons = <>
      DockedLeft = 586
      DockedTop = 0
      FloatLeft = 1310
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar11: TdxBar
      Caption = #1044#1086#1075#1086#1074#1086#1088
      CaptionButtons = <>
      DockedLeft = 748
      DockedTop = 0
      FloatLeft = 1334
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton32'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton33'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavka
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavka
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarCombo1: TdxBarCombo
      Category = 0
      Visible = ivAlways
      ItemIndex = -1
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1057#1090#1072#1090#1091#1089
      Category = 0
      Hint = #1057#1090#1072#1090#1091#1089
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Columns = 2
      Properties.Items = <
        item
        end
        item
        end>
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aZacuvajStavkiExcel
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiTabelaStavki
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPageSetupStavki
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aSnimiPecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aSnimiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aBrisiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aRefreshStavki
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aAvtomatskoBodiranje
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end>
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1088#1077#1075#1083#1077#1076#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton14'
        end>
    end
    object dxBarButton3: TdxBarButton
      Action = aPPonudaOdPartnerSoOpisNaArtikal
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = aPPonudaOdPartner
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = aPListaSitePonud
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aPListaSitePonudiSoOpisNaArtikal
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080' '#1089#1086' '#1073#1086#1076#1086#1074#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end>
    end
    object dxBarButton7: TdxBarButton
      Action = aPListaSitePonudiSoBod
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton9: TdxBarButton
      Action = aPListaPonudiSoBodPoGrupaArtikal
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aDostaveniDokumenti
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aAukcija
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton11: TdxBarButton
      Action = aPIzvestuvanjeZaOtfrlenaPonuda
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = aPPocetnaRangListaZaUcestvoNaEAukcija
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = aPecatiCeniPredIPotoa
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Action = aZacuvajSostojbaPredAukcija
      Category = 0
    end
    object dxBarButton14: TdxBarButton
      Action = aPIzvesstajPoGrupiIPartneri
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Action = actPIzvestuvanjeZaNedobitnaFirma
      Category = 0
    end
    object dxBarButton16: TdxBarButton
      Action = actPIzvestuvanjeZaOtfrlenaPonudaGrupno
      Category = 0
    end
    object dxBarButton17: TdxBarButton
      Action = actPIzvestuvanjeZaNedobitnaFirmaGrupno
      Category = 0
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Action = actDogovor
      Category = 0
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = actDogovorKreiraj
      Category = 0
    end
    object dxBarButton18: TdxBarButton
      Action = actPIzvestuvanjeZaOtfrlenaPonudaExportPdf
      Category = 0
    end
    object dxBarButton19: TdxBarButton
      Action = actPIzvestuvanjeZaNedobitnaFirmaExportPdf
      Category = 0
    end
    object dxBarButton20: TdxBarButton
      Action = actPDogovorOtstapuvanjeOpremaNaKoristenje
      Category = 0
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1089#1090#1086#1082#1080
      Category = 0
      Visible = ivNever
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton21'
        end>
    end
    object dxBarButton21: TdxBarButton
      Action = actPDogovorOtstapuvanjeOpremaNaKoristenje
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 480
    Top = 128
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aDodadiStavka: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aDodadiStavkaExecute
    end
    object aBrisiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
      ShortCut = 16503
      OnExecute = aBrisiStavkaExecute
    end
    object aZacuvajStavkiExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 9
      OnExecute = aZacuvajStavkiExcelExecute
    end
    object aPecatiTabelaStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaStavkiExecute
    end
    object aPodesuvanjePecatenjeStavki: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeStavkiExecute
    end
    object aPageSetupStavki: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupStavkiExecute
    end
    object aSnimiPecatenjeStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeStavkiExecute
    end
    object aBrisiPodesuvanjePecatenjeStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeStavkiExecute
    end
    object aSnimiIzgledStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledStavkiExecute
    end
    object aBrisiIzgledStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledStavkiExecute
    end
    object aGrupnoBodirawe: TAction
      Caption = #1043#1088#1091#1087#1085#1086' '#1073#1086#1076#1080#1088#1072#1114#1077' '#1080' '#1074#1088#1077#1076#1085#1091#1074#1072#1114#1077' '
      ImageIndex = 41
      OnExecute = aGrupnoBodiraweExecute
    end
    object aZapisiGrupnoBodiranje: TAction
      Caption = 'OK'
      ImageIndex = 7
      OnExecute = aZapisiGrupnoBodiranjeExecute
    end
    object aRefreshStavki: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 52
      ShortCut = 16502
      OnExecute = aRefreshStavkiExecute
    end
    object aAvtomatskoBodiranje: TAction
      Caption = #1040#1074#1090#1086#1084#1072#1090#1089#1082#1086' '#1073#1086#1076#1080#1088#1072#1114#1077
      ImageIndex = 40
      OnExecute = aAvtomatskoBodiranjeExecute
    end
    object aPPonudaOdPartnerSoOpisNaArtikal: TAction
      Caption = #1055#1086#1085#1091#1076#1072' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083')'
      ImageIndex = 19
      OnExecute = aPPonudaOdPartnerSoOpisNaArtikalExecute
    end
    object aDizajnPonudaOdPartnerSoOpisNaArtikal: TAction
      Caption = #1055#1086#1085#1091#1076#1072' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083')'
      ImageIndex = 19
      OnExecute = aDizajnPonudaOdPartnerSoOpisNaArtikalExecute
    end
    object aPopUpMeniZadizajnReport: TAction
      Caption = 'aPopUpMeniZadizajnReport'
      ShortCut = 16505
      OnExecute = aPopUpMeniZadizajnReportExecute
    end
    object aPPonudaOdPartner: TAction
      Caption = #1055#1086#1085#1091#1076#1072' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 19
      OnExecute = aPPonudaOdPartnerExecute
    end
    object aDizajnPonudaOdPartner: TAction
      Caption = #1055#1086#1085#1091#1076#1072' '#1086#1076' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088
      ImageIndex = 19
      OnExecute = aDizajnPonudaOdPartnerExecute
    end
    object aPListaSitePonud: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aPListaSitePonudExecute
    end
    object aDizajnListaSitePonud: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aDizajnListaSitePonudExecute
    end
    object aPListaSitePonudiSoOpisNaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083')'
      ImageIndex = 19
      OnExecute = aPListaSitePonudiSoOpisNaArtikalExecute
    end
    object aDizajnListaSitePonudiSoOpisNaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083')'
      ImageIndex = 19
      OnExecute = aDizajnListaSitePonudiSoOpisNaArtikalExecute
    end
    object aPListaSitePonudiSoBod: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080
      ImageIndex = 19
      OnExecute = aPListaSitePonudiSoBodExecute
    end
    object aDizajnListaSitePonudiSoBod: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '
      ImageIndex = 19
      OnExecute = aDizajnListaSitePonudiSoBodExecute
    end
    object aPListaPonudiSoBodPoGrupaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1075#1088#1091#1087#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      ImageIndex = 19
      OnExecute = aPListaPonudiSoBodPoGrupaArtikalExecute
    end
    object aDizajnListaPonudiSoBodPoGrupaArtikal: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1075#1088#1091#1087#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      ImageIndex = 19
      OnExecute = aDizajnListaPonudiSoBodPoGrupaArtikalExecute
    end
    object aDostaveniDokumenti: TAction
      Caption = #1044#1086#1089#1090#1072#1074#1077#1085#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
      ImageIndex = 14
      OnExecute = aDostaveniDokumentiExecute
    end
    object aAukcija: TAction
      Caption = #1077'-'#1040#1091#1082#1094#1080#1112#1072
      ImageIndex = 41
      OnExecute = aAukcijaExecute
    end
    object aPIzvestuvanjeZaOtfrlenaPonuda: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1086#1076#1073#1080#1077#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = aPIzvestuvanjeZaOtfrlenaPonudaExecute
    end
    object aDIzvestuvanjeZaOtfrlenaPonuda: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1086#1076#1073#1080#1077#1085#1072' '#1087#1086#1085#1091#1076#1072
      ImageIndex = 19
      OnExecute = aDIzvestuvanjeZaOtfrlenaPonudaExecute
    end
    object aPPocetnaRangListaZaUcestvoNaEAukcija: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072
      ImageIndex = 19
      OnExecute = aPPocetnaRangListaZaUcestvoNaEAukcijaExecute
    end
    object aDPocetnaRangListaZaUcestvoNaEAukcija: TAction
      Caption = 
        #1055#1086#1095#1077#1090#1085#1072' '#1088#1072#1085#1075'-'#1083#1080#1089#1090#1072' '#1085#1072' '#1089#1087#1086#1089#1086#1073#1085#1080' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080' '#1079#1072' '#1091#1095#1077#1089#1090#1074#1086' '#1085#1072' '#1077'-'#1072#1091#1082#1094#1080#1112 +
        #1072
      ImageIndex = 19
      OnExecute = aDPocetnaRangListaZaUcestvoNaEAukcijaExecute
    end
    object aPecatiCeniPredIPotoa: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1094#1077#1085#1080#1090#1077' '#1087#1086' '#1072#1091#1082#1094#1080#1112#1072
      ImageIndex = 19
      OnExecute = aPecatiCeniPredIPotoaExecute
    end
    object aDecatiCeniPredIPotoa: TAction
      Caption = #1057#1087#1077#1094#1080#1092#1080#1082#1072#1094#1080#1112#1072' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1094#1077#1085#1080#1090#1077' '#1087#1086' '#1072#1091#1082#1094#1080#1112#1072
      ImageIndex = 19
      OnExecute = aDecatiCeniPredIPotoaExecute
    end
    object aZacuvajSostojbaPredAukcija: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112'/'#1055#1088#1077#1075#1083#1077#1076#1072#1112' '#1089#1086#1089#1090#1086#1112#1073#1072' '#1087#1088#1077#1076' '#1077'-'#1040#1091#1082#1094#1080#1112#1072
      ImageIndex = 59
      OnExecute = aZacuvajSostojbaPredAukcijaExecute
    end
    object aPIzvesstajPoGrupiIPartneri: TAction
      Caption = 
        #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083', '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1087#1072#1088#1090#1085#1077#1088' '#1080 +
        ' '#1087#1086#1076#1075#1088#1091#1087#1072') '
      ImageIndex = 19
      OnExecute = aPIzvesstajPoGrupiIPartneriExecute
    end
    object aDIzvesstajPoGrupiIPartneri: TAction
      Caption = 
        #1051#1080#1089#1090#1072' '#1085#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1080' ('#1089#1086' '#1086#1087#1080#1089' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083', '#1075#1088#1091#1087#1080#1088#1072#1085#1080' '#1087#1086' '#1087#1072#1088#1090#1085#1077#1088' '#1080 +
        ' '#1087#1086#1076#1075#1088#1091#1087#1072') '
      ImageIndex = 19
      OnExecute = aDIzvesstajPoGrupiIPartneriExecute
    end
    object actPIzvestuvanjeZaNedobitnaFirma: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1085#1077#1076#1086#1073#1080#1090#1085#1072' '#1092#1080#1088#1084#1072
      ImageIndex = 19
      OnExecute = actPIzvestuvanjeZaNedobitnaFirmaExecute
    end
    object actDIzvestuvanjeZaNedobitnaFirma: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1085#1077#1076#1086#1073#1080#1090#1085#1072' '#1092#1080#1088#1084#1072
      ImageIndex = 19
      OnExecute = actDIzvestuvanjeZaNedobitnaFirmaExecute
    end
    object actPIzvestuvanjeZaOtfrlenaPonudaGrupno: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1086#1076#1073#1080#1077#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1043#1088#1091#1087#1085#1086' '#1079#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
      ImageIndex = 19
      OnExecute = actPIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute
    end
    object actDIzvestuvanjeZaOtfrlenaPonudaGrupno: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1086#1076#1073#1080#1077#1085#1072' '#1087#1086#1085#1091#1076#1072' - '#1043#1088#1091#1087#1085#1086' '#1079#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
      ImageIndex = 19
      OnExecute = actDIzvestuvanjeZaOtfrlenaPonudaGrupnoExecute
    end
    object actPIzvestuvanjeZaNedobitnaFirmaGrupno: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1085#1077#1076#1086#1073#1080#1090#1085#1072' '#1092#1080#1088#1084#1072' - '#1043#1088#1091#1087#1085#1086' '#1079#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
      ImageIndex = 19
      OnExecute = actPIzvestuvanjeZaNedobitnaFirmaGrupnoExecute
    end
    object actDIzvestuvanjeZaNedobitnaFirmaGrupno: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1085#1077#1076#1086#1073#1080#1090#1085#1072' '#1092#1080#1088#1084#1072' - '#1043#1088#1091#1087#1085#1086' '#1079#1072' '#1089#1080#1090#1077' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1080
      ImageIndex = 19
      OnExecute = actDIzvestuvanjeZaNedobitnaFirmaGrupnoExecute
    end
    object actDogovor: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088
      ImageIndex = 19
      OnExecute = actDogovorExecute
    end
    object actDDogovori_Stoki: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' - '#1057#1090#1086#1082#1080
      OnExecute = actDDogovori_StokiExecute
    end
    object actDDogovori_Uslugi: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' - '#1059#1089#1083#1091#1075#1080
      OnExecute = actDDogovori_UslugiExecute
    end
    object actDDogovori_Raboti: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' - '#1056#1072#1073#1086#1090#1080
      OnExecute = actDDogovori_RabotiExecute
    end
    object actDogovorKreiraj: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1076#1086#1075#1086#1074#1086#1088
      ImageIndex = 70
      OnExecute = actDogovorKreirajExecute
    end
    object actPIzvestuvanjeZaOtfrlenaPonudaExportPdf: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1086#1076#1073#1080#1077#1085#1072' '#1087#1086#1085#1091#1076#1072' - Export to .Pdf'
      ImageIndex = 103
      OnExecute = actPIzvestuvanjeZaOtfrlenaPonudaExportPdfExecute
    end
    object actPIzvestuvanjeZaNedobitnaFirmaExportPdf: TAction
      Caption = #1048#1079#1074#1077#1089#1090#1091#1074#1072#1114#1077' '#1079#1072' '#1085#1077#1076#1086#1073#1080#1090#1085#1072' '#1092#1080#1088#1084#1072' - Export to .Pdf'
      ImageIndex = 103
      OnExecute = actPIzvestuvanjeZaNedobitnaFirmaExportPdfExecute
    end
    object actDDogovorOtstapuvanjeOpremaNaKoristenje: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1032#1053' '#1085#1072' '#1089#1090#1086#1082#1080' '#1089#1086' '#1086#1090#1089#1090#1072#1087#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1087#1088#1077#1084#1072' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
      OnExecute = actDDogovorOtstapuvanjeOpremaNaKoristenjeExecute
    end
    object actPDogovorOtstapuvanjeOpremaNaKoristenje: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1032#1053' '#1085#1072' '#1089#1090#1086#1082#1080' '#1089#1086' '#1086#1090#1089#1090#1072#1087#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1087#1088#1077#1084#1072' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
      ImageIndex = 19
      OnExecute = actPDogovorOtstapuvanjeOpremaNaKoristenjeExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 688
    Top = 112
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 760
    Top = 160
  end
  object dsPartner: TDataSource
    AutoEdit = False
    DataSet = tblPartner
    Left = 648
    Top = 256
  end
  object tblPartner: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAT_PARTNER'
      'SET '
      '    NAZIV = :NAZIV,'
      '    NAZIV_SKRATEN = :NAZIV_SKRATEN,'
      '    ADRESA = :ADRESA,'
      '    TEL = :TEL,'
      '    FAX = :FAX,'
      '    DANOCEN = :DANOCEN,'
      '    MESTO = :MESTO,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    TATKO = :TATKO,'
      '    LOGO = :LOGO,'
      '    RE = :RE,'
      '    LOGOTEXT = :LOGOTEXT,'
      '    STATUS = :STATUS,'
      '    MAIL = :MAIL'
      'WHERE'
      '    TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_PARTNER'
      'WHERE'
      '        TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAT_PARTNER('
      '    TIP_PARTNER,'
      '    ID,'
      '    NAZIV,'
      '    NAZIV_SKRATEN,'
      '    ADRESA,'
      '    TEL,'
      '    FAX,'
      '    DANOCEN,'
      '    MESTO,'
      '    IME,'
      '    PREZIME,'
      '    TATKO,'
      '    LOGO,'
      '    RE,'
      '    LOGOTEXT,'
      '    STATUS,'
      '    MAIL'
      ')'
      'VALUES('
      '    :TIP_PARTNER,'
      '    :ID,'
      '    :NAZIV,'
      '    :NAZIV_SKRATEN,'
      '    :ADRESA,'
      '    :TEL,'
      '    :FAX,'
      '    :DANOCEN,'
      '    :MESTO,'
      '    :IME,'
      '    :PREZIME,'
      '    :TATKO,'
      '    :LOGO,'
      '    :RE,'
      '    :LOGOTEXT,'
      '    :STATUS,'
      '    :MAIL'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    mp.TIP_PARTNER,'
      '    mtp.naziv as TIP_NAZIV,'
      '    mp.ID,'
      '    mp.NAZIV,'
      '    mp.naziv_skraten,'
      '    mp.ADRESA,'
      '    mp.TEL,'
      '    mp.FAX,'
      '    mp.DANOCEN,'
      '    mp.MESTO,'
      '    mm.naziv as MESTO_NAZIV,'
      '    mp.IME,'
      '    mp.PREZIME,'
      '    mp.TATKO,'
      '    mp.LOGO,'
      '    mp.RE,'
      '    mp.LOGOTEXT,'
      '    mp.STATUS,'
      '    mp.mail'
      'from MAT_PARTNER mp'
      'left join MAT_TIP_PARTNER mtp on mtp.id = mp.tip_partner'
      'left join MAT_MESTO mm on mm.id = mp.mesto'
      ''
      ' WHERE '
      '        MP.TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and MP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    mp.TIP_PARTNER,'
      '    mtp.naziv as TIP_NAZIV,'
      '    mp.ID,'
      '    mp.NAZIV,'
      '    mp.naziv_skraten,'
      '    mp.RE,'
      '    mp.STATUS'
      'from MAT_PARTNER mp'
      'left join MAT_TIP_PARTNER mtp on mtp.id = mp.tip_partner'
      'where mp.tip_partner like :tip'
      'order by mp.tip_partner, mp.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1200
    Top = 416
    poSQLINT64ToBCD = True
    oTrimCharFields = False
    oRefreshAfterPost = False
    object tblPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblPartnerTIP_NAZIV: TFIBStringField
      DisplayLabel = #1058#1055
      FieldName = 'TIP_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1089#1082#1088#1072#1090#1077#1085
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblPartnerSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 680
    Top = 216
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 128
    Top = 456
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid4
    PopupMenus = <>
    Left = 848
    Top = 448
  end
  object PopupMenu2: TPopupMenu
    Left = 576
    Top = 232
    object TMenuItem
    end
    object N3: TMenuItem
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1088#1077#1075#1083#1077#1076#1080
      ImageIndex = 19
      object N7: TMenuItem
        Action = aDizajnPonudaOdPartner
      end
      object N8: TMenuItem
        Action = aDizajnPonudaOdPartnerSoOpisNaArtikal
      end
      object N9: TMenuItem
        Action = aDizajnListaSitePonud
      end
      object N10: TMenuItem
        Action = aDizajnListaSitePonudiSoOpisNaArtikal
      end
      object N13: TMenuItem
        Action = aDIzvesstajPoGrupiIPartneri
      end
    end
    object N2: TMenuItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080' '#1089#1086' '#1073#1086#1076#1086#1074#1080
      ImageIndex = 19
      object N4: TMenuItem
        Action = aDizajnListaSitePonudiSoBod
      end
      object N5: TMenuItem
        Action = aDizajnListaPonudiSoBodPoGrupaArtikal
      end
    end
    object N6: TMenuItem
      Action = aDIzvestuvanjeZaOtfrlenaPonuda
    end
    object N16: TMenuItem
      Action = actDIzvestuvanjeZaOtfrlenaPonudaGrupno
    end
    object N14: TMenuItem
      Action = actDIzvestuvanjeZaNedobitnaFirma
    end
    object N15: TMenuItem
      Action = actDIzvestuvanjeZaNedobitnaFirmaGrupno
    end
    object N12: TMenuItem
      Action = aDecatiCeniPredIPotoa
    end
    object N11: TMenuItem
      Action = aDPocetnaRangListaZaUcestvoNaEAukcija
    end
    object actDDogovoriStoki1: TMenuItem
      Action = actDDogovori_Stoki
    end
    object actDDogovorOtstapuvanjeOpremaNaKoristenje1: TMenuItem
      Action = actDDogovorOtstapuvanjeOpremaNaKoristenje
      Caption = #1044#1086#1075#1086#1074#1086#1088' - '#1057#1090#1086#1082#1080' '#1089#1086' '#1086#1090#1089#1090#1072#1087#1091#1074#1072#1114#1077' '#1085#1072' '#1086#1087#1088#1077#1084#1072' '#1085#1072' '#1082#1086#1088#1080#1089#1090#1077#1114#1077
    end
    object actDDogovoriRaboti1: TMenuItem
      Action = actDDogovori_Raboti
    end
    object actDDogovoriUslugi1: TMenuItem
      Action = actDDogovori_Uslugi
    end
  end
  object qAukcijaDatum1: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 a.datum'
      'from jn_e_aukcija  a'
      'where a.broj_tender = :tender'
      'order by a.ts_ins desc')
    Left = 328
    Top = 120
  end
  object qAukcijaDatum: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 a.datum'
      'from jn_e_aukcija  a'
      'where a.broj_tender = :tender'
      'order by a.ts_ins desc')
    Left = 296
    Top = 408
  end
  object dlgOpenExportPdf: TOpenDialog
    Left = 368
    Top = 288
  end
end
