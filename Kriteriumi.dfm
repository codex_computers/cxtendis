inherited frmKriteriumi: TfrmKriteriumi
  Caption = #1050#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1073#1086#1076#1080#1088#1072#1114#1077
  ClientHeight = 604
  ClientWidth = 670
  ExplicitWidth = 686
  ExplicitHeight = 643
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 670
    Height = 282
    ExplicitWidth = 670
    ExplicitHeight = 282
    inherited cxGrid1: TcxGrid
      Width = 666
      Height = 278
      ExplicitWidth = 666
      ExplicitHeight = 278
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsKriterium
        object cxGrid1DBTableView1SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'SIFRA'
          Width = 55
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 228
        end
        object cxGrid1DBTableView1VREDNUVANJE: TcxGridDBColumn
          DataBinding.FieldName = 'VREDNUVANJE'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1vrednuvanjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'vrednuvanjeNaziv'
          Width = 276
        end
        object cxGrid1DBTableView1BODIRANJE: TcxGridDBColumn
          DataBinding.FieldName = 'BODIRANJE'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1bodiranjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'bodiranjeNaziv'
          Width = 64
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 408
    Width = 670
    Height = 173
    ExplicitTop = 408
    ExplicitWidth = 670
    ExplicitHeight = 173
    object Label2: TLabel [1]
      Left = 13
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      DataBinding.DataField = 'SIFRA'
      DataBinding.DataSource = dm.dsKriterium
    end
    inherited OtkaziButton: TcxButton
      Left = 579
      Top = 133
      TabOrder = 5
      ExplicitLeft = 579
      ExplicitTop = 133
    end
    inherited ZapisiButton: TcxButton
      Left = 498
      Top = 133
      TabOrder = 4
      ExplicitLeft = 498
      ExplicitTop = 133
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 69
      Top = 46
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsKriterium
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 380
    end
    object rgBodiranje: TcxDBRadioGroup
      Left = 69
      Top = 73
      Hint = 
        #1044#1072#1083#1080' '#1077' '#1087#1086#1090#1088#1077#1073#1085#1086' '#1072#1074#1090#1086#1084#1072#1090#1089#1082#1086' '#1073#1086#1076#1080#1088#1072#1114#1077' '#1080#1083#1080' '#1085#1077'. '#1042#1086' '#1089#1083#1091#1095#1072#1112' '#1082#1086#1075#1072' '#1085#1077' '#1089#1077 +
        ' '#1087#1088#1072#1074#1080' '#1073#1086#1076#1080#1088#1072#1114#1077' '#1072#1074#1090#1086#1084#1072#1090#1089#1082#1080', '#1073#1086#1076#1086#1074#1080#1090#1077' '#1089#1077' '#1087#1086#1087#1086#1083#1085#1091#1074#1072#1072#1090' '#1084#1072#1085#1091#1077#1083#1085#1086' ('#1085#1072 +
        ' '#1087#1088#1080#1084#1077#1088' '#1082#1088#1080#1090#1077#1088#1080#1091#1084' '#1079#1072' '#1082#1074#1072#1083#1080#1090#1077#1090')  '
      TabStop = False
      Caption = #1041#1086#1076#1080#1088#1072#1114#1077
      DataBinding.DataField = 'BODIRANJE'
      DataBinding.DataSource = dm.dsKriterium
      Properties.Items = <
        item
          Caption = #1044#1072
          Value = 1
        end
        item
          Caption = #1053#1077
          Value = 0
        end>
      TabOrder = 2
      Height = 56
      Width = 80
    end
    object rgVrednuvanje: TcxDBRadioGroup
      Left = 167
      Top = 73
      Hint = #1053#1072#1095#1080#1085' '#1085#1072' '#1082#1086#1112' '#1089#1077' '#1087#1088#1072#1074#1080' '#1074#1088#1077#1076#1085#1091#1074#1072#1114#1077
      TabStop = False
      Caption = #1042#1088#1077#1076#1085#1091#1074#1072#1114#1077
      DataBinding.DataField = 'VREDNUVANJE'
      DataBinding.DataSource = dm.dsKriterium
      Properties.Items = <
        item
          Caption = #1055#1086#1084#1072#1083#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080
          Value = 1
        end
        item
          Caption = #1055#1086#1075#1086#1083#1077#1084#1072' '#1074#1088#1077#1076#1085#1086#1089#1090' '#1076#1086#1073#1080#1074#1072' '#1087#1086#1074#1077#1116#1077' '#1073#1086#1076#1086#1074#1080
          Value = 0
        end>
      TabOrder = 3
      Height = 56
      Width = 282
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 670
    ExplicitWidth = 670
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 581
    Width = 670
    ExplicitTop = 581
    ExplicitWidth = 670
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40920.628079861110000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.AnimationDelay = 300
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 128
  end
end
