unit AzurirajNazivMerkaStavka;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles,  cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, FIBDataSet,
  pFIBDataSet, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxRichEdit, cxGridCustomPopupMenu,
  cxGridPopupMenu, System.Actions, Vcl.ActnList, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxContainer, cxTextEdit, cxMemo,
  cxDBRichEdit, Vcl.ExtCtrls, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TfrmAzurirajStavkiNabavka = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tblStavkiNabavka: TpFIBDataSet;
    dsStavkiNabavka: TDataSource;
    tblStavkiNabavkaNAZIV: TFIBStringField;
    tblStavkiNabavkaMERKA: TFIBStringField;
    tblStavkiNabavkaSIF_NAZIV: TFIBStringField;
    tblStavkiNabavkaSIF_MERKA: TFIBStringField;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1SIF_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1SIF_MERKA: TcxGridDBColumn;
    cxGridPopupMenu2: TcxGridPopupMenu;
    tblStavkiNabavkaARTIKAL: TFIBIntegerField;
    tblStavkiNabavkaVID_ARTIKAL: TFIBIntegerField;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    tblStavkiNabavkaVIDARTIKALNAZIV: TFIBStringField;
    cxGrid1DBTableView1VIDARTIKALNAZIV: TcxGridDBColumn;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    aBrisiIzgled: TAction;
    aSifArtikli: TAction;
    aDodadiNedelivaGrupa: TAction;
    aBrisiNedelivaGrupa: TAction;
    aOkNedeliviGrupi: TAction;
    aKopirajStavkiOdPlan: TAction;
    aSpustiSoberi: TAction;
    aBrisiStavkiGrupa: TAction;
    aOKBrisiStavkiGrupa: TAction;
    aAzurirajNazivMerka: TAction;
    aNotepad: TAction;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    Panel1: TPanel;
    cxButton1: TcxButton;
    aPrevzemiStavki: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNotepadExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aPrevzemiStavkiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAzurirajStavkiNabavka: TfrmAzurirajStavkiNabavka;

implementation

uses
  dmUnit, dmKonekcija, dmResources, Utils, Notepad, dmReportUnit, dmProcedureQuey;

{$R *.dfm}

procedure TfrmAzurirajStavkiNabavka.aNotepadExecute(Sender: TObject);
begin
    frmNotepad := TfrmNotepad.Create(Application);
    frmNotepad.LoadText(tblStavkiNabavkaNAZIV.Value);
    frmNotepad.ShowModal;
    if frmNotepad.ModalResult=mrOk then
    begin
      tblStavkiNabavka.Edit;
      tblStavkiNabavkaNAZIV.Value := frmNotepad.ReturnText;
      tblStavkiNabavka.Post;
    end;
    frmNotepad.Free;
end;

procedure TfrmAzurirajStavkiNabavka.aOtkaziExecute(Sender: TObject);
begin
     Close;
end;

procedure TfrmAzurirajStavkiNabavka.aPrevzemiStavkiExecute(Sender: TObject);
begin
      dmPQ.insert6(dmPQ.pPrevzemiNazivArtikal,'BROJ_TENDER_IN',null,Null,Null,Null,Null,
                              dm.tblTenderBROJ.Value,null,Null,Null,Null,Null);
      tblStavkiNabavka.Close;
      tblStavkiNabavka.Open;

end;

procedure TfrmAzurirajStavkiNabavka.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
//  var text_stream : TStream;
begin
    if(Key = VK_INSERT) then
  begin
    frmNotepad := TfrmNotepad.Create(Application);
    frmNotepad.LoadText(tblStavkiNabavkaSIF_NAZIV.Value);
    frmNotepad.ShowModal;
    if frmNotepad.ModalResult=mrOk then
    begin
     { if tblStavkiNabavka.State in [dsBrowse] then
        tblStavkiNabavka.Edit;
       { text_stream := TMemoryStream.Create;
        text_stream.Position := 0;
        text_stream:=  frmNotepad.ReturnStream(false);  }
     // tblStavkiNabavkaNAZIV.Value := frmNotepad.ReturnText;
    end;
    frmNotepad.Free;
  end ;

end;

procedure TfrmAzurirajStavkiNabavka.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin


    if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);

end;


procedure TfrmAzurirajStavkiNabavka.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if tblStavkiNabavka.State in [dsEdit] then
        tblStavkiNabavka.Post;
     tblStavkiNabavka.Close;
     dm.tblTenderStavki.FullRefresh;
end;

procedure TfrmAzurirajStavkiNabavka.FormShow(Sender: TObject);
begin
     tblStavkiNabavka.ParamByName('broj_tender').Value:=dm.tblTenderBROJ.Value;
     tblStavkiNabavka.Open;
     tblStavkiNabavka.Edit;

end;

end.
