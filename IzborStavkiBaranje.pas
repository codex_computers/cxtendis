unit IzborStavkiBaranje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxLabel, cxDBLabel, FIBDataSet, pFIBDataSet, dxCustomHint, cxHint,
  cxImageComboBox, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmIzborStavkiBaranja = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    aBrisiIzgled: TAction;
    Panel1: TPanel;
    cxGrid0: TcxGrid;
    cxGrid0DBTableView1: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    StatusBar1: TdxRibbonStatusBar;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid0DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid0DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid0DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid0DBTableView1POBARANA_CENA: TcxGridDBColumn;
    cxGrid0DBTableView1DOZVOLENA_CENA: TcxGridDBColumn;
    cxGrid0DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid0DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid0DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid0DBTableView1MERKA: TcxGridDBColumn;
    cxGrid0DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid0DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid0DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid0DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid0DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid0DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid0DBTableView1MK: TcxGridDBColumn;
    cxGrid0DBTableView1EN: TcxGridDBColumn;
    cxGrid0DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1POBARANA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1DOZVOLENA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKALNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1MK: TcxGridDBColumn;
    cxGrid1DBTableView1EN: TcxGridDBColumn;
    cxGrid1DBTableView1GENERIKA: TcxGridDBColumn;
    cxGridPopupMenu2: TcxGridPopupMenu;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxDBLabel2: TcxDBLabel;
    cxDBLabel3: TcxDBLabel;
    PanelVnesArtikalKolicina: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    VID_ARTIKAL: TcxDBTextEdit;
    ARTIKAL: TcxDBTextEdit;
    NAZIV_ARTIKAL: TcxDBTextEdit;
    KOLICINA: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    JN_TIP_ARTIKAL: TcxTextEdit;
    RE_NAZIV: TcxDBLookupComboBox;
    Label4: TLabel;
    RE: TcxDBTextEdit;
    PONUDENA_KOLICINA: TcxTextEdit;
    POBARANA_KOLICINA: TcxTextEdit;
    cxDBRadioGroupTIP_BARANJA: TcxDBRadioGroup;
    tblRE: TpFIBDataSet;
    dsRE: TDataSource;
    tblRERE: TFIBIntegerField;
    tblRENAZIV: TFIBStringField;
    cxHintStyleController1: TcxHintStyleController;
    PanelVnesArtikalCena: TPanel;
    cxGroupBox2: TcxGroupBox;
    Label5: TLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    VID_ARTIKAL_1: TcxDBTextEdit;
    ARTIKAL_1: TcxDBTextEdit;
    NAZIV_ARTIKAL_1: TcxDBTextEdit;
    CENA: TcxDBTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    JN_TIP_ARTIKAL_1: TcxTextEdit;
    RE_NAZIV_1: TcxDBLookupComboBox;
    RE_1: TcxDBTextEdit;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton10: TdxBarLargeButton;
    cxStyleRepository1: TcxStyleRepository;
    dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    dxGridReportLinkStyleSheet2: TdxGridReportLinkStyleSheet;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxGrid0DBTableView1IZNOS_DOGOVOR: TcxGridDBColumn;
    cxGrid0DBTableView1OSTANAT_IZNOS_DOGVOR: TcxGridDBColumn;
    PLANIRANA_CENA: TcxDBTextEdit;
    POBARANA_CENA: TcxDBTextEdit;
    cxLabel9: TcxLabel;
    IZNOS_DOGOVOR: TcxDBTextEdit;
    POBARAN_IZNOS_DOGOVOR: TcxDBTextEdit;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    KOLICINA_SEKTOR: TcxTextEdit;
    cxLabel12: TcxLabel;
    POBARANA_KOLICINA_SEKTOR: TcxTextEdit;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGrid0DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid0DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLabel12Click(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    //������� �� ���������� ������������ _sifra_kluc

  end;

var
  frmIzborStavkiBaranja: TfrmIzborStavkiBaranja;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, Baranja, dmUnit,
  dmProcedureQuey;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzborStavkiBaranja.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

procedure TfrmIzborStavkiBaranja.aBrisiIzgledExecute(Sender: TObject);
begin
  if cxGrid0.Visible = true then
      brisiGridVoBaza(Name,cxGrid0DBTableView1)
  else if cxGrid1.Visible = true then
      brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

procedure TfrmIzborStavkiBaranja.aRefreshExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
       begin
          cxGrid0DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
          cxGrid0DBTableView1.DataController.DataSet.Refresh;
       end
     else if cxGrid1.Visible = true then
       begin
          cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
          cxGrid1DBTableView1.DataController.DataSet.Refresh;
       end

end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzborStavkiBaranja.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzborStavkiBaranja.aSnimiIzgledExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        zacuvajGridVoBaza(Name,cxGrid0DBTableView1)
     else if cxGrid1.Visible = true then
        zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzborStavkiBaranja.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        zacuvajVoExcel(cxGrid0, Caption)
     else if cxGrid1.Visible = true then
        zacuvajVoExcel(cxGrid1, Caption)
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzborStavkiBaranja.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzborStavkiBaranja.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzborStavkiBaranja.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if PanelVnesArtikalCena.Visible then
       begin
         if (sender = RE_1)and (Sender = RE_NAZIV_1) then
            begin
              if dm.tblBaranjaRE.IsNull then
                 dm.tblIzborStavkiBaranjePoCena.ParamByName('Re').Value:='%'
              else
                 dm.tblIzborStavkiBaranjePoCena.ParamByName('Re').Value:=dm.tblBaranjaRE.Value;
              dm.tblIzborStavkiBaranjePoCena.FullRefresh;
              PLANIRANA_CENA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaCENA.value);
              POBARANA_CENA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaPOBARANA_CENA.value);
            end;
       end
    else  if PanelVnesArtikalKolicina.Visible then
    begin
      if (Sender = RE)and (Sender = RE_NAZIV) then
         begin
          if dm.tblBaranjaRE.IsNull then
             dm.tblIzborStavkiBaranjePoKolicina.ParamByName('Re').Value:='%'
          else
             dm.tblIzborStavkiBaranjePoKolicina.ParamByName('Re').Value:=dm.tblBaranjaRE.Value;
          dm.tblIzborStavkiBaranjePoKolicina.FullRefresh;
          PONUDENA_KOLICINA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaKOLICINA.value);
          POBARANA_KOLICINA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA.value);
         end;
    end;
end;

procedure TfrmIzborStavkiBaranja.cxGrid0DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_INSERT:
            begin
               PanelVnesArtikalCena.Visible:=true;

               tblRE.Close;
               tblRE.ParamByName('broj_tender').Value:=dm.tblBaranjaBROJ_TENDER.Value;
               tblRe.ParamByName('vid_artikal').Value:= dm.tblIzborStavkiBaranjePoCenaVID.value;
               tblRe.ParamByName('artikal').Value:= dm.tblIzborStavkiBaranjePoCenaSIFRA.value;
               if dm.tblBaranjaRE.IsNull then
                  tblRE.ParamByName('Re').Value:='%'
               else
                  tblRE.ParamByName('Re').Value:=dm.tblBaranjaRE.Value;
               tblRE.ParamByName('username').Value:=dmKon.user;
               tblRE.ParamByName('APP').Value:='JN';
               if prava_re = 0 then
                  tblRE.ParamByName('param').Value:=0
               else
                  tblRE.ParamByName('param').Value:=1;
               tblRE.Open;

               dm.tblBaranjaStavki.Insert;

               if not dm.tblBaranjaRE.IsNull then
                  dm.tblBaranjaStavkiRE.Value:=dm.tblBaranjaRE.Value;
               JN_TIP_ARTIKAL_1.Text:=intToStr(dm.tblIzborStavkiBaranjePoCenaJN_TIP_ARTIKAL.Value);
               //PONUDENA_CENA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaCENA.value);
               //POBARANA_CENA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaPOBARANA_CENA.value);
               //IZNOS_DOGOVOR.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaIZNOS_DOGOVOR.value);
               //POBARAN_IZNOS_DOGOVOR.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoCenaOSTANAT_IZNOS_DOGVOR.value);
               dm.tblBaranjaStavkiARTIKAL.value:=dm.tblIzborStavkiBaranjePoCenaSIFRA.value;
               dm.tblBaranjaStavkiVID_ARTIKAL.value:=dm.tblIzborStavkiBaranjePoCenaVID.value;
               dm.tblBaranjaStavkiARTIKALNAZIV.value:=dm.tblIzborStavkiBaranjePoCenaARTIKALNAZIV.value;
               dm.tblBaranjaStavkiBROJ.value:=dm.tblBaranjaBROJ.value;
               dm.tblBaranjaStavkiKOLICINA.Value:= 1;
               dm.tblBaranjaStavkiCENA.Value:= dm.tblIzborStavkiBaranjePoCenaDOZVOLENA_CENA.value;;
               RE_1.SetFocus;
            end;
     end;
end;

procedure TfrmIzborStavkiBaranja.cxGrid0DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid0DBTableView1);
end;


procedure TfrmIzborStavkiBaranja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (dm.tblBaranjaStavki.DataSource.DataSet.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            dm.tblBaranjaStavki.DataSource.DataSet.Cancel;
            Action := caFree;
        end
        else aZapisi.Execute();
    end;
end;

procedure TfrmIzborStavkiBaranja.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
end;

//------------------------------------------------------------------------------

procedure TfrmIzborStavkiBaranja.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    Caption:= '����� � ���� �� ������ �� ������ ��� '+ dm.tblBaranjaBROJ.Value;
    dmPQ.qTipBaranje.Close;
    dmpq.qTipBaranje.ParamByName('broj_tender').Value:=dm.tblBaranjaBROJ_TENDER.Value;
    dmPQ.qTipBaranje.ExecQuery;

    PanelVnesArtikalKolicina.top:=257;
    PanelVnesArtikalKolicina.Left:=128;

    PanelVnesArtikalCena.top:=257;
    PanelVnesArtikalCena.Left:=128;

    if dmPQ.qTipBaranje.FldByName['tip_baranja'].Value = 0 then
       begin
          cxGrid1.Visible:=true;
          cxGrid0.Visible:=false;
          cxGrid1.Align:=alClient;
          Panel1.Height:=144;
          dm.tblIzborStavkiBaranjePoKolicina.Close;
          if dm.tblBaranjaRE.IsNull then
             dm.tblIzborStavkiBaranjePoKolicina.ParamByName('re').Value:=-99999
          else
             dm.tblIzborStavkiBaranjePoKolicina.ParamByName('re').Value:=dm.tblBaranjaRE.Value;
          dm.tblIzborStavkiBaranjePoKolicina.ParamByName('broj_dogovor').Value:=dm.tblBaranjaBROJ_DOGOVOR.Value;
          dm.tblIzborStavkiBaranjePoKolicina.Open;
       end
    else if (dmPQ.qTipBaranje.FldByName['tip_baranja'].Value = 1) or (dmPQ.qTipBaranje.FldByName['tip_baranja'].Value = 2) then
       begin
          Panel1.Height:=172;
          cxGrid0.Visible:=true;
          cxGrid1.Visible:=false;
          cxGrid0.Align:=alClient;
          dm.tblIzborStavkiBaranjePoCena.Close;
          if dm.tblBaranjaRE.IsNull then
             dm.tblIzborStavkiBaranjePoCena.ParamByName('Re').Value:='%'
          else
             dm.tblIzborStavkiBaranjePoCena.ParamByName('Re').Value:=dm.tblBaranjaRE.Value;
          dm.tblIzborStavkiBaranjePoCena.Open;
       end;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid0DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid0DBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmIzborStavkiBaranja.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_INSERT:
            begin
               PanelVnesArtikalKolicina.Visible:=true;

               tblRE.Close;
               tblRE.ParamByName('broj_tender').Value:=dm.tblBaranjaBROJ_TENDER.Value;
               tblRe.ParamByName('vid_artikal').Value:= dm.tblIzborStavkiBaranjePoKolicinaVID.value;
               tblRe.ParamByName('artikal').Value:= dm.tblIzborStavkiBaranjePoKolicinaSIFRA.value;
               if dm.tblBaranjaRE.IsNull then
                  tblRE.ParamByName('Re').Value:='%'
               else
                  tblRE.ParamByName('Re').Value:=dm.tblBaranjaRE.Value;
               tblRE.ParamByName('username').Value:=dmKon.user;
               tblRE.ParamByName('APP').Value:='JN';
               if prava_re <> 0 then
                  tblRE.ParamByName('param').Value:=1
               else
                  tblRE.ParamByName('param').Value:=0;
               tblRE.Open;

               dm.tblBaranjaStavki.Insert;
               if not dm.tblBaranjaRE.IsNull then
                  dm.tblBaranjaStavkiRE.Value:=dm.tblBaranjaRE.Value;
               JN_TIP_ARTIKAL.Text:=intToStr(dm.tblIzborStavkiBaranjePoKolicinaJN_TIP_ARTIKAL.Value);
               PONUDENA_KOLICINA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaKOLICINA.value);
               POBARANA_KOLICINA.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA.value);
               KOLICINA_SEKTOR.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaKOLICINA_SEKTOR.value);
               POBARANA_KOLICINA_SEKTOR.Text:=FloatToStr(dm.tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA_SEKTOR.value);
               dm.tblBaranjaStavkiARTIKAL.value:=dm.tblIzborStavkiBaranjePoKolicinaSIFRA.value;
               dm.tblBaranjaStavkiVID_ARTIKAL.value:=dm.tblIzborStavkiBaranjePoKolicinaVID.value;
               dm.tblBaranjaStavkiARTIKALNAZIV.value:=dm.tblIzborStavkiBaranjePoKolicinaARTIKALNAZIV.value;
               dm.tblBaranjaStavkiBROJ.value:=dm.tblBaranjaBROJ.value;
               if dm.tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA_SEKTOR.value > dm.tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA.value then
                  dm.tblBaranjaStavkiKOLICINA.Value:=dm.tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA.Value - dm.tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA_SEKTOR.value
               else
                  dm.tblBaranjaStavkiKOLICINA.Value:= dm.tblIzborStavkiBaranjePoKolicinaDOZVOLENA_KOLICINA_SEKTOR.value;
               KOLICINA.SetFocus;
            end;
     end;
end;

procedure TfrmIzborStavkiBaranja.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborStavkiBaranja.cxLabel12Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmIzborStavkiBaranja.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
   if (Validacija(PanelVnesArtikalKolicina) = false) and (PanelVnesArtikalKolicina.Visible = true)then
       begin
         if dm.tblBaranjaStavkiKOLICINA.Value = 0 then
           begin
              ShowMessage('���������� � ����');
              KOLICINA.SetFocus;
           end
         else if (dm.tblIzborStavkiBaranjePoKolicinaKOLICINA_SEKTOR.value - dm.tblIzborStavkiBaranjePoKolicinaPOBARANA_KOLICINA_SEKTOR.value < dm.tblBaranjaStavkiKOLICINA.Value) then
             Begin
               ShowMessage('�������� �������� �������� �� ����������� !!!');
               KOLICINA.SetFocus;
             End
         else
           begin
              dm.tblBaranjaStavki.Post;
              dm.tblBaranjaStavki.Close;
              dm.tblBaranjaStavki.Open;
              dm.tblIzborStavkiBaranjePoKolicina.close;
              dm.tblIzborStavkiBaranjePoKolicina.Open;
              PanelVnesArtikalKolicina.Visible:=False;
              cxGrid1.SetFocus;
           end;
       end
  else if ((Validacija(PanelVnesArtikalCena) = false) and (PanelVnesArtikalCena.Visible = true)and (dmPQ.qTipBaranje.FldByName['tip_baranja'].Value = 1))then
       begin
          if dm.tblBaranjaStavkiCENA.Value <= 0 then
             begin
               ShowMessage('������ ����� �� ���� �������� �� ���� !!!');
               CENA.SetFocus;
             end
          else
        //  if ((dm.tblIzborStavkiBaranjePoCenaDOZVOLENA_CENA.value >= dm.tblBaranjaStavkiCENA.Value))then
             Begin
               if (dm.tblIzborStavkiBaranjePoCenaOSTANAT_IZNOS_DOGVOR.value >= dm.tblBaranjaStavkiCENA.Value) then
                   begin
                      dm.tblBaranjaStavki.Post;
                      dm.tblBaranjaStavki.Close;
                      dm.tblBaranjaStavki.Open;
                      dm.tblIzborStavkiBaranjePoCena.close;
                      dm.tblIzborStavkiBaranjePoCena.Open;
                      PanelVnesArtikalCena.Visible:=False;
                      cxGrid0.SetFocus;
                   end
                else
                   begin
                      ShowMessage('������� �� ��������� � ��������!!!');
                      CENA.SetFocus;
                   End
             end
          //else
            // begin
              //  ShowMessage('�������� �������� ���� �� �����������!!!');
               // CENA.SetFocus;
           //  End
       end
  else if ((Validacija(PanelVnesArtikalCena) = false) and (PanelVnesArtikalCena.Visible = true)and (dmPQ.qTipBaranje.FldByName['tip_baranja'].Value = 2))then
       begin
          if dm.tblBaranjaStavkiCENA.Value <= 0 then
             begin
               ShowMessage('������ ����� �� ���� �������� �� ���� !!!');
               CENA.SetFocus;
             end
          else if (dm.tblIzborStavkiBaranjePoCenaOSTANAT_IZNOS_DOGVOR.value >= dm.tblBaranjaStavkiCENA.Value) then
                   begin
                      dm.tblBaranjaStavki.Post;
                      dm.tblBaranjaStavki.Close;
                      dm.tblBaranjaStavki.Open;
                      dm.tblIzborStavkiBaranjePoCena.close;
                      dm.tblIzborStavkiBaranjePoCena.Open;
                      PanelVnesArtikalCena.Visible:=False;
                      cxGrid0.SetFocus;
                   end
          else
                   begin
                      ShowMessage('������� �� ��������� � ��������!!!');
                      CENA.SetFocus;
                   End
       end
end;

//	����� �� ���������� �� �������
procedure TfrmIzborStavkiBaranja.aOtkaziExecute(Sender: TObject);
begin
     if PanelVnesArtikalKolicina.Visible = true then
        begin
           dm.tblBaranjaStavki.Cancel;
           RestoreControls(PanelVnesArtikalKolicina);
           PanelVnesArtikalKolicina.Visible:=false;
           cxGrid1.SetFocus;
        end
     else if PanelVnesArtikalCena.Visible = true then
        begin
           dm.tblBaranjaStavki.Cancel;
           RestoreControls(PanelVnesArtikalCena);
           PanelVnesArtikalCena.Visible:=false;
           cxGrid0.SetFocus;
        end
     else
        begin
           ModalResult := mrCancel;
           Close();
        end
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzborStavkiBaranja.aPageSetupExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        dxComponentPrinter1Link2.PageSetup
     else if cxGrid1.Visible = true then
        dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzborStavkiBaranja.aPecatiTabelaExecute(Sender: TObject);
begin
if cxGrid0.Visible = true then
  begin
  dxComponentPrinter1Link2.ReportTitle.Text := Caption;

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
  end
else if cxGrid1.Visible = true then
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
  end
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzborStavkiBaranja.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        dxComponentPrinter1Link2.DesignReport()
     else if cxGrid1.Visible = true then
        dxComponentPrinter1Link1.DesignReport();

end;

procedure TfrmIzborStavkiBaranja.aSnimiPecatenjeExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        zacuvajPrintVoBaza(Name,cxGrid0DBTableView1.Name,dxComponentPrinter1Link2)
     else if cxGrid1.Visible = true then
        zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzborStavkiBaranja.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
     if cxGrid0.Visible = true then
        brisiPrintOdBaza(Name,cxGrid0DBTableView1.Name, dxComponentPrinter1Link2)
     else if cxGrid1.Visible = true then
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzborStavkiBaranja.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzborStavkiBaranja.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzborStavkiBaranja.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
