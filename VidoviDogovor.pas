unit VidoviDogovor;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 13.01.2012                                }
{                                                       }
{     ������ : 1.0.0.0                                 }
{                                                       }
{*******************************************************}


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMemo, cxGroupBox,
  cxRadioGroup, cxBlobEdit, dxCustomHint, cxHint, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
  TfrmVidoviDogovor = class(TfrmMaster)
    cxGrid1DBTableView1VID: TcxGridDBColumn;
    cxGrid1DBTableView1SIFRA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1VidNaziv: TcxGridDBColumn;
    rgVidDogovor: TcxDBRadioGroup;
    Opis: TcxDBMemo;
    Label2: TLabel;
    cxHintStyleController1: TcxHintStyleController;
    procedure OpisDblClick(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure rgVidDogovorPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVidoviDogovor: TfrmVidoviDogovor;

implementation

uses dmUnit, Notepad, dmProcedureQuey;



{$R *.dfm}

procedure TfrmVidoviDogovor.aAzurirajExecute(Sender: TObject);
begin
  inherited;
  Opis.SetFocus;
end;

procedure TfrmVidoviDogovor.aNovExecute(Sender: TObject);
begin
  inherited;
  rgVidDogovor.SetFocus;
end;

procedure TfrmVidoviDogovor.aZapisiExecute(Sender: TObject);
begin
   if dm.tblVidoviDogovoriVID.IsNull then
      ShowMessage('�������� ��� �� ������� !!!')
   else
      inherited;
end;

procedure TfrmVidoviDogovor.OpisDblClick(Sender: TObject);
begin
  inherited;
    frmNotepad :=TfrmNotepad.Create(Application);
    frmnotepad.LoadText(dm.tblVidoviDogovoriOPIS.Value);
    frmNotepad.ShowModal;
    if frmNotepad.ModalResult=mrOk then
     begin
       dm.tblVidoviDogovoriOPIS.Value := frmNotepad.ReturnText;
     end;
    frmNotepad.Free;
end;

procedure TfrmVidoviDogovor.rgVidDogovorPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
     begin
       dm.tblVidoviDogovoriSIFRA.Value:=dmPQ.zemiRezultat5(dmPQ.pMaxVidDogovor,'VID', Null, Null,Null,Null, dm.tblVidoviDogovoriVID.Value, Null,Null,Null,Null, 'BROJ');
       Opis.SetFocus;
     end;
end;

end.
