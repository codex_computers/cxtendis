﻿object frmDogovor: TfrmDogovor
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 682
  ClientWidth = 1151
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1151
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar10'
        end
        item
          Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
          ToolbarName = 'dxBarManager1Bar8'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 659
    Width = 1151
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080
        Width = 320
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080', Ctrl+F8 - '#1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F7 - '#1054#1089#1074#1077#1078#1080
        Width = 340
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelMaster: TPanel
    Left = 0
    Top = 126
    Width = 1151
    Height = 251
    Align = alTop
    TabOrder = 2
    object cxPageControlDogovori: TcxPageControl
      Left = 1
      Top = 1
      Width = 1149
      Height = 249
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelaren
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 249
      ClientRectRight = 1149
      ClientRectTop = 24
      object cxTabSheetTabelaren: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '
        ImageIndex = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1149
          Height = 225
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsDogovori
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1072#1090#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
            object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_TENDER'
              Visible = False
              Width = 121
            end
            object cxGrid1DBTableView1TIP: TcxGridDBColumn
              DataBinding.FieldName = 'TIP'
              Visible = False
              Width = 130
            end
            object cxGrid1DBTableView1tipNaziv: TcxGridDBColumn
              DataBinding.FieldName = 'tipNaziv'
              Width = 91
            end
            object cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_DOGOVOR'
              Width = 91
            end
            object cxGrid1DBTableView1VRSKA_DOGOVOR: TcxGridDBColumn
              DataBinding.FieldName = 'VRSKA_DOGOVOR'
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 120
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              DataBinding.FieldName = 'OPIS'
              PropertiesClassName = 'TcxRichEditProperties'
              Properties.MemoMode = True
              Properties.OEMConvert = True
              Properties.PlainText = True
              Properties.ReadOnly = True
              Width = 184
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Width = 93
            end
            object cxGrid1DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIPPARTNERNAZIV'
              Width = 211
            end
            object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER'
              Width = 107
            end
            object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNERNAZIV'
              Visible = False
              Width = 217
            end
            object cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_SKRATEN'
              Width = 217
            end
            object cxGrid1DBTableView1SMETKA: TcxGridDBColumn
              DataBinding.FieldName = 'SMETKA'
              Width = 131
            end
            object cxGrid1DBTableView1DIREKTOR: TcxGridDBColumn
              DataBinding.FieldName = 'DIREKTOR'
              Width = 124
            end
            object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
              DataBinding.FieldName = 'IZNOS'
              Width = 76
            end
            object cxGrid1DBTableView1DATUM_VAZENJE: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM_VAZENJE'
              Width = 152
            end
            object cxGrid1DBTableView1PERIOD: TcxGridDBColumn
              DataBinding.FieldName = 'PERIOD'
              Width = 195
            end
            object cxGrid1DBTableView1ROK_ISPORAKA: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_ISPORAKA'
              Width = 144
            end
            object cxGrid1DBTableView1ROK_FAKTURA: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_FAKTURA'
              Width = 149
            end
            object cxGrid1DBTableView1GARANTEN_ROK: TcxGridDBColumn
              DataBinding.FieldName = 'GARANTEN_ROK'
              Width = 100
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 124
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 124
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 124
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 124
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheetDetalen: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object PanelDetalen: TPanel
          Left = 0
          Top = 0
          Width = 1149
          Height = 225
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            1149
            225)
          object cxGroupBox1: TcxGroupBox
            Left = 447
            Top = 14
            Anchors = [akLeft, akTop, akRight]
            Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095
            TabOrder = 1
            DesignSize = (
              514
              120)
            Height = 120
            Width = 514
            object Label69: TLabel
              Left = 8
              Top = 29
              Width = 78
              Height = 18
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1055#1086#1085#1091#1076#1091#1074#1072#1095' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label3: TLabel
              Left = 16
              Top = 56
              Width = 70
              Height = 15
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1080#1088#1077#1082#1090#1086#1088' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label74: TLabel
              Left = 32
              Top = 83
              Width = 54
              Height = 15
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1057#1084#1077#1090#1082#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object TIP_PARTNER: TcxDBTextEdit
              Tag = 1
              Left = 92
              Top = 26
              Hint = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1090#1080#1087
              BeepOnEnter = False
              DataBinding.DataField = 'TIP_PARTNER'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 40
            end
            object PARTNER: TcxDBTextEdit
              Tag = 1
              Left = 131
              Top = 26
              Hint = #1055#1086#1085#1091#1076#1091#1074#1072#1095' - '#1096#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'PARTNER'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 40
            end
            object PARTNER_NAZIV: TcxLookupComboBox
              Left = 170
              Top = 26
              Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095' '#1089#1086' '#1082#1086#1112' '#1089#1077' '#1089#1082#1083#1091#1095#1091#1074#1072' '#1076#1086#1075#1086#1074#1086#1088
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'TIP_PARTNER;ID'
              Properties.ListColumns = <
                item
                  Width = 100
                  FieldName = 'TIP_PARTNER'
                end
                item
                  Width = 100
                  FieldName = 'ID'
                end
                item
                  Width = 600
                  FieldName = 'NAZIV_SKRATEN'
                end>
              Properties.ListFieldIndex = 2
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dsPartner
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 328
            end
            object DIREKTOR: TcxDBTextEdit
              Left = 92
              Top = 53
              Hint = #1044#1080#1088#1077#1082#1090#1086#1088' '#1085#1072' '#1092#1080#1088#1084#1072#1090#1072' '#1089#1086' '#1082#1086#1112#1072' '#1089#1077' '#1089#1082#1083#1091#1095#1091#1074#1072' '#1076#1086#1075#1086#1074#1086#1088
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              DataBinding.DataField = 'DIREKTOR'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Style.Shadow = False
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 406
            end
            object SMETKA: TcxDBLookupComboBox
              Left = 92
              Top = 80
              Hint = #1057#1084#1077#1090#1082#1072' '#1085#1072' '#1087#1086#1085#1091#1076#1091#1074#1072#1095#1086#1090' '#1089#1086' '#1082#1086#1112' '#1089#1077' '#1089#1082#1083#1091#1095#1091#1074#1072' '#1076#1086#1075#1086#1074#1086#1088
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'SMETKA'
              DataBinding.DataSource = dm.dsDogovori
              Properties.Alignment.Horz = taLeftJustify
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'BankaLookup'
                end
                item
                  FieldName = 'AKTIVNA'
                end>
              Properties.ListSource = dmMat.dsSmetkiP
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 406
            end
          end
          object cxGroupBox2: TcxGroupBox
            Left = 645
            Top = 84
            Anchors = [akTop, akRight]
            Caption = #1056#1086#1082#1086#1074#1080
            TabOrder = 3
            Visible = False
            DesignSize = (
              326
              120)
            Height = 120
            Width = 326
            object Label5: TLabel
              Left = 16
              Top = 29
              Width = 102
              Height = 15
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label9: TLabel
              Left = 0
              Top = 56
              Width = 118
              Height = 15
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1056#1086#1082' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label10: TLabel
              Left = 16
              Top = 83
              Width = 102
              Height = 15
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1043#1072#1088#1072#1085#1090#1077#1085' '#1088#1086#1082' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label8: TLabel
              Left = 278
              Top = 29
              Width = 30
              Height = 15
              AutoSize = False
              Caption = #1076#1077#1085#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label11: TLabel
              Left = 278
              Top = 56
              Width = 30
              Height = 15
              AutoSize = False
              Caption = #1076#1077#1085#1072
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object ROK_ISPORAKA: TcxDBTextEdit
              Left = 124
              Top = 26
              Hint = #1056#1086#1082' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1085#1072' '#1089#1090#1086#1082#1072' '#1074#1086' '#1073#1088#1086#1112' '#1085#1072' '#1076#1077#1085#1086#1074#1080
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              DataBinding.DataField = 'ROK_ISPORAKA'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 148
            end
            object ROK_FAKTURA: TcxDBTextEdit
              Left = 124
              Top = 53
              Hint = #1056#1086#1082' '#1085#1072' '#1087#1083#1072#1116#1072#1114#1077' '#1085#1072' '#1092#1072#1082#1090#1091#1088#1080' '#1074#1086' '#1076#1077#1085#1086#1074#1080
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              DataBinding.DataField = 'ROK_FAKTURA'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Style.Shadow = False
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 148
            end
            object GARANTEN_ROK: TcxDBTextEdit
              Left = 124
              Top = 80
              Hint = #1043#1072#1088#1072#1085#1090#1077#1085' '#1088#1086#1082' ('#1086#1087#1080#1089#1085#1086')'
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              DataBinding.DataField = 'GARANTEN_ROK'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Style.Shadow = False
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 185
            end
          end
          object cxGroupBox3: TcxGroupBox
            Left = 23
            Top = 14
            Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '
            TabOrder = 0
            DesignSize = (
              418
              195)
            Height = 195
            Width = 418
            object Label2: TLabel
              Left = 85
              Top = 29
              Width = 50
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 68
              Top = 56
              Width = 67
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label7: TLabel
              Left = 17
              Top = 101
              Width = 118
              Height = 27
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label1: TLabel
              Left = 17
              Top = 83
              Width = 118
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label6: TLabel
              Left = 97
              Top = 137
              Width = 38
              Height = 16
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1054#1087#1080#1089' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object BROJ_DOGOVOR: TcxDBTextEdit
              Tag = 1
              Left = 141
              Top = 26
              Hint = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
              BeepOnEnter = False
              DataBinding.DataField = 'BROJ_DOGOVOR'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 124
            end
            object DATUM: TcxDBDateEdit
              Tag = 1
              Left = 141
              Top = 53
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dm.dsDogovori
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 124
            end
            object PERIOD: TcxDBTextEdit
              Left = 141
              Top = 107
              Hint = #1042#1088#1077#1084#1077#1090#1088#1072#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' - '#1086#1087#1080#1089#1085#1086
              Anchors = [akLeft, akTop, akRight]
              BeepOnEnter = False
              DataBinding.DataField = 'PERIOD'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Style.Shadow = False
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 261
            end
            object DATUM_VAZENJE: TcxDBDateEdit
              Left = 141
              Top = 80
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1074#1072#1078#1077#1114#1077' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090
              DataBinding.DataField = 'DATUM_VAZENJE'
              DataBinding.DataSource = dm.dsDogovori
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 124
            end
            object OPIS: TcxDBRichEdit
              Left = 141
              Top = 134
              Hint = 
                #1044#1077#1090#1072#1083#1077#1085' '#1086#1087#1080#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088#1086#1090' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072 +
                ' '#1076#1072' '#1075#1086' '#1091#1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'OPIS'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Properties.WantReturns = False
              Style.Font.Charset = RUSSIAN_CHARSET
              Style.Font.Color = clBlack
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.IsFontAssigned = True
              TabOrder = 4
              OnDblClick = OPISDblClick
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Height = 44
              Width = 261
            end
          end
          object ZapisiButton: TcxButton
            Left = 977
            Top = 173
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 4
          end
          object OtkaziButton: TcxButton
            Left = 1058
            Top = 173
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 5
          end
          object cxGroupBoxIznosDogovor: TcxGroupBox
            Left = 447
            Top = 148
            TabOrder = 2
            Height = 60
            Width = 209
            object Label13: TLabel
              Left = 8
              Top = 19
              Width = 78
              Height = 28
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object IZNOS: TcxDBTextEdit
              Tag = 1
              Left = 92
              Top = 24
              Hint = #1048#1079#1085#1086#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
              BeepOnEnter = False
              DataBinding.DataField = 'IZNOS'
              DataBinding.DataSource = dm.dsDogovori
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 101
            end
          end
        end
      end
    end
  end
  object PanelDetail: TPanel
    Left = 0
    Top = 382
    Width = 1151
    Height = 277
    Align = alClient
    TabOrder = 3
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1149
      Height = 35
      Align = alTop
      Color = clCream
      ParentBackground = False
      TabOrder = 0
      object Label12: TLabel
        Left = 14
        Top = 10
        Width = 275
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1073#1088#1086#1112' :'
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object cxDBLabel1: TcxDBLabel
        Left = 295
        Top = 9
        DataBinding.DataField = 'BROJ_DOGOVOR'
        DataBinding.DataSource = dm.dsDogovori
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
        Height = 19
        Width = 186
      end
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 36
      Width = 1149
      Height = 240
      Align = alClient
      TabOrder = 1
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid2DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsDogovorStavki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.00, .'
            Kind = skSum
            Column = cxGrid2DBTableView1IZNOS
          end
          item
            Format = '0.00, .'
            Kind = skSum
            Column = cxGrid2DBTableView1IZNOSBEZDDV
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080' '
        OptionsView.Footer = True
        object cxGrid2DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_DOGOVOR'
          Visible = False
          Width = 93
        end
        object cxGrid2DBTableView1RED_BR: TcxGridDBColumn
          DataBinding.FieldName = 'RED_BR'
          Width = 67
        end
        object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'JN_TIP_ARTIKAL'
          Width = 37
        end
        object cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDNAZIV'
          Width = 71
        end
        object cxGrid2DBTableView1GENERIKA: TcxGridDBColumn
          DataBinding.FieldName = 'GENERIKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 1
            end>
          Width = 74
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Width = 52
        end
        object cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKALNAZIV'
          Width = 226
        end
        object cxGrid2DBTableView1STAVKA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'STAVKA_NAZIV'
          PropertiesClassName = 'TcxRichEditProperties'
          Width = 226
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Width = 68
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Width = 47
        end
        object cxGrid2DBTableView1STAVKA_MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'STAVKA_MERKA'
          Width = 81
        end
        object cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Visible = False
          Width = 94
        end
        object cxGrid2DBTableView1CENABEZDDV: TcxGridDBColumn
          DataBinding.FieldName = 'CENABEZDDV'
          Width = 82
        end
        object cxGrid2DBTableView1DANOK: TcxGridDBColumn
          DataBinding.FieldName = 'DANOK'
          Width = 38
        end
        object cxGrid2DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
          Width = 76
        end
        object cxGrid2DBTableView1IZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOSBEZDDV'
          Width = 85
        end
        object cxGrid2DBTableView1IZNOSBEZDDV: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS'
          Width = 79
        end
        object cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'PROZVODITEL'
          Width = 124
        end
        object cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PROIZVODITELNAZIV'
          Width = 210
        end
        object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Width = 50
        end
        object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Width = 167
        end
        object cxGrid2DBTableView1JN_CPV: TcxGridDBColumn
          DataBinding.FieldName = 'JN_CPV'
          Width = 118
        end
        object cxGrid2DBTableView1MK: TcxGridDBColumn
          DataBinding.FieldName = 'MK'
          Width = 103
        end
        object cxGrid2DBTableView1EN: TcxGridDBColumn
          DataBinding.FieldName = 'EN'
          Width = 94
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          Width = 75
        end
        object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 158
        end
        object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 213
        end
        object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 197
        end
        object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 243
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 377
    Width = 1151
    Height = 5
    AlignSplitter = salTop
    MinSize = 260
    Control = PanelMaster
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 376
    Top = 512
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 552
    Top = 552
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 788
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1070
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 401
      DockedTop = 0
      FloatLeft = 1126
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 281
      DockedTop = 0
      FloatLeft = 1126
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 572
      DockedTop = 0
      FloatLeft = 1126
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 935
      DockedTop = 0
      FloatLeft = 1126
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = '  '
      CaptionButtons = <>
      DockedLeft = 604
      DockedTop = 0
      FloatLeft = 1311
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton33'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar10: TdxBar
      CaptionButtons = <>
      DockedLeft = 148
      DockedTop = 0
      FloatLeft = 1311
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton32'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton29'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavki
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavki
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBaranjaIsporaka
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPageSetupStavki
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aSnimiPecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aSnimiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aBrisiIzgledStvaki
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aZacuvajExcelStavki
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aRefreshStavki
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aPecatiTabelaStavki
      Category = 0
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aDokumentiPregled
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Action = aAneksNaDogovor
      Category = 0
    end
    object dxBarLargeButton32: TdxBarLargeButton
      Action = aDogovorNaAneks
      Category = 0
    end
    object dxBarLargeButton33: TdxBarLargeButton
      Action = actPregledajDogovor
      Category = 0
      DropDownEnabled = False
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aDodadiStavki: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aDodadiStavkiExecute
    end
    object aBrisiStavki: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
      ShortCut = 16503
      OnExecute = aBrisiStavkiExecute
    end
    object aBaranjaIsporaka: TAction
      Caption = #1041#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 16
      OnExecute = aBaranjaIsporakaExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1076#1086#1075#1086#1074#1086#1088#1080' '
      ImageIndex = 9
      OnExecute = aZacuvajExcelExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aPecatiTabelaStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaStavkiExecute
    end
    object aRefreshStavki: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 52
      ShortCut = 16502
      OnExecute = aRefreshStavkiExecute
    end
    object aZacuvajExcelStavki: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1086#1075#1086#1074#1086#1088' '
      ImageIndex = 9
      OnExecute = aZacuvajExcelStavkiExecute
    end
    object aPodesuvanjePecatenjeStavki: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeStavkiExecute
    end
    object aPageSetupStavki: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupStavkiExecute
    end
    object aSnimiPecatenjeStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeStavkiExecute
    end
    object aBrisiPodesuvanjePecatenjeStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeStavkiExecute
    end
    object aSnimiIzgledStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledStavkiExecute
    end
    object aBrisiIzgledStvaki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledStvakiExecute
    end
    object aPDogovorVrabotuvanjeTerk: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1079#1072' '#1074#1088#1072#1073#1086#1090#1091#1074#1072#1114#1077
      ImageIndex = 19
      OnExecute = aPDogovorVrabotuvanjeTerkExecute
    end
    object aDokumentiPregled: TAction
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1080
      ImageIndex = 60
      OnExecute = aDokumentiPregledExecute
    end
    object aAneksNaDogovor: TAction
      Caption = #1040#1085#1077#1082#1089' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088
      ImageIndex = 62
      OnExecute = aAneksNaDogovorExecute
    end
    object aDogovorNaAneks: TAction
      Caption = #1044#1086#1075#1086#1074#1086#1088' '#1085#1072' '#1072#1085#1077#1082#1089
      ImageIndex = 61
      OnExecute = aDogovorNaAneksExecute
    end
    object actPregledajDogovor: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080' '#1076#1086#1075#1086#1074#1086#1088
      ImageIndex = 19
      OnExecute = actPregledajDogovorExecute
    end
    object actDizajn: TAction
      Caption = 'actDizajn'
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = actDizajnExecute
    end
    object actDizajnDogovori: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1088#1077#1087#1086#1088#1090' '#1076#1086#1075#1086#1074#1088' '#1079#1072' '#1057#1090#1086#1082#1080'/'#1059#1089#1083#1091#1075#1080'/'#1056#1072#1073#1086#1090#1080
      OnExecute = actDizajnDogovoriExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43683.537705428240000000
      ShrinkToPageWidth = True
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 592
    Top = 392
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblPartner: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAT_PARTNER'
      'SET '
      '    NAZIV = :NAZIV,'
      '    NAZIV_SKRATEN = :NAZIV_SKRATEN,'
      '    ADRESA = :ADRESA,'
      '    TEL = :TEL,'
      '    FAX = :FAX,'
      '    DANOCEN = :DANOCEN,'
      '    MESTO = :MESTO,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    TATKO = :TATKO,'
      '    LOGO = :LOGO,'
      '    RE = :RE,'
      '    LOGOTEXT = :LOGOTEXT,'
      '    STATUS = :STATUS,'
      '    MAIL = :MAIL'
      'WHERE'
      '    TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAT_PARTNER'
      'WHERE'
      '        TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAT_PARTNER('
      '    TIP_PARTNER,'
      '    ID,'
      '    NAZIV,'
      '    NAZIV_SKRATEN,'
      '    ADRESA,'
      '    TEL,'
      '    FAX,'
      '    DANOCEN,'
      '    MESTO,'
      '    IME,'
      '    PREZIME,'
      '    TATKO,'
      '    LOGO,'
      '    RE,'
      '    LOGOTEXT,'
      '    STATUS,'
      '    MAIL'
      ')'
      'VALUES('
      '    :TIP_PARTNER,'
      '    :ID,'
      '    :NAZIV,'
      '    :NAZIV_SKRATEN,'
      '    :ADRESA,'
      '    :TEL,'
      '    :FAX,'
      '    :DANOCEN,'
      '    :MESTO,'
      '    :IME,'
      '    :PREZIME,'
      '    :TATKO,'
      '    :LOGO,'
      '    :RE,'
      '    :LOGOTEXT,'
      '    :STATUS,'
      '    :MAIL'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    mp.TIP_PARTNER,'
      '    mtp.naziv as TIP_NAZIV,'
      '    mp.ID,'
      '    mp.NAZIV,'
      '    mp.naziv_skraten,'
      '    mp.ADRESA,'
      '    mp.TEL,'
      '    mp.FAX,'
      '    mp.DANOCEN,'
      '    mp.MESTO,'
      '    mm.naziv as MESTO_NAZIV,'
      '    mp.IME,'
      '    mp.PREZIME,'
      '    mp.TATKO,'
      '    mp.LOGO,'
      '    mp.RE,'
      '    mp.LOGOTEXT,'
      '    mp.STATUS,'
      '    mp.mail'
      'from MAT_PARTNER mp'
      'left join MAT_TIP_PARTNER mtp on mtp.id = mp.tip_partner'
      'left join MAT_MESTO mm on mm.id = mp.mesto'
      ''
      ' WHERE '
      '        MP.TIP_PARTNER = :OLD_TIP_PARTNER'
      '    and MP.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select distinct'
      '       d.tip_partner,'
      '       tp.naziv as tip_naziv,'
      '       d.partner as id,'
      '       mp.naziv,'
      '       mp.naziv_skraten,'
      '       mp.re, '
      '       mp.status'
      'from jn_tender_dobitnici d'
      
        'inner join mat_partner mp on mp.tip_partner = d.tip_partner and ' +
        'mp.id = d.partner'
      'inner join mat_tip_partner tp on tp.id = mp.tip_partner'
      'where d.broj_tender = :mas_broj'
      ''
      'order by mp.tip_partner, mp.naziv')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dm.dsТender
    Left = 552
    Top = 352
    poSQLINT64ToBCD = True
    oTrimCharFields = False
    oRefreshAfterPost = False
    object tblPartnerTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1080#1087
      FieldName = 'TIP_PARTNER'
    end
    object tblPartnerTIP_NAZIV: TFIBStringField
      DisplayLabel = #1058#1055
      FieldName = 'TIP_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPartnerNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerNAZIV_SKRATEN: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1089#1082#1088#1072#1090#1077#1085
      FieldName = 'NAZIV_SKRATEN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPartnerRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblPartnerSTATUS: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
  end
  object dsPartner: TDataSource
    DataSet = tblPartner
    Left = 704
    Top = 360
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 752
    Top = 368
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'PrintStyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 472
    Top = 416
  end
  object pm1: TPopupMenu
    Left = 336
    Top = 304
    object N2: TMenuItem
      Action = actDizajnDogovori
    end
    object TMenuItem
    end
  end
end
