object frmPostapkiPredIstekNaDogovor: TfrmPostapkiPredIstekNaDogovor
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1032#1072#1074#1085#1080' '#1085#1072#1073#1072#1074#1082#1080
  ClientHeight = 439
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 329
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 289
      Height = 35
      Caption = 
        #1044#1086#1075#1086#1074#1086#1088#1080#1090#1077' '#1079#1072' '#1089#1083#1077#1076#1085#1080#1090#1077' '#1087#1086#1089#1090#1072#1087#1082#1080' '#1079#1072' '#1032#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' '#1080#1089#1090#1077#1082#1091#1074#1072#1072#1090' '#1079#1072' 1' +
        ' '#1084#1077#1089#1077#1094
      WordWrap = True
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 57
    Width = 329
    Height = 322
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dm.dstblProcTenderZatvoriPred1Mesec
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1BROJ_TENDER_O: TcxGridDBColumn
        DataBinding.FieldName = 'BROJ_TENDER_O'
        Width = 323
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 379
    Width = 329
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      329
      41)
    object cxButton1: TcxButton
      Left = 104
      Top = 10
      Width = 100
      Height = 25
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = 'Ok'
      OptionsImage.ImageIndex = 6
      OptionsImage.Images = dmRes.cxSmallImages
      TabOrder = 0
      OnClick = cxButton1Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 420
    Width = 329
    Height = 19
    Panels = <
      item
        Text = ' Ctrl+E - '#1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
        Width = 50
      end>
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 544
    Top = 144
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 96
    Top = 136
  end
  object ActionList1: TActionList
    Left = 248
    Top = 192
    object ZacuvajExcel: TAction
      Caption = 'ZacuvajExcel'
      ShortCut = 16453
      OnExecute = ZacuvajExcelExecute
    end
  end
end
