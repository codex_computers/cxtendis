object frmBaranja: TfrmBaranja
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 682
  ClientWidth = 1194
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1194
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 659
    Width = 1194
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080
        Width = 320
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Insert - '#1044#1086#1076#1072#1076#1080' '#1089#1090#1072#1074#1082#1080', Ctrl+F8 - '#1041#1088#1080#1096#1080' '#1089#1090#1072#1074#1082#1072', Ctrl+F7 - '#1054#1089#1074#1077#1078#1080
        Width = 340
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelDetail: TPanel
    Left = 0
    Top = 423
    Width = 1194
    Height = 236
    Align = alClient
    TabOrder = 2
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1192
      Height = 35
      Align = alTop
      Color = clCream
      ParentBackground = False
      TabOrder = 0
      object Label12: TLabel
        Left = -26
        Top = 10
        Width = 275
        Height = 19
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '#1073#1088#1086#1112' :'
        Color = clBtnFace
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = 10901821
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object cxDBLabel1: TcxDBLabel
        Left = 252
        Top = 9
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsBaranja
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsBold]
        Style.IsFontAssigned = True
        Transparent = True
        Height = 19
        Width = 186
      end
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 36
      Width = 1192
      Height = 199
      Align = alClient
      TabOrder = 1
      object cxGrid2DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid2DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsBaranjaStavki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1089#1090#1072#1074#1082#1080
        object cxGrid2DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Visible = False
          Options.Editing = False
          Width = 154
        end
        object cxGrid2DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Options.Editing = False
          Width = 140
        end
        object cxGrid2DBTableView1RENAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RENAZIV'
          Options.Editing = False
          Width = 224
        end
        object cxGrid2DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'JN_TIP_ARTIKAL'
          Options.Editing = False
          Width = 34
        end
        object cxGrid2DBTableView1VIDNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VIDNAZIV'
          Options.Editing = False
          Width = 68
        end
        object cxGrid2DBTableView1GENERIKA: TcxGridDBColumn
          DataBinding.FieldName = 'GENERIKA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              ImageIndex = 0
              Value = 1
            end>
          Options.Editing = False
          Width = 72
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Options.Editing = False
          Width = 61
        end
        object cxGrid2DBTableView1ARTIKALNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKALNAZIV'
          Options.Editing = False
          Width = 201
        end
        object cxGrid2DBTableView1STAVKANAZIV: TcxGridDBColumn
          Caption = #1057#1090#1072#1074#1082#1072' '#1085#1072#1079#1080#1074
          DataBinding.FieldName = 'STAVKANAZIV'
          PropertiesClassName = 'TcxRichEditProperties'
          Width = 300
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Options.Editing = False
          Width = 41
        end
        object cxGrid2DBTableView1MERKANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MERKANAZIV'
          Options.Editing = False
          Width = 82
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Styles.Content = dmRes.cxStyle120
          Width = 63
        end
        object cxGrid2DBTableView1CENA: TcxGridDBColumn
          DataBinding.FieldName = 'CENA'
        end
        object cxGrid2DBTableView1PROZVODITEL: TcxGridDBColumn
          DataBinding.FieldName = 'PROZVODITEL'
          Options.Editing = False
          Width = 125
        end
        object cxGrid2DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PROIZVODITELNAZIV'
          Options.Editing = False
          Width = 248
        end
        object cxGrid2DBTableView1GRUPA: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPA'
          Options.Editing = False
          Width = 51
        end
        object cxGrid2DBTableView1GRUPANAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'GRUPANAZIV'
          Options.Editing = False
          Width = 144
        end
        object cxGrid2DBTableView1JN_CPV: TcxGridDBColumn
          DataBinding.FieldName = 'JN_CPV'
          Options.Editing = False
          Width = 112
        end
        object cxGrid2DBTableView1MK: TcxGridDBColumn
          DataBinding.FieldName = 'MK'
          Options.Editing = False
          Width = 101
        end
        object cxGrid2DBTableView1EN: TcxGridDBColumn
          DataBinding.FieldName = 'EN'
          Options.Editing = False
          Width = 86
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          Options.Editing = False
          Width = 73
        end
        object cxGrid2DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 165
        end
        object cxGrid2DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 215
        end
        object cxGrid2DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 240
        end
        object cxGrid2DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 234
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object PanelMaster: TPanel
    Left = 0
    Top = 126
    Width = 1194
    Height = 289
    Align = alTop
    TabOrder = 3
    object cxPageControlBaranja: TcxPageControl
      Left = 1
      Top = 1
      Width = 1192
      Height = 287
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheetTabelaren
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 287
      ClientRectRight = 1192
      ClientRectTop = 24
      object cxTabSheetTabelaren: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085' '#1087#1088#1080#1082#1072#1079' '
        ImageIndex = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1192
          Height = 263
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsBaranja
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1073#1072#1088#1072#1114#1072
            object cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_TENDER'
              Visible = False
              Width = 120
            end
            object cxGrid1DBTableView1BROJ_DOGOVOR: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ_DOGOVOR'
              Visible = False
              Width = 92
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 86
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              DataBinding.FieldName = 'DATUM'
              Width = 83
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_PARTNER'
              Width = 128
            end
            object cxGrid1DBTableView1TIPPARTNERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'TIPPARTNERNAZIV'
              Width = 171
            end
            object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNER'
              Width = 108
            end
            object cxGrid1DBTableView1PARTNERNAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'PARTNERNAZIV'
              Visible = False
              Width = 186
            end
            object cxGrid1DBTableView1NAZIV_SKRATEN: TcxGridDBColumn
              DataBinding.FieldName = 'NAZIV_SKRATEN'
              Width = 214
            end
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              DataBinding.FieldName = 'RE'
              Width = 139
            end
            object cxGrid1DBTableView1RENAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'RENAZIV'
              Width = 238
            end
            object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
              DataBinding.FieldName = 'ZABELESKA'
              Width = 203
            end
            object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
              DataBinding.FieldName = 'TS_INS'
              Visible = False
              Width = 161
            end
            object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'TS_UPD'
              Visible = False
              Width = 217
            end
            object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
              DataBinding.FieldName = 'USR_INS'
              Visible = False
              Width = 203
            end
            object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
              DataBinding.FieldName = 'USR_UPD'
              Visible = False
              Width = 238
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxTabSheetDetalen: TcxTabSheet
        Caption = #1044#1077#1090#1072#1083#1077#1085' '#1087#1088#1080#1082#1072#1079
        ImageIndex = 1
        object PanelDetalen: TPanel
          Left = 0
          Top = 0
          Width = 1192
          Height = 263
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            1192
            263)
          object cxGroupBox3: TcxGroupBox
            Left = 15
            Top = 11
            Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
            TabOrder = 0
            DesignSize = (
              986
              230)
            Height = 230
            Width = 986
            object Label2: TLabel
              Left = 25
              Top = 83
              Width = 110
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1114#1077' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 68
              Top = 110
              Width = 67
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1044#1072#1090#1091#1084' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label6: TLabel
              Left = 57
              Top = 164
              Width = 78
              Height = 16
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label1: TLabel
              Left = 25
              Top = 137
              Width = 110
              Height = 18
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
              Color = clBtnFace
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              WordWrap = True
            end
            object Label3: TLabel
              Left = 57
              Top = 22
              Width = 78
              Height = 28
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1112#1072#1074#1085#1072' '#1085#1072#1073#1072#1074#1082#1072' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              WordWrap = True
            end
            object Label5: TLabel
              Left = 25
              Top = 56
              Width = 110
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1076#1086#1075#1086#1074#1086#1088' :'
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object BROJ: TcxDBTextEdit
              Tag = 1
              Left = 141
              Top = 80
              Hint = #1041#1088#1086#1112' '#1085#1072' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              BeepOnEnter = False
              DataBinding.DataField = 'BROJ'
              DataBinding.DataSource = dm.dsBaranja
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 145
            end
            object DATUM: TcxDBDateEdit
              Tag = 1
              Left = 141
              Top = 107
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dm.dsBaranja
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 145
            end
            object RE_Naziv: TcxDBLookupComboBox
              Tag = 1
              Left = 200
              Top = 134
              Hint = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1079#1072' '#1082#1086#1112#1072' '#1089#1077' '#1087#1088#1072#1074#1080' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataBinding.DataField = 'RE'
              DataBinding.DataSource = dm.dsBaranja
              Properties.DropDownListStyle = lsFixedList
              Properties.KeyFieldNames = 'RE'
              Properties.ListColumns = <
                item
                  Width = 200
                  FieldName = 'RE'
                end
                item
                  Width = 800
                  FieldName = 'NAZIV'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsReBaranja
              TabOrder = 5
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 770
            end
            object ZABELESKA: TcxDBMemo
              Left = 141
              Top = 161
              Hint = 
                #1047#1072#1073#1077#1083#1077#1096#1082#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1077#1090#1086'  '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' *'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1075#1086' '#1091 +
                #1088#1077#1076#1080#1090#1077' '#1090#1077#1082#1089#1090#1086#1090
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataBinding.DataField = 'ZABELESKA'
              DataBinding.DataSource = dm.dsBaranja
              Properties.WantReturns = False
              TabOrder = 6
              OnDblClick = ZABELESKADblClick
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Height = 54
              Width = 829
            end
            object RE: TcxDBTextEdit
              Tag = 1
              Left = 141
              Top = 134
              Hint = #1064#1080#1092#1088#1072
              BeepOnEnter = False
              DataBinding.DataField = 'RE'
              DataBinding.DataSource = dm.dsBaranja
              ParentFont = False
              Properties.BeepOnError = True
              Properties.CharCase = ecUpperCase
              Style.Shadow = False
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 60
            end
            object BROJ_TENDER: TcxDBLookupComboBox
              Tag = 1
              Left = 141
              Top = 26
              DataBinding.DataField = 'BROJ_TENDER'
              DataBinding.DataSource = dm.dsBaranja
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'BROJ'
              Properties.ListColumns = <
                item
                  FieldName = 'BROJ'
                end>
              Properties.ListSource = dm.dsIzborBrojTenderZaBaranje
              Properties.OnEditValueChanged = BROJ_TENDERPropertiesEditValueChanged
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 145
            end
            object BROJ_DOGOVOR: TcxDBLookupComboBox
              Tag = 1
              Left = 141
              Top = 53
              DataBinding.DataField = 'BROJ_DOGOVOR'
              DataBinding.DataSource = dm.dsBaranja
              Properties.KeyFieldNames = 'BROJ_DOGOVOR'
              Properties.ListColumns = <
                item
                  FieldName = 'BROJ_DOGOVOR'
                end>
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dm.dsIzborBrojDogovorZaBaranje
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 145
            end
          end
          object ZapisiButton: TcxButton
            Left = 1020
            Top = 205
            Width = 75
            Height = 25
            Action = aZapisi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 1
          end
          object OtkaziButton: TcxButton
            Left = 1101
            Top = 205
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akTop, akRight]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 2
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 415
    Width = 1194
    Height = 8
    AlignSplitter = salTop
    MinSize = 250
    Control = PanelMaster
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 440
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 544
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 536
    Top = 544
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 620
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 984
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 234
      DockedTop = 0
      FloatLeft = 1228
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 1228
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      CaptionButtons = <>
      DockedLeft = 929
      DockedTop = 0
      FloatLeft = 1228
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1055#1088#1077#1075#1083#1077#1076#1080
      CaptionButtons = <>
      DockedLeft = 413
      DockedTop = 0
      FloatLeft = 1228
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton30'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton31'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072' '
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aDodadiStavka
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiStavka
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPageSetupStavki
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aSnimiPecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenjeStavki
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aSnimiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aBrisiIzgledStavki
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aZacuvajExcelStavki
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aPecatiStavki
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aRefresh
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aRefreshStavki
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Action = aPBaranjeZaIsporaka
      Category = 0
    end
    object dxBarLargeButton31: TdxBarLargeButton
      Action = aPBaranjaZaArtikal
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 9
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1073#1072#1088#1072#1114#1072' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aDodadiStavka: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      ShortCut = 45
      OnExecute = aDodadiStavkaExecute
    end
    object aPodesuvanjePecatenjeStavki: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeStavkiExecute
    end
    object aPageSetupStavki: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupStavkiExecute
    end
    object aSnimiPecatenjeStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeStavkiExecute
    end
    object aBrisiPodesuvanjePecatenjeStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeStavkiExecute
    end
    object aSnimiIzgledStavki: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledStavkiExecute
    end
    object aBrisiIzgledStavki: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledStavkiExecute
    end
    object aZacuvajExcelStavki: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077
      ImageIndex = 9
      OnExecute = aZacuvajExcelStavkiExecute
    end
    object aPecatiStavki: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072' '#1079#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1073#1072#1088#1072#1114#1077
      ImageIndex = 30
      OnExecute = aPecatiStavkiExecute
    end
    object aBrisiStavka: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
      ShortCut = 16503
      OnExecute = aBrisiStavkaExecute
    end
    object aRefreshStavki: TAction
      Caption = #1054#1089#1074#1077#1078#1080
      ImageIndex = 52
      ShortCut = 16502
      OnExecute = aRefreshStavkiExecute
    end
    object aPBaranjeZaIsporaka: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 19
      OnExecute = aPBaranjeZaIsporakaExecute
    end
    object aDBaranjeZaIsporaka: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
      ImageIndex = 19
      OnExecute = aDBaranjeZaIsporakaExecute
    end
    object aPopUpMenu: TAction
      Caption = 'aPopUpMenu'
      ShortCut = 16505
      OnExecute = aPopUpMenuExecute
    end
    object aPBaranjaZaArtikal: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1072#1088#1090#1080#1082#1072#1083
      ImageIndex = 19
      OnExecute = aPBaranjaZaArtikalExecute
    end
    object aDBaranjaZaArtikal: TAction
      Caption = #1041#1072#1088#1072#1114#1077' '#1079#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085' '#1072#1088#1090#1080#1082#1072#1083
      OnExecute = aDBaranjaZaArtikalExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link2
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43593.345947905090000000
      ShrinkToPageWidth = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.StyleSheet = dxGridReportLinkStyleSheet1
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 440
    Top = 528
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 552
    Top = 216
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnShadow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
    end
    object dxGridReportLinkStyleSheet1: TdxGridReportLinkStyleSheet
      Caption = 'PrintStyleSheet'
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.CardCaptionRow = cxStyle3
      Styles.CardRowCaption = cxStyle4
      Styles.Content = cxStyle5
      Styles.ContentEven = cxStyle6
      Styles.ContentOdd = cxStyle7
      Styles.FilterBar = cxStyle8
      Styles.Footer = cxStyle9
      Styles.Group = cxStyle10
      Styles.Header = cxStyle11
      Styles.Preview = cxStyle12
      Styles.Selection = cxStyle13
      BuiltIn = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 464
    Top = 440
  end
  object cxHintStyleController1: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CallOutPosition = cxbpTopLeft
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Tahoma'
    HintStyle.Font.Style = []
    HintStyle.Rounded = True
    HintStyle.RoundRadius = 10
    HintHidePause = 8500
    Left = 1048
    Top = 136
  end
  object PopupMenu2: TPopupMenu
    Left = 464
    Top = 216
    object N2: TMenuItem
      Action = aDBaranjeZaIsporaka
    end
    object N3: TMenuItem
      Action = aDBaranjaZaArtikal
    end
  end
  object qAktivenDogovor: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select case when (d.datum_vazenje is not null)and(d.datum_vazenj' +
        'e >= current_date) then 1'
      '            when (d.datum_vazenje is null) then 1'
      '            else 0'
      '       end "aktiven"'
      'from jn_dogovor  d'
      'where d.broj_dogovor = :broj_dogovor')
    Left = 720
    Top = 224
  end
end
