unit IzborStavkiPonuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxLabel, cxDBLabel, cxRichEdit, cxDBRichEdit, dxCustomHint, cxHint,
  cxImageComboBox, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmIzborStavkiPonuda = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1BROJ_TENDER: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1JN_TIP_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1VIDNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PROZVODITEL: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODITELNAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPA: TcxGridDBColumn;
    cxGrid1DBTableView1GRUPANAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1GENERIKA: TcxGridDBColumn;
    cxGrid1DBTableView1JN_CPV: TcxGridDBColumn;
    cxGrid1DBTableView1MK: TcxGridDBColumn;
    cxGrid1DBTableView1EN: TcxGridDBColumn;
    Panel2: TPanel;
    Label8: TLabel;
    Label7: TLabel;
    cxDBLabel1: TcxDBLabel;
    cxDBLabel2: TcxDBLabel;
    PanelVnesArtikal: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel5: TcxLabel;
    Label6: TLabel;
    VID_ARTIKAL: TcxDBTextEdit;
    ARTIKAL: TcxDBTextEdit;
    NAZIV_ARTIKAL: TcxDBTextEdit;
    KOLICINA: TcxDBTextEdit;
    CENA_BEZ_DDV: TcxDBTextEdit;
    DDV: TcxDBTextEdit;
    CENA: TcxDBTextEdit;
    OPIS: TcxDBRichEdit;
    JN_TIP_ARTIKAL: TcxTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cxHintStyleController1: TcxHintStyleController;
    cxGrid1DBTableView1TARIFA: TcxGridDBColumn;
    cxCheckBoxKreirajArtikal: TcxCheckBox;
    cxCheckBoxOpstNaziv: TcxCheckBox;
    dxBarLargeButton10: TdxBarLargeButton;
    MERKA: TcxDBTextEdit;
    Label1: TLabel;
    cxDBLabel3: TcxDBLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    IZNOSCENBEZDDV: TcxDBTextEdit;
    IZNOSCENA: TcxDBTextEdit;
    cxGrid1DBTableView1VALIDNOST: TcxGridDBColumn;
    cxGrid1DBTableView1STAVKA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STAVKA_MERKA: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OPISDblClick(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmIzborStavkiPonuda: TfrmIzborStavkiPonuda;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, Main, dmUnit, Notepad,
  dmProcedureQuey, dmMaticni;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzborStavkiPonuda.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIzborStavkiPonuda.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIzborStavkiPonuda.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmIzborStavkiPonuda.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmIzborStavkiPonuda.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmIzborStavkiPonuda.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzborStavkiPonuda.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzborStavkiPonuda.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzborStavkiPonuda.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzborStavkiPonuda.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzborStavkiPonuda.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzborStavkiPonuda.cxDBTextEditAllExit(Sender: TObject);
begin
 TEdit(Sender).Color:=clWhite;
// if dm.tblPonudiStavki.State in [dsInsert] then
//   begin
//    if ((Sender = CENA_BEZ_DDV) and (CENA_BEZ_DDV.Text <> '') and (DDV.Text <> '')) then
//       begin
//          dm.tblPonudiStavkiCENA.Value:= dm.tblPonudiStavkiCENABEZDDV.Value + (dm.tblPonudiStavkiDANOK.Value * dm.tblPonudiStavkiCENABEZDDV.Value/100);;
//       end
//    else  if (Sender = DDV) and (CENA_BEZ_DDV.Text <> '') and (DDV.Text <> '') then
//       begin
//          dm.tblPonudiStavkiCENA.Value:= dm.tblPonudiStavkiCENABEZDDV.Value + (dm.tblPonudiStavkiDANOK.Value * dm.tblPonudiStavkiCENABEZDDV.Value/100);;
//       end
//    else if (Sender = CENA) and (CENA.Text <> '') and (DDV.Text <> '') then
//       begin
//          dm.tblPonudiStavkiCENABEZDDV.Value:=dm.tblPonudiStavkiCENA.Value*100/(100 + dm.tblPonudiStavkiDANOK.Value);
//       end;
//   end;
end;

procedure TfrmIzborStavkiPonuda.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmIzborStavkiPonuda.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmIzborStavkiPonuda.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmIzborStavkiPonuda.OPISDblClick(Sender: TObject);
begin
     frmNotepad := TfrmNotepad.Create(Application);
     frmNotepad.LoadStream(dm.tblPonudiStavki.CreateBlobStream(dm.tblPonudiStavkiOPIS , bmRead));
     frmNotepad.ShowModal;
     if frmNotepad.ModalResult=mrOk then
        begin
            //ReturnStream(false); --> rich �����, ReturnStream(true) - ������ (plain) �����
            (dm.tblPonudiStavkiOPIS as TBlobField).LoadFromStream(frmNotepad.ReturnStream(false));
        end;
     frmNotepad.Free;
end;

procedure TfrmIzborStavkiPonuda.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmIzborStavkiPonuda.prefrli;
begin
end;

procedure TfrmIzborStavkiPonuda.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
    //dm.tblBodiranjeStavkiPonudi.FullRefresh;
end;
procedure TfrmIzborStavkiPonuda.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmIzborStavkiPonuda.FormShow(Sender: TObject);
begin
    SpremiForma(self);

    PanelVnesArtikal.Left:=130;
    PanelVnesArtikal.Top:=250;

    dm.tblIzborStavkiZaPonuda.Close;
    dm.tblIzborStavkiZaPonuda.Open;

    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    pom_vnesi_stavki_ponuda:=0;
end;
//------------------------------------------------------------------------------

procedure TfrmIzborStavkiPonuda.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIzborStavkiPonuda.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case Key of
        VK_INSERT:
          begin
           // dm.qCountDobitnici.Close;
           // dm.qCountDobitnici.ParamByName('tender').Value:=dm.tblTenderBROJ.Value;
           // dm.qCountDobitnici.ExecQuery;
           // if dm.qCountDobitnici.FldByName['br'].Value = 0 then
             //  begin
                 if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
                   begin
                    if dm.tblIzborStavkiZaPonudaVALIDNOST.Value = 1 then
                        begin
                          PanelVnesArtikal.Visible:=true;
                          dm.tblPonudiStavki.Insert;
                          if dm.tblIzborStavkiZaPonudaGENERIKA.Value = 1 then
                            begin
                              cxCheckBoxOpstNaziv.Checked:=true;
                              cxCheckBoxKreirajArtikal.Visible:=true;
                              cxCheckBoxKreirajArtikal.Checked:=false;
                            end
                          else
                            begin
                              cxCheckBoxOpstNaziv.Checked:=false;
                              cxCheckBoxKreirajArtikal.Visible:=false;
                            end;
                          JN_TIP_ARTIKAL.Text:=intToStr(dm.tblIzborStavkiZaPonudaJN_TIP_ARTIKAL.Value);
                          dm.tblPonudiStavkiVID_ARTIKAL.Value:=dm.tblIzborStavkiZaPonudaVID_ARTIKAL.Value;
                          dm.tblPonudiStavkiARTIKAL.Value:=dm.tblIzborStavkiZaPonudaARTIKAL.Value;
                          dm.tblPonudiStavkiMERKANAZIV.Value:=dm.tblIzborStavkiZaPonudaMERKANAZIV.Value;
                         { if (dm.tblTenderGRUPA.Value = 2) then
                              dm.tblPonudiStavkiKOLICINA.Value:=1
                          else   }  //ovaa kontrola e trgnata 27.04.20 na baranje od Medicinski fakultet
                              dm.tblPonudiStavkiKOLICINA.Value:=dm.tblIzborStavkiZaPonudaKOLICINA.Value;
                          dm.tblPonudiStavkiDANOK.Value:=dm.tblIzborStavkiZaPonudaTARIFA.Value;
                          dm.tblPonudiStavkiSTAVKA_NAZIV.Value:=dm.tblIzborStavkiZaPonudaSTAVKA_NAZIV.Value;
                          CENA_BEZ_DDV.SetFocus;
                        end
                    else ShowMessage('������������� ������ � ���������');
                   end;
             //  end
           // else ShowMessage('����� ������������ ��������� �� ������������� ����� �������. �� � ��������� �������� �� ���� ������ �� �������� !');
          end;
     end;
end;

procedure TfrmIzborStavkiPonuda.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzborStavkiPonuda.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
if (ARecord is TcxGridDataRow) and (ARecord.Values[cxGrid1DBTableView1VALIDNOST.Index] = 0) then
       AStyle := dmRes.RedLight;
end;

//  ����� �� �����
procedure TfrmIzborStavkiPonuda.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  id,vidArtikal,artikal:integer;
  brTender:string;
begin

   if (Validacija(PanelVnesArtikal) = false) then
    begin
       dm.tblPonudiStavkiPONUDA_ID.Value:=dm.tblPonudiID.Value;
       dm.tblPonudiStavkiBROJ_TENDER.Value:=dm.tblPonudiBROJ_TENDER.Value;
       dm.tblPonudiStavkiTIP_PARTNER.Value:=dm.tblPonudiTIP_PARTNER.Value;
       dm.tblPonudiStavkiPARTNER.Value:=dm.tblPonudiPARTNER.Value;
       dm.tblPonudiStavkiodbiena.Value:=dm.tblPonudiODBIENA.Value;
       DM.tblPonudiStavki.Post;
       ponuda_stavka_edit:=1;

       if cxCheckBoxKreirajArtikal.Checked then
          begin
             id:= dmMat.zemiMax(dmMat.MaxArtikal,'artvid',Null,Null,dm.tblPonudiStavkiVID_ARTIKAL.Value,Null,Null,'MAKS');
             dmPQ.insert6(dmPQ.pInsertArtikal,'NAZIV', 'ID','VID_ARTIKAL', 'ARTIKAL', Null, Null,
                                               OPIS.Text, //(dm.tblPonudiStavkiOPIS.AsString ),
                                               id,
                                               dm.tblPonudiStavkiVID_ARTIKAL.Value,
                                               dm.tblPonudiStavkiARTIKAL.Value, Null,Null);
          end;
       PanelVnesArtikal.Visible:=False;
       dm.tblIzborStavkiZaPonuda.FullRefresh;
       dmMat.tblArtikal.Close;
       dmMat.tblArtikal.Open;
       cxGrid1.SetFocus;
    end;

end;

//	����� �� ���������� �� �������
procedure TfrmIzborStavkiPonuda.aOtkaziExecute(Sender: TObject);
begin
      if PanelVnesArtikal.Visible = true then
         begin
            dm.tblPonudiStavki.Cancel;
            RestoreControls(PanelVnesArtikal);
            PanelVnesArtikal.Visible:=false;
            cxGrid1.SetFocus;
         end
      else
         begin
            ModalResult := mrCancel;
            Close();
         end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzborStavkiPonuda.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzborStavkiPonuda.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzborStavkiPonuda.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIzborStavkiPonuda.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzborStavkiPonuda.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzborStavkiPonuda.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzborStavkiPonuda.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzborStavkiPonuda.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
